<?php
class Customers_Addresses_model extends MY_Model{

	var $table = 'admin,orders,match_codes';
    var $column_order = array('admin.admin_id','admin.admin_name','admin.admin_state','admin.created_date');
    var $column_search = array('admin.admin_id','admin.admin_name', 'admin.admin_state','admin.created_date'); //set column field database for datatable searchable
    var $order = array('admin.admin_id' => 'asc');
	
	public function __construct(){

        $this->primary_key = 'address_id';

        $this->table = 'customers_addresses';

        $this->timestamps = FALSE;

        parent::__construct();

	}

	private function _get_datatables_query() {
        $this->db->select('admin.admin_id,admin.admin_name,admin.admin_state,admin.created_date,admin.updated_date,ulog.last_loggedin,ord.last_order_made,ord.spendamount,ulog.daylogins,mcode.used_code');
        $this->db->from('admin');
        $this->db->join('(SELECT MAX(created_date) AS last_order_made,user_id,(COUNT(id) / order_amount) as spendamount FROM orders GROUP BY user_id ORDER BY last_order_made DESC ) ord','ord.user_id = admin.admin_id', 'left');
        $this->db->join('(SELECT DATE(user_logs.login_date) last_loggedin,user_logs.user_id, COUNT(user_logs.user_id) daylogins
			FROM user_logs LEFT JOIN admin ON admin.admin_id = user_logs.user_id WHERE 
			user_logs.login_date BETWEEN DATE_FORMAT(admin.created_date,"%Y-%m-%d") AND DATE(NOW()) GROUP BY user_logs.login_date,user_logs.user_id ORDER BY user_logs.user_id) ulog','ulog.user_id = admin.admin_id','left');
         $this->db->join('(SELECT COUNT(match_codes.used_by) as used_code,match_codes.user_id FROM match_codes LEFT JOIN admin ON admin.admin_id = match_codes.user_id GROUP BY match_codes.user_id) mcode','mcode.user_id = admin.admin_id','left');
        $this->db->where('admin.login_with','customer');
        $this->db->group_by('admin.admin_id');
    
        if($this->input->post('time')){
        	if($this->input->post('time') == 'week'){
        		$this->db->where('admin.created_date BETWEEN DATE_ADD(CURDATE(), INTERVAL 1-DAYOFWEEK(CURDATE()) DAY) AND DATE_ADD(CURDATE(), INTERVAL 7-DAYOFWEEK(CURDATE()) DAY)');
        	}
        	else if($this->input->post('time') == 'month'){
            	$this->db->where('year(admin.created_date)', date('Y')); 
            	$this->db->where('month(admin.created_date)', date('m')); 
        	}
        	else if($this->input->post('time') == 'last_month'){
        		$this->db->where('YEAR(admin.created_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(admin.created_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)');
        	}
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if (isset($_POST['search']['value'])) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
        	if($_POST['order']['0']['column']=="4"){
                $this->db->order_by('ord.spendamount', $_POST['order']['0']['dir']);
            }else if($_POST['order']['0']['column']=="5"){
                $this->db->order_by('ord.last_order_made', $_POST['order']['0']['dir']);
            }else if($_POST['order']['0']['column']=="6"){
                $this->db->order_by('admin.updated_date', $_POST['order']['0']['dir']);
            }elseif($_POST['order']['0']['column']=="7"){
                $this->db->order_by('ulog.daylogins', $_POST['order']['0']['dir']);
            }elseif($_POST['order']['0']['column']=="8"){
                $this->db->order_by('mcode.used_code', $_POST['order']['0']['dir']);
            }
            else{
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != 1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
       // echo $this->db->last_query();exit;
        return $query->result();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from('products');
        return $this->db->count_all_results();
    }

	// Get address
	function getaddress_by_user($user=null,$address=null){
		$this->db->select('address_id,make_default');
		$this->db->from('customers_addresses');
		$this->db->where_not_in('address_id',[$address]);
		$this->db->where('customer_id',$user);	
		$query = $this->db->get();
		return $query->row();
	}

	// Save bank information
	function save_bankdata($user=null,$data=null){
		if(empty($user)){
			return $this->db->insert("customer_bankinfo",$data);
		}
		else{
			return $this->db->where("customer_id",$user)->update("customer_bankinfo",$data);
		}
	}

	// Check records exists in bank table
	function check_record($key=null){
		$this->db->where('customer_id',$key);
	    $query = $this->db->get('customer_bankinfo');
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}

	// Get bankinfo by customer
	function fetch_bankdata($key=null){
		$this->db->where('customer_id',$key);
	    $query = $this->db->get('customer_bankinfo');
	    if ($query->num_rows() > 0){
	        return $query->row();
	    }
	    else{
	        return false;
	    }
	}

}
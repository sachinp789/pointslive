<?php



class Product_images_model extends MY_Model{

    var $table = 'products,categories,product_images,categories_products';
    var $column_order = array('products.product_id','products.product_name', 'product_images.product_image' ,'products.product_price', 'products.total_qty', 'categories.category_name');
    var $column_search = array('products.product_id','products.product_name', 'product_images.product_image','products.product_price','products.total_qty', 'categories.category_name'); //set column field database for datatable searchable
    var $order = array('products.product_id' => 'desc');

	public function __construct(){

		 

        $this->primary_key = 'image_id';

        $this->table = 'product_images';

        $this->timestamps = FALSE;

        parent::__construct();
        $this->load->database();

	}

    private function _get_datatables_query() {
        $this->db->select('products.product_id,products.product_name,products.global_link,sc.cart,products.total_qty,IFNULL(o.reserved,0),(total_qty - (IFNULL(sc.cart,0) + IFNULL(o.reserved,0))) as available_qty,IFNULL(total_qty,0) as total_qty,(IFNULL(sc.cart,0) + IFNULL(o.reserved,0)) as total_reserved');       
        $this->db->from('products');
        $this->db->join('product_images', 'product_images.product_id=products.product_id','LEFT');
        $this->db->join('categories_products', 'categories_products.product_id=products.product_id','LEFT');
        $this->db->join('(select `product_id`, sum(qty) as cart from `shopping_carts` group by `product_id` ) sc','products.product_id = sc.product_id','LEFT');
        $this->db->join('(select `product_id`, sum(ordered_qty) as reserved from `orders_products` LEFT JOIN orders ON orders.transaction_id=orders_products.order_id AND orders.status="0" group by `product_id` ) o', 'o.product_id=products.product_id','LEFT');
        $this->db->join('categories', 'categories.category_id=categories_products.category_id', 'LEFT');
        $this->db->group_by('products.product_id');

        if(isset($_POST['stock'])){
            if($_POST['stock']=="1"){
                $this->db->having('available_qty > ','0');
            }
            if($_POST['stock']=="0"){
                $this->db->having('total_qty < ','1');
            }
        }

        if(!empty($this->session->userdata('isLogin'))){
            $this->db->where('products.created_by', $this->session->userdata('adminId'));
        }

        if($this->input->post('category')){
            $this->db->where('categories.category_id', $this->input->post('category')); 
        }

        if($this->input->post('hasimage')){
            if($this->input->post('hasimage') == "2"){
                $this->db->where("product_images.product_image IS NULL");
            }
            else if($this->input->post('hasimage') == "1"){
                $this->db->where("product_images.product_image IS NOT NULL");
            }
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if (isset($_POST['search']['value'])) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            if($_POST['order']['0']['column']=="6"){
                $this->db->order_by('available_qty', $_POST['order']['0']['dir']);
            }elseif($_POST['order']['0']['column']=="7"){
                $this->db->order_by('total_reserved', $_POST['order']['0']['dir']);
            }else{
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != 1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from('products');
        return $this->db->count_all_results();
    }

	 function product_images($img_data){

        $this->db->insert('product_images', $img_data);

    }

    // Get images by pid
    function get_images_by_id($pid){
        $this->db->select('GROUP_CONCAT(product_image) as images,GROUP_CONCAT(image_id) as imageids');
        $this->db->from('product_images');
        $this->db->where('product_images.product_id', $pid);
        $this->db->order_by('image_id','asc');
        $query_final = $this->db->get();
        return $query_final->result();
    }

}
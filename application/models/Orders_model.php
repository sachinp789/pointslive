<?php
class Orders_model extends MY_Model{

	var $table = 'orders,products,address,orders_products,admin';
   	var $column_order = array('orders.transaction_id', 'address.receiver_name', 'orders.facebook_username', 'orders.payment_method', 'orders.created_date', 'orders.order_amount', 'orders.status', 'products.shipping_type');
	var $column_search = array('orders.transaction_id', 'address.receiver_name', 'orders.facebook_username','orders.payment_method','orders.created_date', 'orders.order_amount','orders.status','products.shipping_type'); //set column field database for datatable searchable
	var $order = array('orders.transaction_id' => 'desc');

	public function __construct(){

        $this->primary_key = 'id';

        $this->table = 'orders';

        $this->timestamps = FALSE;

        parent::__construct();

        $this->load->database();

	}

	private function _get_datatables_query() {
		$this->db->select('orders.transaction_id,orders.id,orders.store_type,orders.status,orders.facebook_username,orders.payment_method,orders.created_date,orders.order_amount,address.receiver_name,products.shipping_type');
		$this->db->from('orders');
		$this->db->join('orders_products', 'orders_products.order_id=orders.transaction_id','LEFT');
		$this->db->join('address', 'orders.shipping_address=address.id','LEFT');
		$this->db->join('admin', 'admin.admin_id=orders.created_by', 'LEFT');
		$this->db->join('products', 'products.product_id=orders_products.product_id', 'LEFT');
		$this->db->group_by('orders.id');
		$this->db->where('orders.auto_cancelled', '0');
		//echo $this->db->last_query();exit;

		/*if ($this->input->post('shipping_type')) {
			$this->db->where('products.shipping_type', trim($this->input->post('shipping_type')));
		}*/

		if ($this->input->post('shipping_type')) {
			$this->db->where('orders.store_type', trim($this->input->post('shipping_type')));
		}

		if ($this->input->post('payment_type')) {
			$this->db->like('orders.payment_method', trim($this->input->post('payment_type')));
		}

		if($this->input->post('payment_status') && $this->input->post('payment_status') === 0){
			$this->db->where_in('orders.status',$this->input->post('payment_status'));
		}
		else if($this->input->post('payment_status') === '0') {
			$this->db->where('orders.status',$this->input->post('payment_status'));
		}
		else if($this->input->post('payment_status') == 0){
			$this->db->where_in('orders.status',['0','1','2','3','5','6','7']);
		}else{
			$this->db->where('orders.status', trim($this->input->post('payment_status')));
		}

		if(!empty($this->session->userdata('isLogin'))){
			$this->db->where('orders.created_by', $this->session->userdata('adminId'));
		}

		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if (isset($_POST['search']['value'])) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
				{
					$this->db->group_end();
				}
				//close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables() {
		$this->_get_datatables_query();
		if ($_POST['length'] != 1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}

		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function count_filtered() {
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
		$this->db->from('orders');
		return $this->db->count_all_results();
	}

	public function shiporder($data){

		$this->db->where('id', $data['id']);

    	$this->db->update('orders', array('status' => $data['status'],'shipment_date' => $data['shipment_date']));

    	return true;

	}
	public function ordercomplete($data){

		$this->db->where('id', $data['id']);

    	$this->db->update('orders', array('status' => $data['status'],'complete_date' => $data['complete_date']));

    	return true;

	}
	public function cancelorder($data){

		$this->db->where('id', $data['id']);

    	$this->db->update('orders', array('status' => $data['status']));

    	return true;

	}

	// check shipping order status by orderID
	public function checkShipppingStatus($orderID=""){
		$status = $this->db
				->from('orders')
				->where(['transaction_id' => $orderID])
				->get()
				->result();
		return $status;
	}

	//Ajax get shipping types list
	public function get_shippingtypes($orderID=""){
		$this->db->select('products.shipping_type,orders_products.order_id');
		$this->db->from('products');
		$this->db->join('orders_products', 'products.product_id = orders_products.product_id');
		//$this->db->join('orders', 'orders_products.order_id = orders.transaction_id');
		$this->db->where('orders_products.order_id', $orderID);
		$query_final = $this->db->get();
		return $query_final->result();
	}

	// Get all commission list
	public function get_commissions_charges()
    {
        $this->db->select('commission_fees.*,orders.*,admin.admin_name');
        $this->db->from('commission_fees');
        $this->db->join('orders', 'orders.id = commission_fees.order_id');
        $this->db->join('admin', 'admin.admin_id = commission_fees.seller_id');
        $this->db->order_by("commission_fees.id", "asc");
        $query_final = $this->db->get();
		return $query_final->result();
    }

     // Fetch orders data of cancelled,confirmed failed,auto cancelled
    public function fetch_orders(){
    	$this->db->select('id,user_id,order_amount,created_date');
        $this->db->from('orders');
        $this->db->group_start(); //this will start grouping
	    $this->db->where_in('status',['3','7']);
	    $this->db->or_where('auto_cancelled =', "1");
	    $this->db->group_end(); //this will end grouping
	    $this->db->where("payment_method","prepaid");
	    $this->db->where("refund_status","0");
        $query_final = $this->db->get();
		return $query_final->result();
    }
}
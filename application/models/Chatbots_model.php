<?php
class Chatbots_model extends MY_Model{
	public function __construct(){
        $this->primary_key = 'id';
        $this->table = 'chatbots';
        $this->timestamps = FALSE;
        parent::__construct();
	}

	// Get chatbot details
	public function getChatData(){
		$this->db->select('*');
		$this->db->from('chatbots');
		$this->db->where('created_by',$this->session->userdata('adminId'));
		$query = $this->db->get();
		return $query->row();
	}

}

<?php
class Coupons_model extends MY_Model{
	public function __construct(){
        $this->primary_key = 'id';
        $this->table = 'coupons';
        $this->timestamps = FALSE;
        parent::__construct();
	}

	/********** Get redemptions list **********/
	public function getRedemtionsList($page_id=""){
		$this->db->select('cr.*,c.code');
		$this->db->from('coupon_redemptions as cr');
		$this->db->join('coupons as c', 'c.id = cr.coupon_id');
		if(!empty($this->session->userdata('isLogin'))){
			$this->db->where('c.created_by',$this->session->userdata('adminId'));
		}
		elseif(!empty($this->session->userdata('pagemanager'))){
			$this->db->where('c.page_id',$this->session->userdata('pagemanager'));
		}
		/*if($page_id != ""){
		$this->db->where('c.page_id',$page_id);
		}*/
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result();
	}
	/********** Check no of uses coupon by user **********/
	public function usedcountCoupon_By_User($coupon_id="",$userid=""){
		$this->db->select('COUNT(*) AS total_used');
		$this->db->from('coupon_redemptions');
		$this->db->where('coupon_id',$coupon_id);
		$this->db->where('user_id',$userid);
		$this->db->group_by('user_id');
		$query = $this->db->get();
		return $query->row();
	}
	/************ Check coupon code valid **************/
	public function isValidCoupon($code="",$page_id=""){
		$this->db->where('code', $code);
		$this->db->where('page_id', $page_id);
		$this->db->where('status', '1');
		$query = $this->db->get('coupons');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}
		else{
			return false;
		}
	}
	/************** Remove coupon applied **************/
	public function removeCoupon($coupon_id="",$user_id="",$page=""){
		$this->db->where('id',$coupon_id);
		$this->db->where('user_id' , $user_id);
		$this->db->where('page_id' , $page);
    	$query = $this->db->delete("coupon_redemptions");
    	//return $this->db->last_query();
    	return $query;
	}
}

<?php
class Packages_model extends MY_Model{
	public function __construct(){
        $this->primary_key = 'id';
        $this->table = 'packages';
        $this->timestamps = FALSE;
        parent::__construct();
	}

	// List of packages
	public function listPackages($limit,$page){
		$this->db->select('*');
		$this->db->from('packages');
		$this->db->order_by('id','desc');
		$this->db->limit($limit,$page);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	// get packages by name
	public function get_by_Name($pakname="",$lang="english"){
		$this->db->select('*');
		$this->db->from('packages');
		if($lang == 'english'){
			$this->db->where("package_country",$pakname);
		}
		else{
			$this->db->where("package_country_thai",$pakname);
		}
		$query = $this->db->get();
		return $query->row();
	}

}

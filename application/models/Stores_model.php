<?php

class Stores_model extends MY_Model{

	var $table = 'payment_cycles,weekly_payment_generates,admin';
   	var $column_order = array('admin.admin_id', 'weekly_payment_generates.payment_cycle_from', 'weekly_payment_generates.payment_cycle_to','weekly_payment_generates.billing_amount', 'weekly_payment_generates.auto_payment', 'weekly_payment_generates.payment_status');
	var $column_search = array('admin.admin_id', 'weekly_payment_generates.payment_cycle_from', 'weekly_payment_generates.payment_cycle_to','weekly_payment_generates.payment_status'); //set column field database for datatable searchable
	var $order = array('weekly_payment_generates.id' => 'desc');

	public function __construct(){

        $this->primary_key = 'id';

        $this->table = 'stores';

        $this->timestamps = FALSE;

        parent::__construct();
        $this->load->database();

	}

	private function _get_datatables_query() { 
		$this->db->select('weekly_payment_generates.*,admin.admin_name,payment_cycles.payment_bill_amount,payment_cycles.payment_by as cycle_make_payment,payment_cycles.payment_status as cycle_make_status,SUM(payment_cycles.payment_bill_amount) as Totalpaid');
		$this->db->from('weekly_payment_generates');
		$this->db->join('payment_cycles', 'payment_cycles.weekly_payment_id=weekly_payment_generates.id','LEFT');
		$this->db->join('admin', 'admin.admin_id=weekly_payment_generates.seller_id','LEFT');
		//$this->db->where('weekly_payment_generates.payment_cycle_to',$this->input->post('payment_to'));
		$this->db->group_by('weekly_payment_generates.id');

		if($this->input->post('seller')){
			$this->db->where('weekly_payment_generates.seller_id', $this->input->post('seller'));
		}

		if($this->input->post('payment_status')!=""){
			$this->db->where('weekly_payment_generates.payment_status',$this->input->post('payment_status'));
		}

		if($this->input->post('payment_from') && $this->input->post('payment_to')){
			$this->db->where('weekly_payment_generates.payment_cycle_from',$this->input->post('payment_from'));
			$this->db->where('weekly_payment_generates.payment_cycle_to',$this->input->post('payment_to'));
		}

		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if (isset($_POST['search']['value'])) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
				{
					$this->db->group_end();
				}
				//close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables() {
		$this->_get_datatables_query();
		if ($_POST['length'] != 1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}

		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function count_filtered() {
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
		$this->db->from('weekly_payment_generates');
		return $this->db->count_all_results();
	}

	// List of stores
	public function getStores(){

		if(!empty($this->session->userdata('isLogin'))){
			return $this->Stores_model->fields('page_id,page_name')->where('created_by',$this->session->userdata('adminId'))->order_by('created_date','desc')->get_all();
		}
		else{	
			return $this->Stores_model->fields('page_id,page_name')->order_by('created_date','desc')->get_all();
		}

	}
	// Return name of store
	public function getStoreName($page_id=""){
		return $this->Stores_model->where(['page_id' => $page_id])->get();
	}

}


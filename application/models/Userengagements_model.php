<?php
class Userengagements_model extends MY_Model{
	public function __construct(){
        $this->primary_key = 'id';
        $this->table = 'userengagements';
        $this->timestamps = FALSE;
        parent::__construct();
	}

}

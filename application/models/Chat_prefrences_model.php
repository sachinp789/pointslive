<?php

class Chat_prefrences_model extends MY_Model{

	public function __construct(){
        $this->primary_key = 'id';
        $this->table = 'chat_prefrences';
        $this->timestamps = FALSE;
        parent::__construct();
	}

}
<?php



class Admin_model extends MY_Model{



	public $primary_key = 'admin_id';

	public $table = 'admin';

	function email_exists($key)
	{
	    $this->db->where('admin_email',$key);
	    $query = $this->db->get('admin');
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}

	// Validate referral code
	function validate_referralcode($code=null){
		$this->db->where('customer_referral_code',$code);
	    $query = $this->db->get('admin');
	    if ($query->num_rows() > 0){
	        return $query->row();
	    }
	    else{
	        return false;
	    }
	}
}
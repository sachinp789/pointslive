<?php
class Orders_products_model extends MY_Model{

	var $table = 'orders,admin,commission_fees,payment_cycles,weekly_payment_generates';
   	var $column_order = array('orders.transaction_id','orders.created_date','orders.order_amount');
	var $column_search = array('orders.transaction_id','orders.created_date','orders.order_amount'); //set column field database for datatable searchable
	var $order = array('orders.transaction_id' => 'desc');

	public function __construct(){

		$this->primary_key = 'id';

        $this->table = 'orders_products';

        $this->timestamps = FALSE;

        parent::__construct();

	}

	private function _get_datatables_query() {
		$this->db->select('orders.id,orders.transaction_id,orders.created_date as order_date,orders.order_amount,orders.created_by,commission_fees.akin_commission,commission_fees.auto_cancelled_fees,commission_fees.transaction_type,payment_cycles.*,payment_cycles.payment_date as paid_date,weekly_payment_generates.*,weekly_payment_generates.payment_status as paid_status');
		$this->db->from('orders');
		$this->db->join('commission_fees', 'commission_fees.order_id=orders.id');
		$this->db->join('weekly_payment_generates','weekly_payment_generates.id=commission_fees.payment_id','LEFT');
		$this->db->join('payment_cycles', 'payment_cycles.weekly_payment_id=weekly_payment_generates.id','LEFT');
		$this->db->group_by('commission_fees.id');

		if($this->input->post('transaction_type')){
			$this->db->where('commission_fees.transaction_type', $this->input->post('transaction_type'));
		}

		if($this->input->post('payment_status')!=''){
			$this->db->where('weekly_payment_generates.payment_status', $this->input->post('payment_status'));
		}

		/*if($this->input->post('payment_from') && empty($this->input->post('payment_to'))){
			$this->db->where('payment_cycles.created_date >= ', $this->input->post('payment_from'));
		}*/
		if($this->input->post('payment_from') && $this->input->post('payment_to')){ 
			$this->db->where('weekly_payment_generates.payment_cycle_from >= ', $this->input->post('payment_from'));
			$this->db->where('weekly_payment_generates.payment_cycle_to <= ',$this->input->post('payment_to'));
		}

		if(!empty($this->session->userdata('isLogin'))) {
			$this->db->where('orders.created_by', $this->session->userdata('adminId'));
		}
	
		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if (isset($_POST['search']['value'])) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
				{
					$this->db->group_end();
				}
				//close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables() {
		$this->_get_datatables_query();
		if ($_POST['length'] != 1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result();
	}

	public function count_filtered() {
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
		$this->db->from('orders');
		return $this->db->count_all_results();
	}

	// Get top 3 selling products
	public function getTopSellingItems(){
		$this->db->select('p.product_sku,p.product_name,p.product_name_thai,p.product_price,p.sale_price,p.product_description,p.product_description_thai,pi.product_image,op.product_id,sum(op.ordered_qty) as totalquantity');
		$this->db->from('orders_products as op');
		$this->db->join('products as p','p.product_id=op.product_id');
		$this->db->join('product_images as pi','pi.product_id=op.product_id');
		$this->db->group_by('op.product_id');
		$this->db->order_by('sum(op.ordered_qty)','DESC');
		$this->db->limit(3);
		$query=$this->db->get();
		$data= $query->result_array();
		return $data;
	}

}
<?php
class Products_model extends MY_Model{

    var $table = 'products,categories,product_images,categories_products,admin';
    var $column_order = array('products.product_id','products.product_name', 'product_images.product_image' ,'products.product_price', 'products.total_qty', 'categories.category_name');
    var $column_search = array('products.product_id','products.product_name', 'product_images.product_image','products.product_price','products.total_qty', 'categories.category_name'); //set column field database for datatable searchable
    var $order = array('products.product_id' => 'asc');

	public function __construct(){		 

        $this->primary_key = 'product_id';
        $this->table = 'products';
        $this->timestamps = FALSE;
        $this->created_date = 'DESC'; 

		//relationships

        $this->has_many = array(
            'images' => array(
                'foreign_model' => 'Product_images_model',
                'foreign_table' => 'product_images',
                'foreign_key' => 'product_id',
                'local_key' => 'product_id'
            ),
        );

        $this->has_many_pivot = array(
            'categories' => array(
                'foreign_model' => 'Categories_model',
                'pivot_table' => 'categories_products',
                'local_key' => 'product_id',
                'pivot_local_key' => 'product_id',
                'pivot_foreign_key' => 'category_id',
                'foreign_key' => 'category_id',
                'get_relate' => true
            )
        );

        parent::__construct();
        $this->load->database();

	}

     private function _get_datatables_query() { // IFNULL(sc.cart,0) + (available qty remark) 
        $this->db->select('products.product_sku,products.product_name,products.product_id,products.global_link,products.product_price,products.sale_price,products.total_qty,products.created_date,products.created_by,categories.category_name,product_images.product_image,sc.cart,products.total_qty,IFNULL(o.reserved,0),(total_qty - (IFNULL(o.reserved,0))) as available_qty,IFNULL(total_qty,0) as total_qty,(IFNULL(o.reserved,0)) as total_reserved');       // IFNULL(sc.cart,0) + (total reserved remark)
        $this->db->from('products');
        $this->db->join('product_images', 'product_images.product_id=products.product_id','LEFT');
        $this->db->join('categories_products', 'categories_products.product_id=products.product_id','LEFT');
        $this->db->join('(select `product_id`, sum(qty) as cart from `shopping_carts` group by `product_id` ) sc','products.product_id = sc.product_id','LEFT');
        $this->db->join('(select `product_id`, sum(ordered_qty) as reserved from `orders_products` LEFT JOIN orders ON orders.transaction_id=orders_products.order_id AND orders.status="0" group by `product_id` ) o', 'o.product_id=products.product_id','LEFT');
        $this->db->join('categories', 'categories.category_id=categories_products.category_id', 'LEFT');
        $this->db->join('admin', 'admin.admin_id=products.created_by', 'LEFT');
        $this->db->group_by('products.product_id');
       
        if(isset($_POST['stock'])){
            if($_POST['stock']=="1"){
                $this->db->having('available_qty > ','0');
            }
            if($_POST['stock']=="0"){
                $this->db->having('total_qty < ','1');
            }
        }

        if(!empty($this->session->userdata('isLogin'))){
            $this->db->where('products.created_by', $this->session->userdata('adminId'));
        }

        if($this->input->post('category')){
            $this->db->where('categories.category_id', $this->input->post('category')); 
        }

        if($this->input->post('sellerproduct')){
            $this->db->where('products.created_by', $this->input->post('sellerproduct'));
        }

        if($this->input->post('hasimage')){
            if($this->input->post('hasimage') == "2"){
                $this->db->where("product_images.product_image IS NULL");
            }
            else if($this->input->post('hasimage') == "1"){
                $this->db->where("product_images.product_image IS NOT NULL");
            }
        }

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if (isset($_POST['search']['value'])) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                {
                    $this->db->group_end();
                }
                //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {   
            if($_POST['order']['0']['column']=="2"){
                $this->db->order_by('product_name', $_POST['order']['0']['dir']);
            }
            elseif($_POST['order']['0']['column']=="4"){
                $this->db->order_by('product_price', $_POST['order']['0']['dir']);
            }
            elseif($_POST['order']['0']['column']=="6"){
                $this->db->order_by('available_qty', $_POST['order']['0']['dir']);
            }
            elseif($_POST['order']['0']['column']=="7"){
                $this->db->order_by('total_qty', $_POST['order']['0']['dir']);
            }elseif($_POST['order']['0']['column']=="8"){
                $this->db->order_by('total_reserved', $_POST['order']['0']['dir']);
            }else{
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            /*$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);*/
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables() {
        $this->_get_datatables_query();
        if ($_POST['length'] != 1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result();
    }

    public function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all() {
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    //Ajax get product images
    public function GetProductImagesById($productID=""){
        $this->db->select('products.product_id,product_images.*');
        $this->db->from('products');
        $this->db->join('product_images', 'products.product_id = product_images.product_id');
        //$this->db->join('orders', 'orders_products.order_id = orders.transaction_id');
        $this->db->where('product_images.product_id', $productID);
        $query_final = $this->db->get();
        return $query_final->result();
    }

    //Ajax get product categories
    public function GetProductCategoryById($productID=""){
        $this->db->select('categories.category_name,products.product_id');
        $this->db->from('products');
        $this->db->join('categories_products', 'products.product_id = categories_products.product_id');
        $this->db->join('categories', 'categories.category_id = categories_products.category_id');
        //$this->db->join('orders', 'orders_products.order_id = orders.transaction_id');
        $this->db->where('products.product_id', $productID);
        $query_final = $this->db->get();
        return $query_final->result();
    }

    //Ajax get product categories
    public function GetCategories(){
        //var_dump($this->session->userdata('pagemanager'));exit;
        $this->db->select('categories.*');
        $this->db->from('categories');
        if(!empty($this->session->userdata('isLogin'))){
            $this->db->where('categories.created_by', $this->session->userdata('adminId'));
        }
        elseif (!is_null($this->session->userdata('pagemanager'))) {
           $this->db->where('categories.page_id', $this->session->userdata('pagemanager'));
        }
        $query_final = $this->db->get();
        return $query_final->result();
    }

     // Check SKU exists or not
    public function HasSku($sku=null,$pid=null){
        $this->db->select('product_sku');
        $this->db->from('products');
        $this->db->where('product_sku',$sku);
        if(!empty($pid)){
           $this->db->where_not_in('product_id',[$pid]);
        }
        $result = $this->db->get();
        return $result;
    }
}
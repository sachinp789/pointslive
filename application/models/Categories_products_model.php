<?php

class Categories_products_model extends MY_Model{

	public function __construct(){

		$this->primary_key = 'id';
        $this->table = 'categories_products';
	}
}
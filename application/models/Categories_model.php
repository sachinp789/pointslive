<?php



class Categories_model extends MY_Model{



	public $primary_key = 'category_id';

	public $table = 'categories';



	public function new_category($data){

		$this->db->insert('categories', $data);

	}

	//025-05-2018 - Get categories by page and user
	public function getCategoryByUser(){
		$this->db->select('*');
		$this->db->from('categories');
		if(!empty($this->session->userdata('isLogin'))){
			$this->db->where('created_by',$this->session->userdata('adminId'));
		}
		else if(!empty($this->session->userdata('pagemanager'))){
			$this->db->where('page_id',$this->session->userdata('pagemanager'));
		}		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}



}
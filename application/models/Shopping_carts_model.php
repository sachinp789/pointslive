<?php
class Shopping_carts_model extends MY_Model{
	public function __construct(){

        $this->primary_key = 'id';

        $this->table = 'shopping_carts';

        $this->timestamps = FALSE;

        parent::__construct();

	}

	function updatecustomer(){
		$this->db->set('user_id', $this->session->userdata("customerId"));
		$this->db->where('user_id', $this->session->userdata("guest"));
    	$this->db->update('shopping_carts');
    	return true;
	}
}
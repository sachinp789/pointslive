<?php

class Product_categories_model extends MY_Model{

	public function __construct(){
		 
        $this->primary_key = 'id';
        $this->table = 'categories_products';
        $this->timestamps = FALSE;
        
        parent::__construct();
	}

	 function product_with_categories($data){
        $this->db->insert('categories_products', $data);
    }

} 
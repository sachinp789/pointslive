<?php

class Address_model extends MY_Model{

	public function __construct(){
        $this->primary_key = 'id';
        $this->table = 'address';
        $this->timestamps = FALSE;
        parent::__construct();
	}

}
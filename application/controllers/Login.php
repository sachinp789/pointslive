<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Admin_model');

		/*if($this->session->userdata('adminId') == 1) {
        	redirect(base_url() . 'admin/dashboard');
    	}*/
	}

	/**
		Login Form
	**/
	public function index()
	{	
		// Customer redirection if logged in
		if(!empty($this->session->userdata('customerId'))){
			redirect(base_url('item/order_history'));
		}

		$data['content'] = 'login';
		$data['title'] = 'Admin Login';
		$data['page_title'] = 'Login';
		$this->load->view('login', $data);
	}
	/** Process of login *****/
	public function processLogin(){
		$data = array();
        if($this->input->post()){
        
        	$this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

             if ($this->form_validation->run() == true && 
                filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
                $senddata = array(	
                    'admin_email'=> $this->input->post('email'),
                    'admin_password' => $this->_hash($this->input->post('password',true))
                );
                $checkLogin = $this->Admin_model->get($senddata);

                if($checkLogin && is_null($checkLogin->login_with)){
                    $this->session->set_userdata('username',$checkLogin->admin_name);
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('adminId',$checkLogin->admin_id);
                    $this->session->set_userdata('pagemanager',$checkLogin->page_id);
                    redirect(base_url().'admin/dashboard');
                }else{
                	$data['msg'] = 'danger';
                    $data['error_msg'] = 'Wrong email or password, please try again.';
                }
            }
        }

		$data['title'] = 'Admin Login';
		$data['page_title'] = 'Login';
        //load the view
        $this->load->view('login', $data);
	}
	/**
		Reset Password Form and send email to user
	**/
	public function forgotPassword()
	{	
		$data = array();

		if($this->input->post()){

			$this->form_validation->set_rules('email','Email','trim|required|xss_clean');

			if($this->form_validation->run() == true && 
                filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
				$email = strip_tags($this->input->post('email'));
				$checkEmail = $this->Admin_model->get(['admin_email' => $email]);

				if($checkEmail){
					$this->load->helper('string');

                    $token = random_string('alnum', 32); // Generate New token
                    $this->Admin_model->update(['password_token' => $token],['admin_email' => $checkEmail->admin_email]);
                    //send email to entered email address
                    $this->data['name'] = $checkEmail->admin_name;
                    $this->data['reset_password_url'] = base_url("login/resetPassword/" . $token);
                    $message = $this->load->view('templates/reset_email_template', $this->data, true);

                    $config = emailconfig();
                    $this->load->library('email',$config);       
                    $this->email->set_newline("\r\n");
             
                    $this->email->from('no-reply@pointslive.com', config_item('site_name'));
                    $this->email->to($checkEmail->admin_email);
                    $this->email->subject('Reset Password');
                    $this->email->message($message);

                    try {
                        if ($this->email->send()) {
                        	$this->session->set_flashdata('flashmsg', 'Please check your email to reset your password.');
                        	$this->session->set_flashdata('msg', 'success');
                        }
                        else{
                        	$this->session->set_flashdata('flashmsg', 'Error while send mail! Please try again or try after sometime.');
                        	$this->session->set_flashdata('msg', 'danger');
                        }

                    } catch (Exception $e) {
                    	$this->session->set_flashdata('flashmsg', 'Error while send mail! Please try again or try after sometime.');
                        $this->session->set_flashdata('msg', 'danger');
                    }
				}
				else{
					$this->session->set_flashdata('flashmsg', 'Email associated with user does not match.');
                    $this->session->set_flashdata('msg', 'danger');
				}
			//redirect(base_url().'login/forgotPassword');
			}
		}
		$data['content'] = 'Forgot Password';
		$data['title'] = 'Forgot Password';
		$data['page_title'] = 'Forgot Password';
		$this->load->view('forgot_password', $data);
	}

	// Reset password with token
	public function resetPassword($token){
		$data = array();

		if($this->input->post()){
			$checkToken = $this->Admin_model->get(['password_token' => $token]); // Check Token
			if($checkToken){ // Valid
				$this->form_validation->set_rules('new_password','New Password','trim|required|xss_clean');

				if($this->form_validation->run() == true) {
					$newpassword = $this->_hash($this->input->post('new_password',true)); // Set new password
					$updatedtoken = $this->Admin_model->update(['password_token' => '','admin_password' => $newpassword],['admin_email' => $checkToken->admin_email]);

					if($updatedtoken){
						$this->session->set_flashdata('flashmsg', 'New password has been set successfully.');
		                $this->session->set_flashdata('msg', 'success');
					}
				}
			}
			else{
				$this->session->set_flashdata('flashmsg', 'Password token has been expired.');
            	$this->session->set_flashdata('msg', 'danger');
			}
			redirect($this->uri->uri_string());
		}

		$data['content'] = 'Reset Password';
		$data['title'] = 'Reset Password';
		$data['page_title'] = 'Reset Password';
		$data['token'] = $token;
		$this->load->view('reset_password', $data);
	}	

}

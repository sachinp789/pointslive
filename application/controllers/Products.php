<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	public $pageID="";



	public function __construct(){

		parent::__construct();
	    $this->load->library('form_validation');
	    $this->load->model('Products_model');
	    $this->load->model('Product_images_model');
	    $this->load->model('Product_categories_model');	
	    $this->load->model('Categories_model');
	    $this->load->model('Stores_model');
	    $this->load->library('unzip');
        $this->load->library('upload');
        $this->pageID = $this->session->userdata('pagemanager');
	}



	// Add new category



	public function newProduct(){

	    $data['content'] = 'admin/products/addproduct';
	    $data['title'] = 'Add New Product';
	    $data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);

	} 



	



	// Save products



	public function saveProduct(){

		 $maindata = array();
		 $pdata = array();
		 $this->load->library('ciqrcode');
		// For vendor
        /*if(!empty($this->session->userdata('isLogin'))){
            $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();

            if(!$pages){
              $this->session->set_flashdata('flashmsg', 'Store setup is required');
              $this->session->set_flashdata('msg', 'danger');
              redirect(base_url('categories/newCategory'));
            }
        }*/

	     if($this->input->post())
	     {

			     	$this->form_validation->set_rules('product_name','Product Name','trim|xss_clean|required');
			     	/*$this->form_validation->set_rules('product_sku', 'SKU', 'trim|xss_clean|required|is_unique[products.product_sku]');*/
				    $this->form_validation->set_rules('product_price','Product Price','trim|xss_clean|required');
				    $this->form_validation->set_rules('sale_price','Sale Price','trim|xss_clean');
				    $this->form_validation->set_rules('description','Description','trim|xss_clean|required');
				   //$this->form_validation->set_rules('inventory','Inventory','trim|xss_clean|required');
				    //$this->form_validation->set_rules('shipping_cost','Shipping Cost','trim|xss_clean');
				    //$this->form_validation->set_rules('facebook_page','facebook_page','trim|xss_clean|required');
				    if($this->form_validation->run() == true) {


				        $data = [

				         // 'product_sku' 		=> $this->input->post('product_sku'),
				          'product_name' 		=> $this->input->post('product_name'),
				          'product_name_thai' 		=> $this->input->post('product_name_thai'),
				          'shipping_type' 		=> $this->input->post('shippingtype'),
				           //'shortlink'			=> $this->input->post('finallink'),
				          'product_price' 		=> $this->input->post('product_price'),
				          'sale_price'			=> $this->input->post('sale_price'),
				          'product_description'	=> $this->input->post('description'),
				          'product_description_thai'	=> $this->input->post('product_description_thai'),
				          'total_qty' 			=> $this->input->post('total_qty'),
				         // 'inventory' 			=> $this->input->post('inventory'),
				          'shipping_cost'		=> $this->input->post('shipping_cost'),
				          'created_by' => $this->session->userdata('adminId')
				          //'facebook_page' => is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID 

				        ];

				        if(!empty($this->session->userdata('isLogin'))){
		                    $data['facebook_page'] = $this->input->post('facebook_page');
		                }
		                else{
		                    $data['facebook_page'] = $this->input->post('facebook_page');
		                }

				       /* if(is_null($this->pageID)){
				        	$page = $this->input->post('facebook_page');
				        }
				        else{
				        	$page = $this->pageID;
				        }*/

				        // Shortlink QR code generates
			            $data['qrcode']="";
			            if(!empty($this->input->post('finallink'))){
			            	$data['shortlink'] = "http://m.me/{$data['facebook_page']}?ref=".$this->input->post('finallink');
			                //$this->load->library('ciqrcode');
			                $qr_image=rand().'.png';
			                $params['data'] = "http://m.me/{$data['facebook_page']}?ref=".$this->input->post('finallink');
			                $params['level'] = 'H';
			                $params['size'] = 8;
			                $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
			                if($this->ciqrcode->generate($params))
			                {
			                    $data['qrcode']=$qr_image; 
			                }
			            }/*else{
			                $this->load->library('ciqrcode');
			                $qr_image=rand().'.png';
			                   $data['shortlink'] = "";   
			                $params['data'] = $this->input->post('offlinelink');
			                $params['level'] = 'H';
			                $params['size'] = 8;
			                $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
			                if($this->ciqrcode->generate($params))
			                {
			                    $data['qrcode']=$qr_image; 
			                }
			            }*/


			            if(!empty($_FILES['product_image']['name'][0])){

					        if(count($_FILES['product_image']['name']) > 3){
					        	$this->session->set_flashdata('flashmsg','Only 3 images can upload. please try again');
						        $this->session->set_flashdata('msg', 'danger');
						        redirect(base_url().'products/newProduct');

					        }
					        else{
				            	$files = $_FILES['product_image'];
					        	$images = array();
					        	$i = 0;
								$product_id = $this->Products_model->insert($data);

								// Short link for website details page for products - 04-07-2018
		                        $seller = $this->session->userdata('adminId'); // Remove encryption
		                        $product = $product_id; // Remove encryption

		                        $shorturl = base_url()."item/view/{$seller}/{$product}";

		                        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

		                        $ch = curl_init();
		                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		                        curl_setopt($ch,CURLOPT_URL,$postdata);
		                        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		                        $response = curl_exec($ch);
		                        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		                        $content = json_decode($response);
		                        curl_close($ch);

		                        if(isset($content->error)){
							      $path = __DIR__.'\Products.php function saveProduct';
							      $line = 90;
							      apilogger($content->error->message,$path,$line);
							    }
							    elseif(is_null($content) || !$content) {
							      $path = __DIR__.'\Products.php function saveProduct';
							      $line = 90;
							      apilogger("Error while generating short-link",$path,$line);
							    }

		                       /* if(isset($content->status) == 'error'){      
		                             $path = __DIR__.'\Products.php function saveProduct';
		                             $line = 256;
		                             apilogger($content->message,$path,$line);
		                        }*/

		                        if($content->result == true){
		                           $pdata['global_link'] = $content->url;     
		                        }

		                        $qr_image=rand().'.png';
		                        $params['data'] = $pdata['global_link'];//$this->input->post('finallink');
		                        $params['level'] = 'H';
		                        $params['size'] = 8;
		                        $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
		                        if($this->ciqrcode->generate($params))
		                        {
		                            $pdata['global_qrcode']=$qr_image; 
		                        }

		                        $this->Products_model->update($pdata, $product_id);

						      	foreach ($files['name'] as $key => $image) {  



							            $_FILES['product_image[]']['name']= $files['name'][$key];
							            $_FILES['product_image[]']['type']= $files['type'][$key];
							            $_FILES['product_image[]']['tmp_name']= $files['tmp_name'][$key];
							            $_FILES['product_image[]']['error']= $files['error'][$key];
							            $_FILES['product_image[]']['size']= $files['size'][$key]; 

							            $fileName = $image;
							           	$i++;
							           	$images[] = $fileName;  


							           	$config['upload_path'] = './uploads/products';
							           	$config['allowed_types'] = 'gif|jpg|png|jpeg';

							           	$config['max_size'] = 5120;

							           	$config['file_name'] = $fileName;
							           	$this->upload->initialize($config);

						            	if ($this->upload->do_upload('product_image[]')) {
											$fileData =	$this->upload->data();
							            	$img_data=[
							                		'product_id'	=> $product_id,
							                		'product_image' => $fileData['file_name']
							                	];   

						                	$this->Product_images_model->product_images($img_data);

							       		} else {

							                    $error = $this->upload->display_errors();
												$this->session->set_flashdata('flashmsg',$error);
							        			$this->session->set_flashdata('msg', 'danger');
							        			redirect(base_url().'products/newProduct');

						            	}        

						      	}

						      	// Catagory products save
						      	$maindata['product_id'] = $product_id;
                        		$maindata['category_id'] = $this->input->post('category_id');
								$this->Product_categories_model->insert($maindata);
						        $this->session->set_flashdata('flashmsg','New product has been created.');
						        $this->session->set_flashdata('msg', 'success');
						        redirect(base_url().'products/showProducts');
					        }
			            }
			            else if(empty($_FILES['product_image']['name'][0])){
			            	$product_id = $this->Products_model->insert($data);
		                    /*$img_data = [
		                        'product_id' => $product_id,
		                        'product_image' => null
		                    ];
		                    $this->Product_images_model->product_images($img_data);*/

		                    $maindata['product_id'] = $product_id;
		                    $maindata['category_id'] = $this->input->post('category_id');

		                    $this->Product_categories_model->insert($maindata);

		                    $this->session->set_flashdata('flashmsg', 'New product has been created.');
		                    $this->session->set_flashdata('msg', 'success');
		                    redirect(base_url() . 'products/showProducts');
			        	}
			            else{
			               $product_id = $this->Products_model->insert($data);

			               	// Short link for website details page for products - 04-07-2018
	                        $seller = $this->session->userdata('adminId'); // Remove encryption
	                        $product = $product_id; // Remove encryption

	                        $shorturl = base_url()."item/view/{$seller}/{$product}";

	                        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

	                        $ch = curl_init();
	                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	                        curl_setopt($ch,CURLOPT_URL,$postdata);
	                        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	                        $response = curl_exec($ch);
	                        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	                        $content = json_decode($response);
	                        curl_close($ch);

	                        if(isset($content->error)){
						      $path = __DIR__.'\Products.php function saveProduct';
						      $line = 90;
						      apilogger($content->error->message,$path,$line);
						    }
						    elseif(is_null($content) || !$content) {
						      $path = __DIR__.'\Products.php function saveProduct';
						      $line = 90;
						      apilogger("Error while generating short-link",$path,$line);
						    }

	                        /*if(isset($content->status) == 'error'){      
	                             $path = __DIR__.'\Products.php function saveProduct';
	                             $line = 440;
	                             apilogger($content->message,$path,$line);
	                        }*/

	                        if($content->result == true){
	                           $pdata['global_link'] = $content->url;     
	                        }

	                        $qr_image=rand().'.png';
	                        $params['data'] = $pdata['global_link'];//$this->input->post('finallink');
	                        $params['level'] = 'H';
	                        $params['size'] = 8;
	                        $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
	                        if($this->ciqrcode->generate($params))
	                        {
	                            $pdata['global_qrcode']=$qr_image; 
	                        }

		                    $this->Products_model->update($pdata, $product_id);

		                   $name = basename($this->input->post('instaimages'));
		                  // $imgcount = count($this->input->post('instaimages[]'));
		                   $path = './uploads/products/'.$name;
		                   $upload = $this->uploadImageByCurl($this->input->post('instaimages'),$path);
		                   	
		                   if($upload){
		                        $img_data = [
		                            'product_id' => $product_id,
		                            'product_image' => ($name) ? $name : null
		                        ];
		                        $this->Product_images_model->product_images($img_data);

		                        $maindata['product_id'] = $product_id;
		                    	$maindata['category_id'] = $this->input->post('category_id');

		                        $this->Product_categories_model->insert($maindata);
		                        
		                        $this->session->set_flashdata('flashmsg', 'New product has been created.');
		                        $this->session->set_flashdata('msg', 'success');
		                        redirect(base_url() . 'products/showProducts');
		                   }
		                   else{
		                        $this->session->set_flashdata('flashmsg', 'Unable to upload image');
		                        $this->session->set_flashdata('msg', 'danger');
		                        redirect(base_url() . 'products/newProduct'); 
		                   }
			            }

				    }
				    else{
				        $this->session->set_flashdata('flashmsg','Please enter a valid Product.');
				        $this->session->set_flashdata('msg', 'danger');
				        redirect(base_url().'products/newProduct');
				    }
		 }
		 else{
	     	$this->session->set_flashdata('flashmsg','Please enter a valid Product.');
	        $this->session->set_flashdata('msg', 'danger');
	        redirect(base_url().'products/newProduct');

		 }	

	}
	// Get all products with category 
	public function getProductsWithCategory($pageID=null){

		if(!empty($this->session->userdata('isLogin'))) {
            $products = $this->Products_model->where('created_by',$this->session->userdata('adminId'))->with_categories()->with_images('fields:product_image')->get_all();
        }
        else{

			if(is_null($this->pageID)){
	        	$products = $this->Products_model->with_categories()->with_images('fields:product_image')->get_all();
	        }
	        else{
	        	$products = $this->Products_model->where('facebook_page',$this->pageID)->with_categories()->with_images('fields:product_image')->get_all();  
	        }
        }

	   //$products = $this->Products_model->with_categories()->with_images('fields:product_image')->get_all();



	   return $products;



	}







	// Edit product



	public function editProduct($product_id=""){



		$product_details = $this->Products_model->with_categories()->with_images('fields:product_image,image_id')->get($product_id);

		$count_img = $this->Product_images_model->where('product_id' , $product_id)->count_rows();

		$product_details->count_img=$count_img;



		if(!empty($product_details)){



			$data['product_details'] = $product_details;



	    	$data['content'] = 'admin/products/editproduct';



	    	$data['title'] = 'Edit Product';

	    	$data['page'] = $this->pageID;

	    	$this->load->view('admin/template', $data);



	    }else{



	    	$this->session->set_flashdata('flashmsg', 'Invalid Produsct');



      		$this->session->set_flashdata('msg', 'danger');



      		redirect(base_url().'products/showProducts/');



	    }







	}

	// Update products by product id
	public function updateProduct($product_id=""){

		$pdetails = $this->Products_model->get($product_id);

		$maindata = array();

		$count_img = $_POST['count_img'];

		$this->load->library('ciqrcode');


		if(!empty($_FILES['product_image']['name'][0]) ){


		    $images = array();


	       	$files = $_FILES['product_image'];


	       	$count_img +=  count($_FILES['product_image']['name']);



	       	if($count_img > 3){

	       		$this->session->set_flashdata('flashmsg','Only 3 images can upload. please try again');

	        	$this->session->set_flashdata('msg', 'danger');

	        	redirect(base_url().'products/editProduct/'.$product_id);

	       	}
	       	else{

	        	$i = 0;

	      		foreach ($files['name'] as $key => $image) {   



		            $_FILES['product_image[]']['name']= $files['name'][$key];          



		            $_FILES['product_image[]']['type']= $files['type'][$key];            



		            $_FILES['product_image[]']['tmp_name']= $files['tmp_name'][$key];           



		            $_FILES['product_image[]']['error']= $files['error'][$key];            



		            $_FILES['product_image[]']['size']= $files['size'][$key];            



		            $fileName = $image; 



		           	$i++;            



		           	$images[] = $fileName;    


		           	$config['upload_path'] = './uploads/products';                



		           	$config['allowed_types'] = 'gif|jpg|png|jpeg';                



		           	$config['max_size'] = 5120;



		           	$config['file_name'] = $fileName;            



		           	$this->upload->initialize($config);            



	            	if ($this->upload->do_upload('product_image[]')) {    



						$fileData =	$this->upload->data();



		            	$img_data=[



		                	'product_id'	=> $product_id,



		                	'product_image' => $fileData['file_name']



		                ];   



		                $this->Product_images_model->insert($img_data);


			       	}else {         



		                $error = $this->upload->display_errors();



		                $this->session->set_flashdata('flashmsg', $error);



	            		$this->session->set_flashdata('msg', 'danger');



	            		redirect(base_url().'products/editProduct/'.$product_id);



		            }

	            }
	       	}
        }
        else if($this->input->post('instaimages')){
           $name = basename($this->input->post('instaimages'));
           $path = './uploads/products/'.$name;
  
           if($count_img <= 3) {

               $upload = $this->uploadImageByCurl($this->input->post('instaimages'),$path);

               if($upload){
                    $img_data = [
                        'product_id' => $product_id,
                        'product_image' => $name
                    ];
                    $this->Product_images_model->insert($img_data);
               }
               else{
                    $this->session->set_flashdata('flashmsg', 'Unable to upload image');
                    $this->session->set_flashdata('msg', 'danger');
                    redirect(base_url() . 'products/newProduct'); 
               }
           }
           else{
                $this->session->set_flashdata('flashmsg', 'Only 3 image can upload. please try again');
                $this->session->set_flashdata('msg', 'danger');
                redirect(base_url() . 'products/editProduct/' . $product_id);
           }
        }   


        $maindata['product_id'] = $product_id;
        $maindata['category_id'] = $this->input->post('category_id');

		$this->Product_categories_model->delete(array('product_id'=>$product_id)); 

		$this->Product_categories_model->insert($maindata); 

	    $productinfo = [

	     	//'product_sku' 			=> strip_tags($this->input->post('product_sku')),

	    	'product_name' 			=> strip_tags($this->input->post('product_name')),

	    	'product_name_thai' 	=> $this->input->post('product_name_thai'),
	    	'shipping_type' 		=> $this->input->post('shippingtype'),


	    	'product_price' 		=> strip_tags($this->input->post('product_price') ),



	    	'sale_price' 			=> strip_tags($this->input->post('sale_price')),



	    	'product_description'	=> $this->input->post('description'),

	    	'product_description_thai'	=> $this->input->post('product_description_thai'),

	    	'total_qty' 			=> $this->input->post('total_qty'),



	    	//'inventory' 			=> strip_tags($this->input->post('inventory')),



	    	'shipping_cost' 		=> strip_tags($this->input->post('shipping_cost'))


	    	//'facebook_page' => is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID




	    ];

	    if(!empty($this->session->userdata('isLogin'))){
            $data['facebook_page'] = $this->input->post('facebook_page');
        }
        else{
            $data['facebook_page'] = $this->input->post('facebook_page');
        }

	    /*if(is_null($this->pageID)){
        	$page = $this->input->post('facebook_page');
        }
        else{
        	$page = $this->pageID;
        }*/

	    // Shortlink QR code generates
        $productinfo['qrcode']="";
        if(!empty($this->input->post('finallink'))){
            $productinfo['shortlink'] = "http://m.me/{$data['facebook_page']}?ref=".$this->input->post('finallink');
            $this->load->library('ciqrcode');
            $qr_image=rand().'.png';
            $params['data'] = "http://m.me/{$data['facebook_page']}?ref=".$this->input->post('finallink');
            $params['level'] = 'H';
            $params['size'] = 8;
            $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
            if($this->ciqrcode->generate($params))
            {
                $productinfo['qrcode']=$qr_image; 
            }
        }/*else{
        	if($pdetails->shortlink!="" && $this->input->post('finallink')==""){
                 $productinfo['shortlink'] = $pdetails->shortlink;                 
                 $productinfo['qrcode'] = $pdetails->qrcode;
            }else{
                $productinfo['shortlink'] = ""; 
                unlink("./uploads/qr_image/" . $pdetails->qrcode);
                $productinfo['qrcode'] = "";
            }
        }*/

        // 05-07-2018 Shortlinks creation
        if(!empty($_FILES['product_image']['name'][0])){
	        if(is_null($pdetails->global_link)){
	            $seller = $this->session->userdata('adminId'); // Remove encryption
	            $product = $product_id; // Remove encryption

	            $shorturl = base_url()."item/view/{$seller}/{$product}";

	            $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

	            $ch = curl_init();
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	            curl_setopt($ch,CURLOPT_URL,$postdata);
	            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	            $response = curl_exec($ch);
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            $content = json_decode($response);
	            curl_close($ch);

	            if(isset($content->error)){
			      $path = __DIR__.'\Products.php function updateProduct';
			      $line = 636;
			      apilogger($content->error->message,$path,$line);
			    }
			    elseif(empty($content) || !$content) {
			      $path = __DIR__.'\Products.php function updateProduct';
			      $line = 636;
			      apilogger("Error while generating short-link",$path,$line);
			    }

	            if($content->result == true){
	               $productinfo['global_link'] = $content->url;     
	            }

	           /* if(isset($content->status) == 'error'){      
	                $path = __DIR__.'\Products.php function updateProduct';
	                $line = 898;
	                apilogger($content->message,$path,$line);
	            }*/

	            $qr_image=rand().'.png';
	            $params['data'] = $productinfo['global_link'];//$this->input->post('finallink');
	            $params['level'] = 'H';
	            $params['size'] = 8;
	            $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
	            if($this->ciqrcode->generate($params))
	            {
	                $productinfo['global_qrcode']=$qr_image; 
	            }
	        }
	    }


	    $updated = $this->Products_model->update($productinfo,$product_id);



	    if($updated){


	        $this->session->set_flashdata('flashmsg', 'Product has been updated.');



	        $this->session->set_flashdata('msg', 'success');



	        redirect(base_url().'products/showProducts/');

	    }else{
	        redirect(base_url().'products/showProducts/');
	    }



	}







	// Remove products by product id



	public function removeProduct($product_id=""){



		$img_details = $this->Product_images_model->where(array('product_id'=>$product_id))->get_all() ;



		foreach ($img_details as  $row) {



	    unlink("./uploads/products/".$row->product_image);



		}



		$this->Product_categories_model->delete(array('product_id'=>$product_id)); 



		$this->Products_model->delete(array('product_id'=>$product_id));



		$this->Product_images_model->delete(array('product_id'=>$product_id)); 



		$this->session->set_flashdata('flashmsg', 'Product has been removed.');



        $this->session->set_flashdata('msg', 'success');



		redirect(base_url().'products/showProducts/');	    



	} 







	// Show list of products in listing page



	public function showProducts(){	



	  //$productlists = $this->getProductsWithCategory($this->pageID);



	  //$data['products'] = $productlists;



      $data['content'] = 'admin/products/listproducts';



      $data['title'] = 'All Products';



      $this->load->view('admin/template', $data);



	}







	// Remove product image using ajax



	public function removeImage(){



		$image_id = $this->input->post('image_id');



		$img_details = $this->Product_images_model->where(array('image_id'=>$image_id))->get();



	   	unlink("./uploads/products/".$img_details->product_image);



		$this->Product_images_model->delete(array('image_id'=>$image_id)); 



		echo json_encode($image_id); die;



	}



	// Bulk upload form

	public function bulkUpload() {



        $productlists = $this->getProductsWithCategory();

        $data['products'] = $productlists;

        $data['content'] = 'admin/products/bulkupload';

        $data['title'] = 'Import Bulk Products';



        $this->load->view('admin/template', $data);

    }

    // Bulk image upload 12-09-2018
    public function bulkoperation() {

        $data['content'] = 'admin/products/bulkproducts';
        $data['title'] = 'Bulk Image Upload';

        $this->load->view('admin/template', $data);
    }

    // Import products bulk 

    public function import() {

    	// For vendor
        /*if(!empty($this->session->userdata('isLogin'))){
            $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
            if(!$pages){
              $this->session->set_flashdata('flashmsg', 'Store setup is required');
              $this->session->set_flashdata('msg', 'danger');
              redirect(base_url('products/bulkUpload'));
            }else{
              $facebook_page = $pages->page_id;  
            }
        }else if(is_null($this->pageID)){
            $facebook_page = "";    
        }
        else{
            $facebook_page = $this->pageID;
        }*/

        $file = $_FILES['importData']['tmp_name'];

        $file_upload = $_FILES['importData']['name'];

        $exp = explode(".", $file_upload);

        $filext = strtolower(end($exp));



        $extension = array('csv');



        if (!in_array($filext, $extension)) {

            $this->session->set_flashdata("flashmsg", "* Invalid file");

            $this->session->set_flashdata('msg', 'danger');

            redirect(base_url('products/bulkUpload'));

        }



        $this->load->library('Excel');



        $inputFileType = PHPExcel_IOFactory::identify($file);

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objPHPExcel = $objReader->load($file);

        $vendorData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);



        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();



        foreach ($cell_collection as $cell) {

            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();

            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();

            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            //header will/should be in row 1 only. of course this can be modified to suit your need.

            

            if ($row == 1) {

                $header[$row][$column] = $data_value;

            } else {



                $arr_data[$row][$column] = $data_value;

            }

        }



        $data['header'] = $header;

        $data['values'] = $arr_data;
        $dataarr = array();


        $insert_csv = array();
        $updata = array();
        $dataP =array();
        $error = 0;



        if ($data['header'][1]['A'] === "productnameEN" && $data['header'][1]['C'] === "price" && $data['header'][1]['D'] === "salePrice" && $data['header'][1]['E'] === "descriptionEn" && $data['header'][1]['G'] === "quantity" && $data['header'][1]['I'] === "categorynameEN" && $data['header'][1]['J'] === "shippingtype") {


	            foreach ($arr_data as $key => $row) {

	            	// 20-06-2018

	            	if(is_null($row['A'])){
	            		$error = 1;
	            		$dataarr[] = "Field is required for row {$key}: ".$data['header'][1]['A'];
	            	}
	            	if(is_null($row['C'])){
	            		$error = 1;
	            		$dataarr[] = "Field is required for row {$key}: ".$data['header'][1]['C'];
	            	}
	            	if(is_null($row['E'])){
	            		$error = 1;
	            		$dataarr[] = "Field is required for row {$key}: ".$data['header'][1]['E'];
	            	}
	            	if(is_null($row['G'])){
	            		$error = 1;
	            		$dataarr[] = "Field is required for row {$key}: ".$data['header'][1]['G'];
	            	}
	            	if(is_null($row['J'])){
	            		$error = 1;
	            		$dataarr[]= "Field is required for row {$key}: ".$data['header'][1]['J'];
	            	}
	            	if(is_null($row['I'])){
	            		$error = 1;
	            		$dataarr[]= "Field is required for row {$key}: ".$data['header'][1]['I'];
	            	}

	            	$prices = trim($row['C']);
	            	$salesprices = trim($row['D']);
	            	$shipcost = trim($row['H']);

	                if(!is_numeric($prices)) {
	            		$error = 1;
	            		$dataarr[]= "Price must be numeric for row {$key} : ".$data['header'][1]['C'];
	            	}
	            	else if(is_numeric($prices) && $prices == "0"){ //var_dump($price);
	            		$error = 1;
	            		$dataarr[]= "Price greater than 0 for row {$key} : ".$data['header'][1]['C'];
	            	}

	            	if(!empty($salesprices) && !is_numeric($salesprices)) {

	            		$error = 1;
	            		$dataarr[]= "Sale price must be numeric for row {$key} : ".$data['header'][1]['D'];
	            	}
	            	else if(!empty($salesprices) && is_numeric($salesprices) && $salesprices <= 0){
	            		$error = 1;
	            		$dataarr[]= "Sale price greater than 0 for row {$key} : ".$data['header'][1]['D'];
	            	}

	            	if(!empty($shipcost) && !is_numeric($shipcost)) {
	            		$error = 1;
	            		$dataarr[]= "Shipping cost must be numeric for row {$key} : ".$data['header'][1]['H'];
	            	}
	            	else if(!empty($shipcost) && is_numeric($shipcost) && $shipcost < 0){ //var_dump($price);
	            		$error = 1;
	            		$dataarr[]= "Shipping cost greater than 0 for row {$key} : ".$data['header'][1]['H'];
	            	}

	            	// where_product_name(trim($row['A']))
	            	$productcheck = $this->Products_model->where(array('product_name' => trim($row['A']),"created_by" => $this->session->userdata('adminId')))->get();

	            	// If products exist then update
	                if($productcheck){
	                	$updata[] = array(
	                        'product_id' => $productcheck->product_id,
	                        'product_name_thai' => trim($row['B']),
	                        'product_price' => trim($row['C']),
	                        'sale_price' => trim($row['D']),
	                        'product_description' => trim($row['E']),
	                        'product_description_thai' => trim($row['F']),
	                        'shipping_type' => trim(strtolower($row['J'])),
	                        'total_qty' => trim($row['G']),
	                        'shipping_cost' => trim($row['H'])
	                    );
	                    //$this->Products_model->update($updata,$productcheck->product_id); 
	                   if (isset($row['I'])) {
	                        if(!empty($row['I'])){
	                            $cat_details = $this->Categories_model->where(array('category_name' => trim($row['I']),"created_by" => $this->session->userdata('adminId')))->fields('category_id')->get();
	                            if($cat_details){
	                                $this->Product_categories_model->delete(array('product_id' => $productcheck->product_id));

	                                $cid = $cat_details->category_id;
	                                    $dataCat = array(
	                                        'product_id' => $productcheck->product_id,
	                                        'category_id' => $cid
	                                    );
	                                $this->Product_categories_model->insert($dataCat);
	                            }
	                            else{
	                                $error = 1;
	                                $dataarr[]= "Invalid category for row {$key} : ".$data['header'][1]['I'];
	                            }
	                        }
	                    }
	                }
	                else{
		            	$dataP[] = array(
		                    //'product_sku' => trim($row['K']),
		                    'product_name' => trim($row['A']),
		                    'product_name_thai' => trim($row['B']),
		                    'product_price' => trim($row['C']),
		                    'sale_price' => is_null(trim($row['D'])) ? "0" : trim($row['D']),
		                    'product_description' => trim($row['E']),
		                    'product_description_thai' => trim($row['F']),
		                    'shipping_type' => trim(strtolower($row['J'])),
		                    //'inventory' => trim($row['G']),
		                    'total_qty' => trim($row['G']),
		                    'shipping_cost' => is_null(trim($row['H'])) ? "0" : trim($row['H']),
		                    //'facebook_page' => trim($facebook_page),
		                    'created_by' => $this->session->userdata('adminId')
		            	);

		            	$category[] = trim($row['I']);

		            	foreach ($category as $catkey => $rows_cat) {  
	                        $cat_details = $this->Categories_model->where(array('category_name' => trim($rows_cat),"created_by" => $this->session->userdata('adminId')))->fields('category_id')->get();
	                        if(!$cat_details){
	                           $error = 1;
	                           $dataarr[]= "Invalid category for row {$key} : ".$data['header'][1]['I'];
	                        }
	                    }
	                    
	                    $pchar = substr($row['A'], 0, 3);
	                    $SKU[] = "AKS".$this->session->userdata('adminId')."P".$pchar."C".$row['I'];
	                }

	            }

	            //20-06-2018

	            if($error == 1){
	            	//$this->db->trans_rollback();
	            	$data['title'] = 'Import Bulk Products';
		            $data['content'] = 'admin/products/bulkupload';
		            $data['error'] = $dataarr;
		            $this->load->view('admin/template', $data);
	            }
	            else{
	            	if(count($updata) > 0){
                    	$this->db->update_batch('products',$updata, 'product_id'); 
                	}

                	if(count($dataP) > 0){
                		foreach($dataP as $key => $csm)
	                    {
	                       $dataP[$key]['product_sku'] = $SKU[$key]; // Auto generate SKU (product_id)
	                    }
		            	$count = count($dataP);
		            	$this->db->insert_batch('products', $dataP); 
		            	$this->db->select('product_id');
		            	$this->db->limit($count,0);
		            	$this->db->order_by('product_id','DESC');
		            	$product = $this->db->get('products')->result();

		            	/*foreach ($product as $key => $rows) {
			                $dataPI = array(
			                    'product_id' => $product[$key]->product_id,
			                    'product_image' => null
			                );
			                $this->Product_images_model->insert($dataPI);
		                }*/

		                foreach ($category as $catkey => $rows_cat) {  
			            	$cat_details = $this->Categories_model->where(array('category_name' => trim($rows_cat),"created_by" => $this->session->userdata('adminId')))->fields('category_id')->get();
			                if($cat_details){
			                    $cid = $cat_details->category_id;
			                        $dataCat = array(
			                            'product_id' => $product[$catkey]->product_id,
			                            'category_id' => $cid
			                        );
			                    $this->Product_categories_model->insert($dataCat);
			                }
		                }
                	}

	                $this->session->set_flashdata('flashmsg', 'File Imported Successfully');
	                $this->session->set_flashdata('msg', 'success');
	                /*if ($_FILES['uploadZip']['type'] == 'application/x-zip-compressed') {
		                $tmp_file = $_FILES['uploadZip']['tmp_name'];
		                $this->unzip->allow(array('png', 'gif', 'jpeg', 'jpg'));
		                $this->unzip->extract($tmp_file);
		                $this->unzip->extract($tmp_file, FCPATH . '/uploads/products/');
	            	}*/
	            	redirect(base_url() . 'products/bulkUpload');	
	            }
        }
        else 
        {

	        $this->session->set_flashdata("flashmsg", "Columns mismatch. Please download the sample file to make sure of column indexes.");

	        $this->session->set_flashdata('msg', 'danger');

	        redirect(base_url().'products/bulkUpload');

        }

	}



	// 31-05-2018 - Image upload by external URL
    public function uploadImageByCurl($url,$path){
        $ch = curl_init($url);
        $fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        return json_encode($result);
    }

    // Ajax orders list by filter

    public function ajax_list()
    {
        $list = $this->Products_model->get_datatables();
        
        $data = array();
        //$no = isset($_POST['start']) ? $_POST['start'] : 0;
        $no = 1;
        foreach ($list as $products) {
            //$no++;
            $categories = array();
            $pcategories = $this->Products_model->GetProductCategoryById($products->product_id);

            for ($k = 0; $k < count($pcategories); $k++) {
                if ($pcategories[$k]->product_id == $products->product_id) {
                    $categories[] = $pcategories[$k]->category_name;
                }
            }

            $products->categories = implode(",", $categories);

            //$reserved = reservedQty($products->product_id); 
            $reserved = $products->total_reserved;
            //echo($reserved);

            //$availqty = $products->total_qty - $reserved;
            $availqty = $products->available_qty;
            if($availqty < 0){
                $availqty = 0;
            }

            if (!empty($products->global_qrcode)){
            	$link = '<a href="'.base_url().'uploads/qr_image/'.$products->global_qrcode.'" class="bt btn-info btn-xs" download>Download QR code</a>';
            }
            else{
            	$link = "";
            }

            if(empty($products->product_image)){
                $img = "No Image";
            }
            else{
                
            	$img_url = base_url().'uploads/products/'.$products->product_image;
                $newurl = base_url()."Image_lib.php?src=".$img_url."&w=60&h=60&q=50";
                $img = '<img src="'.$newurl.'" width="60">';    
            }

            $row = array();
            $row[] = $no;
            $row[] = $img;
            $row[] = $products->product_name;
            $row[] = "<p style=color:#B86447;font-weight:600><a href=https://".$products->global_link." target='_blank'>".$products->global_link."</a></p>{$link}";
            $row[] = $products->product_price;
            $row[] = $products->categories;
            $row[] = $products->total_qty;
            $row[] = $availqty;
            $row[] = !empty($products->cart) ? $products->cart : 0;
            $row[] = $products->total_reserved;//$reserved;
            $row[] = '<a href="'.base_url("products/editProduct/{$products->product_id}").'" class="btn btn-xs btn-info">Edit</a><a href="'.base_url("products/removeProduct/{$products->product_id}").'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure want to remove product ?\')">Delete</a>';
 
            $data[] = $row;

            $no++;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Products_model->count_all(),
                        "recordsFiltered" => $this->Products_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    // Bulk products ajax listing
    public function bulk_ajax_list()
    {
        $list = $this->Product_images_model->get_datatables();
        
        $data = array();
        $no = 1;
        $i = 1;
        foreach ($list as $key => $products) {
            $i++;

            $reserved = reservedQty($products->product_id); 

            $reserved = $products->total_reserved;
            $availqty = $products->available_qty;
            if($availqty < 0){
                $availqty = 0;
            }

            $pimages = $this->Product_images_model->get_images_by_id($products->product_id);

            $imagecount = explode(",", $pimages[0]->images);

            $pimageid = explode(",", $pimages[0]->imageids);

            if (!empty($products->global_qrcode)){
            	$link = '<a href="'.base_url().'uploads/qr_image/'.$products->global_qrcode.'" class="bt btn-info btn-xs" download>Download QR code</a>';
            }
            else{
            	$link = "";
            }

            $row = array();
            $row[] = $no;
            $row[] = $products->product_name;
            $row[] = "<p style=color:#B86447;font-weight:600><a href=https://".$products->global_link." target='_blank'>".$products->global_link."</a></p>{$link}";
            if(isset($imagecount[0])){
                if(!empty($imagecount[0])){
                	$imagecount_url = base_url().'uploads/products/'.$imagecount[0];
                	$imagecount_newurl = base_url()."Image_lib.php?src=".$imagecount_url."&w=100&h=100&q=50";
                    $imgsrc = "<img src=".$imagecount_newurl." style='margin: 0 auto; display:block; text-align: center;'><input type='hidden' value=".$pimageid[0]." id=imgid-".$i."><a class='btn btn-xs btn-danger pull-right' id='removeimg' data-id=".$pimageid[0].">Remove</a>";
                }
                else{
                    $imgsrc = "";
                }
                
            }
            else{
                $imgsrc = "";
            }
            $row[] = "<input type='file' id=img-".$i." class='imgupload'>".$imgsrc."<input type='hidden' class='imageslect-".$key."' value=''><button class='btn btn-xs btn-success btnupload1' type='button' data-id='".$products->product_id."' id='create_post-".$products->product_id."' data-name=".$i." style='display:none;'>Upload</button><label class='text-danger' style='margin: 10px;'>File not bigger than 5MB</label>";
            $i++;
            if(isset($imagecount[1])){
                if(!empty($imagecount[1])){
                	$imagecount_url1 = base_url().'uploads/products/'.$imagecount[1];
                	$imagecount_newurl1 = base_url()."Image_lib.php?src=".$imagecount_url1."&w=100&h=100&q=50";
                    $imgsrc = "<img src=".$imagecount_newurl1." style='margin: 0 auto; display:block; text-align: center;><input type='hidden' value=".$pimageid[1]." id=imgid-".$i."><a class='btn btn-xs btn-danger pull-right' id='removeimg1' data-id=".$pimageid[1].">Remove</a>";
                }
                else{
                    $imgsrc = "";
                }
            }
            else{
                $imgsrc = "";
            }
            $row[] = "<input type='file' id=img-".$i." class='imgupload'>".$imgsrc."<input type='hidden' class='imageslect-".$key."' value=''>  <button class='btn btn-xs btn-success btnupload2' type='button' id='create_post-".$products->product_id."' data-id='".$products->product_id."' data-name=".$i." style='display:none;'>Upload</button><label class='text-danger' style='margin: 10px;'>File not bigger than 5MB</label>";
            $i++;
            if(isset($imagecount[2])){
                if(!empty($imagecount[2])){
                	$imagecount_url2 = base_url().'uploads/products/'.$imagecount[2];
                	$imagecount_newurl2 = base_url()."Image_lib.php?src=".$imagecount_url2."&w=100&h=100&q=50";
                     $imgsrc = "<img src=".$imagecount_newurl." style='margin: 0 auto; display:block; text-align: center;'><input type='hidden' value=".$pimageid[2]." id=imgid-".$i."><a class='btn btn-xs btn-danger pull-right' data-id=".$pimageid[2]." id='removeimg2'>Remove</a>";
                }
                else{
                    $imgsrc = "";
                }
               
            }
            else{
                $imgsrc = "";
            }
            $row[] = "<input type='file' id=img-".$i." class='imgupload'>".$imgsrc."<input type='hidden' class='imageslect-".$key."' value=''> <button class='btn btn-xs btn-success btnupload3' type='button' id='create_post-".$products->product_id."' data-id='".$products->product_id."' data-name=".$i." style='display:none;'>Upload</button><label class='text-danger' style='margin: 10px;'>File not bigger than 5MB</label>";
           
            $data[] = $row;

            $i++;

            $no++;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Product_images_model->count_all(),
                        "recordsFiltered" => $this->Product_images_model->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

     // Export Products in excel
    public function exportProducts(){
    	// IFNULL(sc.cart,0) + (total qty less of availabel qty)
    	// IFNULL(sc.cart,0) + (total reserved less of sc cart)
        $this->db->select('products.*,admin.admin_name,admin.admin_email,categories.category_name,product_images.product_image,sc.cart,products.total_qty,IFNULL(o.reserved,0),(total_qty - (IFNULL(o.reserved,0))) as available_qty,IFNULL(total_qty,0) as total_qty,(IFNULL(o.reserved,0)) as total_reserved');       
        $this->db->from('products');
        $this->db->join('product_images', 'product_images.product_id=products.product_id','LEFT');
        $this->db->join('categories_products', 'categories_products.product_id=products.product_id','LEFT');
        $this->db->join('(select `product_id`, sum(qty) as cart from `shopping_carts` group by `product_id` ) sc','products.product_id = sc.product_id','LEFT');
        $this->db->join('(select `product_id`, sum(ordered_qty) as reserved from `orders_products` LEFT JOIN orders ON orders.transaction_id=orders_products.order_id AND orders.status="0" group by `product_id` ) o', 'o.product_id=products.product_id','LEFT');
        $this->db->join('categories', 'categories.category_id=categories_products.category_id', 'LEFT');
        $this->db->join('admin', 'admin.admin_id=products.created_by', 'LEFT');
        if(!empty($this->session->userdata('isLogin'))){
            $this->db->where('products.created_by', $this->session->userdata('adminId'));
        }
        $this->db->group_by('products.product_id');

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $results = $query->result();

        require 'application/libraries/php-export-data.class.php';

        $exporter = new ExportDataExcel('browser', 'products.xls');
        $exporter->initialize(); // starts streaming data to web browser
        // pass addRow() an array and it converts it to Excel XML format and sends 
        // it to the browser
        $exporter->generateHeader();
        $headers = array("#", "Product Name", "Product Shortlink","Price","Category","Total Qty","Available Qty","Added to Cart","Reserved Qty","Created Date","Seller ID","Seller Name");
        /*if(empty($this->session->userdata('isLogin'))){
            $headers[] = 'Seller';
        }*/
        $exporter->addRow($headers);

        $count = 1;
      
        foreach ($results as $value) {

            $categories = array();

            $pcategories = $this->Products_model->GetProductCategoryById($value->product_id);

            for ($k = 0; $k < count($pcategories); $k++) {
                if ($pcategories[$k]->product_id == $value->product_id) {
                    $categories[] = $pcategories[$k]->category_name;
                }
            }

            $value->pcategories = implode(",", $categories);
            $reserved = reservedQty($value->product_id); 
            //echo($reserved);
            $reserved = $value->total_reserved;
            //$availqty = $products->total_qty - $reserved;
            $availqty = $value->available_qty;
            if($availqty < 0){
                $availqty = 0;
            }
            $pno = $count;
            $pname   = $value->product_name;
            $shortlink = $value->global_link;
            $price = $value->product_price;
            $category = $value->pcategories;
            $totalqty = $value->total_qty;
            $availqty = $availqty;
            $cartqty = !empty($value->cart) ? $value->cart : 0;
            $reservedqty = $reserved;
            $createddate = date('Y-m-d',strtotime($value->created_date));
            $createdby = $value->created_by;
            $sellername = $value->admin_name;

            $data = [$pno,$pname,$shortlink,$price,$category,$totalqty,$availqty,$cartqty,$reservedqty,$createddate,$createdby,$sellername];
            /*if(empty($this->session->userdata('isLogin'))){
                if(is_null($value->admin_name)){
                    $name = $value->admin_email;
                }
                else{
                    $name = $value->admin_name;
                }
                $data[] = $name;
            }*/
            $exporter->addRow($data);
            $count++;
        }
        $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        exit();
    }

     // Duplicate SKU check
    public function CheckduplicateSKU(){
       
        $Psku = $this->Products_model->HasSku(trim($_POST['SKU']),$_POST['PID']);
          
        if($Psku->num_rows() > 0){
            echo json_encode(array("status" => FALSE,'message' => '* Product SKU already exists.'));
        }
        else{
            echo json_encode(array("status" => TRUE));
        }
        exit;
    }

    // Save bulkimage 
    public function Saveimage(){
        //echo '<pre>';
        $this->load->library('ciqrcode');
        $productID = $_POST['productid'];
        $imgname = $_POST['filename'];
        $imageid = isset($_POST['imageID']) ? $_POST['imageID'] : 0;

        list($type, $data) = explode(';', $_POST['file']);
        list(, $data) = explode(',', $data);
        $file_data = base64_decode($data);

        // Get file mime type
        $finfo = finfo_open();
        $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

        // File extension from mime type
        if($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg')
            $file_type = 'jpeg';
        else if($file_mime_type == 'image/png')
            $file_type = 'png';
        else if($file_mime_type == 'image/gif')
            $file_type = 'gif';
        else 
            $file_type = 'other';
      
        // Validate type of file
        if(in_array($file_type, [ 'jpeg', 'png', 'gif' ])) {
            // Set a unique name to the file and save
            $file_name = uniqid() . '.' . $file_type;
            $path = FCPATH."uploads/products/".$file_name;

            $data =array(
                "product_id" => $productID,
                "product_image" => $file_name
            );

            $checkrecord = $this->db->select('product_image,image_id')->from('product_images')->where('image_id',$imageid)->get();

            if($checkrecord->num_rows() > 0 ) {
                $data = $checkrecord->result();
                unlink("./uploads/products/" . $data[0]->product_image);
                $this->db->where('image_id', $imageid);
                $this->db->update('product_images', array('product_image' => $file_name));

            } else {
            	$productinfo = array();
                $pdetails = $this->Products_model->get($productID);
                // Genertae shortlink and save in DB
                if(is_null($pdetails->global_link)){
		            $seller = $this->session->userdata('adminId'); // Remove encryption
		            $product = $productID; // Remove encryption

		            $shorturl = base_url()."item/view/{$seller}/{$product}";

		            $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

		            $ch = curl_init();
		            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		            curl_setopt($ch,CURLOPT_URL,$postdata);
		            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		            $response = curl_exec($ch);
		            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		            $content = json_decode($response);
		            curl_close($ch);

		            if(isset($content->error)){
				      $path = __DIR__.'\Products.php function Saveimage';
				      $line = 1561;
				      apilogger($content->error->message,$path,$line);
				    }
				    elseif(empty($content) || !$content) {
				      $path = __DIR__.'\Products.php function Saveimage';
				      $line = 1561;
				      apilogger("Error while generating short-link",$path,$line);
				    }

		            if($content->result == true){
		               $productinfo['global_link'] = $content->url;     
		            }

		            $qr_image=rand().'.png';
		            $params['data'] = $productinfo['global_link'];//$this->input->post('finallink');
		            $params['level'] = 'H';
		            $params['size'] = 8;
		            $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
		            if($this->ciqrcode->generate($params))
		            {
		                $productinfo['global_qrcode']=$qr_image; 
		            }
		            $this->Products_model->update($productinfo, $productID);
		        }
                // END
                $savedata = $this->db->insert("product_images",$data);
            }

            file_put_contents($path, $file_data,FILE_APPEND);
            echo json_encode(array("msg" => true));
        }
        else {
            echo json_encode(array("msg" => false,"data" => "Error : Only JPEG, PNG & GIF allowed"));
        }
        exit;
    }

    // Delete image from folder and databse by ajax
    public function DeleteImage(){
        $imageID = $_POST['image'];

        $checkrecord = $this->db->select('product_image,image_id')->from('product_images')->where('image_id',$imageID)->get(); // Check record       

        if($checkrecord->num_rows() > 0){ // Found
            $data = $checkrecord->result();

            unlink("./uploads/products/" . $data[0]->product_image);

            $this->db->where('image_id', $imageID);
            $this->db->delete('product_images');
            echo json_encode(array("msg" => true));
        }
        exit;
    }

}
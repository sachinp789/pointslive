<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends MY_Controller {
	public $pageID="";
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');		
		$this->pageID = $this->session->userdata('pagemanager');
		$this->load->model('Logs_model');
	}

	/************* View coupon form *************/
	public function List(){	
    
		$data['content'] = 'admin/logs/listlogs';
	    $data['title'] = 'Error Logs';
	    $data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);
	}

	/************ Ajax Listing ****************/

	// Ajax orders list by filter
	public function ajax_list()
    {
        $list = $this->Logs_model->get_datatables();
        $data = array();
        //$no = isset($_POST['start']) ? $_POST['start'] : 0;
        foreach ($list as $logs) {
            //$no++;       	

            $row = array();
            $row[] = $logs->id;
            /*$row[] = $logs->errno;*/
            $row[] = $logs->errtype;
            $row[] = $logs->errstr;
            $row[] = $logs->errfile;
            $row[] = $logs->errline;
            $row[] = $logs->user_agent;
            $row[] = $logs->ip_address;
            $row[] = $logs->time;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Logs_model->count_all(),
                        "recordsFiltered" => $this->Logs_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    function export_excel(){       

        $result = $this->db->query("SELECT id as ID,errtype as ErrorType,errstr as Error,errfile as Filepath,errline as LineNo, user_agent as Useragent,ip_address as IPaddress, time as Date_Time FROM logs WHERE time BETWEEN NOW() - INTERVAL 30 DAY AND NOW() order by id desc")->result_array();
        
        require 'application/libraries/php-export-data.class.php';

        $exporter = new ExportDataExcel('browser', 'errorlog.xls');

		$exporter->initialize(); // starts streaming data to web browser

		// pass addRow() an array and it converts it to Excel XML format and sends 
		// it to the browser
		$exporter->addRow(array("ID", "ErrorType", "Error", "Filepath","LineNo","Useragent","IPaddress","Date_Time")); 
		foreach ($result as $rows) {
			# code...
			$id = $rows['ID'];
			$errorType = $rows['ErrorType'];
			$error = $rows['Error'];
			$filepath = $rows['Filepath'];
			$lineno = $rows['LineNo'];
			$useragent = $rows['Useragent'];
			$ipaddress = $rows['IPaddress'];
			$date_time = $rows['Date_Time'];
			$data = [$id,$errorType,$error,$filepath,$lineno,$useragent,$ipaddress,$date_time];
			$exporter->addRow($data);	
			$data = '';
		}
		
		$exporter->finalize(); // writes the footer, flushes remaining data to browser.

		exit(); // all done

    }

    function filterData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

	
}
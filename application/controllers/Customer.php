<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {


	public function __construct(){
		require_once APPPATH.'libraries/twitter/twitteroauth.php';
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->model('Customers_Addresses_model');
		$this->load->model('Shopping_carts_model');
		$this->load->model('Coupons_model');
		$this->load->library('facebook'); 

	}
	
	// Dashboard Of Customer		
	public function login(){
		$data['content'] = 'users/social_login';
	    $data['title'] = 'Login';
	    $this->load->view('global/template', $data);
	}

	// Process login redirection
	public function signIn($type){

		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();
		
		switch ($type) {
			
			case 'twitter':
				if($this->config->item('consumer_key') === '' || $this->config->item('consumer_secret') === '')
				{	
		            redirect(base_url());
				}
				else{
					$connection = new TwitterOAuth($this->config->item('consumer_key'),$this->config->item('consumer_secret'));// Key and Sec

					$request_token = $connection->getRequestToken($this->config->item('oauth_callbackcustomer'));// Retrieve Temporary credentials. 
					
					$this->session->set_userdata('oauth_token',$request_token['oauth_token']);
                    $this->session->set_userdata('oauth_token_secret',$request_token['oauth_token_secret']);
                    $this->session->set_userdata('prev_url',$prevurl);

                   	switch ($connection->http_code) {
					  case 200: 
					  	$url = $connection->getAuthorizeURL($request_token['oauth_token']); // Redirect to authorize page.
						redirect($url);
						break;
					  default:
					  	$this->session->set_flashdata('flashmsg', 'Could not connect to Twitter. Refresh the page or try again later.');
		                $this->session->set_flashdata('msg', 'error');
		                redirect(base_url());
					}
				}
			break;

			case 'facebook':
				$data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('customer/Requestcallback'), 
                'scope' => array("email") // permissions here
            	));
            	$this->session->set_userdata('prev_url',$prevurl);
           		redirect($data['login_url']);	
			break;

			default:
				break;
		}	
	}

	// Twitter
	public function callback(){
		//$this->load->library('user_agent');
		//$prevurl = $this->agent->referrer();
		$connection = $this->twitter_callback();

		$access_token = $this->session->userdata('access_token');

		$prevurl = $this->session->userdata('prev_url');
		
		if(200 == $connection->http_code){
			//$this->session->set_userdata('status','verified');
			$twitterconnected = new TwitterOAuth($this->config->item('consumer_key'), $this->config->item('consumer_secret'), $access_token['oauth_token'], $access_token['oauth_token_secret']);

			$params = array('include_email' => 'true', 'include_entities' => 'false', 'skip_status' => 'true');

			/* If method is set change API call made. Test is called by default. */
			$content = $twitterconnected->get('account/verify_credentials',$params);
			if($content){
				$replaceImage = str_replace("normal", "bigger", $content->profile_image_url);
				$senddata = array(	
                    'admin_name'			=> $content->name,
                    'admin_email'			=> $content->email,
                    'login_with'			=> 'customer',
                    'login_uid'				=> $content->id_str,
                    'login_picture'			=> $replaceImage,
                    'login_username'		=> $content->screen_name,
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	

				$checkLogin = $this->Admin_model->get(array('login_uid' => $content->id_str));

				// Check guest data in cart
            	$user =  $this->session->userdata("guest");
				$cartdata = productcarts_by_userid($user);
				
				if($checkLogin && $checkLogin->login_with == 'customer'){

					$this->session->set_userdata('username',$checkLogin->admin_name);
		            $this->session->set_userdata('customerId',$checkLogin->admin_id);

		            if(empty($checkLogin->customer_referral_code)){
						$custchar  = substr($content->email, 0, 3); // Unique Code 
			        	$uniqCode = $custchar.$checkLogin->admin_id."AI";
            			$this->Admin_model->update(array("customer_referral_code" => $uniqCode),$checkLogin->admin_id); // Save unique code in profile
            		}
		            
		            if(count($cartdata) > 0){ // Update cart data
		            	$this->Shopping_carts_model->updatecustomer();
		            	if($updated){
            				$this->session->unset_userdata('guest');
            			}
		            }
		            else{
		            	$this->session->unset_userdata('guest');
		            }

		            // Track user logged in details
                	$this->db->insert("user_logs",array("user_id" => $checkLogin->admin_id,"login_date"=>date("Y-m-d")));
                	// END

		            if(strpos($prevurl, base_url('item/view')) !== false) {
	            		redirect($prevurl);
	            		$this->session->unset_userdata('prev_url');
		            }
		            else if(strpos($prevurl, base_url('item/checkout')) !== false){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
					}
					else{
						$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');	
					}

		          /*  if($prevurl != base_url()){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
		            }
		            else{
		            	$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');
		            }*/
				}
				else if($checkLogin && $checkLogin->login_with != 'customer'){
					$this->session->set_flashdata('flashmsg', 'Sorry, You are not allowed to logs in.');
            		$this->session->set_flashdata('msg', 'error');
				    redirect(base_url());
				}
				else{
					$inserted = $this->Admin_model->insert($senddata);

					$custchar  = substr($content->email, 0, 3); // Unique Code 

	        		$uniqCode = $custchar.$inserted."AI"; 

					$this->Admin_model->update(array("customer_referral_code" => $uniqCode),$inserted);

					 // Track user logged in details
                	$this->db->insert("user_logs",array("user_id" => $inserted,"login_date"=>date("Y-m-d")));
                	// END

					//$checkLogin = $this->Admin_model->get($inserted);

					$this->session->set_userdata('username',$content->name);
		            $this->session->set_userdata('customerId',$inserted);

		            if(count($cartdata) > 0){ // Update cart data
		            	$this->Shopping_carts_model->updatecustomer();
		            	if($updated){
            				$this->session->unset_userdata('guest');
            			}
		            }
		            else{
		            	$this->session->unset_userdata('guest');
		            }

		            if(strpos($prevurl, base_url('item/view')) !== false) {
	            		redirect($prevurl);
	            		$this->session->unset_userdata('prev_url');
		            }
		            else if(strpos($prevurl, base_url('item/checkout')) !== false){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
					}
					else{
						$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');	
					}

		           /* if($prevurl != base_url()){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
		            }
		            else{
		            	$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');
		            }*/
		            //redirect(base_url().'customer/profile');
				}
			}
		}
		else{
			redirect(base_url());
		}
	}

	// Twitter callback
	public function twitter_callback(){ 
		$connection = new TwitterOAuth($this->config->item('consumer_key'), $this->config->item('consumer_secret'), $this->session->userdata('oauth_token'), $this->session->userdata('oauth_token_secret'));
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);	
		
		$this->session->set_userdata('access_token', $access_token);
		$this->session->unset_userdata('oauth_token');
		$this->session->unset_userdata('oauth_token_secret');
		return $connection;
	}

	// Facebook Callback
	public function Requestcallback(){
		//$this->load->library('user_agent');
		//$prevurl = $this->agent->referrer();
		$prevurl = $this->session->userdata('prev_url');
		if (isset($_GET['code']) && !empty($_GET['code'])) {

			$code = $_GET['code'];
			$validatetoken = $this->get_fb_contents("https://graph.facebook.com/v3.0/oauth/access_token?client_id=".$this->config->item('appId')."&redirect_uri=".base_url('customer/Requestcallback')."&client_secret=".$this->config->item('secret')."&code=".$code);

			$finaltoken = json_decode($validatetoken);

			if(!empty($finaltoken->access_token)) {
	      		// getting all user info using access token.
				$fbuser_info = $this->get_fb_contents("https://graph.facebook.com/v3.0/me?fields=id,name,email,first_name,last_name&access_token=".$finaltoken->access_token);
				$facebookdata = json_decode($fbuser_info);

				$pimage = "https://graph.facebook.com/{$facebookdata->id}/picture?type=large";	
				$facebookdata->profile_image = $pimage;

				$senddata = array(	
                    'admin_name'			=> $facebookdata->name,
                    'admin_email'			=> $facebookdata->email,
                    'login_with'			=> 'customer',
                    'login_uid'				=> $facebookdata->id,
                    'login_picture'			=> $facebookdata->profile_image,
                    'login_username'		=> $facebookdata->name,
                    'login_access_token'	=> $finaltoken->access_token,	
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	
				
				$checkLogin = $this->Admin_model->get(array('login_uid' => $facebookdata->id));
				
				// Check guest data in cart
            	$user =  $this->session->userdata("guest");
				$cartdata = productcarts_by_userid($user);

				if($checkLogin && $checkLogin->login_with == 'customer'){

		        	if(empty($checkLogin->customer_referral_code)){
						$custchar  = substr($checkLogin->admin_email, 0, 3); // Unique Code 
			        	$uniqCode = $custchar.$checkLogin->admin_id."AI";
            			$this->Admin_model->update(array("customer_referral_code" => $uniqCode),$checkLogin->admin_id); // Save unique code in profile
            		}

					$this->session->set_userdata('username',$checkLogin->admin_name);
	            	$this->session->set_userdata('customerId',$checkLogin->admin_id);

	            	if(count($cartdata) > 0){ // Update cart data
	            		$updated = $this->Shopping_carts_model->updatecustomer();
	            		if($updated){
            				$this->session->unset_userdata('guest');
            			}
	            	}
	            	else{
	            		$this->session->unset_userdata('guest');
	            	}

	            	// Track user logged in details
                	$this->db->insert("user_logs",array("user_id" => $checkLogin->admin_id,"login_date"=>date("Y-m-d")));
                	// END

	            	if(strpos($prevurl, base_url('item/view')) !== false) {
	            		redirect($prevurl);
	            		$this->session->unset_userdata('prev_url');
		            }
		            else if(strpos($prevurl, base_url('item/checkout')) !== false){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
					}
					else{
						$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');	
					}

					/*if($prevurl != base_url()){
	            		redirect($prevurl);
	            		$this->session->unset_userdata('prev_url');
		            }
		            else{
		            	$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');
		            }*/
				}
				else if($checkLogin && $checkLogin->login_with != 'customer'){
					$this->session->set_flashdata('flashmsg', 'Sorry, You are not allowed to logs in.');
            		$this->session->set_flashdata('msg', 'error');
				    redirect(base_url());
				}
				else{
					$inserted = $this->Admin_model->insert($senddata);

					$custchar  = substr($facebookdata->email, 0, 3); // Unique Code 

	        		$uniqCode = $custchar.$inserted."AI"; 

					$this->Admin_model->update(array("customer_referral_code" => $uniqCode),$inserted);

					// Track user logged in details
                	$this->db->insert("user_logs",array("user_id" => $inserted,"login_date"=>date("Y-m-d")));
                	// END
				
					//$checkLogin = $this->Admin_model->get($inserted);

					$this->session->set_userdata('username',$facebookdata->name);
		            $this->session->set_userdata('customerId',$inserted);

					if(count($cartdata) > 0){ // Update cart data
		            	$updated = $this->Shopping_carts_model->updatecustomer();
		            	if($updated){
	            			$this->session->unset_userdata('guest');
	            		}
		            }
		            else{
		            	$this->session->unset_userdata('guest');
		            }

		            if(strpos($prevurl, base_url('item/view')) !== false) {
	            		redirect($prevurl);
	            		$this->session->unset_userdata('prev_url');
		            }
		            else if(strpos($prevurl, base_url('item/checkout')) !== false){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
					}
					else{
						$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');	
					}

		           /* if($prevurl != base_url()){
		            	redirect($prevurl);
		            	$this->session->unset_userdata('prev_url');
		            }
		            else{
		            	$this->session->unset_userdata('prev_url');
		            	redirect(base_url().'customer/profile');
		            }*/

		           // redirect(base_url().'customer/profile');
				}	
			}
		}
		else{
            redirect(base_url());
		}
	}

	function get_fb_contents($url) {
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		$response = curl_exec( $curl );
		curl_close( $curl );
		$content = json_decode($response);
		if(isset($content->status) == 'error'){      
			$path = __DIR__.'\Customer.php function get_fb_contents';
			$line = 258;
			apilogger($content->message,$path,$line);
		}

		return $response;
	}

	// Signout customer
	public function signOut(){
		if($this->session->userdata('customerId')){
			$this->session->unset_userdata('customerId');
			$this->session->unset_userdata('username');
		}
		redirect(base_url());
	}

	// Signin customer
	public function signUpwithLogin(){
		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();
		if($this->input->post()){

			$this->form_validation->set_rules('useremail','Email','trim|xss_clean|required');
        	$this->form_validation->set_rules('userpwd','Password','trim|xss_clean|required|callback_validatePassword'); // Check password format

        	if($this->form_validation->run() == true) {

        		$senddata = array(	
                    'admin_email'=> $this->input->post('useremail'),
                    'admin_password' => $this->_hash($this->input->post('userpwd',true))
                );

                // Check guest data in cart
            	$user =  $this->session->userdata("guest");
				$cartdata = productcarts_by_userid($user);

                if($this->isEmailExists($this->input->post('useremail'))){

                	$checkLogin = $this->Admin_model->get($senddata);

                	if($checkLogin){

                		$custchar  = substr($checkLogin->admin_email, 0, 3); // Unique Code 
	        			$uniqCode = $custchar.$checkLogin->admin_id."AI"; 

                		if($checkLogin->login_with == 'customer' && $checkLogin->is_active == '1'){

                			if(empty($checkLogin->customer_referral_code)){
	                			$this->Admin_model->update(array("customer_referral_code" => $uniqCode),$checkLogin->admin_id); // Save unique code in profile
	                		}

		                	$this->session->set_userdata('customerId',$checkLogin->admin_id);

		                	if(count($cartdata) > 0){ // Update cart data
			            		$updated = $this->Shopping_carts_model->updatecustomer();
			            		if($updated){
			            			$this->session->unset_userdata('guest');
			            		}
			            	}
				            else{
				            	$this->session->unset_userdata('guest');
				            }

				            if(strpos($prevurl, base_url('item/view')) !== false) {
			            		$url = $prevurl;
				            }
				            else if(strpos($prevurl, base_url('item/checkout')) !== false){
				            	$url = $prevurl;
							}
							else{
								$url = base_url('customer/profile');	
							}

							// Track user logged in details
		                	$this->db->insert("user_logs",array("user_id" => $checkLogin->admin_id,"login_date"=>date("Y-m-d")));
		                	// END

				            echo json_encode(array("status" => TRUE,"msg" => $url));
                		}
                		else{
                			echo json_encode(array("status" => FALSE,"msg" => '* Sorry, You are not allowed to logs in.'));
                		}
                	}
                	else{
	                	echo json_encode(array("status" => FALSE,"msg" => '* Invalid email address or password.'));
                	}
            	}
            	else{
            		$logindata = array(
	        			"admin_email" 		=> $this->input->post('useremail'),
	        			"login_with"		=> 'customer',	
	        			"admin_password" 	=> $this->_hash($this->input->post('userpwd',true))
	        		);
	        		$inserted = $this->Admin_model->insert($logindata);

	        		$custchar  = substr($this->input->post('useremail'), 0, 3); // Unique Code 

	        		$uniqCode = $custchar.$inserted."AI"; 

	        		if($inserted){
	        			
	        			$this->Admin_model->update(array("customer_referral_code" => $uniqCode),$inserted); // Save unique code in profile

	        			// Track user logged in details
	                	$this->db->insert("user_logs",array("user_id" => $inserted,"login_date"=>date("Y-m-d")));
	                	// END

	        			//$this->session->unset_userdata('guest');
			            $this->session->set_userdata('customerId',$inserted);

			            if(count($cartdata) > 0){ // Update cart data
			            	$updated = $this->Shopping_carts_model->updatecustomer();

			            	if($updated){
			            		$this->session->unset_userdata('guest');
			            	}
			            }
			            else{
			            	$this->session->unset_userdata('guest');
			            }

			            if(strpos($prevurl, base_url('item/view')) !== false) {
		            		$url = $prevurl;
			            }
			            else if(strpos($prevurl, base_url('item/checkout')) !== false){
			            	$url = $prevurl;
						}
						else{
							$url = base_url('customer/profile');	
						}

			            echo json_encode(array("status" => TRUE,"msg" => $url));
	        		}
	        		else{
	        			echo json_encode(array("status" => FALSE,"msg" =>'Error while signup / in.'));
	        		}
            	}
        	}
        	else{
        		echo validation_errors();
        	}
		}
		exit;
		/*else{
			$data['content'] = 'users/signuplogin';
		    $data['title'] = 'Signup/Login';
		    $this->load->view('users/template', $data);		
		}*/
	}

	// Password validation
	public function validatePassword($pass){
        // permitted characters throughout string ------------------------↓↓↓↓↓↓↓↓
        $checkpassword =  preg_match('/^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^\d]*\d)[a-zA-Z\d]{8,}$/',$pass);

        if($checkpassword){
        	return true;
        }
        else{
        	$this->form_validation->set_message('validatePassword', 'The Password field must be 8 letters and contains 1 uppercase,1 lowercase and 1 number.');
        	return false;
        }
        // required characters----------↑↑↑-------------↑↑↑-----------↑↑            ↑-minimum length (no max)
    }
  
    // Check email exists
    public function isEmailExists($key){
    	$checkemail = $this->Admin_model->email_exists($key);
    	if($checkemail){
    		return true;
    	}
    	else{
    		return false;
    	}

    }

     // Update customer profile
    public function profile()
    {	
    	$customerId = $this->session->userdata('customerId');
    	$profiledata = $this->Admin_model->get(['admin_id' => $customerId]);
    	$address = $this->Customers_Addresses_model->where("customer_id",$this->session->userdata('customerId'))->order_by("address_id","asc")->get_all();
    	$cartdata = productcarts_by_userid($this->session->userdata('customerId'));
    	$banklists = getbanks();
    	$banksinfo = $this->Customers_Addresses_model->fetch_bankdata($customerId);

    	if($this->input->post()){

    		if(!empty($_FILES['profile_photo']['name'])){

    			$config['upload_path'] = './uploads/customers';
		        $config['allowed_types'] = 'gif|jpg|png|jpeg';
		        $config['max_size'] = 5120;
		        $new_name = time().'@'.$_FILES["profile_photo"]['name'];
				$config['file_name'] = $new_name;

		        $this->upload->initialize($config);
	           	if($this->upload->do_upload('profile_photo')){
	           		$fileData = $this->upload->data();
	                @unlink("uploads/customers/".$profiledata->admin_photo);
	                $profileinfo['admin_photo'] = $fileData['file_name'];
	           	}
	           	else{
		            $this->session->set_flashdata('flashmsg', $this->upload->display_errors());
		            $this->session->set_flashdata('msg', 'danger');
		            redirect(base_url().'customer/profile');
	           	}
    		}

    		if($profiledata){
    			$profileinfo['admin_name'] = trim($this->input->post('name'));
	            $profileinfo['admin_mobile'] = trim($this->input->post('contact_number'));
	            $profileinfo['admin_email'] = trim($this->input->post('email'));
	          	$profileinfo['admin_state'] = trim($this->input->post('local_state_user'));
	            $updated = $this->Admin_model->update($profileinfo,$customerId);
	            if($updated){
	               // $this->session->set_flashdata('flashmsg', 'Profile has been updated.');
	               // $this->session->set_flashdata('msg', 'success');
	                redirect(base_url().'customer/profile');
	            }	
    		}
    	}

    	$data['content'] = 'global/my_account';
	    $data['title'] = 'My Account';
	    $data['profiledata'] = $profiledata;
	    $data['addressdata'] = $address;
	    $data['carts'] = $cartdata;
	    $data['banks'] = $banklists;
	    $data['banksinfo'] = $banksinfo;
	    $this->load->view('global/template', $data);
    }

    // Save customer address
    public function saveaddress()
    {	
    	if($this->input->post()) {

    		$addressData = array(
    			"customer_id" => $this->session->userdata('customerId'),
    			"country" => 'nigeria',//$this->input->post('local_country'),
    			"state" => str_replace("_", " ", $this->input->post('local_state')) ,
    			"district" => str_replace("_"," ",$this->input->post('local_government')),
    			"address" => $this->input->post('custaddress'),
    			"make_default" => $this->input->post('makedefault') ? $this->input->post('makedefault') : "0"
    		);

    		if(empty($this->input->post('address_ids'))){
				$this->Customers_Addresses_model->insert($addressData);
    		}
    		else{
    			$uaddressData = array(
	    			"country" => 'nigeria',//$this->input->post('local_country'),
	    			"state" => str_replace("_"," ",$this->input->post('local_state')),
	    			"district" => str_replace("_"," ",$this->input->post('local_government')),
	    			"address" => $this->input->post('custaddress'),
	    			"make_default" => $this->input->post('makedefault') ? $this->input->post('makedefault') : "0"
    			);

    			$lastaddress = $this->Customers_Addresses_model->getaddress_by_user($this->session->userdata('customerId'),$this->input->post('address_ids'));

    			if(count($lastaddress) > 0){
    				if($this->input->post('makedefault') == '1'){
    					$this->Customers_Addresses_model->update(array("make_default"=>"0"),$lastaddress->address_id);
    				}
    			}

    			$this->Customers_Addresses_model->update($uaddressData,$this->input->post('address_ids'));
    		}
	    	redirect(base_url().'customer/profile');
    	}
    }

    // Check coupon with user
    public function checkCoupon(){
    	$vcode = $this->input->post('voucher');
    	$user = $this->input->post('userID');
    	$grandtotal = $this->input->post('amount');
    	//echo $user;
    	$flagvalid = 0;
    	 // check code valid or not and also check status
        $validcode = checkValidCoupon($vcode,$user);
        if($validcode){ 

            $isvaliddate = checkDateBetween(date('Y-m-d'),$validcode->from_date,$validcode->to_date); // check expiry date
           // var_dump($isvaliddate);
	      	if($isvaliddate){
	            // check user uses of coupon count  
	            $usedcount = $this->Coupons_model->usedcountCoupon_By_User($validcode->id,$user);
	            if(count($usedcount) >= 0){
	               // if user uses more than 0 or entered limit
	               if($usedcount->total_used == $validcode->per_customer_uses){
	                  $flagvalid = 0;
	               }
	               else{
		                // check cart amount with mimnimum amount of coupon
		                if($grandtotal >= $validcode->minimum_amount){
		                        if($validcode->by_apply == '%'){
		                          $discountamount = ($grandtotal * $validcode->discount_value) / 100;
		                          $finalamount = $grandtotal - $discountamount;
		                        }
		                        else if($validcode->by_apply == "fixed"){
		                          $discountamount = $validcode->discount_value;
		                          $finalamount = $grandtotal - $discountamount;
		                        }

		                        $redeemdata = array(
		                          'coupon_id' => $validcode->id,
		                          'total_discount'  => $discountamount,
		                          'redemption_date' => date('Y-m-d'),
		                          'user_id'   => $user
		                        );

		                        // Saved redeem coupon data
		                        $redeemsaved = $this->db->insert('coupon_redemptions',$redeemdata);

		                        if($redeemsaved){
		                          $lastID = $this->db->insert_id();
		                          $flagvalid = 1;
		                        }
		                }
		                else{
		            		$flagvalid = 0;
		           		}
	               }
	          	}
	          	else{
	            	$flagvalid = 0;
	        	}
	        }
	        else{
	          $flagvalid = 0;
	        }
	    }
	    else{
	    	$flagvalid = 0;
	    }
        // Valid or not check
        if($flagvalid == 1){
        	echo json_encode(array("status" => TRUE,"cdata" => $checkvalid,"discount" => $discountamount,"finaltotal" => $finalamount));
        }
        else{
        	echo json_encode(array("status" => FALSE));
        }	
    	exit;	
	}

	// Remove voucher code
	public function removeCoupon(){
		$voucherID = $this->input->post('coupon');

		$this->db->where('id', $voucherID);
    	$query = $this->db->delete("coupon_redemptions");

    	if($query){
    		echo json_encode(array("status" => TRUE));
    	}
    	else{
    		echo json_encode(array("status" => FALSE));
    	}
    	exit;	
	}

	// Save customer bank details
	public function saveBankDetails(){
		//$bankdata = array();
		if($this->input->post()){

			$bankdata = array(
				"bank_code"		=> $this->input->post('bankname'),
				"account_number"=> $this->input->post('account_number')	
			);

			$exists = $this->Customers_Addresses_model->check_record($this->session->userdata('customerId')); // Check records exists or not

			if($exists){
				$this->Customers_Addresses_model->save_bankdata($this->session->userdata('customerId'),$bankdata); // Update records with bank data
			}
			else{
				$bankdata['customer_id'] = $this->session->userdata('customerId');
				$this->Customers_Addresses_model->save_bankdata("",$bankdata); // Save bank info
			}
		}
		redirect(base_url("customer/profile"));

	}

	// Citizen listing view
	public function citizens(){
		$data['content'] = 'admin/citizen';
        $data['title'] = 'Citizens';
        $this->load->view('admin/template', $data);
	}

	// Citizon list with ajax based
	public function ajax_citizenlist(){
		$list = $this->Customers_Addresses_model->get_datatables();
        $data = array();
        $no = 1;
        foreach ($list as $customer) {
          
            $row = array();
            $row[] = $customer->admin_id;
            $row[] = $customer->admin_name;
            $row[] = $customer->admin_state;
            $row[] = date('Y-m-d',strtotime($customer->created_date));
            $row[] = number_format($customer->spendamount,2);
            $row[] = is_null($customer->last_order_made) ? "-" : date('Y-m-d',strtotime($customer->last_order_made));
            $row[] = is_null($customer->last_loggedin) ? "-" : $customer->last_loggedin;
            $row[] = is_null($customer->daylogins) ? 0 : $customer->daylogins;
            $row[] = is_null($customer->used_code) ? 0 : $customer->used_code;
 
            $data[] = $row;

            $no++;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Customers_Addresses_model->count_all(),
                        "recordsFiltered" => $this->Customers_Addresses_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	// Export Data of Citizens
	public function exportCitizens(){

		$this->db->select('admin.admin_id,admin.admin_name,admin.admin_state,admin.created_date,ulog.last_loggedin,ord.last_order_made,ord.spendamount,ulog.daylogins,mcode.used_code');
        $this->db->from('admin');
        $this->db->join('(SELECT MAX(created_date) AS last_order_made,user_id,(COUNT(id) / order_amount) as spendamount FROM orders GROUP BY user_id ORDER BY last_order_made DESC ) ord','ord.user_id = admin.admin_id', 'left');
        $this->db->join('(SELECT DATE(user_logs.login_date) last_loggedin,user_logs.user_id, COUNT(user_logs.user_id) daylogins
			FROM user_logs LEFT JOIN admin ON admin.admin_id = user_logs.user_id WHERE 
			user_logs.login_date BETWEEN DATE_FORMAT(admin.created_date,"%Y-%m-%d") AND DATE(NOW()) GROUP BY user_logs.login_date,user_logs.user_id ORDER BY user_logs.user_id) ulog','ulog.user_id = admin.admin_id','left');
         $this->db->join('(SELECT COUNT(match_codes.used_by) as used_code,match_codes.user_id FROM match_codes LEFT JOIN admin ON admin.admin_id = match_codes.user_id GROUP BY match_codes.user_id) mcode','mcode.user_id = admin.admin_id','left');
        $this->db->where('admin.login_with','customer');
        $this->db->group_by('admin.admin_id');
       
        $query = $this->db->get();
   
        $results = $query->result();

        require 'application/libraries/php-export-data.class.php';

        $exporter = new ExportDataExcel('browser', 'citizens.xls');
        $exporter->initialize(); // starts streaming data to web browser
        // pass addRow() an array and it converts it to Excel XML format and sends 
        // it to the browser
        $exporter->generateHeader();
        $headers = array("Customer ID", "Customer Name","Customer State","Memeber Since","Average Spend","Last Order Made","Last Login","Consecutive Logins","Sellers Onboarded ");
        
        $exporter->addRow($headers);

        foreach ($results as $value) {

            $cid = $value->admin_id;
            $cname   = $value->admin_name;
            $cstate = empty($value->admin_state) ? '-' : $value->admin_state;
            $cmember = date("Y-m-d",strtotime($value->created_date));
            $spendamt = empty($value->spendamount) ? 0.00 : number_format($value->spendamount,2);
            $lastorder = empty($value->last_order_made) ? '-' : date("Y-m-d",strtotime($value->last_order_made));
            $lastlogin = empty($value->last_loggedin) ? '-' : $value->last_loggedin;
            $consecutives = empty($value->daylogins) ? 0 : $value->daylogins;
            $code = empty($value->used_code) ? 0 : $value->used_code;

            $data = [$cid,$cname,$cstate,$cmember,$spendamt,$lastorder,$lastlogin,$consecutives,$code];
            $exporter->addRow($data);
            $count++;
        }
        $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        exit();
	}

	// Verification of flutterwave user account
	public function verify_account($bankdata=null){
		$accountdata = array();
		$accountdata['recipientaccount'] = $bankdata->account_number;
		$accountdata['destbankcode'] = $bankdata->bank_code;
		$accountdata['PBFPubKey'] = $this->config->item('public_key');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.ravepay.co/flwv3-pug/getpaidx/api/resolve_account");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($accountdata)); //Post Fields
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
		  'Content-Type: application/json',
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$request = curl_exec($ch);
		curl_close ($ch);
	
		$result = json_decode($request, true);
		return $result;
	}

	// Inititate flutterwave bulk transfer payments
	public function initialize_bulktransfer(){
		$accountdata = array();
		$accountdata['seckey'] = $this->config->item('public_secret');
		$accountdata['title'] = 'Bulk Transfer Payments';

		$ordersdata = $this->Orders_model->fetch_orders();

		if($ordersdata){
			foreach ($ordersdata as $key => $order) {

				$timediff = strtotime(date("Y-m-d H:i:s")) - strtotime($order->created_date);// Check 24 hours passed
				
				if($timediff > 86400){ // Ok
					$custinfo = $this->Customers_Addresses_model->fetch_bankdata($order->user_id);
					if($custinfo){
						
						$checkAccount = $this->verify_account($custinfo); // verify account number and code
			
						if($checkAccount && $checkAccount['data']['data']['responsecode'] == "00"){
							$oids[] = $order->id;
							$bulkdata[] = [
						        "Bank"=>$custinfo->bank_code,
						        "Account Number"=> $custinfo->account_number,
						        "Amount"=>$order->order_amount,
						        "Currency"=>"NGN",
						        "Narration"=>"Bulk transfer of order {$order->id}",
						        "reference"=> uniqid()
							];
						}
						else{
							$path = __DIR__.'\Customer.php function initialize_bulktransfer';
				          	$line = 892;
				          	apilogger($checkAccount['message'],$path,$line);
						}
					}
					else{
						$path = __DIR__.'\Customer.php function initialize_bulktransfer';
			          	$line = 892;
			          	apilogger('User not found in database',$path,$line);
					}
				}
			}
		}
		// Inititate bank transfer payments
		if(count($bulkdata) > 0){
			$accountdata['bulk_data'] = $bulkdata;	
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://api.ravepay.co/v2/gpx/transfers/create_bulk");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($accountdata)); //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$headers = [
			  'Content-Type: application/json',
			];
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$request = curl_exec($ch);
			curl_close ($ch);

			$result = json_decode($request, true);

			if($result['status'] == 'success'){
				$checkBulkStatus = $this->retrieve_bulktransfer_status($result['data']['id']); // get status of bulk transfer payments

				if($checkBulkStatus){
					if($checkBulkStatus['status'] == 'success'){
						foreach ($checkBulkStatus['data']['transfers'] as $key => $value) {
							if($value['status'] == 'FAILED' || $value['status'] == 'NEW'){
								
								if($value['status'] == 'FAILED'){
									$message = $value->complete_message;
								}
								else if($value['status'] == 'NEW'){
									$message = "Failed to refund {$value->narration}";
								}

								$path = __DIR__.'\Customer.php function initialize_bulktransfer';
					          	$line = 892;
					          	apilogger($message,$path,$line);
							}
							else{
								// Update refund status to `1`-Paid
								foreach ($oids as $key => $order) {
									if (strpos($value->narration, $order) !== false) {
										$this->Orders_model->update(array('refund_status' => '1'),$order);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// Check bulk transfer payment status
	public function retrieve_bulktransfer_status($batch_id=null){
		$accountdata['seckey'] = $this->config->item('public_secret');
		$accountdata['batch_id'] = $batch_id;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_URL,"https://api.ravepay.co/v2/gpx/transfers?seckey={$accountdata['seckey']}&batch_id={$accountdata['batch_id']}");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
		  'Content-Type: application/json',
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$request = curl_exec($ch);
		curl_close ($ch);

		$result = json_decode($request, true);

		return $result;
	}

	// Track whataspp users by lick on phone icon
	public function trackusers(){
		if(!empty($this->session->userdata("guest"))){ 
        	$guestsession = $this->session->userdata("guest");
      	}
      	else{
        	$guestsession = $this->SetGuestSession();
      	}
      	$user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $guestsession;

		$seller = $this->input->post('sellerID');

		if($this->input->post()){
			
			$checkUser = $this->db->select('id,user_id,click_event')->from('trackwp_users')->where('user_id',$user)->where('seller_id',$seller)->get()->row();
			//var_dump($checkUser);exit;
			if(!$checkUser){
				$trackdata = array(
					"user_id" 	=> $user ,
					"seller_id" => $seller,
					"type" 		=> !empty($this->session->userdata("customerId")) ? 'customer' : 'guest',
					'track_date'=> date("Y-m-d"),
					'click_event' => 1
 				);

 				$saved = $this->db->insert("trackwp_users",$trackdata);
 				if($saved){
 					echo json_encode(array("status" => TRUE));
 				}
			}
			else{
				$counts = $checkUser->click_event + 1;
				$this->db->set('click_event',$counts);
				$this->db->where('id',$checkUser->id);
				$this->db->update('trackwp_users');
				echo json_encode(array("status" => TRUE));
			}
			exit;
		}
	}

}
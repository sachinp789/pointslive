<?php



defined('BASEPATH') OR exit('No direct script access allowed');







class Products extends MY_Controller {



	public $pageID="";



	public function __construct(){



		parent::__construct();



	    $this->load->library('form_validation');



	    $this->load->model('Products_model');



	    $this->load->model('Product_images_model');



	    $this->load->model('Product_categories_model');	    

	    $this->load->model('Categories_model');

	    $this->load->library('unzip');

        $this->load->library('upload');


        $this->pageID = $this->session->userdata('pagemanager');


	}



	// Add new category



	public function newProduct(){



	    $data['content'] = 'admin/products/addproduct';



	    $data['title'] = 'Add New Product';

	    $data['page'] = $this->pageID;

	    $this->load->view('admin/template', $data);



	} 



	



	// Save products



	public function saveProduct(){

		 $maindata = array();

	     if($this->input->post())
	     {

			     	$this->form_validation->set_rules('product_name','Product Name','trim|xss_clean|required');



			     	$this->form_validation->set_rules('product_sku', 'SKU', 'trim|xss_clean|required|is_unique[products.product_sku]');



				    $this->form_validation->set_rules('product_price','Product Price','trim|xss_clean|required');



				    $this->form_validation->set_rules('sale_price','Sale Price','trim|xss_clean');



				    $this->form_validation->set_rules('description','Description','trim|xss_clean|required');



				   //$this->form_validation->set_rules('inventory','Inventory','trim|xss_clean|required');



				    $this->form_validation->set_rules('shipping_cost','Shipping Cost','trim|xss_clean');



				    $this->form_validation->set_rules('facebook_page','facebook_page','trim|xss_clean|required');



				    if($this->form_validation->run() == true) {


				        $data = [

				          'product_sku' 		=> $this->input->post('product_sku'),	

				          'product_name' 		=> $this->input->post('product_name'),

				          'product_name_thai' 		=> $this->input->post('product_name_thai'),

				          'shipping_type' 		=> $this->input->post('shippingtype'),
			              //'shortlink'			=> $this->input->post('finallink'),

				          'product_price' 		=> $this->input->post('product_price'),



				          'sale_price'			=> $this->input->post('sale_price'),



				          'product_description'	=> $this->input->post('description'),

				          'product_description_thai'	=> $this->input->post('product_description_thai'),

				          'total_qty' 			=> $this->input->post('total_qty'),

				         // 'inventory' 			=> $this->input->post('inventory'),

				          'shipping_cost'		=> $this->input->post('shipping_cost'),

				          'created_by' => $this->session->userdata('adminId')

				          //'facebook_page' => is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID       



				        ];

				        if(!empty($this->session->userdata('isLogin'))){
		                    $data['facebook_page'] = $this->input->post('facebook_page');
		                }
		                else{
		                    $data['facebook_page'] = is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID;
		                }

				       /* if(is_null($this->pageID)){
				        	$page = $this->input->post('facebook_page');
				        }
				        else{
				        	$page = $this->pageID;
				        }*/

				        // Shortlink QR code generates
			            $data['qrcode']="";
			            if(!empty($this->input->post('finallink'))){
			            	$data['shortlink'] = "http://m.me/{$page}?ref=".$this->input->post('finallink');
			                $this->load->library('ciqrcode');
			                $qr_image=rand().'.png';
			                $params['data'] = "http://m.me/{$page}?ref=".$this->input->post('finallink');
			                $params['level'] = 'H';
			                $params['size'] = 8;
			                $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
			                if($this->ciqrcode->generate($params))
			                {
			                    $data['qrcode']=$qr_image; 
			                }
			            }else{
			                $this->load->library('ciqrcode');
			                $qr_image=rand().'.png';
			                $params['data'] = $this->input->post('offlinelink');
			                $params['level'] = 'H';
			                $params['size'] = 8;
			                $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
			                if($this->ciqrcode->generate($params))
			                {
			                    $data['qrcode']=$qr_image; 
			                }
			            }


			            if(!empty($_FILES['product_image']['name'])){

					        if(count($_FILES['product_image']['name']) > 3){

					        	$this->session->set_flashdata('flashmsg','Only 1 images can upload. please try again');

						        $this->session->set_flashdata('msg', 'danger');

						        redirect(base_url().'products/newProduct');

					        }
					        else{
				            	$files = $_FILES['product_image'];
					        	$images = array();
					        	$i = 0;
								$product_id = $this->Products_model->insert($data);


						      	foreach ($files['name'] as $key => $image) {   



							            $_FILES['product_image[]']['name']= $files['name'][$key];          



							            $_FILES['product_image[]']['type']= $files['type'][$key];            



							            $_FILES['product_image[]']['tmp_name']= $files['tmp_name'][$key];           



							            $_FILES['product_image[]']['error']= $files['error'][$key];            



							            $_FILES['product_image[]']['size']= $files['size'][$key];            



							            $fileName = $image; 



							           	$i++;            



							           	$images[] = $fileName;    







							           	$config['upload_path'] = './uploads/products';                



							           	$config['allowed_types'] = 'gif|jpg|png|jpeg';                



							           	$config['max_size'] = 5120;



							           	$config['file_name'] = $fileName;            



							           	$this->upload->initialize($config);            



						            	if ($this->upload->do_upload('product_image[]')) {    



											$fileData =	$this->upload->data();

							            	$img_data=[



							                		'product_id'	=> $product_id,



							                		'product_image' => $fileData['file_name']



							                	];   

						                	$this->Product_images_model->product_images($img_data);

							       		} else {         



							                    $error = $this->upload->display_errors();



												$this->session->set_flashdata('flashmsg',$error);



							        			$this->session->set_flashdata('msg', 'danger');



							        			redirect(base_url().'products/newProduct');

						            	}        

						      	}
								$categories = $this->input->post('category_id');			

								foreach($categories as $key => $value) {

									$maindata[$key]['product_id'] = $product_id;	


									$maindata[$key]['category_id'] = $value;

								}

								$this->Product_categories_model->insert($maindata);

						        $this->session->set_flashdata('flashmsg','New product has been created.');

						        $this->session->set_flashdata('msg', 'success');

						        redirect(base_url().'products/showProducts');
					        }
			            }
			            else{
			               $product_id = $this->Products_model->insert($data);
		                   $name = basename($this->input->post('instaimages'));
		                   $path = './uploads/products/'.$name;
		                   $upload = $this->uploadImageByCurl($this->input->post('instaimages'),$path);
		                   
		                   if($upload){
		                        $img_data = [
		                            'product_id' => $product_id,
		                            'product_image' => $name
		                        ];
		                        $this->Product_images_model->product_images($img_data);
		                        $categories = $this->input->post('category_id');
		                        foreach ($categories as $key => $value) {
		                            $maindata[$key]['product_id'] = $product_id;
		                            $maindata[$key]['category_id'] = $value;
		                        }
		                        $this->Product_categories_model->insert($maindata);
		                        $this->session->set_flashdata('flashmsg', 'New product has been created.');
		                        $this->session->set_flashdata('msg', 'success');
		                        redirect(base_url() . 'products/showProducts');
		                   }
		                   else{
		                        $this->session->set_flashdata('flashmsg', 'Unable to upload image');
		                        $this->session->set_flashdata('msg', 'danger');
		                        redirect(base_url() . 'products/newProduct'); 
		                   }
			            }

				    }
				    else{



					        $this->session->set_flashdata('flashmsg','Please enter a valid Product.');



					        $this->session->set_flashdata('msg', 'danger');



					        redirect(base_url().'products/newProduct');



				    }

		 }
		 else{

		     	$this->session->set_flashdata('flashmsg','Please enter a valid Product.');



		        $this->session->set_flashdata('msg', 'danger');



		        redirect(base_url().'products/newProduct');

		 }	

	}







	// Get all products with category 



	public function getProductsWithCategory($pageID=null){

		if(!empty($this->session->userdata('isLogin'))) {
            $products = $this->Products_model->where('created_by',$this->session->userdata('adminId'))->with_categories()->with_images('fields:product_image')->get_all();
        }
        else{

			if(is_null($this->pageID)){
	        	$products = $this->Products_model->with_categories()->with_images('fields:product_image')->get_all();
	        }
	        else{
	        	$products = $this->Products_model->where('facebook_page',$this->pageID)->with_categories()->with_images('fields:product_image')->get_all();  
	        }
        }

	   //$products = $this->Products_model->with_categories()->with_images('fields:product_image')->get_all();



	   return $products;



	}







	// Edit product



	public function editProduct($product_id=""){



		$product_details = $this->Products_model->with_categories()->with_images('fields:product_image,image_id')->get($product_id);

		$count_img = $this->Product_images_model->where('product_id' , $product_id)->count_rows();

		$product_details->count_img=$count_img;



		if(!empty($product_details)){



			$data['product_details'] = $product_details;



	    	$data['content'] = 'admin/products/editproduct';



	    	$data['title'] = 'Edit Product';

	    	$data['page'] = $this->pageID;

	    	$this->load->view('admin/template', $data);



	    }else{



	    	$this->session->set_flashdata('flashmsg', 'Invalid Produsct');



      		$this->session->set_flashdata('msg', 'danger');



      		redirect(base_url().'products/showProducts/');



	    }







	}

	// Update products by product id
	public function updateProduct($product_id=""){

		$pdetails = $this->Products_model->get($product_id);

		$maindata = array();

		$count_img = $_POST['count_img'];


		if(!empty($_FILES['product_image']['name'][0]) ){


		    $images = array();


	       	$files = $_FILES['product_image'];


	       	//$count_img +=  count($_FILES['product_image']['name']);



	       	if($count_img > 3){

	       		$this->session->set_flashdata('flashmsg','Only 1 images can upload. please try again');

	        	$this->session->set_flashdata('msg', 'danger');

	        	redirect(base_url().'products/editProduct/'.$product_id);

	       	}
	       	else{

	        	$i = 0;

	      		foreach ($files['name'] as $key => $image) {   



		            $_FILES['product_image[]']['name']= $files['name'][$key];          



		            $_FILES['product_image[]']['type']= $files['type'][$key];            



		            $_FILES['product_image[]']['tmp_name']= $files['tmp_name'][$key];           



		            $_FILES['product_image[]']['error']= $files['error'][$key];            



		            $_FILES['product_image[]']['size']= $files['size'][$key];            



		            $fileName = $image; 



		           	$i++;            



		           	$images[] = $fileName;    


		           	$config['upload_path'] = './uploads/products';                



		           	$config['allowed_types'] = 'gif|jpg|png|jpeg';                



		           	$config['max_size'] = 5120;



		           	$config['file_name'] = $fileName;            



		           	$this->upload->initialize($config);            



	            	if ($this->upload->do_upload('product_image[]')) {    



						$fileData =	$this->upload->data();



		            	$img_data=[



		                	'product_id'	=> $product_id,



		                	'product_image' => $fileData['file_name']



		                ];   



		                $this->Product_images_model->insert($img_data);


			       	}else {         



		                $error = $this->upload->display_errors();



		                $this->session->set_flashdata('flashmsg', $error);



	            		$this->session->set_flashdata('msg', 'danger');



	            		redirect(base_url().'products/editProduct/'.$product_id);



		            }

	            }
	       	}
        }
        else if($this->input->post('instaimages')){
           $name = basename($this->input->post('instaimages'));
           $path = './uploads/products/'.$name;
  
           if($count_img < 3) {

               $upload = $this->uploadImageByCurl($this->input->post('instaimages'),$path);

               if($upload){
                    $img_data = [
                        'product_id' => $product_id,
                        'product_image' => $name
                    ];
                    $this->Product_images_model->insert($img_data);
               }
               else{
                    $this->session->set_flashdata('flashmsg', 'Unable to upload image');
                    $this->session->set_flashdata('msg', 'danger');
                    redirect(base_url() . 'products/newProduct'); 
               }
           }
           else{
                $this->session->set_flashdata('flashmsg', 'Only 1 image can upload. please try again');
                $this->session->set_flashdata('msg', 'danger');
                redirect(base_url() . 'products/editProduct/' . $product_id);
           }
        }   


	    $categories = $this->input->post('category_id');			

		foreach($categories as $key => $value) {


			$maindata[$key]['product_id'] = $product_id;	


			$maindata[$key]['category_id'] = $value;

		}


		$this->Product_categories_model->delete(array('product_id'=>$product_id)); 

		$this->Product_categories_model->insert($maindata); 

	    $productinfo = [

	     	'product_sku' 			=> strip_tags($this->input->post('product_sku')),

	    	'product_name' 			=> strip_tags($this->input->post('product_name')),

	    	'product_name_thai' 	=> $this->input->post('product_name_thai'),
	    	'shipping_type' 		=> $this->input->post('shippingtype'),


	    	'product_price' 		=> strip_tags($this->input->post('product_price') ),



	    	'sale_price' 			=> strip_tags($this->input->post('sale_price')),



	    	'product_description'	=> $this->input->post('description'),

	    	'product_description_thai'	=> $this->input->post('product_description_thai'),

	    	'total_qty' 			=> $this->input->post('total_qty'),



	    	//'inventory' 			=> strip_tags($this->input->post('inventory')),



	    	'shipping_cost' 		=> strip_tags($this->input->post('shipping_cost'))


	    	//'facebook_page' => is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID




	    ];

	    if(!empty($this->session->userdata('isLogin'))){
            $data['facebook_page'] = $this->input->post('facebook_page');
        }
        else{
            $data['facebook_page'] = is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID;
        }

	    /*if(is_null($this->pageID)){
        	$page = $this->input->post('facebook_page');
        }
        else{
        	$page = $this->pageID;
        }*/

	    // Shortlink QR code generates
        $productinfo['qrcode']="";
        if(!empty($this->input->post('finallink'))){
            $productinfo['shortlink'] = "http://m.me/{$page}?ref=".$this->input->post('finallink');
            $this->load->library('ciqrcode');
            $qr_image=rand().'.png';
            $params['data'] = "http://m.me/{$page}?ref=".$this->input->post('finallink');
            $params['level'] = 'H';
            $params['size'] = 8;
            $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
            if($this->ciqrcode->generate($params))
            {
                $productinfo['qrcode']=$qr_image; 
            }
        }else{
        	if($productinfo['shipping_type'] != "offline"){
               $productinfo['shortlink'] = ""; 
               $productinfo['qrcode'] = ""; 
               unlink("./uploads/qr_image/" . $pdetails->qrcode);
            }
            else{
               $productinfo['qrcode'] = $pdetails->qrcode;
            }
            //$productinfo['qrcode'] = $pdetails->qrcode;
        }


	    $updated = $this->Products_model->update($productinfo,$product_id);



	    if($updated){


	        $this->session->set_flashdata('flashmsg', 'Product has been updated.');



	        $this->session->set_flashdata('msg', 'success');



	        redirect(base_url().'products/showProducts/');

	    }else{
	        redirect(base_url().'products/showProducts/');
	    }



	}







	// Remove products by product id



	public function removeProduct($product_id=""){



		$img_details = $this->Product_images_model->where(array('product_id'=>$product_id))->get_all() ;



		foreach ($img_details as  $row) {



	    unlink("./uploads/products/".$row->product_image);



		}



		$this->Product_categories_model->delete(array('product_id'=>$product_id)); 



		$this->Products_model->delete(array('product_id'=>$product_id));



		$this->Product_images_model->delete(array('product_id'=>$product_id)); 



		$this->session->set_flashdata('flashmsg', 'Product has been removed.');



        $this->session->set_flashdata('msg', 'success');



		redirect(base_url().'products/showProducts/');	    



	} 







	// Show list of products in listing page



	public function showProducts(){	



	  $productlists = $this->getProductsWithCategory($this->pageID);



	  $data['products'] = $productlists;



      $data['content'] = 'admin/products/listproducts';



      $data['title'] = 'All Products';



      $this->load->view('admin/template', $data);



	}







	// Remove product image using ajax



	public function removeImage(){



		$image_id = $this->input->post('image_id');



		$img_details = $this->Product_images_model->where(array('image_id'=>$image_id))->get();



	   	unlink("./uploads/products/".$img_details->product_image);



		$this->Product_images_model->delete(array('image_id'=>$image_id)); 



		echo json_encode($image_id); die;



	}



	// Bulk upload form

	public function bulkUpload() {



        $productlists = $this->getProductsWithCategory();

        $data['products'] = $productlists;

        $data['content'] = 'admin/products/bulkupload';

        $data['title'] = 'Import Bulk Products';



        $this->load->view('admin/template', $data);

    }



    // Import products bulk 

    public function import() {

        $file = $_FILES['importData']['tmp_name'];

        $file_upload = $_FILES['importData']['name'];

        $exp = explode(".", $file_upload);

        $filext = strtolower(end($exp));



        $extension = array('csv');



        if (!in_array($filext, $extension)) {

            $this->session->set_flashdata("flashmsg", "* Invalid file");

            $this->session->set_flashdata('msg', 'danger');

            redirect(base_url('products/bulkUpload'));

        }



        $this->load->library('Excel');



        $inputFileType = PHPExcel_IOFactory::identify($file);

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objPHPExcel = $objReader->load($file);

        $vendorData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);



        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();



        foreach ($cell_collection as $cell) {

            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();

            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();

            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            //header will/should be in row 1 only. of course this can be modified to suit your need.

            

            if ($row == 1) {

                $header[$row][$column] = $data_value;

            } else {



                $arr_data[$row][$column] = $data_value;

            }

        }



        $data['header'] = $header;

        $data['values'] = $arr_data;



        $insert_csv = array();



        if ($data['header'][1]['A'] === "productnameEN" && $data['header'][1]['B'] === "productnameTHAI" && $data['header'][1]['C'] === "price" && $data['header'][1]['D'] === "salePrice" && $data['header'][1]['E'] === "descriptionEn" && $data['header'][1]['F'] === "descriptionTHAI" && $data['header'][1]['G'] === "quantity" &&

                $data['header'][1]['H'] === "shippingCost" && $data['header'][1]['I'] === "facebookpage" && $data['header'][1]['J'] === "productImg" && $data['header'][1]['K'] === "categorynameEN" && $data['header'][1]['L'] === "productsku" && $data['header'][1]['M'] === "shippingtype") {



            foreach ($arr_data as $row) {



                $dataP = array(

                	'product_sku' => trim($row['L']),

                    'product_name' => trim($row['A']),

                    'product_name_thai' => trim($row['B']),

                    'product_price' => trim($row['C']),

                    'sale_price' => trim($row['D']),

                    'product_description' => trim($row['E']),

                    'product_description_thai' => trim($row['F']),
                    'shipping_type' => trim($row['M']),

                    //'inventory' => trim($row['G']),

                    'total_qty' => trim($row['G']),

                    'shipping_cost' => trim($row['H']),

                    'facebook_page' => trim(str_replace(",", "", number_format($row['I'])))

                );


                $this->data['products'] = $this->Products_model->insert($dataP);

                $pid = $this->data['products'];



                $images = explode(',', trim($row['J']));

                $count = 1;

                foreach ($images as $rows) {

                    if ($count <= 3) {

                        $dataPI = array(

                            'product_id' => $pid,

                            'product_image' => trim($rows)

                        );

                        $this->Product_images_model->insert($dataPI);

                    }

                    $count++;

                }

                if (isset($row['K'])) {

                    

                    if(!empty($row['K'])){

                    $category  = explode(',', trim($row['K']));

                        foreach ($category as $rows_cat) {



                        $cat_details = $this->Categories_model->where(array('category_name' => trim($rows_cat)))->fields('category_id')->get();

                            if($cat_details){

                                $cid = $cat_details->category_id;

                                    $dataCat = array(

                                        'product_id' => $pid,

                                        'category_id' => $cid

                                    );

                                $this->Product_categories_model->insert($dataCat);

                            }

                        }

                    }

                }

            }



            if ($_FILES['uploadZip']['type'] == 'application/zip') { //application/x-zip-compressed

                $tmp_file = $_FILES['uploadZip']['tmp_name'];



                $this->unzip->allow(array('png', 'gif', 'jpeg', 'jpg'));

                $this->unzip->extract($tmp_file);

                $this->unzip->extract($tmp_file, FCPATH . '/uploads/products/');

                $this->session->set_flashdata('flashmsg', 'File Imported Successfully');

                $this->session->set_flashdata('msg', 'success');

            }

            redirect(base_url() . 'products/bulkUpload');

        } else {



            $this->session->set_flashdata("flashmsg", "Columns mismatch. Please download the sample file to make sure of column indexes.");

            $this->session->set_flashdata('msg', 'danger');

            redirect(base_url().'products/bulkUpload');

        }

	}



	// 31-05-2018 - Image upload by external URL
    public function uploadImageByCurl($url,$path){
        $ch = curl_init($url);
        $fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        return json_encode($result);
    }



}
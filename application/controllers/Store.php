<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Store extends MY_Controller {

	public $pageID="";

	public function __construct(){

		parent::__construct();

	    $this->load->library('form_validation');

	    $this->load->model('Stores_model');
	    $this->pageID = $this->session->userdata('pagemanager');
	}



	// show store page

	public function addStore(){

	    $data['content'] = 'admin/stores/addstore';

	    $data['title'] = 'Add Store';

	    $this->load->view('admin/template', $data);

	}



	// show store page

	public function saveStore(){

	    $data = array();

	    $storedataexists = $this->Stores_model->where(array("created_by" => $this->session->userdata('adminId')))->get();

        if($this->input->post()){

        	if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "social"){
        		if($storedataexists){
        			$this->session->set_flashdata('flashmsg', 'You are allowed to enter more stores.');
                    $this->session->set_flashdata('msg', 'danger');	
        			redirect(base_url().'store/addStore');
        		}
        	}


        	$this->form_validation->set_rules('page_name', 'Page Name', 'required');

            $this->form_validation->set_rules('page_id', 'Page ID', 'required');

            $this->form_validation->set_rules('access_token', 'Access Token', 'required');


            if ($this->form_validation->run() == true) {


            	$storedata = array(

            			'page_name' => $this->input->post('page_name'),

            			'page_id' => $this->input->post('page_id'),

            			'access_token' => $this->input->post('access_token'),
            			'created_by' => $this->session->userdata('adminId')

            		);

     				

                $success = $this->Stores_model->insert($storedata);



                if($success){

                    $this->session->set_flashdata('flashmsg', 'Store has been added.');

                    $this->session->set_flashdata('msg', 'success');


                    redirect(base_url().'store/addStore');

                }else{

                	$data['msg'] = 'danger';

                    $data['error_msg'] = 'Please try again...';

                }

            }

        }

        

		$data['content'] = 'admin/stores/addstore';

	    $data['title'] = 'Add Store';

        //load the view

        $this->load->view('admin/template', $data);

	} 



	// Get all stores

	public function listStores(){

		if(!empty($this->session->userdata('isLogin'))) {
	  	 $storelists = $this->Stores_model->where('created_by',$this->session->userdata('adminId'))->get_all();
	  	}
	  	else{
	  		
			if(is_null($this->pageID)){
			  	$storelists = $this->Stores_model->get_all();
			 }	
			else{
			  	$storelists = $this->Stores_model->where('page_id',$this->pageID)->get_all();
			}
	  	}	

	  	$data['stores'] = $storelists;

      	$data['content'] = 'admin/stores/liststores';

      	$data['title'] = 'All Stores';

      	$this->load->view('admin/template', $data);

	}



	// Edit store

	public function editStore($storeID=""){

		if($storeID){

			$storesdetails = $this->Stores_model->get(array('id' => $storeID));

			if($storesdetails){

			    $data['content'] = 'admin/stores/editstore';

			    $data['title'] = 'Edit Store';

			    $data['storedatas'] = $storesdetails;

			    $this->load->view('admin/template', $data);

			}else{

				redirect(base_url('admin/dashboard'));

			}

		}else{	

		redirect(base_url('admin/dashboard'));

		}

	}



	// update store by id

	public function updateStore($storeID=""){

		if($storeID){

			$storesdetails = $this->Stores_model->get(array('id' => $storeID));

			if($storesdetails){

				$storedata = array(

        			'page_name' => $this->input->post('page_name'),

        			'page_id' => $this->input->post('page_id'),

        			'access_token' => $this->input->post('access_token')

        		);



				$updated = $this->Stores_model->update($storedata, $storeID);

				if($updated){

					$this->session->set_flashdata('flashmsg', 'Store has been updated.');

		            $this->session->set_flashdata('msg', 'success');

				}else{

					$this->session->set_flashdata('flashmsg', 'No store details has been updated.');

		            $this->session->set_flashdata('msg', 'info');

				}

			}else{

				$this->session->set_flashdata('flashmsg', 'Store not found.');

	            $this->session->set_flashdata('msg', 'danger');

			}

		}else{	

			$this->session->set_flashdata('flashmsg', 'Store not found.');

	        $this->session->set_flashdata('msg', 'danger');

		}

		redirect(base_url() . 'store/listStores'); 

	}



	// Store has been removed

	public function removeStore($storeID=""){

		$this->Stores_model->delete(array('id' => $storeID));

        $this->session->set_flashdata('flashmsg', 'Store has been removed.');

        $this->session->set_flashdata('msg', 'success');

        redirect(base_url() . 'store/listStores');

	}



}
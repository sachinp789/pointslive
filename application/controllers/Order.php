<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public $pageID="";
	public function __construct(){
		parent::__construct();
		$this->load->model('Address_model');
		$this->load->model('Orders_model');
		$this->load->model('Stores_model');
		$this->pageID = $this->session->userdata('pagemanager');
		require_once('Admin.php');
		//echo $friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
      
	   // $lastbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($friday))." +6 days"));
	    //echo $paymentbilldate = date('Y-m-d',strtotime(date("Y-m-d", strtotime($friday))." +7 days"));
		//echo date('Y-m-d');
	}

	// Flutterwave Payment - 24-05-2018
	public function subscription(){
		$this->load->model('Chatbots_model');
		$chatsdata = $this->Chatbots_model->where('created_by',$this->session->userdata('adminId'))->order_by('id','desc')->get();
		$parameters = $this->input->get();
		$paymentinfo = $this->getTransactionByTrf($parameters['txref']);// Verify transaction info
		if(isset($parameters['flwref'])){ 
			//$paymentinfo = $this->getTransactionByTrf($parameters['txref'],$this->config->item('public_secret')); // payment verification

			if(!empty($paymentinfo['data']) && $paymentinfo['data']['status'] == "successful"){
				$orderdata = array(
					"transaction_ref" => $paymentinfo['data']['txref'],
					"order_amount"	=> $paymentinfo['data']['amount'],
					"applicable_fees"	=> $paymentinfo['data']['appfee'],
					"payment_method"	=> 'flutterwave',
					"status"	=> '0',
					"created_by"	=> $this->session->userdata('adminId')
				);

				if(!is_null($paymentinfo['data']['paymentplan']) || is_numeric($paymentinfo['data']['paymentplan'])){
					$orderdata['payment_plan_subscribe'] = $paymentinfo['data']['paymentplan'];
				    $this->Chatbots_model->update(array('subscribe_status' => 'yes','renew_subscription' => '1'),$chatsdata->id); // Renew plan subscription entry
				}else{
					$this->Chatbots_model->update(array('subscribe_status' => 'no','renew_subscription' => '0'),$chatsdata->id);
				}
				// Store data
				$created = $this->db->insert('subscription_orders',$orderdata);

				if($created){ 
					// Send next reminder payment mail
					$this->sendMailtoUsers();
					// end
					$this->session->set_flashdata('flashmsg','Payment has been done successfully.');
		    		$this->session->set_flashdata('msg', 'success');
				}
				redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
			}
			else if(!empty($paymentinfo['data']) && $paymentinfo['data']['status'] == "failed"){
              	$orderdata = array(
					"transaction_ref" => $paymentinfo['data']['txref'],
					"order_amount"	=> $paymentinfo['data']['amount'],
					"applicable_fees"	=> $paymentinfo['data']['appfee'],
					"payment_method"	=> 'flutterwave',
					"status"	=> '1',
					"created_by"	=> $this->session->userdata('adminId')
				);
				if(!is_null($paymentinfo['data']['paymentplan']) || is_numeric($paymentinfo['data']['paymentplan'])){
					$orderdata['payment_plan_subscribe'] = $paymentinfo['data']['paymentplan'];	
				    $this->Chatbots_model->update(array('subscribe_status' => 'yes','renew_subscription' => '1'),$chatsdata->id); // Renew plan subscription entry
				}
				else{
				    $this->Chatbots_model->update(array('subscribe_status' => 'no','renew_subscription' => '0'),$chatsdata->id);
				}

				// Store data
				$created = $this->db->insert('subscription_orders',$orderdata);

				if($created){
					$this->session->set_flashdata('flashmsg','Payment has been failed.');
		    		$this->session->set_flashdata('msg', 'danger');
				}
				redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
          	}	
		}
		else{
			$this->session->set_flashdata('flashmsg','Payment has been cancelled.');
	    	$this->session->set_flashdata('msg', 'danger');	
			redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
		}
	}

	// Retrieve flutterwave transaction details using API - 24-05-2018
	public function getTransactionByTrf($tran=""){

		$result = array();

		$postdata = array( 
		  'txref' => $tran,
		  'SECKEY' => config_item('public_secret'),
		  'inlcude_payment_entity' => 1
		  );

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/xrequery");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$headers = [
		  'Content-Type: application/json',
		];

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$request = curl_exec($ch);
		curl_close ($ch);

		$result = json_decode($request, true);
		return $result;
	}

	// send mail to users for next payment cycle
	public function sendMailtoUsers(){
		$userinfo = admin_info($this->session->userdata('adminId'));
		$message = "";
		$totalpaidamount = 0;
		if(!is_null($userinfo->admin_email)){

			$orderinfo = $this->db->select('*')->from('subscription_orders')->where(array('created_by' => $this->session->userdata('adminId')))->order_by('created_date','desc')->get()->row();

			$totalpaidamount = $orderinfo->order_amount + $orderinfo->applicable_fees;
			$date = strtotime(date("Y-m-d", strtotime($orderinfo->created_date)) . " +1 month");
            $nextpaymentdate = date("Y-m-d",$date); 

			$message .= "Hello {$userinfo->admin_name} ,<br><br>";
			$message .= "Your payment has been done successfully with amount <b>{$totalpaidamount}</b>.<br><br>";
			$message .= "Your next due payment date is :- <b>{$nextpaymentdate}</b>";
			$message .= "<br><br>";
			$message.="Thanks, <br> Pointslive";

			$emailsettings = emailconfig();

			$this->load->library('email',$emailsettings);

			$this->email->set_newline("\r\n");   
	        $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
	        $this->email->message($message); 
	        $this->email->to($userinfo->admin_email);
	        $this->email->subject('Payment Transaction');
	        $send = $this->email->send();
	        $this->email->clear();
			return true;
		}else{
			return true;
		}
	}

	// Save user shipping address
	public function addShippingAddress($userID=""){
		$address = '';
		$address.= trim($this->input->post('address_line1'));
		$address.=",\n";
		if(!empty($this->input->post('address_line2'))){
		$address.= trim($this->input->post('address_line2'));
		$address.=",\n";
		}
		$address.= trim($this->input->post('city'));
		$address.=",\n";
		$address.= trim($this->input->post('state'));
		$address.=",\n";
		$address.= trim($this->input->post('country')).",".$this->input->post('zipcode');

		$info = array(
				'user_id' 	=> $userID,
				'shipping_address'	=> $address
			);

		$saved = $this->Address_model->insert($info);
		$response = array();
		if($saved){
			$response['success'] = true;
		}
		else{
			$response['response'] = false;
		}
		redirect(base_url()."admin/productlistsByUserid/$userID");
	}

	public function saveAddress(){
		$addressid = $this->input->post('id');
		$this->session->set_userdata('shipping_address',$addressid);
		exit;
	}

	// List of users orders
	public function listOrders(){
	  $data = array();
	 
	 /* if(is_null($this->pageID)){
	  	$orders = orderlists();
	  }
	  else{

	  	$orders = orderlists("",$this->pageID);
	  }*/

	  /*if(!empty($this->session->userdata('isLogin'))){
	  	$orders = orderlists("","",$this->session->userdata('adminId'));
	  }
	  else if(is_null($this->pageID)){
	  	$orders = orderlists();
	  }
	  else{
	  	$orders = orderlists("",$this->pageID);
	  }*/

      $data['content'] = 'admin/orders/list';
      $data['title'] = 'Orders';
     // $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	public function listAutoCancelledOrders(){
	  $data = array();
	  //var_dump($this->pageID);

	  if(!empty($this->session->userdata('isLogin'))){
	  	$orders = orderlistscancelled($this->session->userdata('adminId'));
	  }
	  else{
	  	$orders = orderlistscancelled("");
	  }

	  //$orders = orderlistscancelled();
      $data['content'] = 'admin/orders/autocancelledlist';
      $data['title'] = 'Auto Cancelled Orders';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// View order details
	public function viewOrder($orderID=""){
	  $orders = orderlists($orderID);
	  $data = array();
      $data['content'] = 'admin/orders/vieworder';
      $data['title'] = 'Order';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// View cancelled order details
	public function viewCancelledOrder($orderID=""){
	  $orders = orderlists($orderID);
	  $data = array();
      $data['content'] = 'admin/orders/viewcancelorder';
      $data['title'] = 'Order';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// Order shipment
	public function shipOrder($orderID=""){

		$ordersdata = $this->Orders_model->fields('shipping_address,user_id,page_id,transaction_id')->get($orderID);

		$language = chooselanguage($ordersdata->user_id);
	    if(sizeof($language) > 0){
	      $lang = chooselanguage($ordersdata->user_id);
	      $language_code = $lang[0]->language;
	    }
	    else{
	      $language_code = "english";
	    }

	    //echo $language_code;exit;
	    lang_switcher($language_code);
		// Shipping orders pending to create
        $shipaddress = $this->Address_model->get($ordersdata->shipping_address);
      	$shiptype = "";
      	if($shipaddress->delivery_method == 'next_day'){
        	$shiptype = "NEXT_DAY";
      	}
      	if($shipaddress->delivery_method == 'express' || $shipaddress->delivery_method == 'ด่วน'){
          $shiptype = "EXPRESS_1_2_DAYS";
        }
        if($shipaddress->delivery_method == 'standard' || $shipaddress->delivery_method == 'มาตรฐาน'){
          $shiptype = "STANDARD_2_4_DAYS";
        }

      	$provinces = getStateByDistrict($shipaddress->district); // States shipping address

	    if($language_code == 'english'){
	       $state = $provinces[0]->province;
	    }else{
	       $state = $provinces[0]->province_thai;
	    }

      	// Get user information
      	if($ordersdata->page_id == "652803498256943"){
	    	$usertoken = "EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
	    }
	    else{
      	// Get user information
      		$userinfo = $this->Stores_model->fields('access_token')->get(array('page_id' => $ordersdata->page_id));
	    	$usertoken = $userinfo->access_token;		
	    }

      	$userurl = "https://graph.facebook.com/{$ordersdata->user_id}?access_token=".$usertoken;
    	$ch = curl_init();
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch,CURLOPT_URL,$userurl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    $results = curl_exec($ch);
	    curl_close($ch);
	    $content = json_decode($results);

	    $fullname = $content->first_name.' '.$content->last_name;
	    
      	$pageurl = "https://graph.facebook.com/{$ordersdata->page_id}?fields=phone,emails,name,hours,single_line_address&access_token=".$usertoken;
      	$ch = curl_init();
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch,CURLOPT_URL,$pageurl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    $results = curl_exec($ch);
	    curl_close($ch);
	    $businessaddress = json_decode($results);

	    $validatetoken = Admin::acommerceValidateAPI(); // Get Access Token of Logistic API

        if(!is_null($validatetoken)){
          $token = $validatetoken['token']['token_id'];
        }

	    $addressee = "";
      	$address1 = "";
      	$province = "";
      //$postalCode = "";
      	$country = "";
      	$phone = "";
      	$email = "";

      	if(isset($businessaddress->name)){
        	$addressee = $businessaddress->name;
      	}
      	if(isset($businessaddress->single_line_address)){
        	$address1 = $businessaddress->single_line_address;
        	$explodeaddr = explode(",", $businessaddress->single_line_address);
        	$provinces = explode(" ", $explodeaddr[2]);
        	$provincename = $provinces[1];
        	$postalCode = $provinces[2];
      	}
      
      	if(isset($businessaddress->phone)){
        	$phone = $businessaddress->phone;
      	}
      	if(isset($businessaddress->emails)){
        	$email = $businessaddress->emails[0];
      	}

      	$orders = orderlists($ordersdata->transaction_id);
                      
	    $orderItems = array();

	      foreach ($orders[0]->products as $key => $value) {
	          if($value->sale_price > 0){
	            $price = $value->sale_price;
	          }else{
	            $price = $value->product_price;
	          }
	          if($language_code == 'english'){
	            $pname = $value->product_name;
	          }else{
	            $pname = $value->product_name_thai;
	          }
	         $shiporderItems[] = array(
	          "itemDescription" => "{$pname}",
	          "itemQuantity"    => (int)$value->ordered_qty,
	          );
	    }

		    $shipordersDetails = [
	        "shipServiceType" => "DELIVERY",
	        "shipSender" => [
	            "addressee"=>"{$addressee}",
	            "address1"=>"{$address1}",
	            "city"=>"",
	            "province"=>"{$provincename}",
	            "postalCode"=>"{$postalCode}",
	            "country"=> "{$provincename}",
	            "phone"=> "{$phone}",
	            "email"=>"{$email}"
	        ],
	        "shipShipment" =>[
	            "addressee"=>"{$fullname}",
	            "address1"=>"{$shipaddress->shipping_address}",
	            "address2"=>"",
	            "subDistrict"=>"",
	            "district"=>"{$shipaddress->district}",
	            "city"=>"",
	            "province"=>"{$state}",
	            "postalCode"=>"{$shipaddress->postcode}",
	            "country"=>"{$shipaddress->country}",
	            "phone"=>"{$shipaddress->phone}",
	            "email"=>"{$shipaddress->email}"
	        ],
	        "shipPickup" =>[
	          "addressee"=>"{$addressee}",
	          "address1"=>"{$address1}",
	          "city"=>"",
	          "province"=>"{$provincename}",
	          "postalCode"=>"{$postalCode}",
	          "country"=> "{$provincename}",
	          "phone"=> "{$phone}",
	          "email"=>"{$email}"
	        ],
	        "shipShippingType"=>"{$shiptype}",
	        "shipPaymentType"=>"COD",
	        "shipCurrency"=>"THB",
	        "shipGrossTotal"=>(int)$orders[0]->order_amount,
	        "shipInsurance"=>false,
	        "shipPickingList"=>$shiporderItems,
	        "shipPackages"  => []
	      ]; 

	    $salesorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$ordersdata->transaction_id}",$token,$shipordersDetails);
	    //var_dump($salesorder);
	    if(isset($salesorder['http_code']) && $salesorder['http_code'] == 201){
			$data = array(
				 'id' => $orderID,	
				 'status' => '1',
				 //'order_type' => 'shipping',
				 'shipment_date' => date('Y-m-d h:i:s') 				
			);
			$update = $this->Orders_model->shiporder($data);
			if($update){
				// Barcode generate TYPE_CODE_128 of order id
	            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
	            $string = base64_encode($generator->getBarcode("{$ordersdata->transaction_id}", $generator::TYPE_CODE_128));
	           
	            $image = base64_decode($string);
	            $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
	            $filename = $image_name . '.' . 'png';
	            //rename file name with random number
	            $path = FCPATH.'uploads/barcodes/';
	            //image uploading folder path
	            file_put_contents($path . $filename, $image);

	            $imageURL = base_url()."uploads/barcodes/".$filename;
	            //echo $imageURL;
	            $sendorder = Admin::sendImageToBotAPI($usertoken,$ordersdata->page_id,$ordersdata->user_id,$imageURL);
	            //print_r($sendorder);
	            if(isset($sendorder->attachment_id)){
	              $message_to_reply = $this->lang->line('orderidbarcode');

	              $responsee = [
	                  'recipient' => [ 'id' => $ordersdata->user_id ],
	                  'message' => [ 'text' => $message_to_reply]
	              ];
	             
	              $chinit = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$usertoken);  
				  curl_setopt($chinit, CURLOPT_POST, 1);
				  curl_setopt($chinit, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				  curl_setopt($chinit, CURLOPT_POSTFIELDS, json_encode($responsee));
				    //Set the content type to application/json
				  curl_setopt($chinit, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				  curl_exec($chinit);	

	              // Barcode generate TYPE_CODE_128 of product_sku+payment
	              $skupaymetnbarcode = $orders[0]->products[0]->product_sku.'COD';
	              $generatornew = new \Picqer\Barcode\BarcodeGeneratorPNG();
	              $stringcode = base64_encode($generatornew->getBarcode("{$skupaymetnbarcode}", $generator::TYPE_CODE_128));
	             
	              $image = base64_decode($stringcode);
	              $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
	              $filenamenew = $image_name . '.' . 'png';
	              //rename file name with random number
	              $path = FCPATH.'uploads/barcodes/';
	              //image uploading folder path
	              file_put_contents($path . $filenamenew, $image);

	              $imageURL2 = base_url()."uploads/barcodes/".$filenamenew;

	              $send = Admin::sendImageToBotAPI($usertoken,$ordersdata->page_id,$ordersdata->user_id,$imageURL2);

	              if(isset($send->attachment_id)){
	                $message_to_reply = $this->lang->line('skubarcode');

	                $response1 = [
	                    'recipient' => [ 'id' => $ordersdata->user_id ],
	                    'message' => [ 'text' => $message_to_reply]
	                ];

	                $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$usertoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response1));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);

	                $message_to_reply = $this->lang->line('trackingcode');

	                $response = [
	                    'recipient' => [ 'id' => $ordersdata->user_id ],
	                    'message' => [ 'text' => $message_to_reply]
	                ];

	                $ch1 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$usertoken);  
				    curl_setopt($ch1, CURLOPT_POST, 1);
				    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch1);
	              }
	            }
				$this->session->set_flashdata('flashmsg','Order has been dispatched.');
			    $this->session->set_flashdata('msg', 'success');
			}
			else{
				$this->session->set_flashdata('flashmsg','Error to dispatch order.');
	        	$this->session->set_flashdata('msg', 'danger');
			}			
	    }
	    else{
	    	$this->session->set_flashdata('flashmsg',"422 code. Validation failed");
	        $this->session->set_flashdata('msg', 'danger');
	    }
		redirect(base_url().'order/listOrders');
	}

	public function sendMail(){
		//exit;
		$config = emailconfig();
        $this->load->library('email',$config);       
        $this->email->set_newline("\r\n");
 
        $this->email->from('no-reply@pointslive.com', config_item('site_name'));
        $this->email->to("sachin.prajapati@searchnative.in");
        $this->email->subject('Test mail');
        $this->email->message("How are you");

        try {
            if ($this->email->send()) {
            	$this->session->set_flashdata('flashmsg', 'Please check your email to reset your password.');
            	$this->session->set_flashdata('msg', 'success');
            }
            else{
            	$this->session->set_flashdata('flashmsg', 'Error while send mail! Please try again or try after sometime.');
            	$this->session->set_flashdata('msg', 'danger');
            }

        } catch (Exception $e) {
        	$this->session->set_flashdata('flashmsg', 'Error while send mail! Please try again or try after sometime.');
            $this->session->set_flashdata('msg', 'danger');
        }

	}

	// Shipping and sales orders status updatation check API and sync with database
	public function syncOrdersStatus(){
		//exit;
		$shiporders = array();
		$validatetoken = Admin::acommerceValidateAPI(); // Get Access Token of Logistic API

        if(!is_null($validatetoken)){
          $token = $validatetoken['token']['token_id'];
        }

        //$orders = $this->Orders_model->where_transaction_ref("")->get_all();

        $this->db->select('*');
		$this->db->where("created_by IS NULL");
		$this->db->where("order_type","shipping");
		$orders = $this->db->get('orders')->result();

       // echo $this->db->last_query();exit;

        if(count($orders) > 0){
	        foreach ($orders as $key => $value) {
	        	
		      	$userurl = "https://shipping.api.acommercedev.com/partner/1163/shipping-order-status/id?id={$value->transaction_id}";
		    	$ch = curl_init();

		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$token}",'Content-Type:application/json']);
			    $results = curl_exec($ch);
			    $shipdata = json_decode($results);
			    $headerinfo = curl_getinfo($ch);
		
			  	if(!is_null($shipdata)) { //!isset($shipdata[0]->error->code
			  		foreach ($shipdata as $key => $value) {
			  			$shiporders[] = $value;
			  		}
			  	}
		    }

		    if(count($shiporders) > 0){
		    	// shipOrderId
		    	foreach ($shiporders as $key => $ship) {
		    		if(!empty($ship->shipPartnerSalesOrderID)){

	    				$checkStatus = $this->Orders_model->get(array('transaction_id' => $ship->shipOrderId));
	    				//echo '<pre>';
	    				//print_r(end($ship->shipOrderDetailedStatus)->shipOrderDetailedStatus);
	    				if($ship->shipPartnerSalesOrderID == $checkStatus->transaction_id){
	    					//$status = end($ship->shipOrderStatus)->shipOrderStatus; 23-01-2018
	    					//if(!empty($ship->shipPackage[0]->statusHistory)){
	    						$status = end($ship->shipOrderDetailedStatus)->shipOrderDetailedStatus;

	    						if($status == 'IN_TRANSIT'){//&& $checkStatus->status == 0
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '1','shipment_date' => date('Y-m-d h:i:s')));
	    						}
	    						else if($status == 'COMPLETED'){//DELIVERED
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '2','complete_date' => date('Y-m-d h:i:s')));
	    						}
	    						else if($status == 'CANCELLED'){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '3'));
	    						}
	    						else if($status == 'REJECTED'){//REJECTED_BY_CUSTOMER
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '4','status_updates' => 0));
	    						}
	    						else if($status == 'FAILED_TO_DELIVER'){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '5'));
	    						}
	    					//}
	    					//foreach ($packages as $key => $shippack) {
	    						
	    					//}
	    				}
	    			}	
	    		}
	    		//exit;
		    	foreach ($orders as $key => $value) {
				// Send message to chatbot users for status updates	
			    $pgtoken = "";
			    if($value->page_id == "652803498256943"){
			    	$pgtoken="EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
			    }
			    else{
			   	 $userinfo = $this->Stores_model->fields('access_token')->get(array('page_id' => $value->page_id));
			   	 $pgtoken = $userinfo->access_token;
			    }	

			    //$fullname = "";
			    $usersdetails = $this->Address_model->where(array('id' => $value->shipping_address))->fields('receiver_name')->get();
			    if(!is_null($usersdetails->receiver_name)){
			      $fullname = $usersdetails->receiver_name;
			    }
			    else{
			      $fullname = "";
			    }

			    /*$language = chooselanguage($value->user_id);
			    if(sizeof($language) > 0){
			      //$lang = chooselanguage($value->user_id);
			      $language_code = $language[0]->language;
			    }
			    else{
			      $language_code = "english";
			    }
			    lang_switcher($language_code);*/

		      	/*$userurl = "https://graph.facebook.com/{$value->user_id}?access_token=".$pgtoken;
		    	$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			    $results = curl_exec($ch);
			    curl_close($ch);
			    $content = json_decode($results);

			    @$fullname = $content->first_name.' '.$content->last_name;*/

			    // Status is 'Dispatched'
			    if($value->status == '1' && $value->status_updates == 0){
			    	$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);

			    	$findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_ship'));
		    		$message_to_reply = $findstring;

		    		// Send all message using CURL
				    $response = [
			                'recipient' => [ 'id' => $value->user_id ],
			                'message' => [ 'text' => $message_to_reply]
			            ];

		            $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);
				    // END


			    }
			    // Status is 'Complete'
			    else if($value->status == '2' && $value->status_updates == 0){
			    	//echo $checkStatus->transaction_id;
			    	$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);

			    	$findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_delivered'));
		    		$message_to_reply = $findstring;

		    		 // Send all message using CURL
				    $response = [
			                'recipient' => [ 'id' => $value->user_id ],
			                'message' => [ 'text' => $message_to_reply]
			            ];

		            $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);
				    // END
				    //$this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));

			    }
			    // Status is 'Cancelled'
			    else if($value->status == '3' && $value->status_updates == 0){

			    	$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);

				    $findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_cancelled'));
				    $findstring = str_replace("<orderno>", $value->transaction_id, $findstring);

		    		$message_to_reply = $findstring;

		    		 // Send all message using CURL
				    $response = [
			                'recipient' => [ 'id' => $value->user_id ],
			                'message' => [ 'text' => $message_to_reply]
			            ];

		            $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);
				    // END

		    		$message_to_reply_visit = $this->lang->line('visitsite');

		            $response2 = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply_visit]
		            ];

		            $chinit1 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($chinit1, CURLOPT_POST, 1);
				    curl_setopt($chinit1, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($chinit1, CURLOPT_POSTFIELDS, json_encode($response2));
				    //Set the content type to application/json
				    curl_setopt($chinit1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($chinit1);
				    curl_close($chinit1);

				    //$this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));
			    }
			    // Status is 'Rejected'
			   	else if($value->status == '4' && $value->status_updates == 0){
			   		$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);
				    // Get user information
				    $findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_rejected'));
		    		$message_to_reply = $findstring;

		    		 // Send all message using CURL
				    $response = [
			                'recipient' => [ 'id' => $value->user_id ],
			                'message' => [ 'text' => $message_to_reply]
			            ];

		            $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);
				    // END
				   // $this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));
			    }
			    // Status is 'Failed'
			    else if($value->status == '5' && $value->status_updates == 0){
			    	$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);
				    // Get user information
				    $findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_failed'));
		    		$message_to_reply = $findstring;

		    		 // Send all message using CURL
				    $response = [
			                'recipient' => [ 'id' => $value->user_id ],
			                'message' => [ 'text' => $message_to_reply]
			            ];

		            $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);
				    // END

				 }

			    if($value->status == '1' && $value->status_updates == 0){ // IF status is 'dispatched'
			    	$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);
			    	// Send tracking code message to users
			    	$message_to_reply = $this->lang->line('visittracksite');
		    		$findstring = str_replace("http://tracking.acommerce.asia/smartship", "http://tracking.acommerce.asia/smartship?id={$value->transaction_id}", $message_to_reply);

		            $responsees = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $findstring]
		            ];
		            $ch5 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch5, CURLOPT_POST, 1);
				    curl_setopt($ch5, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch5, CURLOPT_POSTFIELDS, json_encode($responsees));
				    //Set the content type to application/json
				    curl_setopt($ch5, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch5);
				    curl_close($ch5);

				    //$this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));
			    }
			    else if($value->status == '5' && $value->status_updates == 0){ // IF status is 'failed'
			    	$language = chooselanguage($value->user_id);
				    if(sizeof($language) > 0){
				      //$lang = chooselanguage($value->user_id);
				      $language_code = $language[0]->language;
				    }
				    else{
				      $language_code = "english";
				    }
				    lang_switcher($language_code);
			    	// Send good time deliver message to users	
			    	$message_to_reply = $this->lang->line('godtime');

		    		$response = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply]
		            ];

		    		$ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);

				    //$this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));
			    }
			    $this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));
			  }
	    	}
	    	else{
	    		echo "API response not found";
	    		exit;
	    	}
       }
	}

	// Order shipment
	public function adminshipOrder($orderID=""){
		//die("d");
		$orders = $this->Orders_model->get($orderID);
		$storesdata = $this->Stores_model->where('page_id',$orders->page_id)->get();
		
		$data = array(
			 'id' => $orderID,	
			 'status' => '1',
			 'shipment_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->shiporder($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been dispatched.');
		    $this->session->set_flashdata('msg', 'success');

		    if($storesdata && !in_array($orders->page_id,array('652803498256943'))) {
		    	$pagetoken = $storesdata->access_token;
		    	$userurl = "https://graph.facebook.com/{$orders->user_id}?access_token=".$pagetoken;
		      	//echo @$userinfo->access_token;
		    	$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			    $results = curl_exec($ch);
			    curl_close($ch);
			    $content = json_decode($results);

			    if(isset($content->error)){
			    	$path = __DIR__.'\Order.php function adminshipOrder';
			        $line = 938;
			        apilogger($content->error->message,$path,$line);
			    }

			    $username = $content->first_name.' '.$content->last_name;
			    $userID = $orders->user_id;

			    $message = "Hello *{$username}*. Good News ! Your order has been processed !";

			     // Send Notifcation of Order
		    	$this->pushNotificationtoUsers($username,"1",$userID,$orders->page_id,$pagetoken,$message,$orders->transaction_id);
			}
			else{
				$sellerInfo = admin_info($orders->created_by);
				$addressinfo = $this->db->select('*')->from('address')->where('id',$orders->shipping_address)->get()->row();
				// Email send 
	            $data['guestname'] = $addressinfo->receiver_name;
	            $data['orderno'] = $orders->transaction_id;
	            $data['userdetails'] = $addressinfo;
	            $data['shipmethod'] = $orders->store_type;
	            $data['seller'] = $sellerInfo->admin_name;
				
				if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
	            $mailConfig['protocol'] = "sendmail";
	            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
	            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
	            $mailConfig['smtp_pass'] = "tg1<LvPL";
	            $mailConfig['smtp_port'] = "25";
	            $mailConfig['mailtype'] = "html";

              	$this->load->library('email', $mailConfig);

              	$body = $this->load->view('templates/guest_order_shipped_template',$data,TRUE);

              	$this->email->set_newline("\r\n");   
	            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
	            $this->email->message($body); 
	            $this->email->to($addressinfo->email);
	            $this->email->subject('Order Shipment');

	            $send = $this->email->send();

	            if(!$send){
	                $path = __DIR__.'\Order.php function adminshipOrder';
	                $line = 938;
	                apilogger($this->email->print_debugger(),$path,$line);
	            }

			}
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to dispatch order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	// Order completion
	public function orderComplete($orderID=""){
		$orders = $this->Orders_model->get($orderID);
		$storesdata = $this->Stores_model->where('page_id',$orders->page_id)->get();

		$data = array(
			 'id' => $orderID,	
			 'status' => '2',
			 'complete_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->ordercomplete($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been completed.');
		    $this->session->set_flashdata('msg', 'success');

		    if($storesdata && !in_array($orders->page_id,array('652803498256943'))) {
		    	$pagetoken = $storesdata->access_token;
				$userurl = "https://graph.facebook.com/{$orders->user_id}?access_token=".$pagetoken;
		      	//echo @$userinfo->access_token;
		    	$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			    $results = curl_exec($ch);
			    curl_close($ch);
			    $content = json_decode($results);

			    if(isset($content->error)){
			    	$path = __DIR__.'\Order.php function orderComplete';
			        $line = 988;
			        apilogger($content->error->message,$path,$line);
			    }

			    $username = $content->first_name.' '.$content->last_name;
			    $userID = $orders->user_id;
			    $message = "Hello *{$username}*. Thank you for shopping with us. Hope you enjoy your order.";

			    // Send Notifcation of Order
		    	$this->pushNotificationtoUsers($username,"2",$userID,$orders->page_id,$pagetoken,$message,$orders->transaction_id);
			}
			else{
				$sellerInfo = admin_info($orders->created_by);
				$addressdata = $this->db->select('*')->from('address')->where("id",$orders->shipping_address)->get()->row();
				// Email send 
				$data['guestname'] = $addressdata->receiver_name;
		        $data['review_link'] = base_url("item/review/".$orderID); // Remove encryption
		        $data['orderno'] = $orders->transaction_id;
		        $data['seller'] = $sellerInfo->admin_name;
				//$fromEmail = "no-reply@pointslive.com";

              	$body = $this->load->view('templates/guest_order_delivered_template',$data,TRUE);

              	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
	            $mailConfig['protocol'] = "sendmail";
	            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
	            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
	            $mailConfig['smtp_pass'] = "tg1<LvPL";
	            $mailConfig['smtp_port'] = "25";
	            $mailConfig['mailtype'] = "html";

              	$this->load->library('email', $mailConfig);

              	$this->email->set_newline("\r\n");   
	            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
	            $this->email->message($body); 
	            $this->email->to($addressdata->email);
	            $this->email->subject('Order Delivery');

	            $send = $this->email->send();

	            if(!$send){
	                $path = __DIR__.'\Order.php function orderComplete';
	                $line = 1038;
	                apilogger($this->email->print_debugger(),$path,$line);
	            }
			}
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to complete order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	// Vendor dispatch Order - 12-06-2018
	public function dispatchOrder($orderID=""){

		$orders = $this->Orders_model->get($orderID);
		
		$storesdata = $this->Stores_model->where('page_id',$orders->page_id)->get();

		$data = array(
			 'id' => $orderID,	
			 'status' => '1',
			 'shipment_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->shiporder($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been dispatched.');
		    $this->session->set_flashdata('msg', 'success');

		    if($storesdata){

		    	$pagetoken = $storesdata->access_token;

				$userurl = "https://graph.facebook.com/{$orders->user_id}?access_token=".$pagetoken;
		      	//echo @$userinfo->access_token;
		    	$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			    $results = curl_exec($ch);
			    curl_close($ch);
			    $content = json_decode($results);

			    if(isset($content->error)){
			    	$path = __DIR__.'\Order.php function dispatchOrder';
			        $line = 1036;
			        apilogger($content->error->message,$path,$line);
			    }

			    $username = $content->first_name.' '.$content->last_name;
			    $userID = $orders->user_id;

			    $message = "Hello *{$username}*. Good News ! Your order has been processed !";

			    // Send Notifcation of Order
			    $this->pushNotificationtoUsers($username,"1",$userID,$orders->page_id,$pagetoken,$message,$orders->transaction_id);
		    }
		    else{

				$sellerInfo = admin_info($orders->created_by);

		    	$addressinfo = $this->db->select('*')->from('address')->where('id',$orders->shipping_address)->get()->row();
				// Email send 
	            $data['guestname'] = $addressinfo->receiver_name;
	            $data['orderno'] = $orders->transaction_id;
	            $data['userdetails'] = $addressinfo;
	            $data['shipmethod'] = $orders->store_type;
	            $data['seller'] = $sellerInfo->admin_name;

	            //$fromEmail = "no-reply@pointslive.com";

              	$body = $this->load->view('templates/guest_order_shipped_template',$data,TRUE);

              	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
	            $mailConfig['protocol'] = "sendmail";
	            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
	            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
	            $mailConfig['smtp_pass'] = "tg1<LvPL";
	            $mailConfig['smtp_port'] = "25";
	            $mailConfig['mailtype'] = "html";

              	$this->load->library('email', $mailConfig);

              	$this->email->set_newline("\r\n");   
	            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
	            $this->email->message($body); 
	            $this->email->to($addressinfo->email);
	            $this->email->subject('Order Shipment');

	            $send = $this->email->send();

	            if(!$send){
	                $path = __DIR__.'\Order.php function dispatchOrder';
	                $line = 1136;
	                apilogger($this->email->print_debugger(),$path,$line);
	            }
		    }
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to dispatched order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	// Push message to users of order status change
	public function pushNotificationtoUsers($username="",$status="",$userID="",$page="",$token="",$message="",$order=""){
		$response = [
	      'recipient' => [ 'id' => $userID ],
	      'message' => [ 'text' => $message]
	    ];

        $ch1 = curl_init("https://graph.facebook.com/v2.6/{$page}/messages?access_token=".$token);  
	    curl_setopt($ch1, CURLOPT_POST, 1);
	    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response));
	    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    curl_exec($ch1);
	    curl_close($ch1);

	    if($status == "2"){

	    	$options = array(
	              0 => array(
	                  'content_type' => 'text',
	                  'title' => "★1",
	                  'payload' => "payload_rate_{$order}"
	              ),
	              1 => array(
	                  'content_type' => 'text',
	                  'title' => "★2",
	                  'payload' => "payload_rate_{$order}"
	              ),
	              2 => array(
	                  'content_type' => 'text',
	                  'title' => "★3",
	                  'payload' => "payload_rate_{$order}"
	              ),
	              3 => array(
	                  'content_type' => 'text',
	                  'title' => "★4",
	                  'payload' => "payload_rate_{$order}"
	              ),
	              4 => array(
	                  'content_type' => 'text',
	                  'title' => "★5",
	                  'payload' => "payload_rate_{$order}"
	              )
	        ); 

	        $quickoptions = json_encode($options);
	        $message_to_reply = "Please rate the service";

	        $response = [
	              'recipient' => [ 'id' => $userID ],
	              'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
	          ];	

	    	$ch1 = curl_init("https://graph.facebook.com/v2.6/{$page}/messages?access_token=".$token);  
		    curl_setopt($ch1, CURLOPT_POST, 1);
		    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response));
		    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		    curl_exec($ch1);
		    curl_close($ch1);
	    }
	}

	// Order cancellation reasons list
	public function pushOrderReasontoUsers($userID="",$page="",$token="",$message=""){

		$response = [
	      'recipient' => [ 'id' => $userID ],
	      'message' => [ 'text' => $message]
	    ];

        $ch1 = curl_init("https://graph.facebook.com/v2.6/{$page}/messages?access_token=".$token);  
	    curl_setopt($ch1, CURLOPT_POST, 1);
	    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response));
	    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    curl_exec($ch1);
	    curl_close($ch1);

	    $options = array(
	              0 => array(
	                  'content_type' => 'text',
	                  'title' => "Not Processed on Time (Auto Cancel)",
	                  'payload' => "payload_order_cancel"
	              ),
	              1 => array(
	                  'content_type' => 'text',
	                  'title' => "Out of Stock",
	                  'payload' => "payload_order_cancel"
	              ),
	              2 => array(
	                  'content_type' => 'text',
	                  'title' => "Founder Cheaper Product",
	                  'payload' => "payload_order_cancel"
	              ),
	              3 => array(
	                  'content_type' => 'text',
	                  'title' => "Customer Cancel",
	                  'payload' => "payload_order_cancel"
	              ),
	              4 => array(
	                  'content_type' => 'text',
	                  'title' => "Outside of Shipping Area",
	                  'payload' => "payload_order_cancel"
	              ),
	              5 => array(
	                  'content_type' => 'text',
	                  'title' => "Other",
	                  'payload' => "payload_order_cancel"
	              )
	        ); 

	        $quickoptions = json_encode($options);
	        $message_to_reply = "Choose from list of cancellation reason";

	        $response = [
	              'recipient' => [ 'id' => $userID ],
	              'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
	          ];	

	    	$ch1 = curl_init("https://graph.facebook.com/v2.6/{$page}/messages?access_token=".$token);  
		    curl_setopt($ch1, CURLOPT_POST, 1);
		    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response));
		    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		    curl_exec($ch1);
		    curl_close($ch1);
	}

	// Cancel order
	public function cancelOrder($orderID=""){

		$orders = $this->Orders_model->get($orderID);
		$storesdata = $this->Stores_model->where('page_id',$orders->page_id)->get();
		
		$data = array(
			 'id' => $orderID,	
			 'status' => '3'
		);
		$update = $this->Orders_model->cancelorder($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been cancelled.');
		    $this->session->set_flashdata('msg', 'success');

		    if($storesdata && !in_array($orders->page_id,array('652803498256943'))) {
		    	$pagetoken = $storesdata->access_token;

				$userurl = "https://graph.facebook.com/{$orders->user_id}?access_token=".$pagetoken;
		      	//echo @$userinfo->access_token;
		    	$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			    $results = curl_exec($ch);
			    curl_close($ch);
			    $content = json_decode($results);

			    if(isset($content->error)){
			    	$path = __DIR__.'\Order.php function cancelOrder';
			        $line = 1215;
			        apilogger($content->error->message,$path,$line);
			    }

			    $username = $content->first_name.' '.$content->last_name;
			    $userID = $orders->user_id;
			    $message = "Hello *{$username}*. Your order *{$orders->transaction_id}* has been cancelled.";

			    // Send Notifcation of Order
		    	$this->pushNotificationtoUsers($username,"3",$userID,$orders->page_id,$pagetoken,$message);

		    	// Cancell reasons message send to users
		    	$message = "We are sorry this happened. Can you tell us why the order was cancelled ?";

		    	$this->pushOrderReasontoUsers($userID,$orders->page_id,$pagetoken,$message);
			}
			else{
				$addressinfo = $this->db->select('*')->from('address')->where('id',$orders->shipping_address)->get()->row();
				// Email send 
	            $data['guestname'] = $addressinfo->receiver_name;
	            $data['orderno'] = $orders->transaction_id;

	            //$fromEmail = "no-reply@pointslive.com";

              	$body = $this->load->view('templates/guest_order_cancelled_template',$data,TRUE);

              	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
	            $mailConfig['protocol'] = "sendmail";
	            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
	            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
	            $mailConfig['smtp_pass'] = "tg1<LvPL";
	            $mailConfig['smtp_port'] = "25";
	            $mailConfig['mailtype'] = "html";

              	$this->load->library('email', $mailConfig);

              	$this->email->set_newline("\r\n");   
	            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
	            $this->email->message($body); 
	            $this->email->to($addressinfo->email);
	            $this->email->subject('Order Cancelled');

              	$send = $this->email->send();

	            if(!$send){
	                $path = __DIR__.'\Order.php function cancelOrder';
	                $line = 1369;
	                apilogger($this->email->print_debugger(),$path,$line);
	            }

			}
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to cancel order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	// Falied order by webbackend - 15-06-2018
	public function FailOrder($orderID=""){

		$orders = $this->Orders_model->get($orderID);
		$storesdata = $this->Stores_model->where('page_id',$orders->page_id)->get();
		

		$data = array(
			 'id' => $orderID,	
			 'status' => '5'
		);
		$update = $this->Orders_model->where('id',$orderID)->update($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been failed.');
		    $this->session->set_flashdata('msg', 'success');

		    if(!$storesdata){
		    
				$addressinfo = $this->db->select('*')->from('address')->where('id',$orders->shipping_address)->get()->row();
				// Email send 
	            $data['guestname'] = $addressinfo->receiver_name;
	            $data['orderno'] = $orders->transaction_id;

	            //$fromEmail = "no-reply@pointslive.com";

              	$body = $this->load->view('templates/guest_order_failed_template',$data,TRUE);

              	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
	            $mailConfig['protocol'] = "sendmail";
	            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
	            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
	            $mailConfig['smtp_pass'] = "tg1<LvPL";
	            $mailConfig['smtp_port'] = "25";
	            $mailConfig['mailtype'] = "html";

              	$this->load->library('email', $mailConfig);

              	$this->email->set_newline("\r\n");   
	            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
	            $this->email->message($body); 
	            $this->email->to($addressinfo->email);
	            $this->email->subject('Order Failed');

	            $send = $this->email->send();

	            if(!$send){
	                $path = __DIR__.'\Order.php function FailOrder';
	                $line = 1468;
	                apilogger($this->email->print_debugger(),$path,$line);
	            }
		    }
		    /*else{
		    	$pagetoken = $storesdata->access_token;
		    }*/

		    //if(!is_null($this->session->userdata('isLogin'))){
			/*$userurl = "https://graph.facebook.com/{$orders->user_id}?access_token=".$pagetoken;
	      	//echo @$userinfo->access_token;
	    	$ch = curl_init();
	      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($ch,CURLOPT_URL,$userurl);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		    $results = curl_exec($ch);
		    curl_close($ch);
		    $content = json_decode($results);
		    $username = $content->first_name.' '.$content->last_name;
		    $userID = $orders->user_id;
		    $message = "Hello *{$username}*. Your order *{$orders->transaction_id}* has been cancelled.";

		    // Send Notifcation of Order
	    	$this->pushNotificationtoUsers($username,"3",$userID,$orders->page_id,$pagetoken,$message);

	    	// Cancell reasons message send to users
	    	$message = "We are sorry this happened. Can you tell us why the order was cancelled ?";

	    	$this->pushOrderReasontoUsers($userID,$orders->page_id,$pagetoken,$message);*/
			//}
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to failed order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	function updateInventory(){
		$this->load->model('Products_model');

		$validatetoken = Admin::acommerceValidateAPI(); // Get Access Token of Logistic API

        if(!is_null($validatetoken)){
          $token = $validatetoken['token']['token_id'];

        }
     	/*$data = $this->db->query('SELECT max(inventory_last_updated) FROM products')->row();*/
     	$data = $this->db->from('products')->order_by('inventory_last_updated', 'desc')->get()->row();
     	$last_updated_time = $data->inventory_time;
     	if(!empty($last_updated_time)){

		$userurl = "https://fulfillment.api.acommercedev.com/channel/demoth1/allocation/merchant/1163?since=$last_updated_time";
		
		}else{

		$userurl = "https://fulfillment.api.acommercedev.com/channel/demoth1/allocation/merchant/1163?since=2018-01-19T07:07:14.479Z";

		}
		$ch = curl_init();

      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch,CURLOPT_URL,$userurl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$token}",'Content-Type:application/json']);
	    $results = curl_exec($ch);
	   
	    $shipdata = json_decode($results);
			  
			if(count($shipdata) > 0){

		    foreach ($shipdata as $rows) {
					$sku = $rows->sku;
					$product = $this->Products_model->get(['product_sku'=>$sku]);
					
					$reserved_qty = reservedQty($product->product_id);
				
					if(empty($reserved_qty)){
						$reserved = 0;
					}else{

						$reserved = $reserved_qty;
					}
					
					$new_qty = $rows->qty;
				
					$total_qty = $reserved + $new_qty;
					
					$inventory_time = $rows->updatedTime;

					$now = new DateTime($inventory_time);		

					$last_updated = $now->format('Y-m-d H:i:s');

					$update_data = ['total_qty'=>$total_qty,'inventory_time'=>$inventory_time,'inventory_last_updated'=>$last_updated];
					$this->Products_model->where('product_sku',$sku)->update($update_data);
					$update_data = '';
				

		    }  // end foreach
		}  // end if
	}

	// Print pdf invoice of orders
	public function printInvoice($orderID=""){
		
		$caddress = "";
		$shipcost = 0;
	    $subtotal = 0;
	    $grandtotal = 0;
	    $products = "";
	    $discount = "";
	    $price = 0;
	    $discamount = 0;
	   //number_format($número, 2, '.', '');
		$orders = orderlists($orderID);

		$userinfo = admin_info($this->session->userdata('adminId'));
		$botinfo = $this->db->select('offer_days')->from('chatbots')->where('created_by',$this->session->userdata('adminId'))->get()->row();
		$name = ($orders[0]->receiver_name !="") ? $orders[0]->receiver_name : facebook_username($orders[0]->user_id,$orders[0]->page_id);

		if($botinfo){
			$returndays = !empty($botinfo->offer_days) ? implode(",",unserialize($botinfo->offer_days)) : "0";
		}else{
			$returndays = 0;
		}


		$discounts = $this->db->select('*')->from('coupon_redemptions')->where('order_id',$orders[0]->transaction_id)->get()->row();

		if(empty($orders[0]->products)){

	      $packgaes = $this->db->select('*')
	                  ->from('shipping_packages')
	                  ->where('transaction_id',$orders[0]->transaction_id)
	                  ->get()->row();
	      $subtotal = $subtotal + $orders[0]->order_amount;
	      $vat = $subtotal * 5 / 100;

	      $products.="<tr><td style=\"text-align: left;\">{$packgaes->package_type}</td><td style=\"text-align: center;\">1</td><td style=\"text-align: right;\">".number_format($orders[0]->order_amount, 2, '.', '')."</td></tr>";
	   
	      }
	      else{
	          foreach ($orders[0]->products as $key => $product) {
	          	$shipcost = $shipcost + $product->shipping_cost;
	          	if($product->sale_price > 0){
	          		$price = $product->sale_price;
	          	}
	          	else{
	          		$price = $product->product_price;
	          	}
	          	$subtotal = $subtotal + ($price * $product->ordered_qty);
	          	$vat = $subtotal * 5 / 100;	
	          	$products.="<tr><td style=\"text-align: left;\">{$product->product_name}</td><td style=\"text-align: center;\">".$product->ordered_qty."</td><td style=\"text-align: right;\">".number_format($price, 2, '.', '')."</td></tr>";
	        }
	     }

	     if($discounts){
	     	$discount.="<td colspan=\"2\" class=\"border-none\">Discount</td>
	            <td class=\"border-none\">".number_format($discounts->total_discount,2,'.','')."</td>";
	        $discamount = $discounts->total_discount;    
	     }
	     else{
	     	$discount.="<td colspan=\"2\" class=\"border-none\">Discount</td>
	            <td class=\"border-none\">".number_format(0,2,'.','')."</td>";
	        $discamount = 0; 
	     }

		 $pages = getFacebookpagetoken($orders[0]->page_id);
		 if(count($pages) > 0):
		  $url = "https://graph.facebook.com/{$orders[0]->page_id}?fields=single_line_address,location&access_token=".$pages[0]->access_token;

		  $ch = curl_init();
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	      curl_setopt($ch,CURLOPT_URL,$url);
	      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	      curl_exec($ch);
          $content = curl_exec($ch);
          $result = json_decode($content);
	      curl_close($ch);

	      $page = $pages[0]->page_name;
	      $address = $result->single_line_address;

	     else :
	       $page = "";	
	       $address = "";
	     endif;
	     /* echo '<pre>';
	      print_r($result);exit;*/

		//ob_clean();		
		$this->load->library('m_pdf');

		$this->m_pdf->pdf->autoScriptToLang = true;
		$this->m_pdf->pdf->autoLangToFont = true;

        $my_html="
        	<!DOCTYPE html>
        	<html lang=\"en\">
 			<head>
		    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
		    <!-- Meta, title, CSS, favicons, etc. -->
		    <meta charset=\"utf-8\">

		    <style>
			.invoice-table {border-collapse: collapse;}
		    .invoice-table td{text-align: left;
		    border: 2px solid grey;
		    border-collapse: collapse;
		    padding: 8px 10px;}
		    .invoice-table th{
		        text-align: center;
		        padding: 8px;
		        border: 2px solid grey;
		    }
		    .invoice-table .border-none {
		        border: none;
		        text-align: right;
		        border-right: 2px solid grey;
		        
		    }
		   .invoice-table .border-none.border-bottom {border-bottom: 2px solid grey;border-top: 2px solid grey;}
		    .text-bold{
		        font-weight: bold;
		        font-size: 20px;
		    }
		    .terms{text-align: left;width: 21cm;margin: 0 auto;padding-top: 180px;}
    		.terms h4{margin-bottom: 0px;}
    		span {
			  content: \"\0E3F\";
			}
			</style>
			</head>
		  <table style=\"width: 21cm;  margin: 0 auto;\">
		  	<tr>
	            <td colspan=\"2\" style=\"text-align: center; line-height: normal;\">
	                <h2>Invoice</h2>
	            </td>
	            <td></td>
        	</tr>
		    <tr>
		        <td>
		            <h2>".$page."</h2>
		        </td>
		    </tr>
		   <tr>
		        <td>".$address."</td>
		        <td style=\"width: 50%\"></td>
		    </tr>
		</table>
		<table>
		    <tr>
		        <td style=\"height: 50px;\"></td>
		    </tr>
		</table>
		<table style=\"width: 21cm;  margin: 0 auto;\">
		<tbody>
		<tr>
	        <td style=\"font-weight: bold;\"><h3 style=\"margin:5px 0;\">Bill To</h3></td>
	        <td></td>
	    </tr>
	    <tr>
            <td style=\"width:65%\">
                <p style=\"margin:5px 0;\">
                    ".$name."
                </p>
                <p style=\"margin:5px 0;\">
                    ".$orders[0]->shipping_address."
                </p>
                <p style=\"margin:5px 0;\">
                    ".$orders[0]->district.", ".$orders[0]->country."
                </p>
                <p style=\"margin:5px 0;\">
                    ".$orders[0]->postcode."
                </p>
            </td>
            <td style=\"vertical-align: top; padding-left:20px; width: 35%\">
                <table style=\"width:100%\">
                	<tbody>
                    <tr>
                        <td>
                            <p style=\"font-weight: bold; margin:0\">Invoice #</p>
                        </td>
                        <td style=\"text-align: right;\">
                            ".$orders[0]->transaction_id."
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p style=\"font-weight: bold; margin:0\">Invoice Date</p>
                        </td>
                        <td style=\"text-align: right;\">
                            ".date('d/m/Y',strtotime($orders[0]->created_date))."
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-weight: bold; margin:0\">Payment Method</p>
                        </td>
                        <td style=\"text-align: right;\">
                            ".$orders[0]->payment_method."
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
		</table>
		<table>
		    <tr>
		        <td style=\"height: 30px;\"></td>
		    </tr>
		</table>
		<div style=\"width: 21cm;  margin: 0 auto;\">
	     <table class=\"invoice-table\" style=\"width: 100%;\">
	           <tr style=\"background-color: #f7f7f7;\">
	           	<th>DESCRIPTION</th>
	           	<th>QUANTITY</th>
	        	<th>AMOUNT</th>
	          </tr>
	         ".$products."
	        <tr>
            <td colspan=\"2\" class=\"border-none\">Subtotal</td>
	            <td  class=\"border-none\">
	                <div style=\"\">".number_format($subtotal,2,'.','')."</div>
	            </td>
	        </tr>
	        <tr>
	            <td colspan=\"2\" class=\"border-none\">Shipping</td>
	            <td  class=\"border-none\">".number_format($shipcost,2,'.','')."</td>
	        </tr>
	        <tr>
	        ".$discount."   
	        </tr>
	        <tr>
	            <td colspan=\"2\" class=\"border-none text-bold\">VAT</td>
	            <td  class=\"border-none\">".number_format($subtotal * 5 / 100,2,'.','')."</td>
	        </tr>
	        <tr>
	            <td colspan=\"2\" class=\"border-none text-bold\">TOTAL</td>
	            <td  class=\"border-none border-bottom text-bold\" style=\"border-bottom:2px solid grey;border-top:2px solid grey;\"><span style=\"font-family: DejaVu Sans; sans-serif;\">&#3647;</span>".number_format($grandtotal = ($subtotal + $shipcost + $vat) - $discamount,2,'.','')."</td>
	        </tr>
	      </table>
	     </div>
	     <div class=\"terms\">
		    <h4>Terms & Conditions</h4>
		    <p>Thank you for buying from <b>".$page."</b>. We hope you enjoy your order. Returns are accepted up to <b>".$returndays." days</b> after your purchase. If you have any enquiries, Please call us on <b>".$userinfo->admin_mobile."</b> or email us at <b>".$userinfo->admin_email."</b></p>
		</div></html>";
	    $this->m_pdf->pdf->WriteHTML($my_html);
	    $output = 'invoice-' . $orderID . '.pdf';
	    $this->m_pdf->pdf->Output("$output", 'D');
	}

	// Ajax orders list by filter

	public function ajax_list()
    {
        $list = $this->Orders_model->get_datatables();
        /*echo '<pre>';
        print_r($this->db->last_query());exit;*/
        $data = array();
        //$no = isset($_POST['start']) ? $_POST['start'] : 0;
        foreach ($list as $orders) {
            //$no++;
        	$shippingtypes = array();
            $shiptypes = $this->Orders_model->get_shippingtypes($orders->transaction_id);

			for ($k = 0; $k < count($shiptypes); $k++) {
				if ($shiptypes[$k]->order_id == $orders->transaction_id) {
					$shippingtypes[] = $shiptypes[$k]->shipping_type;
				}
			}

			$orders->shipping_type = ucwords(implode(",", str_replace("both", "Delivery & Offilne", $shippingtypes)));

			if(is_null($this->session->userdata('isLogin'))):
				$url = base_url()."order/adminshipOrder/{$orders->id}";
			else :
				$url = base_url()."order/dispatchOrder/{$orders->id}";
			endif;	

			if($orders->store_type == "pickup"){
				$btn =''; //'<label class="qrcode-text-btn scanner"><input onchange="openQRCamera(this);" tabindex="-1" type="file"></label>';
			}
			else{
				$btn = '<a href="'.$url.'" class="btn btn-sm btn-primary">Dispatch</a>';
			}

        	if($orders->status == "0"){
        		$status = '<span class="label label-info">Processing</span>';
        		$button = '<a href="'.base_url("order/cancelOrder/{$orders->id}").'" class="btn btn-sm btn-danger">Cancel</a>'.$btn.'<a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}
        	else if($orders->status == "1"){
        		$status = '<span class="label label-primary">Dispatched</span>';
        		$button = '<a href="'.base_url("order/orderComplete/{$orders->id}").'" class="btn btn-sm btn-success">Complete</a><a href="'.base_url("order/FailOrder/{$orders->id}").'" class="btn btn-sm btn-danger">Failed</a><a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}
        	else if($orders->status == "2"){
        		$status = '<span class="label label-success">Delivered</span>';
        		$button = '<a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}
        	else if($orders->status == "3"){
        		$status = '<span class="label label-danger">Canceled</span>';
        		$button = '<a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}
        	else if($orders->status == "5"){
        		$status = '<span class="label order-status-fail">Failed</span>';
        		$button = '<a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}
        	else if($orders->status == "6"){
        		$status = '<span class="label label-success">Completed</span>';
        		$button = '<a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}
        	else if($orders->status == "7"){
        		$status = '<span class="label order-status-fail">Confirmed Failed</span>';
        		$button = '<a href="'.base_url("order/viewOrder/{$orders->transaction_id}").'" class="btn btn-sm btn-info">View</a>';
        	}

            $row = array();
            $row[] = $orders->transaction_id;
            $row[] = ($orders->receiver_name !="") ? $orders->receiver_name : $orders->facebook_username;
            $row[] = ($orders->store_type == 'pickup') ? "offline" : $orders->store_type;//$orders->shipping_type;
            $row[] = $orders->payment_method;
            $row[] = $orders->created_date;
            $row[] = $orders->order_amount;
            $row[] = $status;

            $row[] = $button;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Orders_model->count_all(),
                        "recordsFiltered" => $this->Orders_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function autoCancelOrder(){
    	$fees = 0;
    	$config = emailconfig();
        $this->load->library('email',$config);

       // $cancelledorders = orderlistsprocess("");

		$this->db->where("created_date < DATE_SUB(CURDATE(), INTERVAL 60 DAY)");
		$this->db->where('status','0');
		$this->db->where('auto_cancelled != ','1');		
		$data = $this->Orders_model->update(['auto_cancelled'=>'1']);
		
		$cancelledorders = orderlistsprocess("");
		$fromEmail = "no-reply@pointslive.com";

		if($cancelledorders){
			foreach($cancelledorders as $key => $value) {
				// 5% auto cancelled fees for vendor and pay to akin - 08-08-2018
				$this->db->select('*');
				$this->db->from('commission_fees');
				//$this->db->join('orders','orders.id = commission_fees.order_id');
				$this->db->where('order_id',$value->id);
				//$this->db->where('orders.auto_cancelled','1');
				$query = $this->db->get()->row();
				//var_dump($query);exit;
				if(count($query) > 0){
					$fees = ($value->order_amount * 5) / 100;

					$commissiondata = array(
						"order_id"	=> $value->id,
						"akin_commission"	=> '0',
		                "auto_cancelled_fees" => $fees,
		                "transaction_type"	=> "Auto Cancel Fee",
		                "seller_id"	=> $value->created_by,
		                "payment_id"	=> $query->payment_id
	               	);
					$this->db->insert('commission_fees',$commissiondata);

					/*$commissiondata = array(
		                "auto_cancelled_fees" => $fees
	               	);
	               	$this->db->where('order_id', $value->id);
					$this->db->update('commission_fees', $commissiondata); */
				}
				//END

				/*$orderno = $value->transaction_id;
				
					09-08-2018 facebook stop chatbot message
				$names = facebook_username($value->user_id,$value->page_id);
		
				$pages = getFacebookpagetoken($value->page_id);
		
				$message_to_reply_visit = "Hi {$names}. Sorry your order {$orderno} has been cancelled because {$pages[0]->page_name} could not process it on time.";
	
			        $response1 = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply_visit]
			        ];
	            //exit;
	            	$ch = curl_init("https://graph.facebook.com/v2.10/me/messages?access_token=".$pages[0]->access_token);  
			    	curl_setopt($ch, CURLOPT_POST, 1);
			    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    	//Attach our encoded JSON string to the POST fields.
			    	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response1));
			    	//Set the content type to application/json
			    	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			    	curl_exec($ch);
			    	//curl_close($ch);
			    	
			    	$message_to_reply_visit1 = "We will work with {$pages[0]->page_name} to improve their performance in the future";
	
			        $response2 = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply_visit1]
			        ];
	            //exit;
	            	$ch1 = curl_init("https://graph.facebook.com/v2.10/me/messages?access_token=".$pages[0]->access_token);  
			    	curl_setopt($ch1, CURLOPT_POST, 1);
			    	curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
			    	//Attach our encoded JSON string to the POST fields.
			    	curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response2));
			    	//Set the content type to application/json
			    	curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			    	curl_exec($ch1); */
			    	//curl_close($ch1);

			    	$data = array(
		                'transaction'  => $value->transaction_id,
		                'amount'=> $value->order_amount,
		                'applicable_fees'=> $value->applicable_fees
		             );

			    	$address = $this->db->select('email')->from('address')->where('id',$value->shipping_address)->get()->result();

			    	$body = $this->load->view('templates/auto_cancel_template',$data,TRUE);

	              	// Send Mail usign mail()
	              	$emailMessage = implode("\r\n", array($body));

	              	$headers = array("From: ".$fromEmail,
	                  "Reply-To: ".$fromEmail,
	                  "X-Mailer: PHP/" . PHP_VERSION,
	                  "Content-Type: text/html; charset=UTF-8\r\n"
	              	);
	              	$headers = implode("\r\n", $headers);

	              	$send = mail($address[0]->email, "Payment auto cancel reminder", $emailMessage, $headers, "-f".$fromEmail);
			}
		}

	}

	//List of commission and charges to akin
	public function charges(){
		$listofcharges = $this->Orders_model->get_commissions_charges();
		$data['content'] = 'admin/orders/listofcharges';
	    $data['title'] = 'Commission and Auto Cancel Fee';
	    $data['orders'] = $listofcharges;
	    $this->load->view('admin/template', $data);
	}

	// Ajax  get all finances list - 13-08-2018
	public function finance_ajax_list()
    {	

    	$friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
     	if(date('Y-m-d') == date('Y-m-d',strtotime('friday'))){
        	$fridaydate = date('Y-m-d');
      	}else{
        	$fridaydate = $friday;
      	}
      	$nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($fridaydate))." +7 days"));
      	$duedate = date('Y-m-d', strtotime($nextbill. ' + 3 days'));

    	$this->load->model('Orders_products_model');
        $list = $this->Orders_products_model->get_datatables();

        //echo $this->db->last_query();exit;
       
        $data = array();
        $oid = array();
        $status = "";

        /*$paymentscycle = $this->db->select('*')
			->from('payment_cycles')
			->where('payment_cycle_from >=', $friday)
		    ->where('payment_cycle_to <=', $nextbill)
			->where('payment_by',$this->session->userdata('adminId'))
			->get()->result();

		foreach ($paymentscycle as $key => $value) {
			 $orders = $this->db->select('id')
		        ->from('orders')
		        ->where('created_date >=', $value->payment_cycle_from)
		        ->where('created_date <=', $value->payment_cycle_to)
		        ->where('created_by',$this->session->userdata('adminId'))
		        ->get()->result();
		}

		foreach ($orders as $key => $value) {
			$oid[] = $value->id;
		}*/

        foreach ($list as $orders) {

         	if($orders->paid_status == '0'){
         		//$status = "<span class='label label-info'>Not</span>";
         		$status = "<span class='label label-info'>Not Paid</span>";
         	}
         	else if($orders->paid_status == '1'){
         		//$status = "<span class='label order-status-fail'>Due</span>";
         		$status = "<span class='label label-success'>Paid</span>";
         	}
         	/*else if($orders->payment_status == '2'){
         		//$status = "<span class='label label-success'>Over</span>";
         		$status = "<span class='label label-primary'>Partial</span>";
         	}*/

            $row = array();
            $row[] = $orders->transaction_id;
            $row[] = date('d/m/Y',strtotime($orders->order_date));
            $row[] = $orders->order_amount;
            $row[] = ($orders->transaction_type == 'Commission') ? 'Commission' : 'Auto Cancel Fee';
            $row[] = ($orders->transaction_type == 'Auto Cancel Fee') ? $orders->auto_cancelled_fees : $orders->akin_commission;
            $row[] = !is_null($orders->paid_date) ? date('d/m/Y',strtotime($orders->paid_date)) : "-" ;
            $row[] = !is_null($orders->billing_amount) ? $orders->billing_amount : "-";
            $row[] = !is_null($orders->payment_cycle_from) ? date('jS F',strtotime($orders->payment_cycle_from)).'-'.date('jS F, Y', strtotime($orders->payment_cycle_to)) : date('jS F', strtotime($friday)).'-'.date('jS F, Y', strtotime($nextbill));
            $row[] = !empty($status) ? $status : "-";
            $data[] = $row;
        }
 
        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Orders_products_model->count_all(),
                "recordsFiltered" => $this->Orders_products_model->count_filtered(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

	// 13-08-2018 Finance for admin and vendors
	public function finances(){
		$data['content'] = 'admin/orders/finance';
	    $data['title'] = 'Finance';
	    $this->load->view('admin/template', $data);
	}

	//14-08-2018 export fiances data
	public function exportFinanceCharges(){

		$this->db->select('orders.*,orders.created_date as orderdate, commission_fees.akin_commission,commission_fees.auto_cancelled_fees,commission_fees.transaction_type,payment_cycles.*,weekly_payment_generates.payment_status as paymentstatus,weekly_payment_generates.billing_amount,weekly_payment_generates.payment_cycle_from as fromcycle,weekly_payment_generates.payment_cycle_to as tocycle');
		$this->db->from('orders');
		$this->db->join('commission_fees', 'commission_fees.order_id=orders.id');
		$this->db->join('weekly_payment_generates', 'weekly_payment_generates.id=commission_fees.payment_id','LEFT');
		$this->db->join('payment_cycles', 'payment_cycles.payment_id=weekly_payment_generates.id','LEFT');
		$this->db->group_by('commission_fees.id');
		$this->db->order_by('orders.id','desc');

		if(!empty($this->session->userdata('isLogin'))){
			$this->db->where('orders.created_by', $this->session->userdata('adminId'));
		}

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		$results = $query->result();

		$total = 0;

		$friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
     	if(date('Y-m-d') == date('Y-m-d',strtotime('friday'))){
        	$fridaydate = date('Y-m-d');
      	}else{
        	$fridaydate = $friday;
      	}
      	$nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($fridaydate))." +7 days"));
      	$duedate = date('Y-m-d', strtotime($nextbill. ' + 3 days'));

        require 'application/libraries/php-export-data.class.php';

        $exporter = new ExportDataExcel('browser', 'finances.xls');
		$exporter->initialize(); // starts streaming data to web browser
		// pass addRow() an array and it converts it to Excel XML format and sends 
		// it to the browser
		$exporter->generateHeader();
		$exporter->addRow(array("Order No", "Order Date", "Order Amount","Transaction Type","Transaction Amount","Transaction Date","Payment Cycle","Payment Status","Transaction Amount")); 
		foreach ($results as $rows) {
			
			if($rows->paymentstatus == '0'){
				$status = "Not Paid";
			}
			elseif($rows->paymentstatus == '1'){
				$status = "Paid";
			}
			elseif($rows->paymentstatus == '2'){
				$status = "Partial Paid";
			}

			$orderid = $rows->transaction_id;
			$orderdate = date('d/m/Y',strtotime($rows->orderdate));
			$totalamount = $rows->order_amount;
			$trantype = $rows->transaction_type;
			$tranamount = ($rows->transaction_type == 'Auto Cancel Fee') ? $rows->auto_cancelled_fees : $rows->akin_commission;
			$trandate= is_null($rows->payment_date) ? '-' : date('d/m/Y',strtotime($rows->payment_date));
			$paymentcycle = date('jS F',strtotime($rows->fromcycle)).'-'.date('jS F, Y', strtotime($rows->tocycle));;
			$paymentstatus = $status;
			$totaltransaction = $rows->billing_amount;
			$data = [$orderid,$orderdate,$totalamount,$trantype,$tranamount,$trandate,$paymentcycle,$paymentstatus,$totaltransaction];
			$exporter->addRow($data);	
			//$data = '';
		}
		/*$data['SS'] = 500;
		$exporter->addRow($data);*/
		$exporter->finalize(); // writes the footer, flushes remaining data to browser.
		exit();
	}

	// Order export in excel
	public function exportOrders(){

		require 'application/libraries/php-export-data.class.php';
		$oids = array();

		$this->db->select('orders.*,address.receiver_name,products.shipping_type');
		$this->db->from('orders');
		$this->db->join('orders_products', 'orders_products.order_id=orders.transaction_id','LEFT');
		$this->db->join('address', 'orders.shipping_address=address.id','LEFT');
		$this->db->join('admin', 'admin.admin_id=orders.created_by', 'LEFT');
		$this->db->join('products', 'products.product_id=orders_products.product_id', 'LEFT');
		if(!empty($this->session->userdata('isLogin'))) {
			$this->db->where('orders.created_by',$this->session->userdata('adminId'));
		}
		$this->db->order_by('orders.id','asc');
		$this->db->group_by('orders.id');

		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		$results = $query->result();

		// New items wise records get
		foreach ($results as $key => $value) {
            $oids[] = $value->transaction_id;
        }

        $productsdata = $this->db->select('products.product_id,products.product_sku,orders_products.order_id,products.product_price,products.created_by')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            //->join('admin', 'products.created_by = orders.created_by')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();
        $products = array();

        foreach ($results as $key => $value) {
            foreach ($resultsa as $key => $prod) {
                if($value->transaction_id == $prod->order_id){
                    $products[] = $prod;
                }
        }

        $value->products = @$products;
        unset($products);
        }

        $exporter = new ExportDataExcel('browser', 'orders.xls');
		$exporter->initialize(); // starts streaming data to web browser
		// pass addRow() an array and it converts it to Excel XML format and sends 
		// it to the browser
		$exporter->generateHeader();
		$exporter->addRow(array("Order No", "Username", "Order Type","Payment Type","Created Date","Grand Total","Price","Seller ID","SKU","Status")); 
		foreach ($results as $value) {

			if($value->status == '0' && $value->auto_cancelled == '0'){
				$orderstatus = "Processing";
			}
			else if($value->status == '1' && $value->auto_cancelled == '0'){
				$orderstatus = "Dispatched";
			}
			else if($value->status == '2' && $value->auto_cancelled == '0'){
				$orderstatus = "Delivered";
			}
			else if($value->status == '3' && $value->auto_cancelled == '0'){
				$orderstatus = "Cancelled";
			}
			else if($value->status == '4' && $value->auto_cancelled == '0'){
				$orderstatus = "Rejected";
			}
			else if($value->status == '5' && $value->auto_cancelled == '0'){
				$orderstatus = "Failed";
			}
			else if($value->status == '6' && $value->auto_cancelled == '0'){
				$orderstatus = "Completed";
			}
			else if($value->status == '7' && $value->auto_cancelled == '0'){
				$orderstatus = "Confirm Failed";
			}
			else if($value->auto_cancelled == '1'){
				$orderstatus = "Auto Cancelled";
			}

			if(!is_null($value->products)){
        		foreach ($value->products as $key => $val) {
        			/*$sellerinfo = $this->db->select('admin.admin_name')->from('admin')->where('admin_id',$value->created_by)->get()->row();*/
        			$orderNo = $value->transaction_id;
					$name = ($value->receiver_name !="") ? $value->receiver_name : $value->facebook_username;
					$orderType = ($value->store_type == 'pickup') ? "offline" : $value->store_type;;
					$orderMethod = $value->payment_method;
					$orderDate = date('m-d-Y',strtotime($value->created_date));
					$orderAmount = $value->order_amount;
					$orderStatus = $orderstatus;
					$itemprice = $val->product_price;
					$sellername = 'ID'.$value->created_by;
					$sku = $val->product_sku;
					$data = [$orderNo,$name,$orderType,$orderMethod,$orderDate,$orderAmount,$itemprice,$sellername,$sku,$orderstatus];
					$exporter->addRow($data);
        		}
        	}
		}
		$exporter->finalize(); // writes the footer, flushes remaining data to browser.
		exit();
	}

	// Bulk transfer auto payment using fluterwave API
	public function bulkTransferPayment(){
		
		$datapayments = array();

		$friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
      
	    $lastbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($friday))." +6 days"));
	    $paymentbilldate = date('Y-m-d',strtotime(date("Y-m-d", strtotime($friday))." +7 days"));

	    $orders = $this->db->select('*')
	    	->from('weekly_payment_generates')
	        ->where('weekly_payment_generates.payment_cycle_from >=', $friday)
	        ->where('weekly_payment_generates.payment_cycle_to <=', $paymentbilldate)
	        ->where_in('payment_status',array('0','2'))
	        ->get()->result();

		$bulkdata = array();

		if(count($orders) > 0){
		  	foreach ($orders as $value) {

		  		$bankinfo = $this->db->select('bank_info')->from('chatbots')->where('created_by',$value->seller_id)->get()->result();
		  		$sellerinfo = admin_info($value->seller_id);
		  		
		  		if($value->billing_amount > 0){ 

		  			if($value->payment_status == '2'){
				  		$this->db->where('weekly_payment_id',$value->id);
				  		$this->db->where('payment_by', $value->seller_id);
						$query = $this->db->get('payment_cycles');	
							
						if($query->num_rows() > 0){

							$checkweekrecords = $this->db->select("SUM(payment_bill_amount) as Paid")->from('payment_cycles')
			        	    ->where('payment_cycle_from =', $friday)
		                    ->where('payment_cycle_to =', $paymentbilldate)
		                    ->where('payment_by',$value->seller_id)
		                    ->where('weekly_payment_id',$value->id)
		                    //->where_in('where_in',array('0'))
		                    ->get()->row();

		                    if(is_null($checkweekrecords->Paid)){
		                    	$paidamount = 0;
		                    }
		                    else{
		                    	$paidamount = $checkweekrecords->Paid; 
		                    }

							$paymentlists = $query->result();
							
							foreach ($paymentlists as $key => $payment) {
								
								$billpayment = ($value->billing_amount - $paidamount); // $payment->payment_bill_amount
								foreach ($bankinfo as $key => $bk) {
									$banks = unserialize($bk->bank_info);
									$bulkdata = array(
									"account_bank"=>"{$banks['bank_name']}",
							        "account_number"=> "{$banks['account_number']}",
							        "amount"=>$billpayment,
							        "seckey"	=> "FLWSECK-98b18dd35f2a56a5a628cefbe03ee6ee-X",
							        "narration"=>"Transfer payment of {$sellerinfo->admin_name}",
							        "currency"=>"NGN",
							        "reference"=> uniqid()
									);

									if ( strtotime(date('Y-m-d')) == strtotime($paymentbilldate) ){

										// Payment initiate of seller
										$payments = $this->IntitatePayment($bulkdata);

										// Fetch transaction status
										$checkstatus = $this->FetchTransaction($payments->data->id);

										$dbrecord = array(
											"payment_cycle_from" => $friday,
											"payment_cycle_to"	 => $paymentbilldate,
											"payment_bill_amount" => $billpayment,
											//"payment_type"	=> "partial",
											"payment_by"	=> $payment->payment_by,		
										);

										if($checkstatus->transfers[0]->status == "FAILED"){
											// Error
									    	$path = __DIR__.'\Order.php function bulkTransferPayment';
									        $line = 2261;
									        apilogger($checkstatus->transfers[0]->complete_message,$path,$line);

											//$dbrecord['payment_status'] = '3';	
											//$this->db->insert('payment_cycles',$dbrecord);
										}
										else{
											$dbrecord['payment_status'] = '0';

											$this->db->insert('payment_cycles',$dbrecord);

											// Update weekly payment status to paid
											$this->db->where('weekly_payment_generates.payment_cycle_from =', $friday);
		                                    $this->db->where('weekly_payment_generates.payment_cycle_to =', $paymentbilldate);
											$this->db->where('id',$value->id);
											$this->db->update('weekly_payment_generates',array("payment_status" => '1'));
										}
									}
								}

							}
						}
		  			}
					else{
						if(strtotime(date('Y-m-d')) == strtotime($paymentbilldate)){
							foreach ($bankinfo as $key => $bk) {
								$banks = unserialize($bk->bank_info);
								$bulkdata = array(
									"account_bank"=>"{$banks['bank_name']}",
							        "account_number"=> "{$banks['account_number']}",
							        "amount"=>$value->billing_amount,
							        "seckey"	=> "FLWSECK-98b18dd35f2a56a5a628cefbe03ee6ee-X",
							        "narration"=>"Transfer payment of {$sellerinfo->admin_name}",
							        "currency"=>"NGN",
							        "reference"=> uniqid()
								);

								// Payment initiate of seller
								$payments = $this->IntitatePayment($bulkdata);
								//var_dump($payments);
								// Fetch transaction status
								$checkstatus = $this->FetchTransaction($payments->data->id);

								$dbrecord = array(
									"weekly_payment_id"  => $value->id,
									"payment_cycle_from" => $friday,
									"payment_cycle_to"	 => $paymentbilldate,
									"payment_bill_amount" => $value->billing_amount,
									//"payment_type"	=> "full",
									"payment_by"	=> $value->seller_id,		
								);

								if($checkstatus->transfers[0]->status == "FAILED"){
									// Error
							    	$path = __DIR__.'\Order.php function bulkTransferPayment';
							        $line = 2261;
							        apilogger($checkstatus->transfers[0]->complete_message,$path,$line);
								    
									//$dbrecord['payment_status'] = '3';	
									//$this->db->insert('payment_cycles',$dbrecord);
								}
								else{
									$dbrecord['payment_status'] = '0';

									$this->db->insert('payment_cycles',$dbrecord); // payment data

									// Update weekly payment status to paid
									$this->db->where('weekly_payment_generates.payment_cycle_from =', $friday);
                                    $this->db->where('weekly_payment_generates.payment_cycle_to =', $paymentbilldate);
									$this->db->where('id',$value->id);
									$this->db->update('weekly_payment_generates',array("payment_status" => '1'));
								}
							}
						}	
					}
		  		}
		  	}
		}
	}

	// 24-08-2018 - Auto billing generates of seller weekly
	public function AutobillGenerate(){
		 // Total orders shown to admin and vendors in payment cycle
	      $friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
	      if(date('Y-m-d') == date('Y-m-d',strtotime('friday'))){
	        $fridaydate = date('Y-m-d');
	      }else{
	        $fridaydate = $friday;
	      }
	      //$fridaynextday = date( 'Y-m-d', strtotime($friday. ' + 1 days') );
	      $nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($fridaydate))." +7 days"));
	      $duedate = date('Y-m-d', strtotime($nextbill. ' + 3 days'));
	      $commission = 0;
	      $autocancelled = 0;
	      $totalorderamount = 0;

	      $exists = 0;

	      $paymentdata = array();

	      $orders = $this->db->select('GROUP_CONCAT(orders.id) as orderid,SUM(orders.order_amount) as Total_due,SUM(commission_fees.akin_commission) as Comm,SUM(commission_fees.auto_cancelled_fees) as cancelled_fees,admin.admin_id')
	        ->from('orders')
	        ->join('commission_fees','orders.id=commission_fees.order_id','LEFT')
	        ->join('admin','orders.created_by=admin.admin_id','LEFT')
	        ->where_in('admin.login_with',array('seller','twitter','instagram'))
	        ->where('orders.created_date >=', $fridaydate)
	        ->where('orders.created_date <=', $nextbill)
	        ->group_by('orders.created_by')
	        ->get()->result();
	        //echo $this->db->last_query();exit;
	        if(count($orders) > 0){
		        foreach ($orders as $key => $ord) {

		        	// Cycle dates get ordersID and update with payment IDs	
           			$ordersIDS = explode(",", $ord->orderid);	

		        	$totalorderamount = ($ord->Total_due) - ($ord->Comm - $ord->cancelled_fees);

		        	$checkrecords = $this->db->select("*")->from('weekly_payment_generates')
		        	    ->where('payment_cycle_from =', $fridaydate)
	                    ->where('payment_cycle_to =', $nextbill)
	                    ->where('seller_id',$ord->admin_id)
	                    ->order_by('id','desc')
	                    //->where_in('where_in',array('0'))
	                    ->get()->row();

	               	if(count($checkrecords) > 0){

	               		$checkweekrecords = $this->db->select("SUM(payment_cycles.payment_bill_amount) as Paid")->from('payment_cycles')
	               			->join('weekly_payment_generates','weekly_payment_generates.id = payment_cycles.weekly_payment_id','LEFT')
			        	    ->where('payment_cycles.payment_cycle_from =', $fridaydate)
		                    ->where('payment_cycles.payment_cycle_to =', $nextbill)
		                    ->where('payment_cycles.payment_by',$ord->admin_id)
		                    //->where('weekly_payment_id',$checkrecords->id)
		                    //->where_in('where_in',array('0'))
		                    ->get()->row();

	               		if($checkrecords->payment_status == '1'){
	               			$exists = 0;
	               			$paymentdatas[] = array(
			              		"seller_id" => $ord->admin_id,
			              		"payment_cycle_from" => $fridaydate,
			              		"payment_cycle_to"	=> $nextbill,
			              		"billing_amount"	=> ($totalorderamount - $checkweekrecords->Paid)
		              		);

		              		// Save in table
		        			$this->db->insert_batch('weekly_payment_generates', $paymentdatas);
	               		}
	               		else if($checkrecords->payment_status == '2'){
	               			$exists = 1;
		               		$paymentdata[] = array(
			              		"id" => $checkrecords->id,
			              		"seller_id" => $ord->admin_id,
			              		"payment_cycle_from" => $fridaydate,
			              		"payment_cycle_to"	=> $nextbill,
			              		"billing_amount"	=> ($totalorderamount - $checkweekrecords->Paid)
			              	);
	               		}
	               		else{

	               			if(!is_null($checkweekrecords->Paid)){
	               				$exists = 1;
			               		$paymentdata[] = array(
				              		"id" => $checkrecords->id,
				              		"seller_id" => $ord->admin_id,
				              		"payment_cycle_from" => $fridaydate,
				              		"payment_cycle_to"	=> $nextbill,
				              		"billing_amount"	=> ($totalorderamount - @$checkweekrecords->Paid)
				              	);
	               			}
	               			else{
	               				$exists = 1;
	               				$paymentdata[] = array(
				              		"id" => $checkrecords->id,
				              		"seller_id" => $ord->admin_id,
				              		"payment_cycle_from" => $fridaydate,
				              		"payment_cycle_to"	=> $nextbill,
				              		"billing_amount"	=> ($totalorderamount - @$checkweekrecords->Paid)
				              	);

	               			}
		               		/*$exists = 1;
		               		$paymentdata[] = array(
			              		"id" => $checkrecords->id,
			              		"seller_id" => $ord->admin_id,
			              		"payment_cycle_from" => $fridaydate,
			              		"payment_cycle_to"	=> $nextbill,
			              		"billing_amount"	=> ($totalorderamount - @$checkweekrecords->Paid)
			              	);*/
	               		}

	               		// Update paymentid in commission table	
               			foreach ($ordersIDS as $key => $value) {
               				$checkweeklyid = $this->db->select('*')->from('commission_fees')->where('order_id',$value)->where('seller_id',$checkrecords->seller_id)->get()->row();

               				if($checkweeklyid->payment_id == '0'){

	               				$this->db->where('commission_fees.order_id', $checkweeklyid->order_id);
	               				$this->db->where('commission_fees.seller_id', $checkweeklyid->seller_id);
								$this->db->update('commission_fees',array("payment_id" => $checkrecords->id));
               				}
               			}
	               	}
	               	else{
	               		$exists = 0;
		              	$paymentdatas[] = array(
		              		"seller_id" => $ord->admin_id,
		              		"payment_cycle_from" => $fridaydate,
		              		"payment_cycle_to"	=> $nextbill,
		              		"billing_amount"	=> $totalorderamount
		              	);
		              	// Save in table
		        		$this->db->insert_batch('weekly_payment_generates', $paymentdatas); 
	               	}
	              	 
	            }

	            if($exists == 1){
	            	$this->db->update_batch('weekly_payment_generates', $paymentdata,'id');
	            }
	           /* else{
		            // Save in table
		        	$this->db->insert_batch('weekly_payment_generates', $paymentdata); 
	            }*/
			}
	}

	// Make seller payment via flutterwave API of initiate payment
	public function IntitatePayment($bulkdata){
		$url = "https://api.ravepay.co/v2/gpx/transfers/create";
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($bulkdata));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	  	$content = curl_exec($ch);
       	$contents = json_decode($content);
    	$headerinfo = curl_getinfo($ch);
        curl_close($ch);
        return $contents;
	}

	// Check seller payment success or not using fetch transaction API
	public function FetchTransaction($transaction_id){

		$url = "https://api.ravepay.co/v2/gpx/transfers?seckey=FLWSECK-98b18dd35f2a56a5a628cefbe03ee6ee-X&id={$transaction_id}";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	  	$content = curl_exec($ch);
       	$contents = json_decode($content);
    	$headerinfo = curl_getinfo($ch);
        curl_close($ch);

        return $contents->data;

	}

	// 24-08-2018 Seller payments for
	public function sellerbilling(){
		$data['content'] = 'admin/orders/sellerpayments';
	    $data['title'] = 'Seller Payments';
	    $this->load->view('admin/template', $data);
	}

	// Seller payments tab for admin section ajax based
	public function SellerPaymentLists(){

		$paymentslist = $this->Stores_model->get_datatables();		
		$data = array();
        //$no = isset($_POST['start']) ? $_POST['start'] : 0;
        $status = '';
        foreach ($paymentslist as $payment) {

        	if($payment->payment_status == '0'){
        		$status = "Unpaid";
        		$paidamount = '-';
        		$unpaidamount = $payment->billing_amount;
        		$button = '<div class="center"><button data-toggle="modal" data-target="#requestform" data-id='.$payment->id.' data-amount='.$unpaidamount.' data-seller="'.$payment->admin_name.'" class="btn btn-primary btn-sm center-block">Pay Now</button></div>';
        	}
        	else if($payment->payment_status == '1'){
        		$status = "Paid";
        		$paidamount = $payment->billing_amount;
        		$unpaidamount = '-';
        		$button = '<button disabled>Pay Now</button>';
        	}
        	else if($payment->payment_status == '2'){
        		$status = "Partial";
        		$paidamount = $payment->Totalpaid;
        		$unpaidamount = ($payment->billing_amount - $paidamount);
        		$button = '<div class="center"><button data-toggle="modal" data-target="#requestform" data-id='.$payment->id.' data-amount='.$unpaidamount.' data-seller="'.$payment->admin_name.'" class="btn btn-primary btn-sm center-block">Pay Now</button></div>';
        	}
           
            $row = array();
            $row[] = $payment->admin_name;
            $row[] = date('jS F',strtotime($payment->payment_cycle_from)).' - '.date('jS F', strtotime($payment->payment_cycle_to));
            $row[] = $payment->billing_amount; //ucfirst($orders->shipping_type);
            $row[] = ($payment->auto_payment == '1') ? "Yes" : "No";
            $row[] = $status;
            $row[] = $paidamount;
            $row[] = $unpaidamount;
            $row[] = $button;

            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Stores_model->count_all(),
                        "recordsFiltered" => $this->Stores_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
	}

	// Admin make payment to seller ajax based
	public function ProcessPayment(){
		//exit;
		if($this->input->post()){
			$payID = trim($this->input->post('payment'));
			$amount = trim($this->input->post('amount'));

			$paymentdetails = $this->db->select('*')
		    	->from('weekly_payment_generates')
		        ->where('weekly_payment_generates.id',$payID)
		        ->where_in('payment_status',array('0','2'))
		        ->get()->row();

		    $sellerDetails = admin_info($paymentdetails->seller_id);

		   	$sellerinfo= $this->db->select('bank_info')->from('chatbots')->where('created_by',"$paymentdetails->seller_id")->get()->row();

		   	$checkpaymentsexists = $this->db->select('*')->from('payment_cycles')->where('weekly_payment_id',$payID)->get(); // Check payment done or not

		   	switch ($sellerDetails->country){
		   		case 'thailand':

		   			if(is_null($sellerinfo))
				   	{
				   		echo json_encode(array('status' => FALSE,'msg' => '* Payment details not found for omise'));
				   		exit;
				   	}
				   	else if(is_null($sellerinfo->public_key) || is_null($sellerinfo->secret_key)){
				   		echo json_encode(array('status' => FALSE,'msg' => '* Public and Secret keys are required'));
				   		exit;
				   	}
				   	else{
				   		$paymentdata = array('amount' => $amount);
				   		//var_dump(count($checkpaymentsexists->Totalpaid));exit;
				   		$omisepayments = $this->OmisePayment($paymentdata,$sellerinfo->secret_key); // Redirect to omise API
				   		
				   		if($omisepayments['object'] == 'error'){
				   			echo json_encode(array('status' => FALSE,'msg' => $omisepayments['message']));
				   			exit;
				   		}
				   		else{
				   			$dbrecord = array(
								"weekly_payment_id"  => $paymentdetails->id,
								"payment_cycle_from" => $paymentdetails->payment_cycle_from,
								"payment_cycle_to"	 => $paymentdetails->payment_cycle_to,
								"payment_bill_amount"=> $amount,
								"payment_by"		 => $paymentdetails->seller_id,		
							);
					
				   			// Check if payment has done or not
				   			if($checkpaymentsexists->num_rows() == 0){
				   				
				   				$dbrecord['payment_status'] = '0';
								$this->db->insert('payment_cycles',$dbrecord); // payment data stored

								if($amount < $paymentdetails->billing_amount){
									// Update weekly payment status to paid
									$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
				                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
									$this->db->where('seller_id',$paymentdetails->seller_id);
									$this->db->update('weekly_payment_generates',array("payment_status" => '2'));
					   			}
				   				else{
									// Update weekly payment status to paid
									$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
				                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
									$this->db->where('seller_id',$paymentdetails->seller_id);
									$this->db->update('weekly_payment_generates',array("payment_status" => '1')); 
				   				}
					   			echo json_encode(array("status" => TRUE,'msg' => 'Payment successfully done.'));
					   			exit;
				   			}
				   			else{
				   				if($paymentdetails->payment_status == '2'){

				   					$dbrecord['payment_status'] = '0';
									$lastID = $this->db->insert('payment_cycles',$dbrecord);

									$updatedrecord = $this->db->select('SUM(payment_bill_amount) as Totalpaid')->from('payment_cycles')->where('weekly_payment_id',$payID)->get()->row();
				   					
				   					$pendingamount = ($paymentdetails->billing_amount) - ($updatedrecord->Totalpaid); // Pending payment
					   			
					   				if($pendingamount > 0){
										// Update weekly payment status to paid
										$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
					                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
										$this->db->where('seller_id',$paymentdetails->seller_id);
										$this->db->update('weekly_payment_generates',array("payment_status" => '2'));
					   				}
					   				else{
										// Update weekly payment status to paid
										$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
					                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
										$this->db->where('seller_id',$paymentdetails->seller_id);
										$this->db->update('weekly_payment_generates',array("payment_status" => '1'));
					   				}
					   				echo json_encode(array("status" => TRUE,'msg' => 'Payment successfully done.'));	
					   			}
				   			}
				   		}
				   	}
		   		break;

		   		case 'nigeria':

				   	if(is_null($sellerinfo)){
				   		echo json_encode(array('status' => FALSE,'msg' => '* Bank details not found'));
				   		exit;
				   	}
				   	else
				   	{
				   		$banks = unserialize($sellerinfo->bank_info);
				   		$bulkdata = array(
							"account_bank"	=> "{$banks['bank_name']}",
					        "account_number"=> "{$banks['account_number']}",
					        "amount"		=> $amount,
					        "seckey"		=> "FLWSECK-98b18dd35f2a56a5a628cefbe03ee6ee-X",
					        "narration"		=> "Transfer payment of {$sellerDetails->admin_name}",
					        "currency"		=> "NGN",
					        "reference"		=> uniqid()
						);
				   		//var_dump($checkpaymentsexists->num_rows());exit;
				   		// Payment initiate of seller
						$payments = $this->IntitatePayment($bulkdata);

						if($payments->status == 'error'){
							echo json_encode(array("status" => FALSE,'msg' => $payments->message));
						}
						else{
							// Fetch transaction status
							$checkstatus = $this->FetchTransaction($payments->data->id);

							$dbrecord = array(
								"weekly_payment_id"  => $paymentdetails->id,
								"payment_cycle_from" => $paymentdetails->payment_cycle_from,
								"payment_cycle_to"	 => $paymentdetails->payment_cycle_to,
								"payment_bill_amount"=> $amount,
								"payment_by"		 => $paymentdetails->seller_id,		
							);

					   		if($checkpaymentsexists->num_rows() == 0){ // No payment record found in table
					   			// Amount less than actual amount	
					   			if($checkstatus->transfers[0]->status != "FAILED"){

				   					$dbrecord['payment_status'] = '0';
									$this->db->insert('payment_cycles',$dbrecord); // payment data stored
					   				
					   				if($amount < $paymentdetails->billing_amount){
										// Update weekly payment status to paid
										$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
					                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
										$this->db->where('seller_id',$paymentdetails->seller_id);
										$this->db->update('weekly_payment_generates',array("payment_status" => '2'));
					   				}
					   				else{
										// Update weekly payment status to paid
										$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
					                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
										$this->db->where('seller_id',$paymentdetails->seller_id);
										$this->db->update('weekly_payment_generates',array("payment_status" => '1')); 
					   				}
					   				echo json_encode(array("status" => TRUE,'msg' => 'Payment successfully done.'));
					   			}
					   			else{
					   				echo json_encode(array("status" => FALSE,'msg' => $checkstatus->transfers[0]->complete_message));
					   			}
					   		}
					   		else{ 
					   			if($paymentdetails->payment_status == '2'){
					   				if($checkstatus->transfers[0]->status != "FAILED"){

					   					$dbrecord['payment_status'] = '0';
										$lastID = $this->db->insert('payment_cycles',$dbrecord);

										$updatedrecord = $this->db->select('SUM(payment_bill_amount) as Totalpaid')->from('payment_cycles')->where('weekly_payment_id',$payID)->get()->row();
					   					
					   					$pendingamount = ($paymentdetails->billing_amount) - ($updatedrecord->Totalpaid); // Pending payment
					   			
						   				if($pendingamount > 0){
											// Update weekly payment status to paid
											$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
						                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
											$this->db->where('seller_id',$paymentdetails->seller_id);
											$this->db->update('weekly_payment_generates',array("payment_status" => '2'));
						   				}
						   				else{
											// Update weekly payment status to paid
											$this->db->where('weekly_payment_generates.payment_cycle_from =', $paymentdetails->payment_cycle_from);
						                    $this->db->where('weekly_payment_generates.payment_cycle_to =',$paymentdetails->payment_cycle_to);
											$this->db->where('seller_id',$paymentdetails->seller_id);
											$this->db->update('weekly_payment_generates',array("payment_status" => '1'));
						   				}
						   				echo json_encode(array("status" => TRUE,'msg' => 'Payment successfully done.'));
					   				}
					   				else{
					   					echo json_encode(array("status" => FALSE,'msg' => $checkstatus->transfers[0]->complete_message));
					   				}	
					   			}
					   		}
							
						}
				   	}
		   		break;

		   		default:
		   		break;
		   	}
			
		}
		exit;
	}

	// Scan Qrcode and process order
	public function Scanprocess($orderID=""){
		$checkOrder = $this->Orders_model->where('transaction_id',$orderID)->where('status' ,'0')->get();
		if(count($checkOrder) > 0){
			// Update ordere status as completed
			if($checkOrder->created_by == $this->session->userdata('adminId')){
				$this->Orders_model->update(array("shipment_date" => date("Y-m-d H:i:s"),"complete_date" => date("Y-m-d H:i:s"),"status" => '6'),$checkOrder->id);
				echo "<script language=\"javascript\">alert('Order has been marked as complete delivered');</script>";
			}
			else{
				echo "<script language=\"javascript\">alert('You are not allowed to scan this QR code');</script>";
			}
			header('Refresh:0; url= '. base_url().'order/listOrders');
		}else{
			redirect(base_url('order/listOrders'));
		}
	}

	// Omise payment made by admin - 30-08-2018
	public function OmisePayment($params,$seckey=null)
	{
	   	$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,"https://api.omise.co/transfers");
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_USERPWD, "{$seckey}:");
	    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($params));  //Post Fields
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	    $headers = [
	      'Content-Type: application/json',
	    ];

	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	    $request = curl_exec($ch);

	    $http = curl_getinfo($ch);
	    curl_close ($ch);
	    $result = json_decode($request, true);
	   	
	   	return $result;	
	}
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends MY_Controller {

	public function __construct(){
		parent::__construct();
   // $this->load->library('form_validation');
    $this->load->model('Categories_model');
    $this->load->model('Product_categories_model');
    $this->load->model('Product_images_model');
    $this->load->model('Products_model');
    $this->load->model('Address_model');
    $this->load->model('Orders_model');
    $this->load->model('Shopping_carts_model');
    $this->load->model('Orders_products_model');
    $this->load->model('Chatbots_model');
    $this->load->model('Customers_Addresses_model');
    $this->load->library('pagination');
	}

  // Show products by seller and product ID
  public function view(){ //die("D");

      if(!empty($this->session->userdata("guest"))){ 
        $guestsession = $this->session->userdata("guest");
      }
      else{
        $guestsession = $this->SetGuestSession();
      }

      $seller = decrypt($this->uri->segment(3));
      $product = decrypt($this->uri->segment(4));

      if(is_digit($this->uri->segment(3))){
        $pseller = $this->uri->segment(3);
      }
      else{
        $pseller = decrypt($this->uri->segment(3));
      }

      if(is_digit($this->uri->segment(4))) {
        $pproduct = $this->uri->segment(4);
      }
      else{
        $pproduct = decrypt($this->uri->segment(4));
      }

      $pdatas = $this->Products_model->get($pproduct);
      
      $sellerID = $pdatas->created_by;


      $pdata = viewproduct_by_seller($sellerID,$pproduct);
      $chatdetails = $this->Chatbots_model->where('created_by',$sellerID)->get();

     // var_dump($pdata);

      if($pdata || count($pdata) > 0){
        $data['content'] = 'global/product_detail';
        $data['title'] = 'Product Details';    
        $data['productInfo'] = $pdata;  
        $data['storedata'] = $chatdetails; 
        $this->load->view('global/template', $data);
      }
      else{
        show_404();
      }
  }

  // Show category by seller
  public function category(){
     /* $seller = decrypt($this->uri->segment(3));
      $find = viewcategory_by_seller($seller);*/

      if(!empty($this->session->userdata("guest"))){ 
        $guestsession = $this->session->userdata("guest");
      }
      else{
        $guestsession = $this->SetGuestSession();
      }

      if(is_digit($this->uri->segment(3))){
        $seller = $this->uri->segment(3);
      }
      else{
        $seller = decrypt($this->uri->segment(3));
      }

      $find = viewcategory_by_seller($seller);  
      $bestproducts = BestProducts($seller);
      $chatdetails = $this->Chatbots_model->where('created_by',$seller)->get();
      //if($find || count($find) > 0){
      $data['content'] = 'global/catalog';
      $data['title'] = 'Product Catalogue';    
      $data['categories'] = $find;  
      $data['bestproducts'] = $bestproducts;
      $data['storedata'] = $chatdetails;
      $this->load->view('global/template', $data);
  }

  // Show products by seller and category
  public function products(){
      /*$category = decrypt($this->uri->segment(3));
      $seller = decrypt($this->uri->segment(4));*/

      if(!empty($this->session->userdata("guest"))){ 
        $guestsession = $this->session->userdata("guest");
      }
      else{
        $guestsession = $this->SetGuestSession();
      }

      if(is_digit($this->uri->segment(3))){
        $category = $this->uri->segment(3);
      }
      else{
        $category = decrypt($this->uri->segment(3));
      }

      if(is_digit($this->uri->segment(4))){
        $seller = $this->uri->segment(4);
      }
      else{
        $seller = decrypt($this->uri->segment(4));
      }

      $order = $this->input->get('order', TRUE);
      $rate = $this->input->get('rating', TRUE);
      //$price = $this->input->get('price', TRUE);
      $minprice = $this->input->get('min-amount', TRUE);
      $maxrice = $this->input->get('max-amount', TRUE);
     // $explode = explode("-", $price);
      $filterdata = array("min" => $minprice,"max" => $maxrice);
      $search = $this->input->get('search', TRUE);

      $find = viewproduct_by_seller_category_listing($category,$seller,$order,$filterdata,$search,$rate);
     
      $data['content'] = 'global/category_product';
      $data['title'] = 'View Products';    
      $data['categories'] = $find;  
      $this->load->view('global/template', $data);
    
  }

  // Show cart page
  public function manage_cart($sellerID=""){

    if(!empty($this->session->userdata("guest"))){ 
      $guestsession = $this->session->userdata("guest");
    }
    else{
      $guestsession = $this->SetGuestSession();
    }

    $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $guestsession;

    $cartdata = productcarts_by_userid($user);
    //var_dump($cartdata); 
    $data['content'] = 'global/cart';
    $data['title'] = 'Shopping Cart';    
    $data['products'] = $cartdata; 
    $this->load->view('global/template', $data); 
  
  }

  public function SetGuestSession(){// die("ss");
    if(empty($this->session->userdata("guest"))){
      $random_unique_int = mt_rand(1200,999999999);  
      $this->session->set_userdata("guest", $random_unique_int);
      return $this->session->userdata("guest");
    }
    else{
      return $this->session->userdata("guest");
    }
  }

  // Guest cart items
  public function AddItems(){

      if(!empty($this->session->userdata("guest"))){
        $guestsession = $this->session->userdata("guest");
      }
      else{
        $guestsession = $this->SetGuestSession();
      }

      $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $guestsession;

      if($this->input->post()){

        $sellerID = $this->input->post("seller");

        $productdata = array(
          "user_id"     => $user,
          "product_id"  => $this->input->post("product"),
          "qty"         => $this->input->post("qty"),
          "seller_id"   => $sellerID
         // "product_sku" => $this->input->post("sku")
        );

        $checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$user}",'product_id'=>$productdata['product_id']))->get();

        if($checkexists){
          $updatedqty = $checkexists->qty + $this->input->post("qty");
          $update_data = array('qty' => $updatedqty);
          $updated = $this->Shopping_carts_model->where(array('user_id'=>"{$user}",'product_id'=>$productdata['product_id']))->update($update_data);

          if($updated){
            echo json_encode(array("msg" => true,'message' => 'Item has been updated in cart.'));
            //exit;
          }
          else{
            echo json_encode(array("msg" => false,'message' => '* Unable to update cart.'));
          }
        }
        else{
          $created = $this->Shopping_carts_model->insert($productdata);
          if($created){
            echo json_encode(array("msg" => true,'message' => 'Item has been added in cart.'));
            
          }
          else{
            echo json_encode(array("msg" => false,'message' => '* Unable to add in cart.'));
          }

          /*$checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$user}"))->get();

          if($checkexists && $checkexists->seller_id == $sellerID){ 

            $created = $this->Shopping_carts_model->insert($productdata);
            if($created){
              echo json_encode(array("msg" => true,'message' => 'Item has been added in cart.'));
              
            }
            else{
              echo json_encode(array("msg" => false,'message' => '* Unable to add in cart.'));
            }
          }
          elseif(!$checkexists){
            $created = $this->Shopping_carts_model->insert($productdata);
            if($created){
              echo json_encode(array("msg" => true,'message' => 'Item has been added in cart.'));
              
            }
            else{
              echo json_encode(array("msg" => false,'message' => '* Unable to add in cart.'));
            }
          }
          else{
              echo json_encode(array("msg" => false,'message' => '* Different seller items not allowed in basket'));
          }*/
        }
      }
      exit;
  }

   // Guest cart update by id
  public function update_items(){

      if(!empty($this->session->userdata("guest"))){
        $guestsession = $this->session->userdata("guest");
      }
      else{
        $guestsession = $this->SetGuestSession();
      }

      $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $guestsession;

      if($this->input->post()){

        $userqty = $this->input->post("qty");
        $productID = $this->input->post("product");
        $userID = $user;

        $checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$user}",'product_id'=>$productID))->get();

        if($checkexists){
          //$updatedqty = $checkexists->qty + $this->input->post("qty");
          $update_data = array('qty' => $userqty);
          $updated = $this->Shopping_carts_model->where(array('user_id'=>"{$user}",'product_id'=>$productID))->update($update_data);

          if($updated){
            echo json_encode(array("msg" => true,'message' => '* Item has been updated in cart.'));
            exit;
          }
          else{
            echo json_encode(array("msg" => false,'message' => '* Unable to update cart.'));
            exit;
          }
        }
        else{
          echo json_encode(array("msg" => false,'message' => '* Item not found in cart.'));
          exit;
        }
      }
  }

  // Guest checkout form
  public function checkout($seller){
    $commission = 0;
    if(!empty($this->session->userdata("guest"))){ 
      $guestsession = $this->session->userdata("guest");
    }
    else{
      $guestsession = $this->SetGuestSession();
    }
    //die("dfdd");
    $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $guestsession;

    $products = productlists_by_userid($user);
    $cartdata = productcarts_by_userid($user);

    if(is_digit($seller)){
      $sellerids = $seller;
    }
    else{
      $sellerids = decrypt($seller);
    }
//var_dump($sellerids);
    $sellerID =  $this->input->post('seller');
    $chatdata = $this->Chatbots_model->where('created_by',$sellerID)->get(); // decrypt($sellerID)
    $custaddress = $this->Customers_Addresses_model->where("customer_id",$user)->get_all(); 
    $reedemcode = checkcoupon_apply($user);
    //var_dump($reedemcode);exit;
    if($this->input->post()){

       // User country and seller country check 
        //$vistorinfo = ip_info("Visitor", "Location");
        $countrtname = $this->input->post('countryname');
        $sellerinfo = admin_info($sellerID);

        if(empty($sellerinfo->country)){
          $this->session->set_flashdata('flashmsg', "<p>* Seller country not found.</p>");
          $this->session->set_flashdata('msg', 'error');
          redirect($_SERVER['HTTP_REFERER']);
        }
        //END

        $this->form_validation->set_rules('name','Name','trim|xss_clean|required');
        $this->form_validation->set_rules('email','Email','trim|xss_clean|required');
        $this->form_validation->set_rules('contact_number','Contact Number','trim|xss_clean|required|numeric');
        $this->form_validation->set_rules('delivery_type','Delivery Type','trim|xss_clean|required');

        if(!$this->input->post('custaddress') && $this->input->post('delivery_type') == 'delivery'){
          $this->form_validation->set_rules('choose_state','Shipping State','trim|xss_clean|required');
          $this->form_validation->set_rules('local_government','Local Government','trim|xss_clean|required');
          $this->form_validation->set_rules('shiping_address','Shipping Address','trim|xss_clean|required');
        }
        else if($this->input->post('custaddress') && $this->input->post('delivery_type') == 'delivery'){
          $this->form_validation->set_rules('custaddress','Choose Address','trim|xss_clean|required');
        }

        if($chatdata && $chatdata->delivery_services == 1 && is_null($this->input->post('delivery_option'))){
          $this->form_validation->set_rules('delivery_option','Delivery Option','trim|xss_clean|required');
        }

        $this->form_validation->set_rules('payment_method','Payment Method','trim|xss_clean|required');

        if($this->form_validation->run() == true) {

          /*$paymentkeys = $this->Chatbots_model->where('created_by',$this->input->post('seller'))->get();*/
         
          if(count($products) > 0){

            if($this->input->post('custaddress')){
                $custaddress = $this->Customers_Addresses_model->get($this->input->post('custaddress'));
            }

            $data['reset'] = TRUE;

            $addressdata = array(
              'user_id'           => $user,
              'receiver_name'     => $this->input->post('name'),
              'email'             => $this->input->post('email'),
              'phone'             => $this->input->post('contact_number'),
              'state'             => $this->input->post('choose_state') ? $this->input->post('choose_state') : $custaddress->state,
              'local_government'  => $this->input->post('local_government') ? $this->input->post('local_government') : $custaddress->district,
              'shipping_address'  => $this->input->post('shiping_address') ? $this->input->post('shiping_address') : $custaddress->address,
              'delivery_method'   => is_null($this->input->post('delivery_option')) ? "" : $this->input->post('delivery_option'),
              'country'           => $this->input->post('countryname') ? $this->input->post('countryname') : $custaddress->country
            );

            $payment_method = $this->input->post('payment_method');

            // Seller payment details disabled
            /*if($payment_method == "prepaid"){
              if(!$paymentkeys){
                $this->session->set_flashdata('flashmsg', '<p>* Payment details not found for seller.</p>');
                $this->session->set_flashdata('msg', 'error');
                redirect($_SERVER['HTTP_REFERER']);
              }
              else if(empty($paymentkeys->public_key) || empty($paymentkeys->secret_key)){
                $this->session->set_flashdata('flashmsg', '<p>* Merchant public key or secret key not found.</p>');
                $this->session->set_flashdata('msg', 'error');
                redirect($_SERVER['HTTP_REFERER']);
              }
            }*/
            

            $lastaddressID = $this->Address_model->insert($addressdata);

            // Save order information
            $delivery_type = $this->input->post('delivery_type');
            //$sum = 1;

            $Pids = array();
            $sum = 0;
            foreach ($products as $key => $value) {
                $Pids[] = $value->product_id;
                $qty[] = $value->qty;
                if($value->sale_price > 0){
                  $price = $value->sale_price;
                }else{
                  $price = $value->product_price;
                }
                $productslistnames[] = $value->product_name;
                $sum = $sum + ($price * $value->qty); //+ $value->shipping_cost;
            }

            $discount = ($reedemcode && empty($reedemcode->order_id)) ? $reedemcode->total_discount : 0;

            $orderInfo = array(
                "transaction_id"   => time(),
                "user_id"          => $user,
                "created_by"       => $this->input->post('seller'),
                'page_id'          => "",
                "shipping_address" => $lastaddressID,
                "order_amount"     => ($sum - $discount),
                "payment_method"   => "cash",
                "status"           => "0",
                "created_date"     => date('Y-m-d h:i:s'),
                "store_type"       => $this->input->post('delivery_type'), 
                "order_type"       => ""
            );

            if($payment_method == 'cash'){

             /* $config = emailconfig();
              $this->load->library('email',$config);*/

              $saved = $this->Orders_model->insert($orderInfo); // Save orders

              $orderdetails = $this->Orders_model->get($saved);

               // Update order id in coupon 
              if($reedemcode && empty($reedemcode->order_id))
              {
                $this->db->set('order_id',$orderdetails->transaction_id)
                 ->where('id',$reedemcode->id)
                 ->update('coupon_redemptions');
              }
              // END

              foreach ($Pids as $key => $value) {
                // Save orders with products
                $this->Orders_products_model->insert(array('order_id' => $orderdetails->transaction_id,'product_id' => $value,'ordered_qty' => $qty[$key]));
              }

               // Deduct commission and pay to akin 3.5% - 08-08-2018 - 17-10-2018
              $commission = ($sum * 6) / 100;
              $commissiondata = array(
                "order_id" => $saved,
                "akin_commission" => $commission,
                "auto_cancelled_fees" => "0",
                "transaction_type" => "Commission",
                "seller_id" => $this->input->post('seller')
              );

              $this->db->insert("commission_fees",$commissiondata);
              //END 

              // Remove user shopping cart products
              $this->Shopping_carts_model->force_delete(array('user_id' => $user));

              $this->session->unset_userdata("country_{$guestsession}");

              $this->session->set_flashdata('flashmsg', "<p>* Your order has been confirmed.</p>");
              $this->session->set_flashdata('msg', 'success');

              /*$invoicepdfssss = $this->GenerateInvoice($orderdetails->transaction_id,decrypt($this->input->post('seller'))); 

              $data['invoice'] = $invoicepdfssss;

              $qrcode = QrcodeofOrder($orderdetails->transaction_id);
              $etickets = GenerateTicket($orderdetails->transaction_id,$qrcode['qrcode'],$productslistnames);

              $data['etickets'] = $etickets; */ 

              $data['seller'] = $sellerinfo->admin_name;
              $data['guestname'] = $this->input->post('name');
              $data['totalamount'] = ($sum - $discount);
              $data['orderno'] = $orderdetails->transaction_id;
              $data['ordermethod'] = $this->input->post('delivery_type');
              $data['deliveryspeed'] = "";  
              $data['paymenttype'] = $orderdetails->payment_method;

              if ( ! defined('BASEPATH')) exit('No direct script access allowed');

              $mailConfig['mailpath'] = "/usr/sbin/sendmail";
              $mailConfig['protocol'] = "sendmail";
              $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
              $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
              $mailConfig['smtp_pass'] = "tg1<LvPL";
              $mailConfig['smtp_port'] = "25";
              $mailConfig['mailtype'] = "html";

              $this->load->library('email', $mailConfig);

              $body = $this->load->view('templates/guest_order_confirmed_template',$data,TRUE);
              $this->email->set_newline("\r\n");   
              $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
              $this->email->message($body); 
              $this->email->to($this->input->post('email'));
              $this->email->subject('Order Confirmation');

              $invoicepdfssss = $this->GenerateInvoice($orderdetails->transaction_id,$this->input->post('seller')); 

              $this->email->attach(FCPATH."uploads/invoices/".$invoicepdfssss);

              $qrcode = QrcodeofOrder($orderdetails->transaction_id);
              $eticket = GenerateTicket($orderdetails->transaction_id,$qrcode['qrcode'],$productslistnames);

              $this->email->attach(FCPATH."uploads/invoices/".$eticket);

              $send = $this->email->send();

              if(!$send){
                $path = __DIR__.'\Item.php function checkout';
                $line = 220;
                apilogger($this->email->print_debugger(),$path,$line);
              }
              /*$fromEmail = "no-reply@pointslive.com";

              $body = $this->load->view('templates/guest_order_confirmed_template.php',$data,TRUE);
              // Send Mail usign mail()
              $emailMessage = implode("\r\n", array($body));

              $headers = array("From: ".$fromEmail,
                "Reply-To: ".$fromEmail,
                "X-Mailer: PHP/" . PHP_VERSION,
                "Content-Type: text/html; charset=UTF-8\r\n"
              );
              $headers = implode("\r\n", $headers);

              $send = mail($this->input->post('email'), "Order Confirmation", $emailMessage, $headers, "-f".$fromEmail); */ 

              redirect(base_url("item/order_confirm/".urlencode($orderdetails->transaction_id)));
            }
            else if($payment_method == 'account' || $payment_method == 'ussd' || $payment_method == 'card'){

              $this->session->set_userdata("useraddress_{$user}",$lastaddressID);

              $curl = curl_init();

              $customer_email = $this->input->post('email');
              $amount = ($sum - $discount); 
              $country = "NG";
              $currency = "NGN";
              $txref = uniqid(); // ensure you generate unique references per transaction.
              $PBFPubKey = $this->config->item('public_key');//$paymentkeys->public_key; // get your public key from the dashboard.
              $redirect_url = base_url()."item/flutterresponse/{$seller}/{$user}/{$delivery_type}";
              //$payment_plan = "pass the plan id"; // this is only required for recurring payments.

              curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/hosted/pay",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode([
                  'amount'=>$amount,
                  'customer_email'=>$customer_email,
                  'country' => $country,    
                  'currency'=>$currency,
                  'payment_options' => $payment_method, // ,card,account, ussd
                  'txref'=>$txref,
                  'PBFPubKey'=>$PBFPubKey,
                  'redirect_url'=>$redirect_url
                ]),
                CURLOPT_HTTPHEADER => [
                  "content-type: application/json",
                  "cache-control: no-cache"
                ],
              ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              if($err){
                // there was an error contacting the rave API
                $data['reset'] = FALSE;
                $this->session->set_flashdata('flashmsg', "<p>* Curl returned error: ".$err."</p>");
                $this->session->set_flashdata('msg', 'error');

              }

              $transaction = json_decode($response, true);
           
              if($transaction['status']=='error'){ //!$transaction->data && !$transaction->data->link
                // there was an error from the API
                $data['reset'] = FALSE;
                $this->session->set_flashdata('flashmsg', "<p>* API returned error: ".$transaction['message']."</p>");
                $this->session->set_flashdata('msg', 'error');
              }
              // redirect to page so User can pay
              // uncomment this line to allow the user redirect to the payment page
              header('Location: ' . $transaction['data']['link']);
            }
          }
          else{
            $this->session->set_flashdata('flashmsg', '<p>* No items for checkout.</p>');
            $this->session->set_flashdata('msg', 'error');
            redirect($_SERVER['HTTP_REFERER']);
          }
        }
        else{
          $data['reset'] = FALSE;
          $data['content'] = 'global/guest_checkout';
          $data['title'] = 'Customer Checkout';
          $data['sellerID'] = $sellerids;
          $data['caddress'] = $custaddress;
          $data['codes'] = $reedemcode;
          $data['pdata'] = $cartdata;
          $this->load->view('global/template', $data);
        }
    }
    else{
      $data['content'] = 'global/guest_checkout';
      $data['title'] = 'Customer Checkout';
      $data['sellerID'] = $sellerids;
      $data['caddress'] = $custaddress;
      $data['codes'] = $reedemcode;      
      $data['pdata'] = $cartdata;
      $this->load->view('global/template', $data);
    }
  }

  // Remove Item from cart
  public function RemoveItem(){ 

    $seller = $this->input->post('seller');
    $product = $this->input->post('product');

    $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

    $remove = $this->Shopping_carts_model->where(array('product_id'=>$product))->force_delete(array('user_id' => $user));

    if($remove){
      echo json_encode(array("msg" => true));
    }
    else{
      echo json_encode(array("msg" => false));
    }
    exit;
  }

  // Update cart items
  public function UpdateItems($seller=""){
    
    $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

     $updatecart = array(); 
     if($this->input->post()){
      foreach ($this->input->post('quantity') as $key => $value) {
        $updatecart[] = array(
         'id'         => $this->input->post("masterid")[$key], 
         'user_id'    => $user,
         'product_id' => $this->input->post('products')[$key],
         'qty'        => $value  
        );
      } 

      $updated = $this->db->update_batch('shopping_carts',$updatecart, 'id'); 

      if($updated){
        $this->session->set_flashdata('flashmsg', '<p>* Cart has been updated successfully.</p>');
        $this->session->set_flashdata('msg', 'success');
      }
     }
     redirect(base_url("item/manage_cart/{$seller}"));
  }

  // Get local area by state
  public function GetLocalAreas(){

    $state = $this->input->post('state');
    $country = $this->input->post('country');

    $cities = getDistrictByState($state,$country);
    echo json_encode(array("msg" => true,"listcities" => $cities));  
    exit;
  }

  // Flutterwave response handle
  public function flutterresponse($seller="",$userID="",$type=""){
    $parameters = $this->input->get();
    $products = productlists_by_userid($userID);
    //$paymentkeys = $this->Chatbots_model->where('created_by',$seller)->get();
    $Pids = array();
    $qty = array();
    $productdata = array();
    $sum = 0;
    $price = 0;
    $commission = 0;

    if(is_digit($seller)){
      $sellerids = $seller;
    }
    else{
      $sellerids = decrypt($seller);
    }

    $sellerinfo =  admin_info($sellerids);
    $reedemcode = checkcoupon_apply($userID);

    if(isset($parameters['flwref'])){
        $paymentinfo = $this->getTransactionByTrf($parameters['txref'],$this->config->item('public_secret')/*$paymentkeys->secret_key*/); // verify payment
        foreach ($products as $key => $value) {
            $Pids[] = $value->product_id;
            $qty[] = $value->qty;

            if($value->sale_price > 0){
              $price = $value->sale_price;
            }else{
              $price = $value->product_price;
            }
            $productslistnames[] = $value->product_name;
            $sum = $sum + ($price * $value->qty); //+ $value->shipping_cost;
        }

        $discount = ($reedemcode && empty($reedemcode->order_id)) ? $reedemcode->total_discount : 0;

        $orderInfo = array(
            "transaction_id"   => $paymentinfo['data']['txid'],
            "user_id"          => $userID,
            "created_by"       => $products[0]->created_by,
            'page_id'          => "",
            "shipping_address" => $this->session->userdata("useraddress_{$userID}"), 
            "order_amount"     => ($sum - $discount),
            "applicable_fees"  => $paymentinfo['data']['appfee'], 
            "payment_method"   => "prepaid",
            "store_type"       => $type, 
            "status"           => "0", 
            "created_date"     => date('Y-m-d h:i:s'),
            "transaction_ref"  => $parameters['txref']
        );

        $adressinfo = $this->db->select('*')->from('address')->where('id',$this->session->userdata("useraddress_{$userID}"))->get()->row();

        if($paymentinfo['data']['status'] == "successful"){
            $data['status'] = $paymentinfo['data']['status'];
            $data['orders'] = $paymentinfo['data']['txid'];
            $orderInfo['status'] = "0";
            $saved = $this->Orders_model->insert($orderInfo); // Save order's
            $orderdetails = $this->Orders_model->get($saved);

            // Update order id in coupon 
            if($reedemcode && empty($reedemcode->order_id))
            {
              $this->db->set('order_id',$orderdetails->transaction_id)
               ->where('id',$reedemcode->id)
               ->update('coupon_redemptions');
            }
            // END

            $data['dateoforder'] = $orderdetails->created_date;
            $data['total'] = $orderdetails->order_amount;  
            foreach ($Pids as $key => $value) {
              // Save orders with products
              $this->Orders_products_model->insert(array('order_id' => $orderdetails->transaction_id,'product_id' => $value,'ordered_qty' => $qty[$key]));
            }

            if($saved){
              // Remove user shopping cart products
              $this->Shopping_carts_model->force_delete(array('user_id' => $userID));
            } 

            // Deduct commission and pay to akin 3.5% - 08-08-2018 - 17-10-2018
            $commission = ($sum * 6) / 100;
            $commissiondata = array(
              "order_id" => $saved,
              "akin_commission" => $commission,
              "auto_cancelled_fees" => "0",
              "transaction_type" => "Commission",
              "seller_id" => $products[0]->created_by
            );

            $this->db->insert("commission_fees",$commissiondata);
            //END 

            $data['reset'] = TRUE;

            $this->session->unset_userdata("country_{$userID}");

            $this->session->set_flashdata('flashmsg', "<p>* Your order has been confirmed.</p>");
            $this->session->set_flashdata('msg', 'success');

           /* $invoicepdfssss = $this->GenerateInvoice($orderdetails->transaction_id,$products[0]->created_by);

            $data['invoice'] = $invoicepdfssss;

            $qrcode = QrcodeofOrder($orderdetails->transaction_id);
            $etickets = GenerateTicket($orderdetails->transaction_id,$qrcode['qrcode'],$productslistnames);

            $data['etickets'] = $etickets;*/

            // Email send 
            $data['seller'] = $sellerinfo->admin_name;
            $data['guestname'] = $adressinfo->receiver_name;
            $data['totalamount'] = ($sum - $discount);
            $data['orderno'] = $orderdetails->transaction_id;
            $data['ordermethod'] = $orderdetails->store_type;
            $data['deliveryspeed'] = ""; 
            $data['paymenttype'] = $orderdetails->payment_method;

            if ( ! defined('BASEPATH')) exit('No direct script access allowed');

            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
            $mailConfig['protocol'] = "sendmail";
            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
            $mailConfig['smtp_pass'] = "tg1<LvPL";
            $mailConfig['smtp_port'] = "25";
            $mailConfig['mailtype'] = "html";

            $this->load->library('email', $mailConfig);

            $body = $this->load->view('templates/guest_order_confirmed_template',$data,TRUE);
            $this->email->set_newline("\r\n");   
            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
            $this->email->message($body); 
            $this->email->to($adressinfo->email);
            $this->email->subject('Order Confirmation');

            $invoicepdfssss = $this->GenerateInvoice($orderdetails->transaction_id,$products[0]->created_by); 

            $this->email->attach(FCPATH."uploads/invoices/".$invoicepdfssss);

            $qrcode = QrcodeofOrder($orderdetails->transaction_id);
            $eticket = GenerateTicket($orderdetails->transaction_id,$qrcode['qrcode'],$productslistnames);

            $this->email->attach(FCPATH."uploads/invoices/".$eticket);

            $send = $this->email->send();

            if(!$send){
              $path = __DIR__.'\Item.php function flutterresponse';
              $line = 546;
              apilogger($this->email->print_debugger(),$path,$line);
            }

            /*$fromEmail = "no-reply@pointslive.com";

            $body = $this->load->view('templates/guest_order_confirmed_template.php',$data,TRUE);
            // Send Mail usign mail()
            $emailMessage = implode("\r\n", array($body));

            $headers = array("From: ".$fromEmail,
              "Reply-To: ".$fromEmail,
              "X-Mailer: PHP/" . PHP_VERSION,
              "Content-Type: text/html; charset=UTF-8\r\n"
            );
            $headers = implode("\r\n", $headers);

            $send = mail($adressinfo->email, "Order Confirmation", $emailMessage, $headers, "-f".$fromEmail);*/

            redirect(base_url("item/order_confirm/".urlencode($orderdetails->transaction_id)));
        }
        else if($paymentinfo['data']['status'] == "failed"){
            $orderInfo['status'] = "5";
            $data['status'] = $paymentinfo['data']['status'];
            $data['orders'] = $paymentinfo['data']['txid'];
            $saved = $this->Orders_model->insert($orderInfo); // Save order's
            $orderdetails = $this->Orders_model->get($saved);

             // Update order id in coupon 
            if($reedemcode && empty($reedemcode->order_id))
            {
              $this->db->set('order_id',$orderdetails->transaction_id)
               ->where('id',$reedemcode->id)
               ->update('coupon_redemptions');
            }
            // END

            $data['dateoforder'] = $orderdetails->created_date; 
            $data['total'] = $orderdetails->order_amount;  
            foreach ($Pids as $key => $value) {
              // Save orders with products
              $this->Orders_products_model->insert(array('order_id' => $orderdetails->transaction_id,'product_id' => $value,'ordered_qty' => $qty[$key]));
            }

            if($saved){
              // Remove user shopping cart products
              $this->Shopping_carts_model->force_delete(array('user_id' => $userID));
            } 

            $data['reset'] = TRUE;

            $this->session->unset_userdata("country_{$userID}");

            $this->session->set_flashdata('flashmsg', '<p>* Order has been failed.</p>');
            $this->session->set_flashdata('msg', 'error');
        }
    }
    else
    {
        $data['status'] = $parameters['cancelled'];
        $data['reset'] = FALSE;
        $this->session->set_flashdata('flashmsg', '<p>* Order has been cancelled by payment gateway.</p>');
        $this->session->set_flashdata('msg', 'error');
    }
    //$data['reset'] = TRUE;
    /*$qrcode = QrcodeofOrder($orderdetails->transaction_id);
    $etickets = GenerateTicket($orderdetails->transaction_id,$qrcode['qrcode'],$productslistnames);*/

    //$data['etickets'] = $etickets;
    $this->session->set_userdata("paymentdata_{$userID}",$data);
    $this->session->unset_userdata("useraddress_{$userID}");
    redirect(base_url("item/checkout/{$seller}"));
  }

   // Retrieve flutterwave transaction details using API - 24-05-2018
  public function getTransactionByTrf($tran="",$secrerkey=""){

    $result = array();

    $postdata = array( 
      'txref' => $tran,
      'SECKEY' => $secrerkey,
      'inlcude_payment_entity' => 1
      );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,"https://api.ravepay.co/flwv3-pug/getpaidx/api/xrequery");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $headers = [
      'Content-Type: application/json',
    ];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $request = curl_exec($ch);
    curl_close ($ch);

    $result = json_decode($request, true);
  /*  

    if($result['status'] == 'error'){      
      $path = __DIR__.'\Admin.php function getTransactionByTrf';
      $line = 24169;
      apilogger($result['message'],$path,$line);
    }*/
    
    return $result;
  }

   // Form base review by order
  public function review($orderID=""){

    if(is_digit($orderID)){
      $orderids = $orderID;
    }
    else{
      $orderids = decrypt($orderID);
    }

    $feedback = array();
    $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

    if($this->input->post()){
       $this->form_validation->set_rules('rating','Rating','trim|required',
                        array('required' => '* You must provide a %s.')
                );

       if($this->session->userdata('customerId') || !is_null($this->session->userdata('customerId'))){
          $this->form_validation->set_rules('comments','Comments','trim|required',array('required' => '* Please provide a %s.'));
       }

       if($this->form_validation->run() == true) {

          $rate = $this->input->post('rating');
          $comments = $this->input->post('comments');

          $order = $orderids;
          $checkrecord = $this->Orders_model->where(array('id' => $order))->get();

          if($checkrecord){

            if(empty($checkrecord->order_rate)){
             
              $updateRate = array("order_rate" => $rate);

              if($this->session->userdata('customerId') || !is_null($this->session->userdata('customerId'))){
                $feedback['comments'] = $comments;
                $feedback['given_rating'] = $rate;
                $feedback['customer_id'] = $user;
                $feedback['order_id'] = $order;
                $feedback['created_date'] = date('Y-m-d h:i:s');

                $this->db->insert('reviews',$feedback); // review table
              }

              $this->Orders_model->update($updateRate,$checkrecord->id); 

              $this->session->set_flashdata('flashmsg', '<p>* Thank you for feedback.</p>');
              $this->session->set_flashdata('msg', 'success');
            }
            else{
              $this->session->set_flashdata('flashmsg', "<p style='margin:0'>* Sorry, You already given rating.</p>");
              $this->session->set_flashdata('msg', 'error');
            }
            redirect($_SERVER['HTTP_REFERER']);  
          }
          else{
            $this->session->set_flashdata('flashmsg', "<p style='margin:0'>* Sorry, something may be wrong while providing rating.</p>");
            $this->session->set_flashdata('msg', 'error');
            redirect($_SERVER['HTTP_REFERER']);
          }
         
       }
       else{
          $data['reset'] = FALSE;
       }
    }

    if($this->session->userdata('customerId') || !is_null($this->session->userdata('customerId'))){
      $data['content'] = 'global/customer_review_form';
    }
    else{
      $data['content'] = 'global/review_form';
    }

    //$data['content'] = 'global/review_form';
    $data['title'] = 'Feedback';
    $data['orderID'] = $orderids;
    $this->load->view('global/template', $data); 
  }

  // Order confirmation page
  public function order_confirm($orderID=""){
    $orderdecrypt = urldecode($orderID);
    $orders = orderlists($orderID,"","");
    $data['content'] = 'global/order_confirmation';
    $data['title'] = 'Order Confirmation';
    $data['orders'] = $orders;
    $this->load->view('global/template', $data); 
  }

  // Print pdf invoice of orders
  public function GenerateInvoice($orderID="",$seller=""){

        $this->load->library('m_pdf');
        $mpdf = new mPDF();
        //var_dump($mpdf);
        $caddress = "";
        $shipcost = 0;
        $subtotal = 0;
        $grandtotal = 0;
        $products = "";
        $discount = "";
        $price = 0;
        $discamount = 0;
       //number_format($número, 2, '.', '');
        $orders = orderlists($orderID);
        //die("s");
        $userinfo = admin_info($seller);

        $botinfo = $this->db->select('offer_days')->from('chatbots')->where('created_by',$seller)->get()->row();

        $name = ($orders[0]->receiver_name !="") ? $orders[0]->receiver_name : facebook_username($orders[0]->user_id,$orders[0]->page_id);
        
        if($botinfo){
            $returndays = !empty($botinfo->offer_days) ? implode(",",unserialize($botinfo->offer_days)) : "0";
        }else{
            $returndays = 0;
        }

        $discounts = $this->db->select('*')->from('coupon_redemptions')->where('order_id',$orders[0]->transaction_id)->get()->row();

        if(empty($orders[0]->products)){

          $packgaes = $this->db->select('*')
                      ->from('shipping_packages')
                      ->where('transaction_id',$orders[0]->transaction_id)
                      ->get()->row();
          $subtotal = $subtotal + $orders[0]->order_amount;
          $vat = $subtotal * 5 / 100;

          $products.="<tr><td style=\"text-align: left;\">{$packgaes->package_type}</td><td style=\"text-align: center;\">1</td><td style=\"text-align: right;\">".number_format($orders[0]->order_amount, 2, '.', '')."</td></tr>";
       
          }
          else{
              foreach ($orders[0]->products as $key => $product) {
                $shipcost = $shipcost + $product->shipping_cost;
                if($product->sale_price > 0){
                    $price = $product->sale_price;
                }
                else{
                    $price = $product->product_price;
                }
                $subtotal = $subtotal + ($price * $product->ordered_qty);
                $vat = $subtotal * 5 / 100; 
                $products.="<tr><td style=\"text-align: left;\">{$product->product_name}</td><td style=\"text-align: center;\">".$product->ordered_qty."</td><td style=\"text-align: right;\">".number_format($price, 2, '.', '')."</td></tr>";
            }
         }

         if($discounts){
            $discount.="<td colspan=\"2\" class=\"border-none\">Discount</td>
                <td class=\"border-none\">".number_format($discounts->total_discount,2,'.','')."</td>";
            $discamount = $discounts->total_discount;    
         }
         else{
            $discount.="<td colspan=\"2\" class=\"border-none\">Discount</td>
                <td class=\"border-none\">".number_format(0,2,'.','')."</td>";
            $discamount = 0; 
         }

         $pages = getFacebookpagetoken($orders[0]->page_id);
         if(count($pages) > 0):
          $url = "https://graph.facebook.com/{$orders[0]->page_id}?fields=single_line_address,location&access_token=".$pages[0]->access_token;

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
          curl_setopt($ch,CURLOPT_URL,$url);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
          curl_exec($ch);
          $content = curl_exec($ch);
          $result = json_decode($content);
          curl_close($ch);

          $page = $pages[0]->page_name;
          $address = $result->single_line_address;

         else :
           $page = "";  
           $address = "";
         endif;
       
        $my_html1="
            <!DOCTYPE html>
            <html lang=\"en\">
            <head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset=\"utf-8\">

            <style>
            .invoice-table {border-collapse: collapse;}
            .invoice-table td{text-align: left;
            border: 2px solid grey;
            border-collapse: collapse;
            padding: 8px 10px;}
            .invoice-table th{
                text-align: center;
                padding: 8px;
                border: 2px solid grey;
            }
            .invoice-table .border-none {
                border: none;
                text-align: right;
                border-right: 2px solid grey;
                
            }
           .invoice-table .border-none.border-bottom {border-bottom: 2px solid grey;border-top: 2px solid grey;}
            .text-bold{
                font-weight: bold;
                font-size: 20px;
            }
            .terms{text-align: left;width: 21cm;margin: 0 auto;padding-top: 180px;}
            .terms h4{margin-bottom: 0px;}
            span {
              content: \"\0E3F\";
            }
            </style>
            </head>
          <table style=\"width: 21cm;  margin: 0 auto;\">
            <tr>
                <td colspan=\"2\" style=\"text-align: center; line-height: normal;\">
                    <h2>Invoice</h2>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <h2>".$page."</h2>
                </td>
            </tr>
           <tr>
                <td>".$address."</td>
                <td style=\"width: 50%\"></td>
            </tr>
        </table>
        <table>
            <tr>
                <td style=\"height: 50px;\"></td>
            </tr>
        </table>
        <table style=\"width: 21cm;  margin: 0 auto;\">
        <tbody>
        <tr>
            <td style=\"font-weight: bold;\"><h3 style=\"margin:5px 0;\">Bill To</h3></td>
            <td></td>
        </tr>
        <tr>
            <td style=\"width:65%\">
                <p style=\"margin:5px 0;\">
                    ".$name."
                </p>
                <p style=\"margin:5px 0;\">
                    ".$orders[0]->shipping_address."
                </p>
                <p style=\"margin:5px 0;\">
                    ".$orders[0]->district.", ".$orders[0]->country."
                </p>
                <p style=\"margin:5px 0;\">
                    ".$orders[0]->postcode."
                </p>
            </td>
            <td style=\"vertical-align: top; padding-left:20px; width: 35%\">
                <table style=\"width:100%\">
                    <tbody>
                    <tr>
                        <td>
                            <p style=\"font-weight: bold; margin:0\">Invoice #</p>
                        </td>
                        <td style=\"text-align: right;\">
                            ".$orders[0]->transaction_id."
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p style=\"font-weight: bold; margin:0\">Invoice Date</p>
                        </td>
                        <td style=\"text-align: right;\">
                            ".date('d/m/Y',strtotime($orders[0]->created_date))."
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-weight: bold; margin:0\">Payment Method</p>
                        </td>
                        <td style=\"text-align: right;\">
                            ".$orders[0]->payment_method."
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
        <table>
            <tr>
                <td style=\"height: 30px;\"></td>
            </tr>
        </table>
        <div style=\"width: 21cm;  margin: 0 auto;\">
         <table class=\"invoice-table\" style=\"width: 100%;\">
               <tr style=\"background-color: #f7f7f7;\">
                <th>DESCRIPTION</th>
                <th>QUANTITY</th>
                <th>AMOUNT</th>
              </tr>
             ".$products."
            <tr>
            <td colspan=\"2\" class=\"border-none\">Subtotal</td>
                <td  class=\"border-none\">
                    <div style=\"\">".number_format($subtotal,2,'.','')."</div>
                </td>
            </tr>
            <tr>
            ".$discount."   
            </tr>
            <tr>
                <td colspan=\"2\" class=\"border-none text-bold\">VAT</td>
                <td  class=\"border-none\">".number_format($subtotal * 5 / 100,2,'.','')."</td>
            </tr>
            <tr>
                <td colspan=\"2\" class=\"border-none text-bold\">TOTAL</td>
                <td  class=\"border-none border-bottom text-bold\" style=\"border-bottom:2px solid grey;border-top:2px solid grey;\"><span style=\"font-family: DejaVu Sans; sans-serif;\">&#3647;</span>".number_format($grandtotal = ($subtotal + $vat) - $discamount,2,'.','')."</td>
            </tr>
          </table>
         </div>
         <div class=\"terms\">
            <h4>Terms & Conditions</h4>
            <p>Thank you for buying from <b>".$page."</b>. We hope you enjoy your order. Returns are accepted up to <b>".$returndays." days</b> after your purchase. If you have any enquiries, Please call us on <b>".$userinfo->admin_mobile."</b> or email us at <b>".$userinfo->admin_email."</b></p>
        </div></html>";


        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->WriteHTML($my_html1);

        /*echo '<pre>';
        print_r($my_html1);*/
      //exit;

        $outputs = 'invoice-' . $orderID . '.pdf';
        $pdf_path123 = realpath(FCPATH.'uploads/invoices');
        $mpdf->Output($pdf_path123."/{$outputs}", 'F');
        unset($mpdf); // this is the magic
        unset($my_html1); // this is the magic
        return $outputs;
  }

  // Redirect to akin store
  public function redirect(){
    redirect('https://rite.ly/KPhr');
  }

  // Store information page
  public function viewstore($seller=""){

    if(is_digit($seller)){
      $sellerID = $seller;
    }
    else{
      $sellerID = decrypt($seller); 
    }

    $chatdetails = $this->Chatbots_model->where('created_by',$sellerID)->get();

    $data['content'] = 'global/store_information';
    $data['title'] = 'Store Information';
    $data['storedata'] = $chatdetails;
    $data['sellerID'] = $seller;
    $this->load->view('global/template', $data); 
  }

  // Omise payment to website customers
  public function omischeckout($seller){

    require_once APPPATH.'libraries/omise/lib/Omise.php';

    define('OMISE_PUBLIC_KEY', 'pkey_test_5cmltmh9uqb9rr4r0u7');
    define('OMISE_SECRET_KEY', 'skey_test_5cmlrw28w35kjcwqz96');

    $commission = 0;
    if(!empty($this->session->userdata("guest"))){ 
      $guestsession = $this->session->userdata("guest");
    }
    else{
      $guestsession = $this->SetGuestSession();
    }

    $user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $guestsession;

    $products = productlists_by_userid($user);

   /* if(is_digit($seller)){
      $sellerid = $seller;
    }
    else{
      $sellerid = decrypt($seller);
    }*/

    $sellerID =  $this->input->post('seller');
    $sellerinfo =  admin_info($sellerID);
    $chatdata = $this->Chatbots_model->where('created_by',$sellerID)->get();
    $reedemcode = checkcoupon_apply($user);

    if(count($products) == 0){
      $this->session->set_flashdata('flashmsg', '<p>* No items for checkout.</p>');
      $this->session->set_flashdata('msg', 'error');
      redirect($_SERVER['HTTP_REFERER']);
    }
    else{
      if (isset($_POST['omiseToken']) || isset($_POST['omiseToken1'])){
        try 
        {   
            $Pids = array();
            $qty = array();
            $sum = 0;
            foreach ($products as $key => $value) {
                $Pids[] = $value->product_id;
                $qty[] = $value->qty;
                if($value->sale_price > 0){
                  $price = $value->sale_price;
                }else{
                  $price = $value->product_price;
                }
                $productslistnames[] = $value->product_name;
                $sum = $sum + ($price * $value->qty); //+ $value->shipping_cost;
            }
            $discount = ($reedemcode && empty($reedemcode->order_id)) ? $reedemcode->total_discount : 0;
            // Charge a card.
            $charge = OmiseCharge::create(array(
              'amount'      => ($sum - $discount),
              'currency'    => 'thb',
              'card'        => $this->input->post('omiseToken') ? $this->input->post('omiseToken') : $this->input->post('omiseToken1')
            )); 

            // Authorized charge check
            if($charge['authorized']){

              if($this->input->post('custaddress')){
                $custaddress = $this->Customers_Addresses_model->get($this->input->post('custaddress'));
              }

              $addressdata = array(
                'user_id'           => $user,
                'receiver_name'     => $this->input->post('name'),
                'email'             => $this->input->post('email'),
                'phone'             => $this->input->post('contact_number'),
                'state'             => $this->input->post('choose_state') ? $this->input->post('choose_state') : $custaddress->state,
                'local_government'  => $this->input->post('local_government') ? $this->input->post('local_government') : $custaddress->district,
                'shipping_address'  => $this->input->post('shiping_address') ? $this->input->post('shiping_address') : $custaddress->address ,
                'delivery_method'   => is_null($this->input->post('delivery_option')) ? "" : $this->input->post('delivery_option'),
                'country'           => $this->input->post('countryname') ? $this->input->post('countryname') : $custaddress->country
              );

              $lastaddressID = $this->Address_model->insert($addressdata);

              $orderInfo = array(
                  "transaction_id"   => time(),
                  "user_id"          => $user,
                  "created_by"       => $this->input->post('seller'),
                  'page_id'          => "",
                  "shipping_address" => $lastaddressID,
                  "order_amount"     => ($sum - $discount),
                  "payment_method"   => "prepaid",
                  "status"           => "0",
                  "created_date"     => date('Y-m-d h:i:s'),
                  "store_type"       => $this->input->post('delivery_type'),
                  "transaction_ref"  => $charge['transaction']
              );

              $saved = $this->Orders_model->insert($orderInfo); // Save orders

              $orderdetails = $this->Orders_model->get($saved);

              // Update order id in coupon 
              if($reedemcode && empty($reedemcode->order_id))
              {
                $this->db->set('order_id',$orderdetails->transaction_id)
                 ->where('id',$reedemcode->id)
                 ->update('coupon_redemptions');
              }
              // END

              foreach ($Pids as $key => $value) {
                // Save orders with products
                $this->Orders_products_model->insert(array('order_id' => $orderdetails->transaction_id,'product_id' => $value,'ordered_qty' => $qty[$key]));
              }

              // Deduct commission and pay to akin 3.5% - 08-08-2018 - 17-10-2018
              $commission = ($sum * 6) / 100;
              $commissiondata = array(
                "order_id" => $saved,
                "akin_commission" => $commission,
                "auto_cancelled_fees" => "0",
                "transaction_type" => "Commission",
                "seller_id" => $this->input->post('seller')
              );

              $this->db->insert("commission_fees",$commissiondata);
              //END 

              // Remove user shopping cart products
              $this->Shopping_carts_model->force_delete(array('user_id' => $user));

              // Mail send
              $data['seller'] = $sellerinfo->admin_name;
              $data['guestname'] = $this->input->post('name');
              $data['totalamount'] = ($sum - $discount);
              $data['orderno'] = $orderdetails->transaction_id;
              $data['ordermethod'] = $this->input->post('delivery_type');
              $data['deliveryspeed'] = "";  
              $data['paymenttype'] = $orderdetails->payment_method;

              if ( ! defined('BASEPATH')) exit('No direct script access allowed');

              $mailConfig['mailpath'] = "/usr/sbin/sendmail";
              $mailConfig['protocol'] = "sendmail";
              $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
              $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
              $mailConfig['smtp_pass'] = "tg1<LvPL";
              $mailConfig['smtp_port'] = "25";
              $mailConfig['mailtype'] = "html";

              $this->load->library('email', $mailConfig);

              $body = $this->load->view('templates/guest_order_confirmed_template',$data,TRUE);
              $this->email->set_newline("\r\n");   
              $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
              $this->email->message($body); 
              $this->email->to($this->input->post('email'));
              $this->email->subject('Order Confirmation');

              $invoicepdfssss = $this->GenerateInvoice($orderdetails->transaction_id,$this->input->post('seller')); 

              $this->email->attach(FCPATH."uploads/invoices/".$invoicepdfssss);

              $qrcode = QrcodeofOrder($orderdetails->transaction_id);
              $eticket = GenerateTicket($orderdetails->transaction_id,$qrcode['qrcode'],$productslistnames);

              $this->email->attach(FCPATH."uploads/invoices/".$eticket);

              $send = $this->email->send();
              // END

              $this->session->set_flashdata('flashmsg', "<p>Your order has been confirmed.</p>");
              $this->session->set_flashdata('msg', 'success');
            
              redirect(base_url("item/order_confirm/".urlencode($orderdetails->transaction_id)));
            }
            else{
              $this->session->set_flashdata('flashmsg', "<p>* Payment has been failed.</p>");
              $this->session->set_flashdata('msg', 'error');
              redirect($_SERVER['HTTP_REFERER']);
            } 
        } 
        catch (OmiseException $e) {
            $this->session->set_flashdata('flashmsg', "<p>*".$e->getMessage()."</p>");
            $this->session->set_flashdata('msg', 'error');
            redirect($_SERVER['HTTP_REFERER']);
        }  
      }
      else{
        $this->session->set_flashdata('flashmsg', '<p>* Omise token not found for payment.</p>');
        $this->session->set_flashdata('msg', 'error');
        redirect($_SERVER['HTTP_REFERER']);
      }
    }

  }

  // Order history of customer   
  public function order_history(){

    if(!empty($this->session->userdata('customerId'))){
      $userID = $this->session->userdata('customerId');
    }
    else{
      $userID = $this->session->userdata("guest");
    }

    $config = array();
    $config["base_url"] = base_url() . "/item/order_history";


    $limit_per_page = 3;
    $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $total_records = $this->Orders_model->where('user_id',$userID)->count_rows();

    $config["total_rows"] = $total_records;
    $config["per_page"] = $limit_per_page;
    $config["uri_segment"] = 3;
    $config['use_page_numbers'] = TRUE;
    $config['num_links'] = $total_records;
    $config['cur_tag_open'] = '&nbsp;<a class="current">';
    $config['cur_tag_close'] = '</a>';
    $config['next_link'] = 'Next';
    $config['prev_link'] = 'Previous';

    $this->pagination->initialize($config);

    $data['content'] = 'global/order_history';
    $data['title'] = 'Order History';
    $data['results'] = orderlists_by_userid($userID,$limit_per_page, $start_index);

    $str_links = $this->pagination->create_links();
    $data["links"] = explode('&nbsp;',$str_links );
    $this->load->view('global/template', $data);

  }

  // Customer confirmed order - 06-08-2018
  public function confirmed_order_by_customer($orderID=""){
    $order = decrypt($orderID);

    $checkOrder = $this->Orders_model->where(array('id' => $order))->get();

    if($checkOrder && $checkOrder->status == '6'){
        $this->session->set_flashdata('flashmsg', '<p>Order has been already confirmed.</p>');
        $this->session->set_flashdata('msg', 'error');  
    }
    else{
      $updatestatus = array('status' => '6');
      $updated = $this->Orders_model->update($updatestatus,$checkOrder->id);

      if($updated){
        $this->session->set_flashdata('flashmsg', '<p>Thank you, Your Order has been confirmed successfully.</p>');
        $this->session->set_flashdata('msg', 'success');
      }
    }
    redirect($_SERVER['HTTP_REFERER']);
  }

  // Customer confirmed failed order - 06-08-2018
  public function failed_order_by_customer($orderID=""){
    //$config = emailconfig();
    //$this->load->library('email',$config);
    $data = array();
    $order = decrypt($orderID);
    $checkOrder = $this->Orders_model->where(array('id' => $order))->get();
    if($this->input->post()){
      $this->form_validation->set_rules('fail_reason','Fail Reason','trim|required',
                        array('required' => '* Please provide valid %s.')
                );
      if($this->form_validation->run() == true){
        if($checkOrder){
          if(empty($checkOrder->failed_reason) || is_null($checkOrder->failed_reason)){
            $submitted = $this->Orders_model->update(array("status" => '7',"failed_reason" =>$this->input->post('fail_reason')),$checkOrder->id);
            if($submitted){
              // Fail template send to customer
              $addressinfo = $this->db->select('*')->from('address')->where('id',$checkOrder->shipping_address)->get()->row();
              // Email send 
              $data['customername'] = $addressinfo->receiver_name;
              $data['orderno'] = $checkOrder->transaction_id;

              $body = $this->load->view('templates/confirmed_failed_template',$data,TRUE);

              if ( ! defined('BASEPATH')) exit('No direct script access allowed');

              $mailConfig['mailpath'] = "/usr/sbin/sendmail";
              $mailConfig['protocol'] = "sendmail";
              $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
              $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
              $mailConfig['smtp_pass'] = "tg1<LvPL";
              $mailConfig['smtp_port'] = "25";
              $mailConfig['mailtype'] = "html";

              $this->load->library('email', $mailConfig);

              $this->email->set_newline("\r\n");   
              $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
              $this->email->message($body); 
              $this->email->to($addressinfo->email);
              $this->email->subject('Complete Failed Order Confirmation');

              $this->email->send();

             /* $body = $this->load->view('templates/confirmed_failed_template',$data,TRUE);
              $this->email->set_newline("\r\n");   
              $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
              $this->email->message($body); 
              $this->email->to($addressinfo->email);
              $this->email->subject('Failed Order Confirmation');
              $this->email->send();*/
              // End
              $this->session->set_flashdata('flashmsg', '<p>Thank you, We will get back to you soon.</p>');
              $this->session->set_flashdata('msg', 'success');
            }
          } 
          else{
            $this->session->set_flashdata('flashmsg', '<p>Sorry, You already submitted failed reason.</p>');
            $this->session->set_flashdata('msg', 'error');
          }
        }
        redirect($_SERVER['HTTP_REFERER']);
      }
    }
  }

  // Order details page
  public function order_details($orderID){
    $orderdecrypt = urldecode($orderID);
    $orders = orderlists($orderID,"","");
    $data['content'] = 'global/order_details';
    $data['title'] = 'Order Details';
    $data['orders'] = $orders;
    $this->load->view('global/template', $data); 
  }

  // Save failed reason in orders of customers
  public function submitReason(){
    $data = array();
   // $config = emailconfig();
   // $this->load->library('email',$config);

    $reason = str_replace("+", " ", $this->input->post('reason'));

    $checkOrder = $this->Orders_model->where(array('id' => $this->input->post('order')))->get();
    $submited = $this->Orders_model->update(array("status" => '7',"failed_reason" => $reason),$this->input->post('order'));

    if($submited){
      // Fail template send to customer
      $addressinfo = $this->db->select('*')->from('address')->where('id',$checkOrder->shipping_address)->get()->row();
      // Email send 
      $data['customername'] = $addressinfo->receiver_name;
      $data['orderno'] = $checkOrder->transaction_id;

      $body = $this->load->view('templates/confirmed_failed_template',$data,TRUE);

      if ( ! defined('BASEPATH')) exit('No direct script access allowed');

      $mailConfig['mailpath'] = "/usr/sbin/sendmail";
      $mailConfig['protocol'] = "sendmail";
      $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
      $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
      $mailConfig['smtp_pass'] = "tg1<LvPL";
      $mailConfig['smtp_port'] = "25";
      $mailConfig['mailtype'] = "html";

      $this->load->library('email', $mailConfig);

      $this->email->set_newline("\r\n");   
      $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
      $this->email->message($body); 
      $this->email->to($addressinfo->email);
      $this->email->subject('Complete Failed Order Confirmation');

      $this->email->send();
      // End
      echo json_encode(array("msg" => TRUE));
    }
    else{
      echo json_encode(array("msg" => FALSE));
    }
    exit;
  }

  // Get states by country
  public function GetstateBycountry()
  {
    $country = $this->input->post('countryname');

    $states = GetStatesByCountry($country);
    echo json_encode(array("msg" => true,"liststates" => $states));  
    exit;
  }

  // Ajax based order confirmed order - 24-09-2018
  public function ajax_order_confirm_by_id(){
    $order = $this->input->post('orderID');

    $checkOrder = $this->Orders_model->where(array('id' => $order))->get();

    if($checkOrder && $checkOrder->status == '6'){
        echo json_encode(array("status" => FALSE,"msg" => "* Error: Order has been already confirmed."));
    }
    else{
      $updatestatus = array('status' => '6','tracking' => '0');
      $updated = $this->Orders_model->update($updatestatus,$checkOrder->id);

      if($updated){
         echo json_encode(array("status" => TRUE,"msg" => "Thank you, Your Order has been confirmed successfully!"));
      }
    }
    exit;
  }

  // Ajax based review rating of order 24-09-2018
  public function ajax_order_rating_by_id(){
    $rate = $this->input->post('orderRate');
    $orderid = $this->input->post('orderID');

    $checkrecord = $this->Orders_model->where(array('id' => $orderid))->get();

    if($checkrecord){

      if(empty($checkrecord->order_rate)){
       
        $updateRate = array("order_rate" => $rate);

        $this->Orders_model->update($updateRate,$checkrecord->id);
        echo json_encode(array("status" => TRUE,"msg" => "Thank you for feedback.")); 
      }
      else{
        echo json_encode(array("status" => FALSE,"msg" => "* Error: Sorry, You already given rating"));
      }
    }
    else{
      echo json_encode(array("status" => FALSE,"msg" => "* Error: Sorry, Order not found"));
    }
    exit;
  }

 // My order page
  public function orders(){
      if(!empty($this->session->userdata('customerId'))){
        $userID = $this->session->userdata('customerId');
      }
      else{
        $userID = $this->session->userdata("guest");
      }

      $config = array();
      $config["base_url"] = base_url() . "/item/order_history";

      $limit_per_page = 10;
      $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $total_records = $this->Orders_model->where('user_id',$userID)->count_rows();

      $config["total_rows"] = $total_records;
      $config["per_page"] = $limit_per_page;
      $config["uri_segment"] = 3;
      $config['use_page_numbers'] = TRUE;
      $config['num_links'] = $total_records;
      $config['cur_tag_open'] = '&nbsp;<a class="current">';
      $config['cur_tag_close'] = '</a>';
      $config['next_link'] = 'Next';
      $config['prev_link'] = 'Previous';

      $this->pagination->initialize($config);

      $str_links = $this->pagination->create_links();
      $data["links"] = explode('&nbsp;',$str_links );

      $data['content'] = 'global/my_order';
      $data['title'] = 'My Orders';  
      $data['results'] = orderlists_by_userid($userID,$limit_per_page,$start_index);  
      $this->load->view('global/template', $data);
  }

  // Ajax based product rating by order - 24-09-2018
  public function ajax_product_rating_by_order(){
    $rate = $this->input->post('orderRate');
    $orderid = $this->input->post('orderID');
    $pid = $this->input->post('productID');
    $sellerid = $this->input->post('sellerID');

    $checkrecord = $this->db->select('rating')->from('products_ratings')->where("product_id",$pid)->where('order_id',$orderid)->get()->result();

    if(count($checkrecord) == 0){
      $prates = array(
        "order_id" => $orderid,
        "product_id"  => $pid,
        "customer_id" => $this->session->userdata("customerId"),
        "seller_id" => $sellerid,
        "rating" => $rate
      );

      $saved = $this->db->insert("products_ratings",$prates);

      if($saved){
        echo json_encode(array("status" => TRUE,"msg" => "Thank you for rating {$rate} star.")); 
      }
      else{
        echo json_encode(array("status" => FALSE,"msg" => "Something went wrong.")); 
      }  
    }
    else{
      echo json_encode(array("status" => FALSE,"msg" => "* Error: Sorry, You already rated this product"));
    }
    exit;
  }

  // Order not confirm track by orderID
  public function ajax_order_notconfirm_trackby_id(){
    $order = $this->input->post('orderID');

    $checkOrder = $this->Orders_model->where(array('id' => $order))->get();

    if($checkOrder){

        if($checkOrder->tracking == '0'){
          $updateTrack = array("tracking" => "1");
          $this->Orders_model->update($updateTrack,$checkrecord->id);
          echo json_encode(array("status" => TRUE,"msg" => "Success: Order has been tracked."));
        }
        else{
          echo json_encode(array("status" => FALSE,"msg" => "Info: Order has already been tracked."));
        }
    }
    else{
       echo json_encode(array("status" => FALSE,"msg" => "* Error: Order not found"));
    }
    exit;
  }
  
  /*public function encrypt_decrypt($string) {
   
    $string_length=strlen($string); 
    $encrypted_string=""; 
    for ($position = 0;$position<$string_length;$position++){         
        $key = (($string_length+$position)+1);
        $key = (255+$key) % 255;
        $get_char_to_be_encrypted = SUBSTR($string, $position, 1); 
        $ascii_char = ORD($get_char_to_be_encrypted); 
        $xored_char = $ascii_char ^ $key;  //xor operation 
        $encrypted_char = CHR($xored_char); 
        $encrypted_string .= $encrypted_char; 
    } 
    return $encrypted_string; 
 }*/

}
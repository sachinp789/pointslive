<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userengagement extends MY_Controller {

	public $pageID="";

	public function __construct(){
		parent::__construct();
	    $this->load->library('form_validation');
	    $this->load->model('Userengagements_model');
      $this->load->model('Stores_model');
	    $this->load->library('upload');
      $this->pageID = $this->session->userdata('pagemanager');
	}

	// Add schedular
    public function newSchedular() {
        if(!empty($this->session->userdata('isLogin'))){
          $data['hashtags'] = $this->db->select('*')->from('hashtags')->where('created_by',$this->session->userdata('adminId'))->get()->result();
        }
        else{
          $page_id = is_null($this->pageID) ? '1048681488539732' : $this->pageID;  //145654016010640 - knookest
          $data['hashtags'] = $this->db->select('*')->from('hashtags')->where('page_id',$page_id)->get()->result();
        }
        $data['content'] = 'admin/userengagements/addscheduler';
        $data['title'] = 'Create Social Media Post';
        $data['page'] = $this->pageID;
        $this->load->view('admin/template', $data);
    }

    // Save schedular
    public function saveSchedular(){
    	   // For vendor
        /*if(!empty($this->session->userdata('isLogin'))){
            $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();

            if(!$pages){
              $this->session->set_flashdata('flashmsg', 'Store setup is required');
              $this->session->set_flashdata('msg', 'danger');
              redirect(base_url('categories/newCategory'));
            }
        }*/
        if ($this->input->post()) {

            $this->form_validation->set_rules('choose_item', 'Choose Product', 'required');
            $this->form_validation->set_rules('choose_media', 'Choose Media', 'required');
            //$this->form_validation->set_rules('choose_schedular', 'Choose Schedular', 'required');
           // $this->form_validation->set_rules('share_on', 'Share On', 'required');
           /* if(!empty($this->input->post('event_name'))){
            	$this->form_validation->set_rules('event_name', 'Event Name', 'required');	
            }*/
            $this->form_validation->set_rules('post_description', 'Description', 'required');

            if($this->input->post('choose_item') == "events"){
                $id = $this->input->post('schedularproduct');
                $postimage = $this->input->post($id);
                if(!is_null($id)){
                    $postimage = $this->input->post($id); 
                }else{
                    $postimage = "";
                } 
            }else{
                $postimage = "";
            }
           
            if ($this->form_validation->run() == true) {
                $data = [
                    'choose_item' => $this->input->post('choose_item'),
                    'media' => $this->input->post('choose_media'),
                    'product_id' => is_null($this->input->post('schedularproduct')) ? 0 : $this->input->post('schedularproduct'),
                    'event_name' => $this->input->post('event_name'),
                    //'sharing_frequency' => $this->input->post('choose_schedular'),
                    'post_image'    => $postimage, 
                    'share_on' => $this->input->post('share_on'),
                    'post_description' => $this->input->post('post_description'),
                    'hashtags' => implode(" ",$this->input->post('hashtags')),/*$this->input->post('hashtags'),*/
                    //'trending_hashtags' => implode(" ", $this->input->post('trendhashtags')),
                    //'influence_tags' => implode(" ", $this->input->post('influencers')),
                    //'emojis' => base64_encode(implode(" ", $this->input->post('emoji'))),
                    'events_buylink' => $this->input->post('shorturl'),//events_shortlink
                    'created_by' => $this->session->userdata('adminId')
                   // 'video_link' => $this->input->post('video_link'),
                    //'facebook_page' => is_null($this->pageID) ? '145654016010640' : $this->pageID
                ];

                if(!empty($this->session->userdata('isLogin'))){
                    $data['facebook_page'] = $this->input->post('facebook_page');
                }
                else{
                    $data['facebook_page'] = $this->input->post('facebook_page');//is_null($this->pageID) ? '145654016010640' : $this->pageID;
                }

                if($this->input->post('trendhashtags')){
                    $data['trending_hashtags'] = implode(" ", $this->input->post('trendhashtags'));
                }
                if($this->input->post('influencers')){
                    $data['influence_tags'] = implode(" ", $this->input->post('influencers'));
                }
                if($this->input->post('emoji')){
                    $data['emojis'] = base64_encode(implode(" ", $this->input->post('emoji')));
                }
                //$this->input->post('facebook_page')
               // die($data);

              // Video or link save / upload
              if($this->input->post('choose_media') == "video"){
                  if(!empty($_FILES['video_upload']['name'])){
                    $config['upload_path'] = './uploads/posts';
                    $config['allowed_types'] = 'mp4';
                    $config['max_size'] = 10240; // 10mb / 1024kb
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('video_upload')) {
                        $fileData = $this->upload->data();
                        $data['video_file'] = $fileData['file_name'];
                    } else {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('flashmsg', $error);
                        $this->session->set_flashdata('msg', 'danger');
                        redirect(base_url() . 'userengagement/newSchedular');
                    }    
                  }
                  else{
                     $data['video_link'] = $this->input->post('video_link'); 
                  } 
              }

	            /*if(!empty($_FILES['post_image']['name'])) {
		          $config['upload_path'] = './uploads/events';
		          $config['allowed_types'] = 'gif|jpg|png|jpeg';
		          $config['max_size'] = 5120;
		          $this->upload->initialize($config);
		          if ($this->upload->do_upload('post_image')) {
		              $fileData = $this->upload->data();
		              $data['post_image'] = $fileData['file_name'];
		          } else {
		              $error = $this->upload->display_errors();
		              $this->session->set_flashdata('flashmsg', $error);
		              $this->session->set_flashdata('msg', 'danger');
		              redirect(base_url() . 'userengagement/newSchedular');
		          }
	            }*/

              /*$page = $this->input->post('facebook_page');//is_null($this->pageID) ? '145654016010640' : $this->pageID;

              // 25-07-2018 - Nigeria time set
              $token = getFacebookpagetoken($page);

              $getloc = json_decode(file_get_contents("https://graph.facebook.com/{$page}?fields=location&access_token={$token[0]->access_token}"));*/

              if(count($this->input->post('share_on')) > 0){
                    for ($i=0; $i < count($this->input->post('share_on')) ; $i++) { 

                         $data['share_on'] = $this->input->post('share_on')[$i];   
                         $lastID = $this->Userengagements_model->insert($data); 
                         
                         foreach ($this->input->post('sharing_timeslot') as $key => $value) {
                                /*if(isset($getloc->location)){
                                   $date = new DateTime($value);
                                   if($getloc->location->country == "United States"){
                                    $date->setTimezone(new DateTimeZone('America/'.$getloc->location->city));
                                   } 
                                   else if($getloc->location->country == "Thailand"){
                                    $date->setTimezone(new DateTimeZone('Asia/'.$getloc->location->city));
                                   }
                                   else if($getloc->location->country == "Australia"){
                                    $date->setTimezone(new DateTimeZone('Australia/'.$getloc->location->city));
                                   }
                                   else if($getloc->location->country == "India"){
                                    $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                                   }else{
                                    $date->setTimezone(new DateTimeZone('Asia/Bangkok'));
                                   }
                                   $dates = $date->format('m/d/Y h:i A');
                                }
                                else{
                                   $date = new DateTime($value);
                                   $date->setTimezone(new DateTimeZone('Asia/Bangkok'));
                                   //$date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                                   $dates = $date->format('m/d/Y h:i A');
                                }*/

                             $date = new DateTime($value);
                             //var_dump($value);exit;
                             $date->setTimezone(new DateTimeZone('Africa/Lagos'));
                             //$date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                             $dates = $date->format('m/d/Y h:i A');  

                              $schedulardata = array(
                              'parent_id' => $lastID,
                              'sharing_time' => $value,
                              'datepicker_time' => $value 
                            );
                         $this->db->insert('schedulars',$schedulardata);
                        }
                         
                    }   
                }

                //$lastID = $this->Userengagements_model->insert($data); 


    	        	/*foreach ($this->input->post('sharing_timeslot') as $key => $value) {
                        if(isset($getloc->location)){
                           $date = new DateTime($value);
                           if($getloc->location->country == "United States"){
                            $date->setTimezone(new DateTimeZone('America/'.$getloc->location->city));
                           } 
                           else if($getloc->location->country == "Thailand"){
                            $date->setTimezone(new DateTimeZone('Asia/'.$getloc->location->city));
                           }
                           else if($getloc->location->country == "Australia"){
                            $date->setTimezone(new DateTimeZone('Australia/'.$getloc->location->city));
                           }
                           else if($getloc->location->country == "India"){
                            $date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                           }else{
                            $date->setTimezone(new DateTimeZone('Asia/Bangkok'));
                           }
                           $dates = $date->format('m/d/Y h:i A');
                        }
                        else{
                           $date = new DateTime($value);
                           $date->setTimezone(new DateTimeZone('Asia/Bangkok'));
                           //$date->setTimezone(new DateTimeZone('Asia/Kolkata'));
                           $dates = $date->format('m/d/Y h:i A');
                        }
                  		$schedulardata = array(
      		        		'parent_id' => $lastID,
      		        		'sharing_time' => $dates,
                      'datepicker_time' => $value	
      		        	);
                 $this->db->insert('schedulars',$schedulardata);
    		        }*/   
              	$this->session->set_flashdata('flashmsg','Schedular has been created.');
            		$this->session->set_flashdata('msg', 'success');
                redirect(base_url() . 'userengagement/newSchedular'); 
            }
         	$data['content'] = 'admin/userengagements/addscheduler';
        	$data['title'] = 'Add Scheduler';
        	$this->load->view('admin/template', $data);
        }
    }

     // Edit schedular
    public function editSchedular($schedular_id = "") {
	    $data['schedulars'] = $this->Userengagements_model->get(['id' => $schedular_id]);
	    $data['schedularstimes'] = $this->db->select('*')
	    						  ->from('schedulars')	
	    						  ->where('parent_id', $schedular_id)
	    						  ->get()->result();
        $data['content'] = 'admin/userengagements/editscheduler';
	    $data['title'] = 'Edit Scheduler';
	    $data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);
    }

    // Update schedulars
    public function updateSchedular(){
    	if ($this->input->post()) {

            $this->form_validation->set_rules('choose_item', 'Choose Product', 'required');
            //$this->form_validation->set_rules('choose_schedular', 'Choose Schedular', 'required');
            $this->form_validation->set_rules('share_on', 'Share On', 'required');
            if(!empty($this->input->post('event_name'))){
            	$this->form_validation->set_rules('event_name', 'Event Name', 'required');	
            }
            $this->form_validation->set_rules('post_description', 'Description', 'required');
           
            if ($this->form_validation->run() == true) {
                $data = [
                    'choose_item' => $this->input->post('choose_item'),
                    'product_id' => $this->input->post('schedularproduct'),
                    'event_name' => $this->input->post('event_name'),
                    //'sharing_frequency' => $this->input->post('choose_schedular'),
                    'share_on' => $this->input->post('share_on'),
                    'post_description' => $this->input->post('post_description'),
                    'events_buylink' => $this->input->post('shorturl'),//events_buylink
                    'video_link' => $this->input->post('video_link'),
                    'facebook_page' => is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID
                ];
               // die($data);
	            if(!empty($_FILES['post_image']['name'])) {
		          $config['upload_path'] = './uploads/events';
		          $config['allowed_types'] = 'gif|jpg|png|jpeg';
		          $config['max_size'] = 5120;
		          $this->upload->initialize($config);
		          if ($this->upload->do_upload('post_image')) {
		          	  unlink("./uploads/events/" . $this->input->post('post_image_exists'));	
		              $fileData = $this->upload->data();
		              $data['post_image'] = $fileData['file_name'];
		          } else {
		              $error = $this->upload->display_errors();
		              $this->session->set_flashdata('flashmsg', $error);
		              $this->session->set_flashdata('msg', 'danger');
		              redirect(base_url() . 'userengagement/newSchedular');
		          }
	            }else if($this->input->post('choose_item') == 'products'){

                    $data['post_image'] = '';
                    $data['event_name'] = '';
                    unlink("./uploads/events/" . $this->input->post('post_image_exists'));
                }

                $updated = $this->Userengagements_model->update($data,$this->input->post('schedular_id'));

                $this->db->delete('schedulars', array('parent_id' => $this->input->post('schedular_id')));

	        	foreach ($this->input->post('sharing_timeslot') as $key => $value) {
		        	$schedulardata = array(
		        		'parent_id' => $this->input->post('schedular_id'),
		        		'sharing_time' => $value	
		        	);
		        	$this->db->insert('schedulars',$schedulardata);
		        }
               
            	$this->session->set_flashdata('flashmsg','Schedular has been updated.');
          		$this->session->set_flashdata('msg', 'success');
                
             	redirect(base_url() . "userengagement/editSchedular/".$this->input->post('schedular_id'));
            }
        }
    }

    // List of schedulars
    public function getSchedularLists(){
      if(!empty($this->session->userdata('isLogin'))){
         $data['schedulars'] = $this->Userengagements_model->where(['created_by' => $this->session->userdata('adminId')])->get_all(); 
      }
      else if(is_null($this->pageID)){
        //14-06-2018    
        //$pageid = is_null($this->pageID) ? '145654016010640' : $this->pageID;
        //$data['schedulars'] = $this->Userengagements_model->where(['facebook_page' => $pageid])->get_all();
         $data['schedulars'] = $this->Userengagements_model->get_all();
      }
      else{
        $data['schedulars'] = $this->Userengagements_model->where(['facebook_page' => $this->pageID])->get_all();
      }
      $data['content'] = 'admin/userengagements/listschedulers';
	    $data['title'] = 'All Schedulers';
	    $data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);
    }

    // View schedular
    public function viewSchedular($sechID){
        $data['schedulars'] = $this->Userengagements_model->where(['id' => $sechID])->get_all();
        $data['content'] = 'admin/userengagements/viewschedulers';
        $data['title'] = 'View Scheduler';
        $data['page'] = $this->pageID;
        $this->load->view('admin/template', $data);
    }

    // Get products with ajax
    public function getData(){
    	$this->load->model('Products_model');
    	
    	$type = $_POST['type'];

      //$pageid = is_null($this->pageID) ? '145654016010640' : $this->pageID;
      //$pagedefault = is_null($this->pageID) ? '1048681488539732' : $this->pageID;
    	if(!is_null($type) && $type == "products"){
    		    
            if($this->session->userdata('isLogin') == 'social'){
                //$pageID = $this->input->post('page');
                $productsdata = $this->Products_model->fields('product_name,product_id')->where(['created_by' => $this->session->userdata('adminId')])->get_all();
            }
            else{
                $productsdata = $this->Products_model->fields('product_name,product_id')->get_all();
                //$pageID = $this->input->post('page');//is_null($this->pageID) ? '1048681488539732' : $this->pageID;
            } 
           // $pageid = is_null($this->pageID) ? '1048681488539732' : $this->pageID;

         /* if(empty($pageID)){
            $productsdata = $this->Products_model->fields('product_name,product_id')->where(['created_by' => $this->session->userdata('adminId')])->get_all();
          }else{

      		  $productsdata = $this->Products_model->fields('product_name,product_id')->where(['facebook_page' => $pageID])->get_all();
          } */
      		echo json_encode(array("msg" => true,"products" => $productsdata,"type" => "products"));
            //exit;
    	 }
       else if(!is_null($type) && $type == "events"){
          
          $pageID = '1048681488539732';//$this->input->post('page');
          $token = getFacebookpagetoken($pageID);
          /*if($this->session->userdata('isLogin') == 'social'){
            $token = getFacebookpagetoken($this->input->post('page'));
          }
          else{
            $pageID = $this->input->post('page');
            $token = getFacebookpagetoken($pageID); 

          } 
          
          if($pageID == "1048681488539732"){
            $accesstoken = "EAADZBuJzbhGoBAHoebfaGekWZAZCHZAuX8OYqhebVxZCT8zBIZAVsQw1spAV2yo9GIuTYtcwQoDXYXP5PTZCgjEKNPQsZAFJ30WYiBJudX9gsvemLZCUHzR4kqtdsMaThORK1prfzGQKM14BDk4nULURkuVywI2HkFpwZD";
          } 
          else{
            $accesstoken = $token[0]->access_token;
          }*/

         /* $token = getFacebookpagetoken($pageid); 
          if($pageid == "1048681488539732"){
            $accesstoken = "EAADZBuJzbhGoBAHoebfaGekWZAZCHZAuX8OYqhebVxZCT8zBIZAVsQw1spAV2yo9GIuTYtcwQoDXYXP5PTZCgjEKNPQsZAFJ30WYiBJudX9gsvemLZCUHzR4kqtdsMaThORK1prfzGQKM14BDk4nULURkuVywI2HkFpwZD";
          } 
          else{
            $accesstoken = $token[0]->access_token;
          }*/
         // var_dump($pageid);
          $url = "https://graph.facebook.com/{$pageID}/events?fields=name,description,id,photos{picture},cover&time_filter=upcoming&access_token=".$token[0]->access_token; 
          //echo $url;
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
          curl_setopt($ch,CURLOPT_URL,$url);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
          $content = curl_exec($ch);
          $result = json_decode($content);
          curl_close($ch);

          if(isset($result->error)){
            $path = __DIR__.'\Userengagement.php function getData';
            $line = 346;
            apilogger($result->error->message,$path,$line);
            echo json_encode(array("msg" => true,"events" => "","type" => "events"));
          }
          else{
            echo json_encode(array("msg" => true,"events" => $result,"type" => "events")); 
          }

          //exit;
        }
    	else{
    	  echo json_encode(array("msg" => false));
          //exit;
    	}
    }

    // Remove sechdular
    public function removeProduct($sechID=""){
        if(!is_null($sechID)){

            $getDetails = $this->Userengagements_model->where('id',$sechID)->get();
            $filename = "./uploads/posts/" . $getDetails->video_file;

             // Video delete uploaded
            if (file_exists($filename)) {
                unlink("./uploads/posts/" . $getDetails->video_file);
            }
            
            // Delete multiple table records
            $this->Userengagements_model->delete(['id' => $sechID]);
            $this->db->delete('schedulars', array('parent_id' => $sechID));
    
            $this->session->set_flashdata('flashmsg','Schedular has been deleted.');
            $this->session->set_flashdata('msg', 'success');        
        }
        redirect(base_url() . "userengagement/getSchedularLists");
    }

    // generatae short link
    public function generateUrl(){
        $shorturl = $_POST['shorturl']; 

        if(empty($shorturl)){
            echo json_encode(array("msg" => false));
        }
        else{
            /*$shorturl = $this->make_bitly_url("{$shorturl}","{$this->config->item('bitlyusername')}","{$this->config->item('bitlytapikey')}",'json');
            echo json_encode(array("msg" => true,"url" => $shorturl));*/
            $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";
            $ritlyurl = $this->make_ritly_url($postdata);
            if(is_null($ritlyurl)){
                echo json_encode(array("msg" => false,'url' => ""));
            }
            else{
                echo json_encode(array("msg" => true,"url" => $ritlyurl));
            }
        } 
        exit;   
    }

    // Generate ritly short url
    public function make_ritly_url($postdata){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_URL,$postdata);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $content = json_decode($response);
        curl_close($ch);

        if($http == 200){
            return $content->url;
        }
        else{
            $path = __DIR__.'\Userengagement.php function make_ritly_url';
            $line = 471;
            apilogger($content,$path,$line);
            return $content;
        }

    }

        /* make a URL small */
    /*public function make_bitly_url($url,$login,$appkey,$format = 'xml',$version = '2.0.1')
    {
        //create the URL
        $bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;
        
        //get the url
        //could also use cURL here
        $response = file_get_contents($bitly);
        
        //parse depending on desired format
        if(strtolower($format) == 'json')
        {
            $json = @json_decode($response,true);
            return $json['results'][$url]['shortUrl'];
        }
        else //xml
        {
            $xml = simplexml_load_string($response);
            return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
        }
    }*/

    public function generateRitehashTags(){
        $message = $_POST['description']; 

        if(empty($message)){
            echo json_encode(array("msg" => false));
        }
        else{
            /*$shorturl = $this->make_bitly_url("{$shorturl}","{$this->config->item('bitlyusername')}","{$this->config->item('bitlytapikey')}",'json');*/
            $postdata = "https://api.ritekit.com/v1/stats/auto-hashtag?post={$message}&client_id={$this->config->item('client_id')}&maxHashtags=4";
            $ritlyhashtags = $this->make_ritly_hashtags($postdata);
            if(is_null($ritlyhashtags)){
                echo json_encode(array("msg" => true,'tags' => ""));
            }
            else{
                echo json_encode(array("msg" => true,"tags" => $ritlyhashtags));
            }
        } 
        exit;   
    }

    // Generate ritly hastags
    public function make_ritly_hashtags($postdata){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_URL,$postdata);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $content = json_decode($response);
        curl_close($ch);

        if(!$content->result){
          $path = __DIR__.'\Userengagement.php function make_ritly_hashtags';
          $line = 538;
          apilogger($content->message,$path,$line);
        }

        if($http == 200){
            return $content->post;
        }
        else{
            return $content;
        }

    }

    // Genrate trending hashtags
    public function get_trendinghashtag(){
        
        $trend = array();

        $postdata = "https://private-82bd7-ritekit.apiary-proxy.com/v1/search/trending?green=1&latin=1&client_id={$this->config->item('client_id')}";
       
        $ch = curl_init($postdata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $content = json_decode($response);
        curl_close($ch);

        if(!$content->result){
          $path = __DIR__.'\Userengagement.php function get_trendinghashtag';
          $line = 565;
          apilogger($content->message,$path,$line);
        }

        if($http == 200){
            $counter = 0;
            foreach ($content->tags as $value) {
                if($counter > 9) break;
                $trend[] = $value->tag;
                $counter++;
            }
            echo json_encode(array("msg" => true,'tags' => $trend));
            exit;
        }
        else{
            echo json_encode(array("msg" => false));
            exit;
        }
    }

    // Make Influencers
    public function make_influencers(){
        $influ = array();

        $tag = str_replace("#", "", $_REQUEST['hashtag']);

        $postdata = "https://private-82bd7-ritekit.apiary-proxy.com/v1/influencers/hashtag/{$tag}?client_id={$this->config->item('client_id')}";
       
        $ch = curl_init($postdata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $content = json_decode($response);
        curl_close($ch);

        if(!$content->result){
          $path = __DIR__.'\Userengagement.php function make_influencers';
          $line = 602;
          apilogger($content->message,$path,$line);
        }

        if($http == 200){
            $counter = 0;
            foreach ($content->influencers as $value) {
                if($counter > 2) break;
                $influ[] = $value->username;
                $counter++;
            }
            echo json_encode(array("msg" => true,'influencers' => $influ));
            exit;
        }
        else{
            echo json_encode(array("msg" => false));
            exit;
        }
    }

    // Make Emojis
    public function make_emojis(){

        $suggest = $_REQUEST['text'];

        $url = "https://private-82bd7-ritekit.apiary-proxy.com/v1/emoji/suggestions";

        $dataArray = array(
            "text" => $suggest,
            "client_id" => $this->config->item('client_id')
        );

        $ch = curl_init();
        $data = http_build_query($dataArray);
        $getUrl = $url."?".$data;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
         
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         
        if(curl_error($ch)){
            $path = __DIR__.'\Userengagement.php function make_emojis';
            $line = 640;
            apilogger(curl_error($ch),$path,$line);
            echo 'Request Error:' . curl_error($ch);
        }
        else
        {
            $content = json_decode($response);
        }
        curl_close($ch);
       
        if($http == 200){
            echo json_encode(array("msg" => true,'emojis' => $content->emojis));
            unset($suggest);
            exit;
        }
        else{
            echo json_encode(array("msg" => false));
            exit;
        }
    }

    // Make Hashtag suggestion
    public function get_hashtagsuggestion(){

        $tag = $_REQUEST['metadata'];
       
        $cleantag = str_replace(' ','%20',str_replace("#", "", implode(" ",$tag)));

        $postdata = "https://private-82bd7-ritekit.apiary-proxy.com/v1/stats/hashtag-suggestions/{$cleantag}?client_id={$this->config->item('client_id')}";

        $ch = curl_init($postdata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $content = json_decode($response);
        curl_close($ch);

        if(!$content->result){
          $path = __DIR__.'\Userengagement.php function get_hashtagsuggestion';
          $line = 687;
          apilogger($content->message,$path,$line);
        }

        usort($content->data, function($a, $b) { //Sort the array using a user defined function
            return $a->exposure > $b->exposure ? -1 : 1; //Compare the scores
        });  

        if($http == 200){
            echo json_encode(array("msg" => true,'suggestions' => $content->data));
            exit;
        }
        else{
            echo json_encode(array("msg" => false));
            exit;
        }
    }

}
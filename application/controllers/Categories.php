<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Categories extends MY_Controller {


  public $pageID="";
	public function __construct(){

		parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Categories_model');
    $this->load->model('Product_categories_model');
    $this->load->model('Product_images_model');
    $this->load->model('Stores_model');
    $this->load->model('Products_model');
    $this->pageID = $this->session->userdata('pagemanager');

	}

  // Add new category

  public function newCategory(){

    $data['content'] = 'admin/categories/addcategory';

    $data['title'] = 'Add New Category';
    $data['page'] = $this->pageID;
    $this->load->view('admin/template', $data);

  } 

  // Save categories

  public function saveCategory(){

    $data = array();
    $pdata = array();
    $this->load->library('ciqrcode');
      // For vendor
    /* if(!empty($this->session->userdata('isLogin'))){
        $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();

        if(!$pages){
          $this->session->set_flashdata('flashmsg', 'Store setup is required');
          $this->session->set_flashdata('msg', 'danger');
          redirect(base_url('categories/newCategory'));
        }
     }*/

     if($this->input->post() && !empty($_FILES['category_image']['name'])){

      $this->form_validation->set_rules('category_name','Category Name','trim|xss_clean|required');



        if($this->form_validation->run() == true) {

          $data = [

            'category_name' => ucwords($this->input->post('category_name')),

            'category_name_thai' => $this->input->post('category_name_thai'),
            'page_id' => $this->input->post('facebook_page'),//is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID,
            'created_by' => $this->session->userdata('adminId')

          ];

          /******************* 31-01-2018 Sachin ************************/
          $config['upload_path'] = './uploads/categories';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = 5120;
          //$config['file_name'] = $fileName;
          $this->upload->initialize($config);
          if ($this->upload->do_upload('category_image')) {
              $fileData = $this->upload->data();
              $data['category_image'] = $fileData['file_name'];
              $catID = $this->Categories_model->insert($data);

              // Short link for website details page for products - 05-07-2018
              $seller = $this->session->userdata('adminId'); // Remove encryption
              $category = $catID; // Remove encryption

              $shorturl = base_url()."item/products/{$category}/{$seller}";

              $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

              $ch = curl_init();
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
              curl_setopt($ch,CURLOPT_URL,$postdata);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
              $response = curl_exec($ch);
              $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
              $content = json_decode($response);

              if(!$content || empty($content)) {
                $path = __DIR__.'\Categories.php function saveCategory';
                $line = 43;
                apilogger("Error while generating short-link",$path,$line);
              }
              
              curl_close($ch);
              if(isset($content->status) == 'error'){      
                  $path = __DIR__.'\Categories.php function saveCategory';
                  $line = 101;
                  apilogger($content->message,$path,$line);
              }              

              if($content->result == true){
                  $pdata['global_link'] = $content->url; 
                  $qr_image=rand().'.png';
                  $params['data'] = $content->url;//$this->input->post('finallink');
                  $params['level'] = 'H';
                  $params['size'] = 8;
                  $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
                  if($this->ciqrcode->generate($params))
                  {
                      $data['qrcode']=$qr_image; 
                  }    
              }
              else{
                  $pdata['global_link'] = "";
                  $data['qrcode'] = "";
              }

              $this->Categories_model->update($pdata, $catID);

              $this->session->set_flashdata('flashmsg','New category has been created.');
              $this->session->set_flashdata('msg', 'success');
          } else {
              $error = $this->upload->display_errors();
              $this->session->set_flashdata('flashmsg', $error);
              $this->session->set_flashdata('msg', 'danger');
          }

          redirect(base_url().'categories/newCategory');

          /*$this->Categories_model->new_category($data);

          $this->session->set_flashdata('flashmsg','New category has been created.');

          $this->session->set_flashdata('msg', 'success');

          redirect(base_url().'categories/newCategory');*/

        }

        $data['content'] = 'admin/categories/addcategory';

        $data['title'] = 'Add New Category';

        $this->load->view('admin/template', $data);

    }

    

  }



  // Get all categories 

  public function getCatgories(){

      if(!empty($this->session->userdata('isLogin'))) {
         $categories = $this->Categories_model->where('created_by',$this->session->userdata('adminId'))->fields('category_id,category_name,category_image,global_link')->get_all(); 
      }
      else{
        
        if(is_null($this->pageID)){
          $categories = $this->Categories_model->fields('category_id,category_name,category_image,global_link')->get_all();
        }
        else{
          $categories = $this->Categories_model->where('page_id',$this->pageID)->fields('category_id,category_name,category_image,global_link')->get_all(); 
        }
      }
      //$categories = $this->Categories_model->fields('category_id,category_name,category_image')->get_all();

      $data['categories'] = $categories;

      $data['content'] = 'admin/categories/all_categories';

      $data['title'] = 'All Categories';
      $data['page'] = $this->pageID;

      $this->load->view('admin/template', $data);

  }



  // Edit new category

  public function editCategory($category_id=""){

    $category_details = $this->Categories_model->get($category_id);

    if(!empty($category_details)){

        $data['category_details'] = $category_details;

        $data['content'] = 'admin/categories/editcategory';

        $data['title'] = 'Edit Category';
        $data['page'] = $this->pageID;

        $this->load->view('admin/template', $data); 

    }else{

      $this->session->set_flashdata('flashmsg', 'Invalid Category');

      $this->session->set_flashdata('msg', 'danger');

      redirect(base_url().'categories/getCatgories/');      

    }    

  }



  // Update category by category id

  public function updateCategory($category_id=""){

    $this->load->library('ciqrcode');

    $category_details = $this->Categories_model->get($category_id);

    $categoryinfo['category_name'] = strip_tags(ucwords($this->input->post('category_name')));

    $categoryinfo['category_name_thai'] = $this->input->post('category_name_thai');
    $categoryinfo['page_id'] = $this->input->post('facebook_page');//is_null($this->pageID) ? $this->input->post('facebook_page') : $this->pageID;

    //$updated = $this->Categories_model->update($categoryinfo,$category_id);

    // 05-07-2018 Shortlinks
    if(is_null($category_details->global_link)){
       // Short link for website details page for products - 04-07-2018
        $seller = $this->session->userdata('adminId'); // Remove encryption
        $category = $category_id; // Remove encryption

        $shorturl = base_url()."item/products/{$category}/{$seller}";

        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_URL,$postdata);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $content = json_decode($response);
        curl_close($ch);
        
        if(!$content || empty($content)) {
          $path = __DIR__.'\Categories.php function updateCategory';
          $line = 224;
          apilogger("Error while generating short-link",$path,$line);
        }

        if($content->result == true){
          $categoryinfo['global_link'] = $content->url;     
        }
        else{
          $categoryinfo['global_link'] = "";
        } 
    }

    if(is_null($category_details->qrcode)){
        $qr_image=rand().'.png';
        $params['data'] = is_null($category_details->global_link) ? $categoryinfo['global_link'] : $category_details->global_link;//$this->input->post('finallink');
        $params['level'] = 'H';
        $params['size'] = 8;
        $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
        if($this->ciqrcode->generate($params))
        {
            $categoryinfo['qrcode']=$qr_image; 
        }
    }

    if(!empty($_FILES['category_image']['name'])){
      $config['upload_path'] = './uploads/categories';
      $config['allowed_types'] = 'gif|jpg|png|jpeg';
      $config['max_size'] = 5120;
      $this->upload->initialize($config);
      if ($this->upload->do_upload('category_image')) {
          $fileData = $this->upload->data();
          $categoryinfo['category_image'] = $fileData['file_name'];
          $this->Categories_model->update($categoryinfo,$category_id);
          @unlink("./uploads/categories/".$this->input->post('cat_image'));
          $this->session->set_flashdata('flashmsg','Category has been updated.');
          $this->session->set_flashdata('msg', 'success');
      } else {
          $error = $this->upload->display_errors();
          $this->session->set_flashdata('flashmsg', $error);
          $this->session->set_flashdata('msg', 'danger');
          redirect(base_url().'categories/editCategory/'.$category_id);
      }
    }
    else{
      $updated = $this->Categories_model->update($categoryinfo,$category_id);
      if($updated){
          $this->session->set_flashdata('flashmsg', 'Category has been updated.');

          $this->session->set_flashdata('msg', 'success');
      }
    }
    redirect(base_url().'categories/getCatgories/');
  }

  // Remove Category by category id

  public function removeCategory($category_id=""){

    $product_id = $this->Product_categories_model->where(array('category_id'=>$category_id))->fields('product_id')->get_all() ; // Check record exists

    $catlists = $this->Categories_model->where(array('category_id' => $category_id))->fields('category_image')->get();

    if($product_id){ // True

      foreach ($product_id as $row) {

        $product_id = explode( ' ' , $row->product_id );

        foreach ($product_id as $key) {

          $img_details = $this->Product_images_model->where(array('product_id'=>$key))->get_all() ;

          foreach ($img_details as  $rows) {

            unlink("./uploads/products/".$rows->product_image);

          }

        } 

        $this->Products_model->delete(array('product_id'=>$product_id));

        $this->Product_categories_model->delete(array('category_id'=>$category_id));

        $this->Product_images_model->delete(array('product_id'=>$product_id)); 

      }

    }

    $this->Categories_model->delete(array('category_id' => $category_id));  
    if(!is_null($catlists->category_image)){
      unlink("./uploads/categories/".$catlists->category_image);
    }

    //$this->Categories_model->delete($where = $category_id);  

    $this->session->set_flashdata('flashmsg', 'Category has been removed.');

    $this->session->set_flashdata('msg', 'success');

    redirect(base_url().'categories/getCatgories/');  

  } 



}
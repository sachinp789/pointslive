<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends MY_Controller {
	public $pageID="";
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Coupons_model');
		$this->load->model('Stores_model');
		$this->pageID = $this->session->userdata('pagemanager');
	}

	/************* View coupon form *************/
	public function addCoupon(){
		$data['content'] = 'admin/coupons/addcoupon';
	    $data['title'] = 'Add Coupon';
	    $data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);
	}

	/*************** Save Coupon ****************/
	public function saveCoupon(){
		// For vendor
	    /*if(!empty($this->session->userdata('isLogin'))){
	        $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();

	        if(!$pages){
	          $this->session->set_flashdata('flashmsg', 'Store setup is required');
	          $this->session->set_flashdata('msg', 'danger');
	          redirect(base_url().'coupon/addCoupon');
	        }
	    }*/
		if($this->input->post()){
			$this->form_validation->set_rules('title','Title','trim|xss_clean|required');
			$this->form_validation->set_rules('amount','Amount','trim|xss_clean|required|numeric');
			$this->form_validation->set_rules('uses_per_customer','Uses per customer','trim|xss_clean|numeric');
			$this->form_validation->set_rules('minimum_amount','Minimum order amount','trim|xss_clean|numeric|required');

			if($this->form_validation->run() == true) {
				$data = [
					'code' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'status' => $this->input->post('status'),
					'from_date' => $this->input->post('fromdate'),
					'to_date' => $this->input->post('todate'),
					'by_apply' => $this->input->post('apply_on'),
					'per_customer_uses' => $this->input->post('uses_per_customer'),
					'discount_value' => $this->input->post('amount'),
					'minimum_amount' => $this->input->post('minimum_amount'),
					//'page_id' => is_null($this->pageID) ? $this->input->post('page_id') : $this->pageID,
					'created_by' => $this->session->userdata('adminId')
				];

				if(!empty($this->session->userdata('isLogin'))){
					$data['page_id'] = $this->input->post('facebook_page');
				}
				else{
					$data['page_id'] = $this->input->post('facebook_page');//is_null($this->pageID) ? $this->input->post('page_id') : $this->pageID;
				}

				$success = $this->Coupons_model->insert($data);

				if($success){
					$this->session->set_flashdata('flashmsg','Coupon has been added.');
              		$this->session->set_flashdata('msg', 'success');
				}
				redirect(base_url().'coupon/addCoupon');
			}
		$data['content'] = 'admin/coupons/addcoupon';
        $data['title'] = 'Add Coupon';
        $this->load->view('admin/template', $data);
		}	
	}

	/************ List of coupons ***************/
	public function listCoupons(){

		if(!empty($this->session->userdata('isLogin'))) {
			$coupons = $this->Coupons_model->where('created_by',$this->session->userdata('adminId'))->fields('*')->get_all();
		}
		else{

			if(is_null($this->pageID)){
				$coupons = $this->Coupons_model->fields('*')->get_all();
			}
			else{
				$coupons = $this->Coupons_model->where('page_id',$this->pageID)->fields('*')->get_all();
			}
		}
		//$coupons = $this->Coupons_model->fields('*')->get_all();
	    $data['coupons'] = $coupons;
	    $data['content'] = 'admin/coupons/allcoupons';
	    $data['title'] = 'All Coupons';
	    $this->load->view('admin/template', $data);
	}

	/************ Edit coupon view file  ***************/
	public function editCoupon($coupon_id=""){
		$coupons_details = $this->Coupons_model->get($coupon_id);
	    if(!empty($coupons_details)){
	        $data['coupons_details'] = $coupons_details;
	        $data['content'] = 'admin/coupons/editcoupon';
	        $data['title'] = 'Edit Coupon';
	        $data['page'] = $this->pageID;
	        $this->load->view('admin/template', $data); 
	    }else{
	      $this->session->set_flashdata('flashmsg', 'Invalid Coupon');
	      $this->session->set_flashdata('msg', 'danger');
	      redirect(base_url().'coupon/listCoupons/');      
	    }   
	}

	/*********** Update coupon by ID *****************/
	public function updateCoupon(){ 
		if($this->input->post()){
			$this->form_validation->set_rules('title','Title','trim|xss_clean|required');
			$this->form_validation->set_rules('amount','Amount','trim|xss_clean|required|numeric');
			$this->form_validation->set_rules('uses_per_customer','Uses per customer','trim|xss_clean|numeric');
			$this->form_validation->set_rules('minimum_amount','Minimum order amount','trim|xss_clean|numeric|required');
			$coupon_id = $this->input->post('coupon_id');
			if($this->form_validation->run() == true) {
				$data = [
					'code' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'status' => $this->input->post('status'),
					'from_date' => $this->input->post('fromdate'),
					'to_date' => $this->input->post('todate'),
					'by_apply' => $this->input->post('apply_on'),
					'per_customer_uses' => $this->input->post('uses_per_customer'),
					'discount_value' => $this->input->post('amount'),
					'minimum_amount' => $this->input->post('minimum_amount'),
					//'page_id' => is_null($this->pageID) ? $this->input->post('page_id') : $this->pageID
				];

				if(!empty($this->session->userdata('isLogin'))){
					$data['page_id'] = $this->input->post('facebook_page');
				}
				else{
					$data['page_id'] = $this->input->post('facebook_page');//is_null($this->pageID) ? $this->input->post('page_id') : $this->pageID;
				}

				$success = $this->Coupons_model->update($data,$coupon_id);

				if($success){
					$this->session->set_flashdata('flashmsg','Coupon has been updated.');
              		$this->session->set_flashdata('msg', 'success');
				}
			}
			redirect(base_url().'coupon/listCoupons');
		}	
	}

	/*************** Remove coupon ******************/
	public function removeCoupon($coupon_id=""){
		$deleted = $this->Coupons_model->delete(array('id' => $coupon_id));  
		if($deleted){
		    $this->session->set_flashdata('flashmsg', 'Coupon has been removed.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg', 'Error while removing coupon.');
		    $this->session->set_flashdata('msg', 'danger');
		}
	    redirect(base_url().'coupon/listCoupons/');
	}

	/**************** Get Reedem Coupons List ***************/
	public function redeemCoupons(){
		$redeemlists = $this->Coupons_model->getRedemtionsList($this->pageID);
		$data['redeemlists'] = $redeemlists;
	    $data['content'] = 'admin/coupons/redemptions';
	    $data['title'] = 'Redemption Coupons';
	    $this->load->view('admin/template', $data);
	}


	/************* View hashtag form *************/
	public function addHashtag(){
		$data['content'] = 'admin/hashtags/addhashtag';
	    $data['title'] = 'Add Metadata';
	    $data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);
	}

	/*************** Save Hashtag ****************/
	public function saveHashtag(){
		// For vendor
        /*if(!empty($this->session->userdata('isLogin'))){
            $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();

            if(!$pages){
              $this->session->set_flashdata('flashmsg', 'Store setup is required');
              $this->session->set_flashdata('msg', 'danger');
              redirect(base_url('categories/newCategory'));
            }
        }*/
		if($this->input->post()){
			$this->form_validation->set_rules('hashtag','Hashtag Name','trim|xss_clean|required');

			if($this->form_validation->run() == true) {
				$data = [
					'tagname' => $this->input->post('hashtag'),
					'type' 	  => $this->input->post('metatype'),
					'created_by' => $this->session->userdata('adminId')
					//'page_id' => is_null($this->pageID) ? '1048681488539732' : $this->pageID
				];

				if(!empty($this->session->userdata('isLogin'))){
					$data['page_id'] = $this->input->post('facebook_page');
				}
				else{
					$data['page_id'] = $this->input->post('facebook_page');//is_null($this->pageID) ? '1048681488539732' : $this->pageID;
				}

				$success = $this->db->insert('hashtags',$data);

				if($success){
					$this->session->set_flashdata('flashmsg','Metadata has been added.');
              		$this->session->set_flashdata('msg', 'success');
				}
				redirect(base_url().'coupon/addHashtag');
			}
		$data['content'] = 'admin/hashtags/addHashtag';
        $data['title'] = 'Add Metadata';
        $this->load->view('admin/template', $data);
		}	
	}

	/************ List of hashtags ***************/
	public function listTags(){
		/*if(is_null($this->pageID)){
			$coupons = $this->Coupons_model->fields('*')->get_all();
		}
		else{
		}*/
		if(!empty($this->session->userdata('isLogin'))) {
			$hashtags = $this->db->select('*')->from('hashtags')->where('created_by',$this->session->userdata('adminId'))->get()->result();
		}
		else{
			if(!empty($this->session->userdata('pagemanager'))){
				$page_id = $this->session->userdata('pagemanager');//is_null($this->pageID) ? '1048681488539732' : $this->pageID;
				$hashtags = $this->db->select('*')->from('hashtags')->where('page_id',$page_id)->get()->result();	
			}
			else{
				$hashtags = $this->db->select('*')->from('hashtags')->get()->result();
			}
		}
	    $data['tags'] = $hashtags;
	    $data['content'] = 'admin/hashtags/listtags';
	    $data['title'] = 'Metadata Manager';
	    //$data['page'] = $this->pageID;
	    $this->load->view('admin/template', $data);
	}

	/************ Edit hashtag view file  ***************/
	public function editHashtag($tag_id=""){
		$tags_details = $this->db->select('*')->from('hashtags')->where('id',$tag_id)->get()->row();
	    if(!empty($tags_details)){
	        $data['tags_details'] = $tags_details;
	        $data['content'] = 'admin/hashtags/edithashtag';
	        $data['title'] = 'Edit Metadata';
	        $data['page'] = $this->pageID;
	        $this->load->view('admin/template', $data); 
	    }else{
	      $this->session->set_flashdata('flashmsg', 'Invalid Tag');
	      $this->session->set_flashdata('msg', 'danger');
	      redirect(base_url().'coupon/listTags');      
	    }   
	}

	/*********** Update hashtags by ID *****************/
	public function updateTag(){ 
		if($this->input->post()){
			$this->form_validation->set_rules('hashtag','Hashtag Name','trim|xss_clean|required');
			$tag_id = $this->input->post('tag_id');
			if($this->form_validation->run() == true) {
				$data = [
					'tagname' => $this->input->post('hashtag'),
					'type' 	  => $this->input->post('metatype')
				];

				if(!empty($this->session->userdata('isLogin'))){
					$data['page_id'] = $this->input->post('facebook_page');
				}
				else{
					$data['page_id'] = $this->input->post('facebook_page');//is_null($this->pageID) ? '1048681488539732' : $this->pageID;
				}

				$this->db->where('id', $tag_id);  
				$success = $this->db->update('hashtags', $data);  
				if($success){
					$this->session->set_flashdata('flashmsg','Metadata has been updated.');
              		$this->session->set_flashdata('msg', 'success');
				}
			}
			redirect(base_url().'coupon/listTags');
		}	
	}

	/*************** Remove hashtag ******************/
	public function removeHashtag($tag_id=""){
		$this->db->where('id', $tag_id);
		$deleted = $this->db->delete('hashtags'); 
		if($deleted){
		    $this->session->set_flashdata('flashmsg', 'Metadata has been removed.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg', 'Error while removing Metadata.');
		    $this->session->set_flashdata('msg', 'danger');
		}
	    redirect(base_url().'coupon/listTags');
	}

}
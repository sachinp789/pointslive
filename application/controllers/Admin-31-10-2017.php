<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public $access_token = "EAAG2GcTZCD0MBAILHy8vMNBiRvnL6vd8sGn01Cp9pQ0XwI4TWl983eYwNiRbNS8Xxq1A9Xlle1st1p7laj7PauC9tZAk3ymIR5SmjT37HrgBZCEC4bWLRNoEYZAe6xLoQTC7Hu3MJcrNbevg09WYxJ8gXpNTKOZBzyQyOKhmFBwZDZD";
	/**
  
  Points live :- EAABtFZCRlZCI8BABhj4SQHUL1ihjQqd7gqD8By1aHLFIsJwQZCqJqNn77hTH9s34UXs8cdLyR6dnZB3Ga1TW0mjOiwrQwteYirofEo4Lhq9pzZB4BmfCz64Hz4tr4gmOFZAs65QnD0OBhmEZAhLwo48vjdmezKRybfkoxTcJGnWAgZDZD

  client_id=119949651999887

  client_secret=39e9e077f2223c841ca5f4863c359fe1
  
  **/
  public $verify_token = "moonways";
	public $hub_verify_token = null;
	public $input_chat = "";
  public $perpagerecord = 0;

	public function __construct(){
		parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Chat_prefrences_model');
    $this->load->model('Products_model');
    $this->load->model('Admin_model');
 
		$this->input_chat = json_decode(file_get_contents('php://input'), true);

    if(isset($_REQUEST['hub_challenge'])) {
        $challenge = $_REQUEST['hub_challenge'];
        $hub_verify_token = $_REQUEST['hub_verify_token'];
        
        if ($hub_verify_token === $this->verify_token) {
              echo $challenge;
        }
    }
    //$this->event_status();
   /* if($this->session->userdata('adminId') != 1) {
        redirect(base_url() . 'login');
    }*/

	}
/*
  public function getOtherUserToken(){
    

    $token = $this->getCode();

    $this->access_token = $this->getPageToken($token);

    $url = "https://graph.facebook.com/1520026948056473?access_token={$this->access_token}";
    $response = $this->curl($url,'','GET');

    echo '<pre>';
    print_r($response);
    exit;

  }*/

  // Get refresh page token
  public function getPageToken($token){
    $url = "https://graph.facebook.com/v2.10/473467159675523?access_token={$token}&fields=access_token";
    $response = $this->curl($url,'','GET');
    return $response->access_token;
  }

  public function test($param=""){
      $ch = curl_init("https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAADZBuJzbhGoBAOLQhj4raRZCRYpZAi9mCyUzDF6cm3p4aY7mbSCtEBQKijZBQ4ts0HPYePZCnFPu6N7mu5uf74yIZCFgvSt6ZBobDDiLZBYvmMm5GRe18gqLgzJIEt9zJSpQHtILozIpTXquj8qlTxXXY4WZBbyAKOl1FRZBnIG3vzQZDZD");

        //The JSON data.
        $jsonData = '{
            "whitelisted_domains":{
               "https://dev.searchnative.com"
            }
        }';
        //Encode the array into JSON.
        $jsonDataEncoded = $jsonData;
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        var_dump($result);
  }

  // Admin dashboard
	public function dashboard()
	{ 
		$data['content'] = 'dashboard';
		$data['title'] = 'Points live Admin Dashboard';
		$this->load->view('admin/template', $data);
	}

  // Display chatoption page
	public function chatoptions(){
		$data['content'] = 'admin/chatbot';
		$data['title'] = 'Chatbot Option';
		$this->load->view('admin/template', $data);
	}

  // Show admin profile page
  public function profile($profile_id){
      $data = array();

      $profiledata = $this->Admin_model->get(['admin_id' => $profile_id]);
      $data['configurations'] = emailconfig();

      $profileinfo = array();
      if(!empty($_FILES['profile_photo']['name'])) {

           $uploaded = do_upload($profile_id,$_FILES['profile_photo']);
           if(!empty($uploaded['data']['file_name'])){
              unlink("uploads/".$profiledata->admin_photo);
              $profileinfo['admin_photo'] = $uploaded['data']['file_name'];
           }
           else{
            $this->session->set_flashdata('flashmsg', $uploaded['error']);
            $this->session->set_flashdata('msg', 'danger');
            redirect(base_url().'admin/profile/'.$profile_id);
           }
      }

      if($this->input->post()){

        $this->form_validation->set_rules('admin_name','Name','trim|xss_clean');
        $this->form_validation->set_rules('admin_email','Email','trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('mobile_no','Mobile Number','trim|numeric|xss_clean');

        if($this->form_validation->run() == true) {

            $profileinfo['admin_name'] = strip_tags($this->input->post('admin_name'));
            $profileinfo['admin_email'] = strip_tags($this->input->post('admin_email'));
            $profileinfo['admin_mobile'] = strip_tags($this->input->post('mobile_no'));
            
            $updated = $this->Admin_model->update($profileinfo,$profile_id);
            if($updated){
                $this->session->set_userdata('username', $this->input->post('admin_name'));
                $this->session->set_flashdata('flashmsg', 'Profile has been updated.');
                $this->session->set_flashdata('msg', 'success');
                redirect(base_url().'admin/profile/'.$profile_id);
            }
        }
      }

      if($profiledata){
        $data['profileinfo'] = $profiledata;
      }
      else{
         redirect(base_url().'admin/dashboard');
      }

      $data['content'] = 'admin/profile';
      $data['title'] = 'Profile';
      $this->load->view('admin/template', $data);
  }

  // Change admin password
  public function changePassword(){
      $data = array();

      if($this->input->post()){

          $this->form_validation->set_rules('change_pwd','New Password','trim|required|xss_clean');
          $this->form_validation->set_rules('confirm_password','Confim Password','trim|required|matches[change_pwd]|xss_clean');

          if($this->form_validation->run() == true) {

            $updated = array(
              'admin_password' => $this->_hash($this->input->post('change_pwd',true))  
            );

            $results = $this->Admin_model->update($updated,$this->session->userdata('adminId'));
            if($results){
                $this->session->set_flashdata('flashmsg', 'Password has been changed successfully.');
                $this->session->set_flashdata('msg', 'success');
                redirect(base_url().'admin/changePassword');
            }
            else{
                $this->session->set_flashdata('flashmsg', 'Password not updated.');
                $this->session->set_flashdata('msg', 'error');
                redirect(base_url().'admin/changePassword');
            }
          }
      }

      $data['content'] = 'admin/changepassword';
      $data['title'] = 'Change Password';
      $this->load->view('admin/template', $data);
  }

  // Admin session logout
  public function signout(){
      if($this->session->userdata('adminId')){
          $this->session->unset_userdata('adminId');
      }
      redirect(base_url().'login');
  }

  // Mail configuration for smtp
  public function mailConfiguration(){
      $success = true;
      $data = $this->input->post();

      foreach($data as $key => $value){

        if(!$this->save($key,$value)){
          $success=false;
          break;  
          }
      }
      if($success){
        $this->session->set_flashdata('flashmsg', 'SMTP settiing has been saved.');
        $this->session->set_flashdata('msg', 'success');
      }
      redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
  }

  // Save smptp settings
  public function save($key,$value){
    $config_data=array(
      'key'=>$key,
      'value'=>trim($value)
    );
    $this->db->where('key', $key);
    return $this->db->update('config_data',$config_data); 
  }

  // Facebook get new code for token generate
  public function getCode(){
      $code = '';

      $url = "https://graph.facebook.com/oauth/client_code?access_token=".$this->access_token."&client_secret=ca8ffc244745d8ae83862b9b9f005a71&redirect_uri=https://dev.searchnative.com/pointslive/admin/startchat&client_id=481696772198211"; 

      $response = $this->curl($url,'','GET');

      if(isset($response->code)){
        $code = $response->code;
        return $this->getRefreshAccessToken($code);
      }
      else{
        return $this->getRefreshAccessToken($code);
      } 
  }

  // Get new refresh access token
  public function getRefreshAccessToken($code){
      //var_dump($code);exit;
      $ch = curl_init("https://graph.facebook.com/oauth/access_token?code=".$code."&client_secret=ca8ffc244745d8ae83862b9b9f005a71&redirect_uri=https://dev.searchnative.com/pointslive/admin/startchat&client_id=481696772198211");
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      $results = curl_exec($ch);
      //var_dump($results);exit;
      $results = json_decode($results);
      if($results){
        return $results->access_token;
      }
      else{
        return $this->getCode();
      } 
      curl_close($ch);
  }

  // Call facebook api uysing CURL
	public function curl($url,$data = array(),$type){

		switch ($type) {

			case 'GET':
          $ch = curl_init();
	    		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	    		curl_setopt($ch,CURLOPT_URL,$url);
	    		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    		//curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,2);

	    		if(curl_exec($ch) === false)
  				{
  				    return curl_error($ch);
  				}
  				else{
  			    	$content = curl_exec($ch);
  			   		$content = json_decode($content);
  				}
          curl_close($ch);
				break;

			case 'POST':
        //print_r(json_encode($data));exit;
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

			  $content = curl_exec($ch);
        $content = json_decode($content);
        /*if(!empty($this->input_chat)){
        }*/

        curl_close($ch);
				break;

      case 'DEL':
         $ch = curl_init($url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
         curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

         /* if(!empty($this->input_chat)){*/
            $content = curl_exec($ch);
            $content = json_decode($content);
          /*}*/
         curl_close($ch); 
			default:
				break;
		}

		if(isset($content->error)) {
			return $content->error->message;
		}else{
      return $content;
    }
		
	}

	/******************************** Get page events ***********************************/
	public function events($limit='',$next=''){

    $token = $this->getCode();

    $this->access_token = $this->getPageToken($token);

    $pagelimit = $limit ? $limit : 5;
    $nextevent = $next ? $next : '';

		$url = "https://graph.facebook.com/473467159675523/events?fields=photos{picture},name,place,start_time,end_time,description&access_token=".$this->access_token."&limit={$pagelimit}&after={$nextevent}";

    $response = $this->curl($url,'','GET');
		return $response;
	}
	/***************************************** END ***********************************/

  public function event_status(){

   /* https://www.facebook.com/dialog/oauth?client_id=119949651999887&redirect_uri=https://b73ca01d.ngrok.io/pointslive/admin/startchat&response_type=code&scope=manage_pages,publish_pages*/

    $token = $this->getCode();

    $url = "https://graph.facebook.com/v2.10/1795633330648515/maybe?access_token=".$token;

    $response = [
        'rsvp_status' => 'true'
    ];

    $content = $this->curl($url,$response,'POST');

    echo '<pre>';
    print_r($content);
    exit;

    $results = json_encode($content);
  }

	public function startchat(){

    $total_rows_count = $this->Products_model->count_rows();

    $chatoptionsdata = chatoptions(); // Chat options 

		$input = json_decode(file_get_contents('php://input'), true);

    file_put_contents('fb_response.txt', file_get_contents("php://input") . PHP_EOL, FILE_APPEND);

		// Page ID of Facebook page which is sending message
		$page_id = $input['entry'][0]['id'];

		$sender = $input['entry'][0]['messaging'][0]['sender']['id'];

		// Get Message text if available
		$message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;

		$messagetext = strtolower($message);

    $quick_replies_opt = isset($input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']) ? $input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']: '' ;

    $quick_replies_opt = strtolower($quick_replies_opt);

		// Get Postback payload if available
		$postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;

		$posttitle = isset($input['entry'][0]['messaging'][0]['postback']['title']) ? $input['entry'][0]['messaging'][0]['postback']['title']: '' ;

		$posttitletext = strtolower($posttitle);

		$message_to_reply = '';
    $datas = array();

		if($message || $postback){ 

       $token = $this->getCode(); // Get New User Token
       //$this->access_token = $this->getPageToken($token); // Get Page new token

       /*
        Delete menu

        $response = [
          'recipient' => [ 'id' => $sender ],
          'setting_type'  => 'call_to_actions',
          'thread_state' => 'existing_thread'
        ];  

        $this->curl("https://graph.facebook.com/v2.6/me/thread_settings?access_token=".$this->access_token,$response,'DEL');*/


     /* 
      Add menu
     $answer =[
           [
           "type"=>"postback",
           "title"=>"Help",
           "payload"=>"DEVELOPER_DEFINED_PAYLOAD_FOR_HELP"
           ],
           [
           "type"=>"postback",
           "title"=>"Start a New Order",
           "payload"=>"DEVELOPER_DEFINED_PAYLOAD_FOR_START_ORDER"
           ],
           [
           "type"=>"web_url",
           "title"=>"View Website",
           "url"=>"http://petersapparel.parseapp.com/"
           ]
      ];

       $response = [
          'recipient' => [ 'id' => $sender ],
          'setting_type'  => 'call_to_actions',
          'thread_state' => 'existing_thread',
          'call_to_actions' =>  $answer
        ]; 


      $this->curl("https://graph.facebook.com/v2.6/me/thread_settings?access_token=".$this->access_token,$response,'POST');*/

        if($message){

            switch ($quick_replies_opt) {
              case 'category':
                $page = 0;
                $catproducts = productlists_by_category($messagetext,$page,10);
                $products = array();

                if(count($catproducts) > 0) :
                  foreach ($catproducts as $key => $product) {
                    // http://192.168.1.23/pointslive/uploads/products/{$product->product_images[0]}
                      $products[] = array(
                      "title"     => $product->product_name, 
                      "image_url" => base_url()."uploads/products/{$product->product_images[0]}",
                      "subtitle"  => @$product->product_description,
                      "buttons"   => array(
                              array(
                              "type"=>"web_url",
                              "url"=>"https://www.sandbox.paypal.com/cgi-bin/webscr#/checkout/login",
                              "title"=>"₹ $product->product_price Buy" 
                              )
                          )  
                      );
                  }

                 $answer = ["attachment"=>[
                  "type"=>"template",
                  "payload"=>[
                      "template_type"=>"generic",
                      "elements"=> $products
                      ]
                    ]
                  ];

                else :

                  $message_to_reply = "No products found for category {$message}";
                  $answer = [ 'text' => $message_to_reply];
                
                endif;

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => $answer 
                ];

                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                $options = array(
                    0 => array(
                      'content_type' => 'text',
                      'title' => "Yes",
                      'payload' => "flag"
                    ),
                    1 => array(
                      'content_type' => 'text',
                      'title' => "No",
                      'payload' => "flag"
                    )
                  ); 

                 $quickoptions = json_encode($options);

                 $message_to_reply = $chatoptionsdata[0]->findin_page ? $chatoptionsdata[0]->findin_page : 'Still want to findout moonways ?';

                 $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                 ];
                 $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                break;

              case 'pay_load_1':

               if($messagetext == "categories"){
                    $optionslist = array();
                    $categories = categroy_lists();

                    if(count($categories) > 0){
                      foreach ($categories as $value) {
                          $optionslist[] = array(
                            'content_type' => 'text',
                            'title' => ucwords($value->category_name),
                            'payload' => "category" 
                          );
                      }
                      $quickoptions = json_encode($optionslist);

                      $message_to_reply = 'Here is what you are looking for !';

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                    }
                    else{
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => 'No categories found.' 
                      ];
                   }
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                   exit;
                }
                if($messagetext == "other"){

                    //$this->session->set_userdata('keysearch1','sp');
                    $this->cache->save('keysearch', $messagetext);

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => 'Please type to search products.']
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }
                if($messagetext == "events"){
                  $eventsData = $this->events();
                  $events = array();

                    if(count($eventsData->data) > 0) :

                      foreach ($eventsData->data as $key => $value) {

                          $events[] = array(
                          "title"     => $value->name, 
                          "image_url" => $value->photos->data[0]->picture,
                          "subtitle"  => $value->description,
                          "buttons"   => array(
                                  array(
                                  "type"=>"web_url",
                                  "url"=>"https://www.facebook.com/events/".$value->id,
                                  "title"=>"View Event" 
                                  )/*,
                                  array(
                                  "type"=>"postback",
                                  "title"=>"Pick options",
                                  "payload"=>$value->id
                                  ) */     
                              )  
                          );
                      }

                      $answer = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$events
                                ]
                            ]];

                    else :

                      $message_to_reply = 'No events found.';
                      $answer = ['text' => $answer ];

                    endif;  

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer
                    ];


                    if(isset($eventsData->paging->next)) :

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    
                      $this->cache->save('nextevent', $eventsData->paging->cursors->after);

                      $nextevents[] = array(
                          "title"     => 'Click below to see more events.', 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>"More Events",
                                  "payload"=>'Events_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextevents
                                ]
                            ]];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    exit;

                  else :

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  exit; 
                  endif;
                }
                if($messagetext == "products/services"){
                    $page = 0;
                    $productslists = product_lists($page,10);
                    $products = array();

                    if(count($productslists) > 0) :
                        foreach ($productslists as $key => $product) {
                            //http://192.168.1.23/pointslive/uploads/products/{$product->images[0]->product_image}
                            $products[] = array(
                            "title"     => $product->product_name, 
                            "image_url" => base_url()."uploads/products/{$product->images[0]->product_image}",
                            "subtitle"  => $product->product_description,
                            "buttons"   => array(
                                    array(
                                    "type"=>"web_url",
                                    "url"=>"https://www.sandbox.paypal.com/cgi-bin/webscr#/checkout/login",
                                    "title"=>"₹ $product->product_price Buy" 
                                    )
                                )  
                            );
                        }

                      $this->perpagerecord = count($productslists); // Current page count

                       $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                            "template_type"=>"generic",
                            "elements"=> $products
                            ]
                          ]
                        ];

                    else :
                        
                      $message_to_reply = 'No products found.';

                      $answer = [ 'text' => $message_to_reply];

                    endif;

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];

                    if($total_rows_count > 10) :

                      $page = $page + 1;
                      
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                    
                      $this->cache->save('nextproduct', $page);
                      $this->cache->save('currentpagecount', $this->perpagerecord);

                      $nextproducts[] = array(
                          "title"     => 'Click below to see more products.', 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>"More Products",
                                  "payload"=>'Products_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextproducts
                                ]
                            ]];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    exit;  

                    else :

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;  
                    endif;
                }
              break;

              default:
                
                if($messagetext == 'hi' || $messagetext == 'akin') {
                   $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$chatoptionsdata[0]->welcome_msg ? $chatoptionsdata[0]->welcome_msg : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => "What's Hot Loop",
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => "Discover Products Loop"
                          ],
                          [
                            "type"=>"postback",
                            "title"=>"View More",
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                exit;
            }
            else if($messagetext == 'yes'){
                 $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => "Categories",
                            'payload' => "PAY_LOAD_1"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => "Other",
                            'payload' => "PAY_LOAD_1"
                        )
                    ); 

                  $quickoptions = json_encode($options);
                  $message_to_reply = 'Below are options to find products.';

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  exit;
            }
            else if($messagetext == 'no'){

                  $message_to_reply = $chatoptionsdata[0]->dismiss_msg ? $chatoptionsdata[0]->dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                   ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');   
                exit;
            }
            else if(in_array($messagetext, array('going','interested','not interested'))){

                  if($messagetext == 'going'){

                    $url = "https://graph.facebook.com/v2.10/{$quick_replies_opt}/attending?rsvp_status&access_token=".$token;

                    $content = $this->curl($url,'','POST');
                    $results = json_encode($content);
                  
                    if($results){
                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => 'Thanks']
                        ];
                      }
                  }
                  else if($messagetext == 'interested'){

                    $url = "https://graph.facebook.com/v2.10/{$quick_replies_opt}/maybe?rsvp_status&access_token=".$token;

                    $content = $this->curl($url,'','POST');
                    $results = json_encode($content);
                  
                    if($results){
                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => 'Thanks']
                        ];
                      }
                  }
                  else if($messagetext == 'not interested'){

                    $url = "https://graph.facebook.com/v2.10/{$quick_replies_opt}/declined?rsvp_status&access_token=".$token;

                    $content = $this->curl($url,'','POST');

                    $results = json_encode($content);
          
                    if($results){
                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => 'Thanks']
                        ];
                      }
                  }
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  exit;
              }
              else
              {   
                  //echo $this->session->userdata('keysearch1');
                  if(!empty($this->cache->get('keysearch'))){
                    $datasearch = array(
                        'search_by_userid' => $sender,
                        'search_keyword'  => $messagetext 
                      );

                    if($this->saveSearchKeyword($datasearch)){
                        // Find products / category products
                        $searchproducts = searchproduct_by_keyword($messagetext);
                        $products = array();

                        if(count($searchproducts) > 0) :
                          foreach ($searchproducts as $key => $product) {
                            // http://192.168.1.23/pointslive/uploads/products/{$product->product_images[0]}
                              $products[] = array(
                              "title"     => $product->product_name, 
                              "image_url" => base_url()."uploads/products/{$product->product_images[0]}",
                              "subtitle"  => $product->product_description,
                              "buttons"   => array(
                                      array(
                                      "type"=>"web_url",
                                      "url"=>"https://www.sandbox.paypal.com/cgi-bin/webscr#/checkout/login",
                                      "title"=>"₹ $product->product_price Buy" 
                                      )
                                  )  
                              );
                          }

                          $answer = ["attachment"=>[
                              "type"=>"template",
                              "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=> $products
                                  ]
                                ]
                              ];
                        else :

                          $message_to_reply = 'No products found.';
                          $answer = ['text' => $message_to_reply];

                        endif;

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $chatoptionsdata[0]->feedback_msg ? $chatoptionsdata[0]->feedback_msg : 'Thanks for feedback. We will let Store Manager know']
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                        $options = array(
                                    0 => array(
                                        'content_type' => 'text',
                                        'title' => "Yes",
                                        'payload' => "flag"
                                    ),
                                    1 => array(
                                        'content_type' => 'text',
                                        'title' => "No",
                                        'payload' => "flag"
                                    )
                                ); 

                         $quickoptions = json_encode($options);

                         $message_to_reply = $chatoptionsdata[0]->findin_page ? $chatoptionsdata[0]->findin_page : 'Still want to findout moonways ?';

                         $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                         ];
                        // Remove cache
                        $this->cache->delete('keysearch');
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                        exit;
                      }
                  }
                  else{
                    $message_to_reply = "I don't understand1, what do you mean ? say Hi !";
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                  }
              }
             break;
          }
          //$this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
        }
        else 
        {
            switch($postback) {

            case 'DEVELOPER_DEFINED_PAYLOAD':

                if($posttitletext == "what's hot loop"){

                    $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => "Products/Services",
                                'payload' => "PAY_LOAD_1"
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => "Events",
                                'payload' => "PAY_LOAD_1"
                            )
                        ); 

                       $quickoptions = json_encode($options);

                       $message_to_reply = $posttitletext;

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];
                       $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                       exit;
                }
                if($posttitletext == "discover products lo..."){

                    $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => "Categories",
                            'payload' => "PAY_LOAD_1"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => "Other",
                            'payload' => "PAY_LOAD_1"
                        )
                    ); 

                    $quickoptions = json_encode($options);
                    $message_to_reply = 'Below are options to find products.';

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }
                if($posttitletext == "faq loop"){

                       $message_to_reply = 'Contact Human Manager on Facebook';

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                       ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }
                if($posttitletext == "about page loop"){ 

                        $content = $this->curl("https://graph.facebook.com/473467159675523?fields=about,name&access_token=".$this->access_token,'','GET'); 
                        //curl_close($ch);  
                        $message_to_reply = $content->about;

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                       ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }
                if($posttitletext == "end chat"){

                       $message_to_reply = $chatoptionsdata[0]->dismiss_msg ? $chatoptionsdata[0]->dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                       ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }    
                break;
                
              case 'PAY_LOAD_1':

                if($posttitletext == 'view more'){

                    $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>"More options",
                            "buttons"=>[
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => "About Page Loop",
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => "FAQ Loop"
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => "End Chat"
                              ]
                            ]
                          ]
                        ]];

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                      ];
                }
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                break;

              case 'Events_Payload':
                
                $next = $this->cache->get('nextevent');

                if(!empty($next)){
                    $eventsData = $this->events(5,$next);

                    $events = array();

                    if(count($eventsData->data) > 0) :

                      foreach ($eventsData->data as $key => $value) {

                          $events[] = array(
                          "title"     => $value->name, 
                          "image_url" => $value->photos->data[0]->picture,
                          "subtitle"  => $value->description,
                          "buttons"   => array(
                                  array(
                                  "type"=>"web_url",
                                  "url"=>"https://www.facebook.com/events/".$value->id,
                                  "title"=>"View Event" 
                                  )/*,
                                  array(
                                  "type"=>"postback",
                                  "title"=>"Pick options",
                                  "payload"=>$value->id
                                  ) */     
                              )  
                          );
                      }

                      $answer = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$events
                                ]
                            ]];

                    else :

                      $message_to_reply = 'No events found.';
                      $answer = ['text' => $answer ];

                    endif;  

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer
                    ];

                    if(isset($eventsData->paging->next)) :

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                    
                      $this->cache->save('nextevent', $eventsData->paging->cursors->after);

                      $nextevents[] = array(
                          "title"     => 'Click below to see more events.', 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>"More Events",
                                  "payload"=>'Events_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextevents
                                ]
                            ]];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  

                    else :

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  

                    endif;
                    break;
                }

              case 'Products_Payload':

                  if($posttitletext == 'more products'){
                        
                    $nextpage = $this->cache->get('nextproduct'); 
                    
                    if(!empty($nextpage)){
                      $productslists = product_lists($nextpage,10);
                      $products = array();

                        if(count($productslists) > 0) :
                            foreach ($productslists as $key => $product) {
                                $products[] = array(
                                "title"     => $product->product_name, 
                                "image_url" => base_url()."uploads/products/{$product->images[0]->product_image}",
                                "subtitle"  => $product->product_description,
                                "buttons"   => array(
                                        array(
                                        "type"=>"web_url",
                                        "url"=>"https://www.sandbox.paypal.com/cgi-bin/webscr#/checkout/login",
                                        "title"=>"₹ $product->product_price Buy" 
                                        )
                                    )  
                                );
                            }

                          $this->perpagerecord = $this->cache->get('currentpagecount') + count($productslists);

                           $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                                "template_type"=>"generic",
                                "elements"=> $products
                                ]
                              ]
                            ];

                        else :
                            
                          $message_to_reply = 'No products found.';

                          $answer = [ 'text' => $message_to_reply];

                        endif;

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                        ];

                        if($this->perpagerecord != $total_rows_count) :

                          $nextpage = $nextpage + 1;
                          
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                        
                          $this->cache->save('nextproduct', $nextpage);
                          $this->cache->save('currentpagecount',$this->perpagerecord);

                          $nextproducts[] = array(
                              "title"     => 'Click below to see more products.', 
                              "buttons"   => array(
                                      array(
                                      "type"=>"postback",
                                      "title"=>"More Products",
                                      "payload"=>'Products_Payload'
                                      )      
                                  )  
                              );

                           $nextdata = ["attachment"=>[
                                    "type"=>"template",
                                    "payload"=>[
                                      "template_type"=>"generic",
                                      "elements"=>$nextproducts
                                    ]
                                ]];

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $nextdata
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  

                        else:

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');    
                        endif;
                    }
                }
               break;
              
              default:

                if($posttitletext == 'pick options'){

                   $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => "Interested",
                                'payload' => $postback
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => "Going",
                                'payload' => $postback
                            ),
                            2 => array(
                                'content_type' => 'text',
                                'title' => "Not Interested",
                                'payload' => $postback
                            )
                        ); 

                 $quickoptions = json_encode($options);

                 $message_to_reply = "How about event ?";

                 $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                exit;
              }
              break;
        	}
          //$this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');     
        } 
		}
	}

  public function savePrefrences(){

      if($this->input->post()){
          
          $chatoptions = array(
            'welcome_msg' =>  $this->input->post('welcome_reply'),
            'faq_msg' =>  $this->input->post('faq_message'),
            'dismiss_msg' =>  $this->input->post('dismiss_message'),
            'feedback_msg' =>  $this->input->post('feedback_message'),
            'product_confirm_msg' =>  $this->input->post('product_confirm'),
            'order_cost_msg' =>  $this->input->post('order_cost'),
            'payment_confirm_msg' =>  $this->input->post('payment_confirm'),    
            'findin_page' =>  ''    
            );

          if(empty($this->input->post('last_id'))){

             if($this->Chat_prefrences_model->insert($chatoptions)){
                $this->session->set_flashdata('flashmsg', 'Chat options saved successfully.');
                $this->session->set_flashdata('msg', 'success');
             }
             else{
                $this->session->set_flashdata('flashmsg', 'Error while saved data.');
                $this->session->set_flashdata('msg', 'danger');
             }
          }
          else{
            if($this->Chat_prefrences_model->update($chatoptions, (int) $this->input->post('last_id'))){
                $this->session->set_flashdata('flashmsg', 'Chat options updated successfully.');
                $this->session->set_flashdata('msg', 'success');
            }
            else{
                $this->session->set_flashdata('flashmsg', 'You have no made any changes.');
                $this->session->set_flashdata('msg', 'info');
            }
          }
      }
      redirect(base_url('admin/chatoptions'));
  }

  public function createProduct(){

      $token = $this->getCode();

      $requests = array(
            array(
                "method" => "CREATESAAS",
                "retailer_id" => "P001",
                "data" => array(
                    "availability" => "in stock",
                    "brand" => "Niky", 
                    "category" => "t-shirts", 
                    "currency" => "USD", 
                    "description" => "This is the product description.", 
                    "image_url" => "http://www.images.example.com/t-shirts/1.png", 
                    "name" => "Test", 
                    "price" => "100", 
                    "url" => "http://www.example.com/t-shirts/1.html" 
                ) 
            )
        );

      $postData = array("requests" => $requests);

      $info = $this->curl("https://graph.facebook.com/v2.10/2016718211948689/batch?access_token=".$token,$postData,'POST');

      echo '<pre>';
      var_dump($info->validation_status);exit;

  }

  // Store search keyword in table
  public function saveSearchKeyword($searchdata=""){
      return $this->Search_prefrences_model->insert($searchdata);
  }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# This is the default and home controller for the website
# created by mayur panchal 21-05-2018

class Subscribers extends CI_Controller {

	
	public $pageID="";
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');	
		$this->pageID = $this->session->userdata('pagemanager');
	}

	/***** For User Lisitng *********/

	public function Users(){
		$users = $this->db->select('admin_id,login_picture,is_active,login_with,admin_name')->from('admin')->where_in('login_with',array('facebook','instagram','twitter','seller'))->get()->result();		
	    $data['users'] = $users;
	    $data['content'] = 'admin/users';
	    $data['title'] = 'Users';	    
	    $this->load->view('admin/template', $data);
	}

	/****** For Subscriber user list *******/
	public function listUsers(){
		$users = $this->db->select('so.*,ad.admin_name')->from('subscription_orders as so')->join('admin as ad', 'ad.admin_id = so.created_by')->get()->result();		
	    $data['subscribers'] = $users;
	    $data['content'] = 'admin/subscribers';
	    $data['title'] = 'Subscriber Users';	    
	    $this->load->view('admin/template', $data);
	}

	public function status(){
		$this->load->model('Admin_model');
		$status = $this->input->post('value');
		$userid = $this->input->post('userid');

		$updated = $this->Admin_model->update(array('is_active' => $status),$userid);

		if($updated){
			echo json_encode(array("msg" => true));
		}
		else{
			echo json_encode(array("msg" => false));
		}
		exit;
	}

	// Page authorise for whitelist domain
	public function authorisepage(){
		$domain = $this->input->post('domain');
		$accesstoken = $this->input->post('token');


		$jsonarray = array(
			"setting_type"=>"domain_whitelisting",
			"whitelisted_domains" => array($domain),
			"domain_action_type" => "add"
		);


		$ch = curl_init("https://graph.facebook.com/v2.6/me/messenger_profile?access_token=".$accesstoken);
	    //Tell cURL that we want to send a POST request.
	    curl_setopt($ch, CURLOPT_POST, 1);
	    //Attach our encoded JSON string to the POST fields.
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonarray));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    //Set the content type to application/json
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    $result = curl_exec($ch);
	    curl_close($ch);	
	   	$request = json_decode($result, true);

	   	if(isset($request['error'])){
	   		$path = __DIR__.'\Subscribers.php function authorisepage';
	        $line = 51;
	        apilogger($request['error']['message'],$path,$line);
	   	}

	   	if(isset($request['result'])){
	   		echo json_encode(array("msg" => true));
	   	}
	   	else if(isset($request['error'])){
	   		echo json_encode(array("msg" => false));
	   	}
	   	exit;
	}

	// Export users
	public function exportUsers(){
		$users = $this->db->select('*')->from('admin')->where_in('login_with',array('facebook','instagram','twitter','seller'))->get()->result();

		require 'application/libraries/php-export-data.class.php';

        $exporter = new ExportDataExcel('browser', 'users.xls');
		$exporter->initialize(); // starts streaming data to web browser
		// pass addRow() an array and it converts it to Excel XML format and sends 
		// it to the browser
		$exporter->generateHeader();
		$exporter->addRow(array("#", "Name","Email","Login By","Register Date","Seller ID","Last Login Date","Status")); 
		$count = 1;

		foreach ($users as $rows) {

			if($rows->is_active == '0'){
				$status = "Blocked";
			}
			elseif($rows->is_active == '1'){
				$status = "Active";
			}

			if($rows->login_with == 'seller'){
				$withlogin = "Email";
			}
			else{
				$withlogin = ucfirst($rows->login_with);
			}

			$no = $count;
			$name = $rows->admin_name;
			$email = $rows->admin_email;
			$bylogin = $withlogin;
			$createdate = $rows->created_date;
			$sellerid = $rows->admin_id;
			$lastlogin = $rows->updated_date;
			$userstatus = $status;
			$data = [$no,$name,$email,$bylogin,$createdate,$sellerid,$lastlogin,$userstatus];
			$exporter->addRow($data);	
			//$data = '';
			$count++;
		}
		$exporter->finalize(); // writes the footer, flushes remaining data to browser.
		exit();
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

  public $access_token = "";
  public $verify_token = "pointslive";
  public $hub_verify_token = null;
  public $verify_token1 = "smartchipway"; // moonways
  public $hub_verify_token1= null;
  public $input_chat = "";
  public $perpagerecord = 0;
  public $language="english";
  public $CI = NULL;

  public function __construct(){
    parent::__construct();
    $this->CI = & get_instance();

    date_default_timezone_set('Asia/Kolkata');

    $this->load->library('form_validation');
    $this->load->model('Chat_prefrences_model');
    $this->load->model('Products_model');
    $this->load->model('Admin_model');
    $this->load->model('Shopping_carts_model');
    $this->load->model('Orders_model');
    $this->load->model('Orders_products_model');
    $this->load->model('Address_model');
    $this->load->model('Coupons_model');
    $this->load->model('Packages_model');

    $this->input_chat = json_decode(file_get_contents('php://input'), true);

    $pageID = $this->input_chat['entry'][0]['id'];
    $token = getFacebookpagetoken($pageID);

    if(count($token) > 0){
      $this->access_token = @$token[0]->access_token;
    }
    else{
      $this->access_token = "EAADZBuJzbhGoBAPEbyEl8TRZAOiPILlrjEO1znSRprnDCnTd44RSkbdw08aRNelWllI6ZAIl5mevJZBmHqT5qJMQxWyUdimoMOxeKWEAaMC8Pe0yozYHQq0Nj60oHkuZCTH0Mg9U8JqFEuppOpYTLzQCQT3ZBA0b27LWcFkbcbXgZDZD";
    }
   
    if(isset($_REQUEST['hub_challenge'])) {
        $challenge = $_REQUEST['hub_challenge'];
        $hub_verify_token = $_REQUEST['hub_verify_token'];
        
        if ($hub_verify_token === $this->verify_token) {
              echo $challenge;
        }
        if ($hub_verify_token === $this->verify_token1) {
              echo $challenge;
        }
    }
   /* if($this->session->userdata('adminId') != 1) {
        redirect(base_url() . 'login');
    }*/

  }
  // Get refresh page token
  public function getPageToken($pageID,$token){
    $url = "https://graph.facebook.com/v2.10/{$pageID}?access_token={$token}&fields=access_token";
    $response = $this->curl($url,'','GET');
    return $response->access_token;
  }

  public function waitTime($time=5){
   
     $script= '';
     $script.='<script>
        var seconds = "'.$time.'";
        document.getElementById("countdown").innerHTML = "Buzz Buzz";
        function secondPassed() {
            var minutes = Math.round((seconds - 30)/60);
            var remainingSeconds = seconds % 60;
            if (remainingSeconds < 10) {
                remainingSeconds = "0" + remainingSeconds;  
            }
            document.getElementById("countdown").innerHTML = minutes + ":" + remainingSeconds;
            if (seconds == 0) {
                clearInterval(countdownTimer);
                document.getElementById("countdown").innerHTML = "Buzz Buzz";
            } else {
                seconds--;
            }
        }
      var countdownTimer = setInterval("secondPassed()", 1000);
      </script>';
      echo $script;
  }

  public function test(){

    //$usedcount = $this->Coupons_model->usedcountCoupon_By_User('1','1287153864747096');
    //var_dump(count($usedcount));exit;
/*
    $aa = $this->Shopping_carts_model->get(array('user_id' => "1287153864747096"));

    echo '<pre>';
    var_dump($aa);
    exit;*/

    $ch = curl_init("https://graph.facebook.com/v2.6/me/messenger_profile?access_token=".$this->access_token);

    //The JSON data.
    $jsonData = '{
        "setting_type" : "domain_whitelisting",
        "whitelisted_domains":[
           "https://dev.searchnative.com",
           "https://pr01002.searchnative.com"
        ],
        "domain_action_type": "add"
    }';
    //Encode the array into JSON.
    $jsonDataEncoded = $jsonData;
    //Tell cURL that we want to send a POST request.
    curl_setopt($ch, CURLOPT_POST, 1);
    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
  }

  // Admin dashboard
  public function dashboard()
  { 
    $data['content'] = 'dashboard';
    $data['title'] = 'Points live Admin Dashboard';
    $this->load->view('admin/template', $data);
  }

  // Display chatoption page
  public function chatoptions(){
    $data['content'] = 'admin/chatbot';
    $data['title'] = 'Chatbot Option';
    $this->load->view('admin/template', $data);
  }

  // Show admin profile page
  public function profile($profile_id){
      $data = array();

      $profiledata = $this->Admin_model->get(['admin_id' => $profile_id]);
      $data['configurations'] = emailconfig();

      $profileinfo = array();
      if(!empty($_FILES['profile_photo']['name'])) {

           $uploaded = do_upload($profile_id,$_FILES['profile_photo']);
           if(!empty($uploaded['data']['file_name'])){
              @unlink("uploads/".$profiledata->admin_photo);
              $profileinfo['admin_photo'] = $uploaded['data']['file_name'];
           }
           else{
            $this->session->set_flashdata('flashmsg', $uploaded['error']);
            $this->session->set_flashdata('msg', 'danger');
            redirect(base_url().'admin/profile/'.$profile_id);
           }
      }

      if($this->input->post()){

        $this->form_validation->set_rules('admin_name','Name','trim|xss_clean');
        $this->form_validation->set_rules('admin_email','Email','trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('mobile_no','Mobile Number','trim|numeric|xss_clean');

        if($this->form_validation->run() == true && filter_var($this->input->post('admin_email'), FILTER_VALIDATE_EMAIL)) {

            $profileinfo['admin_name'] = strip_tags($this->input->post('admin_name'));
            $profileinfo['admin_email'] = strip_tags($this->input->post('admin_email'));
            $profileinfo['admin_mobile'] = strip_tags($this->input->post('mobile_no'));
            
            $updated = $this->Admin_model->update($profileinfo,$profile_id);
            if($updated){
                $this->session->set_userdata('username', $this->input->post('admin_name'));
                $this->session->set_flashdata('flashmsg', 'Profile has been updated.');
                $this->session->set_flashdata('msg', 'success');
                redirect(base_url().'admin/profile/'.$profile_id);
            }
        }
      }

      if($profiledata){
        $data['profileinfo'] = $profiledata;
      }
      else{
         redirect(base_url().'admin/dashboard');
      }

      $data['content'] = 'admin/profile';
      $data['title'] = 'Profile';
      $data['page'] = $this->session->userdata('pagemanager');
      $this->load->view('admin/template', $data);
  }

  // Change admin password
  public function changePassword(){
      $data = array();

      if($this->input->post()){

          $this->form_validation->set_rules('change_pwd','New Password','trim|required|xss_clean');
          $this->form_validation->set_rules('confirm_password','Confim Password','trim|required|matches[change_pwd]|xss_clean');

          if($this->form_validation->run() == true) {

            $updated = array(
              'admin_password' => $this->_hash($this->input->post('change_pwd',true))  
            );

            $results = $this->Admin_model->update($updated,$this->session->userdata('adminId'));
            if($results){
                $this->session->set_flashdata('flashmsg', 'Password has been changed successfully.');
                $this->session->set_flashdata('msg', 'success');
                redirect(base_url().'admin/changePassword');
            }
            else{
                $this->session->set_flashdata('flashmsg', 'Password not updated.');
                $this->session->set_flashdata('msg', 'error');
                redirect(base_url().'admin/changePassword');
            }
          }
      }

      $data['content'] = 'admin/changepassword';
      $data['title'] = 'Change Password';
      $this->load->view('admin/template', $data);
  }

  // Admin session logout
  public function signout(){
      if($this->session->userdata('adminId')){
          $this->session->unset_userdata('adminId');
      }
      redirect(base_url().'login');
  }

  // Mail configuration for smtp
  public function mailConfiguration(){
      $success = true;
      $data = $this->input->post();

      foreach($data as $key => $value){

        if(!$this->save($key,$value)){
          $success=false;
          break;  
          }
      }
      if($success){
        $this->session->set_flashdata('flashmsg', 'SMTP settiing has been saved.');
        $this->session->set_flashdata('msg', 'success');
      }
      redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
  }

  // Save smptp settings
  public function save($key,$value){
    $config_data=array(
      'key'=>$key,
      'value'=>trim($value)
    );
    $this->db->where('key', $key);
    return $this->db->update('config_data',$config_data); 
  }

  // Facebook get new code for token generate
  public function getCode(){
      $code = '';

      $url = "https://graph.facebook.com/oauth/client_code?access_token=".$this->access_token."&client_secret=8d78fc75c823c045a8a26f5757201003&redirect_uri=".base_url()."admin/startchat&client_id=280068859069546";  //481696772198211 // ca8ffc244745d8ae83862b9b9f005a71 

      $response = $this->curl($url,'','GET');

      if(isset($response->code)){
        $code = $response->code;
        return $this->getRefreshAccessToken($code);
      }
      else{
        return $this->getRefreshAccessToken($code);
      } 
  }

  // Get new refresh access token
  public function getRefreshAccessToken($code){
      //var_dump($code);exit;
      $ch = curl_init("https://graph.facebook.com/oauth/access_token?code=".$code."&client_secret=8d78fc75c823c045a8a26f5757201003&redirect_uri=".base_url()."admin/startchat&client_id=280068859069546");
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      $results = curl_exec($ch);
      //var_dump($results);exit;
      $results = json_decode($results);
      if($results){
        return $results->access_token;
      }
      else{
        return $this->getCode();
      } 
      curl_close($ch);
  }
  // Call facebook api uysing CURL
  public function curl($url,$data = array(),$type){
   // echo "test";
    switch ($type) {

      case 'GET':
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
          curl_setopt($ch,CURLOPT_URL,$url);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
          //curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,2);

          if(curl_exec($ch) === false)
          {
              return curl_error($ch);
          }
          else{
              $content = curl_exec($ch);
              $content = json_decode($content);
          }
          curl_close($ch);

          if(isset($content->error)) {
            return $content->error->message;
          }else{
            return $content;
          }
        break;

      case 'POST': //echo "Dfgdgd";exit;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        //echo $this->input_chat;
        if(!empty($this->input_chat)){
          $content = curl_exec($ch);
          $content = json_decode($content);
        }
        curl_close($ch);

        if(isset($content->error)) {
          return $content->error->message;
        }else{
          return $content;
        }
        break;

      case 'DEL':
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

        //if(!empty($this->input_chat)){
          $content = curl_exec($ch);
          $content = json_decode($content);
        //}
        curl_close($ch); 

        if(isset($content->error)) {
          return $content->error->message;
        }else{
          return $content;
        }
        break;
      default:
        break;
    }
  }

  /******************************** Get page events ***********************************/
  public function events($limit='',$next=''){

    $token = $this->getCode();

    $this->access_token = $this->getPageToken($token);
 // 473467159675523
    $pagelimit = $limit ? $limit : 10;
    $nextevent = $next ? $next : '';

    $url = "https://graph.facebook.com/1048681488539732/events?fields=photos{picture},name,place,start_time,end_time,description&access_token=".$this->access_token."&limit={$pagelimit}&after={$nextevent}";

    $response = $this->curl($url,'','GET');
    return $response;
  }
  /***************************************** END ***********************************/

  public function sendWaitTimeResponse(){
      //$timer = $this->cache->get('timeinterval');
      //$currenttime = strtotime(date('Y-m-d H:i:s')) - $timer;
      $result = $this->db->get('trigger_data')->result();

      foreach ($result as  $rows) {
          $time = $rows->time_stamp;
          $date_time = date("Y-m-d H:i:s", substr($rows->time_stamp, 0, 10));
          $datetime = strtotime(date($date_time));

          $startTime = date("Y-m-d H:i:s", strtotime('+1 minutes', $datetime)); 
         
          $current = date("Y-m-d H:i:s");

           if($current >= $startTime){
           
                 $options = array(
            0 => array(
              'content_type' => 'text',
              'title' => "Yes",
              'payload' => "flagtimer"
            ),
            1 => array(
              'content_type' => 'text',
              'title' => "No",
              'payload' => "flagtimer"
            )
        ); 
        
        $quickoptions = json_encode($options);

        $message_to_reply = 'Is this what you were looking for ?';

        $response = [
                  'recipient' => [ 'id' => $rows->user_id ],
                  'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
              ];

        $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        //usleep(30000000);
        $content = curl_exec($ch);
        curl_close($ch);
        //$this->cache->delete('timeinterval');
        $trigger_id = $rows->trigger_id;
        $this->db->where('trigger_id',$trigger_id);
        $this->db->delete('trigger_data');
      }
    }
  }

  /*public function sendWaitTimeResponse(){
      $timer = $this->cache->get('timeinterval');
      $currenttime = strtotime(date('Y-m-d H:i:s')) - $timer;

      if($currenttime == 20){
        $this->cache->delete('timeinterval');
        $options = array(
            0 => array(
              'content_type' => 'text',
              'title' => "Yes",
              'payload' => "flagtimer"
            ),
            1 => array(
              'content_type' => 'text',
              'title' => "No",
              'payload' => "flagtimer"
            )
        ); 

        $quickoptions = json_encode($options);

        $message_to_reply = 'Is this what you were looking for ?';

        $response = [
                  'recipient' => [ 'id' => "1287153864747096" ],
                  'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
              ];

        $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $content = curl_exec($ch);
        curl_close($ch);
        //$this->cache->delete('timeinterval');
        clearstatcache();
        exit;
      }
  }*/

  /********************* Get page hours and location addresss ***************************/

  public function getPageLocationHours($page_id){
    // 473467159675523
    if($page_id == '1048681488539732'){
      $this->access_token = "280068859069546|JcVKVo4ZvtXp34pyFV7Djrd70lA";
    }else{
      //$token = $this->getCode();
      $this->access_token = $this->getPageToken($page_id,$this->access_token);
    }

    $url = "https://graph.facebook.com/{$page_id}?fields=about,phone,emails,name,hours,single_line_address&access_token=".$this->access_token;
    $response = $this->curl($url,'','GET');
    //var_dump($response);exit;
    return $response;
  }

  // Create facebook chatbot messenger menu
  public function createChatbotMenu(){
    $ch = curl_init("https://graph.facebook.com/v2.6/me/thread_settings?access_token=".$this->access_token);
    //The JSON data.
    $jsonData = '{
        "setting_type" : "call_to_actions",
        "thread_state" : "existing_thread",
          "call_to_actions":[
            {        
              "type":"postback",
              "title":"Home",      
              "payload":"DEVELOEPR_HOMEMENU"        
            },
            {        
              "type":"postback",
              "title":"Shopping Cart",      
              "payload":"DEVELOEPR_SHOPPINGCART"        
            },
            {        
              "type":"postback",
              "title":"Thai",      
              "payload":"DEVELOEPR_LANGUAGE"        
            },
            {        
              "type":"postback",
              "title":"English",      
              "payload":"DEVELOEPR_LANGUAGE"        
            },
            {        
              "type":"postback",
              "title":"End Chat",      
              "payload":"DEVELOEPR_END"        
            },
          ]
      }';
    //Encode the array into JSON.
    $jsonDataEncoded = $jsonData;
    //Tell cURL that we want to send a POST request.
    curl_setopt($ch, CURLOPT_POST, 1);
    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    //var_dump($result);
  }

  public function startchat(){
    //echo "test1212";
    //$total_rows_count = $this->Products_model->where('inventory','yes')->count_rows();
    $total_rows_count = $this->Products_model->where('total_qty > ','0')->count_rows();

    $chatoptionsdata = chatoptions(); // Chat options 

    $input = json_decode(file_get_contents('php://input'), true);

    file_put_contents('fb_response.txt', file_get_contents("php://input") . PHP_EOL, FILE_APPEND);

    // Page ID of Facebook page which is sending message
    $page_id = $input['entry'][0]['id'];
    $time = $input['entry'][0]['time'];
    //var_dump($input);
    $sender = $input['entry'][0]['messaging'][0]['sender']['id'];

    // Get Message text if available
    $message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;

    $messagetext = strtolower($message);

    $getfbmessage = @$input['entry'][0]['messaging'][0]['message']['attachments'];

    //var_dump($messagetext);

    if($messagetext == "0"){
        $messagetext = 1;
    }

    $quick_replies_opt = isset($input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']) ? $input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']: '' ;

    $quick_replies_opt = strtolower($quick_replies_opt);

    if(!is_null($quick_replies_opt) && $quick_replies_opt == 'payload_language_fbcheckout'){
      $languages = explode("/", $messagetext);
      $this->db->from('chat_languages');
      $this->db->where('user_id',$sender);  
      $set = $this->db->get()->result();
      $messagetext = trim($languages[0]);
      if(count($set) == 0){
          $this->db->insert('chat_languages',array('language' => trim($languages[0]),'user_id' => $sender));
      }
      else{
          $this->db->where("id",$set[0]->id);
          $this->db->update('chat_languages',array('language' => trim($languages[0])) ); 
      }
    }

    if(!is_null($quick_replies_opt) && $quick_replies_opt == 'payload_language'){
      $this->db->from('chat_languages');
      $this->db->where('user_id',$sender);  
      $set = $this->db->get()->result();
      if(count($set) == 0){
          $this->db->insert('chat_languages',array('language' => $messagetext,'user_id' => $sender));
      }
      else{
          $this->db->where("id",$set[0]->id);
          $this->db->update('chat_languages',array('language' => $messagetext)); 
      }
    }

    $language = chooselanguage($sender);
    if(sizeof($language) > 0){
      $lang = chooselanguage($sender);
      $this->language = $lang[0]->language;
    }
    else{
      $this->language = $messagetext;
    }
    //var_dump($this->language);
    lang_switcher($this->language);

    if($this->language == 'english'){
      $welcome = $chatoptionsdata[0]->welcome_msg;
      $faq_msg = $chatoptionsdata[0]->faq_msg;
      $dismiss_msg = $chatoptionsdata[0]->dismiss_msg;
      $feedback_msg = $chatoptionsdata[0]->feedback_msg;
      $product_confirm_msg = $chatoptionsdata[0]->product_confirm_msg;
      $order_cost_msg = $chatoptionsdata[0]->order_cost_msg;
      $payment_confirm_msg = $chatoptionsdata[0]->payment_confirm_msg;
      $findin_page = $chatoptionsdata[0]->findin_page;
    }
    else{
      $welcome = $chatoptionsdata[0]->welcome_msg_thai;
      $faq_msg = $chatoptionsdata[0]->faq_msg_thai;
      $dismiss_msg = $chatoptionsdata[0]->dismiss_msg_thai;
      $feedback_msg = $chatoptionsdata[0]->feedback_msg_thai;
      $product_confirm_msg = $chatoptionsdata[0]->product_confirm_msg_thai;
      $order_cost_msg = $chatoptionsdata[0]->order_cost_msg_thai;
      $payment_confirm_msg = $chatoptionsdata[0]->payment_confirm_msg_thai;
      $findin_page = $chatoptionsdata[0]->findin_page_thai;
    }

    // Get Postback payload if available
    $postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;

    $posttitle = isset($input['entry'][0]['messaging'][0]['postback']['title']) ? $input['entry'][0]['messaging'][0]['postback']['title']: '' ;

    $posttitletext = strtolower($posttitle);

    $message_to_reply = '';
    $datas = array();
    //exit;

    if($messagetext || $postback){ 
    
       //$token = $this->getCode(); // Get New User Token
       //$this->access_token = $this->getPageToken($token); // Get Page new token

        if($messagetext){ //echo "dfd";
            switch ($quick_replies_opt) {

              case 'payload_removecoupon':
                  $methodpay = $this->cache->get("pay_method_{$sender}");
                  $district = $this->cache->get("validdistrict_{$sender}");
                  $postcode = $this->cache->get("validpostcode_{$sender}");
                  $addressdata =$this->cache->get("valid_getaddress_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); 
                  if($this->language == 'english'){
                    $shipcountry = "Thailand";
                  }
                  else{
                    $shipcountry = "ประเทศไทย";
                  }
                  $shipphone =$this->cache->get("delivery_getphone_{$sender}");
                  $shipemail = $this->cache->get("delivery_getemail_{$sender}");
                  $useraddress = $this->cache->get("deliver_address_{$sender}");
                  $couponID = $this->cache->get("lastcouponID_{$sender}");
                  $shipername = $this->cache->get("shipername_{$sender}");

                  $data = productlists_by_userid($sender);
                    $table = '';
                    $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                    $table.="----------------------------------";
                    $table.="\n";
                    $sum = 0;
                    $shipping = 0;
                    $total = 0;
                    foreach ($data as $key => $value) {

                        if($this->language == 'english'){
                          $name = $value->product_name;
                        }
                        else{
                          $name = $value->product_name_thai;
                        }
                        if(strlen($name) > 6){
                          $table.= wordwrap($name, 6, "\n", true);
                          $table.="\t";
                        }else{
                          $table.=$name."\t";
                          //$table.="\t\t\t";
                        }
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }

                        $table.="\t".$value->qty."\t";
                        $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                        $sum = $sum + ($value->qty * $price);
                        $shipping = $shipping + $value->shipping_cost;
                        $table.="----------------------------------";
                        $table.="\n";

                    }

                    $table.="----------------------------------";
                    $table.="\t\n";
                    $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                    $table.="\t\n";
                    $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                    $table.="\t\n";
                    $total = $sum + $shipping;
                    $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                  // check coupon exists
                  $checkcoupon = $this->Coupons_model->removeCoupon($couponID);
                  //file_put_contents('filename.txt', print_r($checkcoupon, true));
                  if($checkcoupon){
                    $message_to_reply = $this->lang->line('removecoupon');
                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                     ];
                    $this->cache->delete("lastcouponID_{$sender}"); // Remove couponID
                    $this->cache->delete("discountamount_{$sender}"); // Remove discount amount
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    // Feedback tayo 07-02-2018
                    $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "payload_coupon"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "payload_coupon"
                        )
                    ); 

                    $quickoptions = json_encode($options);
                    $message_to_reply = $this->lang->line('havecode');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                    ];
                    $this->cache->save("pay_method_{$sender}",$methodpay,300);
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }
                  // Comment out feedback tayo 07-02-2018
                  /*if($methodpay == "cash" || $methodpay == "เงินสด"){
                        $answer = ["attachment"=>[
                              "type"=>"template",
                              "payload"=>[
                                "template_type"=>"button",
                                "text"=>$table,
                                "buttons"=>[
                                  [
                                    "type"=>"postback",
                                    "title"=>$this->lang->line('paybtn'),
                                    "payload"=>"Payment_Checkout_Cash"
                                  ]
                                ]
                              ]
                          ]
                      ];
                    }
                  else if($methodpay == "prepaid" || $methodpay == "เติมเงิน"){
                      $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$table,
                            "buttons"=>[
                              [
                                  "type"=>"web_url",
                                  "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                                  "title"=>"Checkout",
                                  "webview_height_ratio"=>"full",
                                  "messenger_extensions"=> true,
                                  "fallback_url"=> base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}"
                              ]
                            ]
                          ]
                        ]];
                    }
                $response = [
                  'recipient' => [ 'id' => $sender ],
                  'message' => $answer
                ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');*/

                // comment feedback tayo 07-02-2018
                /*$options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "payload_coupon"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "payload_coupon"
                        )
                    ); 

                $quickoptions = json_encode($options);
                $message_to_reply = $this->lang->line('havecode');

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                ];
                $this->cache->save("pay_method_{$sender}",$methodpay,300);
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');*/

                if(!empty($useraddress)){
                  $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                }else{
                  $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                  $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                  $this->cache->save("valid_getaddress_{$sender}",$addressdata,300); // 3 minutes
                  $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                  //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                  $this->cache->save("delivery_getphone_{$sender}",$shipphone,300); // 3 minutes
                  $this->cache->save("delivery_getemail_{$sender}",$shipemail,300); // 3 minutes
                  $this->cache->save("shipername_{$sender}",$shipername,300);
                }

              break;

              case 'payload_coupon':
                  $methodpay = $this->cache->get("pay_method_{$sender}");
                  $district = $this->cache->get("validdistrict_{$sender}");
                  $postcode = $this->cache->get("validpostcode_{$sender}");
                  $addressdata =$this->cache->get("valid_getaddress_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); 
                  $shipername = $this->cache->get("shipername_{$sender}");
                  if($this->language == 'english'){
                    $shipcountry = "Thailand";
                  }
                  else{
                    $shipcountry = "ประเทศไทย";
                  }
                  $shipphone =$this->cache->get("delivery_getphone_{$sender}");
                  $shipemail = $this->cache->get("delivery_getemail_{$sender}");
                  $useraddress = $this->cache->get("deliver_address_{$sender}");

                  if($messagetext == strtolower($this->lang->line('yes'))){
                    $message_to_reply = $this->lang->line('entercoupon');

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->cache->save("couponcode_{$sender}",'coupon_offer',300);
                    $this->cache->save("pay_method_{$sender}",$methodpay,300);

                    if(!empty($useraddress)){
                      $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                    }else{
                      $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                      $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                      $this->cache->save("valid_getaddress_{$sender}",$addressdata,300); // 3 minutes
                      $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                      //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                      $this->cache->save("delivery_getphone_{$sender}",$shipphone,300); // 3 minutes
                      $this->cache->save("delivery_getemail_{$sender}",$shipemail,300); // 3 minutes
                      $this->cache->save("shipername_{$sender}",$shipername,300);
                    }
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }
                  else{
                    $data = productlists_by_userid($sender);
                    $table = '';
                    $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                    $table.="----------------------------------";
                    $table.="\n";
                    $sum = 0;
                    $shipping = 0;
                    $total = 0;
                    foreach ($data as $key => $value) {

                        if($this->language == 'english'){
                          $name = $value->product_name;
                        }
                        else{
                          $name = $value->product_name_thai;
                        }
                        if(strlen($name) > 6){
                          $table.= wordwrap($name, 6, "\n", true);
                          $table.="\t";
                        }else{
                          $table.=$name."\t";
                          //$table.="\t\t\t";
                        }
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }

                        $table.="\t".$value->qty."\t";
                        $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                        $sum = $sum + ($value->qty * $price);
                        $shipping = $shipping + $value->shipping_cost;
                        $table.="----------------------------------";
                        $table.="\n";

                    }

                    $table.="----------------------------------";
                    $table.="\t\n";
                    $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                    $table.="\t\n";
                    $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                    $table.="\t\n";
                    $total = $sum + $shipping;
                    $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                    if($methodpay == "cash" || $methodpay == "เงินสด"){
                      
                      if(!empty($useraddress)){
                        $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                      }
                      $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                              "template_type"=>"button",
                              "text"=>$table,
                              "buttons"=>[
                                [
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('paybtn'),
                                  "payload"=>"Payment_Checkout_Cash"
                                ]
                              ]
                            ]
                        ]
                    ];
                  }
                  else if($methodpay == "prepaid" || $methodpay == "เติมเงิน"){

                    if(!empty($useraddress)){
                      $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                    }
                    else{
                    $info = array(
                        'user_id'   => $sender,
                        'postcode'  => $postcode,
                        'district'  => $district,
                        'country'   => $shipcountry,
                        'receiver_name' => $shipername,
                        'phone'     => $shipphone,
                        'email'     => $shipemail,
                        'delivery_method'  =>$deliverymethod,
                        'shipping_address' => $addressdata
                      );
                      $addressID = $this->Address_model->insert($info);
                    }

                    $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                          "template_type"=>"button",
                          "text"=>$table,
                          "buttons"=>[
                            [
                                "type"=>"web_url",
                                "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                                "title"=>"Checkout",
                                "webview_height_ratio"=>"full",
                                "messenger_extensions"=> true,
                                "fallback_url"=> base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}"
                            ]
                          ]
                        ]
                      ]];
                  }
                }
                $response = [
                  'recipient' => [ 'id' => $sender ],
                  'message' => $answer
                ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              break;

              // feedback tayo implement new case 07-02-2018
              case 'payload_proceedcheckout':
                $this->cache->delete("couponcode_{$sender}");
                $methodpay = $this->cache->get("pay_method_{$sender}");  
                $useraddress = $this->cache->get("deliver_address_{$sender}");
                $shipername = $this->cache->get("shipername_{$sender}");

                $data = productlists_by_userid($sender);
                $table = '';
                $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                $table.="----------------------------------";
                $table.="\n";
                $sum = 0;
                $shipping = 0;
                $total = 0;
                foreach ($data as $key => $value) {

                    if($this->language == 'english'){
                      $name = $value->product_name;
                    }
                    else{
                      $name = $value->product_name_thai;
                    }
                    if(strlen($name) > 6){
                      $table.= wordwrap($name, 6, "\n", true);
                      $table.="\t";
                    }else{
                      $table.=$name."\t";
                      //$table.="\t\t\t";
                    }
                    if($value->sale_price > 0){
                      $price = $value->sale_price;
                    }else{
                      $price = $value->product_price;
                    }

                    $table.="\t".$value->qty."\t";
                    $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                    $sum = $sum + ($value->qty * $price);
                    $shipping = $shipping + $value->shipping_cost;
                    $table.="----------------------------------";
                    $table.="\n";

                }

                $table.="----------------------------------";
                $table.="\t\n";
                $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                $table.="\t\n";
                $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                $table.="\t\n";
                $total = $sum + $shipping;
                $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                if($methodpay == "cash" || $methodpay == "เงินสด"){
                  
                    if(!empty($useraddress)){
                      $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                    }
                    $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$table,
                            "buttons"=>[
                              [
                                "type"=>"postback",
                                "title"=>$this->lang->line('paybtn'),
                                "payload"=>"Payment_Checkout_Cash"
                              ]
                            ]
                          ]
                      ]
                  ];
                }
                else if($methodpay == "prepaid" || $methodpay == "เติมเงิน"){

                  $district = $this->cache->get("validdistrict_{$sender}");
                  $postcode = $this->cache->get("validpostcode_{$sender}");
                  $addressdata = $this->cache->get("valid_getaddress_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); 
                  if($this->language == 'english'){
                    $shipcountry = "Thailand";
                  }
                  else{
                    $shipcountry = "ประเทศไทย";
                  }
                  //$shipcountry = $this->cache->get('delivery_getcountry'); 
                  $shipphone = $this->cache->get("delivery_getphone_{$sender}"); 
                  $shipemail = $this->cache->get("delivery_getemail_{$sender}");  

                  if(!empty($useraddress)){
                    $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                  }
                  else{
                  $info = array(
                      'user_id'   => $sender,
                      'postcode'  => $postcode,
                      'district'  => $district,
                      'country'   => $shipcountry,
                      'receiver_name' => $shipername,
                      'phone'     => $shipphone,
                      'email'     => $shipemail,
                      'delivery_method'  =>$deliverymethod,
                      'shipping_address' => $addressdata
                    );
                    $addressID = $this->Address_model->insert($info);
                  }

                  $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$table,
                        "buttons"=>[
                          [
                              "type"=>"web_url",
                              "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                              "title"=>"Checkout",
                              "webview_height_ratio"=>"full",
                              "messenger_extensions"=> true,
                              "fallback_url"=> base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}"
                          ]
                        ]
                      ]
                    ]];
                }
                $response = [
                  'recipient' => [ 'id' => $sender ],
                  'message' => $answer
                ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              break;

              case 'payload_cart_check':
                    if($messagetext == strtolower($this->lang->line('yes'))){
                        $qty = $this->cache->get("userqty_{$sender}");
                        $pid = $this->cache->get("product_id_{$sender}");
                        $pname = $this->cache->get("product_title_{$sender}");

                        $checkqty = $this->Products_model->get(array('product_id' => $pid));

                        /* if($qty > $checkqty->total_qty){
                          $message_to_reply = "Requested quantity not available for {$pname}.";

                          $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply]
                          ];
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                        }*/
                        /*else{*/
                          $checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}",'product_id'=>$pid))->get();
                          if($checkexists){
                            $updateqty = $checkexists->qty + $qty; // Update quantity
                            $update_data = array('qty'=>$updateqty);
                            $updated = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}",'product_id'=>$pid))->update($update_data);
                            if($updated){

                              //$availqty = $checkqty->total_qty - $qty;
                              //$updated_data = array('total_qty' => $availqty);
                                // update quantity
                             // $this->Products_model->where('product_id',$pid)->update($updated_data);

                              $message_to_reply = $this->lang->line('cartupdated'). " {$updateqty}";

                              $responseupdate = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply]
                              ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseupdate,'POST');

                              $options = array(
                                      0 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('continueshopping'),
                                          'payload' => "flag"
                                      ),
                                      1 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('updatecart'),
                                          'payload' => "PAY_LOAD_1"
                                      ),
                                      2 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('checkout'),
                                          'payload' => "PAY_LOAD_1"
                                      )
                                  ); 

                               $quickoptions = json_encode($options);

                               $message_to_reply = $this->lang->line('addbasket');

                               $response = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                               ];

                               $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                            }
                          }
                          else{
                           // $availqty = $checkqty->total_qty - $qty;
                            //$update_data = array('total_qty' => $availqty);
                            // update quantity
                            //$this->Products_model->where('product_id',$pid)->update($update_data);
                            $data = array(
                              'user_id'     => $sender,
                              'product_id'  => $pid,
                              'qty'         => $qty
                            );  

                            $ok = $this->Shopping_carts_model->insert($data);

                            if($ok){
                              $products = $this->Products_model->get(array('product_id' => $pid));

                              if($this->language == 'english'){
                                $replaceword = str_replace("(product_name)", "{$pname}", $product_confirm_msg);
                              }
                              else{
                                $replaceword = str_replace("(product_name)", "{$pname}", $payment_confirm_msg);
                              }

                              $message_to_reply = $replaceword;

                              $responsedata = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply]
                               ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');

                              if($products->sale_price > 0){
                                $price = $products->sale_price;
                              }else{
                                $price = $products->product_price;
                              }
                              
                              $totalprice = ($price * $qty);
                               if($this->language == 'english'){
                                $replacewordorder = str_replace("(no_of_unit)", "{$qty} ", $order_cost_msg);
                                $replacewordorder = str_replace("(total_price)", "{$totalprice}", $replacewordorder);
                               }
                               else{
                                $replacewordorder = str_replace("(no_of_unit)", "{$qty} ", $order_cost_msg);
                                $replacewordorder = str_replace("(total_price)", "{$totalprice}", $replacewordorder);
                               }

                              $message_to_reply = $replacewordorder;

                              $responsedatas = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply]
                               ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedatas,'POST');

                              $options = array(
                                      0 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('continueshopping'),
                                          'payload' => "flag"
                                      ),
                                      1 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('updatecart'),
                                          'payload' => "PAY_LOAD_1"
                                      ),
                                      2 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('checkout'),
                                          'payload' => "PAY_LOAD_1"
                                      )
                                  ); 

                             $quickoptions = json_encode($options);

                             $message_to_reply = $this->lang->line('addbasket');

                             $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                             ];

                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                              exit; 
                        }
                      }
                    }
                    if($messagetext == strtolower($this->lang->line('no'))){

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $options = array(
                          0 => array(
                            'content_type' => 'text',
                            'title' =>$this->lang->line('yes'),
                            'payload' => "flag"
                          ),
                          1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "flag"
                          )
                        ); 

                       $quickoptions = json_encode($options);
                       $message_to_reply = $findin_page ? $findin_page : $this->lang->line('still');
                     
                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
                    $this->cache->delete("userqty_{$sender}");
                    $this->cache->delete("product_id_{$sender}");
                    $this->cache->delete("product_title_{$sender}");
              break;
              case 'payload_messegner':
                  if($messagetext == strtolower($this->lang->line('yes'))){
                      $pname = $this->cache->get("product_title_{$sender}");
                      $splitsword = explode("/", $pname);
                      $searchproducts = searchproduct_by_messenger($splitsword[0],$page_id);

                      if($searchproducts[0]->total_qty == 0){
                        $message_to_reply = $this->lang->line('outstock');

                        $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                        ];
                        $this->cache->delete("product_title_{$sender}");
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                      }else{

                      $pid = $searchproducts[0]->product_id;
                    
                      $message_to_reply = $this->lang->line('unit_confirm');
          
                      $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                      ];
                      $this->cache->save("product_id_{$sender}",$pid,600); // 10 minutes
                      $this->cache->save("product_title_{$sender}",$pname,600); // 10 minutes
                      //echo "get". $this->cache->get('product_id');
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                      }
                  }
                  if($messagetext == strtolower($this->lang->line('no'))){
                    $this->cache->save("keysearch_{$sender}",'other',300); // 5 minutes

                    $message_to_reply = $this->lang->line('wantbuy');
                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];
                    $this->cache->delete("product_title_{$sender}");
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }
              break;  
              case 'payload_checkout_cart':
                    if($messagetext == strtolower($this->lang->line('yes'))){
                      // Checkout process with shopping list
                      $data = productlists_by_userid($sender);
                      $table = '';
                      $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                      $table.="----------------------------------";
                      $table.="\n";
                      $sum = 0;
                      $shipping = 0;
                      $total = 0;
                      $price = 0;
                      foreach ($data as $key => $value) {
                          if($this->language == 'english'){
                            $name = $value->product_name;
                          }
                          else{
                            $name = $value->product_name_thai;
                          }
                          if(strlen($name) > 6){
                            $table.= wordwrap($name, 6, "\n", true);
                            $table.="\t";
                          }else{
                            $table.=$name."\t";
                            //$table.="\t\t\t";
                          }
                          if($value->sale_price > 0){
                            $price = $value->sale_price;
                          }else{
                            $price = $value->product_price;
                          }

                          $table.="\t".$value->qty."\t";
                          $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                          $sum = $sum + ($value->qty * $price);
                          $shipping = $shipping + $value->shipping_cost;
                          $table.="----------------------------------";
                          $table.="\n";
                      }
                      $table.="----------------------------------";
                      $table.="\t\n";
                      $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                      $table.="\t\n";
                      $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                      $table.="\t\n";
                      $total = $sum + $shipping;
                      $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                      $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$table,
                            /*"buttons"=>[
                              [
                                "type"=>"web_url",
                                "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$this->language}",
                                "title"=>$this->lang->line('checkout'),
                                "webview_height_ratio"=>"full",
                                "messenger_extensions"=> true,
                                "fallback_url"=>base_url()."admin/productlistsByUserid/{$sender}/{$this->language}" 
                                //base_url()."admin/productlistsByUserid/{$sender} 
                              ]
                            ]*/
                          ]
                        ]];
                      
                      $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => ['text' => $table] // $answer
                      ];

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $this->cache->delete("delivery_postcode_{$sender}");
                      $this->cache->delete("validdistrict_{$sender}");
                      $this->cache->delete("validpostcode_{$sender}");
                      $this->cache->delete("valid_getaddress_{$sender}");
                      $this->cache->delete("delivery_method_{$sender}"); 
                      //$this->cache->delete('delivery_getcountry'); 
                      $this->cache->delete("delivery_getphone_{$sender}"); 
                      $this->cache->delete("delivery_getemail_{$sender}");
                      $this->cache->delete("deliver_address_{$sender}");
                      $this->cache->delete("discountamount_{$sender}");   
                      $this->cache->delete("lastcouponID_{$sender}");
                      $this->cache->delete("pay_method_{$sender}");
                      $this->cache->delete("shipername_{$sender}");

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => ['text' => $this->lang->line('pickaddress')]
                      ]; 
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    // Selection of address
                    $address = array();
                    $addressLists = getuserAddress($sender);

                    if(count($addressLists) > 0){
                      $address[] = array(
                          "title"     => $this->lang->line('newaddresstitle'), 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                              array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('newaddress'),
                                "payload"=>"PAYLOAD_NEWADDRESS"
                              )
                          )  
                      );
                      foreach($addressLists as $key => $addr) {
                        $address[] = array(
                          "title"     => $addr->shipping_address, 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('useaddress'),
                                  "payload"=>"{$addr->id}"
                                  )
                              )  
                          );
                      }
                    }
                    else{
                        $address[] = array(
                          "title"     => $this->lang->line('newaddresstitle'), 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('newaddress'),
                                  "payload"=>"PAYLOAD_NEWADDRESS"
                                  )
                              )  
                          );
                    }

                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                          "template_type"=>"generic",
                          "elements"=> $address
                          ]
                        ]
                      ];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];

                    /*$options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_nextday'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_express'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          ),
                          2 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_standard'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          )
                      ); 

                    $quickoptions = json_encode($options);

                    $message_to_reply = $this->lang->line('delivery_speed');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                    ];*/

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    }
                    if($messagetext == strtolower($this->lang->line('no'))){

                    //$this->cache->save('SS',"dfdfd");  
                    $this->cache->delete("delivery_postcode_{$sender}");
                    $this->cache->delete("validdistrict_{$sender}");
                    $this->cache->delete("validpostcode_{$sender}");
                    $this->cache->delete("valid_getaddress_{$sender}");
                    $this->cache->delete("delivery_method_{$sender}"); 
                    //$this->cache->delete('delivery_getcountry'); 
                    $this->cache->delete("delivery_getphone_{$sender}"); 
                    $this->cache->delete("delivery_getemail_{$sender}");

                      $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome: 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('mostpopular'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('discover')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                     $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $answer 
                     ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
              break;
              case 'category':
                $page = 0;
                $catproducts = productlists_by_category($messagetext,$page,10);
                $total_rows_count = $catproducts['totalrows'];
                //file_put_contents('filename.txt', print_r($catproducts, true));
               // file_put_contents('fb_response.txt', serialize($catproducts));
                $products = array();
                $price = 0;

                if(count($catproducts['result']) > 0) :
                  foreach ($catproducts['result'] as $key => $product) {
                      if($this->language == 'english'){
                        $name = $product->product_name;
                        $description = $product->product_description;
                      }
                      else{
                        $name = $product->product_name_thai;
                        $description = $product->product_description_thai;
                      }

                      if($product->sale_price > 0){
                        $price = $product->sale_price;
                      }else{
                        $price = $product->product_price;
                      }

                      $products[] = array(
                      "title"     => $name, 
                      "image_url" => base_url()."uploads/products/{$product->product_images[0]}",
                      "subtitle"  => '฿'.$price .' - '.$description,
                      "buttons"   => array(
                               /*array(
                                "type"=>"payment",
                                "title"=>"buy",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "payment_summary"=>[
                                  "currency"=>"USD",
                                  "payment_type"=>"FIXED_AMOUNT",
                                  "is_test_payment" => true, 
                                  "merchant_name"=>"Points Live",
                                  "requested_user_info"=>["shipping_address","contact_name", "contact_email"],
                                  "price_list"=>[
                                    [
                                      "label"=>"Subtotal",
                                      "amount"=>$product->product_price
                                    ],
                                    [
                                      "label"=>"Shipping",
                                      "amount"=>$product->shipping_cost
                                    ]
                                  ]
                                ]
                              ),*/
                             /* array(
                                "type"=>"web_url",
                                "url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}",
                                "title"=>$this->lang->line('checkout'),
                                "webview_height_ratio"=>"full",
                                "messenger_extensions"=> true,
                                "fallback_url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}" 
                              ),*/
                              array(
                              "type"=>"postback",
                              "title"=>$this->lang->line('checkout'),
                              "payload"=>"$product->product_id"
                              ),
                              array(
                              "type"=>"postback",
                              "title"=>$this->lang->line('addtocart'),
                              "payload"=>"$product->product_id"
                              )
                          )  
                      );
                  }

                 $answer = ["attachment"=>[
                  "type"=>"template",
                  "payload"=>[
                      "template_type"=>"generic",
                      "elements"=> $products
                      ]
                    ]
                  ];

                else :

                  $message_to_reply = "{$this->lang->line('nocatfound')} {$messagetext}";
                  $answer = [ 'text' => $message_to_reply];
                
                endif;

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => $answer 
                ];

                if($total_rows_count > 10){
                      $page = $page + 1;
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $this->cache->save('nextcategoryproduct', $page);
                      $this->cache->save('currentcategory', $messagetext);
                      $this->cache->save('currentcategorypagecount', count($catproducts['result']));
                
                      $nextproducts[] = array(
                          "title"     => $this->lang->line('clickviewmoreproduct'), 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('moreproduct'),
                                  "payload"=>'Category_Payload'
                                  )      
                              )  
                          );

                      $nextproductdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextproducts
                                ]
                            ]];

                    $responsedata = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextproductdata
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');

                    //$checkTime = strtotime(date('H:i:s'));
                    //$this->cache->save('timeinterval',$checkTime,120);
                    exit;
                }
                else{

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  $productslists =  productcarts_by_userid($sender);
                  $total_rows_count_carts = count($productslists);

                  if($total_rows_count_carts > 0){
                    $options = array(
                        0 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('updatecart'),
                          'payload' => "PAY_LOAD_1"
                        ),
                        1 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('no'),
                          'payload' => "flag"
                        )
                      ); 

                     $quickoptions = json_encode($options);

                     $message_to_reply = $this->lang->line('continueaddcart'); //$findin_page ? $findin_page : $this->lang->line('still');
                  }else{
                    $options = array(
                        0 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('yes'),
                          'payload' => "flag"
                        ),
                        1 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('no'),
                          'payload' => "flag"
                        )
                      ); 

                     $quickoptions = json_encode($options);

                     $message_to_reply = $this->lang->line('continueaddcart'); //$findin_page ? $findin_page : $this->lang->line('still');
                  }

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                   ];

                   $this->db->where('user_id',$sender);
                   $this->db->delete('trigger_data');

                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  /*$checkTime = strtotime(date('H:i:s'));
                  $this->cache->save('timeinterval',$checkTime,120);*/  
                }
                $this->db->where('user_id',$sender);    
                $trigger = $this->db->get('trigger_data')->row();
                if($trigger){                        
                  
                  $userdata = ['user_id'=>$sender,'time_stamp'=>$time];
                  $this->db->where('trigger_id',$trigger->trigger_id);
                  $this->db->update('trigger_data',$userdata);
                }else{                        
                  
                  $userdata = ['user_id'=>$sender,'time_stamp'=>$time];
                  $this->db->insert('trigger_data',$userdata);
                }
                break;

              case 'pay_load_1':

               if($messagetext == "categories"){
                    $optionslist = array();
                    $categories = categroy_lists($page_id);//$page_id

                    if(count($categories) > 0){
                      foreach ($categories as $value) {
                          if($this->language == 'english'){
                            $categoryanme = $value->category_name;
                          }
                          else{
                            $categoryanme = $value->category_name_thai; 
                          }
                          $optionslist[] = array(
                            'content_type' => 'text',
                            'title' => ucwords($categoryanme),
                            'payload' => "category" 
                          );
                      }
                      $quickoptions = json_encode($optionslist);

                      $message_to_reply = $this->lang->line('looking');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                    }
                    else{
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $this->lang->line('nocat')
                      ];
                   }
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                   exit;
                }
                if($messagetext == strtolower($this->lang->line('other'))){

                    $this->cache->save("keysearch_{$sender}", $messagetext,300);

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $this->lang->line('searchproduct')]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }
                /* if($messagetext == strtolower($this->lang->line('event'))){
                  $eventsData = $this->events();
                  $events = array();

                    if(count($eventsData->data) > 0) :

                      foreach ($eventsData->data as $key => $value) {

                          $events[] = array(
                          "title"     => $value->name, 
                          "image_url" => $value->photos->data[0]->picture,
                          "subtitle"  => $value->description,
                          "buttons"   => array(
                                  array(
                                  "type"=>"web_url",
                                  "url"=>"https://www.facebook.com/events/".$value->id,
                                  "title"=>$this->lang->line('viewevent') 
                                  ),
                                  array(
                                  "type"=>"postback",
                                  "title"=>"Pick options",
                                  "payload"=>$value->id
                                  )     
                              )  
                          );
                      }

                      $answer = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$events
                                ]
                            ]];

                    else :

                      $message_to_reply = $this->lang->line('noevent');
                      $answer = ['text' => $answer ];

                    endif;  

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer
                    ];


                    if(isset($eventsData->paging->next)) :

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    
                      $this->cache->save('nextevent', $eventsData->paging->cursors->after);

                      $nextevents[] = array(
                          "title"     => $this->lang->line('clickviewmoreevent'), 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('moreevents'),
                                  "payload"=>'Events_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextevents
                                ]
                            ]];

                    $responsedata = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');
                    exit;

                  else :

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  exit; 
                  endif;
                } */
                if($messagetext == strtolower($this->lang->line('product/services')) || $messagetext == strtolower($this->lang->line('yes'))){
                    $page = 0;
                    $productslists = product_lists(0,10,$page_id);
                     // var_dump($productslists);exit;
                    $products = array();

                    if(count($productslists) > 0) :
                        foreach ($productslists as $key => $product) {
                            if($this->language == 'english'){
                              $name = $product->product_name;
                              $description = $product->product_description;
                            }else{
                              $name = $product->product_name_thai;
                              $description = $product->product_description_thai;
                            }

                            if($product->sale_price > 0){
                              $price = $product->sale_price;
                            }else{
                              $price = $product->product_price;
                            }

                            $products[] = array(
                            "title"     => $name, 
                            "image_url" => base_url()."uploads/products/{$product->images[0]->product_image}",
                            "subtitle"  => '฿'.$price .' - '.$description,
                            "buttons"   => array(
                                    /*array(
                                      "type"=>"payment",
                                      "title"=>"buy",
                                      "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                      "payment_summary"=>[
                                        "currency"=>"USD",
                                        "payment_type"=>"FIXED_AMOUNT",
                                        "is_test_payment" => true, 
                                        "merchant_name"=>"Points Live",
                                        "requested_user_info"=>["shipping_address","contact_name", "contact_email"],
                                        "price_list"=>[
                                          [
                                            "label"=>"Subtotal",
                                            "amount"=>$product->product_price
                                          ],
                                          [
                                            "label"=>"Shipping",
                                            "amount"=>$product->shipping_cost
                                          ]
                                        ]
                                      ]
                                    ),*/
                                    /*array(
                                      "type"=>"web_url",
                                      "url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}",
                                      "title"=>$this->lang->line('checkout'),
                                      "webview_height_ratio"=>"full",
                                      "messenger_extensions"=> true,
                                      "fallback_url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}" 
                                    ),*/
                                    array(
                                    "type"=>"postback",
                                    "title"=>$this->lang->line('checkout'),
                                    "payload"=>"$product->product_id"
                                    ),
                                     array(
                                    "type"=>"postback",
                                    "title"=>$this->lang->line('addtocart'),
                                    "payload"=>"$product->product_id"
                                    )
                                )  
                            );
                        }

                      $this->perpagerecord = count($productslists); // Current page count

                       $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                            "template_type"=>"generic",
                            "elements"=> $products
                            ]
                          ]
                        ];

                    else :
                        
                      $message_to_reply = $this->lang->line('noproductfound');

                      $answer = [ 'text' => $message_to_reply];

                    endif;

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];



                    if($total_rows_count > 10) :

                      $page = $page + 1;
                      
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                    
                      $this->cache->save("nextproduct_{$sender}", $page,300);
                      $this->cache->save("currentpagecount_{$sender}", $this->perpagerecord,300);

                      $nextproducts[] = array(
                          "title"     => $this->lang->line('clickviewmoreproduct'), 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('moreproduct'),
                                  "payload"=>'Products_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextproducts
                                ]
                            ]];

                    $responsedata = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');
                  
                    else :

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');    
                    endif;
                    exit;
                }
                // feedback tayo 07-02-2018
                if($messagetext == strtolower($this->lang->line('no')) || $messagetext == strtolower($this->lang->line('checkout'))){

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => ['text' => $this->lang->line('pickaddress')]
                    ]; 
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    $address = array();
                    $addressLists = getuserAddress($sender);

                    if(count($addressLists) > 0){
                      $address[] = array(
                          "title"     => $this->lang->line('newaddresstitle'), 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                              array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('newaddress'),
                                "payload"=>"PAYLOAD_NEWADDRESS"
                              )
                          )  
                      );
                      foreach($addressLists as $key => $addr) {
                        $address[] = array(
                          "title"     => $addr->shipping_address, 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('useaddress'),
                                  "payload"=>"{$addr->id}"
                                  )
                              )  
                          );
                      }
                    }
                    else{
                        $address[] = array(
                          "title"     => $this->lang->line('newaddresstitle'), 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('newaddress'),
                                  "payload"=>"PAYLOAD_NEWADDRESS"
                                  )
                              )  
                          );
                    }

                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                          "template_type"=>"generic",
                          "elements"=> $address
                          ]
                        ]
                      ];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  /*$options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_nextday'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_express'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          ),
                          2 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_standard'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          )
                      ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply = $this->lang->line('delivery_speed');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');*/

                  // Checkout process with shopping list
                  /*$data = productlists_by_userid($sender);
                  $table = '';
                  $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                  $table.="----------------------------------";
                  $table.="\n";
                  $sum = 0;
                  $shipping = 0;
                  $total = 0;
                  $price = 0;
                  foreach ($data as $key => $value) {
                      if($this->language == 'english'){
                        $name = $value->product_name;
                      }
                      else{
                        $name = $value->product_name_thai;
                      }
                      if($value->sale_price > 0){
                        $price = $value->sale_price;
                      }else{
                        $price = $value->product_price;
                      }

                      if(strlen($name) > 6){
                        $table.= wordwrap($name, 6, "\n", true);
                        $table.="\t";
                      }else{
                        $table.=$name."\t";
                        //$table.="\t\t\t";
                      }
                      $table.="\t".$value->qty."\t";
                      $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                      $sum = $sum + ($value->qty * $price);
                      $shipping = $shipping + $value->shipping_cost;
                      $table.="----------------------------------";
                      $table.="\n";
                  }
                  $table.="----------------------------------";
                  $table.="\t\n";
                  $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                  $table.="\t\n";
                  $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                  $table.="\t\n";
                  $total = $sum + $shipping;
                  $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                  $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$table,
                        "buttons"=>[
                          [
                            "type"=>"web_url",
                            "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$this->language}",
                            "title"=>$this->lang->line('checkout'),
                            "webview_height_ratio"=>"full",
                            "messenger_extensions"=> true,
                            "fallback_url"=>base_url()."admin/productlistsByUserid/{$sender}/{$this->language}" 
                            //base_url()."admin/productlistsByUserid/{$sender} 
                          ]
                        ]
                      ]
                    ]];
                  
                  $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer
                  ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  exit;*/
                }
                if($messagetext == strtolower($this->lang->line('updatecart'))){
                    $page = 0;
                    $productslists =  productcarts_by_userid($sender);
                    $products = array();
                    $price = 0;
                    $total_rows_carts = $this->Shopping_carts_model->where(array('user_id' => $sender))->get_all();
                    $total_rows_count_carts = count($total_rows_carts);

                    foreach ($productslists as $key => $product) {
                        if($this->language == 'english'){
                           $name = $product->product_name;
                           $description = $product->product_description;
                        }else{
                           $name = $product->product_name_thai;
                           $description = $product->product_description_thai;
                        }

                        if($product->sale_price > 0){
                          $price = $product->sale_price;
                        }else{
                          $price = $product->product_price;
                        }

                        $products[] = array(
                        "title"     => $name, 
                        "image_url" => base_url()."uploads/products/{$product->product_image}",
                        "subtitle"  => '฿'.$price .' - '.$description,
                        "buttons"   => array(
                                array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('updateqty'),
                                "payload"=>"$product->product_id"
                                ),
                                 array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('removeproduct'),
                                "payload"=>"$product->product_id"
                                )
                            )  
                        );
                    }
                    $this->perpagerecord = count($productslists); // Current page count

                     $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                          "template_type"=>"generic",
                          "elements"=> $products
                          ]
                        ]
                      ];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    exit;

                    if($total_rows_count_carts > 10) :

                      $page = $page + 1;
                      
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                    
                      $this->cache->save('nextcartproduct', $page);
                      $this->cache->save('currentcartpagecount', $this->perpagerecord);

                      $nextproducts[] = array(
                          "title"     => $this->lang->line('clickviewmoreproduct'), 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('viewmore'),
                                  "payload"=>'Products_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextproducts
                                ]
                            ]];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    exit;  

                    else :

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;  
                    endif;
                }
              break;

              case 'pay_load_delivery':
                   if($messagetext == strtolower($this->lang->line('delivery_nextday')) || strtolower($this->lang->line('delivery_express')) || strtolower($this->lang->line('delivery_standard'))) {

                     $options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_pickupopt1'),
                              'payload' => "PAY_LOAD_PICKUP"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_pickupopt2'),
                              'payload' => "PAY_LOAD_PICKUP"
                          )
                      ); 

                      $quickoptions = json_encode($options);

                      $message_to_reply = $this->lang->line('delivery_pickup');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];

                      //$this->cache->save('delivery_method',$messagetext,300); // 2 minutes
                      $this->cache->save("delivery_method_{$sender}",$messagetext,300); // 2 minutes

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    
                   }
              break;

              case 'pay_load_pickup':
                    $deliverymethod = $this->cache->get("delivery_method_{$sender}");
                    $useaddress = $this->cache->get("deliver_address_{$sender}");

                    if($messagetext == strtolower($this->lang->line('delivery_pickupopt1'))){

                      $message_to_reply = $this->lang->line('pickup_soon');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_pickupopt1'),
                              'payload' => "PAY_LOAD_PICKUP"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_pickupopt2'),
                              'payload' => "PAY_LOAD_PICKUP"
                          )
                      ); 

                      $quickoptions = json_encode($options);

                      $message_to_reply = $this->lang->line('delivery_pickupchoose');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];

                       //01-02-2018 sachin 
                      if(!empty($useaddress)){
                        $this->cache->save("deliver_address_{$sender}",$useaddress,320);
                      }else{  
                        $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 2 minutes
                      }
                      //$this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 2 minutes
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
                    if($messagetext == strtolower($this->lang->line('delivery_pickupopt2'))){

                      if(!empty($useaddress)){
                        $options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_paycash'),
                              'payload' => "payload_payment_gateway"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_payprepaid'),
                              'payload' => "payload_payment_gateway"
                          )
                        ); 

                        $quickoptions = json_encode($options);
                        
                        $message_to_reply = $this->lang->line('howpay');

                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                        ];
                        $this->cache->save("deliver_address_{$sender}",$useaddress,300);
                      }
                      else{
                        $message_to_reply = $this->lang->line('delivery_postcode');

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                        ];

                        $this->cache->save("delivery_postcode_{$sender}",'delivery',300); // 2 minutes
                        $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 2 minutes
                      }
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
              break;

              case 'pay_load_time':

                    if($page_id == "1048681488539732"){ //SP
                      $token = "280068859069546|JcVKVo4ZvtXp34pyFV7Djrd70lA";
                    }
                    else{
                      $token = $this->access_token;
                    }

                    $times = $this->curl("https://graph.facebook.com/{$page_id}?fields=hours&access_token=".$token,'','GET');

                    //$times = $this->getPageLocationHours($page_id); // Get page details
                    
                    $text = '';

                    if(isset( $times->hours)){
                      
                      $message_to_reply = (array) $times->hours;
                      foreach ($message_to_reply as $key => $value) {
                          $text.= $key.':-';
                          $text.=$value;
                          $text.="\n";
                      }
                    }
                    else{
                      $text = $this->lang->line('sorry');
                    }

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $text]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                   
                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=> $welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('mostpopular'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('discover')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $responses = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responses,'POST');  
                exit;
              break;

              case 'pay_load_order':

                  $orderlists = orderlists_by_userid($sender);
                  if(count($orderlists > 0)){
                    $myArray = array();

                    $table = '';
                    $table.="\n# {$this->lang->line('shiporder')}\t\t{$this->lang->line('shipamt')}\t\t{$this->lang->line('shipstatus')}\n";
                    $table.="-------------------------------------------------------------";
                    $table.="\n";
                    $st = "";
                  foreach ($orderlists as $key => $value) {
                      $table.=$value->transaction_id."\t";
                      /*foreach ($value->products as $key1 => $value1){ 
                          $myArray[$key][$key1] = $value1->product_name;
                      }
                      $table.=implode(', ', $myArray[$key] );*/
                      if($value->status == 0){ // 0 -23-11-2017
                        $st = "{$this->lang->line('orderprocess')}";
                      }
                      if($value->status == 1){
                        $st = "{$this->lang->line('orderdispatch')}";
                      }
                      if($value->status == 2){
                        $st = "{$this->lang->line('ordercomplete')}";
                      }
                      /*if($value->status == 3){ -23-11-2017
                        $st = "{$this->lang->line('ordercan')}";
                      }
                      if($value->status == 99){ -23-11-2017
                        $st = "{$this->lang->line('orderfail')}";
                      }*/
                      $table.=$value->order_amount."\t\t";
                      $table.=$st."\n";
                  }
                    $table.="-------------------------------------------------------------";
                  
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $table]
                    ];
                  }
                  else{
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => "No order found."]
                    ];
                  }
        
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  exit;
              break;
              case 'pay_load_office':

                    if($page_id == "1048681488539732"){ //SP
                      $token = "280068859069546|JcVKVo4ZvtXp34pyFV7Djrd70lA";
                    }
                    else{
                      $token = $this->access_token;
                    }

                    $locations = $this->curl("https://graph.facebook.com/{$page_id}?fields=single_line_address&access_token=".$token,'','GET');

                    //$locations = $this->getPageLocationHours($page_id); // Get page location details
                    
                    if(isset($locations->single_line_address)){
                      $message_to_reply = $locations->single_line_address;
                    }
                    else{
                      $message_to_reply = $this->lang->line('noaddress');
                    }
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  
                    $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$this->lang->line('moreopt'),
                            "buttons"=>[
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('aboutpage'),
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('faq')
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('endchat')
                              ]
                            ]
                          ]
                        ]];

                     $responses = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $answer 
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responses,'POST');
                    exit;
              break;

              case 'pay_load_issue':

                    $message_to_reply = $this->lang->line('whatisit');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->cache->save("pagequestion_{$sender}",'usersearch');

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    exit;
              break;

              case 'flagtimer':

                  if($messagetext == strtolower($this->lang->line('no'))){

                    $optionslist = array();
                    $categories = categroy_lists($page_id);//$page_id

                    if(count($categories) > 0){
                      $optionslist[] = array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('other'),
                            'payload' => "PAY_LOAD_1" 
                          );
                      foreach ($categories as $value) {
                          if($this->language == 'english'){
                            $name = $value->category_name;
                          }
                          else{
                            $name = $value->category_name_thai;
                          }
                          $optionslist[] = array(
                            'content_type' => 'text',
                            'title' => ucwords($name),
                            'payload' => "category" 
                          );
                      }


                      $quickoptions = json_encode($optionslist);

                      $message_to_reply = $this->lang->line('looking');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                    }
                    else{
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $this->lang->line('nocat') 
                      ];
                   }
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                   exit;

                  }

              break;

              case 'payload_language_fbcheckout':

                  $title = $this->cache->get("product_title_{$sender}");
                  $splitsword = explode("/", $title);
                  $searchproducts = searchproduct_by_messenger($splitsword[0],$page_id);
                  
                  if(count($searchproducts) > 0){

                    $message_to_reply = "{$this->lang->line('mean')} {$title} ?";

                    $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('yes'),
                                'payload' => "payload_messegner"
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('no'),
                                'payload' => "payload_messegner"
                            )
                        ); 

                    $quickoptions = json_encode($options);

                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                          ];

                    $this->cache->save("product_title_{$sender}",$title,600); // 10 minutes save

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  }
                  else{
                    $this->cache->save("keysearch_{$sender}",'other',120); // 2 minutes
                    
                    $message_to_reply = $this->lang->line('noproductfound');
                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');     

                    $message_to_reply = $this->lang->line('wishtobuy');
                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];
                  
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }

              break;

              case 'payload_language':
                  
                    $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                              "template_type"=>"button",
                              "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                              "buttons"=>[
                                [
                                  "type"=>"postback",
                                  "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                  "title" => $this->lang->line('mostpopular'), //What's Hot
                                ],
                                [
                                  "type"=>"postback",
                                  "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                  "title" => $this->lang->line('discover')
                                ],
                                [
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('viewmore'),
                                  "payload"=>"PAY_LOAD_1"
                                ]
                              ]
                            ]
                          ]];

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                       ];
                      
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $total_rows_carts = $this->Shopping_carts_model->where(array('user_id' => $sender))->get_all();

                      if($total_rows_carts){

                        $options = array(
                            0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('yes'),
                              'payload' => "payload_checkout_cart"
                            ),
                            1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('no'),
                              'payload' => "payload_checkout_cart"
                            )
                        ); 

                        $quickoptions = json_encode($options);
                       // echo "hello";
                        //var_dump($this->lang->line('haveitem'));
                        //$message_to_reply = "Hey, You already have products in your cart. \n\nDo you want checkout ?";
                        $message_to_reply = $this->lang->line('haveitem');

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
              break;

              case 'delivery_district':
                  $postcode = $this->cache->get("validpostcode_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}");

                  $message_to_reply = $this->lang->line('choose_address');
                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                  ];
                  
                  $this->cache->delete("delivery_postcode_{$sender}"); 

                  $this->cache->save("validdistrict_{$sender}",$messagetext,300); // 2 minutes
                  $this->cache->save("validpostcode_{$sender}",$postcode,300); // 2 minutes
                  $this->cache->save("validaddress_{$sender}",'pickupaddress',300); // 2 minutes
                  $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 2 minutes

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
              break;

              case 'payload_payment_gateway':

                $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "payload_coupon"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "payload_coupon"
                        )
                    ); 

                $quickoptions = json_encode($options);
                $message_to_reply = $this->lang->line('havecode');

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                ];
                $this->cache->save("pay_method_{$sender}",$messagetext,300);
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              
              break;

              //Comment as feedback tayo 07-02-2018

              /*case 'payload_payment_gateway':
                $useraddress = $this->cache->get("deliver_address_{$sender}");
                $data = productlists_by_userid($sender);
                $table = '';
                $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                $table.="----------------------------------";
                $table.="\n";
                $sum = 0;
                $shipping = 0;
                $total = 0;
                foreach ($data as $key => $value) {

                    if($this->language == 'english'){
                      $name = $value->product_name;
                    }
                    else{
                      $name = $value->product_name_thai;
                    }
                    if(strlen($name) > 6){
                      $table.= wordwrap($name, 6, "\n", true);
                      $table.="\t";
                    }else{
                      $table.=$name."\t";
                      //$table.="\t\t\t";
                    }
                    if($value->sale_price > 0){
                      $price = $value->sale_price;
                    }else{
                      $price = $value->product_price;
                    }

                    $table.="\t".$value->qty."\t";
                    $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                    $sum = $sum + ($value->qty * $price);
                    $shipping = $shipping + $value->shipping_cost;
                    $table.="----------------------------------";
                    $table.="\n";

                }

                $table.="----------------------------------";
                $table.="\t\n";
                $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                $table.="\t\n";
                $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                $table.="\t\n";
                $total = $sum + $shipping;
                $table.="{$this->lang->line('gt')} = ".'฿'.$total;


                if($messagetext == strtolower($this->lang->line('delivery_paycash'))){ 
                  if(!empty($useraddress)){
                    $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                  }
                  $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                          "template_type"=>"button",
                          "text"=>$table,
                          "buttons"=>[
                            [
                              "type"=>"postback",
                              "title"=>$this->lang->line('paybtn'),
                              "payload"=>"Payment_Checkout_Cash"
                            ]
                          ]
                        ]
                      ]];
                }
                if($messagetext == strtolower($this->lang->line('delivery_payprepaid'))){

                $district = $this->cache->get("validdistrict_{$sender}");
                $postcode = $this->cache->get("validpostcode_{$sender}");
                $addressdata = $this->cache->get("valid_getaddress_{$sender}");
                $deliverymethod = $this->cache->get("delivery_method_{$sender}"); 
                if($this->language == 'english'){
                  $shipcountry = "Thailand";
                }
                else{
                  $shipcountry = "ประเทศไทย";
                }
                //$shipcountry = $this->cache->get('delivery_getcountry'); 
                $shipphone = $this->cache->get("delivery_getphone_{$sender}"); 
                $shipemail = $this->cache->get("delivery_getemail_{$sender}");  

                if(!empty($useraddress)){
                  $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                }
                else{
                $info = array(
                    'user_id'   => $sender,
                    'postcode'  => $postcode,
                    'district'  => $district,
                    'country'   => $shipcountry,
                    'phone'     => $shipphone,
                    'email'     => $shipemail,
                    'delivery_method'  =>$deliverymethod,
                    'shipping_address' => $addressdata
                  );
                  $addressID = $this->Address_model->insert($info);
                }

                $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                          "template_type"=>"button",
                          "text"=>$table,
                          "buttons"=>[
                            [
                                "type"=>"web_url",
                                "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                                "title"=>"Checkout",
                                "webview_height_ratio"=>"full",
                                "messenger_extensions"=> true,
                                "fallback_url"=> base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}"
                            ]
                          ]
                        ]
                      ]];
                }

                $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer
                ];

                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "payload_coupon"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "payload_coupon"
                        )
                    ); 

                $quickoptions = json_encode($options);
                $message_to_reply = $this->lang->line('havecode');

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                ];
                $this->cache->save("pay_method_{$sender}",$messagetext,300);
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

              break;*/

              default:
                if(in_array($messagetext, array('hi','akin','hey','hello','สวัสดี','คล้ายกัน','เฮ้','สวัสดี'))) {
                  //die("Sdfsfdsf");
                   $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => "English",
                            'payload' => "payload_language"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => "Thai",
                            'payload' => "payload_language"
                        )
                    ); 

                  $quickoptions = json_encode($options);
                  $message_to_reply = 'Please choose language.';

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  /*$answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$chatoptionsdata[0]->welcome_msg ? $chatoptionsdata[0]->welcome_msg : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => "Most Popular", //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => "Discover Products"
                          ],
                          [
                            "type"=>"postback",
                            "title"=>"View More",
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');*/ 
                //exit;
                /*$total_rows_carts = $this->Shopping_carts_model->where(array('user_id' => $sender))->get_all();

                if($total_rows_carts){

                  $options = array(
                      0 => array(
                        'content_type' => 'text',
                        'title' => "Yes",
                        'payload' => "payload_checkout_cart"
                      ),
                      1 => array(
                        'content_type' => 'text',
                        'title' => "No",
                        'payload' => "payload_checkout_cart"
                      )
                  ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply = "Hey, You already have products in your cart. \n\nDo you want checkout ?";

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');*/
              }

                //$checkTime = strtotime(date('Y-m-d H:i:s'));
              
                //$this->cache->save('timeinterval',$checkTime);

                //echo 'crontab -r';

               //echo 'crontab -e * * * * * /home/user/public_html/pointslive/admin/sendWaitTimeResponse';
                //echo 'crontab -e * * * * * C:\xampp\htdocs\pointslive\admin\sendWaitTimeResponse';

            // feedback tayo 07-02-2018
            else if($messagetext == strtolower($this->lang->line('yes')) || $messagetext == strtolower($this->lang->line('continueshopping'))) {

                $this->db->where('user_id',$sender);
                $this->db->delete('trigger_data');

                $optionslist = array();
                $categories = categroy_lists($page_id);//$page_id

                if(count($categories) > 0){
                  /*$optionslist[] = array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('other'),
                        'payload' => "PAY_LOAD_1" 
                      );*/
                  $optionslist[] = array(
                  "title"     => $this->lang->line('other'), 
                  "image_url" => base_url()."uploads/searchimg.png",
                  "buttons"   => array(
                          array(
                            "type"=>"postback",
                            "title"=>$this->lang->line('other'),
                            "payload"=>"PAY_LOAD_1"
                          )
                      )  
                  );
                  foreach ($categories as $value) {
                      if($this->language == 'english'){
                        $name = $value->category_name;
                      }
                      else{
                        $name = $value->category_name_thai;
                      }
                      /*$optionslist[] = array(
                        'content_type' => 'text',
                        'title' => ucwords($name),
                        'payload' => "category" 
                      );*/
                      $optionslist[] = array(
                      "title"     => $name, 
                      "image_url" => base_url()."uploads/categories/".$value->category_image,
                      "buttons"   => array(
                              array(
                                "type"=>"postback",
                                "title"=>ucwords($name),
                                "payload"=>"PAY_LOAD_CATEGORY"
                              )    
                          )  
                      );
                  }

                  $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"generic",
                        "elements"=>$optionslist
                      ]
                  ]];

                  /*$quickoptions = json_encode($optionslist);

                  $message_to_reply = $this->lang->line('looking');*/

                  $message_to_reply = $this->lang->line('looking');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                  ];
                  
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer //,[ 'text' => $message_to_reply]'quick_replies' => $quickoptions
                  ];
                 // file_put_contents('filename.txt', print_r($response, true));
                }
                else{
                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $this->lang->line('nocat') 
                  ];
               }
               $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
               exit;  
            }
            else if($messagetext == strtolower($this->lang->line('no'))){

                  $this->db->where('user_id',$sender);
                  $this->db->delete('trigger_data');  

                  $message_to_reply = $dismiss_msg ? $dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                   ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');   
                exit;
            }
            else
            {   
                  if(!empty($this->cache->get("keysearch_{$sender}"))){
                    $datasearch = array(
                        'search_by_userid' => $sender,
                        'search_keyword'  => $messagetext,
                        'page_id'         => $page_id 
                      );

                    if($this->saveSearchKeyword($datasearch)){
                        // Find products / category products
                        $searchproducts = searchproduct_by_keyword($messagetext);
                        $products = array();

                        if(count($searchproducts) > 0) :
                          foreach ($searchproducts as $key => $product) {
                               if($this->language == 'english'){
                                  $name = $product->product_name;
                                  $description = $product->product_description;
                                }else{
                                  $name = $product->product_name_thai;
                                  $description = $product->product_description_thai;
                                }

                                if($product->sale_price > 0){
                                  $price = $product->sale_price;
                                }else{
                                  $price = $product->product_price;
                                }

                              $products[] = array(
                              "title"     => $name, 
                              "image_url" => base_url()."uploads/products/{$product->product_images[0]}",
                              "subtitle"  => '฿'.$price .' - '.$description,
                              "buttons"   => array(
                                       /*array(
                                        "type"=>"payment",
                                        "title"=>"buy",
                                        "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                        "payment_summary"=>[
                                          "currency"=>"USD",
                                          "payment_type"=>"FIXED_AMOUNT",
                                          "is_test_payment" => true, 
                                          "merchant_name"=>"Points Live",
                                          "requested_user_info"=>["shipping_address","contact_name", "contact_email"],
                                          "price_list"=>[
                                            [
                                              "label"=>"Subtotal",
                                              "amount"=>$product->product_price
                                            ],
                                            [
                                              "label"=>"Shipping",
                                              "amount"=>$product->shipping_cost
                                            ]
                                          ]
                                        ]
                                      ),*/  
                                     /* array(
                                        "type"=>"web_url",
                                        "url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}",
                                        "title"=>$this->lang->line('checkout'),
                                        "webview_height_ratio"=>"full",
                                        "messenger_extensions"=> true,
                                        "fallback_url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}" 
                                      ),*/
                                      array(
                                        "type"=>"postback",
                                        "title"=>$this->lang->line('checkout'),
                                        "payload"=>"$product->product_id"
                                      ),
                                      array(
                                        "type"=>"postback",
                                        "title"=>$this->lang->line('addtocart'),
                                        "payload"=>"$product->product_id"
                                      )
                                  )  
                              );
                          }

                          $answer = ["attachment"=>[
                              "type"=>"template",
                              "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=> $products
                                  ]
                                ]
                              ];

                          $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                          ];

                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                        else :

                          $message_to_reply = $this->lang->line('noproductfound');
                          $answer = ['text' => $message_to_reply];

                          $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => $answer 
                          ];
                        
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                          $responseno = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $feedback_msg ? $feedback_msg : 'Thanks for feedback. We will let Store Manager know']
                          ];

                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseno,'POST');

                        endif;
                        //feedback tayo 07-02-2018
                        /*$options = array(
                                    0 => array(
                                        'content_type' => 'text',
                                        'title' => $this->lang->line('yes'),
                                        'payload' => "flag"
                                    ),
                                    1 => array(
                                        'content_type' => 'text',
                                        'title' => $this->lang->line('no'),
                                        'payload' => "flag"
                                    )
                                ); 

                         $quickoptions = json_encode($options);

                         $message_to_reply = $findin_page ? $findin_page : $this->lang->line('still');

                         $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                         ];*/
                        // Remove cache
                        $this->cache->delete("keysearch_{$sender}");

                        $this->db->where('user_id',$sender);
                        $this->db->delete('trigger_data');

                        //$this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

                        /*$checkTime = strtotime(date('H:i:s'));
                        $this->cache->save('timeinterval',$checkTime,120); */
                        exit;
                      }
                  }
                  else if(!empty($this->cache->get("couponcode_{$sender}"))){

                    $couponcode = $messagetext;
                    $methodpay = $this->cache->get("pay_method_{$sender}");
                    $district = $this->cache->get("validdistrict_{$sender}");
                    $postcode = $this->cache->get("validpostcode_{$sender}");
                    $addressdata = $this->cache->get("valid_getaddress_{$sender}");
                    $deliverymethod = $this->cache->get("delivery_method_{$sender}"); 
                    $shipername = $this->cache->get("shipername_{$sender}");
                    if($this->language == 'english'){
                      $shipcountry = "Thailand";
                    }
                    else{
                      $shipcountry = "ประเทศไทย";
                    } 
                    $shipphone = $this->cache->get("delivery_getphone_{$sender}"); 
                    $shipemail = $this->cache->get("delivery_getemail_{$sender}"); 
                    $useraddress = $this->cache->get("deliver_address_{$sender}");
                    $flagvalid = 1;

                    $data = productlists_by_userid($sender);
                    $table = '';
                    $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                    $table.="----------------------------------";
                    $table.="\n";
                    $sum = 0;
                    $shipping = 0;
                    $grandtotal = 0;
                    $finalamount = 0;
                    foreach ($data as $key => $value) {

                        if($this->language == 'english'){
                          $name = $value->product_name;
                        }
                        else{
                          $name = $value->product_name_thai;
                        }
                        if(strlen($name) > 6){
                          $table.= wordwrap($name, 6, "\n", true);
                          $table.="\t";
                        }else{
                          $table.=$name."\t";
                          //$table.="\t\t\t";
                        }
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }

                        $table.="\t".$value->qty."\t";
                        $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                        $sum = $sum + ($value->qty * $price);
                        $shipping = $shipping + $value->shipping_cost;
                        $table.="----------------------------------";
                        $table.="\n";

                    }

                    $table.="----------------------------------";
                    $table.="\t\n";
                    $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                    $table.="\t\n";
                    $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                    $table.="\t\n";
                    $grandtotal = $sum + $shipping;

                    // check code valid or not and also check status
                    $validcode = $this->Coupons_model->isValidCoupon($couponcode);
                    //var_dump($validcode);
                    if($validcode){ 

                          $isvaliddate = checkDateBetween(date('Y-m-d'),$validcode->from_date,$validcode->to_date); // check expiry date

                          if($isvaliddate){
                            // check user uses of coupon count  
                            $usedcount = $this->Coupons_model->usedcountCoupon_By_User($validcode->id,$sender);
                            if(count($usedcount) >= 0){ //count($usedcount) >= 0
                               // if user uses more than 0 or entered limit
                               if($usedcount->total_used == $validcode->per_customer_uses){
                                  $message_to_reply = $this->lang->line('codeused');
                                  /*$response = [
                                      'recipient' => [ 'id' => $sender ],
                                      'message' => [ 'text' => $message_to_reply]
                                  ];*/
                                  $flagvalid = 0;
                                  $this->cache->save("couponcode_{$sender}",'coupon_offer',300);
                               }
                               else{
                                  // check cart amount with mimnimum amount of coupon
                                  if($grandtotal >= $validcode->minimum_amount){
                                    if($validcode->by_apply == '%'){
                                      $discountamount = ($grandtotal * $validcode->discount_value) / 100;
                                      $finalamount = $grandtotal - $discountamount;
                                    }
                                    else if($validcode->by_apply == "fixed"){
                                      $discountamount = $validcode->discount_value;
                                      $finalamount = $grandtotal - $discountamount;
                                    }

                                    $redeemdata = array(
                                      'coupon_id' => $validcode->id,
                                      'total_discount'  => $discountamount,
                                      'redemption_date' => date('Y-m-d'),
                                      'user_id'   => $sender
                                    );

                                    $table.="Discount amount (-) = ".'฿'.$discountamount;
                                    $table.="\t\n";
                                    $table.="Grand Total = ".'฿'.$finalamount;
                                    // Saved redeem coupon data
                                    $redeemsaved = $this->db->insert('coupon_redemptions',$redeemdata);

                                    if($redeemsaved){
                                      $lastID = $this->db->insert_id();
                                      $message_to_reply = $this->lang->line('codesuccess');
                                      $response = [
                                          'recipient' => [ 'id' => $sender ],
                                          'message' => [ 'text' => $message_to_reply]
                                      ];
                                      $flagvalid = 1;
                                      $this->cache->save("lastcouponID_{$sender}",$lastID,300);
                                    }
                                  }
                                  else{
                                    $message_to_reply = $this->lang->line('codeminimumamount').' '.$validcode->minimum_amount;
                                   /* $response = [
                                        'recipient' => [ 'id' => $sender ],
                                        'message' => [ 'text' => $message_to_reply]
                                    ];*/
                                    $flagvalid = 0;
                                    $this->cache->save("couponcode_{$sender}",'coupon_offer',300);
                                  }
                               }
                            }
                          }
                          else{
                            $message_to_reply = $this->lang->line('codeexpired');
                            /*$response = [
                                'recipient' => [ 'id' => $sender ],
                                'message' => [ 'text' => $message_to_reply]
                            ];*/
                            $flagvalid = 0;
                            $this->cache->save("couponcode_{$sender}",'coupon_offer',300);
                        }
                    }
                    else{
                      $message_to_reply = $this->lang->line('codenotvalid');
                      /*$response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];*/
                      $flagvalid = 0;
                      $this->cache->save("couponcode_{$sender}",'coupon_offer',300);
                    }

                    if($flagvalid == 0){ // MV

                      $options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('checkout'),
                              'payload' => "payload_proceedcheckout"
                          )
                      ); 

                      $quickoptions = json_encode($options);
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                    }
                    else{
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      if($methodpay == 'cash' || $methodpay == 'เงินสด'){
                        $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                              "template_type"=>"button",
                              "text"=>$table,
                              "buttons"=>[
                                [
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('paybtn'),
                                  "payload"=>"Payment_Checkout_Cash"
                                ]
                              ]
                            ]
                          ]
                        ];        
                      }
                      else if($methodpay == "prepaid" || $methodpay == "เติมเงิน"){
                        $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$table,
                            "buttons"=>[
                              [
                                  "type"=>"web_url",
                                  "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                                  "title"=>"Checkout",
                                  "webview_height_ratio"=>"full",
                                  "messenger_extensions"=> true,
                                  "fallback_url"=> base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}"
                              ]
                            ]
                          ]
                        ]];  
                      } 
                      $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer
                      ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    // Remove coupon code if applied - continue
                    $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "payload_removecoupon"
                        )
                    ); 

                    $quickoptions = json_encode($options);
                    $message_to_reply = $this->lang->line('removecode');
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                    ];
                    // END

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    if(!empty($useraddress)){
                        $this->cache->save("deliver_address_{$sender}",$useraddress,300);
                      }
                    else{
                      $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                      $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                      $this->cache->save("valid_getaddress_{$sender}",$addressdata,300); // 3 minutes
                      $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                      //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                      $this->cache->save("delivery_getphone_{$sender}",$shipphone,300); // 3 minutes
                      $this->cache->save("delivery_getemail_{$sender}",$shipemail,300); // 3 minutes
                      $this->cache->save("shipername_{$sender}",$shipername,300);  
                    }
                    $this->cache->save("discountamount_{$sender}",$discountamount,300);
                    $this->cache->save("pay_method_{$sender}",$methodpay,300);
                    //$this->cache->delete("pay_method_{$sender}");
                    $this->cache->delete("couponcode_{$sender}");
                    } 
                  }
                  else if(!empty($this->cache->get("pagequestion_{$sender}"))){
                      $datasearch = array(
                        'search_by_userid' => $sender,
                        'search_keyword'  => $messagetext,
                        'page_id'         => $page_id 
                      );

                      if($this->saveSearchKeyword($datasearch)){
                       $message_to_reply = $this->lang->line('pm');
                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                        ];
                      }
                      $this->cache->delete("pagequestion_{$sender}");
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                      exit;
                  }
                // feedback tayo 07-02-2018  
                else if(!empty($this->cache->get("updateproduct_{$sender}"))) {
                     $flag = 0;
                     $productid = $this->cache->get("updateproduct_{$sender}");
                     $checkqty = $this->Products_model->get(array('product_id' => $productid)); // check quantity
                    
                     if(is_numeric($messagetext)) {
                        if($messagetext > 0){
                          if($messagetext > $checkqty->total_qty){
                            $message_to_reply = $this->lang->line('qtyoutstock');
                            $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply]
                            ];
                            $this->cache->save("updateproduct_{$sender}",$productid,300); // 5 minutes
                            $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                          }
                          else{
                            $number = round($messagetext);
                            $products = $this->Shopping_carts_model->where(array('user_id' => "{$sender}",'product_id' => $productid))->get();
                            if($products){
                              $update_data = array('qty'=>$messagetext);

                              $updated = $this->Shopping_carts_model->where(array('user_id' => "{$sender}",'product_id' => $productid))->update($update_data);
                              if($updated){
                                /****** 14-11-2017 **********/
                                $availqty = $checkqty->total_qty - $messagetext;
                                $updated_data = array('total_qty' => $availqty);
                                  // update quantity
                                $this->Products_model->where('product_id',$productid)->update($updated_data);
                                /************* end ************/
                                $flag = 1;
                              }
                            }
                            if($flag == 1){
                              $message_to_reply = "{$this->lang->line('qtyupdated')} {$number}";
                              $response = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply]
                                ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                              $this->cache->delete("updateproduct_{$sender}"); // Delete product id  

                              $options = array(
                                  /*0 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('yes'),
                                      'payload' => "PAY_LOAD_1"
                                  ),*/
                                  0 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('continueshopping'),
                                      'payload' => "flag"
                                  ),
                                  1 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('updatecart'),
                                      'payload' => "PAY_LOAD_1"
                                  ),
                                  2 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('checkout'),
                                      'payload' => "PAY_LOAD_1"
                                  )
                              ); 

                             $quickoptions = json_encode($options);

                             $message_to_reply = $this->lang->line('addbasket');

                             $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                             ];
                             $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                             exit;
                            }
                          }
                        } 
                        else{
                          $message_to_reply = $this->lang->line('qtygreaterthanzero');

                          $response = [
                                'recipient' => [ 'id' => $sender ],
                                'message' => [ 'text' => $message_to_reply]
                          ];
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                          exit;
                        }
                      }
                     else{
                      $message_to_reply = $this->lang->line('qtynum');
                      $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                      $this->cache->save("updateproduct_{$sender}",$productid,300);
                      exit;
                     }   
                }
                else if(isset($getfbmessage)){
                  $title = $input['entry'][0]['messaging'][0]['message']['attachments'][0]['title'];

                  $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => "English / อังกฤษ",
                            'payload' => "payload_language_fbcheckout"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => "Thai / ไทย",
                            'payload' => "payload_language_fbcheckout"
                        )
                    ); 

                  $quickoptions = json_encode($options);
                  $message_to_reply = 'Please choose language.';

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->cache->save("product_title_{$sender}",$title,600); // 10 minutes save
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  //echo metaphone($title);
                  /*$splitsword = explode("/", $title);
                  $searchproducts = searchproduct_by_messenger($splitsword[0]);
                  
                  if(count($searchproducts) > 0){

                    $message_to_reply = "{$this->lang->line('mean')} {$title} ?";

                    $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('yes'),
                                'payload' => "payload_messegner"
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('no'),
                                'payload' => "payload_messegner"
                            )
                        ); 

                    $quickoptions = json_encode($options);

                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                          ];

                    $this->cache->save('product_title',$title,600); // 10 minutes save

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  }
                  else{
                    $this->cache->save('keysearch','other',120); // 2 minutes
                    
                    $message_to_reply = $this->lang->line('noproductfound');
                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');     

                    $message_to_reply = $this->lang->line('wishtobuy');
                    $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];
                  
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }*/

                }
                else if(!empty($this->cache->get("product_id_{$sender}"))){
                  // echo "S=". $messagetext;
                   if(is_numeric($messagetext)){
                      if($messagetext > 0){

                        $checkqty = $this->Products_model->get(array('product_id' => $this->cache->get("product_id_{$sender}")));

                        if($messagetext > $checkqty->total_qty){
                          $message_to_reply = $this->lang->line('qtynotavail').' '.$this->cache->get("product_title_{$sender}");

                          $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply]
                          ];
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                        }
                        else{
                          $options = array(
                              0 => array(
                                  'content_type' => 'text',
                                  'title' => $this->lang->line('yes'),
                                  'payload' => "payload_cart_check"
                              ),
                              1 => array(
                                  'content_type' => 'text',
                                  'title' => $this->lang->line('no'),
                                  'payload' => "payload_cart_check"
                              )
                          ); 

                          $quickoptions = json_encode($options);
                          
                          $message_to_reply = $this->lang->line('wanttoaddcart');

                          $response = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                                ];

                          $this->cache->save("product_id_{$sender}",$this->cache->get("product_id_{$sender}"),600); // 10 minutes save
                          $this->cache->save("userqty_{$sender}",$messagetext,600); // 10 minutes save 
                          $this->cache->save("product_title_{$sender}",$this->cache->get("product_title_{$sender}"),600); // 10 minutes save
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                        }
                      }
                      else{
                        $message_to_reply = $this->lang->line('qtygreaterthanzero');
                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                        ];
                        $this->cache->save("product_id_{$sender}",$this->cache->get("product_id_{$sender}"),600);
                        $this->cache->save("product_title_{$sender}",$this->cache->get("product_title_{$sender}"),600); // 10 minutes save
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                      }
                   } 
                   else{
                      $message_to_reply = $this->lang->line('qtynum');
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                      $this->cache->save("product_id_{$sender}",$this->cache->get("product_id_{$sender}"),600);
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                   }
                }
                // sales flow updated
                else if(!empty($this->cache->get("delivery_postcode_{$sender}"))){
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}");
                  $postcode = (int)$messagetext;
                  $checkvalid = checkPostalcode($postcode);
                  //print_r($checkvalid);exit;
                  if(count($checkvalid) > 0){ //echo '<pre>';
                   
                    foreach ($checkvalid as $key => $code) { 
                        if($this->language == 'english'){
                          $districtname = $code->district;
                        }
                        else{
                          $districtname = $code->district_thai;
                        }

                        $optionslist[] = array(
                          'content_type' => 'text',
                          'title' => $districtname,
                          'payload' => "delivery_district" 
                        );
                    }
                 
                    $message_to_reply = $this->lang->line('choose_district');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $optionslist]
                    ];

                    $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                    $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  }
                  else{
                    $message_to_reply = $this->lang->line('valid_postcode');
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->cache->save("delivery_postcode_{$sender}",'delivery',300); // 2 minutes
                    $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 2 minutes
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  }
                }
                else if(!empty($this->cache->get("validaddress_{$sender}"))){

                  $address = $messagetext;
                  $district = $this->cache->get("validdistrict_{$sender}"); // get district
                  $postcode = $this->cache->get("validpostcode_{$sender}"); // get postcode
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); // 2 minutes

                  $message_to_reply = $this->lang->line('enter_shipername');//$this->lang->line('enter_phone'); // enter_country 16-01-2018

                  $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply]
                  ];

                  $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                  $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                  $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                  $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                  //$this->cache->save('delivery_country',"validcountry",300); // 2 minutes 16-01-2018
                  $this->cache->save("delivery_shiper_{$sender}","recivername",300); // 2 minutes
                  //$this->cache->save("delivery_phone_{$sender}","phonenumber",300); // 2 minutes

                  $this->cache->delete("validaddress_{$sender}");

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                }
                else if(!empty($this->cache->get("delivery_shiper_{$sender}"))){
                    //$country = $this->cache->get('delivery_getcountry'); // get country
                    $address = $this->cache->get("valid_getaddress_{$sender}"); // get address
                    $district = $this->cache->get("validdistrict_{$sender}"); // get district
                    $postcode = $this->cache->get("validpostcode_{$sender}"); // get postcode
                    $deliverymethod = $this->cache->get("delivery_method_{$sender}"); // method

                    $message_to_reply = $this->lang->line('enter_phone');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                    $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                    $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                    $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                    //$this->cache->save('delivery_country',"validcountry",300); // 2 minutes 16-01-2018
                    $this->cache->save("shipername_{$sender}",$messagetext,300); // 2 minutes
                    $this->cache->save("delivery_phone_{$sender}","phonenumber",300); // 2 minutes
                    $this->cache->delete("delivery_shiper_{$sender}");

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else if(!empty($this->cache->get("delivery_phone_{$sender}"))){

                  $phoneno = $messagetext;
                  //$country = $this->cache->get('delivery_getcountry'); // get country
                  $address = $this->cache->get("valid_getaddress_{$sender}"); // get address
                  $district = $this->cache->get("validdistrict_{$sender}"); // get district
                  $postcode = $this->cache->get("validpostcode_{$sender}"); // get postcode
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); // method
                  $shipername = $this->cache->get("shipername_{$sender}");
                  $number_lngth = strlen($phoneno);
                 // if(is_numeric($phoneno)){

                  if(preg_match("~^0\d+$~", $phoneno) && $number_lngth==10){ // [0-9]{10}

                    $message_to_reply = $this->lang->line('enter_email');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                    $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                    $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                    $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                   // $this->cache->save("delivery_getcountry_{$sender}",$country,300); // 3 minutes
                    $this->cache->save("delivery_getphone_{$sender}",$phoneno,300); // 3 minutes
                    $this->cache->save("shipername_{$sender}",$shipername,300);
                    $this->cache->save("delivery_custemail_{$sender}","customeremail",300); // 2 minutes
                    $this->cache->delete("delivery_phone_{$sender}");
                  }
                  else{

                    $message_to_reply = $this->lang->line('errphonenum');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                    ]; 
                    $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                    $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                    $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                    $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                    $this->cache->save("shipername_{$sender}",$shipername,300);
                    //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                    //$this->cache->save('delivery_getphone',$phoneno,300); // 3 minutes
                    $this->cache->save("delivery_phone_{$sender}",'phonenumber',300); // 2 minutes
                  }
                  /*}
                  else{

                    $message_to_reply = $this->lang->line('errphone');

                      $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                      ];
                      $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                      $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                      $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                      $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                      //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                        //$this->cache->save('delivery_getphone',$phoneno,300); // 3 minutes
                     $this->cache->save("delivery_phone_{$sender}",'phonenumber',300); // 2 minutes
                  }*/

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else if(!empty($this->cache->get("delivery_custemail_{$sender}"))){

                  $customeremail = $messagetext;  
                  $phoneno = $this->cache->get("delivery_getphone_{$sender}");
                  //$country = $this->cache->get('delivery_getcountry'); // get country
                  $address = $this->cache->get("valid_getaddress_{$sender}"); // get address
                  $district = $this->cache->get("validdistrict_{$sender}"); // get district
                  $postcode = $this->cache->get("validpostcode_{$sender}"); // get postcode
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); // method
                  $shipername = $this->cache->get("shipername_{$sender}");

                  if (filter_var($customeremail, FILTER_VALIDATE_EMAIL)) {
                     $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_paycash'),
                            'payload' => "payload_payment_gateway"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_payprepaid'),
                            'payload' => "payload_payment_gateway"
                        )
                      ); 

                      $quickoptions = json_encode($options);
                      
                      $message_to_reply = $this->lang->line('howpay');

                      $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                      $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                      $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                      $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                      $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                      //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                      $this->cache->save("delivery_getphone_{$sender}",$phoneno,300); // 3 minutes
                      $this->cache->save("delivery_getemail_{$sender}",$customeremail,300); // 3 minutes
                      $this->cache->save("shipername_{$sender}",$shipername,300);
                      $this->cache->delete("delivery_custemail_{$sender}");
                  }
                  else{
                    $message_to_reply = $this->lang->line('erremail');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->cache->save("validdistrict_{$sender}",$district,300); // 3 minutes
                    $this->cache->save("validpostcode_{$sender}",$postcode,300); // 3 minutes
                    $this->cache->save("valid_getaddress_{$sender}",$address,300); // 3 minutes
                    $this->cache->save("delivery_method_{$sender}",$deliverymethod,300); // 3 minutes
                    //$this->cache->save('delivery_getcountry',$country,300); // 3 minutes
                    $this->cache->save("delivery_getphone_{$sender}",$phoneno,300); // 3 minutes
                    $this->cache->save("delivery_custemail_{$sender}",'customeremail',300); // 2 minutes
                    $this->cache->save("shipername_{$sender}",$shipername,300);
                  }
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else{
                  $message_to_reply = $this->lang->line('endmsg');
                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                  ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  exit;
                }
              }
             break;
          }
          //$this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
        }
        else 
        {   
            switch($postback) {
            case 'DEVELOEPR_END':
               $message_to_reply = $dismiss_msg ? $dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

               $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply]
               ];
              $this->cache->delete("product_id_{$sender}"); 
              $this->cache->delete("userqty_{$sender}");
              $this->cache->delete("product_title_{$sender}");
              $this->cache->delete("keysearch_{$sender}");  
              $this->cache->delete("delivery_postcode_{$sender}");
              $this->cache->delete("validdistrict_{$sender}");
              $this->cache->delete("validpostcode_{$sender}");
              $this->cache->delete("valid_getaddress_{$sender}");
              $this->cache->delete("delivery_method_{$sender}"); 
              //$this->cache->delete('delivery_getcountry'); 
              $this->cache->delete("delivery_getphone_{$sender}"); 
              $this->cache->delete("delivery_getemail_{$sender}"); 
              $this->cache->delete("delivery_custemail_{$sender}");
              $this->cache->delete("deliver_address_{$sender}");
              $this->cache->delete("discountamount_{$sender}");
              $this->cache->delete("pay_method_{$sender}");
              $this->cache->delete("couponcode_{$sender}");
              $this->cache->delete("lastcouponID_{$sender}");
              $this->cache->delete("shipername_{$sender}");
              //$this->session->unset_userdata('shipping_address');
              //$this->session->unset_userdata('redeemID');
              //$this->cache->delete('keysearch');   
              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
              exit;
            break;  
            case 'DEVELOEPR_LANGUAGE':  

                $this->db->from('chat_languages');
                $this->db->where('user_id',$sender);  
                $set = $this->db->get()->result();
                if(count($set) == 0){
                    $this->db->insert('chat_languages',array('language' => $posttitletext,'user_id' => $sender));
                }
                else{
                    $this->db->where("id",$set[0]->id);
                    $this->db->update('chat_languages',array('language' => $posttitletext)); 
                } 
                          
               $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => ['text' => "You choose {$posttitletext}."]
               ];

              $this->cache->delete("keysearch_{$sender}");
              $this->cache->delete("delivery_postcode_{$sender}");
              $this->cache->delete("validdistrict_{$sender}");
              $this->cache->delete("validpostcode_{$sender}");
              $this->cache->delete("valid_getaddress_{$sender}");
              $this->cache->delete("delivery_method_{$sender}"); 
              //$this->cache->delete('delivery_getcountry'); 
              $this->cache->delete("delivery_getphone_{$sender}"); 
              $this->cache->delete("delivery_getemail_{$sender}");
              $this->cache->delete("delivery_custemail_{$sender}");
              $this->cache->delete("deliver_address_{$sender}");
              $this->cache->delete("discountamount_{$sender}");
              $this->cache->delete("pay_method_{$sender}");
              $this->cache->delete("couponcode_{$sender}"); 
              $this->cache->delete("lastcouponID_{$sender}");
              $this->cache->delete("shipername_{$sender}");
              //$this->session->unset_userdata('shipping_address');
              //$this->session->unset_userdata('redeemID');

              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
            break;    

            case 'DEVELOEPR_SHOPPINGCART':

                $data = productlists_by_userid($sender);
                if(count($data) > 0) :

                    $table = '';
                    $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                    $table.="----------------------------------";
                    $table.="\n";
                    $sum = 0;
                    $shipping = 0;
                    $total = 0;
                    $price = 0;
                    foreach ($data as $key => $value) {
                        if($this->language == 'english'){
                          $name = $value->product_name;
                        }
                        else{
                          $name = $value->product_name_thai;
                        }

                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }

                        if(strlen($name) > 6){
                          $table.= wordwrap($name, 6, "\n", true);
                          $table.="\t";
                        }else{
                          $table.=$name."\t";
                          //$table.="\t\t\t";
                        }


                        $table.="\t".$value->qty."\t";
                        $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                        $sum = $sum + ($value->qty * $price);
                        $shipping = $shipping + $value->shipping_cost;
                        $table.="----------------------------------";
                        $table.="\n";
                    }
                    $table.="----------------------------------";
                    $table.="\t\n";
                    $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                    $table.="\t\n";
                    $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                    $table.="\t\n";
                    $total = $sum + $shipping;
                    $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                    $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                          "template_type"=>"button",
                          "text"=>$table,
                          /*"buttons"=>[
                            [
                              "type"=>"web_url",
                              "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                              "title"=>$this->lang->line('checkout'),
                              "webview_height_ratio"=>"full",
                              "messenger_extensions"=> true,
                              "fallback_url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}" 
                              //base_url()."admin/productlistsByUserid/{$sender} 
                            ]
                          ]*/
                        ]
                      ]];
                    
                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $table]
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    $this->cache->delete("product_id_{$sender}"); 
                    $this->cache->delete("userqty_{$sender}");
                    $this->cache->delete("product_title_{$sender}");
                    $this->cache->delete("keysearch_{$sender}");  
                    $this->cache->delete("delivery_postcode_{$sender}");
                    $this->cache->delete("validdistrict_{$sender}");
                    $this->cache->delete("validpostcode_{$sender}");
                    $this->cache->delete("valid_getaddress_{$sender}");
                    $this->cache->delete("delivery_method_{$sender}"); 
                    //$this->cache->delete('delivery_getcountry'); 
                    $this->cache->delete("delivery_getphone_{$sender}"); 
                    $this->cache->delete("delivery_getemail_{$sender}");
                    $this->cache->delete("delivery_custemail_{$sender}");
                    $this->cache->delete("deliver_address_{$sender}");
                    $this->cache->delete("discountamount_{$sender}");
                    $this->cache->delete("pay_method_{$sender}");
                    $this->cache->delete("couponcode_{$sender}");   
                    $this->cache->delete("lastcouponID_{$sender}");
                    $this->cache->delete("shipername_{$sender}");
                    //$this->session->unset_userdata('shipping_address');
                    //$this->session->unset_userdata('redeemID');

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => ['text' => $this->lang->line('pickaddress')]
                    ]; 
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    // Selection of address
                    $address = array();
                    $addressLists = getuserAddress($sender);

                    if(count($addressLists) > 0){
                      $address[] = array(
                          "title"     => $this->lang->line('newaddresstitle'), 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                              array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('newaddress'),
                                "payload"=>"PAYLOAD_NEWADDRESS"
                              )
                          )  
                      );
                      foreach($addressLists as $key => $addr) {
                        $address[] = array(
                          "title"     => $addr->shipping_address, 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('useaddress'),
                                  "payload"=>"{$addr->id}"
                                  )
                              )  
                          );
                      }
                    }
                    else{
                        $address[] = array(
                          "title"     => $this->lang->line('newaddresstitle'), 
                          "image_url" => base_url()."uploads/addressnew.jpg",
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('newaddress'),
                                  "payload"=>"PAYLOAD_NEWADDRESS"
                                  )
                              )  
                          );
                    }

                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                          "template_type"=>"generic",
                          "elements"=> $address
                          ]
                        ]
                      ];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];


                    /*$options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_nextday'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_express'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          ),
                          2 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_standard'),
                              'payload' => "PAY_LOAD_DELIVERY"
                          )
                      ); 

                    $quickoptions = json_encode($options);

                    $message_to_reply = $this->lang->line('delivery_speed');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                    ];*/

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                else:
                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => ['text' => $this->lang->line('noitems')]
                    ]; 
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                endif; 
                $this->cache->delete("keysearch_{$sender}");  
            break;

            case 'PAYLOAD_NEWADDRESS':
                
                // Remove all caches 
                $this->cache->delete("product_id_{$sender}"); 
                $this->cache->delete("userqty_{$sender}");
                $this->cache->delete("product_title_{$sender}");
                $this->cache->delete("keysearch_{$sender}");  
                $this->cache->delete("delivery_postcode_{$sender}");
                $this->cache->delete("validdistrict_{$sender}");
                $this->cache->delete("validpostcode_{$sender}");
                $this->cache->delete("valid_getaddress_{$sender}");
                $this->cache->delete("delivery_method_{$sender}"); 
                //$this->cache->delete("delivery_getcountry_{$sender}"); 
                $this->cache->delete("delivery_getphone_{$sender}"); 
                $this->cache->delete("delivery_getemail_{$sender}");
                $this->cache->delete("delivery_custemail_{$sender}");
                $this->cache->delete("deliver_address_{$sender}");
                $this->cache->delete("discountamount_{$sender}");   
                $this->cache->delete("lastcouponID_{$sender}");
                $this->cache->delete("pay_method_{$sender}");
                //$this->cache->clean();

                $options = array(
                      0 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('delivery_nextday'),
                          'payload' => "PAY_LOAD_DELIVERY"
                      ),
                      1 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('delivery_express'),
                          'payload' => "PAY_LOAD_DELIVERY"
                      ),
                      2 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('delivery_standard'),
                          'payload' => "PAY_LOAD_DELIVERY"
                      )
                  ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply = $this->lang->line('delivery_speed');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
            break; 

            case 'DEVELOEPR_HOMEMENU':

            $this->cache->delete("product_id_{$sender}"); 
            $this->cache->delete("userqty_{$sender}");
            $this->cache->delete("product_title_{$sender}");
            $this->cache->delete("keysearch_{$sender}");  
            $this->cache->delete("delivery_postcode_{$sender}");
            $this->cache->delete("validdistrict_{$sender}");
            $this->cache->delete("validpostcode_{$sender}");
            $this->cache->delete("valid_getaddress_{$sender}");
            $this->cache->delete("delivery_method_{$sender}"); 
            //$this->cache->delete("delivery_getcountry_{$sender}"); 
            $this->cache->delete("delivery_getphone_{$sender}"); 
            $this->cache->delete("delivery_getemail_{$sender}");
            $this->cache->delete("delivery_custemail_{$sender}");
            $this->cache->delete("deliver_address_{$sender}");
            $this->cache->delete("discountamount_{$sender}");   
            $this->cache->delete("lastcouponID_{$sender}");
            $this->cache->delete("pay_method_{$sender}");
            $this->cache->delete("shipername_{$sender}");
            //$this->session->unset_userdata('shipping_address');
            //$this->session->unset_userdata('redeemID');

              //die($this->lang->line('haveitem'));
            /*************************** Welcome loop ****************************/
                  $answer = ["attachment"=>[
                    "type"=>"template",
                    "payload"=>[
                      "template_type"=>"button",
                      "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                      "buttons"=>[
                        [
                          "type"=>"postback",
                          "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                          "title" => $this->lang->line('mostpopular'), //What's Hot
                        ],
                        [
                          "type"=>"postback",
                          "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                          "title" => $this->lang->line('discover')
                        ],
                        [
                          "type"=>"postback",
                          "title"=>$this->lang->line('viewmore'),
                          "payload"=>"PAY_LOAD_1"
                        ]
                      ]
                    ]
                  ]];

               $responseloop = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => $answer 
               ];
              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseloop,'POST');

              $total_rows_carts = $this->Shopping_carts_model->where(array('user_id' => $sender))->get_all();

                if($total_rows_carts){

                  $options = array(
                      0 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('yes'),
                        'payload' => "payload_checkout_cart"
                      ),
                      1 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('no'),
                        'payload' => "payload_checkout_cart"
                      )
                  ); 

                  $quickoptions = json_encode($options);
                  /*$message_to_reply = "Hey, You already have products in your cart. \n\nDo you want checkout ?";*/
                  $message_to_reply = $this->lang->line('haveitem');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
            break;  

            case 'DEVELOPER_DEFINED_PAYLOAD':

                if($posttitletext == strtolower($this->lang->line('mostpopular'))){ //what's hot

                    $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('product/services'),
                                'payload' => "PAY_LOAD_1"
                            )/*,
                            1 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('event'),
                                'payload' => "PAY_LOAD_1"
                            )*/
                        ); 

                       $quickoptions = json_encode($options);

                       $message_to_reply = $this->lang->line('checkoutmost');

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];
                       $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                       exit;
                }
                if($posttitletext == strtolower($this->lang->line('discover'))){

                    $optionslist = array();
                    $categories = categroy_lists($page_id);//$page_id

                    if(count($categories) > 0){
                          $optionslist[] = array(
                          "title"     => $this->lang->line('other'), 
                          "image_url" => base_url()."uploads/searchimg.png",
                          "buttons"   => array(
                                  array(
                                    "type"=>"postback",
                                    "title"=>$this->lang->line('other'),
                                    "payload"=>"PAY_LOAD_1"
                                  )
                              )  
                          );
                      foreach ($categories as $value) {
                          if($this->language == 'english'){
                            $name = $value->category_name;
                          }else{
                            $name = $value->category_name_thai;
                          }

                          $optionslist[] = array(
                          "title"     => $name, 
                          "image_url" => base_url()."uploads/categories/".$value->category_image,
                          "buttons"   => array(
                                  array(
                                    "type"=>"postback",
                                    "title"=>ucwords($name),
                                    "payload"=>"PAY_LOAD_CATEGORY"
                                  )    
                              )  
                          );

                      }

                      $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"generic",
                            "elements"=>$optionslist
                          ]
                      ]];

                      $message_to_reply = $this->lang->line('browse');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                      
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      //$message_to_reply = $this->lang->line('browse');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $answer
                      ];

                    }
                    else{
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $this->lang->line('nocat')
                      ];
                   }

                   //file_put_contents('filename.txt', print_r($response, true));

                    /*$optionslist = array();
                    $categories = categroy_lists();

                    if(count($categories) > 0){
                      $optionslist[] = array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('other'),
                            'payload' => "PAY_LOAD_1" 
                          );
                      foreach ($categories as $value) {
                          if($this->language == 'english'){
                            $name = $value->category_name;
                          }else{
                            $name = $value->category_name_thai;
                          }
                          $optionslist[] = array(
                            'content_type' => 'text',
                            'title' => ucwords($name),
                            'payload' => "category" 
                          );
                      }


                      $quickoptions = json_encode($optionslist);

                      $message_to_reply = $this->lang->line('browse');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                    }
                    else{
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $this->lang->line('nocat')
                      ];
                   }*/
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                   exit;
                }
                if($posttitletext == strtolower($this->lang->line('faq'))){

                   //$message_to_reply = 'Contact Human Manager on Facebook';
                  $message_to_reply = $faq_msg ? $faq_msg : "Here is a list of General Enquiries";

                   $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('plt'),
                                'payload' => "PAY_LOAD_TIME"
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('plorder'),
                                'payload' => "PAY_LOAD_ORDER"
                            ),
                            2 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('ploffice'),
                                'payload' => "PAY_LOAD_OFFICE"
                            ),
                            3 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('plissue'),
                                'payload' => "PAY_LOAD_ISSUE"
                            )

                        ); 

                   $quickoptions = json_encode($options);

                  // $message_to_reply = $posttitletext;

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                   ];
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                   exit;

                  /*   $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                     ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;*/

                }
                if($posttitletext == strtolower($this->lang->line('aboutpage'))){ 

                    if($page_id == "1048681488539732"){
                      $token = "280068859069546|JcVKVo4ZvtXp34pyFV7Djrd70lA";
                    }
                    else{
                      $token = $this->access_token;
                    }
                    $content = $this->curl("https://graph.facebook.com/{$page_id}?fields=about,name&access_token=".$token,'','GET'); 
                      //curl_close($ch);  
                    $message_to_reply = $content->about;

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];
                    //file_put_contents('filename.txt', print_r($page_id, true));
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  
                    /*************************** Welcome loop ****************************/
                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('mostpopular'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('discover')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $responseloop = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseloop,'POST');  
                exit;

                }
                // 14-11-2017
                if($posttitletext == strtolower($this->lang->line('endchat'))){

                     $message_to_reply = $dismiss_msg ? $dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

                     $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                     ];

                    $this->cache->delete("keysearch_{$sender}");  
                    $this->cache->delete("delivery_postcode_{$sender}");
                    $this->cache->delete("validdistrict_{$sender}");
                    $this->cache->delete("validpostcode_{$sender}");
                    $this->cache->delete("valid_getaddress_{$sender}");
                    $this->cache->delete("delivery_method_{$sender}"); 
                    //$this->cache->delete("delivery_getcountry_{$sender}"); 
                    $this->cache->delete("delivery_getphone_{$sender}"); 
                    $this->cache->delete("delivery_getemail_{$sender}"); 
                    $this->cache->delete("deliver_address_{$sender}");
                    $this->cache->delete("discountamount_{$sender}");
                    $this->cache->delete("pay_method_{$sender}");
                    $this->cache->delete("couponcode_{$sender}");
                    $this->cache->delete("lastcouponID_{$sender}");

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }   
              break;

              case 'PAY_LOAD_CATEGORY':

                $page = 0;
                $catproducts = productlists_by_category($posttitletext,$page,10);
               
                $total_rows_count = $catproducts['totalrows'];
                $products = array();


                if(count($catproducts['result']) > 0) :
                  foreach ($catproducts['result'] as $key => $product) {
                      if($this->language == 'english'){
                        $name = $product->product_name;
                        $description = $product->product_description;
                      }
                      else{
                        $name = $product->product_name_thai;
                        $description = $product->product_description_thai;
                      }
                      if($product->sale_price > 0){
                        $price = $product->sale_price;
                      }else{
                        $price = $product->product_price;
                      }
                     
                      $products[] = array(
                      "title"     => $name, 
                      "image_url" => base_url()."uploads/products/{$product->product_images[0]}",
                      "subtitle"  => '฿'.$price.' - '.$description,
                      "buttons"   => array(
                              array(
                              "type"=>"postback",
                              "title"=>$this->lang->line('checkout'),
                              "payload"=>"{$product->product_id}"
                              ),
                              array(
                              "type"=>"postback",
                              "title"=>$this->lang->line('addtocart'),
                              "payload"=>"$product->product_id"
                              )
                          )  
                      );
                  }

                 $answer = ["attachment"=>[
                  "type"=>"template",
                  "payload"=>[
                      "template_type"=>"generic",
                      "elements"=> $products
                      ]
                    ]
                  ];

                else :

                  $message_to_reply = "{$this->lang->line('nocatfound')} {$posttitletext}";
                  $answer = [ 'text' => $message_to_reply];
                
                endif;

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => $answer 
                ];

                if($total_rows_count > 10){
                      $page = $page + 1;
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $this->cache->save('nextcategoryproduct', $page);
                      $this->cache->save('currentcategory', $posttitletext);
                      $this->cache->save('currentcategorypagecount', count($catproducts['result']));
                
                      $nextproducts[] = array(
                          "title"     => $this->lang->line('clickviewmoreproduct'), 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('moreproduct'),
                                  "payload"=>'Category_Payload'
                                  )      
                              )  
                          );

                      $nextproductdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextproducts
                                ]
                            ]];

                    $responsedata = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextproductdata
                    ];
                     
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');

                    $checkTime = strtotime(date('Y-m-d H:i:s'));
                    $this->cache->save('timeinterval',$checkTime,300);
                    exit;
                }

                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                // Feedback 07-02-2018
                /* else{
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  $options = array(
                      0 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('yes'),
                        'payload' => "flag"
                      ),
                      1 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('no'),
                        'payload' => "flag"
                      )
                    ); 

                   $quickoptions = json_encode($options);
           
                   $message_to_reply = $findin_page ? $findin_page : $this->lang->line('still');

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                   ];
                   $this->db->where('user_id',$sender);
                   $this->db->delete('trigger_data');
                  
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                } */

                  $this->db->where('user_id',$sender);    
                  $trigger = $this->db->get('trigger_data')->row();
                  if($trigger){                        
                    
                    $userdata = ['user_id'=>$sender,'time_stamp'=>$time];
                    $this->db->where('trigger_id',$trigger->trigger_id);
                    $this->db->update('trigger_data',$userdata);
                  }else{                        
                    
                    $userdata = ['user_id'=>$sender,'time_stamp'=>$time];
                    $this->db->insert('trigger_data',$userdata);
                  }

              break;

              case 'PAY_LOAD_1':

                if($posttitletext == strtolower($this->lang->line('viewmore'))){

                    $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$this->lang->line('moreopt'),
                            "buttons"=>[
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('aboutpage'),
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('faq')
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('endchat')
                              ]
                            ]
                          ]
                        ]];

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                      ];
                }
               if($posttitletext == strtolower($this->lang->line('other'))){
                    $this->cache->save("keysearch_{$sender}", $posttitletext);
                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $this->lang->line('searchproduct')]
                    ];
                }
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                break;

              case 'Products_Payload':

                  if($posttitletext == strtolower($this->lang->line('moreproduct'))){
                        
                    $nextpage = $this->cache->get("nextproduct_{$sender}"); 
                   
                    if(!empty($nextpage)){
                      $productslists = product_lists($nextpage,10,$page_id);
                      $products = array();

                        if(count($productslists) > 0) :
                            foreach ($productslists as $key => $product) {
                                if($this->language == 'english'){
                                  $name = $product->product_name;
                                  $description = $product->product_description;
                                }else{
                                  $name = $product->product_name_thai;
                                  $description = $product->product_description_thai;
                                }

                                if($product->sale_price > 0){
                                  $price = $product->sale_price;
                                }else{
                                  $price = $product->product_price;
                                }

                                $products[] = array(
                                "title"     => $name, 
                                "image_url" => base_url()."uploads/products/{$product->images[0]->product_image}",
                                "subtitle"  => '฿'.$price .' - '.$description,
                                "buttons"   => array(
                                        /*array(
                                          "type"=>"payment",
                                          "title"=>"buy",
                                          "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                          "payment_summary"=>[
                                            "currency"=>"USD",
                                            "payment_type"=>"FIXED_AMOUNT",
                                            "is_test_payment" => true, 
                                            "merchant_name"=>"Points Live",
                                            "requested_user_info"=>["shipping_address","contact_name", "contact_email"],
                                            "price_list"=>[
                                              [
                                                "label"=>"Subtotal",
                                                "amount"=>$product->product_price
                                              ],
                                              [
                                                "label"=>"Shipping",
                                                "amount"=>$product->shipping_cost
                                              ]
                                            ]
                                          ]
                                        ),*/
                                        /*array(
                                          "type"=>"web_url",
                                          "url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}",
                                          "title"=>$this->lang->line('checkout'),
                                          "webview_height_ratio"=>"full",
                                          "messenger_extensions"=> true,
                                          "fallback_url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}" 
                                        ),*/
                                        array(
                                        "type"=>"postback",
                                        "title"=>$this->lang->line('checkout'),
                                        "payload"=>"$product->product_id"
                                        ),
                                        array(
                                          "type"=>"postback",
                                          "title"=>$this->lang->line('addtocart'),
                                          "payload"=>"$product->product_id"
                                        )
                                    )  
                                );
                            }

                          $this->perpagerecord = $this->cache->get("currentpagecount_{$sender}") + count($productslists);

                           $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                                "template_type"=>"generic",
                                "elements"=> $products
                                ]
                              ]
                            ];

                        else :
                            
                          $message_to_reply = $this->lang->line('noproductfound');

                          $answer = [ 'text' => $message_to_reply];

                        endif;

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                        ];

                        if($this->perpagerecord != $total_rows_count) :

                          $nextpage = $nextpage + 1;
                          
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                        
                          $this->cache->save("nextproduct_{$sender}", $nextpage,300);
                          $this->cache->save("currentpagecount_{$sender}",$this->perpagerecord,300);

                          $nextproducts[] = array(
                              "title"     => $this->lang->line('clickviewmoreproduct'), 
                              "buttons"   => array(
                                      array(
                                      "type"=>"postback",
                                      "title"=>$this->lang->line('moreproduct'),
                                      "payload"=>'Products_Payload'
                                      )      
                                  )  
                              );

                           $nextdata = ["attachment"=>[
                                    "type"=>"template",
                                    "payload"=>[
                                      "template_type"=>"generic",
                                      "elements"=>$nextproducts
                                    ]
                                ]];

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $nextdata
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  

                        else:
                        $this->cache->delete("nextproduct_{$sender}");
                        $this->cache->delete("currentpagecount_{$sender}");  
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');    
                        endif;
                    }
                }
                if($posttitletext == strtolower($this->lang->line('viewmore'))){

                    $nextpage = $this->cache->get('nextcartproduct'); 
                   
                    if(!empty($nextpage)){

                      $productslists =  productcarts_by_userid($sender,$nextpage,10);
                      $products = array();
                      $price = 0;
                      $total_rows_carts = $this->Shopping_carts_model->where(array('user_id' => $sender))->get_all();
                      $total_rows_count_carts = count($total_rows_carts);

                      if(count($productslists) > 0) :
                            foreach ($productslists as $key => $product) {
                                if($this->language == 'english'){
                                  $name = $product->product_name;
                                  $description = $product->product_description;
                                }else{
                                  $name = $product->product_name_thai;
                                  $description = $product->product_description_thai;
                                }

                                if($product->sale_price > 0){
                                  $price = $product->sale_price;
                                }else{
                                  $price = $product->product_price;
                                }

                                $products[] = array(
                                "title"     => $name, 
                                "image_url" => base_url()."uploads/products/{$product->product_image}",
                                "subtitle"  => '฿'.$price .' - '.$description,
                                "buttons"   => array(
                                        array(
                                          "type"=>"postback",
                                          "title"=>$this->lang->line('updateqty'),
                                          "payload"=>"$product->product_id"
                                        ),
                                        array(
                                          "type"=>"postback",
                                          "title"=>$this->lang->line('removeproduct'),
                                          "payload"=>"$product->product_id"
                                        )
                                    )  
                                );
                            }

                          $this->perpagerecord = $this->cache->get('currentcartpagecount') + count($productslists);

                           $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                                "template_type"=>"generic",
                                "elements"=> $products
                                ]
                              ]
                            ];

                        else :
                            
                          $message_to_reply = $this->lang->line('noproductfound');

                          $answer = [ 'text' => $message_to_reply];

                        endif;

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                        ];

                        if($this->perpagerecord != $total_rows_count_carts) :

                          $nextpage = $nextpage + 1;
                          
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                        
                          $this->cache->save('nextcartproduct', $nextpage);
                          $this->cache->save('currentcartpagecount',$this->perpagerecord);

                          $nextproducts[] = array(
                              "title"     => $this->lang->line('clickviewmoreproduct'), 
                              "buttons"   => array(
                                      array(
                                      "type"=>"postback",
                                      "title"=>$this->lang->line('viewmore'),
                                      "payload"=>'Products_Payload'
                                      )      
                                  )  
                              );

                           $nextdata = ["attachment"=>[
                                    "type"=>"template",
                                    "payload"=>[
                                      "template_type"=>"generic",
                                      "elements"=>$nextproducts
                                    ]
                                ]];

                        $responsedata = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $nextdata
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');  

                        else:

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');    
                        endif;
                    }
                }
               break;

              case 'Category_Payload': 

                  $nextpage = $this->cache->get('nextcategoryproduct'); 
                   
                  if(!empty($nextpage)){
                    $category = $this->cache->get('currentcategory');
                    $productslists = productlists_by_category($category,$nextpage,10);
                    $products = array();
                    $total_rows_count = $productslists['totalrows'];

                    foreach ($productslists['result'] as $key => $product) {
                        if($this->language == 'english'){
                          $name = $product->product_name;
                          $description = $product->product_description;
                        }else{
                          $name = $product->product_name_thai;
                          $description = $product->product_description_thai;
                        }

                        if($product->sale_price > 0){
                          $price = $product->sale_price;
                        }else{
                          $price = $product->product_price;
                        }

                        $products[] = array(
                        "title"     => $name, 
                        "image_url" => base_url()."uploads/products/{$product->product_images[0]}",
                        "subtitle"  => '฿'.$price .' - '.$description,
                        "buttons"   => array(
                                 /*array(
                                  "type"=>"payment",
                                  "title"=>"buy",
                                  "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                  "payment_summary"=>[
                                    "currency"=>"USD",
                                    "payment_type"=>"FIXED_AMOUNT",
                                    "is_test_payment" => true, 
                                    "merchant_name"=>"Points Live",
                                    "requested_user_info"=>["shipping_address","contact_name", "contact_email"],
                                    "price_list"=>[
                                      [
                                        "label"=>"Subtotal",
                                        "amount"=>$product->product_price
                                      ],
                                      [
                                        "label"=>"Shipping",
                                        "amount"=>$product->shipping_cost
                                      ]
                                    ]
                                  ]
                                ),*/
                                /*array(
                                  "type"=>"web_url",
                                  "url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}",
                                  "title"=>$this->lang->line('checkout'),
                                  "webview_height_ratio"=>"full",
                                  "messenger_extensions"=> true,
                                  "fallback_url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$product->product_id}/{$this->language}" 
                                ),*/
                                array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('checkout'),
                                "payload"=>"$product->product_id"
                                ),
                                array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('addtocart'),
                                "payload"=>"$product->product_id"
                                )
                            )  
                        );
                    }

                  $this->perpagerecord = $this->cache->get('currentcategorypagecount') + count($productslists['result']);

                   $answer = ["attachment"=>[
                    "type"=>"template",
                    "payload"=>[
                        "template_type"=>"generic",
                        "elements"=> $products
                        ]
                      ]
                    ];

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                  ];

                  if($this->perpagerecord != $total_rows_count) :

                    $nextpage = $nextpage + 1;
                    
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  
                    $this->cache->save('nextcategoryproduct', $nextpage);
                    $this->cache->save('currentcategory',$category);
                    $this->cache->save('currentcategorypagecount',$this->perpagerecord);

                    $nextproducts[] = array(
                        "title"     => $this->lang->line('clickviewmoreproduct'), 
                        "buttons"   => array(
                                array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('moreproduct'),
                                "payload"=>'Category_Payload'
                                )      
                            )  
                        );

                     $nextdata = ["attachment"=>[
                              "type"=>"template",
                              "payload"=>[
                                "template_type"=>"generic",
                                "elements"=>$nextproducts
                              ]
                          ]];

                  $responsedata = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $nextdata
                  ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');  

                  else:

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  $options = array(
                      0 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('yes'),
                        'payload' => "flag"
                      ),
                      1 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('no'),
                        'payload' => "flag"
                      )
                    ); 

                   $quickoptions = json_encode($options);

                   $message_to_reply = $findin_page ? $findin_page : $this->lang->line('still');

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                   ];
                  
                   $this->db->where('user_id',$sender);
                   $this->db->delete('trigger_data');

                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                   endif;
                  }
              break;

              case 'Payment_Checkout_Cash':

                $products = productlists_by_userid($sender); // Get products ID's
                $useraddress = $this->cache->get("deliver_address_{$sender}");

                if(count($products) > 0){
                 // Save address in table of users
                  $district = $this->cache->get("validdistrict_{$sender}");
                  $postcode = $this->cache->get("validpostcode_{$sender}");
                  $addressdata = $this->cache->get("valid_getaddress_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_method_{$sender}"); 
                  if($this->language == 'english'){
                    $shipcountry = "Thailand";
                  }
                  else{
                    $shipcountry = "ประเทศไทย";
                  }
                  //$shipcountry = $this->cache->get('delivery_getcountry'); 
                  $shipphone = $this->cache->get("delivery_getphone_{$sender}"); 
                  $shipemail = $this->cache->get("delivery_getemail_{$sender}"); 
                  $discountamount = $this->cache->get("discountamount_{$sender}");
                  $shipername = $this->cache->get("shipername_{$sender}");

                  $info = array(
                      'user_id'   => $sender,
                      'postcode'  => $postcode,
                      'district'  => $district,
                      'country'   => $shipcountry,
                      'receiver_name' => $shipername,
                      'phone'     => $shipphone,
                      'email'     => $shipemail,
                      'delivery_method' => $deliverymethod,
                      'shipping_address'  => $addressdata
                    );

                  if(empty($useraddress)){
                     $provinces = getStateByDistrict($district); // States shipping address

                      if($this->language == 'english'){
                        $state = $provinces[0]->province;
                      }else{
                        $state = $provinces[0]->province_thai;
                      }
                    $addressID = $this->Address_model->insert($info); // Save address in table  
                  }
                  else{
                    $addressID = $useraddress;  
                    $shipaddress = $this->Address_model->get($addressID);
                    $provinces = getStateByDistrict($shipaddress->district); // States shipping address
                    if($this->language == 'english'){
                      $state = $provinces[0]->province;
                    }else{
                      $state = $provinces[0]->province_thai;
                    }
                  }

                  //$addressID = $this->Address_model->insert($info); // Save address in table
                  $Pids = array();
                  $sum = 0;
                  foreach ($products as $key => $value) {
                      $Pids[] = $value->product_id;
                      $qty[] = $value->qty;
                      if($value->sale_price > 0){
                        $price = $value->sale_price;
                      }else{
                        $price = $value->product_price;
                      }
                      $sum = $sum + ($price * $value->qty) + $value->shipping_cost;
                  }

                  $orderInfo = array(
                      "transaction_id"   => time(),
                      "user_id"          => $sender,
                      'page_id'          => $page_id,
                      "shipping_address" => $addressID,
                      "order_amount"     => ($sum - $discountamount),
                      "payment_method"   => "cash",
                      "status"           => "0",
                      "transaction_ref"  => "",
                      "order_type"       => "sales"
                  );

                  $saved = $this->Orders_model->insert($orderInfo); // Save order's

                  if($saved){
                    $orderdetails = $this->Orders_model->get($saved);
                    foreach ($Pids as $key => $value) {
                      // Save orders with products
                      $this->Orders_products_model->insert(array('order_id' => $orderdetails->transaction_id,'product_id' => $value,'ordered_qty' => $qty[$key]));
                    }
                    // Remove user shopping cart products
                    $this->Shopping_carts_model->force_delete(array('user_id' => $sender));

                    // Stored order id in redeemption coupon code of user
                    if(!empty($this->cache->get("lastcouponID_{$sender}"))){
                      // Get last discount amount of user
                      $discountuseramount = $this->db->select('id,total_discount')->from('coupon_redemptions')
                        ->where('id',$this->cache->get("lastcouponID_{$sender}"))
                        //->limit(1)
                        //->order_by('coupon_id','DESC')
                        ->get()
                        ->row();

                      $where = array('id' => $discountuseramount->id);
                      $this->db->where($where);
                      $this->db->update('coupon_redemptions',array('order_id' => $orderdetails->transaction_id));
                      //file_put_contents('filename.txt', print_r($this->db->last_query(), true));
                      //echo $this->db->last_query();
                    }
                    //exit;
                    $findstring = "";
                    $findstring = str_replace("<pagename>","Points live",$this->lang->line('cashthanks'));
                    $findstring = str_replace("<ordernumber>",$orderdetails->transaction_id,$findstring);

                    $message_to_reply = $findstring;

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    $message_to_reply = $this->lang->line('keepstatus');

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    // Create Sales Orders
                    $shipaddress = $this->Address_model->get($addressID);
                    $shiptype = "";
                    if($shipaddress->delivery_method == 'next_day'){
                      $shiptype = "NEXT_DAY";
                    }
                    if($shipaddress->delivery_method == 'express' || $shipaddress->delivery_method == 'ด่วน'){
                      $shiptype = "EXPRESS_1_2_DAYS";
                    }
                    if($shipaddress->delivery_method == 'standard' || $shipaddress->delivery_method == 'มาตรฐาน'){
                      $shiptype = "STANDARD_2_4_DAYS";
                    }

                    /*$userinfo = $this->getUserInfo("{$page_id}","{$sender}");

                    $fullname = $userinfo->first_name.' '.$userinfo->last_name;*/
                    
                    $businessaddress = $this->getPageLocationHours("{$page_id}");

                    $validatetoken = $this->validateAPI(); // Get Access Token of Logistic API

                    if(!is_null($validatetoken)){
                      $token = $validatetoken['token']['token_id'];
                    }
       
                    $addressee = "";
                    $address1 = "";
                    $province = "";
                    $postalCode = "";
                    $country = "";
                    $phone = "";
                    $email = "";

                    if(isset($businessaddress->name)){
                      $addressee = $businessaddress->name;
                    }
                    if(isset($businessaddress->single_line_address)){
                      $address1 = $businessaddress->single_line_address;
                      $explodeaddr = explode(",", $businessaddress->single_line_address);
                      $provinces = explode(" ", $explodeaddr[2]);
                      $provincename = $provinces[1];
                      $postalCode = $provinces[2];
                    }
                    
                    if(isset($businessaddress->phone)){
                      $phone = $businessaddress->phone;
                    }
                    if(isset($businessaddress->emails)){
                      $email = $businessaddress->emails[0];
                    }

                    $orders = orderlists($orderdetails->transaction_id);
                    
                    $orderItems = array();

                    foreach ($orders[0]->products as $key => $value) {
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }
                        if($this->language == 'english'){
                            $pname = $value->product_name;
                          }else{
                            $pname = $value->product_name_thai;
                          }
                       $orderItems[] = array(
                        "partnerId"=> "1163",
                        "itemId"=> "{$value->product_sku}",
                        "qty"=> (int)$value->ordered_qty,
                        "subTotal"=> (int)$price 
                        );
                       $shiporderItems[] = array(
                          "itemDescription" => "{$pname}",
                          "itemQuantity"    => (int)$value->ordered_qty,
                          );
                    }
      
                    $ordersDetails = [
                      "customerInfo" => [
                          "addressee"=>"{$addressee}",
                          "address1"=>"{$address1}",
                          "province"=>"{$provincename}",
                          "postalCode"=>"{$postalCode}",
                          "country"=>"{$provincename}",
                          "phone"=> "{$phone}",
                          "email"=>"{$email}"
                      ],
                      "orderShipmentInfo" =>[
                          "addressee"=>"{$shipaddress->receiver_name}",//{$fullname}
                          "address1"=>"{$shipaddress->shipping_address}",
                          "address2"=>"",
                          "subDistrict"=>"",
                          "district"=>"{$shipaddress->district}",
                          "city"=>"",
                          "province"=>"{$state}",
                          "postalCode"=>"{$shipaddress->postcode}",
                          "country"=>"{$shipaddress->country}",
                          "phone"=>"{$shipaddress->phone}",
                          "email"=>"{$shipaddress->email}"
                      ],
                      "paymentType"=>"COD",
                      "shippingType"=>"{$shiptype}",
                      "grossTotal"=>(int)$orders[0]->order_amount,
                      "currUnit"=>"THB",
                      "orderItems"=>$orderItems
                    ]; 
                    $salesorder = createSalesOrder("https://fulfillment.api.acommercedev.com/channel/demoth1/order/{$orderdetails->transaction_id}",$token,$ordersDetails);

                    //file_put_contents('filename.txt', print_r($ordersDetails, true));

                    if(isset($salesorder['http_code']) && $salesorder['http_code'] == 201){
                      
                      $findorder = str_replace("<orderno>", "{$orderdetails->transaction_id}", $this->lang->line('is_process'));  

                      $message_to_reply = $findorder;

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                      $pgtoken = getFacebookpagetoken($page_id); // Get access token
                     // file_put_contents('filename.txt', print_r($result, true));
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken[0]->access_token,$response,'POST');

                      // shipping order generation
                      $shipordersDetails = [
                        "shipServiceType" => "SMARTSHIP_DROPOFF",
                        "shipSender" => [
                            "addressee"=>"{$addressee}",
                            "address1"=>"{$address1}",
                            "city"=>"",
                            "province"=>"{$provincename}",
                            "postalCode"=>"{$postalCode}",
                            "country"=> "{$provincename}",
                            "phone"=> "{$phone}",
                            "email"=>"{$email}"
                        ],
                        "shipShipment" =>[
                            "addressee"=>"{$shipaddress->receiver_name}",
                            "address1"=>"{$shipaddress->shipping_address}",
                            "address2"=>"",
                            "subDistrict"=>"",
                            "district"=>"{$shipaddress->district}",
                            "city"=>"",
                            "province"=>"{$state}",
                            "postalCode"=>"{$shipaddress->postcode}",
                            "country"=>"{$shipaddress->country}",
                            "phone"=>"{$shipaddress->phone}",
                            "email"=>"{$shipaddress->email}"
                        ],
                        "shipShippingType"=>"{$shiptype}",
                        "shipPaymentType"=>"COD",
                        "shipCurrency"=>"THB",
                        "shipGrossTotal"=>(int)$orders[0]->order_amount,
                        "shipInsurance"=>false,
                        "shipPickingList"=>$shiporderItems,
                        "shipPackages"  => []
                      ];

                     // file_put_contents('filename.txt', print_r($shipordersDetails, true));

                      $shippingorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$orderdetails->transaction_id}",$token,$shipordersDetails);

                      /*if(isset($shippingorder['http_code']) && $shippingorder['http_code'] == 201){
                          $this->Orders_model->where('transaction_id',$orderdetails->transaction_id)->update(array('status' => '1','shipment_date' => date('Y-m-d h:i:s')));
                      }*/
                      // end shipping creation
                     
                    }else{
                     
                      $message_to_reply = $this->lang->line('code'); //422 code. Validation failed.
 
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
                  }

                  $this->cache->delete("delivery_postcode_{$sender}");
                  $this->cache->delete("validdistrict_{$sender}");
                  $this->cache->delete("validpostcode_{$sender}");
                  $this->cache->delete("valid_getaddress_{$sender}");
                  $this->cache->delete("delivery_method_{$sender}"); 
                  $this->cache->delete("delivery_getcountry_{$sender}"); 
                  $this->cache->delete("delivery_getphone_{$sender}"); 
                  $this->cache->delete("delivery_getemail_{$sender}");
                  $this->cache->delete("deliver_address_{$sender}");
                  $this->cache->delete("discountamount_{$sender}");
                  $this->cache->delete("lastcouponID_{$sender}");
                  $this->cache->delete("pay_method_{$sender}");
                  $this->cache->delete("shipername_{$sender}");
                }
              break;

              default:

              if($posttitletext == strtolower($this->lang->line('useaddress'))){

                $this->cache->delete("product_id_{$sender}"); 
                $this->cache->delete("userqty_{$sender}");
                $this->cache->delete("product_title_{$sender}");
                $this->cache->delete("keysearch_{$sender}");  
                $this->cache->delete("delivery_postcode_{$sender}");
                $this->cache->delete("validdistrict_{$sender}");
                $this->cache->delete("validpostcode_{$sender}");
                $this->cache->delete("valid_getaddress_{$sender}");
                $this->cache->delete("delivery_method_{$sender}"); 
                //$this->cache->delete("delivery_getcountry_{$sender}"); 
                $this->cache->delete("delivery_getphone_{$sender}"); 
                $this->cache->delete("delivery_getemail_{$sender}");
                $this->cache->delete("delivery_custemail_{$sender}");
                $this->cache->delete("deliver_address_{$sender}");
                $this->cache->delete("discountamount_{$sender}");   
                $this->cache->delete("lastcouponID_{$sender}");
                $this->cache->delete("pay_method_{$sender}");
                $this->cache->delete("shipername_{$sender}");

                $options = array(
                    0 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('delivery_pickupopt1'),
                        'payload' => "PAY_LOAD_PICKUP"
                    ),
                    1 => array(
                        'content_type' => 'text',
                        'title' => $this->lang->line('delivery_pickupopt2'),
                        'payload' => "PAY_LOAD_PICKUP"
                    )
                ); 

                $quickoptions = json_encode($options);

                $message_to_reply = $this->lang->line('delivery_pickup');

                $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                ];

                $this->cache->save("deliver_address_{$sender}",$postback,320); // 5 minutes
               // echo "SS=". $this->cache->get("deliver_address_{$sender}");
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              }

              if($posttitletext == strtolower($this->lang->line('addtocart')) || $posttitletext == strtolower($this->lang->line('checkout'))){
                  // Check product added in basket then update
                  $checkqty = $this->Products_model->get(array('product_id' => $postback)); // check quantity
                  $checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}",'product_id'=>$postback))->get();

                  if($checkexists){
                    $qty = $checkexists->qty + 1; // Update quantity
                    $update_data = array('qty'=>$qty);
                    $updated = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}",'product_id'=>$postback))->update($update_data);
                    if($updated){
                      /****** 14-11-2017 **********/
                     // $availqty = $checkqty->total_qty - 1;
                     // $updated_data = array('total_qty' => $availqty);
                        // update quantity
                      //$this->Products_model->where('product_id',$postback)->update($updated_data);
                      /************* end ************/
                      $message_to_reply = "{$this->lang->line('cartupdated')} {$qty}";

                      $responseupdate = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseupdate,'POST');

                      // feedback tayo 07-02-2018
                       $options = array(
                                /*0 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('yes'),
                                    'payload' => "PAY_LOAD_1"
                                ),*/
                                /*0 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('no'),
                                    'payload' => "PAY_LOAD_1"
                                ),*/
                                0 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('continueshopping'),
                                    'payload' => "flag"
                                ),
                                1 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('updatecart'),
                                    'payload' => "PAY_LOAD_1"
                                ),
                                2 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('checkout'),
                                    'payload' => "PAY_LOAD_1"
                                )
                            ); 

                       $quickoptions = json_encode($options);

                       $message_to_reply = $this->lang->line('addbasket');

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];
                       $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
                  }
                  else{
                    /*$checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}"))->get();

                    if($checkexists){
                    // 20-12-2017
                      $message_to_reply = $this->lang->line('cannotadd');
                      $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                         ];
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                      
                    }else{*/
                    $data = array(
                        'user_id'     => $sender,
                        'product_id'  => $postback,
                        'qty'         => 1
                      );  

                    $ok = $this->Shopping_carts_model->insert($data);

                    if($ok){
                        
                        /********14-11-2017 ***********/
                        //$availqty = $checkqty->total_qty - 1;
                        //$updated_data = array('total_qty' => $availqty);
                          // update quantity
                        //$this->Products_model->where('product_id',$postback)->update($updated_data);
                        /************ end *************/

                        $products = $this->Products_model->get(array('product_id' => $postback));

                        $replaceword = str_replace("(product_name)", "{$products->product_name}", $product_confirm_msg);

                        $message_to_reply = $product_confirm_msg ? $replaceword : "Thanks. To confim, you want {$products->product_name}";

                        $responsedata = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                         ];
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');

                        if($products->sale_price > 0){
                          $price = $products->sale_price;
                        }else{
                          $price = $products->product_price;
                        }

                        $replacewordorder = str_replace("(no_of_unit)", "1", $order_cost_msg);
                        $replacewordorder = str_replace("(total_price)", "{$price}", $replacewordorder);

                        $message_to_reply = $order_cost_msg ? $replacewordorder : "1 {$this->lang->line('willcost')} {$price}";

                        $responsedatas = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                         ];
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedatas,'POST');

                        $options = array(
                                /*0 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('yes'),
                                    'payload' => "PAY_LOAD_1"
                                ),*/
                                /*0 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('no'),
                                    'payload' => "PAY_LOAD_1"
                                ),*/
                                0 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('continueshopping'),
                                    'payload' => "flag"
                                ),
                                1 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('updatecart'),
                                    'payload' => "PAY_LOAD_1"
                                ),
                                2 => array(
                                    'content_type' => 'text',
                                    'title' => $this->lang->line('checkout'),
                                    'payload' => "PAY_LOAD_1"
                                )
                            ); 

                       $quickoptions = json_encode($options);

                       $message_to_reply = $this->lang->line('addbasket');

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
                }
            }
            // feedback tayo 07-02-2018
            if($posttitletext == strtolower($this->lang->line('removeproduct'))){
                // 15-11-2017
               // $carts = $this->Shopping_carts_model->get(array('product_id' => $postback,'user_id' => "{$sender}"));
                //$reservedqty = $carts->qty;
               // $checkqty = $this->Products_model->get(array('product_id' => $postback));

               // $availqty = $checkqty->total_qty + $carts->qty;
               // $updated_data = array('total_qty' => $availqty);
                // update quantity
               // $this->Products_model->where('product_id',$postback)->update($updated_data);
                // end 
                $success = $this->Shopping_carts_model->where(array('product_id'=>$postback))->force_delete(array('user_id' => "{$sender}"));
                if($success){

                  $message_to_reply =  $this->lang->line('removebasket');

                  $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                  ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }

                $cartscount = $this->Shopping_carts_model->get(array('user_id' => "{$sender}"));

                if($cartscount){
                    $options = array(
                        /*0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "PAY_LOAD_1"
                        ),*/
                       /* 0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "PAY_LOAD_1"
                        ),*/
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('continueshopping'),
                            'payload' => "flag"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('updatecart'),
                            'payload' => "PAY_LOAD_1"
                        ),
                        2 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('checkout'),
                            'payload' => "PAY_LOAD_1"
                        )
                    ); 

                   $quickoptions = json_encode($options);

                   $message_to_reply = $this->lang->line('addbasket');

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                   ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                 exit;
              }
              else{
                $message_to_reply = $this->lang->line('noitemcart');
                $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                $optionslist = array();
                $categories = categroy_lists($page_id);//$page_id

                if(count($categories) > 0){
                  /*$optionslist[] = array(
                      'content_type' => 'text',
                      'title' => $this->lang->line('other'),
                      'payload' => "PAY_LOAD_1" 
                    );*/
                 $optionslist[] = array(
                      "title"     => $this->lang->line('other'), 
                      "image_url" => base_url()."uploads/searchimg.png",
                      "buttons"   => array(
                              array(
                                "type"=>"postback",
                                "title"=>$this->lang->line('other'),
                                "payload"=>"PAY_LOAD_1"
                              )
                          )  
                      );
                  foreach ($categories as $value) {
                      if($this->language == 'english'){
                        $categoryanme = $value->category_name;
                      }
                      else{
                        $categoryanme = $value->category_name_thai; 
                      }
                      $optionslist[] = array(
                      "title"     => $categoryanme, 
                      "image_url" => base_url()."uploads/categories/".$value->category_image,
                      "buttons"   => array(
                              array(
                                "type"=>"postback",
                                "title"=>ucwords($categoryanme),
                                "payload"=>"PAY_LOAD_CATEGORY"
                              )    
                          )  
                      );
                     /* $optionslist[] = array(
                        'content_type' => 'text',
                        'title' => ucwords($categoryanme),
                        'payload' => "category" 
                      );*/
                  }
                 /* $quickoptions = json_encode($optionslist);

                  $message_to_reply = $this->lang->line('browse');*/

                  $message_to_reply = $this->lang->line('browse');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                  ];
                  
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"generic",
                        "elements"=>$optionslist
                      ]
                  ]];

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer
                   ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else{
                  $message_to_reply = $this->lang->line('nocat');

                  $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                   ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
              }
            } 
            if($posttitletext == strtolower($this->lang->line('updateqty'))){
               $message_to_reply = $this->lang->line('enterqty');
               $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                  ];
               $this->cache->save("updateproduct_{$sender}",$postback,300); // 5 minutes stored  
               $this->cache->delete("userqty_{$sender}");
               $this->cache->delete("product_id_{$sender}");
               $this->cache->delete("product_title_{$sender}");
               $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
            }
            break;
          }
          //$this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');     
        } 
    }
  }

  public function savePrefrences(){

      if($this->input->post()){
          
          $chatoptions = array(
            'welcome_msg' =>  $this->input->post('welcome_reply'),
            'welcome_msg_thai' =>  $this->input->post('welcome_reply_thai'),
            'faq_msg' =>  $this->input->post('faq_message'),
            'faq_msg_thai' =>  $this->input->post('faq_message_thai'),
            'dismiss_msg' =>  $this->input->post('dismiss_message'),
            'dismiss_msg_thai' =>  $this->input->post('dismiss_message_thai'),
            'feedback_msg' =>  $this->input->post('feedback_message'),
            'feedback_msg_thai' =>  $this->input->post('feedback_message_thai'),
            'product_confirm_msg' =>  $this->input->post('product_confirm'),
            'product_confirm_msg_thai' =>  $this->input->post('product_confirm_thai'),
            'order_cost_msg' =>  $this->input->post('order_cost'),
            'order_cost_msg_thai' =>  $this->input->post('order_cost_thai'),
            'payment_confirm_msg' =>  $this->input->post('payment_confirm'),    
            'payment_confirm_msg_thai' =>  $this->input->post('payment_confirm_thai'),    
            'findin_page' =>  '',    
            'findin_page_thai' =>  ''    
            );

          if(empty($this->input->post('last_id'))){

             if($this->Chat_prefrences_model->insert($chatoptions)){
                $this->session->set_flashdata('flashmsg', 'Chat options saved successfully.');
                $this->session->set_flashdata('msg', 'success');
             }
             else{
                $this->session->set_flashdata('flashmsg', 'Error while saved data.');
                $this->session->set_flashdata('msg', 'danger');
             }
          }
          else{
            if($this->Chat_prefrences_model->update($chatoptions, (int) $this->input->post('last_id'))){
                $this->session->set_flashdata('flashmsg', 'Chat options updated successfully.');
                $this->session->set_flashdata('msg', 'success');
            }
            else{
                $this->session->set_flashdata('flashmsg', 'You have no made any changes.');
                $this->session->set_flashdata('msg', 'info');
            }
          }
      }
      redirect(base_url('admin/chatoptions'));
  }

  public function createProduct(){

      $token = $this->getCode();

      $requests = array(
            array(
                "method" => "CREATESAAS",
                "retailer_id" => "P001",
                "data" => array(
                    "availability" => "in stock",
                    "brand" => "Niky", 
                    "category" => "t-shirts", 
                    "currency" => "USD", 
                    "description" => "This is the product description.", 
                    "image_url" => "http://www.images.example.com/t-shirts/1.png", 
                    "name" => "Test", 
                    "price" => "100", 
                    "url" => "http://www.example.com/t-shirts/1.html" 
                ) 
            )
        );

      $postData = array("requests" => $requests);

      $info = $this->curl("https://graph.facebook.com/v2.10/2016718211948689/batch?access_token=".$token,$postData,'POST');

      echo '<pre>';
      var_dump($info->validation_status);exit;

  }

  // Store search keyword in table
  public function saveSearchKeyword($searchdata=""){
      return $this->Search_prefrences_model->insert($searchdata);
  }

  // Get user's product lists
  public function productlistsByUserid($userID,$pageID="",$lang="english"){
      $data = array();
      $users = productlists_by_userid($userID);
      $data['title'] = 'Checkout';
      $data['users'] = $users;
      $data['userID'] = $userID;
      $data['language'] = $lang;
      $data['pageID'] = $pageID;
      $useraddress = $this->cache->get("deliver_address_{$userID}");
      if(!empty($useraddress)){
        $this->session->set_userdata('shipping_address',$useraddress);
        $this->session->set_userdata('redeemID',$this->cache->get("lastcouponID_{$userID}"));
      }else{
        $this->session->unset_userdata("shipping_address");
        $this->session->set_userdata('redeemID',$this->cache->get("lastcouponID_{$userID}"));  
      }
      $this->load->view('checkout', $data);
  }

  // Get 2c2p payment repsonse's
  public function paymentResponse($userID = "",$pageID = " ",$type=" ",$lang="english"){
      $data = array();
      $response = file_get_contents('php://input');
      //$discount = $this->cache->gtet("discountamount_{$userID}");
      $status = $_REQUEST["payment_status"];
     
      if($status == '000') // Success
      {   
          $products = productlists_by_userid($userID); // Get products ID's
          $Pids = array();
          $qty = array();
          $sum = 0;
          $price = 0;
          foreach ($products as $key => $value) {
              $Pids[] = $value->product_id;
              $qty[] = $value->qty;
              if($value->sale_price > 0){
                $price = $value->sale_price;
              }else{
                $price = $value->product_price;
              }
              $sum = $sum + ($price * $value->qty) + $value->shipping_cost;
          }

          if(!empty($this->session->userdata('redeemID'))){
            // Get last discount amount of user
            $discountuseramount = $this->db->select('id,total_discount')->from('coupon_redemptions')
              ->where('id',$this->session->userdata('redeemID'))
              ->get()
              ->row();

            $sum = ($sum) - ($discountuseramount->total_discount); // Final amount  
          }
         
          $shipdate = "";
          $shipstatus = "0";
          if($type == 'shipping'){
            $shipdate = date('Y-m-d h:i:s');
            $shipstatus = "1";
          }

          $orderInfo = array(
              "transaction_id"   => $_REQUEST["order_id"],
              "user_id"          => $userID,
              'page_id'          => $pageID,
              "shipping_address" => $this->session->userdata('shipping_address'),
              "order_amount"     => $sum ,
              "payment_method"   => "2c2p",
              //"status"           => "0",
              "status"           => "0",//$shipstatus
              "order_type"       => $type,
              "shipment_date"    => "",//$shipdate
              "transaction_ref"  => $_REQUEST['transaction_ref']
          );

          $saved = $this->Orders_model->insert($orderInfo); // Save order's

          foreach ($Pids as $key => $value) {
            // Save orders with products
            $this->Orders_products_model->insert(array('order_id' => $_REQUEST["order_id"],'product_id' => $value,'ordered_qty' => $qty[$key]));
          }

          if($saved){
            // Remove user shopping cart products
            $this->Shopping_carts_model->force_delete(array('user_id' => $userID));
          }

          // Update order id in redeemption table for user
          if(!empty($this->session->userdata('redeemID'))){
            $where = array('id' => $this->session->userdata('redeemID'));
            $this->db->where($where);
            $this->db->update('coupon_redemptions',array('order_id' => $_REQUEST["order_id"]));
          }
          
          /************* Send mail to user ****************/
          //$this->send($userID,$_REQUEST["order_id"]);  
          /***************** END **************************/

          $this->session->set_flashdata('flashmsg1', 'Success ! Payment has been successfully received.');
          $this->session->set_flashdata('msg', 'success');
      }else{
          $this->session->set_flashdata("flashmsg1", "{$_REQUEST['channel_response_desc']} ! Payment has been failed.");
          $this->session->set_flashdata('msg', 'error');
      }
      //$this->session->unset_userdata('shipping_address');
      $data['orders'] = $_REQUEST["order_id"];
      $data['status'] = $status;
      $data['title'] = 'Payment';
      $data['userid'] = $userID;
      $data['token'] = $this->access_token;
      $data['addressId'] = $this->session->userdata('shipping_address');
      $data['language'] = $lang;
      $data['pageID'] = $pageID;

      $this->load->view('paymentresponse', $data);
  }

  // Get 2c2p payment repsonse's
  public function paymentShippingResponse($userID = "",$pageID = " ",$type=" ",$lang="english"){
      $data = array();
      $response = file_get_contents('php://input');
      $status = $_REQUEST["payment_status"];
      $shipqty = $this->cache->get("shipqty_{$userID}");

      $this->access_token = "EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
     
      if($status == '000') // Success
      {   
          $products = $this->Shopping_carts_model->where(array('user_id' => $userID,'product_id' => 0))->get();

          $Pids = array();
          $sum = 0;

          $packagetypename = $this->cache->get("packagtypename_{$userID}");
          $pkgprice = $this->cache->get("packagepricem_{$userID}");
          $sum = $sum + ($pkgprice * $products->qty) + 0;

          $orderInfo = array(
              "transaction_id"   => $_REQUEST["order_id"],
              "user_id"          => $userID,
              'page_id'          => $pageID,
              "shipping_address" => $this->session->userdata('shipping_address'),
              "order_amount"     => $sum ,
              "payment_method"   => "2c2p",
              "status"           => "0",//1
              "order_type"       => $type,
              "shipment_date"    => "",//date('Y-m-d h:i:s') 
              "transaction_ref"  => $_REQUEST['transaction_ref']
          );

          $saved = $this->Orders_model->insert($orderInfo); // Save order's

            // Save orders with products
          $this->Orders_products_model->insert(array('order_id' => $_REQUEST["order_id"],'product_id' => 0,'ordered_qty' => $products->qty));

          // Shipping info stored
          $this->db->insert('shipping_packages', array('transaction_id' => $_REQUEST["order_id"],'package_type' => $packagetypename));

          if($saved){
            // Remove user shopping cart products
            $this->Shopping_carts_model->force_delete(array('user_id' => $userID,'product_id' =>0));
          }  

           /************* Send mail to user ****************/
          //$this->send($userID,$_REQUEST["order_id"]);  
          /***************** END **************************/

          $this->session->set_flashdata('flashmsg', 'Success ! Payment has been successfully received.');
          $this->session->set_flashdata('msg', 'success');
      }else{
          $this->session->set_flashdata("flashmsg", "{$_REQUEST['channel_response_desc']} ! Payment has been failed.");
          $this->session->set_flashdata('msg', 'error');
      }
      $data['orders'] = $_REQUEST["order_id"];
      $data['status'] = $status;
      $data['title'] = 'Payment';
      $data['userid'] = $userID;
      $data['token'] = $this->access_token;
      $data['addressId'] = $this->session->userdata('shipping_address');
      $data['language'] = $lang;
      $data['pageID'] = $pageID;
      $data['packageprice'] = $pkgprice;
      $data['shipqty'] = $shipqty;
      //$this->session->unset_userdata('shipping_address');
      $this->load->view('paymentshippingresponse', $data); 
  }

  public function send($userID,$orderID){

    $token = $this->getCode();

    $pagetoken = $this->getPageToken($token); //Refresh page token

    $name = '';

    /************** GET facebook user name ***************/
    $url = "https://graph.facebook.com/v2.10/$userID?access_token={$pagetoken}";
    $response = $this->curl($url,'','GET');

    $name = @$response->first_name;

    /********************** END *****************************/
  
    $myArray = array();
    $results = orderlists_by_userid($userID);
    
    $cost = 0;
    $subtotal = 0;
    $price = 0;    
    $message = "";
    $message .= "Hello {$name},<br><br>";
    $message .= "Your order hase been placed successfully and order number is <b>$orderID</b>.<br><br>";
    $message .= "Products details are below :-";
    $message .= "<br><br>";
    $table ="<table style='width:auto;border-collapse: collapse;'>
    <tr><th style='text-align:left;border: 1px solid #ddd;'>Name</th><th style='text-align:left;border: 1px solid #ddd;'>Qty</th><th style='text-align:left;border: 1px solid #ddd;'>Price</th></tr>";
    foreach ($results as $key => $value) {
      foreach ($value->products as $key1 => $value1){ 
        $cost = $cost + $value1->shipping_cost;
        if($value1->sale_price > 0){
          $price = $value1->sale_price;
        }else{
          $price = $value1->product_price;
        }
        $subtotal = $subtotal + ($price * $value1->ordered_qty);
        $grandTotal = $subtotal + $cost;
        $table.="<tr>";
        $table.="<td style='text-align:left;border: 1px solid #ddd;'>".$value1->product_name."</td>";
        $table.="<td style='text-align:left;border: 1px solid #ddd;'>".$value1->ordered_qty."</td>";
        $table.="<td style='text-align:left;border: 1px solid #ddd;'>".$price."</td>";  
        $table.="</tr>";
      }
    }
    $table.="<tr>";
    $table.="<th colspan=2 style='text-align:left;border: 1px solid #ddd;'>Subtotal</th>";
    $table.="<td colspan=1 style='text-align:left;border: 1px solid #ddd;'>{$subtotal}</td>";
    $table.="</tr>";
    $table.="<tr>";
    $table.="<th colspan=2 style='text-align:left;border: 1px solid #ddd;'>Shipping Cost</th>";
    $table.="<td colspan=1 style='text-align:left;border: 1px solid #ddd;'>{$cost}</td>";
    $table.="</tr>";
    $table.="<tr>";
    $table.="<th colspan=2 style='text-align:left;border: 1px solid #ddd;'>Grand Total </th>";
    $table.="<td colspan=1 style='text-align:left;border: 1px solid #ddd;'>{$grandTotal}</td>";
    $table.="</tr>";
    $table.="</table>";
    $message.=$table;
    $message.="<br>";
    $message.="Thanks, <br> Pointslive";

    $emailsettings = emailconfig();

    $ci = get_instance();
    $ci->load->library('email');
    $config['protocol'] = $emailsettings['protocol'];
    $config['smtp_host'] =$emailsettings['smtp_host'];
    $config['smtp_port'] = $emailsettings['smtp_port'];
    $config['smtp_user'] = $emailsettings['smtp_user']; 
    $config['smtp_pass'] = $emailsettings['smtp_pass'];
    $config['charset'] = $emailsettings['charset'];
    $config['mailtype'] = $emailsettings['mailtype'];
    $config['newline'] = "\r\n";

    $ci->email->initialize($config);

    $ci->email->from('webadmin@searchantive.com', $ci->config->item('site_name'));
    //$list = array('xxx@gmail.com');
    $ci->email->to('sachin.prajapati@searchnative.in');
    $ci->email->subject('Payment Confirmation');
    $ci->email->message($message);
    $ci->email->send(); 
    //echo exec('* * * * * C:\xampp\php\php.exe -f C:\xampp\htdocs\pointslive\cron.php');
    
  }

 // Get user's product lists on direct checkout
  public function checkoutproductlistsByUserid($userID="",$pageID = " ",$lang="english"){
      $data = array();
      $shipqty = $this->cache->get("shipqty_{$userID}");
      $price = $this->cache->get("packagprice_{$userID}");
      $users = $this->Shopping_carts_model->where(array('user_id' => $userID,'product_id' => 0))->get();
      $data['title'] = 'Checkout';
      $data['users'] = $users;
      $data['userID'] = $userID;
      $data['language'] = $lang;
      $data['pageID'] = $pageID;
      $data['shipqty'] = $shipqty;
      $data['shippackageprice'] = $this->cache->get("packagprice_{$userID}");
      $this->load->view('directcheckout', $data);
  }

  /*public function checkoutproductlistsByUserid($userID,$productid,$lang="english"){
    // echo $lang;exit;
      $data = array();
      $checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$userID}",'product_id'=>$productid))->get();
      if($checkexists){
        $updateqty = $checkexists->qty + 1; // Update quantity
        $update_data = array('qty'=>$updateqty);
        $this->Shopping_carts_model->where(array('user_id' => "{$userID}",'product_id' => $productid))->update($update_data); 
      }
      else{
        $carts = array(
          'user_id' => $userID,
          'product_id'  => $productid,
          'qty' => 1 
        );
        $this->Shopping_carts_model->insert($carts);
      }

      $users = productlists_by_userid($userID);

      $data['title'] = 'Checkout';
      $data['users'] = $users;
      $data['userID'] = $userID;
      $data['language'] = $lang;
      $this->load->view('directcheckout', $data);
  }*/

  // Get user facebook name using API
  public function getUserInfo($pageID,$userID){
    $token = $this->getCode();
    $this->access_token = $this->getPageToken($pageID,$token);
    $url = "https://graph.facebook.com/v2.10/$userID?access_token={$this->access_token}";
    $response = $this->curl($url,'','GET');
    return $response;
    //$name = @$response->first_name;
  }

   // Logistic API authentication and get access token
  public function validateAPI() {
    //API URL */
    $url = 'https://api.acommercedev.com/identity/token';
    //create a new cURL resource
    $ch = curl_init($url);
    //setup request to send json via POST
    $data = array(
        'username' => 'demoth1',
        'apiKey' => 'demoth1234!'
    );
    $payload = json_encode(array('auth' => array("apiKeyCredentials" => $data)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //attach encoded JSON string to the POST fields
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    //execute the POST request
    $result = curl_exec($ch);
    return json_decode($result, TRUE);  
  }

  // Logistic API authentication and get access token
  public static function acommerceValidateAPI() {
    //API URL */
    $url = 'https://api.acommercedev.com/identity/token';
    //create a new cURL resource
    $ch = curl_init($url);
    //setup request to send json via POST
    $data = array(
        'username' => 'demoth1',
        'apiKey' => 'demoth1234!'
    );
    $payload = json_encode(array('auth' => array("apiKeyCredentials" => $data)));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //attach encoded JSON string to the POST fields
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    //execute the POST request
    $result = curl_exec($ch);
    return json_decode($result, TRUE);  
  }

  // Send barcode image to chatbot
  public function sendChatbotImageAPI($token="",$page_id="",$userID="",$imageurl=""){//$filename - 08/02/2018
   // $pagetoken = $this->getPageToken($token,$page_id);
    /*$pagetoken = "EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr"; */

    /*$src = imagecreatefromstring(file_get_contents(FCPATH.'uploads/barcodes/'.$filename));
    $src_w = imagesx($src);
    $src_h = imagesy($src);
    // create destination image with dimensions increased from $src for borders.
    $dest_w = $src_w + 12;
    $dest_h = $src_h + 12;
    $dest = imagecreatetruecolor($dest_w, $dest_h);
    // draw white border (no need for black since new images default to that).
    imagerectangle($dest, 5, 5, $dest_w - 6, $dest_h - 6, 0x00ffffff);
    imagerectangle($dest, 4, 4, $dest_w - 5, $dest_h - 5, 0x00ffffff);
    imagecopy($dest, $src, 6, 6, 0, 0, $src_w, $src_h);
    $image_name_new = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
    $filenamenew = $image_name_new . '.' . 'png';
    //rename file name with random number
    $path = FCPATH.'uploads/barcodes/';
    imagepng($dest, $path.$filenamenew);
    imagedestroy($src);
    imagedestroy($dest);

    $imageurl = base_url()."uploads/barcodes/".$filenamenew; 
*/
    //echo $pagetoken;exit;
    $list = array(
        "attachment" => array(
          "type" => "image",
          "payload" => array(
            "url" => "{$imageurl}",
            "is_reusable" => true
          ) 
        )
    );
    $jsondata = array(
        "recipient" => array(
            "id" => "{$userID}",
        ),  
        'message' => $list
      );

    $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);  
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsondata));
    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);

    $passed = json_decode($result);
    return $passed;
  }

  // Send barcode image to chatbot while order dispatched by admin
  public static function sendImageToBotAPI($token="",$page_id="",$userID="",$imageurl=""){

    $ch = curl_init("https://graph.facebook.com/v2.10/{$page_id}?access_token={$token}&fields=access_token");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    //curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    //curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,2);
    $content = curl_exec($ch);
    $response = json_decode($content);
    curl_close($ch);

    $ptoken = $response->access_token;

    $list = array(
        "attachment" => array(
          "type" => "image",
          "payload" => array(
            "url" => "{$imageurl}",
            "is_reusable" => true
          ) 
        )
    );
    $jsondata = array(
        "recipient" => array(
            "id" => "{$userID}",
        ),  
        'message' => $list
      );

    $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$ptoken);  
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //Attach our encoded JSON string to the POST fields.
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsondata));
    //Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    $passed = json_decode($result);
    curl_close($ch);
    return $passed;
  }

  // Shipping orders generation flow for chatbot

  public function smartchipchat(){

    $total_rows_count = $this->Products_model->where('total_qty > ','0')->count_rows();
    $total_rows_country = $this->Packages_model->count_rows();

    $chatoptionsdata = chatoptions(); // Chat options 

    $input = json_decode(file_get_contents('php://input'), true);

    file_put_contents('fb_response.txt', file_get_contents("php://input") . PHP_EOL, FILE_APPEND);

    // Page ID of Facebook page which is sending message
    $page_id = $input['entry'][0]['id'];
    $time = $input['entry'][0]['time'];
    //var_dump($input);
    $sender = $input['entry'][0]['messaging'][0]['sender']['id'];

    // Get Message text if available
    $message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;

    $messagetext = strtolower($message);

    //$getfbmessage = @$input['entry'][0]['messaging'][0]['message']['attachments'];

    //var_dump($messagetext);

    if($messagetext == "0"){
        $messagetext = 1;
    }

    $quick_replies_opt = isset($input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']) ? $input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']: '' ;

    $quick_replies_opt = strtolower($quick_replies_opt);

    if(!is_null($quick_replies_opt) && $quick_replies_opt == 'payload_language'){
      $this->db->from('chat_languages');
      $this->db->where('user_id',$sender);  
      $set = $this->db->get()->result();
      if(count($set) == 0){
          $this->db->insert('chat_languages',array('language' => $messagetext,'user_id' => $sender));
      }
      else{
          $this->db->where("id",$set[0]->id);
          $this->db->update('chat_languages',array('language' => $messagetext)); 
      }
    }

    $language = chooselanguage($sender);
    if(sizeof($language) > 0){
      $lang = chooselanguage($sender);
      $this->language = $lang[0]->language;
    }
    else{
      $this->language = $messagetext;
    }

    lang_switcher($this->language);

    if($this->language == 'english'){
      $welcome = $chatoptionsdata[0]->welcome_msg;
      $faq_msg = $chatoptionsdata[0]->faq_msg;
      $dismiss_msg = $chatoptionsdata[0]->dismiss_msg;
      $feedback_msg = $chatoptionsdata[0]->feedback_msg;
      $product_confirm_msg = $chatoptionsdata[0]->product_confirm_msg;
      $order_cost_msg = $chatoptionsdata[0]->order_cost_msg;
      $payment_confirm_msg = $chatoptionsdata[0]->payment_confirm_msg;
      $findin_page = $chatoptionsdata[0]->findin_page;
    }
    else{
      $welcome = $chatoptionsdata[0]->welcome_msg_thai;
      $faq_msg = $chatoptionsdata[0]->faq_msg_thai;
      $dismiss_msg = $chatoptionsdata[0]->dismiss_msg_thai;
      $feedback_msg = $chatoptionsdata[0]->feedback_msg_thai;
      $product_confirm_msg = $chatoptionsdata[0]->product_confirm_msg_thai;
      $order_cost_msg = $chatoptionsdata[0]->order_cost_msg_thai;
      $payment_confirm_msg = $chatoptionsdata[0]->payment_confirm_msg_thai;
      $findin_page = $chatoptionsdata[0]->findin_page_thai;
    }

    // Get Postback payload if available
    $postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;

    $posttitle = isset($input['entry'][0]['messaging'][0]['postback']['title']) ? $input['entry'][0]['messaging'][0]['postback']['title']: '' ;

    $posttitletext = strtolower($posttitle);

    $this->access_token = "EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";

    $message_to_reply = '';
    $datas = array();

    if($messagetext || $postback){
        //file_put_contents('fb_response.txt', $quick_replies_opt, FILE_APPEND | LOCK_EX);
        if($messagetext){ 
              
            switch ($quick_replies_opt) {

              case 'payload_cart_check':
                    $services = $this->cache->get('services');
                    if($messagetext == strtolower($this->lang->line('yes'))){
                        $qty = $this->cache->get("userqty_{$sender}");
                        $pid = $this->cache->get("product_id_{$sender}");
                        $pname = $this->cache->get("product_title_{$sender}");

                        $checkqty = $this->Products_model->get(array('product_id' => $pid));

                        /* if($qty > $checkqty->total_qty){
                          $message_to_reply = "Requested quantity not available for {$pname}.";

                          $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply]
                          ];
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                        }*/
                        /*else{*/
                          $checkexists = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}",'product_id'=>$pid))->get();
                          if($checkexists){
                            $updateqty = $checkexists->qty + $qty; // Update quantity
                            $update_data = array('qty'=>$updateqty);
                            $updated = $this->Shopping_carts_model->where(array('user_id'=>"{$sender}",'product_id'=>$pid))->update($update_data);
                            if($updated){

                              //$availqty = $checkqty->total_qty - $qty;
                              //$updated_data = array('total_qty' => $availqty);
                                // update quantity
                             // $this->Products_model->where('product_id',$pid)->update($updated_data);

                              $message_to_reply = $this->lang->line('cartupdated'). " {$updateqty}";

                              $responseupdate = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply]
                              ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseupdate,'POST');

                              $options = array(
                                      0 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('continueshopping'),
                                          'payload' => "flag"
                                      ),
                                      1 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('updatecart'),
                                          'payload' => "PAY_LOAD_1"
                                      ),
                                      2 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('checkout'),
                                          'payload' => "PAY_LOAD_1"
                                      )
                                  ); 

                               $quickoptions = json_encode($options);

                               $message_to_reply = $this->lang->line('addbasket');

                               $response = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                               ];

                               $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                            }
                          }
                          else{
                           // $availqty = $checkqty->total_qty - $qty;
                            //$update_data = array('total_qty' => $availqty);
                            // update quantity
                            //$this->Products_model->where('product_id',$pid)->update($update_data);
                            $data = array(
                              'user_id'     => $sender,
                              'product_id'  => $pid,
                              'qty'         => $qty
                            );  

                            $ok = $this->Shopping_carts_model->insert($data);

                            if($ok){
                              $products = $this->Products_model->get(array('product_id' => $pid));

                              if($this->language == 'english'){
                                $replaceword = str_replace("(product_name)", "{$pname}", $product_confirm_msg);
                              }
                              else{
                                $replaceword = str_replace("(product_name)", "{$pname}", $payment_confirm_msg);
                              }

                              $message_to_reply = $replaceword;

                              $responsedata = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply]
                               ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedata,'POST');

                              if($products->sale_price > 0){
                                $price = $products->sale_price;
                              }else{
                                $price = $products->product_price;
                              }
                              
                              $totalprice = ($price * $qty);
                               if($this->language == 'english'){
                                $replacewordorder = str_replace("(no_of_unit)", "{$qty} ", $order_cost_msg);
                                $replacewordorder = str_replace("(total_price)", "{$totalprice}", $replacewordorder);
                               }
                               else{
                                $replacewordorder = str_replace("(no_of_unit)", "{$qty} ", $order_cost_msg);
                                $replacewordorder = str_replace("(total_price)", "{$totalprice}", $replacewordorder);
                               }

                              $message_to_reply = $replacewordorder;

                              $responsedatas = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply]
                               ];
                              $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responsedatas,'POST');

                              $options = array(
                                       0 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('continueshopping'),
                                          'payload' => "flag"
                                      ),
                                      1 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('updatecart'),
                                          'payload' => "PAY_LOAD_1"
                                      ),
                                      2 => array(
                                          'content_type' => 'text',
                                          'title' => $this->lang->line('checkout'),
                                          'payload' => "PAY_LOAD_1"
                                      )
                                  ); 

                             $quickoptions = json_encode($options);

                             $message_to_reply = $this->lang->line('addbasket');

                             $response = [
                                  'recipient' => [ 'id' => $sender ],
                                  'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                             ];
                             $this->cache->save('services',$services,600);
                            $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                              exit; 
                        }
                      }
                    }
                    if($messagetext == strtolower($this->lang->line('no'))){

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                      $options = array(
                          0 => array(
                            'content_type' => 'text',
                            'title' =>$this->lang->line('yes'),
                            'payload' => "flag"
                          ),
                          1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "flag"
                          )
                        ); 

                       $quickoptions = json_encode($options);
                       $message_to_reply = $findin_page ? $findin_page : $this->lang->line('still');
                     
                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];
                      $this->cache->save('services',$services,600);
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
                    $this->cache->delete("userqty_{$sender}");
                    $this->cache->delete("product_id_{$sender}");
                    $this->cache->delete("product_title_{$sender}");
              break;
              case 'payload_checkout_cart':
                    if($messagetext == strtolower($this->lang->line('yes'))){
                      // Checkout process with shopping list
                      $data = productlists_by_userid($sender);
                      $table = '';
                      $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                      $table.="----------------------------------";
                      $table.="\n";
                      $sum = 0;
                      $shipping = 0;
                      $total = 0;
                      $price = 0;
                      foreach ($data as $key => $value) {
                          if($this->language == 'english'){
                            $name = $value->product_name;
                          }
                          else{
                            $name = $value->product_name_thai;
                          }
                          if(strlen($name) > 6){
                            $table.= wordwrap($name, 6, "\n", true);
                            $table.="\t";
                          }else{
                            $table.=$name."\t";
                            //$table.="\t\t\t";
                          }
                          if($value->sale_price > 0){
                            $price = $value->sale_price;
                          }else{
                            $price = $value->product_price;
                          }

                          $table.="\t".$value->qty."\t";
                          $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                          $sum = $sum + ($value->qty * $price);
                          $shipping = $shipping + $value->shipping_cost;
                          $table.="----------------------------------";
                          $table.="\n";
                      }
                      $table.="----------------------------------";
                      $table.="\t\n";
                      $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                      $table.="\t\n";
                      $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                      $table.="\t\n";
                      $total = $sum + $shipping;
                      $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                      $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$table,
                            "buttons"=>[
                              [
                                "type"=>"web_url",
                                "url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}{$this->language}",
                                "title"=>$this->lang->line('checkout'),
                                "webview_height_ratio"=>"full",
                                "messenger_extensions"=> true,
                                "fallback_url"=>base_url()."admin/productlistsByUserid/{$sender}/{$page_id}/{$this->language}" 
                                //base_url()."admin/productlistsByUserid/{$sender} 
                              ]
                            ]
                          ]
                        ]];
                      
                      $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer
                      ];

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    }
                    if($messagetext == strtolower($this->lang->line('no'))){

                      $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome: 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('counterservice'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('sellerpickup')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                     $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $answer 
                     ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }
              break;

              case 'pay_load_delivery_speed':

                    if($messagetext == strtolower($this->lang->line('delivery_nextday')) || strtolower($this->lang->line('delivery_express')) || strtolower($this->lang->line('delivery_standard'))) {

                      $shipqty = $this->cache->get("shipqty_{$sender}");

                      $packagetype = $this->cache->get("packagtype_{$sender}");
                      $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                      $message_to_reply = $this->lang->line('delivery_shippingpostcode');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];

                      $this->cache->save("shipqty_{$sender}",$shipqty,300); // 3 minutes
                      $this->cache->save("packagtype_{$sender}",$packagetype,300); // 3 minutes
                      $this->cache->save("typeprice_{$sender}",$packagtypeprice,300); // 3 minutes
                      $this->cache->save("delivery_shippingmethod_{$sender}",$messagetext,300); // 3 minutes
                      $this->cache->save("delivery_shippingpostcode_{$sender}",'postcode',300); // 2 minutes

                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                   }
              break;

              case 'pay_load_time':

                    $times = $this->getOtherPageLocationHours($page_id); // Get page details
                    
                    $text = '';

                    if(isset( $times->hours)){
                      
                      $message_to_reply = (array) $times->hours;
                      foreach ($message_to_reply as $key => $value) {
                          $text.= $key.':-';
                          $text.=$value;
                          $text.="\n";
                      }
                    }
                    else{
                      $text = $this->lang->line('sorry');
                    }

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $text]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                   
                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=> $welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('counterservice'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('sellerpickup')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $responses = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responses,'POST');  
                exit;
              break;

              case 'pay_load_order':

                  $orderlists = orderlists_by_userid($sender);
                  if(count($orderlists > 0)){
                    $myArray = array();

                    $table = '';
                    $table.="\n# {$this->lang->line('shiporder')}\t\t{$this->lang->line('shipamt')}\t\t{$this->lang->line('shipstatus')}\n";
                    $table.="-------------------------------------------------------------";
                    $table.="\n";
                    $st = "";
                  foreach ($orderlists as $key => $value) {
                      $table.=$value->transaction_id."\t";
                      /*foreach ($value->products as $key1 => $value1){ 
                          $myArray[$key][$key1] = $value1->product_name;
                      }
                      $table.=implode(', ', $myArray[$key] );*/
                      if($value->status == 0){ // 0 -23-11-2017
                        $st = "{$this->lang->line('orderprocess')}";
                      }
                      if($value->status == 1){
                        $st = "{$this->lang->line('orderdispatch')}";
                      }
                      if($value->status == 2){
                        $st = "{$this->lang->line('ordercomplete')}";
                      }
                      if($value->status == 3){
                        $st = "{$this->lang->line('ordercan')}";
                      }
                      if($value->status == 4){
                        $st = "{$this->lang->line('orderrej')}";
                      }
                      if($value->status == 5){
                        $st = "{$this->lang->line('orderfail')}";
                      }
                      /*if($value->status == 3){ -23-11-2017
                        $st = "{$this->lang->line('ordercan')}";
                      }
                      if($value->status == 99){ -23-11-2017
                        $st = "{$this->lang->line('orderfail')}";
                      }*/
                      $table.=$value->order_amount."\t\t";
                      $table.=$st."\n";
                  }
                    $table.="-------------------------------------------------------------";
                  
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $table]
                    ];
                  }
                  else{
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => "No order found."]
                    ];
                  }
        
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  exit;
              break;
              case 'pay_load_office':
            
                    $locations = $this->getOtherPageLocationHours($page_id); // Get page location details
                    
                    if(isset($locations->single_line_address)){
                      $message_to_reply = $locations->single_line_address;
                    }
                    else{
                      $message_to_reply = $this->lang->line('noaddress');
                    }
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  
                    $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$this->lang->line('moreopt'),
                            "buttons"=>[
                              /*[
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('aboutpage'),
                              ],*/
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('faq')
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('endchat')
                              ]
                            ]
                          ]
                        ]];

                     $responses = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => $answer 
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responses,'POST');
                    exit;
              break;

              // 08-02-2018 tayo feedback
              case 'pay_load_aboutus':
                    $token = "1733222426709661|hkJN_Vy-PlG-tLWD0w_NnZC5KW8";

                    $content = $this->curl("https://graph.facebook.com/{$page_id}?fields=about,name&access_token=".$token,'','GET'); 
                      //curl_close($ch);  
                    $message_to_reply = $content->about;

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  
                    /*************************** Welcome loop ****************************/
                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('counterservice'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('sellerpickup')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
              break;

              case 'pay_load_issue':

                    $message_to_reply = $this->lang->line('whatisit');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->cache->save("pagequestion_{$sender}",'usersearch');

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    exit;
              break;

              case 'payload_language':

                    $this->cache->delete("saveorderid_{$sender}");
                    $this->cache->delete("shipqty_{$sender}");
                    $this->cache->delete("shipping_district_{$sender}");
                    $this->cache->delete("shipping_postcode_{$sender}");
                    $this->cache->delete("shipping_deliveryaddress_{$sender}");
                    $this->cache->delete("delivery_shippingmethod_{$sender}"); 
                    $this->cache->delete("packagtype_{$sender}"); 
                    $this->cache->delete("packagprice_{$sender}");
                    $this->cache->delete("shipping_phoneno_{$sender}"); 
                    $this->cache->delete("shipping_shippemail_{$sender}");
                    $this->cache->delete("delivery_shippemail_{$sender}"); 
                    $this->cache->delete("delivery_shippingpostcode_{$sender}");
                    $this->cache->delete("shippingpostcode_{$sender}");
                    $this->cache->delete("packagtypename_{$sender}");
                    $this->cache->delete("typeprice_{$sender}");
                    $this->cache->delete("shippingcountry_{$sender}");
                    $this->cache->delete("packagepricem_{$sender}");
                    $this->cache->delete("shipername_{$sender}");
                    
                    $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                              "template_type"=>"button",
                              "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                              "buttons"=>[
                                [
                                  "type"=>"postback",
                                  "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                  "title" => $this->lang->line('counterservice'), //What's Hot
                                ],
                                [
                                  "type"=>"postback",
                                  "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                  "title" => $this->lang->line('sellerpickup')
                                ],
                                [
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('viewmore'),
                                  "payload"=>"PAY_LOAD_1"
                                ]
                              ]
                            ]
                          ]];

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                       ];
                       //file_put_contents('fb_response.txt', $this->access_token, FILE_APPEND | LOCK_EX);  
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              break;

              // 10-01-2018 updated chatbot flow 
            
              case 'delivery_shipping_district':

                  $postcode = $this->cache->get("shippingpostcode_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}");
                  $packagtype = $this->cache->get("packagtype_{$sender}");
                  $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                  $shipqty = $this->cache->get("shipqty_{$sender}");

                  $message_to_reply = $this->lang->line('enter_shipername');;//$this->lang->line('choose_shipaddress');
                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                  ];
                  
                  $this->cache->save("shipping_district_{$sender}",$messagetext,180); // 3 minutes
                  $this->cache->save("shipping_postcode_{$sender}",$postcode,180); // 2 minutes
                  //$this->cache->save('shipping_address','shipaddress',180); // 3 minutes -15-02-2018
                  $this->cache->save("shipping_name_{$sender}",'recivername',300);
                  $this->cache->save("delivery_shippingmethod_{$sender}",$deliverymethod,180); // 3 minutes
                  $this->cache->save("packagtype_{$sender}",$packagtype,300);
                  $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                  $this->cache->save("shipqty_{$sender}",$shipqty,300);
                  
                  //$this->cache->delete('delivery_shippingpostcode'); 

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              break;

              case 'payload_payment_counter':
                    $packagtype = $this->cache->get("packagtype_{$sender}");
                    $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                    $shipername = $this->cache->get("shipername_{$sender}");
                    $shipqty = $this->cache->get("shipqty_{$sender}");
                    $data = productlists_by_userid($sender);
                    
                    $table = '';
                    $table.="\n{$this->lang->line('pname')}\t{$this->lang->line('pqty')}\t{$this->lang->line('pprice')}\n";
                    $table.="----------------------------------";
                    $table.="\n";
                    $sum = 0;
                    $shipping = 0;
                    $total = 0;

                    if(count($data) > 0){
                        foreach ($data as $key => $value) {
                        if($this->language == 'english'){
                          $name = $value->product_name;
                        }
                        else{
                          $name = $value->product_name_thai;
                        }
                        if(strlen($name) > 6){
                          $table.= wordwrap($name, 6, "\n", true);
                          $table.="\t";
                        }else{
                          $table.=$name."\t";
                          //$table.="\t\t\t";
                        }
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }

                        $table.="\t".$value->qty."\t";
                        $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.$value->shipping_cost."\n";
                        $sum = $sum + ($value->qty * $price);
                        $shipping = $shipping + $value->shipping_cost;
                        $table.="----------------------------------";
                        $table.="\n";
                      }
                    }
                    else{
                      $datas = $this->Shopping_carts_model->where(array('user_id' => $sender,'product_id' => 0))->get();
                      //echo $this->db->last_query();
                     // $datas = $this->Shopping_carts_model->where(array('user_id' => $sender))->get();
                      if($packagtype == 'small' || $packagtype == 'เล็ก'){
                        $price = $packagtypeprice;
                      }
                      if($packagtype == 'medium' || $packagtype == 'กลาง'){
                        $price = $packagtypeprice;
                      }
                      if($packagtype == 'large' || $packagtype == 'ใหญ่'){
                        $price = $packagtypeprice;
                      }

                      $this->cache->save("packagprice_{$sender}",$price,300); // 5 minute
                      $this->cache->delete("typeprice_{$sender}");
                      $this->cache->save("shipqty_{$sender}",$shipqty,300); // 5 minute
                      $this->cache->save("shipername_{$sender}",$shipername,300); // 5 minute

                      $table.= ucfirst($packagtype)."\t";
                      $table.=$datas->qty."\t";
                      $table.='฿'.$price." + {$this->lang->line('shipcost')} ".'฿'.@$shipping."\n";
                      $sum = $sum + ($datas->qty * $price);
                      $shipping = $shipping + @$value->shipping_cost;
                      $table.="\n";
                    }

                    $table.="----------------------------------";
                    $table.="\t\n";
                    $table.="{$this->lang->line('subtotal')} = ".'฿'.$sum;
                    $table.="\t\n";
                    $table.="{$this->lang->line('shipcharge')} = ".'฿'.$shipping;
                    $table.="\t\n";
                    $total = $sum + $shipping;
                    $table.="{$this->lang->line('gt')} = ".'฿'.$total;

                    if($messagetext == strtolower($this->lang->line('delivery_paycash'))){
                      $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                              "template_type"=>"button",
                              "text"=>$table,
                              "buttons"=>[
                                [
                                  "type"=>"postback",
                                  "title"=>$this->lang->line('paybtn'),
                                  "payload"=>"Payment_Counter_Cash"
                                ]
                              ]
                            ]
                          ]];
                    }

                    if($messagetext == strtolower($this->lang->line('delivery_payprepaid'))){
                        //file_put_contents('fb_response.txt', $messagetext, FILE_APPEND | LOCK_EX);  
                        //$this->cache->save('packagprice',$price,300); // 5 minute
                        $this->cache->save("packagtype_{$sender}",$packagtype,300); // 5 minute
                  
                        $district = $this->cache->get("shipping_district_{$sender}");
                        $postcode = $this->cache->get("shipping_postcode_{$sender}");
                        $addressdata = $this->cache->get("shipping_deliveryaddress_{$sender}");
                        $deliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}"); 
                        $this->cache->save("shipqty_{$sender}",$shipqty,300); // 5 minute
                        if($this->language == 'english'){
                          $shipcountry = "Thailand";
                        }
                        else{
                          $shipcountry = "ประเทศไทย";
                        }
                        //$shipcountry = $this->cache->get('delivery_shippingcountry'); 
                        $shipphone = $this->cache->get("shipping_phoneno_{$sender}"); 
                        $shipemail = $this->cache->get("shipping_shippemail_{$sender}");  
       
                        $info = array(
                            'user_id'   => $sender,
                            'postcode'  => $postcode,
                            'district'  => $district,
                            'country'   => $shipcountry,
                            'receiver_name' => $shipername,
                            'phone'     => $shipphone,
                            'email'     => $shipemail,
                            'delivery_method'  =>$deliverymethod,
                            'shipping_address' => $addressdata
                          );

                        $addressID = $this->Address_model->insert($info);

                           $answer = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"button",
                                  "text"=>$table,
                                  "buttons"=>[
                                    [
                                        "type"=>"web_url",
                                        "url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$page_id}/{$this->language}",
                                        "title"=>"Checkout",
                                        "webview_height_ratio"=>"full",
                                        "messenger_extensions"=> true,
                                        "fallback_url"=>base_url()."admin/checkoutproductlistsByUserid/{$sender}/{$page_id}/{$this->language}"
                                    ]
                                  ]
                                ]
                              ]];
                        }
                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer //$answer
                  ];
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              break;

              case 'pay_load_package':

                  $details = array(
                    'user_id' => $sender,
                    'product_id' => 0,
                    'qty' => 1
                    );

                  $check = $this->Shopping_carts_model->where(array('user_id' => $sender,'product_id' =>0))->get();
                  
                  $shippingcountries = $this->cache->get("shippingcountry_{$sender}");

                  $packages = $this->Packages_model->get_by_Name($shippingcountries,$this->language);

                  if($messagetext == 'small' || $messagetext == 'เล็ก'){
                    $pkprice = $packages->package_small_weight;
                  }
                  else if($messagetext == 'medium' || $messagetext == 'กลาง'){
                    $pkprice = $packages->package_medium_weight;
                  }
                  else if($messagetext == 'large' || $messagetext == 'ใหญ่'){
                    $pkprice = $packages->package_large_weight;
                  }

                  if($check){

                    $packagtype = $this->cache->get("packagtype_{$sender}");
                    $packagtypeprice = $this->cache->get("typeprice_{$sender}");

                    $message_to_reply  = $this->lang->line('pkexists');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];
                    
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

                    $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_nextday'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_express'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        2 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_standard'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        )
                    ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply  = $this->lang->line('delivery_speed');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->cache->save("packagtype_{$sender}",$packagtype,600); // 5 minutes
                  $this->cache->save("typeprice_{$sender}",$packagtypeprice,600); // 5 minutes
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

                  }else{

                  $added = $this->Shopping_carts_model->insert($details);  

                  $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('yes'),
                            'payload' => "payload_shipping"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('no'),
                            'payload' => "payload_shipping"
                        )
                    ); 

                  $quickoptions = json_encode($options);
                  $message_to_reply = $this->lang->line('wantshipmore');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ]; 

                  $this->cache->save("packagtype_{$sender}",$messagetext,600); // 5 minutes
                  $this->cache->save("typeprice_{$sender}",$pkprice,600); // 5 minutes
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }
              break;

              case 'payload_shipping':
                $packagetype = $this->cache->get("packagtype_{$sender}"); 
                $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                if($messagetext == strtolower($this->lang->line('yes'))){

                  $message_to_reply = $this->lang->line('shippackage');

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                   ];

                  $this->cache->save("packagtype_{$sender}",$packagetype,600); // 5 minutes
                  $this->cache->save("typeprice_{$sender}",$packagtypeprice,600);
                  $this->cache->save("shipquantity_{$sender}",'shipqty',600); // 5 minutes 

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                }
                if($messagetext == strtolower($this->lang->line('no'))){

                  $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_nextday'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_express'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        2 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_standard'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        )
                    ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply  = $this->lang->line('delivery_speed');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];
                  $this->cache->save("packagtype_{$sender}",$packagetype,600); // 5 minutes
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
              break; 

             /* case 'pay_load_package':

                  $details = array(
                    'user_id' => $sender,
                    'product_id' => 0,
                    'qty' => 1
                    );

                  $check = $this->Shopping_carts_model->where(array('user_id' => $sender,'product_id' =>0))->get();
                  
                  if($check){

                    $packagtype = $this->cache->get('packagtype');

                    $message_to_reply  = $this->lang->line('pkexists');

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];
                    
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

                    $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_nextday'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_express'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        2 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_standard'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        )
                    ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply  = $this->lang->line('delivery_speed');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->cache->save('packagtype',$packagtype,600); // 5 minutes
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

                  }else{

                  $added = $this->Shopping_carts_model->insert($details);   

                   $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_nextday'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_express'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        ),
                        2 => array(
                            'content_type' => 'text',
                            'title' => $this->lang->line('delivery_standard'),
                            'payload' => "PAY_LOAD_DELIVERY_SPEED"
                        )
                    ); 

                  $quickoptions = json_encode($options);

                  $message_to_reply  = $this->lang->line('delivery_speed');

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];
                  $this->cache->save('packagtype',$messagetext,600); // 5 minutes
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }
              break; */ 

              default:

                if(in_array($messagetext, array('hi','akin','hey','hello','สวัสดี','คล้ายกัน','เฮ้','สวัสดี'))) {
                  //die("Sdfsfdsf");
                   $options = array(
                        0 => array(
                            'content_type' => 'text',
                            'title' => "English",
                            'payload' => "payload_language"
                        ),
                        1 => array(
                            'content_type' => 'text',
                            'title' => "Thai",
                            'payload' => "payload_language"
                        )
                    ); 

                  $quickoptions = json_encode($options);
                  $message_to_reply = 'Please choose language.';

                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                  ];

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
              }
            else if($messagetext == strtolower($this->lang->line('no'))){

                  $message_to_reply = $dismiss_msg ? $dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                   ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');   
                exit;
            }
            else
            {  
                // updated 10-01-2018 chatbot flow by sachin
                  if(!empty($this->cache->get("shipping_name_{$sender}"))){

                    $shippingname = $messagetext;
                    $shippingdistrict = $this->cache->get("shipping_district_{$sender}"); // get district
                    $shippingpostcode = $this->cache->get("shipping_postcode_{$sender}"); // get postcode
                    $shippingdeliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}"); // 2 minutes
                    $packagtype = $this->cache->get("packagtype_{$sender}");
                    $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                    $shipqty = $this->cache->get("shipqty_{$sender}");
                    // $message_to_reply = $this->lang->line('enter_country'); 16-01-2018

                    $message_to_reply = $this->lang->line('choose_shipaddress');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->cache->save("shipping_district_{$sender}",$shippingdistrict,300); // 3 minutes
                    $this->cache->save("shipping_postcode_{$sender}",$shippingpostcode,300); // 3 minutes
                    $this->cache->save("shipping_deliveryaddress_{$sender}",$shippingaddress,300); // 3 minutes
                    $this->cache->save("delivery_shippingmethod_{$sender}",$shippingdeliverymethod,300); // 3 minutes
                    $this->cache->save("packagtype_{$sender}",$packagtype,300);
                    $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                    $this->cache->save("shipqty_{$sender}",$shipqty,300);
                    $this->cache->save("shipername_{$sender}",$shippingname,300);
                    //$this->cache->save('delivery_shipcountry',"validcountry",300); // 5 minutes
                    $this->cache->save("shipping_address_{$sender}",'shipaddress',300);
                    //$this->cache->save('delivery_shipphone','phonevalid',300);
                    //$this->cache->delete('shipping_address');
                    $this->cache->delete("shipping_name_{$sender}");
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                  }
                  else if(!empty($this->cache->get("shipping_address_{$sender}"))){

                  $shippingaddress = $messagetext;
                  $shippingdistrict = $this->cache->get("shipping_district_{$sender}"); // get district
                  $shippingpostcode = $this->cache->get("shipping_postcode_{$sender}"); // get postcode
                  $shippingdeliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}"); // 2 minutes
                  $packagtype = $this->cache->get("packagtype_{$sender}");
                  $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                  $shipqty = $this->cache->get("shipqty_{$sender}");
                  $shipername = $this->cache->get("shipername_{$sender}");
                  // $message_to_reply = $this->lang->line('enter_country'); 16-01-2018

                  $message_to_reply = $this->lang->line('enter_phone');

                  $response = [
                    'recipient' => [ 'id' => $sender ],
                    'message' => [ 'text' => $message_to_reply]
                  ];

                  $this->cache->save("shipping_district_{$sender}",$shippingdistrict,300); // 3 minutes
                  $this->cache->save("shipping_postcode_{$sender}",$shippingpostcode,300); // 3 minutes
                  $this->cache->save("shipping_deliveryaddress_{$sender}",$shippingaddress,300); // 3 minutes
                  $this->cache->save("delivery_shippingmethod_{$sender}",$shippingdeliverymethod,300); // 3 minutes
                  $this->cache->save("packagtype_{$sender}",$packagtype,300);
                  $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                  $this->cache->save("shipqty_{$sender}",$shipqty,300);
                  $this->cache->save("shipername_{$sender}",$shipername,300);
                  //$this->cache->save('delivery_shipcountry',"validcountry",300); // 5 minutes

                  $this->cache->save("delivery_shipphone_{$sender}",'phonevalid',300);

                  $this->cache->delete("shipping_address_{$sender}");
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else if(!empty($this->cache->get("shipquantity_{$sender}"))){
                  
                  $packagetype = $this->cache->get("packagtype_{$sender}"); 
                  $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                  $shipqty = (int)$messagetext;

                  if($shipqty > 10){
                      $message_to_reply  = $this->lang->line('pkglimit');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                      $this->cache->save("packagtype_{$sender}",$packagetype,600);
                      $this->cache->save("typeprice_{$sender}",$packagtypeprice,600);
                      $this->cache->save("shipquantity_{$sender}",'shipqty',600);
                  }else{

                    $options = array(
                          0 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_nextday'),
                              'payload' => "PAY_LOAD_DELIVERY_SPEED"
                          ),
                          1 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_express'),
                              'payload' => "PAY_LOAD_DELIVERY_SPEED"
                          ),
                          2 => array(
                              'content_type' => 'text',
                              'title' => $this->lang->line('delivery_standard'),
                              'payload' => "PAY_LOAD_DELIVERY_SPEED"
                          )
                      ); 

                      $quickoptions = json_encode($options);

                      $message_to_reply  = $this->lang->line('delivery_speed');

                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                      ];
                   /* }*/
                    $reaminqty = $shipqty - 1;
                    //file_put_contents('test.txt',$reaminqty, FILE_APPEND | LOCK_EX);
                    $this->cache->save("packagtype_{$sender}",$packagetype,300);
                    $this->cache->save("typeprice_{$sender}",$packagtypeprice,600);
                    $this->cache->save("shipqty_{$sender}",$reaminqty,300);
                    $this->cache->delete("shipquantity_{$sender}");

                  }
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                  /*$check = $this->Shopping_carts_model->where(array('user_id' => $sender,'product_id' =>0))->get();

                  if($check){

                    $updated = $this->Shopping_carts_model->where(array('user_id'=>$sender,'product_id'=>0))->update(array('qty' => $shipqty));

                    if($updated){*/
                      
                 /* }*/
                }
                else if(!empty($this->cache->get("delivery_shipphone_{$sender}"))){

                  $shippingphone = $messagetext;

                 // $shippingcountry = $this->cache->get('delivery_shippingcountry');
                  $shippingdistrict = $this->cache->get("shipping_district_{$sender}"); // get district
                  $shippingpostcode = $this->cache->get("shipping_postcode_{$sender}"); // get postcode
                  $shippingdeliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}"); // 2 minutes
                  $shippingaddress = $this->cache->get("shipping_deliveryaddress_{$sender}"); // 2 minutes
                  $packagtype = $this->cache->get("packagtype_{$sender}");
                  $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                  $shipqty = $this->cache->get("shipqty_{$sender}");
                  $shipername = $this->cache->get("shipername_{$sender}");
                  $number_lngth = strlen($shippingphone);
                  //if(is_numeric($shippingphone)){

                      if (preg_match("~^0\d+$~", $shippingphone) && $number_lngth == 10){
                        //file_put_contents('fb_response.txt', $shippingphone, FILE_APPEND | LOCK_EX);
                        $message_to_reply = $this->lang->line('enter_email');

                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                        ];
                        $this->cache->save("shipping_district_{$sender}",$shippingdistrict,300); // 3 minutes
                        $this->cache->save("shipping_postcode_{$sender}",$shippingpostcode,300); // 3 minutes
                        $this->cache->save("shipping_deliveryaddress_{$sender}",$shippingaddress,300); // 3 minutes
                        $this->cache->save("delivery_shippingmethod_{$sender}",$shippingdeliverymethod,300); // 3 minutes
                       // $this->cache->save('delivery_shippingcountry',$shippingcountry,300); // 3 minutes
                        $this->cache->save("shipping_phoneno_{$sender}",$shippingphone,300); // 3 minutes
                        $this->cache->save("packagtype_{$sender}",$packagtype,300);
                        $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                        $this->cache->save("shipqty_{$sender}",$shipqty,300);
                        $this->cache->save("shipername_{$sender}",$shipername,300);
                        $this->cache->save("delivery_shippemail_{$sender}",'shipemail',300); // 2 minutes
                        $this->cache->delete("delivery_shipphone_{$sender}");

                      }else{

                        $message_to_reply = $this->lang->line('errphonenum');

                        $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                        ];
                        $this->cache->save("shipping_district_{$sender}",$shippingdistrict,300); // 3 minutes
                        $this->cache->save("shipping_postcode_{$sender}",$shippingpostcode,300); // 3 minutes
                        $this->cache->save("shipping_deliveryaddress_{$sender}",$shippingaddress,300); // 3 minutes
                        $this->cache->save("delivery_shippingmethod_{$sender}",$shippingdeliverymethod,300); // 3 minutes
                       // $this->cache->save('delivery_shippingcountry',$shippingcountry,300); // 3 minutes
                        $this->cache->save("shipqty_{$sender}",$shipqty,300);
                        $this->cache->save("shipername_{$sender}",$shipername,300);
                        $this->cache->save("packagtype_{$sender}",$packagtype,300);
                        $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                        $this->cache->save("delivery_shipphone_{$sender}",'phonevalid',300);
                      }
                  /*}
                  else{

                      $message_to_reply = $this->lang->line('errphone');

                      $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                      ];
                      $this->cache->save('shipping_district',$shippingdistrict,300); // 3 minutes
                      $this->cache->save('shipping_postcode',$shippingpostcode,300); // 3 minutes
                      $this->cache->save('shipping_deliveryaddress',$shippingaddress,300); // 3 minutes
                      $this->cache->save('delivery_shippingmethod',$shippingdeliverymethod,300); // 3 minutes
                     // $this->cache->save('delivery_shippingcountry',$shippingcountry,300); // 3 minutes
                      $this->cache->save('shipqty',$shipqty,300);
                      $this->cache->save('shipername',$shipername,300);
                      $this->cache->save('packagtype',$packagtype,300);
                      $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                      $this->cache->save('delivery_shipphone','phonevalid',300);
                  }*/
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else if(!empty($this->cache->get("delivery_shippemail_{$sender}"))){

                  $shippingemail = $messagetext;
                  $shippingphone = $this->cache->get("shipping_phoneno_{$sender}");
                  //$shippingcountry = $this->cache->get('delivery_shippingcountry');
                  $shippingdistrict = $this->cache->get("shipping_district_{$sender}"); // get district
                  $shippingpostcode = $this->cache->get("shipping_postcode_{$sender}"); // get postcode
                  $shippingdeliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}"); // get method
                  $shippingaddress = $this->cache->get("shipping_deliveryaddress_{$sender}"); // get address
                  $packagtype = $this->cache->get("packagtype_{$sender}");
                  $packagtypeprice = $this->cache->get("typeprice_{$sender}");
                  $shipqty = $this->cache->get("shipqty_{$sender}");
                  $shipername = $this->cache->get("shipername_{$sender}");

                  if (filter_var($shippingemail, FILTER_VALIDATE_EMAIL)) {

                    $options = array(
                      0 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('delivery_paycash'),
                          'payload' => "payload_payment_counter"
                      ),
                      1 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('delivery_payprepaid'),
                          'payload' => "payload_payment_counter"
                      )
                    ); 

                    $quickoptions = json_encode($options);

                    $message_to_reply = $this->lang->line('pay_counter');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                    ];

                    $this->cache->save("shipping_district_{$sender}",$shippingdistrict,300); // 3 minutes
                    $this->cache->save("shipping_postcode_{$sender}",$shippingpostcode,300); // 3 minutes
                    $this->cache->save("shipping_deliveryaddress_{$sender}",$shippingaddress,300); // 3 minutes
                    $this->cache->save("delivery_shippingmethod_{$sender}",$shippingdeliverymethod,300); // 3 minutes
                    //$this->cache->save('delivery_shippingcountry",$shippingcountry,300); // 3 minutes
                    $this->cache->save("shipping_phoneno_{$sender}",$shippingphone,300); // 3 minutes

                    $this->cache->save("shipping_shippemail_{$sender}",$shippingemail,300); // 3 minutes
                    $this->cache->save("packagtype_{$sender}",$packagtype,300);
                    $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                    $this->cache->save("shipqty_{$sender}",$shipqty,300);
                    $this->cache->save("shipername_{$sender}",$shipername,300);
                   
                  }
                  else{
                    $message_to_reply = $this->lang->line('erremail');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->cache->save("shipping_district_{$sender}",$shippingdistrict,300); // 3 minutes
                    $this->cache->save("shipping_postcode_{$sender}",$shippingpostcode,300); // 3 minutes
                    $this->cache->save("shipping_deliveryaddress_{$sender}",$shippingaddress,300); // 3 minutes
                    $this->cache->save("delivery_shippingmethod_{$sender}",$shippingdeliverymethod,300); // 3 minutes
                    //$this->cache->save('delivery_shippingcountry',$shippingcountry,300); // 3 minutes
                    $this->cache->save("shipping_phoneno_{$sender}",$shippingphone,300); // 3 minutes
                    $this->cache->save("packagtype_{$sender}",$packagtype,300);
                    $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                    $this->cache->save("shipqty_{$sender}",$shipqty,300);
                    $this->cache->save("shipername_{$sender}",$shipername,300);
                    $this->cache->save("delivery_shippemail_{$sender}",'shipemail',300); // 2 minutes
                  }   

                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                }
                else if(!empty($this->cache->get("delivery_shippingpostcode_{$sender}"))){                  
                  $deliveryshippingmethod = $this->cache->get("delivery_shippingmethod_{$sender}");
                  $packagtype = $this->cache->get("packagtype_{$sender}");
                  $shipqty = $this->cache->get("shipqty_{$sender}");
                  $packagtypeprice = $this->cache->get("typeprice_{$sender}");

                  $postcode = (int)$messagetext;
                  $checkvalid = checkPostalcode($postcode);

                  if(count($checkvalid) > 0){
                    foreach ($checkvalid as $key => $code) {
                        if($this->language == 'english'){
                          $districtname = $code->district;
                        }
                        else{
                          $districtname = $code->district_thai;
                        }
                        $optionslist[] = array(
                          'content_type' => 'text',
                          'title' => ucwords($districtname),
                          'payload' => "delivery_shipping_district" 
                        );
                    }

                    $message_to_reply = $this->lang->line('choose_shipdistrict');

                    $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $optionslist]
                    ];

                    $this->cache->save("shipqty_{$sender}",$shipqty,300); // 3 minutes
                    $this->cache->save("shippingpostcode_{$sender}",$postcode,300); // 3 minutes
                    $this->cache->save("delivery_shippingmethod_{$sender}",$deliveryshippingmethod,300); // 3 minutes
                    $this->cache->save("packagtype_{$sender}",$packagtype,300);
                    $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                    //$this->cache->save('delivery_shippingdistrict','district',300); // 2 minutes

                    $this->cache->delete("delivery_shippingpostcode_{$sender}");

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  }
                  else{
                    $message_to_reply = $this->lang->line('valid_postcode');
                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->cache->save("shipqty_{$sender}",$shipqty,300); // 3 minutes
                    $this->cache->save("packagtype_{$sender}",$packagtype,300);
                    $this->cache->save("typeprice_{$sender}",$packagtypeprice,300);
                    $this->cache->save("delivery_shippingpostcode_{$sender}",'postcode',300); // 2 minutes
                    $this->cache->save("delivery_shippingmethod_{$sender}",$deliveryshippingmethod,300); // 2 minutes

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                  }
                }
                else if(!empty($this->cache->get("pagequestion_{$sender}"))){
                    $datasearch = array(
                      'search_by_userid' => $sender,
                      'search_keyword'  => $messagetext,
                      'page_id'         => $page_id  
                    );

                    if($this->saveSearchKeyword($datasearch)){
                     $message_to_reply = $this->lang->line('pm');
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                    }
                  $this->cache->delete("pagequestion_{$sender}");
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                }
                else if(!empty($this->cache->get("saveorderid_{$sender}"))){
                   $orderID = $messagetext; 
                   // check status of order or valid order
                   $shippingstatus = $this->checkShippingOrderStatus($orderID);

                   if($shippingstatus == 'error'){
                      $message_to_reply = $this->lang->line('findorderfailed');
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                      $this->cache->save("saveorderid_{$sender}",'order',300);
                   }
                   else{
                      $findordermsg = str_replace("<shipping_status>", $shippingstatus, $this->lang->line('findorder'));
                      $message_to_reply = str_replace("<tracking_url>", "http://tracking.acommerce.asia/smartship?id={$orderID}", $findordermsg);
                      $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                      ];
                     $this->cache->delete("saveorderid_{$sender}");
                   }
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                }
                else{
                  $find = str_replace("pointslive", "Smartship Thailand", $this->lang->line('endmsg'));
                  $message_to_reply = $find;
                  $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply]
                  ];
                  //$this->cache->save('packagtype',$packagtype,300);
                  $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  exit;
                }
              }
             break;
          }
        }
        else 
        {   
            switch($postback) {

            /***************** 14-02-2018 ****************/
            case 'PAY_LOAD_SHIPCOUNTRY':

              $this->cache->delete("saveorderid_{$sender}");
              $options = array(
                      0 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('pksmall'),
                          'payload' => "PAY_LOAD_PACKAGE"
                      ),
                      1 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('pkmedi'),
                          'payload' => "PAY_LOAD_PACKAGE"
                      ),
                      2 => array(
                          'content_type' => 'text',
                          'title' => $this->lang->line('pklarge'),
                          'payload' => "PAY_LOAD_PACKAGE"
                      )
                  ); 

                 $quickoptions = json_encode($options);

                 $message_to_reply = $this->lang->line('pkmsg');

                 $response = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                 ];

                 $this->cache->save("shippingcountry_{$sender}",$posttitletext,300);
                 $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
            break;

            case 'DEVELOPER_DEFINED_PAYLOAD':
                // file_put_contents('filename.txt', print_r($posttitletext, true));
                if($posttitletext == strtolower($this->lang->line('counterservice')) ){ //what's hot
                //|| $posttitletext == strtolower($this->lang->line('sellerpickup')) || $posttitletext == "seller pickup servic..." || $posttitletext == "รถกระบะผู้ขาย servic ..."
                $packageslist = array();
                $perpagerecord = 0;
                $nextcities = array();
                $page = 0;
                $packages = $this->Packages_model->listPackages(10,$page);
                //file_put_contents('filename.txt', print_r($packages, true));
                if(count($packages) > 0) :
                        foreach ($packages as $key => $package) {
                            if($this->language == 'english'){
                              $name = $package->package_country;
                            }else{
                              $name = $package->package_country_thai;
                            }

                            $packageslist[] = array(
                            "title"     => $name, 
                            "image_url" => base_url()."uploads/packages/".$package->package_image,
                            "buttons"   => array(
                                    array(
                                      "type"=>"postback",
                                      "title"=>$name,
                                      "payload"=>"PAY_LOAD_SHIPCOUNTRY"
                                    )    
                                )  
                            );
                        }

                      $perpagerecord = count($packages); // Current page count

                       $answer = ["attachment"=>[
                        "type"=>"template",
                        "payload"=>[
                            "template_type"=>"generic",
                            "elements"=> $packageslist
                            ]
                          ]
                        ];

                    else :
                        
                      $message_to_reply = $this->lang->line('citimissing');

                      $answer = [ 'text' => $message_to_reply];

                    endif;

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $answer 
                    ];

                    /*if($total_rows_country > 10) :

                      $page = $page + 1;
                      
                      $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                    
                      $this->cache->save("nextpshippingcountry_{$sender}", $page,300);
                      $this->cache->save("currentshippagecount_{$sender}", $perpagerecord,300);

                      $nextcities[] = array(
                          "title"     => "Click below to more cities", 
                          "buttons"   => array(
                                  array(
                                  "type"=>"postback",
                                  "title"=>'More cities',
                                  "payload"=>'Shipcities_Payload'
                                  )      
                              )  
                          );

                       $nextdata = ["attachment"=>[
                                "type"=>"template",
                                "payload"=>[
                                  "template_type"=>"generic",
                                  "elements"=>$nextcities
                                ]
                            ]];

                    $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => $nextdata
                    ];

                    endif;*/  

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 

                /*$this->cache->delete("saveorderid_{$sender}");
                    $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('pksmall'),
                                'payload' => "PAY_LOAD_PACKAGE"
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('pkmedi'),
                                'payload' => "PAY_LOAD_PACKAGE"
                            ),
                            2 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('pklarge'),
                                'payload' => "PAY_LOAD_PACKAGE"
                            )
                        ); 

                       $quickoptions = json_encode($options);

                       $message_to_reply = $this->lang->line('pkmsg');

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                       ];

                       $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                       exit;*/
                }
                
                if($posttitletext == strtolower($this->lang->line('faq'))){
                  $this->cache->delete("saveorderid_{$sender}");
                   //$message_to_reply = 'Contact Human Manager on Facebook';
                  $message_to_reply = $faq_msg ? $faq_msg : "Here is a list of General Enquiries";

                   $options = array(
                            0 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('plt'),
                                'payload' => "PAY_LOAD_TIME"
                            ),
                            1 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('plorder'),
                                'payload' => "PAY_LOAD_ORDER"
                            ),
                            2 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('ploffice'),
                                'payload' => "PAY_LOAD_OFFICE"
                            ),
                            3 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('aboutpage'),
                                'payload' => "PAY_LOAD_ABOUTUS"
                            ),
                            4 => array(
                                'content_type' => 'text',
                                'title' => $this->lang->line('plissue'),
                                'payload' => "PAY_LOAD_ISSUE"
                            )

                        ); 

                   $quickoptions = json_encode($options);

                  // $message_to_reply = $posttitletext;

                   $response = [
                        'recipient' => [ 'id' => $sender ],
                        'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                   ];
                   $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                   exit;

                }
                if($posttitletext == strtolower($this->lang->line('aboutpage'))){ 

                    $token = "1733222426709661|hkJN_Vy-PlG-tLWD0w_NnZC5KW8";

                    $content = $this->curl("https://graph.facebook.com/{$page_id}?fields=about,name&access_token=".$token,'','GET'); 
                      //curl_close($ch);  
                    $message_to_reply = $content->about;

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                  
                    /*************************** Welcome loop ****************************/
                    $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('counterservice'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('sellerpickup')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                 $responseloop = [
                      'recipient' => [ 'id' => $sender ],
                      'message' => $answer 
                 ];
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$responseloop,'POST');  
                exit;

                }
                // 14-11-2017
                if($posttitletext == strtolower($this->lang->line('endchat'))){

                    $this->cache->delete("saveorderid_{$sender}"); 
                    $this->cache->delete("shipqty_{$sender}");
                    $this->cache->delete("shipping_district_{$sender}");
                    $this->cache->delete("shipping_postcode_{$sender}");
                    $this->cache->delete("shipping_deliveryaddress_{$sender}");
                    $this->cache->delete("delivery_shippingmethod_{$sender}"); 
                    $this->cache->delete("packagtype_{$sender}"); 
                    $this->cache->delete("packagprice_{$sender}");
                    $this->cache->delete("shipping_phoneno_{$sender}"); 
                    $this->cache->delete("shipping_shippemail_{$sender}");
                    $this->cache->delete("delivery_shippemail_{$sender}"); 
                    $this->cache->delete("delivery_shippingpostcode_{$sender}");
                    $this->cache->delete("shippingpostcode_{$sender}");
                    $this->cache->delete("packagtypename_{$sender}");
                    $this->cache->delete("typeprice_{$sender}");
                    $this->cache->delete("shippingcountry_{$sender}");

                     $message_to_reply = $dismiss_msg ? $dismiss_msg : 'Sure. Just type my name Akin in this chat if you need me';

                     $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                     ];
                
                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                    exit;
                }   
              //08-02-2018
              if($posttitletext == strtolower($this->lang->line('sellerpickup'))){

                $message_to_reply = $this->lang->line('enterorderid');
                $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                     ];
                $this->cache->save("saveorderid_{$sender}",'order',300);   
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
              }
              break;
            case 'PAY_LOAD_1':
                if($posttitletext == strtolower($this->lang->line('viewmore'))){
                    $this->cache->delete("saveorderid_{$sender}");
                    $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                            "template_type"=>"button",
                            "text"=>$this->lang->line('moreopt'),
                            "buttons"=>[
                             /* [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('aboutpage'),
                              ],*/
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('faq')
                              ],
                              [
                                "type"=>"postback",
                                "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                                "title" => $this->lang->line('endchat')
                              ]
                            ]
                          ]
                        ]];

                       $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => $answer 
                      ];
                }
                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');  
                break;

              // updated 10-01-2018 chatbot flow
            case 'Payment_Counter_Cash': 
                // Save address in table of users
                //$products = productlists_by_userid($sender); // Get products ID's
                $products = $this->Shopping_carts_model->where(array('user_id' => $sender,'product_id' => 0))->get(); 
                
                if(count($products) > 0){
                  $district = $this->cache->get("shipping_district_{$sender}");
                  $postcode = $this->cache->get("shipping_postcode_{$sender}");
                  $addressdata = $this->cache->get("shipping_deliveryaddress_{$sender}");
                  $deliverymethod = $this->cache->get("delivery_shippingmethod_{$sender}"); 
                  $packagtype = $this->cache->get("packagtype_{$sender}"); 
                  $packagprice = $this->cache->get("packagprice_{$sender}");
                  $shipqty = (int)$this->cache->get("shipqty_{$sender}");
                  $shipername = $this->cache->get("shipername_{$sender}");
                  $reaminqty = $shipqty - 1;

                  if($this->language == 'english'){
                    $shipcountry = "Thailand";
                  }
                  else{
                    $shipcountry = "ประเทศไทย";
                  }
                 // $shipcountry = $this->cache->get('delivery_shippingcountry'); 
                  $shipphone = $this->cache->get("shipping_phoneno_{$sender}"); 
                  $shipemail = $this->cache->get("shipping_shippemail_{$sender}"); 

                  $provinces = getStateByDistrict($district); // States shipping address

                  if($this->language == 'english'){
                    $state = $provinces[0]->province;
                  }else{
                    $state = $provinces[0]->province_thai;
                  }

                  $info = array(
                      'user_id'   => $sender,
                      'postcode'  => $postcode,
                      'district'  => $district,
                      'country'   => $shipcountry,
                      'receiver_name'   => $shipername,
                      'phone'     => $shipphone,
                      'email'     => $shipemail,
                      'delivery_method' => $deliverymethod,
                      'shipping_address'  => $addressdata
                    );

                  $addressID = $this->Address_model->insert($info); // Save address in table

                  //$Pids = array();
                  $sum = 0;
                  $sum = $sum + ($packagprice * $products->qty) + 0;

                  $orderInfo = array(
                      "transaction_id"   => time(),
                      "user_id"          => $sender,
                      'page_id'          => $page_id,
                      "shipping_address" => $addressID,
                      "order_amount"     => $sum ,
                      "payment_method"   => "cash",
                      "status"           => "0",//1
                      "transaction_ref"  => "",
                      "shipment_date"    => "",//date('Y-m-d h:i:s')
                      "order_type"       => "shipping" 
                  );

                  $saved = $this->Orders_model->insert($orderInfo); // Save order's

                  if($saved){

                    $orderdetails = $this->Orders_model->get($saved);

                    $this->Orders_products_model->insert(array('order_id' => $orderdetails->transaction_id,'product_id' => 0,'ordered_qty' => $products->qty));

                    // Shipping info stored
                    $this->db->insert('shipping_packages', array('transaction_id' => $orderdetails->transaction_id,'package_type' => $packagtype));

                    // Remove user shopping cart products
                    $this->Shopping_carts_model->force_delete(array('user_id' => $sender,'product_id' => 0));

                    $message_to_reply = $this->lang->line('smartchip');

                    $response = [
                          'recipient' => [ 'id' => $sender ],
                          'message' => [ 'text' => $message_to_reply]
                    ];

                    $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                    // Shipping orders pending to create
                    $shipaddress = $this->Address_model->get($addressID);
                      //$shiptype = "";
                      if($shipaddress->delivery_method == 'next_day'){
                        $shiptype = "NEXT_DAY";
                      }
                      if($shipaddress->delivery_method == 'express' || $shipaddress->delivery_method == 'ด่วน'){
                        $shiptype = "EXPRESS_1_2_DAYS";
                      }
                      if($shipaddress->delivery_method == 'standard' || $shipaddress->delivery_method == 'มาตรฐาน'){
                        $shiptype = "STANDARD_2_4_DAYS";
                      }

                     /* $userinfo = $this->getPageUserInfo("{$page_id}","{$sender}");

                      $fullname = $userinfo->first_name.' '.$userinfo->last_name;*/
                      
                      $businessaddress = $this->getOtherPageLocationHours("{$page_id}");

                      $validatetoken = $this->validateAPI(); // Get Access Token of Logistic API

                      if(!is_null($validatetoken)){
                        $token = $validatetoken['token']['token_id'];
                      }
         
                      $addressee = "";
                      $address1 = "";
                      $province = "";
                      $postalCode = "";
                      $country = "";
                      $phone = "";
                      $email = "";

                      if(isset($businessaddress->name)){
                        $addressee = $businessaddress->name;
                      }
                      if(isset($businessaddress->single_line_address)){
                        $address1 = $businessaddress->single_line_address;
                        $explodeaddr = explode(",", $businessaddress->single_line_address);
                        $provinces = explode(" ", $explodeaddr[2]);
                        $provincename = $provinces[1];
                        $postalCode = $provinces[2];
                      }
                      
                      if(isset($businessaddress->phone)){
                        $phone = $businessaddress->phone;
                      }
                      if(isset($businessaddress->emails)){
                        $email = $businessaddress->emails[0];
                      }

                      $orders = orderlists($orderdetails->transaction_id);
                      
                      $pname = $packagtype;
                      $price = $packagprice;

                      $shiporderItems[] = array(
                        "itemDescription" => "{$pname}",
                        "itemQuantity"    => 1,
                      );

                      /*foreach ($orders[0]->products as $key => $value) {

                          if($value->sale_price > 0){
                            $price = $value->sale_price;
                          }else{
                            $price = $value->product_price;
                          }
                          if($this->language == 'english'){
                            $pname = $value->product_name;
                          }else{
                            $pname = $value->product_name_thai;
                          }
                         $shiporderItems[] = array(
                          "itemDescription" => "{$pname}",
                          "itemQuantity"    => (int)$value->ordered_qty,
                          );
                      }*/
        
                      $shipordersDetails = [
                        "shipServiceType" => "SMARTSHIP_DROPOFF",
                        "shipSender" => [
                            "addressee"=>"Smartship Thailand",//{$addressee}
                            "address1"=>"253,Thailand", // {$address1}
                            "city"=>"",
                            "province"=>"Thailand", // {$provincename}
                            "postalCode"=>"10110", //{$postalCode}
                            "country"=> "Thailand",//{$provincename}
                            "phone"=> "022613540", //{$phone} 
                            "email"=>"info@smartship.asia" // {$email} 
                        ],
                        "shipShipment" =>[
                            "addressee"=>"{$shipaddress->receiver_name}",
                            "address1"=>"{$shipaddress->shipping_address}",
                            "address2"=>"",
                            "subDistrict"=>"",
                            "district"=>"{$shipaddress->district}",
                            "city"=>"",
                            "province"=>"{$state}",
                            "postalCode"=>"{$shipaddress->postcode}",
                            "country"=>"{$shipaddress->country}",
                            "phone"=>"{$shipaddress->phone}",
                            "email"=>"{$shipaddress->email}"
                        ],
                        "shipShippingType"=>"{$shiptype}",
                        "shipPaymentType"=>"COD",
                        "shipCurrency"=>"THB",
                        "shipGrossTotal"=>(int)$orders[0]->order_amount,
                        "shipInsurance"=>false,
                        //"shipPickingList"=>$shiporderItems,
                        "shipPackages"=>[
                            [
                                "packageWeight"=>$price,
                                "packageItems"=> $shiporderItems
                            ]
                        ]
                      ]; 

                      $salesorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$orderdetails->transaction_id}",$token,$shipordersDetails);


                      if(isset($salesorder['http_code']) && $salesorder['http_code'] == 201){
                        $findorder = str_replace("<orderno>", "{$orderdetails->transaction_id}", $this->lang->line('is_process'));
                        $message_to_reply = $findorder;

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                        ];

                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                        // Barcode generate TYPE_CODE_128 of order id
                        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                        $string = base64_encode($generator->getBarcode("{$orderdetails->transaction_id}", $generator::TYPE_CODE_128));
                       
                        $image = base64_decode($string);
                        $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
                        $filename = $image_name . '.' . 'png';
                        //rename file name with random number
                        $path = FCPATH.'uploads/barcodes/';
                        //image uploading folder path
                        file_put_contents($path . $filename, $image);
                        $imageURL = base_url()."uploads/barcodes/".$filename;
                        //file_put_contents('filename.txt', print_r($imageURL, true));
                        //file_put_contents('fb_response.txt', $imageURL, FILE_APPEND | LOCK_EX);
                        $sendorder = $this->sendChatbotImageAPI($this->access_token,$page_id,$sender,$imageURL);//$filename - 08/02/2018

                        //file_put_contents('filename.txt', print_r($sendorder, true));
                        //file_put_contents('fb_response.txt',$sendorder, FILE_APPEND | LOCK_EX);
                       // print_r($sendorder);
                        if(isset($sendorder->attachment_id)){
                          $message_to_reply = $this->lang->line('orderidbarcode');

                          $response = [
                              'recipient' => [ 'id' => $sender ],
                              'message' => [ 'text' => $message_to_reply]
                          ];
                          $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                          // Barcode generate TYPE_CODE_128 of product_sku+payment
                          $skupaymetnbarcode = @$orders[0]->products[0]->product_sku.'COD';
                          $generatornew = new \Picqer\Barcode\BarcodeGeneratorPNG();
                          $stringcode = base64_encode($generatornew->getBarcode("{$skupaymetnbarcode}", $generator::TYPE_CODE_128));
                         
                          $image = base64_decode($stringcode);
                          $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
                          $filenamenew = $image_name . '.' . 'png';
                          //rename file name with random number
                          $path = FCPATH.'uploads/barcodes/';
                          //image uploading folder path
                          file_put_contents($path . $filenamenew, $image);

                          $imageURL2 = base_url()."uploads/barcodes/".$filenamenew;

                          $send = $this->sendChatbotImageAPI($this->access_token,$page_id,$sender,$imageURL2);
                          //$filenamenew - 08/02/2018

                          if(isset($send->attachment_id)){
                            $message_to_reply = $this->lang->line('skubarcode');

                            $response = [
                                'recipient' => [ 'id' => $sender ],
                                'message' => [ 'text' => $message_to_reply]
                            ];
                            $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                            $message_to_reply = $this->lang->line('trackingcode');

                            $response = [
                                'recipient' => [ 'id' => $sender ],
                                'message' => [ 'text' => $message_to_reply]
                            ];
                            $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');

                            $this->cache->delete("saveorderid_{$sender}");
                            $this->cache->delete("shipping_district_{$sender}");
                            $this->cache->delete("shipping_postcode_{$sender}");
                            $this->cache->delete("shipping_deliveryaddress_{$sender}");
                            $this->cache->delete("delivery_shippingmethod_{$sender}"); 
                            //$this->cache->delete('packagtype'); 
                            //$this->cache->delete('packagprice');
                            $this->cache->delete("shipping_phoneno_{$sender}"); 
                            $this->cache->delete("shipping_shippemail_{$sender}"); 
                            $this->cache->delete("delivery_shippemail_{$sender}"); 
                            $this->cache->delete("shippingpostcode_{$sender}");
                            $this->cache->delete("shipername_{$sender}");
                            $this->cache->delete("shippingcountry_{$sender}");

                            if($shipqty >= 1){

                                $message_to_reply =  $this->lang->line('anotership'); // 422 code. Validation failed.

                                $response = [
                                    'recipient' => [ 'id' => $sender ],
                                    'message' => [ 'text' => $message_to_reply]
                                ];
                                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');


                                $data = array(
                                  'user_id'     => $sender,
                                  'product_id'  => 0,
                                  'qty'         => 1
                                );  

                                $ok = $this->Shopping_carts_model->insert($data);

                                $options = array(
                                  0 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('delivery_nextday'),
                                      'payload' => "PAY_LOAD_DELIVERY_SPEED"
                                  ),
                                  1 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('delivery_express'),
                                      'payload' => "PAY_LOAD_DELIVERY_SPEED"
                                  ),
                                  2 => array(
                                      'content_type' => 'text',
                                      'title' => $this->lang->line('delivery_standard'),
                                      'payload' => "PAY_LOAD_DELIVERY_SPEED"
                                  )
                                ); 

                                $quickoptions = json_encode($options);

                                $message_to_reply = $this->lang->line('delivery_speed');

                                $response = [
                                'recipient' => [ 'id' => $sender ],
                                'message' => [ 'text' => $message_to_reply,'quick_replies' => $quickoptions]
                                ];

                                //file_put_contents("test.txt", $reaminqty,FILE_APPEND | LOCK_EX);
                                //$this->cache->save('packagprice',$packagprice,300);  
                                $this->cache->save("typeprice_{$sender}",$packagprice,300);
                                $this->cache->save("packagtype_{$sender}",$packagtype,300);  
                                $this->cache->save("shipqty_{$sender}",$reaminqty,300);

                                $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                            }
                            else{
                              $this->cache->delete("packagtype_{$sender}"); 
                              $this->cache->delete("packagprice_{$sender}");
                              $this->cache->delete("shipqty_{$sender}");
                            }

                          }
                        }
                      }else{
                       
                        $message_to_reply =  $this->lang->line('code'); // 422 code. Validation failed.

                        $response = [
                            'recipient' => [ 'id' => $sender ],
                            'message' => [ 'text' => $message_to_reply]
                        ];
                        $this->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST');
                    }

                  /*$this->cache->delete('shipping_district');
                  $this->cache->delete('shipping_postcode');
                  $this->cache->delete('shipping_deliveryaddress');
                  $this->cache->delete('delivery_shippingmethod'); 
                  $this->cache->delete('packagtype'); 
                  $this->cache->delete('packagprice');
                  $this->cache->delete('shipping_phoneno'); 
                  $this->cache->delete('shipping_shippemail'); */
                  }
                }
              break;

            default:
            break;
          }   
       } 
    }
  }

  // Get user facebook name using API
  public function getPageUserInfo($pageID,$userID){
    $url = "https://graph.facebook.com/v2.10/$userID?access_token=EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
    $response = $this->curl($url,'','GET');
    return $response;
  }

  public function getOtherPageLocationHours($page_id){
    $url = "https://graph.facebook.com/{$page_id}?fields=phone,emails,name,hours,single_line_address&access_token=EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
    //1733222426709661|hkJN_Vy-PlG-tLWD0w_NnZC5KW8
    $response = $this->curl($url,'','GET');
    //echo '<pre>';
    //print_r($response);
    return $response;
  }
  // Check shiping order status
  public function checkShippingOrderStatus($orderID=""){
      $findorder = $this->Orders_model->checkShipppingStatus($orderID);
      $code = '';
      if(count($findorder) > 0){
        if($findorder->status == 0){
          $code = "Processing";
        }
        else if($findorder->status == 1){
          $code = "Dispatched"; 
        }
        else if($findorder->status == 2){
          $code = "Completed"; 
        }
        else if($findorder->status == 3){
          $code = "Cancelled"; 
        }
        else if($findorder->status == 4){
          $code = "Rejected"; 
        }
        else if($findorder->status == 5){
          $code = "Failed"; 
        }
      }
      else{
        $code = "error";
      }
      return $code;
    }

}
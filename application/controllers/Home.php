<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# This is the default and home controller for the website
# created by mayur panchal 21-05-2018

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	  if($this->session->userdata('adminId')){
	  	redirect(base_url().'admin/dashboard');
	  }
	  else if($this->session->userdata('customerId')){
	  	redirect(base_url().'customer/profile');	
	  }		
	  $data = array();
      $data['content'] = 'front/home';
      $data['title'] = 'Home page';      
      $this->load->view('front/template', $data);
		
	}

}

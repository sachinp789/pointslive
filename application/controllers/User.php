<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct(){
		require_once APPPATH.'libraries/twitter/twitteroauth.php';
		require_once APPPATH.'libraries/insta/instagram.class.php';
		
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->model('Chatbots_model');
		$this->load->library('facebook'); 
	}

	public function sociallogin(){
		$data['content'] = 'users/social_login';
	    $data['title'] = 'Login With Social';
	    $this->load->view('users/template', $data);
	}

	// Process login redirection
	public function loginprocess($type){
		switch ($type) {
			case 'insta':
				$instagram = new Instagram(array(
				  "apiKey" 		=> $this->config->item('apiKey'),
				  "apiSecret"	=> $this->config->item('apiSecret'),
				  "apiCallback"	=> $this->config->item('apiCallback')
				));
				$loginUrl = $instagram->getLoginUrl();
				redirect($loginUrl);
				break;

			case 'twitter':
				if($this->config->item('consumer_key') === '' || $this->config->item('consumer_secret') === '')
				{	
					/*$error = "You need a consumer key and secret to test the sample code. Get one from <a href='https://twitter.com/apps'>https://twitter.com/apps</a>";
					$this->session->set_flashdata('flashmsg', $error);
		            $this->session->set_flashdata('msg', 'danger');*/
		            redirect(base_url());
				}
				else{
					$connection = new TwitterOAuth($this->config->item('consumer_key'),$this->config->item('consumer_secret'));// Key and Sec

					$request_token = $connection->getRequestToken($this->config->item('oauth_callback'));// Retrieve Temporary credentials. 
					
					$this->session->set_userdata('oauth_token',$request_token['oauth_token']);
                    $this->session->set_userdata('oauth_token_secret',$request_token['oauth_token_secret']);

                   	switch ($connection->http_code) {
					  case 200: 
					  	$url = $connection->getAuthorizeURL($request_token['oauth_token']); // Redirect to authorize page.
						redirect($url);
						break;
					  default:
					  	$this->session->set_flashdata('flashmsg', 'Could not connect to Twitter. Refresh the page or try again later.');
		                $this->session->set_flashdata('msg', 'error');
		                redirect(base_url());
					}
				}
			break;

			case 'facebook':
				$data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/fbcallback'), 
                'scope' => array("email","user_location","user_hometown","manage_pages","publish_pages","pages_messaging","pages_show_list","pages_messaging_subscriptions") // permissions here
                // 'scope' => array("email","user_location","user_hometown","manage_pages","publish_pages") // permissions here
            	));
           		redirect($data['login_url']);	
			break;

			default:
				break;
		}	
	}

	// Twitter
	public function callback(){
		$this->load->library('ciqrcode');
		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();

		//$config = emailconfig();
        //$this->load->library('email',$config);

		$connection = $this->twitter_callback();
		
		$access_token = $this->session->userdata('access_token');

		if(200 == $connection->http_code){
			//$this->session->set_userdata('status','verified');
			$twitterconnected = new TwitterOAuth($this->config->item('consumer_key'), $this->config->item('consumer_secret'), $access_token['oauth_token'], $access_token['oauth_token_secret']);

			$params = array('include_email' => 'true', 'include_entities' => 'false', 'skip_status' => 'true');
			/* If method is set change API call made. Test is called by default. */
			$content = $twitterconnected->get('account/verify_credentials',$params);

			if($content){
				$replaceImage = str_replace("normal", "bigger", $content->profile_image_url);
				$senddata = array(	
                    'admin_name'			=> $content->name,
                    'admin_email'			=> $content->email,
                    'login_with'			=> 'twitter',
                    'login_uid'				=> $content->id_str,
                    'login_picture'			=> $replaceImage,
                    'login_username'		=> $content->screen_name,
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	

				$checkLogin = $this->Admin_model->get(array('login_uid' => $content->id_str));
				
				if($checkLogin){

					if($checkLogin->is_active == 0){
						$this->session->set_flashdata('flashmsg', 'Your account has been blocked by admin.');
	            		$this->session->set_flashdata('msg', 'error');
					    redirect(base_url());
					}
					else{
						$admininfo = array();
						// 05-07-2018 Shortlinks
		              	if(is_null($checkLogin->catalog_shortlink)){
			              	$seller = $checkLogin->admin_id; // Remove encryption
					        $shorturl = base_url()."item/category/{$seller}";
					        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";
					        $ch = curl_init();
					        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
					        curl_setopt($ch,CURLOPT_URL,$postdata);
					        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					        $response = curl_exec($ch);
					        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					        $content = json_decode($response);
					        curl_close($ch);

					        if(isset($content->error)){
						      $path = __DIR__.'\User.php function callback';
						      $line = 78;
						      apilogger($content->error->message,$path,$line);
						    }
						    elseif(empty($content) || !$content) {
						      $path = __DIR__.'\User.php function callback';
						      $line = 78;
						      apilogger("Error while generating short-link",$path,$line);
						    }
		    				/*
					        if(isset($content->status) == 'error'){      
				                $path = __DIR__.'\Products.php function callback';
				                $line = 135;
				                apilogger($content->message,$path,$line);
				            }*/

					        if($content->result == true){
					          $admininfo['catalog_shortlink'] = $content->url;     
					        }
					        else{
					          $admininfo['catalog_shortlink'] = "";
					        }
		              	}

		              	if(is_null($checkLogin->catalog_qrcode)){
		              		$qr_image=rand().'.png';
					        $params['data'] = is_null($checkLogin->catalog_shortlink) ? $admininfo['global_link'] : $checkLogin->catalog_shortlink;//$this->input->post('finallink');
					        $params['level'] = 'H';
					        $params['size'] = 8;
					        $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
					        if($this->ciqrcode->generate($params))
					        {
					            $admininfo['catalog_qrcode']=$qr_image; 
					        }
		              	}
					  	
					  	$admininfo['login_picture'] = $replaceImage;

						$this->Admin_model->update($admininfo,$checkLogin->admin_id);// Image updated
						$this->session->set_userdata('username',$checkLogin->admin_name);
			            $this->session->set_userdata('isLogin','social');
			            $this->session->set_userdata('isLoginexists',true);
			            $this->session->set_userdata('adminId',$checkLogin->admin_id);
			            $this->session->set_userdata('pagemanager',$checkLogin->page_id);

			            redirect(base_url('admin/dashboard'));
			            /*if($prevurl != base_url()){
			            	redirect(base_url().'admin/profile/'.$checkLogin->admin_id);	
			            }
			            else{
			            	redirect(base_url().'admin/chatoptions');
			            }*/
					}

				}
				else{
					$admindata = array();

					$inserted = $this->Admin_model->insert($senddata);

					// 05-07-2018 Catalog shortlink for seller
					$seller = $inserted; // Remove encryption

			        $shorturl = base_url()."item/category/{$seller}";

			        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

			        $ch = curl_init();
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			        curl_setopt($ch,CURLOPT_URL,$postdata);
			        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			        $response = curl_exec($ch);
			        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			        $content = json_decode($response);
			        curl_close($ch);

			        if(isset($content->error)){
				      $path = __DIR__.'\User.php function callback';
				      $line = 78;
				      apilogger($content->error->message,$path,$line);
				    }
				    elseif(empty($content) || !$content) {
				      $path = __DIR__.'\User.php function callback';
				      $line = 78;
				      apilogger("Error while generating short-link",$path,$line);
				    }

			        /*if(isset($content->status) == 'error'){      
				        $path = __DIR__.'\User.php function callback';
				        $line = 186;
				        apilogger($content->message,$path,$line);
				    }*/

			        if($content->result == true){
			          $admindata['catalog_shortlink'] = $content->url;
			          $qr_image=rand().'.png';
				      $params['data'] = $content->url;//$this->input->post('finallink');
				      $params['level'] = 'H';
				      $params['size'] = 8;
				      $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
				      if($this->ciqrcode->generate($params))
				      {
				        $admindata['catalog_qrcode']=$qr_image; 
				      }     
			        }
			        else{
			          $admindata['catalog_shortlink'] = "";
			          $admindata['catalog_qrcode']=""; 
			        }

			        $this->Admin_model->update($admindata,$inserted);
			        // End

			        // Send signup email to sellers
			        if ( ! defined('BASEPATH')) exit('No direct script access allowed');

			        $datas = array("guestname" => $content->name);

		            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
		            $mailConfig['protocol'] = "sendmail";
		            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
		            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
		            $mailConfig['smtp_pass'] = "tg1<LvPL";
		            $mailConfig['smtp_port'] = "25";
		            $mailConfig['mailtype'] = "html";

	              	$this->load->library('email', $mailConfig);

	              	$body = $this->load->view('templates/seller_signup_template',$datas,TRUE);

	              	$this->email->set_newline("\r\n");   
		            $this->email->from('no-reply@pointslive.com', config_item('site_name'));
		            $this->email->to($content->email);
		           	$this->email->subject('Thank You for Signing Up to Akinai');
              		$this->email->message($body); 
              		$this->email->attach(FCPATH.'Akinai_Seller_Center_User_Guide.pdf');

		            $send = $this->email->send();
		            // End

					$checkLogin = $this->Admin_model->get($inserted);
					$this->session->set_userdata('username',$checkLogin->admin_name);
		            $this->session->set_userdata('isLogin','social');
		            $this->session->set_userdata('adminId',$checkLogin->admin_id);
		            $this->session->set_userdata('pagemanager',$checkLogin->page_id);
		            redirect(base_url('admin/dashboard'));
		            //redirect(base_url().'admin/chatoptions');
				}
			}
		}
		else{
			redirect(base_url());
		}
	}

	// Twitter callback
	public function twitter_callback(){
		$connection = new TwitterOAuth($this->config->item('consumer_key'), $this->config->item('consumer_secret'), $this->session->userdata('oauth_token'), $this->session->userdata('oauth_token_secret'));
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);	
		
		$this->session->set_userdata('access_token', $access_token);
		
		$this->session->unset_userdata('oauth_token');
		$this->session->unset_userdata('oauth_token_secret');
		return $connection;
	}

	// Instagram callback
	public function instacallback($code=""){
		$this->load->library('user_agent');
		$this->load->library('ciqrcode');
		$prevurl = $this->agent->referrer();
		// Receive OAuth code parameter && Check whether the user has granted access
		$code = $this->input->get('code');
		if (true === isset($code)) {
			$instagram = new Instagram(array(
				  "apiKey" 		=> $this->config->item('apiKey'),
				  "apiSecret"	=> $this->config->item('apiSecret'),
				  "apiCallback"	=> $this->config->item('apiCallback')
				));

			// Receive OAuth token object
			$data = $instagram->getOAuthToken($code);

			if(empty($data->user->username)){
                redirect(base_url());
			}
			else{
				$senddata = array(	
                    'admin_name'			=> $data->user->full_name,
                    'login_with'			=> 'instagram',
                    'login_uid'				=> $data->user->id,
                    'login_picture'			=> $data->user->profile_picture,
                    'login_username'		=> $data->user->username,
                    'login_access_token'	=> $data->access_token,	
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	
				
				$checkLogin = $this->Admin_model->get(array('login_uid' => $data->user->id));
				
				if($checkLogin){
					if($checkLogin->is_active == 0){
						$this->session->set_flashdata('flashmsg', 'Your account has been blocked by admin.');
	            		$this->session->set_flashdata('msg', 'error');
					    redirect(base_url());
					}else{
						$admininfo = array();
						// 05-07-2018 Shortlinks
		              	if(is_null($checkLogin->catalog_shortlink)){
			              	$seller = $checkLogin->admin_id; // Remove encryption
					        $shorturl = base_url()."item/category/{$seller}";
					        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";
					        $ch = curl_init();
					        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
					        curl_setopt($ch,CURLOPT_URL,$postdata);
					        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
					        $response = curl_exec($ch);
					        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					        $content = json_decode($response);
					        curl_close($ch);

					        if(isset($content->error)){
						      $path = __DIR__.'\User.php function instacallback';
						      $line = 257;
						      apilogger($content->error->message,$path,$line);
						    }
						    elseif(empty($content) || !$content) {
						      $path = __DIR__.'\User.php function instacallback';
						      $line = 257;
						      apilogger("Error while generating short-link",$path,$line);
						    }

					        /*if(isset($content->status) == 'error'){      
						        $path = __DIR__.'\User.php function instacallback';
						        $line = 289;
						        apilogger($content->message,$path,$line);
						    }*/

					        if($content->result == true){
					          $admininfo['catalog_shortlink'] = $content->url;     
					        }
					        else{
					          $admininfo['catalog_shortlink'] = "";
					        }
					    
		              	}

		              	if(is_null($checkLogin->catalog_qrcode)){
		              		$qr_image=rand().'.png';
					        $params['data'] = is_null($checkLogin->catalog_shortlink) ? $admininfo['global_link'] : $checkLogin->catalog_shortlink;//$this->input->post('finallink');
					        $params['level'] = 'H';
					        $params['size'] = 8;
					        $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
					        if($this->ciqrcode->generate($params))
					        {
					            $admininfo['catalog_qrcode']=$qr_image; 
					        }
		              	}

		              	$admininfo['login_access_token'] = $data->access_token;
		              	$admininfo['login_picture'] = $data->user->profile_picture;

						$this->Admin_model->update($admininfo,$checkLogin->admin_id); // Updated
						$this->session->set_userdata('username',$checkLogin->admin_name);
			            $this->session->set_userdata('isLogin','social');
			            $this->session->set_userdata('isLoginexists',true);
			            $this->session->set_userdata('adminId',$checkLogin->admin_id);
			            $this->session->set_userdata('pagemanager',$checkLogin->page_id);

			            $adminpage = $this->db->where(['created_by'=>$checkLogin->admin_id])->get('stores')->row();

			            if(empty($adminpage)){

				            if(!empty($accounts)){

								$datacount = count($accounts->data);

								if($datacount > 1){
									$dataaccount = $accounts->data;
									foreach ($dataaccount as $rows) {
										$id = $rows->id;
										$name = $rows->name;
										$access_token = $rows->access_token;

										$setup = $this->db->where(['page_id'=>$id])->get('store_pages')->row();



										if(!empty($setup)){


										$this->db->where(['id'=>$setup->id]);
										$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

										}else{

										$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);
										}
									

									}
									

								}else{
							

									$accounts_page = $accounts->data;

									$name = $accounts_page[0]->name;
									$id = $accounts_page[0]->id;
									$access_token = $accounts_page[0]->access_token;

									$setup = $this->db->where(['page_id'=>$id])->get('store_pages')->row();

									

									if(!empty($setup)){

										$this->db->where(['id'=>$setup->id]);
										$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

										}else{
									
										$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);
									}
									
								
								}
							}
						}

						redirect(base_url('admin/dashboard'));

			           /* if($prevurl != base_url()){
			            	redirect(base_url().'admin/profile/'.$checkLogin->admin_id);	
			            }
			            else{
			            	redirect(base_url().'admin/chatoptions');
			            }*/
					}
				}
				else{
					$admindata = array();
					$inserted = $this->Admin_model->insert($senddata);

					// 05-07-2018 Catalog shortlink for seller
					$seller = $inserted; // Remove encryption

			        $shorturl = base_url()."item/category/{$seller}";

			        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

			        $ch = curl_init();
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			        curl_setopt($ch,CURLOPT_URL,$postdata);
			        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			        $response = curl_exec($ch);
			        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			        $content = json_decode($response);
			        curl_close($ch);

			        if(isset($content->error)){
				      $path = __DIR__.'\User.php function instacallback';
				      $line = 257;
				      apilogger($content->error->message,$path,$line);
				    }
				    elseif(empty($content) || !$content) {
				      $path = __DIR__.'\User.php function instacallback';
				      $line = 257;
				      apilogger("Error while generating short-link",$path,$line);
				    }

			        /*if(isset($content->status) == 'error'){      
						$path = __DIR__.'\User.php function instacallback';
						$line = 399;
						apilogger($content->message,$path,$line);
					}*/

			        if($content->result == true){
			          $admindata['catalog_shortlink'] = $content->url;  
			          $qr_image=rand().'.png';
				      $params['data'] = $content->url;//$this->input->post('finallink');
				      $params['level'] = 'H';
				      $params['size'] = 8;
				      $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
				      if($this->ciqrcode->generate($params))
				      {
				        $admindata['catalog_qrcode']=$qr_image; 
				      }   
			        }
			        else{
			          $admindata['catalog_shortlink'] = "";
			          $admindata['catalog_qrcode']=""; 
			        }
			        $this->Admin_model->update($admindata,$inserted);
			        // End

					$checkLogin = $this->Admin_model->get($inserted);
					$this->session->set_userdata('username',$checkLogin->admin_name);
		            $this->session->set_userdata('isLogin','social');
		            $this->session->set_userdata('adminId',$checkLogin->admin_id);
		            $this->session->set_userdata('pagemanager',$checkLogin->page_id);
		            redirect(base_url('admin/dashboard'));
		           // redirect(base_url().'admin/chatoptions');
				}
			}
		}
		else{
			redirect(base_url());
		}
	}

	// Facebook Callback
	public function fbcallback(){
		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();
		$fromEmail = "no-reply@pointslive.com";
		if (isset($_GET['code']) && !empty($_GET['code'])) {

			$code = $_GET['code'];
			$validatetoken = $this->get_fb_contents("https://graph.facebook.com/v3.0/oauth/access_token?client_id=".$this->config->item('appId')."&redirect_uri=".base_url('user/fbcallback')."&client_secret=".$this->config->item('secret')."&code=".$code);

			$finaltoken = json_decode($validatetoken);

			if(!empty($finaltoken->access_token)) {
	      		// getting all user info using access token.
				$fbuser_info = $this->get_fb_contents("https://graph.facebook.com/v3.0/me?fields=id,name,email,first_name,last_name,location{location},accounts&access_token=".$finaltoken->access_token);
				$facebookdata = json_decode($fbuser_info);
				$fb_id = $facebookdata->id;
				$accounts = $facebookdata->accounts;

				$pimage = "https://graph.facebook.com/{$facebookdata->id}/picture?type=large";	
				$facebookdata->profile_image = $pimage;

				$senddata = array(	
                    'admin_name'			=> $facebookdata->name,
                    'admin_email'			=> $facebookdata->email,
                    'login_with'			=> 'facebook',
                    'login_uid'				=> $facebookdata->id,
                    'login_picture'			=> $facebookdata->profile_image,
                    'login_username'		=> '',
                    'login_access_token'	=> $finaltoken->access_token,
                    'location'				=> serialize($facebookdata->location),	
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	
				
				$checkLogin = $this->Admin_model->get(array('login_uid' => $facebookdata->id));
				
				if($checkLogin){

					if($checkLogin->is_active == 0){
						$this->session->set_flashdata('flashmsg', 'Your account has been blocked by admin.');
	            		$this->session->set_flashdata('msg', 'error');
					    redirect(base_url());
					}
					else{
						$this->Admin_model->update(array("login_access_token" => $finaltoken->access_token,"login_picture" => $facebookdata->profile_image),$checkLogin->admin_id); // Updated
						$this->session->set_userdata('username',$checkLogin->admin_name);
			            $this->session->set_userdata('isLogin','social');
			            $this->session->set_userdata('isLoginexists',true);
			            $this->session->set_userdata('adminId',$checkLogin->admin_id);
			            $this->session->set_userdata('isFacebook',true);			            
			            $this->session->set_userdata('pagemanager',$checkLogin->page_id);

			            $adminpage = $this->db->where(['created_by'=>$checkLogin->admin_id])->get('stores')->row();

			            if(empty($adminpage)){

				            if(!empty($accounts)){

								$datacount = count($accounts->data);

								if($datacount > 1){
									$dataaccount = $accounts->data;
									foreach ($dataaccount as $rows) {
										$id = $rows->id;
										$name = $rows->name;
										$access_token = $rows->access_token;

										$setup = $this->db->where(['page_id'=>$id])->get('store_pages')->row();



										if(!empty($setup)){


										$this->db->where(['id'=>$setup->id]);
										$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

										}else{

										$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);
										}
									

									}
									

								}else{
							

									$accounts_page = $accounts->data;

									$name = $accounts_page[0]->name;
									$id = $accounts_page[0]->id;
									$access_token = $accounts_page[0]->access_token;

									$setup = $this->db->where(['page_id'=>$id])->get('store_pages')->row();

									

									if(!empty($setup)){

										$this->db->where(['id'=>$setup->id]);
										$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

										}else{
									
										$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);
									}
									
								
								}
							}
						}

			            if($prevurl != base_url()){
			            	redirect(base_url().'admin/profile/'.$checkLogin->admin_id);	
			            }
			            else{
			            	redirect(base_url().'admin/chatoptions');
			            }
					}	
				}
				else{
					$inserted = $this->Admin_model->insert($senddata);
					$checkLogin = $this->Admin_model->get($inserted);
					//$accounts = $facebookdata->accounts;
					if(!empty($accounts)){

						$datacount = count($accounts->data);

						if($datacount > 1){
						$dataaccount = $accounts->data;
								
						foreach ($dataaccount as $rows) { //echo "Df";
							$id = $rows->id;
							$name = $rows->name;
							$access_token = $rows->access_token;
							if(!empty($setup)){
							$this->db->where(['id'=>$setup->id]);
							$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

							}else{

							$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$inserted]);
							}
						}

					}else{
						//die("dfs");
						$accounts_page = $accounts->data;

						$name = $accounts_page[0]->name;
						$id = $accounts_page[0]->id;
						$access_token = $accounts_page[0]->access_token;
						
						$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$inserted]);	

						/*$this->db->insert('stores',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$inserted]);*/
						
						}
					}
					//die("out");
					$this->session->set_userdata('username',$checkLogin->admin_name);
		            $this->session->set_userdata('isLogin','social');
		            $this->session->set_userdata('isFacebook',true);
		            $this->session->set_userdata('adminId',$checkLogin->admin_id);
		            //$this->session->set_userdata('pagemanager',$checkLogin->page_id);


		            $body = $this->load->view('templates/welcome_email.php',$senddata,TRUE);

	              	// Send Mail usign mail()
	              	$emailMessage = implode("\r\n", array($body));

	              	$headers = array("From: ".$fromEmail,
	                  "Reply-To: ".$fromEmail,
	                  "X-Mailer: PHP/" . PHP_VERSION,
	                  "Content-Type: text/html; charset=UTF-8\r\n"
	              	);
	              	$headers = implode("\r\n", $headers);

	              	$send = mail($facebookdata->email, "Welcome Email", $emailMessage, $headers, "-f".$fromEmail);

	              	var_dump($send);

		            redirect(base_url().'admin/chatoptions');
				}

			}
		}
		else{
            redirect(base_url());
		}
	}

	function get_fb_contents($url) {
		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		$response = curl_exec( $curl );
		curl_close( $curl );
		return $response;
	}

	// Reusability for connect channel
	// Process login redirection
	public function ConnectProcess($type){
		
		switch ($type) {
			case 'insta':
				$instagram = new Instagram(array(
				  "apiKey" 		=> $this->config->item('apiKey'),
				  "apiSecret"	=> $this->config->item('apiSecret'),
				  "apiCallback"	=> $this->config->item('apiConnectCallback')
				));
				$loginUrl = $instagram->getLoginUrl();
				redirect($loginUrl);
				break;

			case 'twitter'://echo($this->config->item('consumer_secret'));exit;
				if($this->config->item('consumer_key') === '' || $this->config->item('consumer_secret') === '')
				{	//echo "Dfd";exit;
					/*$error = "You need a consumer key and secret to test the sample code. Get one from <a href='https://twitter.com/apps'>https://twitter.com/apps</a>";
					$this->session->set_flashdata('flashmsg', $error);
		            $this->session->set_flashdata('msg', 'danger');*/
		            redirect(base_url());
				}
				else{// exit;
					$connection = new TwitterOAuth($this->config->item('consumer_key'),$this->config->item('consumer_secret'));// Key and Sec

					$request_token = $connection->getRequestToken($this->config->item('oauth_connectcallback'));// Retrieve Temporary credentials. 

					$this->session->set_userdata('oauth_token',$request_token['oauth_token']);
                    $this->session->set_userdata('oauth_token_secret',$request_token['oauth_token_secret']);


                   	switch ($connection->http_code) {
					  case 200: 
					  	$url = $connection->getAuthorizeURL($request_token['oauth_token']); // Redirect to authorize page.
					  	//echo $url;exit;
						redirect($url);
						break;
					  default:
					  	$this->session->set_flashdata('flashmsg', 'Could not connect to Twitter. Refresh the page or try again later.');
		                $this->session->set_flashdata('msg', 'error');
		                redirect(base_url());
					}
				}
			break;

			case 'facebook':
				$data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user/fbconnectcallback'), 
                'scope' => array("email","user_location","user_hometown","manage_pages","publish_pages","pages_messaging","pages_show_list","pages_messaging_subscriptions") // permissions here
            	));
           		redirect($data['login_url']);	
			break;

			default:
				break;
		}	
	}

	// Twitter
	public function connectcallback(){
		//die("sdff");
		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();
		//die("dsf");
		$connection = $this->twitter_connectcallback();
		
		$access_token = $this->session->userdata('access_token');

		if(200 == $connection->http_code){
			//$this->session->set_userdata('status','verified');
			$twitterconnected = new TwitterOAuth($this->config->item('consumer_key'), $this->config->item('consumer_secret'), $access_token['oauth_token'], $access_token['oauth_token_secret']);
			/* If method is set change API call made. Test is called by default. */
			$content = $twitterconnected->get('account/verify_credentials');
			if($content){
				$replaceImage = str_replace("normal", "bigger", $content->profile_image_url);
				$senddata = array(	
                    'admin_name'			=> $content->name,
                    'login_with'			=> 'twitter',
                    'login_uid'				=> $content->id_str,
                    'login_picture'			=> $replaceImage,
                    'login_username'		=> $content->screen_name,
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	

				$checkLogin = $this->Admin_model->get(array('login_uid' => $content->id_str));
				//var_dump($checkLogin);exit;
				//die($content->id_str);
				if($checkLogin){

					$connectaccounts = $this->Admin_model->where_connected_with($this->session->userdata('adminId'))->get();

					//$socialsaccounts = explode(",", $checkLogin->connected_with);
					
					if($checkLogin->is_active == 0){
						$this->session->set_flashdata('flashmsg', 'Account has been blocked by admin.');
	            		$this->session->set_flashdata('msg', 'error');
					    redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
					}
					/*elseif(!$connectaccounts){
						$this->update_connect($this->session->userdata('adminId'),$checkLogin->admin_id);
						$this->session->set_flashdata('flashmsg', 'Account has been connected.');
			            $this->session->set_flashdata('msg', 'success');
						redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
					}*/
					else{
						$this->session->set_flashdata('flashmsg', 'Your account has been already connected.');
			            $this->session->set_flashdata('msg', 'error');
						redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
					}
				}
				else{

					$checkLogin = $this->Admin_model->get(array('admin_id' => $this->session->userdata('adminId')));

					$senddata['connected_with'] = $this->session->userdata('adminId');
					$inserted = $this->Admin_model->insert($senddata);

					$admin_id = $this->session->userdata('adminId');

					$this->update_connect($admin_id,$inserted);
					/*if(empty($checkLogin->connected_with)){
						$accounts = $checkLogin->admin_id;
					}
					else{
						$accounts = $checkLogin->connected_with .= ",{$inserted}";
					}

					$this->Admin_model->update(array("connected_with" => $accounts),$this->session->userdata('adminId'));*/

					// Update social account
					//$this->Admin_model->update(array("connected_with" => $inserted),$this->session->userdata('adminId'));// Image updated
					$this->session->set_userdata('isLogin','social');
					$this->session->set_flashdata('flashmsg', 'Your account has been successfully connected.');
	        		$this->session->set_flashdata('msg', 'success');
					//$checkLogin = $this->Admin_model->get($inserted);
		            redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
				}
			}
		}
		else{
			redirect(base_url());
		}
	}

	// Twitter callback
	public function twitter_connectcallback(){
		$connection = new TwitterOAuth($this->config->item('consumer_key'), $this->config->item('consumer_secret'), $this->session->userdata('oauth_token'), $this->session->userdata('oauth_token_secret'));
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);	
		
		$this->session->set_userdata('access_token', $access_token);
		
		$this->session->unset_userdata('oauth_token');
		$this->session->unset_userdata('oauth_token_secret');
		return $connection;
	}

	// Instagram callback
	public function connectinstacallback($code=""){
		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();
		// Receive OAuth code parameter && Check whether the user has granted access
		$code = $this->input->get('code');
		if (true === isset($code)) {
			$instagram = new Instagram(array(
				  "apiKey" 		=> $this->config->item('apiKey'),
				  "apiSecret"	=> $this->config->item('apiSecret'),
				  "apiCallback"	=> $this->config->item('apiConnectCallback')
				));

			// Receive OAuth token object
			$data = $instagram->getOAuthToken($code);

			if(empty($data->user->username)){
                redirect(base_url());
			}
			else{
				$senddata = array(	
                    'admin_name'			=> $data->user->full_name,
                    'login_with'			=> 'instagram',
                    'login_uid'				=> $data->user->id,
                    'login_picture'			=> $data->user->profile_picture,
                    'login_username'		=> $data->user->username,
                    'login_access_token'	=> $data->access_token,	
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	
				
				$checkLogin = $this->Admin_model->get(array('login_uid' => $data->user->id));
				
				if($checkLogin){ 
					$connectaccounts = $this->Admin_model->where_connected_with($this->session->userdata('adminId'))->get();

					if($checkLogin->is_active == 0){
						$this->session->set_flashdata('flashmsg', 'Your account has been blocked by admin.');
	            		$this->session->set_flashdata('msg', 'error');
					    redirect(base_url());
					}else{
						$this->session->set_flashdata('flashmsg', 'Your account has been already connected.');
			            		$this->session->set_flashdata('msg', 'error');
							    redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
					}
				}
				else{

					$checkLogin = $this->Admin_model->get(array('admin_id' => $this->session->userdata('adminId')));

					$senddata['connected_with'] = $this->session->userdata('adminId');
					$inserted = $this->Admin_model->insert($senddata);

					/*if(empty($checkLogin->connected_with)){
						$accounts = $checkLogin->admin_id;
					}
					else{
						$accounts = $checkLogin->connected_with .= ",{$inserted}";
					}*/
					$admin_id = $this->session->userdata('adminId');
					$this->update_connect($admin_id,$inserted);

					// $this->Admin_model->update(array("connected_with" => $accounts),$this->session->userdata('adminId'));

					$this->session->set_userdata('isLogin','social');
					$this->session->set_flashdata('flashmsg', 'Your account has been successfully connected.');
	        		$this->session->set_flashdata('msg', 'success');
					//$checkLogin = $this->Admin_model->get($inserted);
		            redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
				}
			}
		}
		else{
			redirect(base_url());
		}
	}

	function update_connect($loggedin_id,$new_id){

		$login = $this->Admin_model->get(['admin_id'=>$loggedin_id]);
		$connected_account = $login->connected_with;
		if(empty($connected_account)){

			$this->Admin_model->update(['connected_with'=>$new_id],$loggedin_id);

			$this->Admin_model->update(['connected_with'=>$loggedin_id],$new_id);

		}else{
			
			// second account update which is connected in middle of first and third
			$connected_with = $loggedin_id.",".$new_id;
			$this->Admin_model->update(['connected_with'=>$connected_with],$connected_account);

			$loggedin_connect_with = $connected_account.",".$new_id;			
			$this->Admin_model->update(['connected_with'=>$loggedin_connect_with],$loggedin_id);

			$new_connect_with = $loggedin_id.",".$connected_account;
			$this->Admin_model->update(['connected_with'=>$new_connect_with],$new_id);

		}



	}

	// Facebook Callback
	public function fbconnectcallback(){
		$this->load->library('user_agent');
		$prevurl = $this->agent->referrer();

		if (isset($_GET['code']) && !empty($_GET['code'])) {

			$code = $_GET['code'];
			$validatetoken = $this->get_fb_contents("https://graph.facebook.com/v3.0/oauth/access_token?client_id=".$this->config->item('appId')."&redirect_uri=".base_url('user/fbconnectcallback')."&client_secret=".$this->config->item('secret')."&code=".$code);

			$finaltoken = json_decode($validatetoken);

			if(!empty($finaltoken->access_token)) {
	      		// getting all user info using access token.
				$fbuser_info = $this->get_fb_contents("https://graph.facebook.com/v3.0/me?fields=id,name,email,first_name,last_name,accounts&access_token=".$finaltoken->access_token);
				$facebookdata = json_decode($fbuser_info);
				$fb_id = $facebookdata->id;
				$accounts = $facebookdata->accounts;

				$pimage = "https://graph.facebook.com/{$facebookdata->id}/picture?type=large";	
				$facebookdata->profile_image = $pimage;

				$senddata = array(	
                    'admin_name'			=> $facebookdata->name,
                    'admin_email'			=> $facebookdata->email,
                    'login_with'			=> 'facebook',
                    'login_uid'				=> $facebookdata->id,
                    'login_picture'			=> $facebookdata->profile_image,
                    'login_username'		=> '',
                    'login_access_token'	=> $finaltoken->access_token,	
                    'is_social'				=> '1',
                    'page_events'			=> '0',
                    'is_active'				=> '1'	
                );	
				
				$checkLogin = $this->Admin_model->get(array('login_uid' => $facebookdata->id));
				
				if($checkLogin){

					$connectaccounts = $this->Admin_model->where_connected_with($this->session->userdata('adminId'))->get();

					if($checkLogin->is_active == 0){
						$this->session->set_flashdata('flashmsg', 'Your account has been blocked by admin.');
	            		$this->session->set_flashdata('msg', 'error');
					    redirect(base_url());
					}
					else{

						// Mayur Panchal
						$this->Admin_model->update(array("login_access_token" => $finaltoken->access_token,"login_picture" => $facebookdata->profile_image),$checkLogin->admin_id); // Updated
						$this->session->set_userdata('isLogin','social');
						/*$this->session->set_userdata('username',$checkLogin->admin_name);
			            $this->session->set_userdata('isLogin','social');
			            $this->session->set_userdata('isLoginexists',true);
			            $this->session->set_userdata('adminId',$checkLogin->admin_id);
			            $this->session->set_userdata('isFacebook',true);			            
			            $this->session->set_userdata('pagemanager',$checkLogin->page_id);*/

			            $adminpage = $this->db->where(['created_by'=>$checkLogin->admin_id])->get('stores')->row();

			            //var_dump($this->session->userdata('adminId'));exit;

			            if(empty($adminpage)){

				            if(!empty($accounts)){

								$datacount = count($accounts->data);

								if($datacount > 1){
									$dataaccount = $accounts->data;
									foreach ($dataaccount as $rows) {
										$id = $rows->id;
										$name = $rows->name;
										$access_token = $rows->access_token;

										$setup = $this->db->where(['page_id'=>$id])->get('store_pages')->row();



										if(!empty($setup)){


										$this->db->where(['id'=>$setup->id]);
										$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

										}else{

										$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);
										}
									

									}
									

								}else{
							

									$accounts_page = $accounts->data;

									$name = $accounts_page[0]->name;
									$id = $accounts_page[0]->id;
									$access_token = $accounts_page[0]->access_token;

									$setup = $this->db->where(['page_id'=>$id])->get('store_pages')->row();

									

									if(!empty($setup)){

										$this->db->where(['id'=>$setup->id]);
										$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

										}else{
									
										$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);
									}
									
								
								}
							}
						}

						$this->session->set_flashdata('flashmsg', 'Your account has been already connected.');
			           	$this->session->set_flashdata('msg', 'error');
						redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
					}
				}
				else{

					$checkLogin = $this->Admin_model->get(array('admin_id' => $this->session->userdata('adminId')));

					$senddata['connected_with'] = $this->session->userdata('adminId');
					$inserted = $this->Admin_model->insert($senddata);

					if(!empty($accounts)){

						$datacount = count($accounts->data);

						if($datacount > 1){
							$dataaccount = $accounts->data;
									
							foreach ($dataaccount as $rows) {
								$id = $rows->id;
								$name = $rows->name;
								$access_token = $rows->access_token;
								if(!empty($setup)){
								$this->db->where(['id'=>$setup->id]);
								$this->db->update('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$checkLogin->admin_id]);

								}else{

								$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$inserted]);
								}
							}

						}else{
							
							$accounts_page = $accounts->data;

							$name = $accounts_page[0]->name;
							$id = $accounts_page[0]->id;
							$access_token = $accounts_page[0]->access_token;
							
							$this->db->insert('store_pages',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$inserted]);	

							/*$this->db->insert('stores',['page_id'=>$id,'page_name'=>$name,'access_token'=>$access_token,'created_by'=>$inserted]);*/
						
						}
					}

					$admin_id = $this->session->userdata('adminId');
					$this->update_connect($admin_id,$inserted);
					/*if(empty($checkLogin->connected_with)){
						$accounts = $checkLogin->admin_id;
					}
					else{
						$accounts = $checkLogin->connected_with .= ",{$inserted}";
					}

					$this->Admin_model->update(array("connected_with" => $accounts),$this->session->userdata('adminId'));*/

					//$checkLogin = $this->Admin_model->get($inserted);
		            $this->session->set_userdata('isLogin','social');
		            //$this->session->set_userdata('isFacebook',true);
					$this->session->set_flashdata('flashmsg', 'Your account has been successfully connected.');
	        		$this->session->set_flashdata('msg', 'success');
					//$checkLogin = $this->Admin_model->get($inserted);
		            redirect(base_url().'admin/profile/'.$this->session->userdata('adminId'));
				}
			}
		}
		else{
            redirect(base_url());
		}
	}

	// Save new chatbot 
	public function savechatBot(){
		$data = array();
		$socialinfo = array();

		if($this->input->post()){
			$storesopt = $this->input->post('businesshours');

			$chatbotdata = array(
				'store_open' => $storesopt,
				'delivery_services' => $this->input->post('serviceprovide'),
				'channel_connect'	=> $this->input->post('optchannel'),
				'return_offer'		=> $this->input->post('offer'),
				'website_url'		=> $this->input->post('website_url'),
				'contact_number'	=> $this->input->post('contact_number'),
				'public_key'		=> $this->input->post('public_key'),
				'secret_key'		=> $this->input->post('secret_key'),
				'omis_paymentlink'	=> $this->input->post('omispaymentlink'),
				'created_by'		=> $this->session->userdata('adminId'),
				'page_id'			=> "",
			);

			$sociallogin[$this->input->post('optchannel')] = array(
				'website' => $this->input->post('website_url')
				//'contact' => $this->input->post('contact_number')
			);

			//$chatbotdata['social_info'] = 
			//$chatbotdata['social_info'] = 

			$chatbotdata['social_info'] = serialize($sociallogin);

			if($chatbotdata['delivery_services'] == 1){
				$chatbotdata['service_provides'] = serialize($this->input->post('services'));
			}
			else{
				$chatbotdata['service_provides'] = "";
			}

			if($chatbotdata['return_offer'] == 1){
				$chatbotdata['offer_days'] = serialize($this->input->post('daysoffer'));
			}
			else{
				$chatbotdata['offer_days'] = "";
			}

			if($storesopt == 'custom'){	
				$chatbotdata['business_hours'] = serialize($this->input->post('days'));
			}
			else{
				$chatbotdata['business_hours'] = "";
			}

			// 26-06-2018 

			$chatbot_page_id = $this->input->post('chatbot_page');


			$chatbot_page = $this->db->where(['page_id'=>$chatbot_page_id])->get('store_pages')->row();

			
			
			$page_name = $chatbot_page->page_name;

			$page_id = $chatbot_page->page_id;

			$access_token = $chatbot_page->access_token;

			$created_by = $this->session->userdata('adminId');//$chatbot_page->created_by;

			$insert_botdata = ['page_name'=>$page_name,'page_id'=>$page_id,'access_token'=>$access_token,'created_by'=>$created_by];

			$this->db->insert('stores',$insert_botdata);

			$inserted = $this->Chatbots_model->insert($chatbotdata);

			if($inserted){
				$this->session->set_flashdata('flashmsg', 'Chatbot setup completed.');
	            $this->session->set_flashdata('msg', 'success');
	            redirect(base_url('admin/chatoptions'));
			}
			else{
				$this->session->set_flashdata('flashmsg', 'Error while setup chatbot.');
	            $this->session->set_flashdata('msg', 'error');
	           	redirect(base_url('admin/chatoptions'));
			}
		}
	}

	public function saveChatbotDetails($id=""){
		$channel = array();
		$chatdata = array();
		$socials = array();
		$paymentinfo = array();
		if($this->input->post())
		{	//echo '<pre>';
			//print_r($this->input->post());exit;
			if(!empty($this->input->post('channel_connect'))){
				$chatdata = array(
					"channel_connect"	=> $this->input->post('channel_connect')
					//"website_url"	=> trim($this->input->post('website_url')),
					//"contact_number"	=> trim($this->input->post('contact_number'))
				);
				
				/*$channel[$this->input->post('channelname1')] = array(
					"apikey" 	=> $this->input->post('apikeys'),
					"apisecret" => $this->input->post('apisecret')	
				);
				$channel[$this->input->post('channelname2')] = array(
					"apikey" 	=> $this->input->post('appID'),
					"apisecret" => $this->input->post('fbsecretkey')	
				);
				$channel[$this->input->post('channelname3')] = array(
					"apikey" 	=> $this->input->post('consumer_keys'),
					"apisecret" => $this->input->post('consumer_secret')	
				);
				
				$chatdata['social_apikeys'] = serialize($channel);*/	

				$socials[$this->input->post('channelname1')]	= array(
					'website' => $this->input->post('website_url_insta')
					//'contact' => $this->input->post('contact_number_insta')
				);
				$socials[$this->input->post('channelname2')]	= array(
					'website' => $this->input->post('website_url_fb')
					//'contact' => $this->input->post('contact_number_fb')
				);
				$socials[$this->input->post('channelname3')]	= array(
					'website' => $this->input->post('website_url_tweeter')
					//'contact' => $this->input->post('contact_number_tweeter')
				);

				$chatdata['social_info'] = serialize($socials);
			}
			else if(!empty($this->input->post('businesshours'))){
				if($this->input->post('businesshours') == 'custom'){	
					$chatdata['business_hours'] = serialize($this->input->post('days'));
				}
				else{
					$chatdata['business_hours'] = "";
				}
				$chatdata['store_open'] = $this->input->post('businesshours');
			}
			else if(!is_null($this->input->post('serviceprovide'))){
				$chatdata['delivery_services'] = $this->input->post('serviceprovide');
				if($this->input->post('serviceprovide') == 1){
					$chatdata['service_provides'] = serialize($this->input->post('services'));
				}
				else{
					$chatdata['service_provides'] = "";
				}

				$chatdata['return_offer'] = $this->input->post('offer');
				if($this->input->post('offer') == 1){
					$chatdata['offer_days'] = serialize($this->input->post('daysoffer'));
				}
				else{
					$chatdata['offer_days'] = "";
				}
			}
			else{ 
				// 08-08-2018 - Code updated for vendor bank info
				$chatdata['public_key'] = $this->input->post('public_key');
				$chatdata['secret_key'] = $this->input->post('secret_key');
				$paymentinfo['bank_name'] = $this->input->post('bank_name');
				$paymentinfo['account_number'] = $this->input->post('account_number');
				$paymentinfo['account_name'] = $this->input->post('account_name');
				$chatdata['bank_info'] = serialize($paymentinfo);
			}

			if(!is_null($this->input->post('offerstates'))){
				$chatdata['offer_states'] = serialize($this->input->post('offerstates'));
			}/*else{
				$chatdata['offer_states'] = "";
			}*/

			//$chatdata['omis_paymentlink'] = $this->input->post('omispaymentlink');

			$chatdata['page_id'] = "";

			if(!is_null($this->input->post('website_url'))){
				$chatdata['website_url'] = $this->input->post('website_url');
			}

			if(!is_null($this->input->post('contact_number'))){
				$code = $this->input->post('countrycode');
				$chatdata['contact_number'] = $code.'-'.$this->input->post('contact_number');
			}

			//$chatdata['website_url'] = $this->input->post('website_url');
			//$chatdata['contact_number'] = $this->input->post('contact_number');

			$checkdata = $this->Chatbots_model->where("created_by",$this->session->userdata('adminId'))->get();

			if($checkdata){
				$updated = $this->Chatbots_model->update($chatdata,$checkdata->id);	
			}
			else{
				$chatdata['created_by'] = $this->session->userdata('adminId');
				$updated = $this->Chatbots_model->insert($chatdata); 
			}

			//$this->Chatbots_model->update($chatdata,$id);
			if($updated){
				// Chatbot details has been updated 
				$this->session->set_flashdata('flashmsg', 'Details has been updated.');
	            $this->session->set_flashdata('msg', 'success');
	            redirect($_SERVER['HTTP_REFERER']);
	            //redirect(base_url('admin/profile/'.$this->session->userdata('adminId')));
			}
			else{
				$this->session->set_flashdata('flashmsg', 'You have no made any changes.');
	            $this->session->set_flashdata('msg', 'info');
	            redirect($_SERVER['HTTP_REFERER']);
	            //redirect(base_url('admin/profile/'.$this->session->userdata('adminId')));
			}
		}
	}

	// Facebook get userinfo
	/*function get_basic_info($id,$token) {
	    $url = "https://graph.facebook.com/".$id."?access_token={$token}";
	    $info = json_decode($this->file_get_contents_curl($url), true);
	    return $info;
	}

	function file_get_contents_curl( $url ) {

	  $ch = curl_init();
	  curl_setopt( $ch, CURLOPT_AUTOREFERER, TRUE );
	  curl_setopt( $ch, CURLOPT_HEADER, 0 );
	  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	  curl_setopt( $ch, CURLOPT_URL, $url );
	  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, TRUE );
	  $data = curl_exec( $ch );
	  curl_close( $ch );
	  return $data;
	}*/

	public function sentiment(){
		$json = array("sentence" => "The official account of FirstBank of Nigeria. Get solutions to all your financial needs as you journey through life. Download the #FirstMobile Banking App!");
		$api_key = 'simEZGYxsvpXGkW6vP6702ShKbA1';

		$ch = curl_init();
		$headers = array(
		  'Content-Type: application/json',
		  'Authorization: Simple ' . $api_key,
		  'Content-Length: ' . strlen(json_encode($json))
		);
		curl_setopt_array($ch, array(
		  CURLOPT_URL => 'https://api.algorithmia.com/v1/algo/nlp/SocialSentimentAnalysis/0.1.4',
		  CURLOPT_HTTPHEADER => $headers,
		  CURLOPT_POSTFIELDS => json_encode($json),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_POST => true
		));

		// run the algorithm and get the results (usually a JSON-encoded string)
		$response_json = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response_json);
		echo '<pre>';
		print_r($response);
	}

	public function autotag(){
		$json = array("We will be FLASHING you a lot of good news about Nigeria this year... #TheYearOfInfrastructure 🇳🇬🇳🇬 We are always here to serve u FREE of Charge 😁😁😁");
		$api_key = 'simEZGYxsvpXGkW6vP6702ShKbA1';

		$ch = curl_init();
		$headers = array(
		  'Content-Type: application/json',
		  'Authorization: Simple ' . $api_key
		  //'Content-Length: ' . strlen(json_encode($json))
		);
		curl_setopt_array($ch, array(
		  CURLOPT_URL => 'https://api.algorithmia.com/v1/algo/nlp/AutoTag/1.0.1',
		  CURLOPT_HTTPHEADER => $headers,
		  CURLOPT_POSTFIELDS => json_encode($json),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_POST => true
		));

		// run the algorithm and get the results (usually a JSON-encoded string)
		$response_json = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($response_json);
		echo '<pre>';
		print_r($response);
	}


	public function getTweets(){

		$arrContextOptions=array(
		    "ssl"=>array(
		        "verify_peer"=>false,
		        "verify_peer_name"=>false,
		    ),
		); 

		$response = file_get_contents("https://twitter.com/search?q=nigeria&count=100", false, stream_context_create($arrContextOptions));

		echo $response;

		/*$url = "https://twitter.com/search?q=nigeria";

		echo '<pre>';
		print_r(file_get_contents($url) );*/

	}

	// 31-05-2018 Get instagam images in products of logged in user
	public function recentImages(){
		$accestoken = $this->input->post('usertoken');
		$instauser = $this->input->post('user');
		$arrContextOptions=array(
		      "ssl"=>array(
		          "verify_peer"=>false,
		          "verify_peer_name"=>false,
		      ),
		  ); 

		$response = file_get_contents("https://api.instagram.com/v1/users/{$instauser}/media/recent/?access_token=".$accestoken, false, stream_context_create($arrContextOptions));

		$data = json_decode($response);

		if(empty($data) || is_null($data)) {
			$path = __DIR__.'\User.php function recentImages';
		    $line = 1421;
		    apilogger("Unbale to fetch user instagram images",$path,$line);
		}

		// to get the array with all resolutions
		$images = array();
		foreach( $data->data as $user_data ) {
		    $images[] = (array) $user_data->images;
		}
		// to get the array with standard resolutions
		$standard = array_map( function( $item ) {
		    return $item['standard_resolution']->url;
		}, $images );

		echo json_encode(array("images" => $standard));
		exit;
	}

	// User deactivate 
	public function deactivateSeller(){

	    $ratio = 0;
	  	// Calculate auto cancell orders last 14 days more than 10%
	   	$totalorders = $this->db->select('created_by as SellerID,COUNT(*) as Totalorders')
	    ->from('orders')
	    ->where('created_date >= CURDATE() - INTERVAL 14 DAY')
	    ->group_by('created_by')
	    ->get()->result();

	    // If orders founds
	    if(count($totalorders) > 0){
	    	foreach ($totalorders as $key => $order) {

	    		$totalcancelorders = $this->db->select('created_by as SellerID, COUNT(*) as Totalcancelorders')
				    ->from('orders')
				    ->where('auto_cancelled','1')
				    ->where('created_date >= CURDATE() - INTERVAL 14 DAY')
				    ->where('created_by',$order->SellerID)
				    ->get()->row();

				if($totalcancelorders->Totalcancelorders > 0){
		
					$ratio = ($order->Totalorders) / 10;
					$finalper = round($ratio);

					if($totalcancelorders->Totalcancelorders >= $finalper ){
						//echo $finalper;
						if(!empty($totalcancelorders->SellerID) && $totalcancelorders->SellerID != '19'){ // Check official account akinai
							
							$sellerinfo = admin_info($totalcancelorders->SellerID);
							$senddata = array('name' => $sellerinfo->admin_name);

							if($sellerinfo->is_active == '1'){
								// Send account suspension email to seller
								$mailConfig['mailpath'] = "/usr/sbin/sendmail";
						        $mailConfig['protocol'] = "sendmail";
						        $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
						        $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
						        $mailConfig['smtp_pass'] = "tg1<LvPL";
						        $mailConfig['smtp_port'] = "25";
						        $mailConfig['mailtype'] = "html";

						      	$this->load->library('email', $mailConfig);

						      	$body = $this->load->view('templates/seller_account_suspension_template',$senddata,TRUE);

						      	$this->email->set_newline("\r\n");   
						        $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
						        $this->email->message($body); 
						        $this->email->to($sellerinfo->admin_email);
						        $this->email->subject('Your account has been Suspended');

						        $send = $this->email->send();
						        // End

								//Deactivate seller
			    				$this->Admin_model->update(array("is_active" => '0'),$totalcancelorders->SellerID);
							}

						}
	    			}

				}
	    	}
	    }	

	  // Billing cycle of weekly dates	
      /*$friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week echo '<br>';
      $nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($friday))." +6 days"));

      $duedate = date('Y-m-d', strtotime($nextbill. ' + 3 days'));
      //var_dump($duedate);
      $orders = $this->db->select('created_by as SellerId,SUM(order_amount) as Total')
	    ->from('orders')
	    ->where('created_date >=', $friday)
	    ->where('created_date <=', $nextbill)
	    ->group_by('created_by')
	    ->get()->result();

	  if(count($orders) > 0){
	  	foreach ($orders as $value) {

	  		if($value->Total > 0){
		  		$this->db->where('payment_cycle_from >=', $friday);
		  		$this->db->where('payment_cycle_to <=', $duedate);
		  		$this->db->where('payment_by', $value->SellerId);
				$query = $this->db->get('payment_cycles');	
			
				if($query->num_rows() == 0){
					// Deactivate seller account if due date over
					if ( strtotime(date('Y-m-d')) > strtotime($duedate) ){
						$this->Admin_model->update(array("is_active" => '0'),$value->SellerId);
					}
				}
	  		}
	  	}

	  }*/
	}

	// Seller login with email and password 20-08-2018
	public function signUpwithLogin(){
		$this->load->library('user_agent');
		$this->load->library('ciqrcode');
		if($this->input->post()){

			$this->form_validation->set_rules('useremail','Email','trim|xss_clean|required');
        	$this->form_validation->set_rules('userpwd','Password','trim|xss_clean|required|callback_validatePassword'); // Check password format

        	if($this->form_validation->run() == true) {

        		$senddata = array(	
                    'admin_email'=> trim($this->input->post('useremail')),
                    'admin_password' => $this->_hash($this->input->post('userpwd',true))
                );

                if($this->isEmailExists($this->input->post('useremail'))){

                	$checkLogin = $this->Admin_model->get($senddata);

                	if($checkLogin){
                		if($checkLogin->login_with == 'seller' && $checkLogin->is_active == '1'){
                			$this->session->set_userdata('username',$checkLogin->admin_name);
                    		$this->session->set_userdata('isLogin','seller');
                    		$this->session->set_userdata('adminId',$checkLogin->admin_id);
		                	//$this->session->set_userdata('customerId',$checkLogin->admin_id);
                    		$admininfo = array();
							// 05-07-2018 Shortlinks
			              	if(is_null($checkLogin->catalog_shortlink)){
				              	$seller = $checkLogin->admin_id; // Remove encryption
						        $shorturl = base_url()."item/category/{$seller}";
						        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";
						        $ch = curl_init();
						        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
						        curl_setopt($ch,CURLOPT_URL,$postdata);
						        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
						        $response = curl_exec($ch);
						        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						        $content = json_decode($response);
						        curl_close($ch);

						        if(isset($content->error)){
							      $path = __DIR__.'\User.php function signUpwithLogin';
							      $line = 1554;
							      apilogger($content->error->message,$path,$line);
							    }
							    elseif(empty($content) || !$content) {
							      $path = __DIR__.'\User.php function signUpwithLogin';
							      $line = 1554;
							      apilogger("Error while generating short-link",$path,$line);
							    }
			  
						        if($content->result == true){
						          $admininfo['catalog_shortlink'] = $content->url;     
						        }
						        else{
						          $admininfo['catalog_shortlink'] = "";
						        }
			              	}

			              	if(is_null($checkLogin->catalog_qrcode)){
			              		$qr_image=rand().'.png';
						        $params['data'] = is_null($checkLogin->catalog_shortlink) ? $admininfo['global_link'] : $checkLogin->catalog_shortlink;//$this->input->post('finallink');
						        $params['level'] = 'H';
						        $params['size'] = 8;
						        $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
						        if($this->ciqrcode->generate($params))
						        {
						            $admininfo['catalog_qrcode']=$qr_image; 
						        }
			              	}

			              	if(is_null($checkLogin->catalog_qrcode) || is_null($checkLogin->catalog_shortlink)){
			              		$this->Admin_model->update($admininfo,$checkLogin->admin_id);
			              	}
					        echo "1";
                		}
                		else{
                			echo "* Sorry, You are not allowed to logs in.";
                		}
                	}
                	else{
	                	echo "* Invalid email address or password.";
                	}
            	}
            	else{
            		$admindata = array();
            		$logindata = array(
	        			"admin_email" 		=> trim($this->input->post('useremail')),
	        			"login_with"		=> 'seller',	
	        			"admin_password" 	=> $this->_hash($this->input->post('userpwd',true))
	        		);
	        		$inserted = $this->Admin_model->insert($logindata);

	        		// 05-07-2018 Catalog shortlink for seller
					$seller = $inserted; // Remove encryption

			        $shorturl = base_url()."item/category/{$seller}";

			        $postdata = "https://api.ritekit.com/v1/link/short-link?url={$shorturl}&client_id={$this->config->item('client_id')}&cta={$this->config->item('cta')}";

			        $ch = curl_init();
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			        curl_setopt($ch,CURLOPT_URL,$postdata);
			        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			        $response = curl_exec($ch);
			        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			        $content = json_decode($response);
			        curl_close($ch);

			        if(isset($content->error)){
				      $path = __DIR__.'\User.php function signUpwithLogin';
				      $line = 1554;
				      apilogger($content->error->message,$path,$line);
				    }
				    elseif(empty($content) || !$content) {
				      $path = __DIR__.'\User.php function signUpwithLogin';
				      $line = 1554;
				      apilogger("Error while generating short-link",$path,$line);
				    }

			        if($content->result == true){
			          $admindata['catalog_shortlink'] = $content->url;  
			          $qr_image=rand().'.png';
				      $params['data'] = $content->url;//$this->input->post('finallink');
				      $params['level'] = 'H';
				      $params['size'] = 8;
				      $params['savename'] =FCPATH."uploads/qr_image/".$qr_image;
				      if($this->ciqrcode->generate($params))
				      {
				        $admindata['catalog_qrcode']=$qr_image; 
				      }   
			        }
			        else{
			          $admindata['catalog_shortlink'] = "";
			          $admindata['catalog_qrcode']=""; 
			        }
			        $this->Admin_model->update($admindata,$inserted);
			        // End

	        		$checkLogin = $this->Admin_model->get($inserted);

	        		if($inserted){
	        			$this->session->set_userdata('username',$checkLogin->admin_name);
                    	$this->session->set_userdata('isLogin','seller');
                    	$this->session->set_userdata('adminId',$checkLogin->admin_id);

                    	// Send signup email to sellers
				        if ( ! defined('BASEPATH')) exit('No direct script access allowed');

				        $datas = array("guestname" => trim($this->input->post('useremail')));
				        
			            $mailConfig['mailpath'] = "/usr/sbin/sendmail";
			            $mailConfig['protocol'] = "sendmail";
			            $mailConfig['smtp_host'] = "relay-hosting.secureserver.net";
			            $mailConfig['smtp_user'] = "aix22mb4lcz8@godaddydomain.com";
			            $mailConfig['smtp_pass'] = "tg1<LvPL";
			            $mailConfig['smtp_port'] = "25";
			            $mailConfig['mailtype'] = "html";

		              	$this->load->library('email', $mailConfig);

		              	$body = $this->load->view('templates/seller_signup_template',$datas,TRUE);

		              	$this->email->set_newline("\r\n");   
			            $this->email->from('no-reply@pointslive.com', config_item('site_name'));    
			            //$this->email->message($body); 
			            $this->email->to(trim($this->input->post('useremail')));
			           	$this->email->subject('Thank You for Signing Up to Akinai');
	              		$this->email->message($body); 
	              		$this->email->attach(FCPATH.'Akinai_Seller_Center_User_Guide.pdf');

			            $send = $this->email->send();

			            echo "1";
	        		}
	        		else{
	        			echo "0";
	        		}
            	}
        	}
        	else{
        		echo validation_errors();
        	}
		}
	}

	// Password validation
	public function validatePassword($pass){
        // permitted characters throughout string ------------------------↓↓↓↓↓↓↓↓
        $checkpassword =  preg_match('/^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^\d]*\d)[a-zA-Z\d]{8,}$/',$pass);

        if($checkpassword){
        	return true;
        }
        else{
        	$this->form_validation->set_message('validatePassword', 'The Password field must be 8 letters and contains 1 uppercase,1 lowercase and 1 number.');
        	return false;
        }
        // required characters----------↑↑↑-------------↑↑↑-----------↑↑            ↑-minimum length (no max)
    }

	// Check email exists
    public function isEmailExists($key){
    	$checkemail = $this->Admin_model->email_exists($key);
    	if($checkemail){
    		return true;
    	}
    	else{
    		return false;
    	}

    }

     // Store information view
    public function storeview(){
    	$data['content'] = 'admin/usertabs/storeinfo';
	    $data['title'] = 'Store Information';
	    $this->load->view('admin/template', $data);
    }

    // Delivery return and servies area
    public function service_delivery(){
    	$data['content'] = 'admin/usertabs/services';
	    $data['title'] = 'Service Area and Delivery Returns';
	    $this->load->view('admin/template', $data);
    }

    // Social media channel connect
    public function socialchannel(){
    	$data['content'] = 'admin/usertabs/channels';
	    $data['title'] = 'Social Channel';
	    $this->load->view('admin/template', $data);
    }

    // Payments and billing tab view
    public function payment_billing(){
    	  $profiledata = $this->Admin_model->get(['admin_id' => $this->session->userdata('adminId')]);
	      // Total orders shown to admin and vendors in payment cycle
	      $friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week

	      if(date('Y-m-d') == date('Y-m-d',strtotime('friday'))){
	        $fridaydate = date('Y-m-d');
	      }else{
	        $fridaydate = $friday;
	      }

	      $nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($fridaydate))." +7 days"));
	      $duedate = date('Y-m-d', strtotime($nextbill. ' + 3 days'));
	      $commission = 0;
	      $autocancelled = 0;
	      $totalorderamount = 0;

	      if(!empty($this->session->userdata('isLogin'))){
	        	$weeklypayments = $this->db->select('*')->from('weekly_payment_generates')
	            ->where('payment_cycle_from =', $friday)
	            ->where('payment_cycle_to =', $nextbill)
	            ->where('seller_id',$this->session->userdata('adminId'))
	            ->where('payment_status','0')
	            ->group_by('seller_id')
	            ->get()->row();
	   
		        $orderdata = array(
		         'Totalamount' => isset($weeklypayments->billing_amount) ? $weeklypayments->billing_amount : 0,//($totalorderamount) - ($commission - $autocancelled),
		         'commission'  => $commission,
		         'autocancelledamt' => $autocancelled
		        ); 
	      }
	      else{
	      	$orderdata = [];
	      }
      	// END  
    	$data['content'] = 'admin/usertabs/payment_billing';
    	$data['orders'] = $orderdata;
    	$data['profileinfo'] = $profiledata;
	    $data['title'] = 'Payment and Billing Information';
	    $this->load->view('admin/template', $data);
    }
}
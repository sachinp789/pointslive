<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Address_model');
		$this->load->model('Orders_model');
	}

	// Save user shipping address
	public function addShippingAddress($userID=""){
		$address = '';
		$address.= trim($this->input->post('address_line1'));
		$address.=",\n";
		if(!empty($this->input->post('address_line2'))){
		$address.= trim($this->input->post('address_line2'));
		$address.=",\n";
		}
		$address.= trim($this->input->post('city'));
		$address.=",\n";
		$address.= trim($this->input->post('state'));
		$address.=",\n";
		$address.= trim($this->input->post('country')).",".$this->input->post('zipcode');

		$info = array(
				'user_id' 	=> $userID,
				'shipping_address'	=> $address
			);

		$saved = $this->Address_model->insert($info);
		$response = array();
		if($saved){
			$response['success'] = true;
		}
		else{
			$response['response'] = false;
		}
		redirect(base_url()."admin/productlistsByUserid/$userID");
	}

	public function saveAddress(){
		$addressid = $this->input->post('id');
		$this->session->set_userdata('shipping_address',$addressid);
		exit;
	}

	// List of users orders
	public function listOrders(){
	  $data = array();
	  $orders = orderlists();
      $data['content'] = 'admin/orders/list';
      $data['title'] = 'Orders';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// View order details
	public function viewOrder($orderID=""){
	  $orders = orderlists($orderID);
	  $data = array();
      $data['content'] = 'admin/orders/vieworder';
      $data['title'] = 'Order';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// Order shipment
	public function shipOrder($orderID=""){
		$data = array(
			 'id' => $orderID,	
			 'status' => '1',
			 'shipment_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->shiporder($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been dispatched.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to dispatch order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	// Order completion
	public function orderComplete($orderID=""){
		$data = array(
			 'id' => $orderID,	
			 'status' => '2',
			 'complete_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->ordercomplete($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been completed.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to complete order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}
}

?>
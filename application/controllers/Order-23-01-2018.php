<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Address_model');
		$this->load->model('Orders_model');
		$this->load->model('Stores_model');
		require_once('Admin.php');
	}

	// Save user shipping address
	public function addShippingAddress($userID=""){
		$address = '';
		$address.= trim($this->input->post('address_line1'));
		$address.=",\n";
		if(!empty($this->input->post('address_line2'))){
		$address.= trim($this->input->post('address_line2'));
		$address.=",\n";
		}
		$address.= trim($this->input->post('city'));
		$address.=",\n";
		$address.= trim($this->input->post('state'));
		$address.=",\n";
		$address.= trim($this->input->post('country')).",".$this->input->post('zipcode');

		$info = array(
				'user_id' 	=> $userID,
				'shipping_address'	=> $address
			);

		$saved = $this->Address_model->insert($info);
		$response = array();
		if($saved){
			$response['success'] = true;
		}
		else{
			$response['response'] = false;
		}
		redirect(base_url()."admin/productlistsByUserid/$userID");
	}

	public function saveAddress(){
		$addressid = $this->input->post('id');
		$this->session->set_userdata('shipping_address',$addressid);
		exit;
	}

	// List of users orders
	public function listOrders(){
	  $data = array();
	  $orders = orderlists();
      $data['content'] = 'admin/orders/list';
      $data['title'] = 'Orders';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// View order details
	public function viewOrder($orderID=""){
	  $orders = orderlists($orderID);
	  $data = array();
      $data['content'] = 'admin/orders/vieworder';
      $data['title'] = 'Order';
      $data['orders'] = $orders;
      $this->load->view('admin/template', $data);
	}

	// Order shipment
	public function shipOrder($orderID=""){

		$ordersdata = $this->Orders_model->fields('shipping_address,user_id,page_id,transaction_id')->get($orderID);

		$language = chooselanguage($ordersdata->user_id);
	    if(sizeof($language) > 0){
	      $lang = chooselanguage($ordersdata->user_id);
	      $language_code = $lang[0]->language;
	    }
	    else{
	      $language_code = "english";
	    }

	    //echo $language_code;exit;
	    lang_switcher($language_code);
		// Shipping orders pending to create
        $shipaddress = $this->Address_model->get($ordersdata->shipping_address);
      	$shiptype = "";
      	if($shipaddress->delivery_method == 'next_day'){
        	$shiptype = "NEXT_DAY";
      	}
      	if($shipaddress->delivery_method == 'express' || $shipaddress->delivery_method == 'ด่วน'){
          $shiptype = "EXPRESS_1_2_DAYS";
        }
        if($shipaddress->delivery_method == 'standard' || $shipaddress->delivery_method == 'มาตรฐาน'){
          $shiptype = "STANDARD_2_4_DAYS";
        }

      	$provinces = getStateByDistrict($shipaddress->district); // States shipping address

	    if($language_code == 'english'){
	       $state = $provinces[0]->province;
	    }else{
	       $state = $provinces[0]->province_thai;
	    }

      	// Get user information
      	if($ordersdata->page_id == "652803498256943"){
	    	$usertoken = "EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
	    }
	    else{
      	// Get user information
      		$userinfo = $this->Stores_model->fields('access_token')->get(array('page_id' => $ordersdata->page_id));
	    	$usertoken = $userinfo->access_token;		
	    }

      	$userurl = "https://graph.facebook.com/{$ordersdata->user_id}?access_token=".$usertoken;
    	$ch = curl_init();
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch,CURLOPT_URL,$userurl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    $results = curl_exec($ch);
	    curl_close($ch);
	    $content = json_decode($results);

	    $fullname = $content->first_name.' '.$content->last_name;
	    
      	$pageurl = "https://graph.facebook.com/{$ordersdata->page_id}?fields=phone,emails,name,hours,single_line_address&access_token=".$usertoken;
      	$ch = curl_init();
      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch,CURLOPT_URL,$pageurl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	    $results = curl_exec($ch);
	    curl_close($ch);
	    $businessaddress = json_decode($results);

	    $validatetoken = Admin::acommerceValidateAPI(); // Get Access Token of Logistic API

        if(!is_null($validatetoken)){
          $token = $validatetoken['token']['token_id'];
        }

	    $addressee = "";
      	$address1 = "";
      	$province = "";
      //$postalCode = "";
      	$country = "";
      	$phone = "";
      	$email = "";

      	if(isset($businessaddress->name)){
        	$addressee = $businessaddress->name;
      	}
      	if(isset($businessaddress->single_line_address)){
        	$address1 = $businessaddress->single_line_address;
        	$explodeaddr = explode(",", $businessaddress->single_line_address);
        	$provinces = explode(" ", $explodeaddr[2]);
        	$provincename = $provinces[1];
        	$postalCode = $provinces[2];
      	}
      
      	if(isset($businessaddress->phone)){
        	$phone = $businessaddress->phone;
      	}
      	if(isset($businessaddress->emails)){
        	$email = $businessaddress->emails[0];
      	}

      	$orders = orderlists($ordersdata->transaction_id);
                      
	    $orderItems = array();

	      foreach ($orders[0]->products as $key => $value) {
	          if($value->sale_price > 0){
	            $price = $value->sale_price;
	          }else{
	            $price = $value->product_price;
	          }
	          if($language_code == 'english'){
	            $pname = $value->product_name;
	          }else{
	            $pname = $value->product_name_thai;
	          }
	         $shiporderItems[] = array(
	          "itemDescription" => "{$pname}",
	          "itemQuantity"    => (int)$value->ordered_qty,
	          );
	    }

		    $shipordersDetails = [
	        "shipServiceType" => "DELIVERY",
	        "shipSender" => [
	            "addressee"=>"{$addressee}",
	            "address1"=>"{$address1}",
	            "city"=>"",
	            "province"=>"{$provincename}",
	            "postalCode"=>"{$postalCode}",
	            "country"=> "{$provincename}",
	            "phone"=> "{$phone}",
	            "email"=>"{$email}"
	        ],
	        "shipShipment" =>[
	            "addressee"=>"{$fullname}",
	            "address1"=>"{$shipaddress->shipping_address}",
	            "address2"=>"",
	            "subDistrict"=>"",
	            "district"=>"{$shipaddress->district}",
	            "city"=>"",
	            "province"=>"{$state}",
	            "postalCode"=>"{$shipaddress->postcode}",
	            "country"=>"{$shipaddress->country}",
	            "phone"=>"{$shipaddress->phone}",
	            "email"=>"{$shipaddress->email}"
	        ],
	        "shipPickup" =>[
	          "addressee"=>"{$addressee}",
	          "address1"=>"{$address1}",
	          "city"=>"",
	          "province"=>"{$provincename}",
	          "postalCode"=>"{$postalCode}",
	          "country"=> "{$provincename}",
	          "phone"=> "{$phone}",
	          "email"=>"{$email}"
	        ],
	        "shipShippingType"=>"{$shiptype}",
	        "shipPaymentType"=>"COD",
	        "shipCurrency"=>"THB",
	        "shipGrossTotal"=>(int)$orders[0]->order_amount,
	        "shipInsurance"=>false,
	        "shipPickingList"=>$shiporderItems,
	        "shipPackages"  => []
	      ]; 

	    $salesorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$ordersdata->transaction_id}",$token,$shipordersDetails);
	    //var_dump($salesorder);
	    if(isset($salesorder['http_code']) && $salesorder['http_code'] == 201){
			$data = array(
				 'id' => $orderID,	
				 'status' => '1',
				 //'order_type' => 'shipping',
				 'shipment_date' => date('Y-m-d h:i:s') 				
			);
			$update = $this->Orders_model->shiporder($data);
			if($update){
				// Barcode generate TYPE_CODE_128 of order id
	            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
	            $string = base64_encode($generator->getBarcode("{$ordersdata->transaction_id}", $generator::TYPE_CODE_128));
	           
	            $image = base64_decode($string);
	            $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
	            $filename = $image_name . '.' . 'png';
	            //rename file name with random number
	            $path = FCPATH.'uploads/barcodes/';
	            //image uploading folder path
	            file_put_contents($path . $filename, $image);

	            $imageURL = base_url()."uploads/barcodes/".$filename;
	            //echo $imageURL;
	            $sendorder = Admin::sendImageToBotAPI($usertoken,$ordersdata->page_id,$ordersdata->user_id,$imageURL);
	            //print_r($sendorder);
	            if(isset($sendorder->attachment_id)){
	              $message_to_reply = $this->lang->line('orderidbarcode');

	              $responsee = [
	                  'recipient' => [ 'id' => $ordersdata->user_id ],
	                  'message' => [ 'text' => $message_to_reply]
	              ];
	             
	              $chinit = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$usertoken);  
				  curl_setopt($chinit, CURLOPT_POST, 1);
				  curl_setopt($chinit, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				  curl_setopt($chinit, CURLOPT_POSTFIELDS, json_encode($responsee));
				    //Set the content type to application/json
				  curl_setopt($chinit, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				  curl_exec($chinit);	

	              // Barcode generate TYPE_CODE_128 of product_sku+payment
	              $skupaymetnbarcode = $orders[0]->products[0]->product_sku.'COD';
	              $generatornew = new \Picqer\Barcode\BarcodeGeneratorPNG();
	              $stringcode = base64_encode($generatornew->getBarcode("{$skupaymetnbarcode}", $generator::TYPE_CODE_128));
	             
	              $image = base64_decode($stringcode);
	              $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
	              $filenamenew = $image_name . '.' . 'png';
	              //rename file name with random number
	              $path = FCPATH.'uploads/barcodes/';
	              //image uploading folder path
	              file_put_contents($path . $filenamenew, $image);

	              $imageURL2 = base_url()."uploads/barcodes/".$filenamenew;

	              $send = Admin::sendImageToBotAPI($usertoken,$ordersdata->page_id,$ordersdata->user_id,$imageURL2);

	              if(isset($send->attachment_id)){
	                $message_to_reply = $this->lang->line('skubarcode');

	                $response1 = [
	                    'recipient' => [ 'id' => $ordersdata->user_id ],
	                    'message' => [ 'text' => $message_to_reply]
	                ];

	                $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$usertoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response1));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);

	                $message_to_reply = $this->lang->line('trackingcode');

	                $response = [
	                    'recipient' => [ 'id' => $ordersdata->user_id ],
	                    'message' => [ 'text' => $message_to_reply]
	                ];

	                $ch1 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$usertoken);  
				    curl_setopt($ch1, CURLOPT_POST, 1);
				    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch1);
	              }
	            }
				$this->session->set_flashdata('flashmsg','Order has been dispatched.');
			    $this->session->set_flashdata('msg', 'success');
			}
			else{
				$this->session->set_flashdata('flashmsg','Error to dispatch order.');
	        	$this->session->set_flashdata('msg', 'danger');
			}			
	    }
	    else{
	    	$this->session->set_flashdata('flashmsg',"422 code. Validation failed");
	        $this->session->set_flashdata('msg', 'danger');
	    }
		redirect(base_url().'order/listOrders');
	}

	public function sendMail(){
		exit;
		$config = emailconfig();
        $this->load->library('email',$config);       
        $this->email->set_newline("\r\n");
 
        $this->email->from('no-reply@pointslive.com', config_item('site_name'));
        $this->email->to("sachin.prajapati@searchnative.in");
        $this->email->subject('Test mail');
        $this->email->message("How are you");

        try {
            if ($this->email->send()) {
            	$this->session->set_flashdata('flashmsg', 'Please check your email to reset your password.');
            	$this->session->set_flashdata('msg', 'success');
            }
            else{
            	$this->session->set_flashdata('flashmsg', 'Error while send mail! Please try again or try after sometime.');
            	$this->session->set_flashdata('msg', 'danger');
            }

        } catch (Exception $e) {
        	$this->session->set_flashdata('flashmsg', 'Error while send mail! Please try again or try after sometime.');
            $this->session->set_flashdata('msg', 'danger');
        }

	}

	// Shipping and sales orders status updatation check API and sync with database
	public function syncOrdersStatus(){

		$shiporders = array();
		$validatetoken = Admin::acommerceValidateAPI(); // Get Access Token of Logistic API

        if(!is_null($validatetoken)){
          $token = $validatetoken['token']['token_id'];
        }

        $orders = $this->Orders_model->get_all();

        if(count($orders) > 0){
	        foreach ($orders as $key => $value) {
	        
		      	$userurl = "https://shipping.api.acommercedev.com/partner/1163/shipping-order-status/id?id={$value->transaction_id}";
		    	$ch = curl_init();

		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$token}",'Content-Type:application/json']);
			    $results = curl_exec($ch);
			    $shipdata = json_decode($results);
			    $headerinfo = curl_getinfo($ch);
		
			  	if(!is_null($shipdata)) { //!isset($shipdata[0]->error->code
			  		foreach ($shipdata as $key => $value) {
			  			$shiporders[] = $value;
			  		}
			  	}
		    }

		    if(count($shiporders) > 0){
		    	// shipOrderId
		    	foreach ($shiporders as $key => $ship) {
		    		if(!empty($ship->shipPartnerSalesOrderID)){

	    				$checkStatus = $this->Orders_model->get(array('transaction_id' => $ship->shipOrderId));

	    				if($ship->shipOrderId == $checkStatus->transaction_id){
	    					//$status = end($ship->shipOrderStatus)->shipOrderStatus; 23-01-2018
	    					$status = end($ship->shipPackage[0]->statusHistory);
	    					//foreach ($packages as $key => $shippack) {
	    						if($status == 'IN_TRANSIT' && $checkStatus->status == 0){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '1'));
	    						}
	    						else if($status == 'DELIVERED'){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '2'));
	    						}
	    						else if($status == 'CANCELLED'){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '3'));
	    						}
	    						else if($status == 'REJECTED_BY_CUSTOMER'){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '4','status_updates' => 0));
	    						}
	    						else if($status == 'FAILED_TO_DELIVER'){
	    							$this->Orders_model->where('transaction_id',$checkStatus->transaction_id)->update(array('status' => '5'));
	    						}
	    					//}
	    				}
	    			}	
	    		}

		    	foreach ($orders as $key => $value) {
				// Send message to chatbot users for status updates	
			    $pgtoken = "";
			    if($value->page_id == "652803498256943"){
			    	$pgtoken="EAAYoW0sPjp0BAPLZAiz7seXSxpHdHgRHcnHBZClY8zL4SzE125OOSsHlXsZB116EvbH7MZCENSIVvl7LldZB6lHonXSlOSbKeoudKIoWf66lz6EZBtrWjXUaqmYf52VDhnEa8ZCNxKLXnJYirvWPGagaddI2ZCdW6wvjnqR2srDSJeBVYfvbWmtr";
			    }
			    else{
			   	 $userinfo = $this->Stores_model->fields('access_token')->get(array('page_id' => $value->page_id));
			   	 $pgtoken = $userinfo->access_token;
			    }	

			    $language = chooselanguage($value->user_id);
			    if(sizeof($language) > 0){
			      //$lang = chooselanguage($value->user_id);
			      $language_code = $language[0]->language;
			    }
			    else{
			      $language_code = "english";
			    }
			    lang_switcher($language_code);

		      	$userurl = "https://graph.facebook.com/{$value->user_id}?access_token=".$pgtoken;
		    	$ch = curl_init();
		      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_URL,$userurl);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			    $results = curl_exec($ch);
			    curl_close($ch);
			    $content = json_decode($results);

			    $fullname = $content->first_name.' '.$content->last_name;

			    // Status is 'Dispatched'
			    if($value->status == '1' && $value->status_updates == 0){
			    	$findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_ship'));
		    		$message_to_reply = $findstring;
			    }
			    // Status is 'Complete'
			    else if($value->status == '2' && $value->status_updates == 0){
			    	//echo $checkStatus->transaction_id;
			    	$findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_delivered'));
		    		$message_to_reply = $findstring;
			    }
			    // Status is 'Cancelled'
			    else if($value->status == '3' && $value->status_updates == 0){

				    $findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_cancelled'));
				    $findstring = str_replace("<orderno>", $value->transaction_id, $findstring);

		    		$message_to_reply = $findstring;

		    		$message_to_reply_visit = $this->lang->line('visitsite');

		            $response2 = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply_visit]
		            ];

		            $chinit1 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($chinit1, CURLOPT_POST, 1);
				    curl_setopt($chinit1, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($chinit1, CURLOPT_POSTFIELDS, json_encode($response2));
				    //Set the content type to application/json
				    curl_setopt($chinit1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($chinit1);
				    curl_close($chinit1);
			    }
			    // Status is 'Rejected'
			   	else if($value->status == '4' && $value->status_updates == 0){
				    // Get user information
				    $findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_rejected'));
		    		$message_to_reply = $findstring;
			    }
			    // Status is 'Failed'
			    else if($value->status == '5' && $value->status_updates == 0){
				    // Get user information
				    $findstring = str_replace("<buynerfbname>", $fullname, $this->lang->line('is_failed'));
		    		$message_to_reply = $findstring;
				 }

			    // Send all message using CURL
			    $response = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply]
		            ];

	            $ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
			    curl_setopt($ch2, CURLOPT_POST, 1);
			    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
			    //Attach our encoded JSON string to the POST fields.
			    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
			    //Set the content type to application/json
			    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			    curl_exec($ch2);
			    curl_close($ch2);
			    // END

			    if($value->status == '1' && $value->status_updates == 0){ // IF status is 'dispatched'
			    	// Send tracking code message to users
			    	$message_to_reply = $this->lang->line('visittracksite');
		    		$findstring = str_replace("http://tracking.acommerce.asia/smartship", "http://tracking.acommerce.asia/smartship?id={$value->transaction_id}", $message_to_reply);

		            $responsees = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $findstring]
		            ];
		            $ch5 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch5, CURLOPT_POST, 1);
				    curl_setopt($ch5, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch5, CURLOPT_POSTFIELDS, json_encode($responsees));
				    //Set the content type to application/json
				    curl_setopt($ch5, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch5);
				    curl_close($ch5);
			    }
			    else if($value->status == '5' && $value->status_updates == 0){ // IF status is 'failed'
			    	// Send good time deliver message to users	
			    	$message_to_reply = $this->lang->line('godtime');

		    		$response = [
		                'recipient' => [ 'id' => $value->user_id ],
		                'message' => [ 'text' => $message_to_reply]
		            ];

		    		$ch2 = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$pgtoken);  
				    curl_setopt($ch2, CURLOPT_POST, 1);
				    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				    //Attach our encoded JSON string to the POST fields.
				    curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($response));
				    //Set the content type to application/json
				    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				    curl_exec($ch2);
				    curl_close($ch2);
			    }
			    $this->Orders_model->where('transaction_id',$value->transaction_id)->update(array('status_updates' => 1));
			  }
	    	}
	    	else{
	    		echo "API response not found";
	    		exit;
	    	}
       }
	}

	// Order shipment
	/*public function shipOrder($orderID=""){
		$data = array(
			 'id' => $orderID,	
			 'status' => '1',
			 'shipment_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->shiporder($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been dispatched.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to dispatch order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}*/

	// Order completion
	public function orderComplete($orderID=""){
		$data = array(
			 'id' => $orderID,	
			 'status' => '2',
			 'complete_date' => date('Y-m-d h:i:s') 				
		);
		$update = $this->Orders_model->ordercomplete($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been completed.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to complete order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	// Cancel order
	public function cancelOrder($orderID=""){
		$data = array(
			 'id' => $orderID,	
			 'status' => '3'
		);
		$update = $this->Orders_model->cancelorder($data);
		if($update){
			$this->session->set_flashdata('flashmsg','Order has been cancelled.');
		    $this->session->set_flashdata('msg', 'success');
		}
		else{
			$this->session->set_flashdata('flashmsg','Error to cancel order.');
        	$this->session->set_flashdata('msg', 'danger');
		}			
		redirect(base_url().'order/listOrders');
	}

	function updateInventory(){
		$this->load->model('Products_model');

		$validatetoken = Admin::acommerceValidateAPI(); // Get Access Token of Logistic API

        if(!is_null($validatetoken)){
          $token = $validatetoken['token']['token_id'];

        }
     	/*$data = $this->db->query('SELECT max(inventory_last_updated) FROM products')->row();*/
     	$data = $this->db->from('products')->order_by('inventory_last_updated', 'desc')->get()->row();
     	$last_updated_time = $data->inventory_time;
     	if(!empty($last_updated_time)){

		$userurl = "https://fulfillment.api.acommercedev.com/channel/demoth1/allocation/merchant/1163?since=$last_updated_time";
		
		}else{

		$userurl = "https://fulfillment.api.acommercedev.com/channel/demoth1/allocation/merchant/1163?since=2018-01-19T07:07:14.479Z";

		}
		$ch = curl_init();

      	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch,CURLOPT_URL,$userurl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$token}",'Content-Type:application/json']);
	    $results = curl_exec($ch);
	   
	    $shipdata = json_decode($results);
			  
			if(count($shipdata) > 0){

		    foreach ($shipdata as $rows) {
					$sku = $rows->sku;
					$product = $this->Products_model->get(['product_sku'=>$sku]);
					
					$reserved_qty = reservedQty($product->product_id);
				
					if(empty($reserved_qty)){
						$reserved = 0;
					}else{

						$reserved = $reserved_qty;
					}
					
					$new_qty = $rows->qty;
				
					$total_qty = $reserved + $new_qty;
					
					$inventory_time = $rows->updatedTime;

					$now = new DateTime($inventory_time);		

					$last_updated = $now->format('Y-m-d H:i:s');

					$update_data = ['total_qty'=>$total_qty,'inventory_time'=>$inventory_time,'inventory_last_updated'=>$last_updated];
					$this->Products_model->where('product_sku',$sku)->update($update_data);
					$update_data = '';
				

		    }  // end foreach
		}  // end if
	}

}

?>
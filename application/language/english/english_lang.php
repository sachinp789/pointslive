<?php



defined('BASEPATH') OR exit('No direct script access allowed');



$lang['mostpopular'] = "Most Popular";

$lang['discover'] = "Discover Products";

$lang['counterservice'] = "Counter Service"; //Most Popular

$lang['sellerpickup'] = "Track a Package"; //Discover Products,Seller Pickup Service

$lang['viewmore'] = "View More";

$lang['aboutpage'] = "About Page";

$lang['faq'] = "FAQ";

$lang['endchat'] = "End Chat";

$lang['product/services'] = "Product/Services";

$lang['moreproduct'] = "More Products";

$lang['event'] = "Event";

$lang['viewevent'] = "View Event";

$lang['clickviewmoreevent'] = "Click below to see more events";

$lang['clickviewmoreproduct'] = "Click below to see more products";

$lang['viewmore event'] = "View More Events";

$lang['moreevents'] = "More Events";

$lang['moreopt'] = "More options";

$lang['addtocart'] = "Add to Cart";

$lang['buy'] = "Buy";

$lang['cartupdated'] = "Your basket has been updated with";

//feedaback tayo 
$lang['addbasket'] = "Great ! what do you want to do next ?"; // Do you want to continue to update products in basket ?

$lang['yes'] = "Yes";

$lang['no'] = "No";

$lang['updatecart'] = "Update Cart";

$lang['updateqty'] = "Update Quantity";

$lang['removeproduct'] = "Remove Product";

$lang['removebasket'] = "Item has been removed from basket.";

$lang['productconfirm'] = "Thanks. To confim, you want";

$lang['willcost'] = "will cost";

$lang['enterqty'] = "Please enter quantity";

$lang['qtyupdated'] = "Quantity has been updated as";

$lang['haveitem'] = "Hey, You already have products in your cart. Do you want checkout ?";

$lang['whatisit'] = "what is it ?";

$lang['enquiries'] = "Here is a list of General Enquiries";

$lang['looking'] = "Here is what you are looking for !"; 

$lang['noproduct'] = "No products found for"; 

$lang['still'] = "Still want to findout pointslive ?";

$lang['unit_confirm'] = "please confirm how many units you want.";

$lang['wantbuy'] = "please enter the name of the product you want to buy";

$lang['nocatfound'] = "No products found for category";

$lang['searchproduct'] = "Please type to search products.";

$lang['nocat'] = "No categories found.";

$lang['noevent'] = "No events found.";

$lang['noaddress'] = "No address found.";

$lang['noproductfound'] = "No products found.";

$lang['sorry'] = "sorry that information is not available";

$lang['pm'] = "thanks for your feedback. I will connect you with the Page Manager";

$lang['qtygreaterthanzero'] = "Quantity must be greater than 0.";

$lang['qtynum'] = "Please enter quantity amount in numeric format.";

$lang['wishtobuy'] = "please enter the name of the product you want to buy";

$lang['wanttoaddcart'] = "Do you want to add Product to Cart ?";

$lang['contact'] = "Contact Human Manager on Facebook";

$lang['plt'] = "Opening Times";//"What are your opening and closing times ?";

$lang['plorder'] = "Delivery Status";//"Check My Order Status";

$lang['ploffice'] = "Location";//"Do you have an Office ?";

$lang['plissue'] = "I have an Issue";

$lang['deliveryopt'] = "Delivery Options";

$lang['returnpolicy'] = "Return Policy";

$lang['website'] = "Website";

$lang['insta'] = "Instagram";

$lang['talkus'] = "Talk to Us";

$lang['endmsg'] = "Sorry I don’t understand what you typed. I can help you find out more about this pointslive though";

$lang['other'] = "Product Search"; //"Other";

$lang['pname'] = "Name";

$lang['pqty'] = "Qty";

$lang['pprice'] = "Price";

$lang['subtotal'] = "Subtotal";

$lang['shipcharge'] = "Shipping charge";

$lang['gt'] = "Grand Total";

$lang['checkout'] = "Checkout";

$lang['continueshopping'] = "Continue Shopping";

$lang['shipcost'] = "Cost";

$lang['shiporder'] = "Order";

$lang['shipamt'] = "Amount";

$lang['shipstatus'] = "Status";

$lang['ordersuccess'] = "Success";

$lang['orderpending'] = "Pending";

$lang['orderrej'] = "Rejected";

$lang['ordercan'] = "Canceled";

$lang['orderfail'] = "Failed";

$lang['orderprocess'] = "Processing";

$lang['orderdispatch'] = "Dispatched";

$lang['ordercomplete'] = "Complete";

//$lang['paybtn'] = "Pay";

$lang['shipaddress'] = "Shipping Address";

$lang['address_line1'] = "Address Line 1";

$lang['address_line2'] = "Address Line 2";

$lang['acity'] = "City";

$lang['astate'] = "State";

$lang['acountry'] = "Country";

$lang['zipcode'] = "Zipcode";

$lang['choose'] = "Choose..";

$lang['noitems'] = "No items in shopping cart.";

$lang['paymentdone'] = "Payment has been done.";

$lang['paysuccess'] = "Success ! Payment has been successfully received.";

$lang['orderrec'] = "Your order has been received. Thank you for your purchase !";

$lang['orderis'] = "Your order # is";

$lang['outstock'] = "Requested product out of stock.";

$lang['qtynotavail'] = "Requested quantity not available for";

$lang['mean'] = "Thank you. Did you mean";

$lang['qtyoutstock'] = "Requested quantity not available";

$lang['checkoutmost'] = "Check out our most popular Products, Services"; // and Events

$lang['browse'] = "You can browse through our list of Product and Service Categories";

$lang['continueaddcart'] = "Do you want to​ ​continue to update products in Shopping Cart ?"; // add

$lang['noitemcart'] = "Looks like there are No Items in your Shopping Cart";

$lang['cannotadd'] = "You can not add more than one product in shopping cart.";

$lang['method_v1'] = "Checkout V2";

$lang['method_v2'] = "Checkout V2.1";

$lang['method_message'] = "Please specify the checkout flow";

$lang['delivery_speed'] = "Please choose a Delivery Speed";

$lang['delivery_pickup'] = "Do you want to pick up your order or have it delivered to you ?";

$lang['delivery_pickupopt1'] = "PICKUP";

$lang['delivery_pickupopt2'] = "DELIVERY";

$lang['delivery_pickupchoose'] = "choose between PICKUP & DELIVERY";

$lang['delivery_postcode'] = "Please enter your postcode";

$lang['delivery_shippingpostcode'] = "Please enter shipping Postcode";

$lang['delivery_address'] = "Please enter your Address";

$lang['delivery_pay'] = "How do you want to Pay.";

$lang['delivery_paycash'] = "CASH";

$lang['delivery_payprepaid'] = "PREPAID";

$lang['delivery_nextday'] = "NEXT_DAY";

$lang['delivery_express'] = "EXPRESS";

$lang['delivery_standard'] = "STANDARD";

$lang['pickup_soon'] = "Service coming soon !";

$lang['valid_postcode'] = "Please enter correct postcode";

$lang['choose_district'] = "Please choose your District";

$lang['choose_shipdistrict'] = "Please choose a District";

$lang['enter_country'] = "Please enter country";

$lang['enter_phone'] = "Please enter phone number";

$lang['enter_email'] = "Please enter email address";

$lang['choose_address'] = "Please enter your Address";

$lang['choose_shipaddress'] = "Please enter shipping Address";
$lang['enter_shipername'] = "Enter shipping name";

$lang['howpay'] = "How do you want to Pay.";

$lang['paybtn'] = "Payment";

$lang['cashthanks'] = "Thank you for shopping at <pagename>. Your Order Number is <ordernumber>";

$lang['smartchip'] = "Thank you for using Smartship Counter Service !";

$lang['keepstatus'] = "I will keep you updated with your order status via this chat";

$lang['pay_counter'] = "Do you want to pay Cash at the Counter or Debit/Credit Card ?";

$lang['is_process'] = "Your order <orderno> has been received and is being processed.";

$lang['is_cancelled'] = "Hello <buynerfbname>. Your order <orderno> has been cancelled.";

$lang['is_ship'] = "Hello <buynerfbname>. Good News ! Your order has been shipped !";

$lang['is_rejected'] = "Hello <buynerfbname>. Sorry you did not like your order.";

$lang['is_failed'] = "Hello <buynerfbname>. The delivery man was not able to get to you today";

$lang['is_delivered'] = "Hello <buynerfbname>. Thank you for shopping with us. Hope you enjoy your order";

$lang['godtime'] = "Please let me know a good time to deliver to order";

$lang['trackingcode'] = "Here are your Tracking Numbers. Tap each one to generate a barcode and show to Counter staff.";

$lang['visitsite'] = "For more details please visit points live page.";

$lang['visittracksite'] = "For more details please visit http://tracking.acommerce.asia/smartship";

$lang['orderidbarcode'] = "OrderID barcode is generated as above.";

$lang['skubarcode'] = "Product SKU + Payment barcode is generated as above.";

$lang['erremail'] = "Please enter valid email address.";

$lang['errphone'] = "Please enter valid phone number.";

$lang['errphonenum'] = "Number should be 10 digits and start with 0.";

$lang['pkmsg'] = "Please select package.";

$lang['pksmall'] = "Small";

$lang['pkmedi'] = "Medium";

$lang['pklarge'] = "Large";

$lang['pkexists'] = "You can ship only one package at a time.";

$lang['endmsgsmart'] = "Sorry I don’t understand what you typed. I can help you find out more about this Smartship Thailand though";

$lang['code'] = "422 code. Validation failed.";
$lang['shippackage'] = "How many Packages do you want to ship ?";
$lang['wantshipmore'] = "Wants to ship more than 1 quantity ?";
$lang['anotership'] = "Please enter another shipping address details.";
$lang['havecode'] = "Have a coupon code ?";
$lang['entercoupon'] = "Please enter coupon code";
$lang['codedeactive'] = "Coupon code is deactivated";
$lang['codenotvalid'] = "Please re-enter coupon number in text box or continue with checkout";// Invalid coupon code
$lang['codeexpired'] = "Please re-enter coupon number in text box or continue with checkout"; // Coupon code expired
$lang['codeused'] = "Maximum coupon redemption limit reached";
$lang['codeminimumamount'] = "Minimum order amount must be";
$lang['codesuccess'] = "Coupon code has been successfully applied";
$lang['useaddress'] = "Use Address";
$lang['newaddress'] = "Enter New Address"; //Add Address
$lang['newaddresstitle'] = "Use this to enter new address"; //Use for enter new address
$lang['pickaddress'] = "Choose delivery address";
$lang['removecode'] = "If you want to remove coupon code then click on 'yes' button or proceed to checkout";
$lang['removecoupon'] = "Coupon code has been removed successfully";
$lang['enterorderid'] = "Please enter your Tracking Number";
$lang['findorder'] = "This package is currently <shipping_status>. For more information please follow this link <tracking_url>";
$lang['findorderfailed'] = "Sorry we cannot find this package on our system. Please check the Tracking Number";
$lang['pkglimit'] = "Packages must be between 1 to 10 numbers";
$lang['specifyciti'] = "Please specify city to ship product";
$lang['citimissing'] = "Shipping city missing";
$lang['same_address'] = "Same Address";
$lang['different_address'] = "Different Address";
$lang['store_info'] = "Store Information";
$lang['delivery_return'] = "Delivery & Returns";
$lang['aboutus'] = "About Us";
$lang['policyres'] = "7 days, 14 days, 30 days, No Returns Allowed";
$lang['sitelink'] = "Check us out at <websiteurl>";
$lang['instalink'] = "<instapagelink>";
$lang['talktous'] = "Contact us on +66123456789 or help@ccare.com";
$lang['nostock'] = "Sorry we don’t have anymore coupons to give out";//"Sorry we don't have stock";
$lang['backstock'] = "There will be more at our next event. Do you want us to reserve a coupon for you ?";//"Do you want us to let you know when it's back in stock ?";
$lang['great'] = "Great ! we'll message you once it's back";
$lang['noproblem'] = "No problem. Discover our other products";
$lang['meantime'] = "In the meantime, Discover our other products";
$lang['havelocation'] = "Have a opening times and location";
$lang['havedelivery'] = "Have a delivery and returns";
$lang['haveabout'] = "Have a about us";
$lang['whatliketodo'] = "What would you like to do ?";

//05-02-2018 (m-d-y)
$lang['digital'] = "This is your Digital Receipt that confirms your order and payment";
$lang['shop'] = "Enter Shop";
$lang['enter'] = "Please click below button to enter in shop";

//29-05-2018
$lang['vendortime'] = "Have a opening times";
$lang['alldayopen'] = "24 hours";
$lang['noservice'] = "No delivery services found";
$lang['offerservice'] = "We offer this delivery options :";
$lang['nowebsite'] = "No website found";
$lang['noinstasite'] = "No instagram website found";
$lang['nocontact'] = "No contact information found";
$lang['contactinfo'] = "Contact us on";
$lang['flutter'] = "Flutterwave";
$lang['paycancel'] = "Payment has been cancelled";
$lang['payfailed'] = "Payment has been failed by payment gateway";
$lang['endmsgchat'] = "Sorry I don’t understand what you typed. I can help you find out more about this <page> though";

// 07-06-2018
$lang['entername'] = "Please enter your name";
$lang['entercity'] = "Please enter city";
$lang['enterpostal'] = "Please enter postal code";
$lang['enterstate'] = "Please enter state name";
$lang['entercountry'] = "Please enter country name";
$lang['enteraddress'] = "Please enter shipping address";
$lang['validphone'] = "Enter valid nigerian phone number";
$lang['ratemessage'] = "Thank you. This will help us provide better service to you :)";

//21-06-2018
$lang['nigriastate'] = "Please enter the State in Nigeria you want us to deliver to";
$lang['localgovt'] = "Please choose your Local Government";
$lang['statevalid'] = "Please enter correct state";
$lang['morearea'] = "Click to load More local area";
$lang['loadarea'] = "More area";

$lang['endchatmsg'] = "Sorry I don't understand that.";
$lang['endchatmsg2'] = "Scroll up and choose another option or use Sticky Button to go Home";
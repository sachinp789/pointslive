<?php



defined('BASEPATH') OR exit('No direct script access allowed');



$lang['mostpopular'] = "ที่นิยมมากที่สุด";

$lang['discover'] = "ค้นหาผลิตภัณฑ์";

$lang['counterservice'] = "เคาน์เตอร์เซอร์วิส"; //Most Popular

$lang['sellerpickup'] = "ติดตามแพ็กเกจ"; //Discover Products,บริการ Pickup ผู้ขาย

$lang['viewmore'] = "ดูเพิ่มเติม";

$lang['aboutpage'] = "เกี่ยวกับหน้านี้";

$lang['faq'] = "คำถามที่พบบ่อย";

$lang['endchat'] = "สิ้นสุดการแชท";

$lang['product/services'] = "สินค้า / บริการ";

$lang['moreproduct'] = "ผลิตภัณฑ์อื่น ๆ";

$lang['event'] = "กิจกรรม(อีเว้นท์)";

$lang['viewevent'] = "ดูกิจกรรม";

$lang['clickviewmoreevent'] = "คลิกด้านล่างเพื่อดูกิจกรรมเพิ่มเติม";

$lang['clickviewmoreproduct'] = "คลิกด้านล่างเพื่อดูผลิตภัณฑ์อื่น ๆ";

$lang['viewmore event'] = "ดูกิจกรรมเพิ่มเติม";

$lang['moreevents'] = "กิจกรรมอื่น ๆ";

$lang['moreopt'] = "ตัวเลือกเพิ่มเติม";

$lang['addtocart'] = "เพิ่มในรถเข็น";

$lang['buy'] = "ซื้อ";

$lang['cartupdated'] = "ตะกร้าของคุณได้รับการเปลี่บนแปลงด้วย";

//feedback tayo	
$lang['addbasket'] = "ดี! คุณต้องการทำอะไรต่อไป?";//คุณต้องการเพิ่มผลิตภัณฑ์ในตะกร้าสินค้าหรือไม่?

$lang['yes'] = "ใช่";

$lang['no'] = "ไม่";

$lang['updatecart'] = "อัปเดตรถเข็น";

$lang['updateqty'] = "อัพเดตจำนวน";

$lang['removeproduct'] = "ลบผลิตภัณฑ์";

$lang['removebasket'] = "รายการถูกนำออกจากตะกร้าแล้ว";

$lang['productconfirm'] = "ขอบคุณ หากต้องการยืนยันคุณต้องการ";

$lang['willcost'] = "จะเสียค่าใช้จ่าย";

$lang['enterqty'] = "กรุณาใส่จำนวนที่คุณต้องการ";

$lang['qtyupdated'] = "จำนวนที่ได้รับการเปลี่ยนแปลงเป็น";

$lang['haveitem'] = "Hey, คุณมีสินค้าในตะกร้าแล้ว คุณต้องการเช็คเอาต์ไหม";

$lang['looking'] = "นี่คือสิ่งที่คุณกำลังมองหา!";

$lang['noproduct'] = "ไม่พบผลิตภัณฑ์สำหรับ";

$lang['still'] = "คุณยังต้องการค้นหาใน pointlive?";

$lang['unit_confirm'] = "โปรดยืนยันจำนวนที่คุณต้องการ";

$lang['wantbuy'] = "กรุณาใส่ชื่อผลิตภัณฑ์ที่คุณต้องการซื้อ";

$lang['nocatfound'] = "ไม่พบผลิตภัณฑ์สำหรับหมวดหมู่";

$lang['searchproduct'] = "โปรดพิมพ์เพื่อค้นหาผลิตภัณฑ์";

$lang['nocat'] = "ไม่พบกิจกรรมที่คุณต้องผาร";

$lang['noevent'] = "ไม่มีที่อยู่";

$lang['noaddress'] = "ไม่พบที่อยู่";

$lang['noproductfound'] = "ไม่พบผลิตภัณฑ์ที่คุณต้องการ";

$lang['sorry'] = "ขออภัยข้อมูลนี้ไม่สามารถใช้ได้";

$lang['pm'] = "ขอบคุณสำหรับความคิดเห็นของคุณ ฉันจะเชื่อมต่อคุณกับ Page Manager";

$lang['qtygreaterthanzero'] = "จำนวนต้องมากกว่า 0";

$lang['qtynum'] = "โปรดป้อนจำนวนเงินในรูปแบบตัวเลข";

$lang['wishtobuy'] = "กรุณาใส่ชื่อผลิตภัณฑ์ที่คุณต้องการซื้อ";

$lang['wanttoaddcart'] = "คุณต้องการเพิ่มสินค้าในรถเข็นหรือไม่?";

$lang['contact'] = "ติดต่อ Human Manager บน Facebook";

$lang['plt'] = "เวลาเปิด";//"เวลาเปิดและปิดของคุณคืออะไร?";

$lang['plorder'] = "สถานะการจัดส่ง";//"ตรวจสอบสถานะการสั่งซื้อของฉัน";

$lang['ploffice'] = "คุณมีออฟฟิศไหม";//"คุณมีออฟฟิศไหม?";

$lang['plissue'] = "ฉันมีปัญหา";

$lang['deliveryopt'] = "ตัวเลือกการจัดส่ง";

$lang['returnpolicy'] = "นโยบายการคืนสินค้า";

$lang['website'] = "เว็บไซต์";

$lang['insta'] = "Instagram";

$lang['talkus'] = "พูดคุยกับเรา";

$lang['endmsg'] = "ขออภัยฉันไม่เข้าใจสิ่งที่คุณพิมพ์ ฉันสามารถช่วยคุณหาข้อมูลเพิ่มเติมเกี่ยวกับเรื่องนี้ได้แม้ว่า";

$lang['other'] = "ค้นหาผลิตภัณฑ์"; //"อื่น ๆ";

$lang['pname'] = "ชื่อ";

$lang['pqty'] = "จำนวน";

$lang['pprice'] = "ราคา";

$lang['subtotal'] = "ไม่ทั้งหมด";

$lang['shipcharge'] = "ค่าจัดส่ง";

$lang['gt'] = "ผลรวมทั้งสิ้น";

$lang['checkout'] = "เช็คเอาท์";

$lang['continueshopping'] = "ช้อปปิ้งต่อ";

$lang['shipcost'] = "ราคา";

$lang['shiporder'] = "ใบสั่ง";

$lang['shipamt'] = "จำนวน";

$lang['shipstatus'] = "สถานะ";

$lang['ordersuccess'] = "ความสำเร็จ";

$lang['orderpending'] = "รอดำเนินการ";

$lang['orderrej'] = "ปฏิเสธ";

$lang['ordercan'] = "ยกเลิก";

$lang['orderfail'] = "ล้มเหลว";

$lang['orderprocess'] = "การประมวลผล";

$lang['orderdispatch'] = "ส่ง";

$lang['ordercomplete'] = "สมบูรณ์";

//$lang['paybtn'] = "จ่ายเงิน";

$lang['shipaddress'] = "ที่อยู่จัดส่ง";

$lang['address_line1'] = "ที่อยู่บรรทัดที่ 1";

$lang['address_line2'] = "ที่อยู่บรรทัดที่ 2";

$lang['acity'] = "เมือง";

$lang['astate'] = "สถานะ";

$lang['acountry'] = "ประเทศ";

$lang['zipcode'] = "รหัสไปรษณีย์";

$lang['choose'] = "เลือก..";

$lang['noitems'] = "ไม่มีสินค้าในรถเข็นช็อปปิ้ง";

$lang['paymentdone'] = "การชำระเงินเสร็จสิ้นแล้ว";

$lang['paysuccess'] = "ความสำเร็จ! การชำระเงินได้รับเรียบร้อยแล้ว";

$lang['orderrec'] = "การสั่งซื้อของคุณได้รับการตอบรับ. ขอขอบคุณสำหรับการซื้อของคุณ !";

$lang['orderis'] = "คำสั่งซื้อของคุณ # คือ";

$lang['outstock'] = "ขอสินค้าหมดแล้ว";

$lang['qtynotavail'] = "ปริมาณที่ขอไม่สามารถใช้ได้สำหรับ";

$lang['mean'] = "ขอขอบคุณ. คุณหมายถึง";

$lang['qtyoutstock'] = "ปริมาณที่ขอไม่สามารถใช้ได้";

$lang['checkoutmost'] = "ค้นพบผลิตภัณฑ์บริการและกิจกรรมยอดนิยมของเรา";

$lang['browse'] = "คุณสามารถเรียกดูรายการผลิตภัณฑ์และบริการของเราได้";

$lang['continueaddcart'] = "คุณต้องการที่จะเพิ่มผลิตภัณฑ์ในรถเข็นต่อไปหรือไม่";

$lang['noitemcart'] = "ดูเหมือนว่าไม่มีรายการในรถเข็นของคุณ";

$lang['whatisit'] = "มันคืออะไร ?";

$lang['cannotadd'] = "คุณไม่สามารถเพิ่มผลิตภัณฑ์ในตะกร้าสินค้าได้มากกว่าหนึ่งรายการ";

$lang['method_v1'] = "Checkout V2";

$lang['method_v2'] = "Checkout V2.1";

$lang['method_message'] = "โปรดระบุการชำระเงิน";

$lang['delivery_speed'] = "โปรดเลือกความเร็วในการจัดส่ง";

$lang['delivery_pickup'] = "คุณต้องการรับใบสั่งซื้อของคุณหรือจัดส่งถึงคุณหรือไม่?";

$lang['delivery_pickupopt1'] = "PICKUP";

$lang['delivery_pickupopt2'] = "การจัดส่ง";

$lang['delivery_pickupchoose'] = "เลือกระหว่าง PICKUP & DELIVERY";

$lang['delivery_postcode'] = "กรุณาใส่รหัสไปรษณีย์ของคุณ";

$lang['delivery_shippingpostcode'] = "กรุณาใส่รหัสไปรษณีย์";

$lang['delivery_address'] = "โปรดป้อนที่อยู่ของคุณ";

$lang['delivery_pay'] = "คุณต้องการชำระเงินแค่ไหน";

$lang['delivery_paycash'] = "เงินสด";

$lang['delivery_payprepaid'] = "เติมเงิน";

$lang['delivery_nextday'] = "NEXT_DAY";

$lang['delivery_express'] = "ด่วน";

$lang['delivery_standard'] = "มาตรฐาน";

$lang['pickup_soon'] = "บริการเร็ว ๆ นี้!";

$lang['valid_postcode'] = "โปรดป้อนรหัสไปรษณีย์ที่ถูกต้อง";

$lang['choose_district'] = "โปรดเลือกเขตของคุณ";

$lang['choose_shipdistrict'] = "โปรดเลือกอำเภอ";

$lang['enter_country'] = "โปรดป้อนประเทศ";

$lang['enter_phone'] = "โปรดป้อนหมายเลขโทรศัพท์";

$lang['enter_email'] = "โปรดป้อนที่อยู่อีเมล";

$lang['choose_address'] = "โปรดป้อนที่อยู่ของคุณ";

$lang['choose_shipaddress'] = "กรุณาใส่ที่อยู่จัดส่ง";
$lang['enter_shipername'] = "ป้อนชื่อการจัดส่ง";

$lang['howpay'] = "คุณต้องการชำระเงินแค่ไหน";

$lang['paybtn'] = "การชำระเงิน";

$lang['cashthanks'] = "ขอขอบคุณที่ช้อปปิ้งที่ <pagename> หมายเลขคำสั่งซื้อของคุณคือ <ordernumber>";

$lang['smartchip'] = "ขอขอบคุณที่ใช้บริการ Smartship Counter!";

$lang['keepstatus'] = "เราจะแจ้งให้คุณทราบสถานะการสั่งซื้อของคุณผ่านการแชทนี้";

$lang['pay_counter'] = "คุณต้องการชำระเงินสดที่เคาน์เตอร์หรือเดบิต / เครดิตการ์ดหรือไม่?";

$lang['is_process'] = "คำสั่งซื้อของคุณ <orderno> ได้รับและกำลังดำเนินการอยู่";

$lang['is_cancelled'] = "สวัสดี <buynerfbname>. คำสั่งซื้อของคุณ <orderno> ถูกยกเลิกแล้ว";

$lang['is_ship'] = "สวัสดี <buynerfbname>. ข่าวดี ! สั่งซื้อของคุณได้รับการจัดส่ง !";

$lang['is_rejected'] = "สวัสดี <buynerfbname>. ขออภัยคุณไม่ชอบคำสั่งซื้อของคุณ";

$lang['is_failed'] = "สวัสดี <buynerfbname>. คนที่ส่งสินค้าไม่สามารถติดต่อคุณได้ในวันนี้";

$lang['is_delivered'] = "สวัสดี <buynerfbname>. ขอขอบคุณสำหรับการช้อปปิ้งกับเรา. หวังว่าคุณจะสนุกกับการสั่งซื้อของคุณ";

$lang['godtime'] = "โปรดแจ้งให้เราทราบเวลาที่ดีเพื่อจัดส่งตามใบสั่ง";

$lang['trackingcode'] = "ต่อไปนี้เป็นหมายเลขติดตามของคุณ แตะแต่ละอันเพื่อสร้างบาร์โค้ดและแสดงให้เจ้าหน้าที่เคาน์เตอร์";

$lang['visittracksite'] = "รายละเอียดเพิ่มเติมได้ที่ http://tracking.acommerce.asia/smartship";

$lang['visitsite'] = "สำหรับรายละเอียดเพิ่มเติมโปรดไปที่หน้าจุดสด";

$lang['orderidbarcode'] = "บาร์โค้ด OrderID ถูกสร้างขึ้นข้างต้น";

$lang['skubarcode'] = "รหัสสินค้า SKU + บาร์โค้ดการชำระเงินสร้างขึ้นข้างต้น";

$lang['erremail'] = "โปรดป้อนที่อยู่อีเมลที่ถูกต้อง";

$lang['errphone'] = "โปรดป้อนหมายเลขโทรศัพท์ที่ถูกต้อง";

$lang['errphonenum'] = "จำนวนต้องเป็น 10 หลักและเริ่มต้นด้วย 0";

$lang['pkmsg'] = "โปรดเลือกแพคเกจ";

$lang['pksmall'] = "เล็ก";

$lang['pkmedi'] = "กลาง";

$lang['pklarge'] = "ใหญ่";

$lang['pkexists'] = "คุณสามารถจัดส่งได้ครั้งละหนึ่งชุดเท่านั้น";

$lang['endmsgsmart'] = "ขออภัยฉันไม่เข้าใจสิ่งที่คุณพิมพ์ ฉันสามารถช่วยคุณหาข้อมูลเพิ่มเติมเกี่ยวกับ Smartship Thailand ได้";

$lang['code'] = "422 การยืนยันล้มเหลว.";
$lang['shippackage'] = "คุณต้องการจัดส่งแพคเกจกี่ชุด?";
$lang['wantshipmore'] = "ต้องการจัดส่งสินค้ามากกว่า 1 ปริมาณ?";
$lang['anotership'] = "โปรดป้อนรายละเอียดที่อยู่สำหรับจัดส่ง";
$lang['havecode'] = "มีรหัสคูปองหรือไม่?";
$lang['entercoupon'] = "โปรดป้อนรหัสคูปอง";
$lang['codedeactive'] = "รหัสคูปองถูกปิดการใช้งาน";
$lang['codenotvalid'] = "โปรดป้อนหมายเลขคูปองอีกครั้งในกล่องข้อความหรือดำเนินการชำระเงินต่อ";//รหัสคูปองไม่ถูกต้อง
$lang['codeexpired'] = "โปรดป้อนหมายเลขคูปองอีกครั้งในกล่องข้อความหรือดำเนินการชำระเงินต่อ";// รหัสคูปองหมดอายุแล้ว
$lang['codeused'] = "วงเงินแลกรางวัลสูงสุดถึง";
$lang['codeminimumamount'] = "จำนวนการสั่งซื้อขั้นต่ำต้องเป็น";
$lang['codesuccess'] = "ใช้รหัสคูปองเรียบร้อยแล้ว";
$lang['useaddress'] = "ใช้ที่อยู่";
$lang['newaddress'] = "ป้อนที่อยู่ใหม่"; //เพิ่มที่อยู่
$lang['newaddresstitle'] = "ใช้เพื่อป้อนที่อยู่ใหม่"; //ใช้สำหรับป้อนที่อยู่ใหม่
$lang['pickaddress'] = "เลือกที่อยู่จัดส่ง";
$lang['removecode'] = "หากคุณต้องการลบรหัสคูปองจากนั้นคลิกที่ปุ่ม 'ใช่' หรือดำเนินการเช็คเอาท์";
$lang['removecoupon'] = "รหัสคูปองถูกนำออกเรียบร้อยแล้ว";
$lang['enterorderid'] = "โปรดป้อนหมายเลขติดตามของคุณ";
$lang['findorder'] = "แพคเกจนี้อยู่ในขณะนี้ <shipping_status> สำหรับข้อมูลเพิ่มเติมโปรดไปที่ลิงค์นี้ <tracking_url>";
$lang['findorderfailed'] = "ขออภัยเราไม่พบแพคเกจนี้ในระบบของเรา โปรดตรวจสอบหมายเลขการติดตาม";
$lang['pkglimit'] = "แพคเกจต้องอยู่ระหว่าง 1 ถึง 10 หมายเลข";
$lang['specifyciti'] = "โปรดระบุเมืองที่จะจัดส่งผลิตภัณฑ์";
$lang['citimissing'] = "ไม่มีการจัดส่งเมือง";
$lang['store_info'] = "เก็บข้อมูล";
$lang['delivery_return'] = "การจัดส่งและการคืนสินค้า";
$lang['aboutus'] = "เกี่ยวกับเรา";
$lang['policyres'] = "7 วัน 14 วัน 30 วันไม่อนุญาตให้ส่งคืน";
$lang['sitelink'] = "ตรวจสอบเราที่ <websiteurl>";
$lang['instalink'] = "<instapagelink>";
$lang['talktous'] = "ติดต่อเราได้ที่ +66123456789 หรือ help@ccare.com";
$lang['nostock'] = "ขออภัย! คูปองของเราหมดแล้ว";//"ขออภัยเราไม่มีสต็อก";
$lang['backstock'] = "คุณสามารถรับคูปองได้สำหรับอีเวนท์หน้า ต้องการจองไว้หรือไม่?";//"คุณต้องการให้เราแจ้งให้คุณทราบเมื่อสินค้าคืน";
$lang['great'] = "ดี! เราจะส่งข้อความถึงคุณเมื่อได้รับการตอบกลับ";
$lang['noproblem'] = "ไม่มีปัญหา. ค้นพบผลิตภัณฑ์อื่น ๆ ของเรา";
$lang['meantime'] = "ค้นพบผลิตภัณฑ์อื่น ๆ ของเราในระหว่างนี้";
$lang['havelocation'] = "มีเวลาเปิดและที่ตั้ง";
$lang['havedelivery'] = "มีการส่งมอบและผลตอบแทน";
$lang['haveabout'] = "มีเรื่องเกี่ยวกับเรา";

// 05-02-2018 (m-d-y)
$lang['digital'] = "นี่คือใบเสร็จรับเงินดิจิทัลที่ยืนยันการสั่งซื้อและการชำระเงินของคุณ";
$lang['shop'] = "ป้อนร้านค้า";
$lang['enter'] = "โปรดคลิกที่ปุ่มด้านล่างเพื่อเข้าสู่ร้านค้า";

//29-05-2018
$lang['vendortime'] = "มีเวลาเปิดทำการ";
$lang['alldayopen'] = "24 ชั่วโมง";
$lang['noservice'] = "ไม่พบบริการจัดส่ง";
$lang['offerservice'] = "เรามีตัวเลือกการจัดส่งนี้:";
$lang['nowebsite'] = "No website found";
$lang['noinstasite'] = "ไม่พบเว็บไซต์ Instagram";
$lang['nocontact'] = "ไม่พบข้อมูลการติดต่อ";
$lang['contactinfo'] = "ติดต่อเราได้ที่";
$lang['flutter'] = "Flutterwave";
$lang['paycancel'] = "การชำระเงินถูกยกเลิกแล้ว";
$lang['payfailed'] = "การชำระเงินล้มเหลวโดยเกตเวย์การชำระเงิน";
$lang['endmsgchat'] = "ขออภัยฉันไม่เข้าใจสิ่งที่คุณพิมพ์ ามารถช่วยคุณหาข้อมูลเพิ่มเติมเกี่ยวกับ <page> นี้ได้";

// 07-06-2018
$lang['entername'] = "กรุณากรอกชื่อของคุณ";
$lang['entercity'] = "โปรดป้อนเมือง";
$lang['enterpostal'] = "โปรดใส่รหัสไปรษณีย์";
$lang['enterstate'] = "โปรดป้อนชื่อรัฐ";
$lang['entercountry'] = "โปรดป้อนชื่อประเทศ";
$lang['enteraddress'] = "กรุณาใส่ที่อยู่จัดส่ง";
$lang['validphone'] = "ป้อนหมายเลขโทรศัพท์ไนจีเรียที่ถูกต้อง";
$lang['ratemessage'] = "ขอขอบคุณ. นี้จะช่วยให้เราให้บริการที่ดีกับคุณ :)";

//21-06-2018
$lang['nigriastate'] = "โปรดป้อนรัฐไนจีเรียที่คุณต้องการให้เรานำส่ง";
$lang['localgovt'] = "โปรดเลือกรัฐบาลท้องถิ่นของคุณ";
$lang['statevalid'] = "โปรดป้อนรัฐที่ถูกต้อง";
$lang['morearea'] = "คลิกเพื่อโหลดพื้นที่ในท้องถิ่นเพิ่มเติม";
$lang['loadarea'] = "พื้นที่อื่น ๆ";

$lang['endchatmsg'] = "ขออภัยฉันไม่เข้าใจ";
$lang['endchatmsg2'] = "เลื่อนขึ้นและเลือกตัวเลือกอื่นหรือใช้ปุ่มเหนียวเพื่อไปที่หน้าแรก";
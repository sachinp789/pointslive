<?php
class MY_Controller extends CI_Controller
{

    public function __construct() {

        parent::__construct(); 
        $this->initiate_cache();
        //$this->data['page'] = '';
    }

    /**
     * Generate one way hash by using sha512
     * @param  string $str
     * @return string
     */
    public function _hash($str)
    {
        return hash('sha512', $str . config_item('encryption_key'));
    }

    private function initiate_cache()
    {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    }

}
 ?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('do_upload')){
  // Image upload
  function do_upload($id,$file)
  {     
      $CI = &get_instance();  
      $config['upload_path']          = 'uploads/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['max_size']             = 5120;
      $config['file_name']            = $id.$file['name'];

      $CI->upload->initialize($config);
      
      if ( ! $CI->upload->do_upload('profile_photo'))
      {  
        return array('error' => $CI->upload->display_errors());
      }
      else
      {
        return array('data' => $CI->upload->data());
      }
  } 
}

if ( ! function_exists('admin_info'))
{
    function admin_info($id = '')
    {
		$CI = &get_instance();
        $CI->load->model('Admin_model');
        $results = $CI->Admin_model->get(['admin_id' => $id]);
        return $results;
    }   
}

if (!function_exists('emailconfig')) {
   function emailconfig(){
        $CI = &get_instance();
	    $config = array();

	    $siteconfig = $CI->db->get('config_data')->result();
	    //var_dump($$siteconfig);exit;
	    foreach($siteconfig as $key => $val){
	        $config[$val->key] = $val->value;
	    }
	    $config['charset'] = "utf-8";
	    $config['mailtype'] = "html";
	    $config['newline'] = "\r\n";
	    return $config;

    }
}

if ( ! function_exists('categroy_lists'))
{
    function categroy_lists($page_id="") //$page_id=""
    {   //where(['page_id' => $page_id])
		$CI = &get_instance();
        $CI->load->model('Categories_model');
        $results = $CI->Categories_model->where(['page_id' => $page_id])->fields('category_id,category_name,category_name_thai,category_image')->order_by('created_date','desc')->limit(9)->get_all();
        return $results;
    }   
}

if (! function_exists('product_lists')){

    function product_lists($page=0,$limit=10,$page_id=0){
        $CI = &get_instance();
        $CI->load->model('Products_model');

        $page = ($page * $limit);

        /*$products = $CI->Products_model->where(array('inventory' => 'yes'))->order_by('created_date','desc')->with_categories()->with_images('fields:product_image')->limit($limit,$page)->get_all();*/
        //where('total_qty >','0')
        $products = $CI->Products_model->where('facebook_page',$page_id)->where('shipping_type !=','offline')->order_by('created_date','desc')->with_categories()->with_images('fields:product_image')->limit($limit,$page)->get_all();
        //file_put_contents('filename.txt', print_r($CI->db->last_query(), true));
        return $products;
    }
}

//05-02-2018
if (! function_exists('product_lists_by_shippingtype')){

    function product_lists_by_shippingtype($page=0,$limit=10,$page_id=0){
        $CI = &get_instance();
        $CI->load->model('Products_model');

        $page = ($page * $limit);

        $products = $CI->Products_model
                    //->where('total_qty >','0')
                    ->where('facebook_page',$page_id)
                    //->where('shipping_type','offline') // 03-05-2018 - 21-06-2018
                    ->order_by('created_date','desc')
                    ->with_categories()->with_images('fields:product_image')
                    ->limit($limit,$page)
                    ->get_all();
        return $products;
    }
}

if (! function_exists('productlists_by_category')){

    function productlists_by_category($param='',$page=0,$limit=10){
        $CI = &get_instance();
        $page = ($page * $limit);

        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();
        $array = [];
        $results = $CI->db->select('products.shipping_type,products.product_sku,products.product_id,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.product_description,products.product_description_thai,products.shipping_cost')   
                ->from('categories')
                ->join('categories_products', 'categories.category_id = categories_products.category_id')
                ->join('products', 'categories_products.product_id = products.product_id')
                ->where('products.shipping_type !=','offline')
                ->where("(categories.category_name LIKE '%$param%' OR categories.category_name_thai LIKE '%$param%')")
                //->like('categories.category_name',$param,'both')
                //->or_like('categories.category_name_thai',$param,'both')
                //->where('products.inventory','yes')
                //->where('products.total_qty >','0')
                ->order_by("products.created_date", "desc")
                ->limit($limit,$page)
                ->get();

        $results = $results->result();
       // echo $CI->db->last_query();exit;
        /*$totalrows = $CI->db->select('products.product_id,products.product_name,products.product_price,products.product_description')   
                ->from('categories')
                ->join('categories_products', 'categories.category_id = categories_products.category_id')
                ->join('products', 'categories_products.product_id = products.product_id')
                ->like('categories.category_name',$param,'both')
                ->where('products.inventory','yes')
                ->order_by("products.created_date", "desc")                
                ->get()
                ->num_rows();*/


        foreach ($results as $key => $value) {
            $ids[] = $value->product_id; 
            $images = array();
            $value->product_images = array();

            for ($i=0; $i < count($imagseslists); $i++) { 
                if ($imagseslists[$i]->product_id == $value->product_id) {
                    $images[] = $imagseslists[$i]->product_image;
                }
            }
            $value->product_images = $images;
        }

        $array = ['result'=>$results,"totalrows"=>$totalrows];

        return $array;  
    }
}

if( ! function_exists('chatoptions')){
    
    function chatoptions(){
        $CI = &get_instance();
        $CI->load->model('Chat_prefrences_model');
        $results = $CI->Chat_prefrences_model->fields('*')->get_all();
        return $results;
    }
}

if( ! function_exists('searchproduct_by_keyword')){
    
    function searchproduct_by_keyword($param="",$page=0,$limit=10,$pageid=""){
        $CI = &get_instance();
        $CI->load->model('Chat_prefrences_model');

        $page = ($page * $limit);

        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();

        $results = $CI->db->select('products.product_sku,products.product_id,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost,products.product_description,products.product_description_thai')   
                ->from('categories')
                ->join('categories_products', 'categories.category_id = categories_products.category_id')
                ->join('products', 'categories_products.product_id = products.product_id')
                ->where('products.facebook_page',$pageid)
                ->where("(products.product_name LIKE '%$param%' OR products.product_name_thai LIKE '%$param%' OR categories.category_name LIKE '%$param%' OR categories.category_name_thai LIKE '%$param%')")
                /*->like('categories.category_name',$param,'both')
                ->or_like('products.product_name',$param,'both')
                ->or_like('products.product_name_thai',$param,'both')*/
                //->where('products.inventory','yes')
                //->where('products.total_qty >','0')
                //->where('products.facebook_page',$pageID)
                ->order_by("products.created_date", "desc")
                ->group_by("products.product_id")
                //->limit($limit,$page)
                ->get();

        //file_put_contents('filename.txt', print_r($CI->db->last_query(), true));

        $results = $results->result();

        foreach ($results as $key => $value) {
            $ids[] = $value->product_id; 
            $images = array();
            $value->product_images = array();

            for ($i=0; $i < count($imagseslists); $i++) { 
                if ($imagseslists[$i]->product_id == $value->product_id) {
                    $images[] = $imagseslists[$i]->product_image;
                }
            }
            $value->product_images = $images;
        }
        return $results; 
    }
}

if (! function_exists('productlists_by_userid')){

    function productlists_by_userid($userid=''){
        $CI = &get_instance();

        $userdata = $CI->db->select('products.created_by,products.shipping_type,products.product_sku,products.product_id,products.total_qty,products.product_description,products.product_description_thai,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost,shopping_carts.qty')   
                ->from('shopping_carts')
                ->join('products', 'shopping_carts.product_id = products.product_id')
                ->where('shopping_carts.user_id',$userid)
                ->order_by("products.created_date", "desc")
                ->get();

        $results = $userdata->result();
        return $results;  
    }
}

if (! function_exists('productcarts_by_userid')){

    function productcarts_by_userid($userid='',$page=0,$limit=10){
        $CI = &get_instance();
        
        $page = ($page * $limit);

        $userdata = $CI->db->select('shopping_carts.id,products.created_by,products.product_sku,products.product_id,products.product_name,products.total_qty,products.product_description,products.product_name_thai,products.product_description_thai,products.product_price,products.sale_price,products.shipping_cost,shopping_carts.qty,product_images.product_image')   
                ->from('shopping_carts')
                ->join('products', 'shopping_carts.product_id = products.product_id')
                ->join('product_images', 'shopping_carts.product_id = product_images.product_id','LEFT')
                ->where('shopping_carts.user_id',$userid)
                ->order_by("shopping_carts.created_date", "desc")
                ->group_by('shopping_carts.product_id')
                ->limit($limit,$page)
                ->get();

        $results = $userdata->result();
        //echo $CI->db->last_query();
        return $results;  
    }
}

if (! function_exists('orderlists_by_userid')){

    function orderlists_by_userid($userid='',$perpage="",$page=""){
        $CI = &get_instance();
        $oids = array();

        $userdata = $CI->db->select('orders.*')   
                ->from('orders')
                ->where('orders.user_id',$userid)
                ->order_by('orders.created_date','DESC')
                ->limit($perpage,$page) // last order
                ->get();
        $results = $userdata->result();
        if(count($results) > 0){    
        foreach ($results as $key => $value) {
                $oids[] = $value->transaction_id;
        }
        $productsdata = $CI->db->select('products.product_id,products.product_sku,orders_products.order_id,orders_products.ordered_qty,products.product_price,products.sale_price,products.shipping_cost,products.product_name')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();
        $products = array();
       
            foreach ($results as $key => $value) {
                foreach ($resultsa as $key => $prod) {
                    if($value->transaction_id == $prod->order_id){
                        $products[] = $prod;
                    }
                }
            $value->products = $products;
            unset($products);
            }
        }
        return $results;
    }
}

if( ! function_exists('searchproduct_by_messenger')){
    
    function searchproduct_by_messenger($param="",$pagename="",$page=0,$limit=10){
        $CI = &get_instance();
        $CI->load->model('Chat_prefrences_model');

        $page = ($page * $limit);

        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();

        $results = $CI->db->select('*')   
                ->from('products')
                //->join('categories_products', 'categories.category_id = categories_products.category_id')
                //->join('products', 'categories_products.product_id = products.product_id')
                //->like('categories.category_name',$param,'both')
               // ->or_like('categories.category_name_thai',$param2,'both')
                ->where('products.product_name_thai',$param)
                ->where('products.facebook_page',$pagename)
                ->or_where('products.product_name',$param)
                //->where('products.inventory','yes')
                ->order_by("products.created_date", "desc")
                ->group_by("products.product_id")
                //->limit($limit,$page)
                ->get();

        $results = $results->result();

        foreach ($results as $key => $value) {
            $ids[] = $value->product_id; 
            $images = array();
            $value->product_images = array();

            for ($i=0; $i < count($imagseslists); $i++) { 
                if ($imagseslists[$i]->product_id == $value->product_id) {
                    $images[] = $imagseslists[$i]->product_image;
                }
            }
            $value->product_images = $images;
        }
        return $results; 
    }
}

if (! function_exists('orderlists')){

    function orderlists($orderID = "",$page_id="",$userID=""){
        $CI = &get_instance();
        $oids = array();

        $CI->db->select('orders.*,address.state,address.local_government,address.shipping_address,address.district,address.country,address.phone,address.email,address.delivery_method,address.postcode,address.receiver_name');   
        $CI->db->from('orders');
        $CI->db->join('address', 'orders.shipping_address = address.id','LEFT');
        
        if($userID != ''){
            $CI->db->where('orders.created_by',$userID);
        }

        if($orderID != ''){
            $CI->db->where('orders.transaction_id',$orderID);
        }
        if($page_id != ''){
            //$CI->db->where('orders.transaction_id',$orderID);
            $CI->db->where('orders.page_id',$page_id);
        }
       // $CI->db->where('orders.auto_cancelled','0'); 
        $CI->db->order_by('orders.created_date','DESC');
        $userdata =  $CI->db->get();
        $results = $userdata->result();
        //echo $CI->db->last_query();exit;
        if(count($results) > 0){
        
        foreach ($results as $key => $value) {
                $oids[] = $value->transaction_id;
        }
        $productsdata = $CI->db->select('products.product_id,products.shipping_type,products.product_sku,orders_products.order_id,orders_products.ordered_qty,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();
        //echo $CI->db->last_query();exit;
        $products = array();
       
        foreach ($results as $key => $value) {
            foreach ($resultsa as $key => $prod) {
                if($value->transaction_id == $prod->order_id){
                    $products[] = $prod;
                }
            }
        if(isset($products)){
            $value->products = @$products;
            unset($products);
        }
        else{
            $value->products = array();
        }    
        }
        }
        return $results;
    }
}

if (! function_exists('orderlistscancelled')){

    function orderlistscancelled($userID=""){
        //var_dump("sdd");exit;
        $CI = &get_instance();
        $oids = array();
        $pagtoken = array();
        //echo $orderID;exit;
        $CI->db->select('orders.*,address.shipping_address,address.district,address.country,address.phone,address.email,address.delivery_method,address.postcode,address.receiver_name');   
        $CI->db->from('orders');
        $CI->db->join('address', 'orders.shipping_address = address.id','LEFT');

        if($userID != "") {
            $CI->db->where('orders.created_by',$userID);
        }
       // $CI->db->where('orders.auto_cancelled','1');
        $CI->db->where('orders.auto_cancelled','1');
        $CI->db->order_by('orders.created_date','DESC');
        $userdata =  $CI->db->get();
        $results = $userdata->result();

        //echo $CI->db->last_query();exit;
        if(count($results) > 0){
        
        foreach ($results as $key => $value) {
                $oids[] = $value->transaction_id;
        }

        $productsdata = $CI->db->select('products.shipping_type,products.product_sku,orders_products.order_id,orders_products.ordered_qty,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            ->where('orders.auto_cancelled','1')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();

        $products = array();
       
        foreach ($results as $key => $value) {
            foreach ($resultsa as $key => $prod) {
                if($value->transaction_id == $prod->order_id){
                    $products[] = $prod;
                }
        }
         if(isset($products)){

            $value->products = @$products;
            unset($products);
         }
         else{
            $value->products = array();
         }
        }
        }

         return $results;
        
    }
}

if (! function_exists('orderlistsprocess')){

    function orderlistsprocess($userID=""){
        //var_dump("sdd");exit;
        $CI = &get_instance();
        $oids = array();
        $pagtoken = array();
        //echo $orderID;exit;
        $CI->db->select('orders.*,address.shipping_address,address.district,address.country,address.phone,address.email,address.delivery_method,address.postcode,address.receiver_name');   
        $CI->db->from('orders');
        $CI->db->join('address', 'orders.shipping_address = address.id','LEFT');

        if($userID != "") {
            $CI->db->where('orders.created_by',$userID);
        }
       // $CI->db->where('orders.auto_cancelled','1');
        $CI->db->where('orders.status','0');
        $CI->db->where('orders.auto_cancelled','0');
        $CI->db->order_by('orders.created_date','DESC');
        $userdata =  $CI->db->get();
        $results = $userdata->result();

        //echo $CI->db->last_query();exit;
        if(count($results) > 0){
        
        foreach ($results as $key => $value) {
                $oids[] = $value->transaction_id;
        }

        $productsdata = $CI->db->select('orders.id,orders.order_amount,orders.created_by,products.shipping_type,products.product_sku,orders_products.order_id,orders_products.ordered_qty,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            ->where('orders.auto_cancelled','1')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();
        //echo $CI->db->last_query();exit;
        $products = array();
       
            foreach ($results as $key => $value) {
                foreach ($resultsa as $key => $prod) {
                    if($value->transaction_id == $prod->order_id){
                        $products[] = $prod;
                    }
            }
            $value->products = @$products;
            unset($products);
            }
        }
        return $resultsa;
    }
}

function reservedComplited($product_id){
    $CI = &get_instance();
    $processing = $CI->db->query("SELECT IFNULL(SUM(orders_products.ordered_qty),0) as reserved FROM `orders` LEFT JOIN orders_products ON orders_products.order_id=orders.transaction_id WHERE orders_products.product_id='$product_id' AND orders.status!='0'")->row();
    return $processing;
}


if (! function_exists('availableQty')){
    function availableQty($product_id = '' ){

        $CI = &get_instance();
        /*SELECT *   FROM `orders_products` LEFT JOIN shopping_carts ON shopping_carts.product_id=orders_products.product_id WHERE shopping_carts.product_id='43' AND orders_products.product_id='43'*/
        $CI->db->select('SUM(qty) as reserved');
        $CI->db->from('shopping_carts sc');                      
        $CI->db->where("sc.product_id",$product_id);           
        $cart = $CI->db->get()->row();

        $CI->db->select('SUM(ordered_qty) as reserved');
        $CI->db->from('orders_products');                      
        $CI->db->where("product_id",$product_id);           
        $ordered = $CI->db->get()->row();
        if(!empty($cart->reserved)){
            $creserved = $cart->reserved;
        }else{
            $creserved = 0;
        }
        if(!empty($ordered->reserved)){
            $oreserved = $ordered->reserved;
        }else{
            $oreserved = 0;
        }
        return $test = ($creserved + $oreserved);
    }
}

/*if (! function_exists('availableQty')){
    function availableQty($product_id = '' ){

        $CI = &get_instance();

               $CI->db->select('SUM(op.ordered_qty) as reserved');
               $CI->db->from('orders_products op');
               $CI->db->join('orders o','o.transaction_id=op.order_id','left');
               $CI->db->where("o.status","0");
               $CI->db->where("op.product_id",$product_id);           
        return    $CI->db->get()->row();
    }
}*/

if (! function_exists('reservedQty')){
function reservedQty($product_id = '' ){
$CI = &get_instance();
    $CI->db->select('SUM(qty) as reserved');
    $CI->db->from('shopping_carts sc');                      
    $CI->db->where("sc.product_id",$product_id);           
    $cart = $CI->db->get()->row();

    $CI->db->select('SUM(orders_products.ordered_qty) as reserved');    
    $CI->db->from('orders_products');                      
    $CI->db->join('orders','orders.transaction_id=orders_products.order_id','left');                      
    $CI->db->where("orders_products.product_id",$product_id);           
    $CI->db->where("orders.status",'0');           
    $ordered = $CI->db->get()->row();
    if(!empty($cart->reserved)){
        $creserved = $cart->reserved;
    }else{
        $creserved = 0;
    }
    if(!empty($ordered->reserved)){
        $oreserved = $ordered->reserved;
    }else{
        $oreserved = 0;
    }
    return $test = ($creserved + $oreserved);

    /*$CI = &get_instance();

           $CI->db->select('IFNULL(SUM(op.ordered_qty),0) as reserved');
           $CI->db->from('orders_products op');
           $CI->db->join('orders o','o.transaction_id=op.order_id','left'); 
           $CI->db->where("o.status",$status);
           $CI->db->where("op.product_id",$product_id);           
     return $CI->db->get()->row();*/
     //echo $CI->db->last_query();

}
}


/*if (! function_exists('reservedQty')){
    function reservedQty($product_id = '' , $status ){

        $CI = &get_instance();

               $CI->db->select('SUM(op.ordered_qty) as reserved');
               $CI->db->from('orders_products op');
               $CI->db->join('orders o','o.transaction_id=op.order_id','left'); 
               $CI->db->where("o.status",$status);
               $CI->db->where("op.product_id",$product_id);           
      return   $CI->db->get()->row();

    }
}*/

if (! function_exists('chooselanguage')){

    function chooselanguage($userid=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('language');   
        $CI->db->from('chat_languages');
        $CI->db->where('user_id',$userid);
        $language =  $CI->db->get();
        return $language->result();
       /* if(!is_null($language->result())){
            return $language->result()[0]->language;
        }
        else{
            return $language;
        }*/
    }
}

if (! function_exists('getFacebookpagetoken')){

    function getFacebookpagetoken($pageid=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('access_token,page_name');   
        $CI->db->from('stores');
        $CI->db->where('page_id',$pageid);
        $language =  $CI->db->get();
        return $language->result();
    }
}

// Check postcode validation
if (! function_exists('checkPostalcode')){
    function checkPostalcode($code=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('district,district_thai');   
        $CI->db->from('codes');
        $CI->db->where('postalcode',$code);
        $data =  $CI->db->get();
        return $data->result();
    }
}

// Check postcode validation and get states 
if (! function_exists('getStateByDistrict')){
    function getStateByDistrict($district=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('province,province_thai');   
        $CI->db->from('codes');
        $CI->db->where('district',$district);
        $CI->db->or_where('district_thai',$district);
        $data =  $CI->db->get();
        return $data->result();
    }
}

// Logistic API for sales order creation
if(! function_exists('createSalesOrder')){
    function createSalesOrder($url="",$token="",$data=""){

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$token}",'Content-Type:application/json']);

        $content = curl_exec($ch);
        $contents = json_decode($content);
        $headerinfo = curl_getinfo($ch);
        curl_close($ch);
        //return $content;    
        if(isset($contents->error)) {
          return $headerinfo;
        }
        else
        {
          return $headerinfo;
        }

    }
}

/************ Check coupon code valid user **************/
function checkValidCoupon($code=""){
    $CI = &get_instance();
    $CI->db->where('code', $code);
    $CI->db->where('status', '1');
    $CI->db->order_by('id', 'desc'); // New add
    $query = $CI->db->get('coupons');
    //echo $CI->db->last_query();exit;
    if($query->num_rows() > 0)
    {   
        return $query->row();
    }
    else{
        return false;
    }
}


// Check user applied coupon code
function checkcoupon_apply($user=null){
    $CI = &get_instance();
    
    $CI->db->select('*');   
    $CI->db->from('coupon_redemptions');
    $CI->db->where('user_id', $user);
    $CI->db->order_by('id', 'desc');
    $data =  $CI->db->get()->result();
    //echo $CI->db->last_query();exit;
    if(count($data) > 0 )
    {   
       return $data[0];
    }
    else{
        return false;
    }
}

// Check date between two dates - 01-02-2018
if(! function_exists('checkDateBetween')){
    function checkDateBetween($todate,$startdate,$enddate){
        $todaydate=date('Y-m-d', strtotime($todate));
        $couponDateFrom = date('Y-m-d', strtotime($startdate));
        $couponDateTo = date('Y-m-d', strtotime($enddate));
        if (($todaydate >= $couponDateFrom) && ($todaydate <= $couponDateTo))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

// Get users address list
if(! function_exists('getuserAddress')){
    function getuserAddress($userid=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('*');   
        $CI->db->from('address');
        $CI->db->where('user_id',$userid);
        $CI->db->order_by('address.id','DESC');
        $CI->db->limit(9);
        $data =  $CI->db->get();
        return $data->result();
    }
}

// Get difference of month
if(! function_exists('get_month_diff')){
function get_month_diff($start,$end = FALSE)
    {   
        $date1 = new DateTime($start);
        $endtime = date('Y-m-d H:i:s', time());
        $date2 = $date1->diff(new DateTime($endtime));
        return $date2->m;
    }
}

// Get facebook username
if(! function_exists('facebook_username')){
function facebook_username($userid="",$page="")
    {   
        $CI = &get_instance();    
        $pagetokens = $CI->db->select('access_token')->from('stores')->where('page_id',$page)->get()->row();   
        $userurl = "https://graph.facebook.com/{$userid}?access_token=".$pagetokens->access_token;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch,CURLOPT_URL,$userurl);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $results = curl_exec($ch);
        //var_dump($results);exit;
        curl_close($ch);
        $content = json_decode($results);

        $fullname = $content->first_name.' '.$content->last_name;
        return $fullname;
    }
}

// Check state validation fo nigeria
if (! function_exists('CheckStateName')){
    function CheckStateName($state="",$page = 0,$limit =0){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('*');   
        $CI->db->from('nigeria_states');
        $CI->db->where('state_name',$state);
        $CI->db->limit($limit,$page);
        $data = $CI->db->get();
        return $data->result();
    }
}

// New code 28-06-2018

if(!function_exists('getFacebookPageID')){
    function getFacebookPageID(){

        $CI = &get_instance();
        // check if user logged in with facebook
        $isFacebook = $CI->session->userdata('isFacebook');

        if($isFacebook){

            $adminId = $CI->session->userdata('adminId');

            $CI->db->where(['created_by'=>$adminId]);
            $store = $CI->db->get('stores')->row();

            if(!empty($store)){
                return $store->page_id;
            }else{
                return "";
            }
            
        }else{
            $adminId = $CI->session->userdata('adminId');

            $CI->db->where(['admin_id'=>$adminId]);
            $admin = $CI->db->get('admin')->row();
            
            $connected_with = $admin->connected_with;

            if(!empty($connected_with)){

               $connected_with = explode(",",$connected_with);

               $CI->db->where('login_with',"facebook");
               $CI->db->where_in('admin_id',$connected_with);
               $admin = $CI->db->get('admin')->row();

               $adminId = $admin->admin_id;

               $CI->db->where(['created_by'=>$adminId]);
               $store = $CI->db->get('stores')->row();

               if(!empty($store)){
                   return $store->page_id;
               }else{
                   return "";
               }


            }
        }
    }
}


if(!function_exists('orderchartdata')){
    function orderchartdata(){
        $CI = &get_instance();

        $begin = new DateTime('2018-06-26');
        $end = new DateTime(date('Y-m-d',strtotime($begin . ' +1 day'))); // 2018-08-1

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        
        foreach ($period as $dt) {
            $date =  $dt->format("Y-m-d");
            if($CI->session->userdata('isLogin')){  

                $order = $CI->db->select("IFNULL(SUM(order_amount),0) as total, created_date")->from('orders')->where(['DATE(created_date)'=>$date,'created_by'=>$CI->session->userdata('adminId')])->get()->row();

                /*$page_id = getFacebookPageID();
                //echo $page_id;
                if($page_id!=""){
                    
                    $order = $CI->db->select("IFNULL(SUM(order_amount),0) as total, created_date")->from('orders')->where(['DATE(created_date)'=>$date,'page_id'=>$page_id])->get()->row();
                    //echo $CI->db->last_query();
                    //exit;
                }else{
                    $order = [];
                }*/
            }else{
                $order = $CI->db->select("IFNULL(SUM(order_amount),0) as total, created_date")->from('orders')->where(['DATE(created_date)'=>$date])->get()->row();
            }

            
            
            
              /*echo "<pre>";
              print_r($order[0]->total);
              echo "</pre>";*/
            if(!empty($order)){
              $amount = number_format($order->total,2);
              $chatoption[] = (object)['period'=>$date,'park1'=>$amount];
            }else{
              $chatoption[] = (object)['period'=>$date,'park1'=>"0"];
            }
            
        }   

        return json_encode($chatoption);


        /*if($CI->session->userdata('isLogin')=="social"){  
            $page_id = getFacebookPageID();

            if($page_id!=""){

            $CI->db->where(['page_id'=>$page_id]);

            $orders = $CI->db->select("DATE(created_date) as period,SUM(order_amount) as park1")->from('orders')->where("created_date between date_sub(now(),INTERVAL 7 DAY) and now() GROUP BY DATE(created_date)")->get()->result();

            }else{
                
                $orders = "";
            }

          }else{

              $orders = $CI->db->select("DATE(created_date) as period,SUM(order_amount) as park1")->from('orders')->where("created_date between date_sub(now(),INTERVAL 7 DAY) and now() GROUP BY DATE(created_date)")->get()->result();
          }
  
        if(!empty($orders)){
           
            return json_encode($orders);

        }else{

            $date = date('Y-m-d');
            $startdate = date("Y-m-d", strtotime("-7 day", strtotime($date)));
            for($i=1;$i<=7;$i++){
                if($i==1){
                    $startdate = date("Y-m-d", strtotime("+0 day", strtotime($startdate)));
                }
                else{
                    $startdate = date("Y-m-d", strtotime("+1 day", strtotime($startdate))); 
                }
                $chartdata[] = (object)["period"=>$startdate,"park1"=>"0"];
                
            }
            
            
            return json_encode($chartdata);
        }*/
    }
}

/**
* Top five products by revenue order was completed
* @return response array
*/
if(!function_exists('topfiveproductsbyrevenue')){
    function topfiveproductsbyrevenue(){
        $CI = &get_instance();
        if($CI->session->userdata('isLogin')){  
            $seller = $CI->session->userdata('adminId');

            return $CI->db->query("SELECT products.product_id,products.product_name,(SUM(orders_products.ordered_qty) * products.product_price) as total_revenue FROM products LEFT JOIN orders_products ON orders_products.product_id=products.product_id LEFT JOIN orders ON orders.transaction_id=orders_products.order_id  WHERE orders.status='2' AND orders.created_by='$seller' GROUP BY orders_products.product_id  ORDER BY total_revenue DESC LIMIT 5")->result();

           /* $page_id = getFacebookPageID();
                if($page_id!=""){       
                    
                    return $CI->db->query("SELECT products.product_id,products.product_name,(SUM(orders_products.ordered_qty) * products.product_price) as total_revenue FROM products LEFT JOIN orders_products ON orders_products.product_id=products.product_id LEFT JOIN orders ON orders.transaction_id=orders_products.order_id  WHERE orders.status='2' AND products.facebook_page='$page_id' GROUP BY orders_products.product_id  ORDER BY total_revenue DESC LIMIT 5")->result();
                }else{
                    return $CI->db->query("SELECT products.product_id,products.product_name,(SUM(orders_products.ordered_qty) * products.product_price) as total_revenue FROM products LEFT JOIN orders_products ON orders_products.product_id=products.product_id LEFT JOIN orders ON orders.transaction_id=orders_products.order_id  WHERE orders.status='2' AND orders.created_by='$seller' GROUP BY orders_products.product_id  ORDER BY total_revenue DESC LIMIT 5")->result();
                    //return [];
                }*/
        }else{
            return $CI->db->query("SELECT products.product_id,products.product_name,(SUM(orders_products.ordered_qty) * products.product_price) as total_revenue FROM products LEFT JOIN orders_products ON orders_products.product_id=products.product_id LEFT JOIN orders ON orders.transaction_id=orders_products.order_id  WHERE orders.status='2' GROUP BY orders_products.product_id  ORDER BY total_revenue DESC LIMIT 5")->result();
        }
        
       
    }
}

/**
* Top five state by revenue order was completed
* @return response array
*/
if(!function_exists('topfivestate')){
    function topfivestate(){
        $CI = &get_instance();
        if($CI->session->userdata('isLogin')){  
            $seller = $CI->session->userdata('adminId');
            $sellerInfo = admin_info($seller);

            //echo $seller;exit;
             return $CI->db->query("SELECT ROUND(SUM(order_amount),2) as revenue,address.state  FROM orders LEFT JOIN address ON address.user_id=orders.user_id WHERE address.state IS NOT NULL AND orders.created_by='$seller' AND orders.status='2' GROUP BY address.state ORDER BY revenue DESC LIMIT 5")->result();
            //echo $CI->db->last_query();exit;
            /*$page_id = getFacebookPageID();
                if($page_id!=""){       
                    
                    return $CI->db->query("SELECT ROUND(SUM(order_amount),2) as revenue,address.state  FROM orders LEFT JOIN address ON address.user_id=orders.user_id WHERE address.state IS NOT NULL AND orders.page_id='$page_id' AND orders.status='2' GROUP BY address.state ORDER BY revenue DESC LIMIT 5")->result();
                }else{

                    return $CI->db->query("SELECT ROUND(SUM(order_amount),2) as revenue,address.state  FROM orders LEFT JOIN address ON address.user_id=orders.user_id WHERE address.state IS NOT NULL AND orders.created_by='$seller' AND orders.status='2' GROUP BY address.state ORDER BY revenue DESC LIMIT 5")->result();

                    //return [];
                }*/
        }else{   
                    return $CI->db->query("SELECT ROUND(SUM(order_amount),2) as revenue,address.state  FROM orders LEFT JOIN address ON address.user_id=orders.user_id WHERE address.state IS NOT NULL AND orders.status='2' GROUP BY address.state ORDER BY revenue DESC LIMIT 5")->result();
        }
    }
}


if(!function_exists('topcategories')){
    function topcategories(){
        $CI = &get_instance();
        if($CI->session->userdata('isLogin')){  
            $seller = $CI->session->userdata('adminId');
            return $CI->db->query("SELECT (SUM(ordered_qty) * product_price) as revenue,categories.category_name  FROM orders LEFT JOIN orders_products ON orders.transaction_id=orders_products.order_id LEFT JOIN categories_products ON orders_products.product_id=categories_products.product_id LEFT JOIN products ON products.product_id=orders_products.product_id LEFT JOIN categories ON categories.category_id=categories_products.category_id WHERE orders.status='2' AND orders.created_by='$seller' GROUP BY categories_products.category_id ORDER BY revenue DESC LIMIT 5")->result();
            /*$page_id = getFacebookPageID();
                if($page_id!=""){       
                    
                    return $CI->db->query("SELECT (SUM(ordered_qty) * product_price) as revenue,categories.category_name  FROM orders LEFT JOIN orders_products ON orders.transaction_id=orders_products.order_id LEFT JOIN categories_products ON orders_products.product_id=categories_products.product_id LEFT JOIN products ON products.product_id=orders_products.product_id LEFT JOIN categories ON categories.category_id=categories_products.category_id WHERE orders.status='2' AND orders.page_id='$page_id' GROUP BY categories_products.category_id ORDER BY revenue DESC LIMIT 5")->result();
                }else{
                    return $CI->db->query("SELECT (SUM(ordered_qty) * product_price) as revenue,categories.category_name  FROM orders LEFT JOIN orders_products ON orders.transaction_id=orders_products.order_id LEFT JOIN categories_products ON orders_products.product_id=categories_products.product_id LEFT JOIN products ON products.product_id=orders_products.product_id LEFT JOIN categories ON categories.category_id=categories_products.category_id WHERE orders.status='2' AND orders.created_by='$seller' GROUP BY categories_products.category_id ORDER BY revenue DESC LIMIT 5")->result();
                    //return [];
                }*/
        }else{   
                return $CI->db->query("SELECT (SUM(ordered_qty) * product_price) as revenue,categories.category_name  FROM orders LEFT JOIN orders_products ON orders.transaction_id=orders_products.order_id LEFT JOIN categories_products ON orders_products.product_id=categories_products.product_id LEFT JOIN products ON products.product_id=orders_products.product_id LEFT JOIN categories ON categories.category_id=categories_products.category_id WHERE orders.status='2'  GROUP BY categories_products.category_id ORDER BY revenue DESC LIMIT 5")->result();
            }
        }
}


if(!function_exists('getFacebookpages')){
    function getFacebookpages(){
        $CI = &get_instance();

        $adminId = $CI->session->userdata('adminId');

            $CI->db->where(['admin_id'=>$adminId]);
            $admin = $CI->db->get('admin')->row();
            //var_dump($admin);exit;
            $connected_with = $admin->connected_with.",".$adminId;
            if(!empty($connected_with)){
               $connected_with = explode(",",$connected_with);
               $CI->db->where_in('created_by',$connected_with);
               return $CI->db->get('store_pages')->result();
            }else{
                return [];
            }
    }
}

// 03-07-2018 - Sachin

/************** URL paramter encrypt and decrypt ****************/

if(!function_exists('encrypt')){

    function encrypt($sData){
        $CI = &get_instance();
        $key = $CI->config->item('encryption_key');
        return base64_encode($sData.$key);
    }
}

if(!function_exists('decrypt')){

    function decrypt($sData){
        $CI = &get_instance();
        $key = $CI->config->item('encryption_key');
        $realmatch = str_replace($key,'', base64_decode($sData));
        return $realmatch;
    }
}

if(!function_exists('viewproduct_by_seller')){
    // Get product by seller ID & product ID
    function viewproduct_by_seller($sellerID="",$productId=""){
        $CI = &get_instance();
        $CI->load->model('Products_model');

        $products = $CI->Products_model
                    ->where('created_by',$sellerID)
                    ->where('product_id',$productId)
                    ->fields("product_id,product_sku,product_name,product_price,sale_price,product_description,total_qty,created_by,shipping_cost")
                    ->order_by('created_date','desc')
                    ->with_categories()->with_images('fields:product_image')
                    ->get_all();
        return $products;
    }
}

if(!function_exists('viewcategory_by_seller')){
// Get product by seller ID
function viewcategory_by_seller($sellerID=""){
    $CI = &get_instance();
    $CI->load->model('Categories_model');

    $categories = $CI->Categories_model
                ->where('created_by',$sellerID)
                ->fields("category_id,category_name,category_image")
                ->order_by('created_date','desc')
                ->get_all();
    return $categories;
}
}

if(!function_exists('viewproduct_by_seller_category')){
// Get product by seller ID
function viewproduct_by_seller_category($categoryID="",$sellerID=""){
    $CI = &get_instance();
    $data = $CI->db->select('products.product_name,products.product_id,product_images.*,categories.*')
            ->from('categories')
            ->join('categories_products', 'categories_products.category_id = categories.category_id','left')
            ->join('products', 'products.product_id = categories_products.product_id','left')
            ->join('product_images', 'product_images.product_id = products.product_id','left')
            ->where('categories.category_id',$categoryID)
            ->where('categories.created_by',$sellerID)
            ->group_by("categories_products.product_id")
            ->order_by("categories.created_date", "desc")
            ->get()->result();
    //echo $CI->db->last_query();;exit;
    return $data;
}
}

if(!function_exists('viewproduct_by_seller_category_listing')){
// Get product by seller ID
function viewproduct_by_seller_category_listing($categoryID="",$sellerID="",$order="asc",$filter=null,$search=null,$rating=0){
   // echo $categoryID;
    $CI = &get_instance();
    $CI->db->select('products.product_price,products.sale_price,products.product_name,products.product_description,products.product_id,categories.category_id,categories.category_name,categories.created_by,categories.created_by');
    $CI->db->from('categories');
    $CI->db->join('categories_products', 'categories_products.category_id = categories.category_id');
    $CI->db->join('products', 'products.product_id = categories_products.product_id');
    if($rating > 0){
        $CI->db->join('products_ratings', 'products_ratings.product_id = products.product_id','left');
    } 
    $CI->db->where('categories.category_id',$categoryID);
    $CI->db->where('categories.created_by',$sellerID);

   /* if(!is_null($filter['min']) && $filter['min'] > 0){
        $filter['max'] = 0;
        $CI->db->where("products.product_price >=",$filter['max']);
        $CI->db->where("products.product_price <=",$filter['min']);
    }*/
    if(!is_null($filter['min']) && !is_null($filter['max'])){ // Price filter
        $CI->db->where("products.product_price >=",$filter['min']);
        $CI->db->where("products.product_price <=",$filter['max']);
    }

    if(!is_null($search)){
        $CI->db->like('products.product_name',$search,'both');
    }

    if($rating > 0){
        $CI->db->where("products_ratings.rating >=",$rating);
    }

    $CI->db->order_by("products.product_price", $order); // Sorting on price (asc/desc)
    $CI->db->group_by("categories_products.product_id");
    $data = $CI->db->get()->result();

    //echo $CI->db->last_query();
    if(count($data) > 0){

        foreach ($data as $key => $value) {    

            $product_images = $CI->db->select('image_id,product_image')->where(['product_id'=>$value->product_id])->get('product_images')->result();
            $data[$key]->product_images = $product_images;        

        }
    }
    //echo $CI->db->last_query();exit;
    return $data;
    }
}

if(!function_exists('apilogger')){
/**
* Api error logger function, Save errors in DB
* @param $message,$filepath, $line
* 
*/
function apilogger($message,$filepath,$line){

    $CI = &get_instance();
    $last_error_line = $line;
    $last_error = error_get_last();
    if(isset($last_error)){
        $last_error_line = $last_error['line'];
        $last_error_no = $last_error['type'];
    }      
    
    $data = [
                'errno' => 'Notice',
                'errtype' => "CURL response Error",
                'errstr' => $message,
                'errfile' => $filepath,
                'errline' => $last_error_line,
                'user_agent' => $CI->input->user_agent(),
                'ip_address' => $CI->input->ip_address(),
                'time' => date('Y-m-d H:i:s')
            ];

    $CI->db->insert('logs', $data);    
    $CI->db->update('logs',['errtype'=>'Notice'],['errtype'=>'0']);
}
}

if(!function_exists('generateBreadcrumb')){
function generateBreadcrumb(){
    $ci=&get_instance();
    $i=1;
    $uri = $ci->uri->segment($i);
    $link='
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i>'.$ci->uri->segment($i).'</h2>
    <div class="breadcrumb-wrapper">

    <ol class="breadcrumb">';

    while($uri != ''){
    $prep_link = '';
    for($j=1; $j<=$i; $j++){
    $prep_link.=$ci->uri->segment($j).'/';
    }

    if($ci->uri->segment($i+1)== ''){
        $link.='<li class="active"><a href="'.site_url($prep_link).'">';
        $link.=$ci->uri->segment($i).'</a></li>';
    }else{
        $link.='<li><a href="'.site_url($prep_link).'">';
        $link.=$ci->uri->segment($i).'</a><span class="divider"></span></li>';
    }

    $i++;
    $uri = $ci->uri->segment($i);
    }
    $link .='</ol></div></div>';
    return $link;
}
}

if(!function_exists('GetStatesByCountry')){
// Get state lists by country - 06-07-2018 -sachin
function GetStatesByCountry($country=""){
    $CI = &get_instance();
    //$oids = array();
    if($country == "thailand"){
        $CI->db->select('codes.*');   
        $CI->db->from('codes');
        $CI->db->group_by('province');
        $data =  $CI->db->get()->result();
    }
    else if($country == "nigeria"){
        $CI->db->select('nigeria_states.*');   
        $CI->db->from('nigeria_states');
        $CI->db->group_by('state_name');
        $data =  $CI->db->get()->result();
    }
    else{
        $data = array();
    }
    return $data;
}
}
// Get local district by state
if (! function_exists('getDistrictByState')){
    function getDistrictByState($state="",$country=""){
        $CI = &get_instance();
        if($country == "thailand"){
            $CI->db->select('district');   
            $CI->db->from('codes');
            $CI->db->where('province',$state);
            $CI->db->group_by('district');
            $data =  $CI->db->get();
            //echo $CI->db->last_query();exit;
        }
        else if($country == "nigeria"){
            $CI->db->select('local_area');   
            $CI->db->from('nigeria_states');
            $CI->db->where('state_name',$state);
            $CI->db->group_by('local_area');
            $data =  $CI->db->get();
        }
        return $data->result();
    }
}

if (! function_exists('QrcodeofOrder')){
// Generate eTickets for order number
function QrcodeofOrder($orderID=""){ 
      $CI = &get_instance();  
      $CI->load->library('ciqrcode');
      $qr_image=rand().'.png';
      $params['data'] = base_url("order/Scanprocess/{$orderID}"); // $orderID
      $params['level'] = 'H';
      $params['size'] = 8; 
      $params['savename'] = FCPATH.'uploads/qr_image/'.$qr_image;
      if($CI->ciqrcode->generate($params))
      {
          $data['qrcode']=$qr_image; 
      }
      return $data;
}}

if (! function_exists('GenerateTicket')){
 // eTicket generate as pdf
function GenerateTicket($order="",$qrcode="",$product=""){
    $CI = &get_instance(); 
    $CI->load->library('m_pdf');
    
    $tr = "";
    $count = 0;
    foreach ($product as $key => $value) {
        if($count == 1){
          $tr.="<tr><td></td><td colspan=\"2\" style=\"text-align:right;\">".$value."</td></tr>";
        }
        else{
          $tr.="<tr><td>Service :</td><td>".$value."</td></tr>";
        }
        $count++;  
    }

    $useraddress = $CI->db->select('address.receiver_name,orders.created_date')->from('orders')->join('address','address.id = orders.shipping_address')->where('orders.transaction_id',$order)->get()->row();

    $my_html="
          <!DOCTYPE html>
      <html lang=\"en\" xml:lang=\"en\">
      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset=\"utf-8\">
      <head>
          <style>
              body {
                  height: 500px;
                  width: 400px;
                  /* to centre page on screen*/
                  margin-left: auto;
                  margin-right: auto;
              font-family: Arial, Helvetica, sans-serif;
            }

            .pdf-main {
                border: 2px solid #000;
                padding: 15px;
                min-height: 500px;
                width: 350px;
                  margin: auto;
                  text-align: justify;
            }

            .logo-main {
                text-align: center;
            }

            .main-title {
                text-align: center;
                margin: 25px 0px;
                font-size: 28px;
            }

            .logo-img {
                text-align: center;
                margin: 0 auto;
                width: 100px;
                -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(70%, transparent), to(rgba(250, 250, 250, 0.5)));
                -moz-box-reflect: below 0px -moz-gradient(linear, left top, left bottom, from(transparent), color-stop(70%, transparent), to(rgba(250, 250, 250, 0.5)));
                margin-bottom: 5px;
            }

            .content-ticket {
                margin: 30px;
                margin-top: 0px;
            }

            .barcode-detail {
                text-align: center;
            }

            .barcode-img {
                width: 200px;
                height: 200px;
                margin: 15px 0px 0px;
            }

            table.order-detail{
                margin-bottom: 5px;
                margin-left: 30px;
            }
            p, td {
                margin: 0;
                font-size: 18px;
            }

            .order-detail p {
                margin-bottom: 5px;
                margin-left: 30px;
            }

            p {
                margin: 0;
                font-size: 18px;
            }

            .ticket-content-main {
                margin-top: 20px;
            }

            .m-b-0 {
                margin-bottom: 0px;
            }

            .m-b-10 {
                margin-bottom: 10px;
            }

            @media print {
              @page {
                    margin: 0 auto; /* imprtant to logo margin */
                    sheet-size: 500px 175mm; /* imprtant to set paper size */
                }
                .page-break {
                    height: 0;
                    page-break-before: always;
                    margin: 0;
                    border-top: none;
                }
            }

        </style>
        </head>

      <body>
          <h1 class=\"main-title\">eTICKET</h1>
          <div class=\"pdf-main\">
              <div class=\"logo-main\"><img src=".FCPATH."assets/img/logo.png class=\"logo-img\"></div>
              <div class=\"ticket-content-main\">
                  <p class=\"content-ticket m-b-0\">
                      &#3649;&#3626;&#3604;&#3591;&#3605;&#3633;&#3659;&#3623;&#3651;&#3627;&#3657;&#3612;&#3641;&#3657;&#3586;&#3634;&#3618;</p>
                  <p class=\"content-ticket m-b-10\" lang=\"thai\">
                      &#21521;&#21334;&#23478;&#20986;&#31034;&#38376;&#31080;</p>
              </div>
              <div class=\"order-detail\">
                  <p><span style=\"width:30%;float:left;\">Customer Name :</span> <span style=\"width:70%;float:right;\" class=\"order-id\">".$useraddress->receiver_name."</span></p>  
                  <p><span style=\"width:30%;float:left;\">Order ID :</span> <span style=\"width:70%;float:right;\" class=\"order-id\">".$order."</span></p>
                  <p><span style=\"width:30%;float:left;\">Order Date :</span> <span style=\"width:70%;float:right;\" class=\"order-id\">".date("jS F, Y", strtotime($useraddress->created_date))."</span></p>
                  <table class=\"order-detail\">".$tr."</table>
              </div>
              <div class=\"barcode-detail\">
                  <img src=".FCPATH."uploads/qr_image/".$qrcode." class=\"barcode-img\">
              </div>
          </div>
      </body>

      </html>
      ";

      $CI->m_pdf->pdf->autoScriptToLang = true;
      $CI->m_pdf->pdf->autoLangToFont = true;
      $CI->m_pdf->pdf->WriteHTML($my_html);
      // write the HTML into the PDF
      $output = "eTicket-".$order.".pdf";
      //Save in directory
      $image_path = realpath(FCPATH.'uploads/invoices');
      $CI->m_pdf->pdf->Output($image_path."/{$output}", 'F');
      //$CI->m_pdf->pdf->Output($output, 'D');
      return $output;
  }}

if (! function_exists('ip_info')){
    function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
}

// Get list of banks using flutterwave API
function listofbanks(){
   
    $listbanks = array();
    $secretkey = "FLWSECK-cc17ce494aca45e5be8707fbde83cba6-X";;
    $url = "https://ravesandboxapi.flutterwave.com/banks?country=NG";
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$secretkey}",'Content-Type:application/json']);

    $content = curl_exec($ch);
    $contents = json_decode($content);
    $headerinfo = curl_getinfo($ch);
    //echo '<pre>';
    if($headerinfo['http_code'] == 200){
       foreach ($contents->data as $key => $value) {
           $listbanks[] = array(
            "bank_name" => $value->name ,
            "bank_code" => $value->code,
            "country"   => 'NG'
           );
       }
    }
    else{
        $listbanks[] = "";
    }
    return $listbanks;
}

// bank lists
function getbanks(){
    $CI = &get_instance(); 
    
    $CI->db->select('*');   
    $CI->db->from('banks');
    $data =  $CI->db->get()->result();
    return $data;
}

// Best seller products by qty sold
function BestProducts($seller=null){
    $CI = &get_instance();
    return $CI->db->query("SELECT sum(od.ordered_qty) as total,pimg.product_image,p.product_id,p.product_name,p.product_price from products as p inner join orders_products as od on p.product_id = od.product_id inner join product_images as pimg on p.product_id = pimg.product_id  WHERE p.created_by = {$seller} group by p.product_id order by sum(od.ordered_qty) DESC")->result();
}

// Get Full URL With Query Strings
function current_full_url()
{
    $CI =& get_instance();
    $url = $CI->config->site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}

// Find average rating of seller by order 
function seller_rating($id=null){
    $CI = &get_instance();
    return $CI->db->query("SELECT AVG(order_rate) as average from orders  WHERE created_by = {$id} AND order_rate IS NOT NULL")->result();
}

// Find maximum price by seller
function Maxprice($id=null){
    $CI = &get_instance();
    return $CI->db->query("SELECT MAX(products.product_price) as maximumprice from products WHERE created_by = {$id}")->result();
}

// Products rating list by user
function products_rating_by_user($pid=null,$customer=null,$order=null){
    $CI = &get_instance();
    return $CI->db->query("SELECT * from products_ratings  WHERE customer_id = {$customer} AND order_id = {$order} AND product_id = {$pid}")->result();
    echo $CI->db->last_query();
}

function match($needles, $haystack)
{
    foreach($needles as $needle){
        if (strpos($haystack, $needle) !== false) {
            return true;
        }
    }
    return false;
}

// Top seller by highest average rating - 09-10-2018
function Topsellers(){
    $CI = &get_instance();
    return $CI->db->query("SELECT a.login_picture,a.admin_photo,a.is_social,a.seller_type, AVG(od.order_rate) as average FROM orders as od INNER JOIN admin a ON od.created_by = a.admin_id WHERE od.created_date >= NOW() - INTERVAL 30 DAY AND od.order_rate IS NOT NULL GROUP BY od.created_by order by average DESC");
}

// Top categories by numer of people buy - 09-10-2018
function Topcategories(){
    $CI = &get_instance();
    return $CI->db->query("SELECT c.category_name,c.op.product_id, SUM(op.ordered_qty) AS TotalQuantity FROM orders_products as op INNER JOIN categories_products cp ON op.product_id = cp.product_id INNER JOIN categories c on cp.category_id = c.category_id GROUP BY c.category_id ORDER BY TotalQuantity DESC");
}

// Top selling products by numer of people buy - 09-10-2018
function TopsellingProducts(){
    $CI = &get_instance();
    return $CI->db->query("SELECT op.product_id,p.product_name, SUM(op.ordered_qty) AS TotalQuantity,(SELECT product_image FROM product_images WHERE product_images.product_id=op.product_id LIMIT 1) as p_img FROM orders_products as op  INNER JOIN products as p ON op.product_id = p.product_id  GROUP BY op.product_id  
        ORDER BY TotalQuantity DESC");
}

// Top selling products by numer of people buy of customer state - 09-10-2018
function TopsellingProductsByState($state=null){
    $CI = &get_instance();
    return $CI->db->query("SELECT op.product_id,p.product_name, SUM(op.ordered_qty) AS TotalQuantity,(SELECT product_image FROM product_images WHERE product_images.product_id=op.product_id LIMIT 1) as p_img FROM orders_products as op INNER JOIN orders as od ON od.transaction_id = op.order_id INNER JOIN address as ad ON od.shipping_address = ad.id INNER JOIN products as p ON op.product_id = p.product_id WHERE ad.state = '{$state}' GROUP BY op.product_id  
        ORDER BY TotalQuantity DESC");
}

// Check numerix number
function is_digit($number=null){
    $regex = preg_match("/^[1-9][0-9]*$/",$number);
    if (!$regex) {
        return false;
    }
    else{
        return true;
    }
}
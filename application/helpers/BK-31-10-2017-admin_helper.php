<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if( ! function_exists('do_upload')){

  // Image upload

  function do_upload($id,$file)

  {     

      $CI = &get_instance();  

      $config['upload_path']          = 'uploads/';

      $config['allowed_types']        = 'gif|jpg|png|jpeg';

      $config['max_size']             = 5120;

      $config['file_name']            = $id.$file['name'];



      $CI->upload->initialize($config);

      

      if ( ! $CI->upload->do_upload('profile_photo'))

      {  

        return array('error' => $CI->upload->display_errors());

      }

      else

      {

        return array('data' => $CI->upload->data());

      }

  } 

}



if ( ! function_exists('admin_info'))

{

    function admin_info($id = '')

    {

		$CI = &get_instance();

        $CI->load->model('Admin_model');

        $results = $CI->Admin_model->get(['admin_id' => $id]);

        return $results;

    }   

}



if (!function_exists('emailconfig')) {

   function emailconfig(){

        $CI = &get_instance();

	    $config = '';



	    $siteconfig = $CI->db->get('config_data')->result();

	    foreach($siteconfig as $key => $val){

	        $config[$val->key] = $val->value;

	    }

	    $config['charset'] = "utf-8";

	    $config['mailtype'] = "html";

	    $config['newline'] = "\r\n";

	    return $config;



    }

}



if ( ! function_exists('categroy_lists'))

{

    function categroy_lists()

    {

		$CI = &get_instance();

        $CI->load->model('Categories_model');

        $results = $CI->Categories_model->fields('category_id,category_name')->get_all();

        return $results;

    }   

}



if (! function_exists('product_lists')){



    function product_lists($page=0,$limit=10){

        $CI = &get_instance();

        $CI->load->model('Products_model');

        $page = ($page * $limit);

        $products = $CI->Products_model->order_by('created_date','desc')->with_categories()->with_images('fields:product_image')->limit($limit,$page)->get_all();

        return $products;

    }

}



if (! function_exists('productlists_by_category')){



    function productlists_by_category($param=''){

        $CI = &get_instance();



        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();



        $results = $CI->db->select('products.product_id,products.product_name,products.product_price,products.product_description')   

                ->from('categories')

                ->join('categories_products', 'categories.category_id = categories_products.category_id')

                ->join('products', 'categories_products.product_id = products.product_id')

                ->like('categories.category_name',$param,'both')

                ->order_by("products.created_date", "desc")

                ->limit(10)

                ->get();



        $results = $results->result();



        foreach ($results as $key => $value) {

            $ids[] = $value->product_id; 

            $images = array();

            $value->product_images = array();



            for ($i=0; $i < count($imagseslists); $i++) { 

                if ($imagseslists[$i]->product_id == $value->product_id) {

                    $images[] = $imagseslists[$i]->product_image;

                }

            }

            $value->product_images = $images;

        }

        return $results;  

    }

}



if( ! function_exists('chatoptions')){

    

    function chatoptions(){

        $CI = &get_instance();

        $CI->load->model('Chat_prefrences_model');



        $results = $CI->Chat_prefrences_model->fields('*')->get_all();

        return $results;

    }

}



if( ! function_exists('searchproduct_by_keyword')){

    

    function searchproduct_by_keyword($param=""){

        $CI = &get_instance();

        $CI->load->model('Chat_prefrences_model');



        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();



        $results = $CI->db->select('products.product_id,products.product_name,products.product_price,products.product_description')   

                ->from('categories')

                ->join('categories_products', 'categories.category_id = categories_products.category_id')

                ->join('products', 'categories_products.product_id = products.product_id')

                ->like('categories.category_name',$param,'both')

                ->or_like('products.product_name',$param,'both')

                ->order_by("products.created_date", "desc")

                ->group_by("products.product_id")

                ->limit(10)

                ->get();



        $results = $results->result();



        foreach ($results as $key => $value) {

            $ids[] = $value->product_id; 

            $images = array();

            $value->product_images = array();



            for ($i=0; $i < count($imagseslists); $i++) { 

                if ($imagseslists[$i]->product_id == $value->product_id) {

                    $images[] = $imagseslists[$i]->product_image;

                }

            }

            $value->product_images = $images;

        }

        return $results; 

    }

}


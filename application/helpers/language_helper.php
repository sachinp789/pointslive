<?php
function lang_switcher($lang='english'){

    $ci =& get_instance(); // CI instance

    if($lang == 'thai'){
        $ci->lang->load('thai_lang', 'thai');
    }
    else if($lang == 'english'){
        $ci->lang->load('english_lang', 'english');
    }
    else{
        $ci->lang->load('english_lang', 'english');
    }
}   
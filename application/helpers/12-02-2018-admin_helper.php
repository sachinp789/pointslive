<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( ! function_exists('do_upload')){
  // Image upload
  function do_upload($id,$file)
  {     
      $CI = &get_instance();  
      $config['upload_path']          = 'uploads/';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['max_size']             = 5120;
      $config['file_name']            = $id.$file['name'];

      $CI->upload->initialize($config);
      
      if ( ! $CI->upload->do_upload('profile_photo'))
      {  
        return array('error' => $CI->upload->display_errors());
      }
      else
      {
        return array('data' => $CI->upload->data());
      }
  } 
}

if ( ! function_exists('admin_info'))
{
    function admin_info($id = '')
    {
		$CI = &get_instance();
        $CI->load->model('Admin_model');
        $results = $CI->Admin_model->get(['admin_id' => $id]);
        return $results;
    }   
}

if (!function_exists('emailconfig')) {
   function emailconfig(){
        $CI = &get_instance();
	    $config = array();

	    $siteconfig = $CI->db->get('config_data')->result();
	    //var_dump($$siteconfig);exit;
	    foreach($siteconfig as $key => $val){
	        $config[$val->key] = $val->value;
	    }
	    $config['charset'] = "utf-8";
	    $config['mailtype'] = "html";
	    $config['newline'] = "\r\n";
	    return $config;

    }
}

if ( ! function_exists('categroy_lists'))
{
    function categroy_lists()
    {
		$CI = &get_instance();
        $CI->load->model('Categories_model');
        $results = $CI->Categories_model->fields('category_id,category_name,category_name_thai,category_image')->order_by('created_date','desc')->limit(9)->get_all();
        return $results;
    }   
}

if (! function_exists('product_lists')){

    function product_lists($page=0,$limit=10){
        $CI = &get_instance();
        $CI->load->model('Products_model');

        $page = ($page * $limit);

        /*$products = $CI->Products_model->where(array('inventory' => 'yes'))->order_by('created_date','desc')->with_categories()->with_images('fields:product_image')->limit($limit,$page)->get_all();*/
        $products = $CI->Products_model->where('total_qty >','0')->order_by('created_date','desc')->with_categories()->with_images('fields:product_image')->limit($limit,$page)->get_all();
        return $products;
    }
}

if (! function_exists('productlists_by_category')){

    function productlists_by_category($param='',$page=0,$limit=10){
        $CI = &get_instance();
        $page = ($page * $limit);

        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();
        $array = [];
        $results = $CI->db->select('products.product_sku,products.product_id,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.product_description,products.product_description_thai,products.shipping_cost')   
                ->from('categories')
                ->join('categories_products', 'categories.category_id = categories_products.category_id')
                ->join('products', 'categories_products.product_id = products.product_id')
                ->like('categories.category_name',$param,'both')
                ->or_like('categories.category_name_thai',$param,'both')
                //->where('products.inventory','yes')
                ->where('products.total_qty >','0')
                ->order_by("products.created_date", "desc")
                ->limit($limit,$page)
                ->get();

        $results = $results->result();
       // echo $CI->db->last_query();exit;
        /*$totalrows = $CI->db->select('products.product_id,products.product_name,products.product_price,products.product_description')   
                ->from('categories')
                ->join('categories_products', 'categories.category_id = categories_products.category_id')
                ->join('products', 'categories_products.product_id = products.product_id')
                ->like('categories.category_name',$param,'both')
                ->where('products.inventory','yes')
                ->order_by("products.created_date", "desc")                
                ->get()
                ->num_rows();*/


        foreach ($results as $key => $value) {
            $ids[] = $value->product_id; 
            $images = array();
            $value->product_images = array();

            for ($i=0; $i < count($imagseslists); $i++) { 
                if ($imagseslists[$i]->product_id == $value->product_id) {
                    $images[] = $imagseslists[$i]->product_image;
                }
            }
            $value->product_images = $images;
        }

        $array = ['result'=>$results,"totalrows"=>$totalrows];

        return $array;  
    }
}

if( ! function_exists('chatoptions')){
    
    function chatoptions(){
        $CI = &get_instance();
        $CI->load->model('Chat_prefrences_model');
        $results = $CI->Chat_prefrences_model->fields('*')->get_all();
        return $results;
    }
}

if( ! function_exists('searchproduct_by_keyword')){
    
    function searchproduct_by_keyword($param="",$page=0,$limit=10){
        $CI = &get_instance();
        $CI->load->model('Chat_prefrences_model');

        $page = ($page * $limit);

        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();

        $results = $CI->db->select('products.product_sku,products.product_id,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost,products.product_description,products.product_description_thai')   
                ->from('categories')
                ->join('categories_products', 'categories.category_id = categories_products.category_id')
                ->join('products', 'categories_products.product_id = products.product_id')
                ->like('categories.category_name',$param,'both')
                ->or_like('products.product_name',$param,'both')
                ->or_like('products.product_name_thai',$param,'both')
                //->where('products.inventory','yes')
                ->where('products.total_qty >','0')
                ->order_by("products.created_date", "desc")
                ->group_by("products.product_id")
                //->limit($limit,$page)
                ->get();

        $results = $results->result();

        foreach ($results as $key => $value) {
            $ids[] = $value->product_id; 
            $images = array();
            $value->product_images = array();

            for ($i=0; $i < count($imagseslists); $i++) { 
                if ($imagseslists[$i]->product_id == $value->product_id) {
                    $images[] = $imagseslists[$i]->product_image;
                }
            }
            $value->product_images = $images;
        }
        return $results; 
    }
}

if (! function_exists('productlists_by_userid')){

    function productlists_by_userid($userid=''){
        $CI = &get_instance();

        $userdata = $CI->db->select('products.product_sku,products.product_id,products.product_name,products.product_name_thai,products.product_price,products.sale_price,products.shipping_cost,shopping_carts.qty')   
                ->from('shopping_carts')
                ->join('products', 'shopping_carts.product_id = products.product_id')
                ->where('shopping_carts.user_id',$userid)
                ->order_by("products.created_date", "desc")
                ->get();

        $results = $userdata->result();
        return $results;  
    }
}

if (! function_exists('productcarts_by_userid')){

    function productcarts_by_userid($userid='',$page=0,$limit=10){
        $CI = &get_instance();
        
        $page = ($page * $limit);

        $userdata = $CI->db->select('products.product_sku,products.product_id,products.product_name,products.product_description,products.product_name_thai,products.product_description_thai,products.product_price,products.sale_price,products.shipping_cost,shopping_carts.qty,product_images.product_image')   
                ->from('shopping_carts')
                ->join('products', 'shopping_carts.product_id = products.product_id')
                ->join('product_images', 'shopping_carts.product_id = product_images.product_id')
                ->where('shopping_carts.user_id',$userid)
                ->order_by("shopping_carts.created_date", "desc")
                ->group_by('shopping_carts.product_id')
                ->limit($limit,$page)
                ->get();

        $results = $userdata->result();
        //echo $CI->db->last_query();
        return $results;  
    }
}

if (! function_exists('orderlists_by_userid')){

    function orderlists_by_userid($userid=''){
        $CI = &get_instance();
        $oids = array();

        $userdata = $CI->db->select('orders.*')   
                ->from('orders')
                ->where('orders.user_id',$userid)
                ->order_by('orders.created_date','DESC')
                ->limit(1) // last order
                ->get();
        $results = $userdata->result();
        if(count($results) > 0){    
        foreach ($results as $key => $value) {
                $oids[] = $value->transaction_id;
        }
        $productsdata = $CI->db->select('orders_products.order_id,orders_products.ordered_qty,products.product_price,products.sale_price,products.shipping_cost,products.product_name')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();
        $products = array();
       
            foreach ($results as $key => $value) {
                foreach ($resultsa as $key => $prod) {
                    if($value->transaction_id == $prod->order_id){
                        $products[] = $prod;
                    }
                }
            $value->products = $products;
            unset($products);
            }
        }
        return $results;
    }
}

if( ! function_exists('searchproduct_by_messenger')){
    
    function searchproduct_by_messenger($param="",$page=0,$limit=10){
        $CI = &get_instance();
        $CI->load->model('Chat_prefrences_model');

        $page = ($page * $limit);

        $imagseslists = $CI->db->select('*')->from('product_images')->get()->result();

        $results = $CI->db->select('*')   
                ->from('products')
                //->join('categories_products', 'categories.category_id = categories_products.category_id')
                //->join('products', 'categories_products.product_id = products.product_id')
                //->like('categories.category_name',$param,'both')
               // ->or_like('categories.category_name_thai',$param2,'both')
                ->where('products.product_name_thai',$param)
                ->or_where('products.product_name',$param)
                //->where('products.inventory','yes')
                ->order_by("products.created_date", "desc")
                ->group_by("products.product_id")
                //->limit($limit,$page)
                ->get();

        $results = $results->result();

        foreach ($results as $key => $value) {
            $ids[] = $value->product_id; 
            $images = array();
            $value->product_images = array();

            for ($i=0; $i < count($imagseslists); $i++) { 
                if ($imagseslists[$i]->product_id == $value->product_id) {
                    $images[] = $imagseslists[$i]->product_image;
                }
            }
            $value->product_images = $images;
        }
        return $results; 
    }
}

if (! function_exists('orderlists')){

    function orderlists($orderID = ""){
        $CI = &get_instance();
        $oids = array();

        $CI->db->select('orders.*,address.shipping_address,address.district,address.country,address.phone,address.email,address.delivery_method,address.postcode');   
        $CI->db->from('orders');
        $CI->db->join('address', 'orders.shipping_address = address.id');
        if($orderID != ''){
            $CI->db->where('orders.transaction_id',$orderID);
        }
        $CI->db->order_by('orders.created_date','DESC');
        $userdata =  $CI->db->get();
        $results = $userdata->result();
        //echo $CI->db->last_query();exit;
        if(count($results) > 0){
        
        foreach ($results as $key => $value) {
                $oids[] = $value->transaction_id;
        }
        $productsdata = $CI->db->select('products.product_sku,orders_products.order_id,orders_products.ordered_qty,products.product_name,products.product_price,products.sale_price,products.shipping_cost')   
            ->from('products')
            ->join('orders_products', 'products.product_id = orders_products.product_id')
            ->join('orders', 'orders_products.order_id = orders.transaction_id')
            ->where_in('orders_products.order_id',$oids)
            ->get();
        $resultsa = $productsdata->result();
        //echo $CI->db->last_query();exit;
        $products = array();
       
        foreach ($results as $key => $value) {
            foreach ($resultsa as $key => $prod) {
                if($value->transaction_id == $prod->order_id){
                    $products[] = $prod;
                }
            }
        $value->products = @$products;
        unset($products);
        }
        }
        return $results;
    }
}

function reservedComplited($product_id){
    $CI = &get_instance();
    $processing = $CI->db->query("SELECT IFNULL(SUM(orders_products.ordered_qty),0) as reserved FROM `orders` LEFT JOIN orders_products ON orders_products.order_id=orders.transaction_id WHERE orders_products.product_id='$product_id' AND orders.status!='0'")->row();
    return $processing;
}


if (! function_exists('availableQty')){
    function availableQty($product_id = '' ){

        $CI = &get_instance();
        /*SELECT *   FROM `orders_products` LEFT JOIN shopping_carts ON shopping_carts.product_id=orders_products.product_id WHERE shopping_carts.product_id='43' AND orders_products.product_id='43'*/
        $CI->db->select('SUM(qty) as reserved');
        $CI->db->from('shopping_carts sc');                      
        $CI->db->where("sc.product_id",$product_id);           
        $cart = $CI->db->get()->row();

        $CI->db->select('SUM(ordered_qty) as reserved');
        $CI->db->from('orders_products');                      
        $CI->db->where("product_id",$product_id);           
        $ordered = $CI->db->get()->row();
        if(!empty($cart->reserved)){
            $creserved = $cart->reserved;
        }else{
            $creserved = 0;
        }
        if(!empty($ordered->reserved)){
            $oreserved = $ordered->reserved;
        }else{
            $oreserved = 0;
        }
        return $test = ($creserved + $oreserved);
    }
}

/*if (! function_exists('availableQty')){
    function availableQty($product_id = '' ){

        $CI = &get_instance();

               $CI->db->select('SUM(op.ordered_qty) as reserved');
               $CI->db->from('orders_products op');
               $CI->db->join('orders o','o.transaction_id=op.order_id','left');
               $CI->db->where("o.status","0");
               $CI->db->where("op.product_id",$product_id);           
        return    $CI->db->get()->row();
    }
}*/

if (! function_exists('reservedQty')){
function reservedQty($product_id = '' ){
$CI = &get_instance();
    $CI->db->select('SUM(qty) as reserved');
    $CI->db->from('shopping_carts sc');                      
    $CI->db->where("sc.product_id",$product_id);           
    $cart = $CI->db->get()->row();

    $CI->db->select('SUM(orders_products.ordered_qty) as reserved');    
    $CI->db->from('orders_products');                      
    $CI->db->join('orders','orders.transaction_id=orders_products.order_id','left');                      
    $CI->db->where("orders_products.product_id",$product_id);           
    $CI->db->where("orders.status",'0');           
    $ordered = $CI->db->get()->row();
    if(!empty($cart->reserved)){
        $creserved = $cart->reserved;
    }else{
        $creserved = 0;
    }
    if(!empty($ordered->reserved)){
        $oreserved = $ordered->reserved;
    }else{
        $oreserved = 0;
    }
    return $test = ($creserved + $oreserved);

    /*$CI = &get_instance();

           $CI->db->select('IFNULL(SUM(op.ordered_qty),0) as reserved');
           $CI->db->from('orders_products op');
           $CI->db->join('orders o','o.transaction_id=op.order_id','left'); 
           $CI->db->where("o.status",$status);
           $CI->db->where("op.product_id",$product_id);           
     return $CI->db->get()->row();*/
     //echo $CI->db->last_query();

}
}


/*if (! function_exists('reservedQty')){
    function reservedQty($product_id = '' , $status ){

        $CI = &get_instance();

               $CI->db->select('SUM(op.ordered_qty) as reserved');
               $CI->db->from('orders_products op');
               $CI->db->join('orders o','o.transaction_id=op.order_id','left'); 
               $CI->db->where("o.status",$status);
               $CI->db->where("op.product_id",$product_id);           
      return   $CI->db->get()->row();

    }
}*/

if (! function_exists('chooselanguage')){

    function chooselanguage($userid=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('language');   
        $CI->db->from('chat_languages');
        $CI->db->where('user_id',$userid);
        $language =  $CI->db->get();
        return $language->result();
       /* if(!is_null($language->result())){
            return $language->result()[0]->language;
        }
        else{
            return $language;
        }*/
    }
}

if (! function_exists('getFacebookpagetoken')){

    function getFacebookpagetoken($pageid=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('access_token');   
        $CI->db->from('stores');
        $CI->db->where('page_id',$pageid);
        $language =  $CI->db->get();
        return $language->result();
    }
}

// Check postcode validation
if (! function_exists('checkPostalcode')){
    function checkPostalcode($code=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('district,district_thai');   
        $CI->db->from('codes');
        $CI->db->where('postalcode',$code);
        $data =  $CI->db->get();
        return $data->result();
    }
}

// Check postcode validation and get states 
if (! function_exists('getStateByDistrict')){
    function getStateByDistrict($district=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('province,province_thai');   
        $CI->db->from('codes');
        $CI->db->where('district',$district);
        $CI->db->or_where('district_thai',$district);
        $data =  $CI->db->get();
        return $data->result();
    }
}

// Logistic API for sales order creation
if(! function_exists('createSalesOrder')){
    function createSalesOrder($url="",$token="",$data=""){

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["X-Subject-Token:{$token}",'Content-Type:application/json']);

        $content = curl_exec($ch);
        $contents = json_decode($content);
        $headerinfo = curl_getinfo($ch);
        curl_close($ch);
        //return $content;    
        if(isset($contents->error)) {
          return $headerinfo;
        }
        else
        {
          return $headerinfo;
        }

    }
}

// Check date between two dates - 01-02-2018
if(! function_exists('checkDateBetween')){
    function checkDateBetween($todate,$startdate,$enddate){
        $todaydate=date('Y-m-d', strtotime($todate));
        $couponDateFrom = date('Y-m-d', strtotime($startdate));
        $couponDateTo = date('Y-m-d', strtotime($enddate));
        if (($todaydate >= $couponDateFrom) && ($todaydate <= $couponDateTo))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

// Get users address list
if(! function_exists('getuserAddress')){
    function getuserAddress($userid=""){
        $CI = &get_instance();
        $oids = array();
        $CI->db->select('*');   
        $CI->db->from('address');
        $CI->db->where('user_id',$userid);
        $CI->db->order_by('address.id','DESC');
        $CI->db->limit(9);
        $data =  $CI->db->get();
        return $data->result();
    }
}
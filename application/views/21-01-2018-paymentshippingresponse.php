<?php
$this->load->model('Address_model'); 
lang_switcher($language); 
$chatoptionsdata = chatoptions(); 
if($language == 'english'){
  $welcome = $chatoptionsdata[0]->welcome_msg;
}
else{
  $welcome = $chatoptionsdata[0]->welcome_msg_thai;
}
//die("Fdsfsdfdsfs");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <script>
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'Messenger'));
    </script>
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="sas login_wrapper">
          <div class="x_content">
            <div class="row">
              <h2><?php echo $title ?></h2>
              <div class="separator">
                <div class="clearfix"></div>
              </div>
              <div class="col-sm-12">
                <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                   <!--  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button> -->
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                  </div>
                <?php endif; ?> 
                <div class="clearfix"></div>
                <?php if($status == 000) : ?>
                <p class="text-muted well well-sm no-shadow">
                  Your order has been received. Thank you for your purchase !<br>Your order # is: <?php echo $orders;?>
                </p>
                <p class="responsebot">
                <?php 
                //echo "fgdfgdfg1111";
                // get message on chatbot messenger
                $text = $this->lang->line('paymentdone');
                $response = [
                        'recipient' => [ 'id' => $userid ],
                        'message' => [ 'text' => $text]
                ];

                $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                $content = curl_exec($ch);
                curl_close($ch);

                $message_to_reply = $this->lang->line('smartchip');

                $response = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => [ 'text' => $message_to_reply]
                ];

                $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                $content = curl_exec($ch);
                curl_close($ch);

                //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                // Create sales orders
                $shipaddress = $this->Address_model->get($addressId);
                $shiptype = "SAME_DAY";
                    if($shipaddress->delivery_method == 'next_day'){
                      $shiptype = "NEXT_DAY";
                    }
                    if($shipaddress->delivery_method == 'express' || $shipaddress->delivery_method == 'ด่วน'){
                      $shiptype = "EXPRESS_1_2_DAYS";
                    }
                    if($shipaddress->delivery_method == 'standard' || $shipaddress->delivery_method == 'มาตรฐาน'){
                      $shiptype = "STANDARD_2_4_DAYS";
                    }

                    $provinces = getStateByDistrict($shipaddress->district); // States shipping address

                    if($language == 'english'){
                      $state = $provinces[0]->province;
                    }else{
                      $state = $provinces[0]->province_thai;
                    }
                
                    $userinfo = $this->CI->getPageUserInfo("{$pageID}","{$userid}");

                    $fullname = $userinfo->first_name.' '.$userinfo->last_name;
                    
                    $businessaddress = $this->CI->getOtherPageLocationHours("{$pageID}");
                 
                    $validatetoken = $this->CI->validateAPI(); // Get Access Token of Logistic API

                    if(!is_null($validatetoken)){
                      $pagetoken = $validatetoken['token']['token_id'];
                    }
       
                    $addressee = "";
                    $address1 = "";
                    $province = "";
                    //$postalCode = "";
                    $country = "";
                    $phone = "";
                    $email = "";

                    if(isset($businessaddress->name)){
                      $addressee = $businessaddress->name;
                    }
                    if(isset($businessaddress->single_line_address)){
                      $address1 = $businessaddress->single_line_address;
                      $explodeaddr = explode(",", $businessaddress->single_line_address);
                      $provinces = explode(" ", $explodeaddr[2]);
                      $provincename = $provinces[1];
                      $postalCode = $provinces[2];
                    }
                    
                    if(isset($businessaddress->phone)){
                      $phone = $businessaddress->phone;
                    }
                    if(isset($businessaddress->emails)){
                      $email = $businessaddress->emails[0];
                    }

                    $shippingorders = orderlists($orders);
                    
                    $shiporderItems = array();

                    $this->db->where('transaction_id', $orders);
                    // here we select every column of the table
                    $q = $this->db->get('shipping_packages');
                    $data = $q->result_array();

                    $pname = $data[0]['package_type'];
                    $price = $packageprice;

                    $shiporderItems[] = array(
                      "itemDescription" => "{$pname}",
                      "itemQuantity"    => $packageqty,
                    );

                   /* foreach ($shippingorders[0]->products as $key => $value) {
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }
                        if($this->language == 'english'){
                          $pname = $value->product_name;
                        }else{
                          $pname = $value->product_name_thai;
                        }
                        $shiporderItems[] = array(
                        "itemDescription" => "{$pname}",
                        "itemQuantity"    => (int)$value->ordered_qty,
                        );
                    }*/
      
                    $shipordersDetails = [
                        "shipServiceType" => "DELIVERY",
                        "shipSender" => [
                            "addressee"=>"{$addressee}",
                            "address1"=>"250,Thailand", // {$address1}
                            "city"=>"",
                            "province"=>"Thailand", // {$provincename}
                            "postalCode"=>"10110",// {$postalCode}
                            "country"=> "Thailand", //{$provincename}
                            "phone"=> "{$phone}",
                            "email"=>"{$email}"
                        ],
                        "shipShipment" =>[
                            "addressee"=>"{$fullname}",
                            "address1"=>"{$shipaddress->shipping_address}",
                            "address2"=>"",
                            "subDistrict"=>"",
                            "district"=>"{$shipaddress->district}",
                            "city"=>"",
                            "province"=>"{$state}",
                            "postalCode"=>"{$shipaddress->postcode}",
                            "country"=>"{$shipaddress->country}",
                            "phone"=>"{$shipaddress->phone}",
                            "email"=>"{$shipaddress->email}"
                        ],
                        "shipPickup" =>[
                           "addressee"=>"{$addressee}",
                            "address1"=>"250,Thailand", // {$address1}
                            "city"=>"",
                            "province"=>"Thailand", // {$provincename}
                            "postalCode"=>"10110",// {$postalCode}
                            "country"=> "Thailand", //{$provincename}
                            "phone"=> "{$phone}",
                            "email"=>"{$email}"
                        ],
                        "shipShippingType"=>"{$shiptype}",
                        "shipPaymentType"=>"CCOD",
                        "shipCurrency"=>"THB",
                        "shipGrossTotal"=>(int)$shippingorders[0]->order_amount,
                        "shipInsurance"=>false,
                        //"shipPickingList"=>$shiporderItems,
                        "shipPackages"=>[
                            [
                                "packageWeight"=>$price,
                                "packageItems"=> $shiporderItems
                            ]
                        ]
                      ];  
                   
                      $shippingorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$orders}",$pagetoken,$shipordersDetails);

                      if(isset($shippingorder['http_code']) && $shippingorder['http_code'] == 201){
                        $findorder = str_replace("<orderno>", "{$orders}", $this->lang->line('is_process'));
                        $message_to_reply = $findorder;

                        $response = [
                            'recipient' => [ 'id' => $userid ],
                            'message' => [ 'text' => $message_to_reply]
                        ];

                        $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                        $content = curl_exec($ch);
                        curl_close($ch);

                       // $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                         // Barcode generate TYPE_CODE_128 of order id
                        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                        $string = base64_encode($generator->getBarcode("{$orders}", $generator::TYPE_CODE_128));
                       
                        $image = base64_decode($string);
                        $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
                        $filename = $image_name . '.' . 'png';
                        //rename file name with random number
                        $path = FCPATH.'uploads/barcodes/';
                        //image uploading folder path
                        file_put_contents($path . $filename, $image);

                        $imageURL = base_url()."uploads/barcodes/".$filename;
                        //echo $imageURL;
                        $sendorder = $this->CI->sendChatbotImageAPI($token,$pageID,$userid,$imageURL);

                        if(isset($sendorder->attachment_id)){
                          $message_to_reply = $this->lang->line('orderidbarcode');

                          $response = [
                              'recipient' => [ 'id' => $userid ],
                              'message' => [ 'text' => $message_to_reply]
                          ];

                          $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                          curl_setopt($ch, CURLOPT_POST, 1);
                          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                          curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                          $content = curl_exec($ch);
                          curl_close($ch);

                          //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                           // Barcode generate TYPE_CODE_128 of product_sku+payment
                          $skupaymetnbarcode = @$shippingorders[0]->products[0]->product_sku.'CCOD';
                          //echo "SP:-".$skupaymetnbarcode;
                          $generatornew = new \Picqer\Barcode\BarcodeGeneratorPNG();
                          $stringcode = base64_encode($generatornew->getBarcode("{$skupaymetnbarcode}", $generator::TYPE_CODE_128));
                         
                          $imagenew = base64_decode($stringcode);
                          $image_namenew = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
                          $filename = $image_namenew . '.' . 'png';
                          //rename file name with random number
                          $pathnew = FCPATH.'uploads/barcodes/';
                          //image uploading folder path
                          file_put_contents($pathnew . $filename, $imagenew);

                          $imagenewURL = base_url()."uploads/barcodes/".$filename;

                          $sendnew = $this->CI->sendChatbotImageAPI($token,$pageID,$userid,$imagenewURL);

                          if(isset($sendnew->attachment_id)){
                            $message_to_reply = $this->lang->line('skubarcode');;

                            $response = [
                                'recipient' => [ 'id' => $userid ],
                                'message' => [ 'text' => $message_to_reply]
                            ];

                            $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                            $content = curl_exec($ch);
                            curl_close($ch);

                           // $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                            $message_to_reply = $this->lang->line('trackingcode');

                            $response = [
                                'recipient' => [ 'id' => $userid ],
                                'message' => [ 'text' => $message_to_reply]
                            ];

                            $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                            $content = curl_exec($ch);
                            curl_close($ch);

                            //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$this->access_token,$response,'POST'); 
                          } 
                        }
                      }else{
                        $message_to_reply = "422 code. Validation failed.";

                        $response = [
                            'recipient' => [ 'id' => $userid ],
                            'message' => [ 'text' => $message_to_reply]
                        ];

                        $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                        $content = curl_exec($ch);
                        curl_close($ch);

                        //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');
                }  
                $this->session->unset_userdata('shipping_address');
                $this->cache->delete('shipping_district');
                $this->cache->delete('shipping_postcode');
                $this->cache->delete('shipping_deliveryaddress');
                $this->cache->delete('delivery_shippingmethod'); 
                $this->cache->delete('packagtype'); 
                $this->cache->delete('packagprice');
                $this->cache->delete('packagepricem'); 
                $this->cache->delete('packagepricem');
                $this->cache->delete('shipping_phoneno'); 
                $this->cache->delete('shipping_shippemail');
                $this->cache->delete('delivery_shippemail');
                $this->cache->delete('shippingpostcode');
                // get message on chatbot messenger for start loop
                $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('counterservice'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('sellerpickup')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                  $responses = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => $answer 
                  ];  

                  $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($responses));
                  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                  $content = curl_exec($ch);
                  curl_close($ch);
                  endif; 
                ?>  
                </p>
             </div>
            </div>
          </div>
      </div>
    </div>
    <script>
       window.extAsyncInit = function() {
         setTimeout(function(){
          MessengerExtensions.requestCloseBrowser(function success() { // close dialogbox
              //alert("ok");
          }, function error(err) {
             // alert(err);
          });
      
        }, 1000);
      }
      </script>
     <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/build/js/custom.min.js"></script>
  </body>
</html>

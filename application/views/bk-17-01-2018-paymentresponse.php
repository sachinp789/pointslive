<?php
lang_switcher($language); 
$chatoptionsdata = chatoptions(); 
if($language == 'english'){
  $welcome = $chatoptionsdata[0]->welcome_msg;
}
else{
  $welcome = $chatoptionsdata[0]->welcome_msg_thai;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      p.responsebot{
        display: none;
      }
    </style>
  </head>

  <body class="login">
  <script>
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'Messenger'));
    </script>
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <div class="sas login_wrapper">
          <div class="x_content">
            <div class="row">
              <h2><?php echo $title ?></h2>
              <div class="separator">
                <div class="clearfix"></div>
              </div>
              <div class="col-sm-12">
                <?php if(!empty($this->session->flashdata('flashmsg1'))): ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                   <!--  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button> -->
                    <?php echo $this->session->flashdata('flashmsg1');
                     //echo $this->lang->line('paysuccess');  
                    ?>
                  </div>
                <?php endif; ?> 
                <div class="clearfix"></div>
                <?php if($status == 000) : ?>
                <p class="text-muted well well-sm no-shadow">
                  <?php echo $this->lang->line('orderrec') ?><br><?php echo $this->lang->line('orderis') ?>: <?php echo $orders;?>
                </p>
                <p class="responsebot">
                <?php 
                // get message on chatbot messenger
                $text = $this->lang->line('paymentdone');
                $response = [
                        'recipient' => [ 'id' => $userid ],
                        'message' => [ 'text' => $text]
                ];

                $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                $content = curl_exec($ch);
                curl_close($ch);

                // get message on chatbot messenger for start loop
                $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('mostpopular'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('discover')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                  $responses = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => $answer 
                  ];  

                  $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($responses));
                  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                  $content = curl_exec($ch);
                  curl_close($ch);
                  endif; 
                ?>  
                </p>
             </div>
            </div>
          </div>
      </div>
    </div>
    <script>
       window.extAsyncInit = function() {
         setTimeout(function(){
          MessengerExtensions.requestCloseBrowser(function success() { // close dialogbox
              //alert("ok");
          }, function error(err) {
             // alert(err);
          });
      
        }, 1000);
      }
      </script>
     <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/build/js/custom.min.js"></script>
  </body>
</html>

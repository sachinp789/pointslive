<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mockups/dist/device-mockups.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/devices.min.css">
    <title>Akin</title>
    
    <!-- Hotjar Tracking Code for www.akinai.io -->
<script type="text/javascript">
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:930145,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121417530-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121417530-1');
</script>


<!-- Twitter universal website tag code -->
<script type="text/javascript">
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nzy0k');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2244243415809474');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2244243415809474&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- <script>
    fbq('track', 'Lead', { 
     content_name: 'Akin Website', //this is just an example. You should replace it with actual value
     content_category: 'page' //this is just an example. You should replace it with actual value
    });
</script> -->
<script>
  fbq('track', 'Lead');
</script>

</head>

<body class="front-page">
    <div class="sidebar-menu">
        <div class="sidebar-menu--close--container">
            <i id="menu_slide" class="fas fa-chevron-right"></i>
        </div>

        <div class="sidebar-menu--list--container">
            <ul class="sidebar-menu--list">
                <!-- <div class="sidebar-menu--signup--container" style="padding: 20px 20px 0;"> -->
                    <li class="sidebar-menu--signup--container sidebar-menu--list--item"> <a href="#" class="sidebar-menu--user--action nav-section--user--action btn login-btn sellers-btn" id="sellers-login" style="background:none; color: #333; padding:0; border:none; margin-bottom:0;"><img src="<?php echo base_url(); ?>assets/img/store.png" alt="" >For Vendors</a></li>
                <!-- </div> -->
                <li class="sidebar-menu--list--item">
                    <a href="#about-us">about akinai</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="#features">Services</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="#pricing">pricing</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="#" class="sidebar-menu--user--action nav-section--user--action btn login-btn login-btn-customers customers-btn" id="customers-login" style="background:none; color: #333; padding:0; border:none; margin-bottom:0;"><img src="<?php echo base_url(); ?>assets/img/family.png" alt="">For Citizens</a>
                </li>
            </ul>
        </div>

        <div class="sidebar-menu--signup--container">
            <!-- <a href="" class="sidebar-menu--user--action nav-section--user--action btn login-btn sellers-btn" id="sellers-login"><img src="<?php echo base_url(); ?>assets/img/logo.png" alt="">Sellers</a> -->
            <!-- <a href="" class="sidebar-menu--user--action nav-section--user--action btn login-btn login-btn-customers customers-btn" id="customers-login"><img src="assets/img/logo.png" alt="">Customers</a> -->
            <!-- <a href="" class="sidebar-menu--user--action btn">Signup / Login</a> -->
            <form id="form1" runat="server">
                <select id="nav-section--language--selector" name="lang-selector">
                    <option id="en">
                        <a href="javascript:;"><span>English </span></a>
                    </option>English
                    <!-- <option id="th">
                        <a href="javascript:;" ><span>Thai </span></a>
                    </option> -->
                </select>
            <!-- <div id="google_translate_element" style="display: none"></div> -->
            </form>
           <!--  <select name="lang-selector" id="nav-section--language--selector">
                <option value="nigeria">Nigeria</option>
                <option value="thailand" disabled>Thailand</option>
            </select> -->
        </div>
    </div>
    <div id="popup-form"></div>

    <!-- SELLERS LOGIN FORM EMAIL -->
        <div class="form-sec" id="customers-form">
            <div class="form-sec--close--container">
                <i id="menu_slide_form_sec_customers" class="fas fa-times"></i>
            </div>
            <span class="error" style="display: none;"></span>
            <form method="post" id="login-form">
            <div class="form-sec--main--container">
                <input type="email" id="email" placeholder="Enter Email" class="form-control">
                <input type="password" id="password" placeholder="Enter Password" class="form-control">
                <button type="submit" name="login-submit" id="customer-submit" class="btn">
                <span class="spinner"><i class="icon-spin icon-refresh" id="spinner"></i></span> Signup / In 
                </button>
                <!-- <input type="submit" value="Signup/In" class="btn" id="login-submit"> -->
            </div>
            </form>
        </div>

        <!-- CUSTOMERS LOGIN FORM EMAIL -->
    <div class="form-sec" id="sellers-form">
            <div class="form-sec--close--container">
                <i id="menu_slide_form_sec_sellers" class="fas fa-times"></i>
            </div>
            <span class="sellererror" style="display: none;"></span>
            <form method="post" id="login-form1">
            <div class="form-sec--main--container">
                <input type="email" id="selleremail" placeholder="Enter Email" class="form-control">
                <input type="password" id="sellerpassword" placeholder="Enter Password" class="form-control">
                <button type="submit" name="login-submit1" id="sellerlogin-submit" class="btn">
                <span class="spinner"><i class="icon-spin icon-refresh" id="spinner"></i></span> Signup / In
                </button>
                <!-- <input type="submit" value="Signup/In" class="btn" id="login-submit"> -->
            </div>
            </form>
        </div>

    <div class="form--main">
        <div class="form-main--close--container">
            <i class="fas fa-times"></i>
        </div>

        <div class="social-signup--container">
           <!--  <a href="<?php echo base_url()?>user/loginprocess/facebook"  class="social-signup social-signup--facebook">
                <i class="fab fa-facebook"></i>
                <span>sign up using facebook</span>
            </a> -->
            <!-- <a href="<?php echo base_url()?>user/loginprocess/insta" class="social-signup social-signup--instagram">
                <i class="fab fa-instagram"></i>
                <span> sign up using instagram</span>
            </a> -->

            <!-- email seller login -->
             <a href="#" class="social-signup social-signup--email second-anchor-signup sellers-email" id="sellers-login" style="display:none;">
                <i class="fas fa-envelope"></i>
                <span>
                    sign up using email 
                </span>
            </a>

            <!-- email customer login -->
             <a href="#" class="social-signup social-signup--email second-anchor-signup customers-email" id="customers-login" style="display:none;">
                <i class="fas fa-envelope"></i>
                <span>
                    sign up using email 
                </span>
            </a>

             <!-- twitter seller -->
            <a href="<?php echo base_url()?>user/loginprocess/twitter" class="social-signup social-signup--twitter sellers-twitter" style="display:none">
                <i class="fab fa-twitter"></i>
                <span>
                    sign up using twitter 
                </span>
            </a>

            <!-- twitter customer -->
             <a href="<?php echo base_url('customer/signIn/twitter');?>" class="social-signup social-signup--twitter customers-twitter" style="display:none">
                <i class="fab fa-twitter" ></i>
                <span>
                    sign up using twitter 
                </span>
            </a>

            <!-- Facebook customer -->
            <a href="<?php echo base_url('customer/signIn/facebook');?>" class="social-signup social-signup--facebook customers-twitter" style="display:none">
                <i class="fab fa-facebook" ></i>
                <span>
                    sign up using facebook 
                </span>
            </a>

           <!--  <a href="<?php echo base_url()?>user/loginprocess/twitter" class="social-signup social-signup--twitter">
                <i class="fab fa-twitter"></i>
                <span>
                    sign up using twitter
                </span>
            </a> -->
        </div>
    </div>
    <div id="go-top">
        <i class="fas fa-angle-up"></i>
    </div>
    <header>
        <div class="container">
            <div class="logo-section">
                <a href="<?php echo base_url(); ?>">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" />
                </a>
            </div>
            <div class="mobile-nav--section" style="display:none;">
                <a id="sidebar-open--anchor">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
            <div class="nav-section">
                 <form id="form1" runat="server">
                    <select id="nav-section--language--selector" name="lang-selector">
                        <option id="en">
                            <a href="javascript:;"><span>English </span></a>
                        </option>English
                       <!--  <option id="th">
                            <a href="javascript:;" ><span>Thai </span></a>
                        </option> -->
                    </select>
                <!-- <div id="google_translate_element" style="display: none"></div> -->
                </form>
                <ul class="nav-section--list">
                    <a href="" class="nav-section--user--action btn login-btn nav-section--list--anchor" id="sellers-login" style="background:none; border:none; padding:0; width: auto"><img src="<?php echo base_url(); ?>assets/img/store.png" alt="">For Vendors</a>
                    <li class="nav-section--list--item">
                        <a href="#about-us" class="about-us-anchor nav-section--list--anchor">About Akinai</a>
                    </li>
                    <li class="nav-section--list--item">
                        <a href="#features" class="features-anchor nav-section--list--anchor">Services</a>
                    </li>
                    <li class="nav-section--list--item">
                        <a href="#pricing" class="pricing-anchor nav-section--list--anchor">Pricing</a>
                    </li>
                   <!--  <a href="" class="nav-section--user--action btn login-btn" id="sellers-login"><i class="fa fa-university" aria-hidden="true" style="margin-right: 5px;font-size: 30px;"></i>Sellers</a>
                    <li class="nav-section--list--item">
                        <a href="#about-us" class="about-us-anchor nav-section--list--anchor">About Akinai</a>
                    </li>
                    <li class="nav-section--list--item">
                        <a href="#instagram" class="features-anchor nav-section--list--anchor">Features</a>
                    </li>
                    <li class="nav-section--list--item">
                        <a href="#pricing" class="pricing-anchor nav-section--list--anchor">Pricing</a>
                    </li> -->
                </ul>
                <!-- <a href="" class="nav-section--user--action btn">Signup / Login</a> -->
                <!-- <a href="" class="nav-section--user--action btn login-btn" id="sellers-login"><img src="<?php echo base_url();?>assets/img/logo.png" alt="">Sellers</a> -->
                <a href="" class="nav-section--user--action btn login-btn login-btn-customers nav-section--list--anchor" id="customers-login" style="background:none;border:none; padding:0; width: auto"><img src="assets/img/family.png" alt="">For Citizens</a>
                <!-- <a href="" class="nav-section--user--action btn login-btn login-btn-customers" id="customers-login"><img src="assets/img/logo.png" alt="">Customers</a> -->
                <!-- <select name="lang-selector" id="nav-section--language--selector">
                    <option value="nigeria">Nigeria</option>
                    <option value="thailand" disabled>Thailand</option>
                </select> -->
               
            </div>
        </div>
    </header>
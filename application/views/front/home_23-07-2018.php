
     <div id="banner">
        <div class="container">
            <h1 class="banner-tagline section-title">Social Commerce for individuals and small businesses. 
</h1>
            <h2 class="banner-slogan section-content">No coding and free setup !</h2>
            <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                <h3 class="banner-slogan section-content message"><?php echo $this->session->flashdata('flashmsg');?></h3>
            <?php endif;?>
            <div class="banner-anchor">
                <a href="#" class="nav-section--user--action btn">Get Started</a>
                <a href="https://rite.ly/KPhr" class=" btn commentsToggle">
                    <!-- <i class="fab fa-facebook-messenger" style="margin-right: 10px; font-size:25px; position: relative; top: 4px;"></i> --><img src="<?php echo base_url(); ?>assets/img/logo.png" width="30" height="30" alt="">Try It Out</a>
            </div>
            <div class="videos">
                <div class="desktop">
                    <div class="content">
                        <video loop="loop" preload="auto" vid="100">
                            <source src="<?php echo base_url(); ?>assets/img/video_desktop_iphone.mp4">
                        </video>
                    </div>
                </div>
                <div class="mobile">
                    <div class="content">
                        <video loop="loop" preload="auto" vid="200">
                            <source src="<?php echo base_url(); ?>assets/img/video_desktop_iphone.mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section id="about-us" class="">
        <div class="container">
            <div class="about-content--wrap">
                <div class="about-content--description">
                    <p class="section-title">
                        We believe that existing technology should be used to improve people's lives by adding value to it.
                        <br><br>
                        So we created Akin, a Social Commerce service that is going to change the way we discover and pay for things online and offline
                    </p>
                </div>
                <div class="about-content--logo">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" />
                </div>
            </div>
        </div>
       <div class="container">
            <div class="feature-content--wrap" id="instagram">
                <div class="feature-content--image">
                    <img src="<?php echo base_url(); ?>assets/img/fb-side.png" alt="">
                </div>
                <div class="feature-content--main">
                    <p class="section-title">
                        Connect your Instagram, Facebook and Twitter accounts to 1 platform. Use Facebook for a simple and easy social commerce checkout!
                        <br>
                        <br> We're adding new channels every month. Follow us on
                        <a href="#">Facebook</a>,
                        <a href="#">Instagram</a> and
                        <a href="#">Twitter</a>
                        to stay updated.
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="feature-content--wrap">
                <div class="feature-content--image">
                    <img src="<?php echo base_url(); ?>assets/img/flutterwave.png" alt="">
                </div>
                <div class="feature-content--main">
                    <p class="section-title">
                        Seamless ePayments experience on Social Media
                    </p><br>
                    <p class="section-title">
                        All payments are powered by Flutterwave! Connect your Rave by Flutterwave account to allow you accept payments through your chatbot.
                    </p>
                </div>
            </div>
        </div>
        <div class="features-wrap" id="features">
            <div class="feature-single--wrap">
                <div class="feature-single--icon color-1">
                    <i class="fas fa-robot"></i>
                </div>
                <span class="feature-single--title">bot power</span>
                <div class="feature-single--content">
                    <p>
                        simple chatbots that makes information easy to discover
                    </p>
                </div>
            </div>
            <div class="feature-single--wrap ">
                <div class="feature-single--icon color-2">
                    <i class="fas fa-users"></i>
                </div>
                <span class="feature-single--title">MULTICHANNEL</span>
                <div class="feature-single--content">connect with your audience on different social media channels, all in one platform</div>
            </div>
            <div class="feature-single--wrap ">
                <div class="feature-single--icon color-4 ">
                    <i class="fas fa-shopping-cart"></i>
                </div>
                <span class="feature-single--title">easy checkout</span>
                <div class="feature-single--content">checkout through facebook or directly with payment link</div>
            </div>
            <div class="feature-single--wrap ">
                <div class="feature-single--icon color-3">
                    <i class="fas fa-qrcode"></i>
                </div>
                <span class="feature-single--title">OMNI CHANNEL</span>
                <div class="feature-single--content">QR code can be used to checkout offline or bring your audience from physical location to your channel</div>
            </div>
        </div>
    </section>
    <!-- 
    <section id="features" class="border-blue">
        <div class="container">

        </div>
    </section> -->

        <section id="pricing" class="slide slide3">
        <div class="container">
            <div class="pricing-details--wrap">
                <div class="pricing-details pricing-details--free">
                    <div class="pricing-details--icon ">
                        <i class="free--icon fas fa-thumbs-up"></i>
                        <img class="free-tag--image" src="<?php echo base_url(); ?>assets/img/free.png" />
                    </div>
                    <div class="pricing-details--title">
                        <p style="text-align:right"></p>
                    </div>
                    <div class="pricing-details--content">
                        <p>
                           As part of our Launch you can use Akin for free for the 1st month !
                        </p>
                    </div>
                </div>

                <div class="pricing-details pricing-details--subscription">
                    <div class="pricing-details--icon doller">
                        <i class="fas fa-hand-holding-usd"></i>
                    </div>
                    <div class="pricing-details--title">
                        <p>
                            NO HIDDEN FEES. WE CHARGE A FIXED FEE OF 3.5% OF YOUR ORDER VALUE
                        </p>
                    </div>
                    <div class="pricing-details--content">
                        

                        <p class="price--anchor">
                            <span class="price--main"></span>
                            <span class="price--duration"></span>
                            <a href="" class="nav-section--user--action btn price--btn">Signup / Login</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
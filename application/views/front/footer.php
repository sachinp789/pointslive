  <footer>
        <div class="container">
            <div class="footer-content--about">
                <div class="footer-content">
                    <a class="footer-content--logo" href="#">
                        <img src="<?php echo base_url(); ?>assets/img/logo.png" />
                    </a>
                    <p>
                        we believe that technology should be used to improve people's lives by adding value to it
                    </p>
                </div>

                <ul class="social-list--main">
                    <li>
                        <a href="https://business.facebook.com/akinbear">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/akinaiafrica">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/akinai_africa/ ">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="footer-content--quicklinks">
                <ul class="footer-content--quicklinks--list">
                    <li>
                        <a href="">Terms of Use</a>
                    </li>
                    <li>
                        <a href="">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="#" id="sellers-login" class="nav-section--user--action">Signup / Login</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="copyright-container">
            <p>
                &copy; 2018, all rights reserved.
            </p>
        </div>
    </footer>
     <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
     <!-- <script src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script> 


    <script type="text/javascript">
         $(document).on('click','#sellers-login',function() {
            fbq('track', 'CompleteRegistration', { 
                 content_name: 'seller', //this is just an example. You should replace it with actual value
                 content_category: 'signup', //this is just an example. You should replace it with actual value
                 value: 1,
                 currency: 'NGN'
            });

             //fbq('track', 'CompleteRegistration');
        });
        $(document).on('click','#customers-login',function() { 
            fbq('track', 'CompleteRegistration', { 
                 content_name: 'customer', //this is just an example. You should replace it with actual value
                 content_category: 'signup', //this is just an example. You should replace it with actual value
                 value: 1,
                 currency: 'NGN'
            });
            //fbq('track', 'CompleteRegistration');
        });
    </script>

    
   <!--  <script type="text/javascript">
        function googleTranslateElementInit() {         
            new google.translate.TranslateElement({ pageLanguage: 'en,th', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false }, 'google_translate_element');
        }
    </script>
    
    <script type="text/javascript">
        function translateLanguage(lang) {

            var $frame = $('.goog-te-menu-frame:first');
            if (!$frame.size()) {
                alert("Error: Could not find Google translate frame.");
                return false;
            }
            $frame.contents().find('.goog-te-menu2-item span.text:contains(' + lang + ')').get(0).click();
            return false;
        }
    </script> -->

    <!-- <script type="text/javascript">
        // Website chatbot hide/show 
        $('.commentsToggle').click(function(e){
            e.preventDefault();
            $(".webchat").toggleClass('hidden');
        });

    </script>

    <div class="webchat hidden">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-customerchat"
          attribution="setup_tool"
          page_id="372330003262949">
        </div>
    </div> -->

</body>

</html>
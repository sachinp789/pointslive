
     <div id="banner" class="banner">
        <div class="container">
            <h1 class="banner-tagline section-title">Building communities and people with eCommerce
</h1>
            <!-- <h2 class="banner-slogan section-content">welcome to the akinai experience</h2> -->
            <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                <h3 class="banner-slogan section-content message"><?php echo $this->session->flashdata('flashmsg');?></h3>
            <?php endif;?>
            <div class="banner-anchor">
                <a href="#about-us" class="btn" id="banner-cta">Find Out More</a>
                <!-- <a href="#" class="nav-section--user--action btn" id="sellers-login">Setup Your Store</a> -->
                <!-- <a href="#" class="nav-section--user--action btn" id="sellers-login">Get Started</a>
                <a href="https://rite.ly/KrlX" class=" btn commentsToggle">
                <img src="<?php echo base_url(); ?>assets/img/logo.png" width="30" height="30" alt="">Try It Out</a> -->
            </div>
            <!-- <div class="videos">
                <div class="desktop">
                    <div class="content">
                        <video id="episodeVideo" autoplay="autoplay" preload muted onended="run()" data-info="">
                            <source src="<?php echo base_url(); ?>assets/img/Video1.mov">
                        </video>
                    </div>
                </div>
                <div class="mobile">
                    <div class="content">
                        <video id="episodeVideo1" data-info="" muted autoplay>
                        </video>
                    </div>
                </div>
            </div> -->
            <div class="mobile-navigator">
                <ul class="digits">
                    <li class="active"><a href="" data-index="1">1</a></li>
                    <li class=""><a href="" data-index="2">2</a></li>
                    <li class=""><a href="" data-index="3">3</a></li>
                    <li class=""><a href="" data-index="4">4</a></li>
                </ul>

                <div class="mobile-navigator-text">
                    <div class="step-single active" data-index="1">
                        <span>Click the link</span>
                        <p>whether on Twitter, Instagram or Facebook, click the rite.ly link to get to the store</p>
                    </div>
                    <div class="step-single" data-index="2">
                        <span>Browse the Store</span>
                        <p>See what the seller has to offer or check out our Care Packages</p>
                    </div>
                    <div class="step-single" data-index="3">
                        <span>Pay securely online</span>
                        <p>Use bank transfer, bank card or USSD. Get refund in 24 hours if your order is cancelled
                            or fails to get to you</p>
                    </div>
                    <div class="step-single" data-index="4">
                        <span>Ratings and Reviews</span>
                        <p>Rate your experience with vendors to help build a strong trusted community</p>
                    </div>
                </div>
            </div>


            <div class="banner-mockup-container">
                <div class="mockup-main">
                    <div class="device-wrapper">
                        <div class="device" data-device="iPhone7" data-orientation="portrait" data-color="black">
                            <div class="screen mockup-image">
                                <img data-index="1" src="<?php echo base_url(); ?>assets/img/1.jpg" alt="" class="active">
                                <img data-index="2" src="<?php echo base_url(); ?>assets/img/2.jpg" alt="" class="">
                                <img data-index="3" src="<?php echo base_url(); ?>assets/img/3.jpg" alt="" class="">
                                <img data-index="4" src="<?php echo base_url(); ?>assets/img/4.jpg" alt="" class="">
                            </div>
                            <div class="button">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="navigator">
                    <div class="step-single active" data-index="1">
                        <button><span>1</span> Click the link</button>
                        <p>whether on Twitter, Instagram or Facebook, click the rite.ly link to get to the store</p>
                    </div>
                    <div class="step-single" data-index="2">
                        <button><span>2</span> Browse the Store</button>
                        <p>See what the seller has to offer or check out our Care Packages</p>
                    </div>
                    <div class="step-single" data-index="3">
                        <button><span>3</span> Pay securely online</button>
                        <p>Use bank transfer, bank card or USSD. Get refund in 24 hours if your order is cancelled or
                            fails to get to you</p>
                    </div>
                    <div class="step-single" data-index="4">
                        <button><span>4</span> Ratings and Reviews</button>
                        <p>Rate your experience with vendors to help build a strong trusted community</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section id="about-us" class="">
        <div class="container">
            <div class="about-content--wrap">
                <div class="about-content--description">
                    <p class="section-title">
                       Akinai was created to make it easy for people to get access to the things they need for daily life locally.
                        <br><br>
                        We connect local vendors to citizens through our eCommerce platform. This platform will provide a technically strong foundation to build communities and the people in those communities.
                        <br><br>
                        Click this link to see what it is like to discover prodicts and services on out platform - <a href="https://rite.ly/KrlX" target="_blank">https://rite.ly/KrlX</a>
                       <!--  <div class="banner-anchor-2">
                            <a href="https://rite.ly/KrlX" class="btn btn-description-checkout"><i class="fa fa-arrow-circle-right" aria-hidden="true" style="font-size: 30px;margin-right: 10px;"></i>Try It Out !</a> 
                        </div> -->
                    </p>
                </div>
                <div class="about-content--logo">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" />
                </div>
            </div>
        </div>
       <div class="container">
            <div class="feature-content--wrap" id="instagram">
                <div class="feature-content--image">
                    <img src="<?php echo base_url(); ?>assets/img/fb-side.png" alt="">
                </div>
                <div class="feature-content--main">
                   <h3  class="section-title" style="font-weight: bold;">SETUP - CREATE - SHARE !</h3>
                    <p class="section-title">
                        Setup your store and create categories with products. Share shortlinks to products on social media or print a QR Code and let your customers scan at your store.
                    </p>
                    <br> 
                    <h3  class="section-title" style="font-weight: bold;">CUSTOMERS REVIEWS</h3>
                    <p class="section-title">
                        Bought something from a seller ? Rated them so that everyone knows what to expect from that seller
                    </p>
                    <br>
                    <h3  class="section-title" style="font-weight: bold;">GET PROMOTED</h3>
                     <p class="section-title">
                        Top rated sellers will be promoted every week for free on Facebook, Twitter and Instagram
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="feature-content--wrap">
                <div class="feature-content--image">
                    <img src="<?php echo base_url(); ?>assets/img/flutterwave.png" alt="">
                </div>
                <div class="feature-content--main">
                    <p class="section-title">
                        As a Seller, enter your bank details while setting up your store and automatically recieve payments weekly (every friday) for your orders
                    </p><br>
                    <p class="section-title">
                        As a Customers, buy products with confidence through Flutterwave &trade;
                    </p>
                </div>
            </div>
        </div>
        <div class="features-wrap" id="features">
        <div class="service-wrap">
            <div class="service-header">Akinai <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width:30px;" alt=""> Store</div>
            <div class="service-content">
                <h2>FREE ONLINE STORE</h2>
                <p>
                    Start selling in less than 5 minutes with our guided setup.
                    New and highest rated sellers get promoted for free!
                </p>
            </div>

            <div class="service-read-more">
                <a href="#akin-store">Read More !</a>
            </div>
        </div>
        <div class="service-wrap">
            <div class="service-header">Akinai <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width:30px;" alt=""> Care</div>
            <div class="service-content">
                <h2>SOCIAL WELFARE PROGRAM</h2>
                <p>
                    We know that not everyone has enough to survice.</p>
                <p>
                    Buy a care package and we will work with vendors to provide for those most in need.</p>
            </div>

            <div class="service-read-more">
                <a href="#akin-care">Read More !</a>
            </div>
        </div>
        <div class="service-wrap">
            <div class="service-header">Akinai <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width:30px;" alt=""> Citizen</div>
            <div class="service-content">
                <h2>
                    PEOPLE PROGRAM</h2>
                <p>
                    A community or state or country is nothing without its people.</p>
                <p>
                    Freebies, Jobs, Community building and so much more !
                </p>
            </div>
            <div class="service-read-more">
                <a href="#akin-citizen">Read More !</a>
            </div>
        </div>
    </div>
        <!-- <div class="features-wrap" id="features">
            <div class="feature-single--wrap">
                <div class="feature-single--icon color-1">
                    <i class="fab fa-connectdevelop"></i>
                </div>
                <span class="feature-single--title">Product Discovery</span>
                <div class="feature-single--content">
                    <p>
                        Simple Interface that allows buyers browse the catalogue of social media sellers
                    </p>
                </div>
            </div>
            <div class="feature-single--wrap ">
                <div class="feature-single--icon color-2">
                    <i class="fas fa-users"></i>
                </div>
                <span class="feature-single--title">MULTICHANNEL</span>
                <div class="feature-single--content">connect with your audience on different social media channels, all in one platform</div>
            </div>
            <div class="feature-single--wrap ">
                <div class="feature-single--icon color-4 ">
                    <i class="fas fa-shopping-cart"></i>
                </div>
                <span class="feature-single--title">easy checkout</span>
                <div class="feature-single--content">add items to a shopping cart and checkout using Flutterwave</div>
            </div>
            <div class="feature-single--wrap ">
                <div class="feature-single--icon color-3">
                    <i class="fas fa-qrcode"></i>
                </div>
                <span class="feature-single--title">OMNI CHANNEL</span>
                <div class="feature-single--content">QR code can be used to checkout offline or bring your audience from physical location to your channel</div>
            </div>
        </div> -->
    </section>
    
    <section>
    <div class="container">
        <div id="akin-store" class="service-content-section akin-store">
            <div class="section-content">
                <div class="section-title">AKINAI STORE</div>
                <p>Our guided setup means that you will be able to start selling to your community in under 5 minutes. After you
                    create products, share links online or let people scan QR Codes to discover your store.
                    <p>
                        Akinai acts as a middle man. Your customer pays us through the platform using their card, bank transfer or USSD.
                        We then pay you every Friday.
                    </p>
                    <p>
                        Every week we promote new sellers as well as the top rated sellers on Instagram, Twitter, Facebook, WhatsAPP for
                        free !</p>
                        <a href="#" class="btn nav-section--user--action btn login-btn nav-section--list--anchor" id="sellers-login">become a seller</a>
            </div>
            <div class="section-image"><img src="<?php echo base_url(); ?>assets/img/section-store.png" alt=""></div>
        </div>
        <div id="akin-care" class="service-content-section akin-care">
            <div class="section-content">
                <div class="section-title">
                    AKINAI CITIZEN</div>
                <p>
                    Signup and let's start building the country that you love ! Get sellers to sign up in your communities and make
                    sure to give them your referral code
                </p>
                <p>
                    Earn up to 30,000 NGN a month when you get sellers to sign and serve at least 5 paying customers.</p>
                <p>
                    We know that's not enough, so if you are an active user on the platform, you get to use some services for free and
                    get almost all other services at a discount
                </p>

                <a href="#" class="btn nav-section--user--action btn login-btn login-btn-customers nav-section--list--anchor" id="customers-login">become a citizen</a>
            </div>
            <div class="section-image"><img src="<?php echo base_url(); ?>assets/img/citizen.png" alt=""></div>
        </div>
        <div id="akin-citizen" class="service-content-section akin-citizen">
            <div class="section-content">
                <div class="section-title">AKINAI CARE</div>
                <p>

                    CARE is open to both citizens and the general public.
                </p>
                <p>
                    Nigeria is a tough place to survive. A large number of people and families are not able to eat 3 meals a day, get
                    access to medicine, buy provisions and so much more.
                </p>
                <p>
                    When you buy a care package, we work directly with vendors to make sure that those in need get service for free.
                    For now, our platform randomly selects people from a list to receive a care package.
                </p>
                <p>
                    Buy a package and change someone's life. If you don't hace the means then plaese share your ideas on how we can
                    help people better

                </p>
                <a href="#" class="btn">buy a package</a>

                <a href="#" class="feedback-link">give feedback</a>
            </div>
            <div class="section-image"><img src="<?php echo base_url(); ?>assets/img/care.png" alt=""></div>
        </div>
    </div>
</section>

        <section id="pricing" class="slide slide3">
        <div class="container">
            <div class="pricing-details--wrap">
                <div class="pricing-details pricing-details--free">
                    <div class="pricing-details--icon bullhorn">
                        <!-- <i class="free--icon fas fa-thumbs-up"></i> -->
                        <img width="290" src="<?php echo base_url(); ?>assets/img/free-badge.png" />
                    </div>
                    <div class="pricing-details--title">
                        <p style="text-align:right"></p>
                    </div>
                    <div class="pricing-details--content">
                        <p>
                           SIGN UP AND GET 14 DAYS FREE TRIAL OF THE SERVICE WITH NO COMMITMENT
                        </p>
                    </div>
                </div>

                <div class="pricing-details pricing-details--subscription">
                    <div class="pricing-details--icon bullhorn">
                        <img src="<?php echo base_url(); ?>assets/img/deal.png" alt="">
                    </div>
                    <div class="pricing-details--title">
                        <p>
                            
                        </p>
                    </div>
                    <div class="pricing-details--content">
                        
                        <p>
                          UNTIL THE END OF 2018, WE WILL CHARGE FLAT FEE OF 3.5% PER CONFIRMED ORDER
                        </p>
                        <p class="price--anchor">
                            <a href="" class="nav-section--user--action btn price--btn" id="sellers-login">Signup / Login</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
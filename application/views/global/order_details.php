<?php
$total = 0;
$price = 0;
$currency = '';
$sellerInfo = admin_info($orders[0]->created_by);
if($sellerInfo->country == "thailand"){
    $currency = "฿";
}
else if($sellerInfo->country == "nigeria"){
    $currency = "₦";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jqueryui.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title; ?></title>
</head>

<body id="order_details">
    <div class="desktop-view">
        <div class="sidebar-menu">
            <div class="sidebar-menu--close--container text-right">
                <i id="menu_slide" class="fas fa-chevron-left"></i>
            </div>

            <div class="sidebar-menu--list--container">
                <ul class="sidebar-menu--list">
                    <?php if($this->session->userdata('customerId')):?>
                    <li class="sidebar-menu--list--item"><a href="<?php echo base_url("item/orders")?>">Order History</a></li>
                    <li class="sidebar-menu--list--item"><a href="<?php echo base_url()?>customer/signOut">Logout</a></li>
                     <?php endif;?>
                </ul>
            </div>
        </div>
        <div id="popup-form"></div>
        <section id="page-title-section">
            <div class="container">
                <div class="mobile-menu">
                    <a href="#" id="sidebar-open--anchor">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                 <select id="main-nav--selector" class="main-nav--selector">
                        <option value="#">Go to</option>
                        <?php if($this->session->userdata('customerId')):?>
                        <option data-fa="fa fa-history" value="<?php echo base_url("item/orders")?>">Order History</option>
                        <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                        <?php endif;?>
                    </select>
                <h1><?php echo $title; ?></h1>
                <a href="#">
                    <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
                </a>
            </div>
        </section>
        <section id="order-meta">
            <div class="container">
               <!--  <?php if (!empty($this->session->flashdata('flashmsg'))): ?>
                    <div id="message-box-<?php echo $this->session->userdata('msg')?>"><img src="<?php echo base_url('assets/img/logo.png')?>"><?php echo $this->session->flashdata('flashmsg');?></div>
                <?php endif;?> -->
            <div id="message-box-success"></div>
            </div>
            <div class="container">
                <div class="order-date">
                    <h2>Order Date:</h2>
                    <span><?php echo date("jS F, Y", strtotime($orders[0]->created_date));  ?></span>
                </div>
                <div class="order-id">
                    <h2>Order ID:</h2>
                    <span><?php echo $orders[0]->transaction_id ?></span>
                </div>
                <div class="order-id">
                    <h2>Payment Type:</h2>
                    <span><?php echo $orders[0]->payment_method ?></span>
                </div>
                <div class="total">
                    <h2>Order Total:</h2>
                    <span><?php echo $currency."".$orders[0]->order_amount ?></span>
                </div>
            </div>
        </section>
        <div class="container eticket-container">
            <div class="status-container">
                <h3>Status: </h3>
                <?php
                if($orders[0]->status == '0'){
                    $class = 'processing';
                    $text = 'Processing';
                }
                else if($orders[0]->status == '1'){
                    $class = 'dispatched';
                    $text = 'Dispatched';
                }
                else if($orders[0]->status == '2'){
                    $class = 'completed';
                    $text = 'Delivered';    
                }
                else if($orders[0]->status == '3'){
                    $class = 'canceled';
                    $text = 'Canceled';
                }
                else if($orders[0]->status == '4'){
                    $class = 'rejected';
                    $text = 'Rejected';
                }
                else if($orders[0]->status == '5'){
                    $class = 'failed';
                    $text = 'Failed';
                }
                else if($orders[0]->status == '6'){
                    $class = 'completed';
                    $text = 'Completed';  
                }
                else if($orders[0]->status == '7'){
                    $class = 'failed';
                    $text = 'Confirm Failed';  
                }
                /*else if($orders[0]->status == '6'){
                    $class = 'processing';
                    $text = 'Processing';
                }*/
                ?>
                <p class="status status-<?php echo $class;?>"><?php echo $text;?></p>
            </div>
            <div class="action-container">
                <?php if($orders[0]->status == '2' && $orders[0]->store_type == 'delivery') {?>
                <a href="<?php echo base_url("item/confirmed_order_by_customer/".encrypt($orders[0]->id))?>" class="btn confirm-delivered">Confirm Delivered</a>
                <?php }?>
                <?php if($orders[0]->status == '5') {?>
                <a href="#" class="btn confirm-failed">Confirm Failed</a>
                <?php }?>
                <?php if($orders[0]->status == '2' || $orders[0]->status == '6'){?>
                <a href="<?php echo base_url("item/review/".encrypt($orders[0]->id))?>" class="btn">Review</a>
                <?php }?>
                <a href="<?php echo base_url("uploads/invoices/eTicket-{$orders[0]->transaction_id}.pdf")?>" class="btn" download>Download eTicket</a>

                <div class="dropdown-confirm-failed">
                    <div class="radio">
                        <input id="radio-1" name="failed_reason" type="radio" value="Fail Reason 1" class="deliverytype">
                        <label for="radio-1" class="radio-label">Fail Reason 1</label>
                    </div>
                    <div class="radio">
                        <input id="radio-2" name="failed_reason" type="radio" value="Fail Reason 2" class="deliverytype">
                        <label for="radio-2" class="radio-label">Fail Reason 2</label>
                    </div>
                    <div class="radio">
                        <input id="radio-3" name="failed_reason" type="radio" value="Fail Reason 3" class="deliverytype">
                        <label for="radio-3" class="radio-label">Fail Reason 3</label>
                    </div>
                    <div class="radio">
                        <input id="radio-4" name="failed_reason" type="radio" value="Fail Reason 4" class="deliverytype">
                        <label for="radio-4" class="radio-label">Fail Reason 4</label>
                    </div>
                    <div class="radio">
                        <input id="radio-5" name="failed_reason" type="radio" value="Fail Reason 5" class="deliverytype">
                        <label for="radio-5" class="radio-label">Fail Reason 5</label>
                    </div>
                    <input type="hidden" name="orderid" value="<?php echo $orders[0]->id?>" id="orderid">
                    <input type="submit" name="btnaction" value="Submit" class="btn" id="btnaction"><span id="loader"></span>
                </div>
            </div>

        </div>
        <section id="cart-section">
            <div class="container">
                <?php if(count($orders) > 0): 
                    foreach($orders[0]->products as $value) :
                    if($value->sale_price > 0){
                        $price = $value->sale_price;
                    }
                    else{
                        $price = $value->product_price;
                    }
                    //$total  = $total + ($price * $value->ordered_qty);
                ?>
                <div class="cart-single">
                    <div class="cart-image">
                        <?php
                        $pimages = $this->db->select('product_image')->from('product_images')->where('product_id',$value->product_id)->get()->row();
                        ?>
                        <img src="<?php echo base_url()?>uploads/products/<?php echo $pimages->product_image?>" alt="">
                    </div>
                    <div class="cart-details">
                        <div class="name-container">
                            <h2><?php echo $value->product_name; ?></h2>
                        </div>
                        <div class="product-price">
                            <h2>Quantity</h2>
                            <h3><?php echo $value->ordered_qty; ?></h3>
                        </div>
                        <div class="product-price">
                            <h2>Price</h2>
                            <h3><?php echo $currency."".number_format($price); ?></h3>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php else :?>
                <p>* No orders found.</p>
                <?php endif;?>
            </div>
            <div class="container">
                <div class="total-price-container">
                    <div class="label-container">
                        <h2>Total Price</h2>
                    </div>

                    <div class="price-container">
                        <p><?php echo $currency."".number_format($orders[0]->order_amount);?></p>
                    </div>
                </div>
            </div>
            <?php if($orders[0]->store_type == "delivery"):?>
            <div class="container order-details-container">
                <h2>Shipping Address</h2>
                <p>
                    <?php echo $orders[0]->shipping_address.",\n".$orders[0]->state.",\t".$orders[0]->local_government.",\n".$orders[0]->country?>
                </p>
            </div>
            <?php endif;?>
        </section>
    </div>
    
    <div class="mobile-view">
        <div class="details-overlay"></div>
        <div id="order-confirm-popup" class="modal-window">
            <div class="close-container">
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>

            <div class="modal-content">
                <img class="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="">
                <?php if($orders[0]->status != '6') :?>
                <h2>Do you confirm this order is Delivered/Failed ?</h2>

                <div class="action-container">
                    <a href="#" class="confirm-positive" id="confrim-order-event" data-id="<?php echo $orders[0]->id?>">Confirmed</a>
                    <a href="#" class="confirm-negative" id="confrim-order-negative" data-id="<?php echo $orders[0]->id?>">No</a>
                </div>
                <?php else :?>
                <h2>* Order has already been confirmed.</h2>
                <?php endif;?>
            </div>
        </div>

        <div id="order-rating-popup" class="modal-window">
            <div class="close-container">
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>

            <div class="modal-content">
                <img class="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="">
                <?php if(empty($orders[0]->order_rate)):?>
                <h2>Please rate the product/service you got from</h2>

                <div class="rate-popup" data-rate-value="<?php echo ($orders[0]->order_rate) ? $orders[0]->order_rate : "0";?>" data-id="<?php echo $orders[0]->id?>"></div>
                <?php else :?>
                <h2 style="font-size: 15px;">* You already rated this order.</h2>
                <?php endif;?>    
            </div>
        </div>

        <div class="navigator-head">
            <div class="container">
                <div class="left-head">
                    <a href="<?php echo base_url("item/orders")?>"><i class="fa fa-chevron-left"></i>Back</a>
                </div>
                <div class="right-head navigator-right">
                    <p class="status status-<?php echo $class?> navigator-status-order ">Status: <span class=""><?php echo $text;?></span></p>
                </div>
            </div>
        </div>
        <div class="order-info-container">
             <?php if(count($orders) > 0): 
                    foreach($orders[0]->products as $value) :
                    if($value->sale_price > 0){
                        $price = $value->sale_price;
                    }
                    else{
                        $price = $value->product_price;
                    }
                ?>
            <div class="order-listing">
                <div class="order-single">
                    <!-- <div class="close-container">
                        <a href="#"><i class="fa fa-times"></i></a>
                    </div> -->
                    <div class="order-details">
                        <?php
                        $pimages = $this->db->select('product_image')->from('product_images')->where('product_id',$value->product_id)->get()->row();
                        ?>
                        <div class="order-image">
                            <img src="<?php echo base_url()?>uploads/products/<?php echo $pimages->product_image?>" alt="">
                        </div>

                        <div class="order-meta">
                            <div class="product-title">
                                <p><?php echo $value->product_name; ?></p>
                            </div>

                            <div class="product-quantity">
                                <p>Quantity Bought: <span><?php echo $value->ordered_qty; ?></span></p>
                            </div>
                            <div class="product-amount">
                                <p>Total Amount: <span><?php echo $currency."".number_format($price); ?></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php else :?>
            <p>* No orders found.</p>
            <?php endif;?>
        </div>

        <div class="seller-details-container">
            <div class="container">
                <div class="seller-name">
                    <i class="fas fa-id-card"></i><span><?php echo $sellerInfo->admin_name;?></span>
                </div>
                <div class="seller-contact-number">
                    <i class="fas fa-phone"></i><span><?php echo $sellerInfo->admin_mobile;?></span>
                </div>
                <div class="delivery-type">
                    <i class="fas fa-truck"></i><span><?php echo ucfirst($orders[0]->store_type);?></span>
                </div>
                <?php if($orders[0]->store_type == "delivery"):?>
                <div class="delivery-address">
                    <i class="fas fa-location-arrow"></i><span><?php echo $orders[0]->shipping_address.",\n".$orders[0]->state.",\t".$orders[0]->local_government.",\n".$orders[0]->country?></span>                
                </div>
                <?php endif;?>
            </div>
        </div>

        <div class="order-details-action-container">
            <div class="total-row">
                <p class="label">Total: </p>
                <p><?php echo $currency."".number_format($orders[0]->order_amount);?></p>
            </div>
            <div class="action-bar">
                <a href="#" id="order-confirm-anchor">Confirm Order</a>
                <a href="#" id="order-rating-anchor">Rating</a>
            </div>
        </div>
    </div>    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <script src="<?php echo base_url()?>assets/js/jqueryui.js"></script>
    <script src="<?php echo base_url()?>assets/js/rater.js"></script>
    <script type="text/javascript">
    baseurl = "<?php echo base_url()?>";    
    message = "<?php echo $this->session->flashdata('flashmsg');?>";
    if(message.length > 0){
        $('#message-box-success').html('<img src='+baseurl+'assets/img/logo.png>'+message);
        $('#message-box-success').slideDown('slow').delay(2000).slideUp('slow');
    }
    </script>
    <script>
        $('#order-confirm-anchor').on('click', function () {
            $('#order-confirm-popup').fadeIn();
            $('.details-overlay').fadeIn();

            $('#order-confirm-popup .close-container a').on('click', function () {
                $('#order-confirm-popup').fadeOut();
                $('.details-overlay').fadeOut();
            });
        });

        $('#order-rating-anchor').on('click', function () {
            $('#order-rating-popup').fadeIn();
            $('.details-overlay').fadeIn();

            $('#order-rating-popup .close-container a').on('click', function () {
                $('#order-rating-popup').fadeOut();
                $('.details-overlay').fadeOut();
            });
        });

         $('.confirm-negative').on('click', function () {
            $('#order-confirm-popup').fadeOut();
            $('.details-overlay').fadeOut();
        });

        $(".rate-popup").rate({
            max_value: 5,
            step_size: 1,
        }).on('click', function () {
            ratevalue = $(this).attr("data-rate-value");
            orderid = $(this).attr("data-id");
            $.ajax({
              url:'<?php echo base_url()?>item/ajax_order_rating_by_id',
              type:"POST",
              data: {orderID:orderid,orderRate:ratevalue},
              dataType:"json",
              beforeSend: function(){ 
                $(".rate-popup").html('&nbsp; <span style="font-size: 18px;">Please wait...</span>');
              },
              success: function(response){
                if(response.staus == true){
                    alert(response.msg);
                }
                else{
                    alert(response.msg);
                }
                location.reload();
              }
            });
        });
    </script>
</body>

</html>
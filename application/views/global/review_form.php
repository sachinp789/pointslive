<?php
 $orderinfo = $this->db->select()->from('orders')->where('id',$orderID)->get()->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?php echo $title;?></title>
</head>

<body>
    <section id="page-title-section">
        <div class="container">
            <h1>Feedback</h1>
        </div>
    </section>
    <section id="checkout-form-section">
        <div class="container">
            <!-- <?php if (!empty($this->session->flashdata('flashmsg'))): ?>
                <div id="message-box-<?php echo $this->session->userdata('msg')?>"><?php echo $this->session->flashdata('flashmsg'); ?></div>
            <?php endif;?> -->
            <?php if (!empty($this->session->flashdata('flashmsg')) && $this->session->userdata('msg') == 'error'): ?>
                <div id="message-box-error"><?php echo $this->session->flashdata('flashmsg'); ?></div>
            <?php endif;?>
            <?php if (!empty($this->session->flashdata('flashmsg'))):?>
            <div id="message-box-success" style="margin: 0px 0px 15px;padding: 0;"></div>
            <?php endif;?>
            <?php if(validation_errors()) :?>
            <div class="error">
            <?php echo validation_errors(); ?>
            </div>
            <?php endif;?>
            <?php if(empty($orderinfo->order_rate)):?>
            <form action="<?php echo base_url('item/review/'.$orderID);?>" method="post">
                <input type="hidden" name="order" value="<?php echo $this->uri->segment(3);?>">
                <h3 class="label">Please rate the service</h3> 
                <br>   
                <div class="form-group form-group-radio">
                    <div class="radio">
                        <input id="radio-3" name="rating" type="radio" value="1" <?php echo (isset($reset)) ? "" : set_radio('rating', 'cash'); ?>>
                        <label for="radio-3" class="radio-label"><span class="fa fa-star checked"></span> 1</label>
                    </div>

                    <div class="radio">
                        <input id="radio-4" name="rating" type="radio" value="2" <?php echo (isset($reset)) ? "" : set_radio('rating', '2'); ?>>
                        <label for="radio-4" class="radio-label"><span class="fa fa-star checked"></span> 2</label>
                    </div>

                    <div class="radio">
                        <input id="radio-5" name="rating" type="radio" value="3" <?php echo (isset($reset)) ? "" : set_radio('rating', '3'); ?>>
                        <label for="radio-5" class="radio-label"><span class="fa fa-star checked"></span> 3</label>
                    </div>

                    <div class="radio">
                        <input id="radio-6" name="rating" type="radio" value="4" <?php echo (isset($reset)) ? "" : set_radio('rating', '4'); ?>>
                        <label for="radio-6" class="radio-label"><span class="fa fa-star checked"></span> 4</label>
                    </div>

                    <div class="radio">
                        <input id="radio-7" name="rating" type="radio" value="5" <?php echo (isset($reset)) ? "" : set_radio('rating', '5'); ?>>
                        <label for="radio-7" class="radio-label"><span class="fa fa-star checked"></span> 5</label>
                    </div>

                </div>

                <div class="form-group">
                    <input class="btn" type="submit" value="Submit" style="cursor: pointer;">
                </div>
            </form>
            <?php endif;?>
        </div>
    </section>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <script type="text/javascript">
    baseurl = "<?php echo base_url()?>";
    message = "<?php echo $this->session->flashdata('flashmsg');?>";
    type = "<?php echo $this->session->flashdata('msg');?>";
    if(message.length > 0 && type == 'success'){//alert(1)
        $('#message-box-success').html('<img src='+baseurl+'assets/img/logo.png>'+message);
        $('#message-box-success').slideDown('slow').delay(2000).slideUp('slow');
    }
    </script>
</body>

</html>
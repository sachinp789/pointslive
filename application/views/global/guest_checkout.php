<?php
$seller = $sellerID;
$sellerinfo = admin_info($seller);
$currency = '';

if($sellerinfo->country == "thailand"){
    $currency = "฿";
}
else if($sellerinfo->country =="nigeria"){
    $currency = "₦";
}

$chatdata = $this->Chatbots_model->where('created_by',$seller)->get();

$user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

if(empty($chatdata->offer_states)){
    $statelists = GetStatesByCountry($sellerinfo->country);
}
else{
    $statelists = unserialize($chatdata->offer_states);
    if($sellerinfo->country == "nigeria"){
        $statelists = array_map(function($state) {
            return array(
                'state_name' => $state
            );
        }, $statelists);
        $statelists = json_decode(json_encode($statelists), FALSE);
    }
    else if($sellerinfo->country == "thailand"){
        $statelists = array_map(function($state) {
            return array(
            'province' => $state
        );
        }, $statelists);
        $statelists = json_decode(json_encode($statelists), FALSE);
    }
}

$type = array();
$sum = 0;
$products = productlists_by_userid($user);
foreach ($products as $key => $value) {
    $type[] = $value->shipping_type;

    if($value->sale_price > 0){
      $price = $value->sale_price;
    }else{
      $price = $value->product_price;
    }
    $sum = $sum + ($price * $value->qty);
}

if(!empty($this->session->userdata('customerId'))){
    $last_row = $this->db->select('shipping_address')->where('user_id',$this->session->userdata('customerId'))->order_by('id',"desc")->limit(1)->get('orders')->row();
    $lastaddress = $this->db->select('*')->where('id',$last_row->shipping_address)->get('address')->row();
    $customers = admin_info($this->session->userdata('customerId'));
}

if($sellerinfo->country == "nigeria"){
    $action = base_url('item/checkout/'.$this->uri->segment(3));
}   
else{
    $action = base_url('item/omischeckout/'.$this->uri->segment(3));
}

$offer = 0;     
if($codes && empty($codes->order_id)){
    $offer = $codes->total_discount;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title;?></title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121417530-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121417530-1');
    </script>


     <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2244243415809474');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2244243415809474&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
      fbq('track', 'InitiateCheckout');
    </script>
</head>

<body id="guest_checkout">
    <div class="desktop-view">
        <section id="page-title-section">
            <div class="container">
                <a href="<?php echo base_url()?>item/manage_cart/<?php echo $this->uri->segment(3)?>">
                    <i class="fa fa-chevron-left"></i>
                    <span> BACK</span>
                </a>
                <h1><?php echo $title;?></h1>
                <a href="#">
                    <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
                </a>
            </div>
        </section>
        <section id="checkout-form-section">
            <?php if (!empty($this->session->flashdata('flashmsg'))): ?>
                <div id="message-box-<?php echo $this->session->userdata('msg')?>"><?php echo $this->session->flashdata('flashmsg');?></div>
            <?php endif;?>
            <div class="container">
                <div class="error">
                <?php echo validation_errors(); ?>
                </div>
                <form action="<?php echo $action;?>" method="post" name="checkoutForm" id="checkoutForm">
                    <input type="hidden" name="countryname" id="countryname" value="<?php echo  (isset($sellerinfo->country)) ? $sellerinfo->country : '';?>">
                    <input type="hidden" name="seller" value="<?php echo $seller;?>">
                    <div class="form-group">
                        <h3 class="label">Name</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <input type="text" name="name" value="<?php echo (!isset($reset)) ? "" : set_value('name'); ?>" id="name">
                        <?php elseif(!empty($customers->admin_name)) : 
                        $fname = set_value('name') == false ? $customers->admin_name : set_value('name'); 
                        ?>
                        <input type="text" name="name" value="<?php echo $fname; ?>" id="name">
                        <?php else : 
                        $fname = set_value('name') == false ? $lastaddress->receiver_name : set_value('name');    
                        ?>
                        <input type="text" name="name" value="<?php echo $fname; ?>" id="name">
                        <?php endif;?>
                    </div>

                    <div class="form-group">
                        <h3 class="label">Email</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <input type="email" name="email" value="<?php echo (isset($reset)) ? "" : set_value('email'); ?>" id="email" id="email">
                        <?php elseif(!empty($customers->admin_email)) : 
                        $email = set_value('email') == false ? $customers->admin_email : set_value('email'); 
                        ?>
                        <input type="email" name="email" value="<?php echo $email; ?>" id="email" id="email">
                        <?php else : 
                        $email = set_value('email') == false ? $lastaddress->email : set_value('email');    
                        ?>
                        <input type="email" name="email" value="<?php echo $email; ?>" id="email">
                        <?php endif;?>
                    </div>

                    <div class="form-group">
                        <h3 class="label">Contact Number</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <input type="tel" name="contact_number" value="<?php echo (isset($reset)) ? "" : set_value('contact_number'); ?>" id="contact">
                        <?php elseif(!empty($customers->admin_mobile)) : 
                        $phone = set_value('contact_number') == false ? $customers->admin_mobile : set_value('contact_number');    
                        ?>
                        <input type="tel" name="contact_number" value="<?php echo $phone; ?>" id="contact">
                        <?php else :  
                        $phone = set_value('contact_number') == false ? $lastaddress->phone : set_value('contact_number');?>
                        <input type="tel" name="contact_number" value="<?php echo $phone; ?>" id="contact">    
                        <?php endif;?>
                    </div>

                    <h3 class="label" style="font-size: 16px;">Delivery Type</h3>    
                    <div class="form-group form-group-radio" id="delitype">
                        <?php if(in_array('both', $type)):?>
                            <div class="radio">
                                <input id="radio-1" name="delivery_type" type="radio" checked value="pickup" <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype">
                                <label for="radio-1" class="radio-label">Buy At Store / Digital Service</label>
                            </div>

                            <div class="radio">
                                <input id="radio-2" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype">
                                <label for="radio-2" class="radio-label">Home Delivery</label>
                            </div>
                        <?php elseif(count($products) > 0 && in_array('delivery', $type) && in_array('offline', $type)): ?>
                              <div class="radio">
                                <input id="radio-1" name="delivery_type" type="radio" value="pickup" checked <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype">
                                <label for="radio-1" class="radio-label">Buy At Store / Digital Service</label>
                            </div>

                            <div class="radio">
                                <input id="radio-2" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype">
                                <label for="radio-2" class="radio-label">Home Delivery</label>
                            </div>
                        <?php elseif(count($products) == 1 && in_array('delivery', $type)): ?>
                             <div class="radio">
                                <input id="radio-2" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype">
                                <label for="radio-2" class="radio-label">Home Delivery</label>
                            </div> 
                        <?php elseif(count($products) == 1 && in_array('offline', $type)): ?>
                            <div class="radio">
                                <input id="radio-1" name="delivery_type" type="radio" value="pickup" checked <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype">
                                <label for="radio-1" class="radio-label">Buy At Store / Digital Service</label>
                            </div>  
                        <?php else :?>
                            <div class="radio">
                                <input id="radio-1" name="delivery_type" type="radio" value="pickup" checked <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype">
                                <label for="radio-1" class="radio-label">Buy At Store / Digital Service</label>
                            </div>

                            <div class="radio">
                                <input id="radio-2" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype">
                                <label for="radio-2" class="radio-label">Home Delivery</label>
                            </div>
                        <?php endif;?>
                    </div>

                    <?php if($chatdata && !empty($chatdata->delivery_services) && $chatdata->delivery_services == 1): ?>
                    <h3 class="label">Delivery Options</h3>
                    <div class="form-group form-group-radio" id="options">
                    <?php 
                    $i = 10;
                    foreach (unserialize($chatdata->service_provides) as $value):
                    ?>
                        <div class="radio">
                            <input id="radio-<?php echo $i?>" name="delivery_option" type="radio" value="<?php echo $value;?>" <?php echo (isset($reset)) ? "" : set_radio('delivery_option', $value); ?> class="deliveryoption">
                            <label for="radio-<?php echo $i?>" class="radio-label"><?php echo ucfirst($value);?></label>
                        </div>
                    <?php $i++; endforeach; ?></div><?php endif;?>

                    <?php
                    if(set_value('delivery_type') == "pickup"){
                      $css = "display:none";  
                    }
                    else if(set_value('delivery_type') == "delivery"){
                      $css = "display:block";   
                    }
                    else{
                      $css = "display:none";   
                    }
                    ?>

                    <?php if($caddress) :?>
                    <div class="form-group deliverymethod" style="<?php echo $css;?>" id="deliverymethod-1">
                        <h3 class="label">Address</h3>
                        <?php foreach($caddress as $addres):?>
                        <div class="radio">
                            <input id="radio-useraddress-<?php echo $addres->address_id?>" name="custaddress" type="radio" value="<?php echo $addres->address_id?>" class="custaddressses" <?php echo ($addres->make_default == '1') ? "checked" : "";?>>
                            <label for="radio-useraddress-<?php echo $addres->address_id?>" class="radio-label"><?php echo $addres->address.', '.$addres->state.','.$addres->district.', '.$addres->country;?></label>
                        </div>
                        <?php endforeach;?>
                    </div>
                    <?php else:?>
                    <div class="form-group form-group-select deliverymethod" style="<?php echo $css;?>">
                        <h3 class="label">Shipping State</h3>
                        <select name="choose_state" id="selectstate" >
                            <option value="">Choose From List of States</option>
                            <?php
                            $states = "";
                            $city = "";
                            foreach ($statelists as $value) {  
                            if(isset($value->province)){
                                $city = $value->province;
                            }
                            else if(isset($value->state_name)){
                                $city = $value->state_name;
                            }  
                            $states.="<option value=".str_replace(" ", "_", $city).">".$city."</option>";
                            }
                            echo $states;
                            ?>
                        </select>
                    </div>

                    <div class="form-group form-group-select deliverymethod" style="<?php echo $css;?>">
                        <h3 class="label">Local Government</h3>
                        <select name="local_government" id="selectarea"><option value="">Choose From List of Local Governments</option></select>
                    </div>

                    <div class="form-group deliverymethod" style="<?php echo $css;?>">
                        <h3 class="label">Shipping Address</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <textarea name="shiping_address" placeholder="Enter Shipping Address" cols="30" rows="10" id="shiping_address"><?php echo (!isset($reset)) ? "" : set_value('shiping_address'); ?></textarea>
                        <?php else:
                        $address = set_value('shiping_address') == false ? $lastaddress->shipping_address : set_value('shiping_address');     
                        ?>
                        <textarea name="shiping_address" placeholder="Enter Shipping Address" cols="30" rows="10" id="shiping_address"><?php echo $address; ?></textarea>    
                        <?php endif;?>
                    </div>
                    <?php endif;?>

                    <h3 class="label" style="font-size: 16px;">Payment Method</h3>
                    <div class="form-group form-group-radio" id="paymethod">
                        <?php if($sellerinfo->admin_id == '19'): ?>
                        <div class="radio">
                            <input id="radio-3" name="payment_method" type="radio" value="cash" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'cash'); ?>>
                            <label for="radio-3" class="radio-label">Cash</label>
                        </div>
                        <div class="radio">
                            <input id="radio-445" name="payment_method" type="radio" value="account" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'account'); ?>>
                            <label for="radio-445" class="radio-label">Bank Transfer</label>
                        </div>
                        <div class="radio">
                            <input id="radio-421" name="payment_method" type="radio" value="ussd" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'ussd'); ?>>
                            <label for="radio-421" class="radio-label">USSD</label>
                        </div>
                        <div class="radio">
                            <input id="radio-400" name="payment_method" type="radio" value="card" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'card'); ?>>
                            <label for="radio-400" class="radio-label">Bank Card</label>
                        </div>
                        <?php elseif($sellerinfo->country == 'thailand'): ?>
                        <div class="radio">
                            <input id="radio-3" name="payment_method" type="radio" value="prepaid" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'prepaid'); ?>>
                            <label for="radio-3" class="radio-label">Card / Transfer</label>
                        </div>
                        <?php elseif($sellerinfo->country == "nigeria") :?>
                        <div class="radio">
                            <input id="radio-445" name="payment_method" type="radio" value="account" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'account'); ?>>
                            <label for="radio-445" class="radio-label">Bank Transfer</label>
                        </div>
                        <div class="radio">
                            <input id="radio-421" name="payment_method" type="radio" value="ussd" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'ussd'); ?>>
                            <label for="radio-421" class="radio-label">USSD</label>
                        </div>
                        <div class="radio">
                            <input id="radio-400" name="payment_method" type="radio" value="card" <?php echo (!isset($reset)) ? "" : set_radio('payment_method', 'card'); ?>>
                            <label for="radio-400" class="radio-label">Bank Card</label>
                        </div>

                        <?php endif;?>
                    </div>

                    <div class="form-group">
                        <?php if($sellerinfo->country == "nigeria"):?>
                        <input class="btn" type="submit" value="Make Payment" style="cursor: pointer;">
                        <?php else: ?>
                        <button class="btn" id="btncheckout-button-1">Make Payment</button>
                        <?php endif;?> 
                    </div>
                    <input type="hidden" name="omiseToken"><input type="hidden" name="omiseSource">
                </form>
            </div>
        </section>
    </div>

    <div class="mobile-view">
        <div class="details-overlay"></div>
        <div id="login-popup" class="modal-window">
            <div class="modal-content" >
                <img class="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="">
                <div class="social-login-container">
                    <h2>Sign in with</h2>

                    <ul class="login-list">
                        <li class="modal-list-item"><a href="<?php echo base_url('customer/signIn/facebook')?>"><i class="fab fa-facebook"></i></a></li>
                        <li class="modal-list-item"><a href="#" id="sign-in-email"><i class="fas fa-envelope"></i></a></li>
                        <li class="modal-list-item"><a href="<?php echo base_url('customer/signIn/twitter')?>"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="action-container">
                    <span id="guest_checkout_link" class="btn">Continue as Guest</span>
                </div>
            </div>
        </div>
        <div id="email-login-popup" class="modal-window">
            <span class="error error-user" style="display: none;"></span>
            <div class="close-container">
                <a href="#" id="close-modal">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <div class="modal-content">
                <form method="post" id="login-form-customer">
                    <input type="email" id="emailaddress" placeholder="Enter Email" class="form-control" >
                    <input type="password" id="userpassword" placeholder="Enter Password" class="form-control" >
                    <button type="submit" name="login-submit" id="login-submit" class="btn">
                        </span> Signup / In 
                    </button>       
                </form>
            </div>
        </div>
        <!-- <div class="navigator-head">
            <div class="container">
                <div class="left-head">
                    <a href="<?php echo base_url()?>item/manage_cart/<?php echo $this->uri->segment(3)?>"><i class="fa fa-chevron-left"></i>Back</a>
                </div>
            </div>
        </div> -->

        <div class="checkout-details-container">
          <?php if (!empty($this->session->flashdata('flashmsg'))): ?>
            <div id="message-box-<?php echo $this->session->userdata('msg')?>"><?php echo $this->session->flashdata('flashmsg');?></div>
        <?php endif;?>
        <form action="<?php echo $action;?>" method="post" name="usercheckoutForm" id="usercheckoutForm">
            <div class="container">
                <div class="error" style="margin: 0;">
                <?php echo validation_errors(); ?>
                </div>
                <div class="form-step-first">
                    <div class="user-form-title"><span class="step-form">1</span>  <span class="title-text">Contact Details</span> </div>
                    <input type="hidden" name="countryname" id="countryname" value="<?php echo (isset($sellerinfo->country)) ? $sellerinfo->country : '';?>">
                    <input type="hidden" name="seller" value="<?php echo $seller;?>">
                    <div class="form-group">
                        <h3 class="label">Name</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <input type="text" name="name" value="<?php echo (!isset($reset)) ? "" : set_value('name'); ?>" id="name1" placeholder="Name">
                        <?php elseif(!empty($customers->admin_name)) : 
                        $fname = set_value('name') == false ? $customers->admin_name : set_value('name');    
                        ?>
                        <input type="text" name="name" placeholder="Name" value="<?php echo $fname; ?>" id="name1">
                        <?php else : 
                        $fname = set_value('name') == false ? $lastaddress->receiver_name : set_value('name');    
                        ?>
                        <input type="text" name="name" placeholder="Name" value="<?php echo $fname; ?>" id="name1">
                        <?php endif;?>
                    </div>

                    <div class="form-group">
                        <h3 class="label">Email</h3>

                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <input type="email" name="email" value="<?php echo (isset($reset)) ? "" : set_value('email'); ?>" id="email1" placeholder="Email">
                        <?php elseif(!empty($customers->admin_email)) : 
                        $email = set_value('email') == false ? $customers->admin_email : set_value('email');    
                        ?>
                        <input type="email" name="email" placeholder="Email" value="<?php echo $email; ?>" id="email1">
                        <?php else : 
                        $email = set_value('email') == false ? $lastaddress->email : set_value('email');    
                        ?>
                        <input type="email" name="email" placeholder="Email" value="<?php echo $email; ?>" id="email1">
                        <?php endif;?>
                    </div>

                    <div class="form-group">
                        <h3 class="label">Contact Number</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <input type="tel" name="contact_number" value="<?php echo (isset($reset)) ? "" : set_value('contact_number'); ?>" id="contact1" placeholder="Contact Number">
                        <?php elseif(!empty($customers->admin_mobile)) : 
                        $phone = set_value('contact_number') == false ? $customers->admin_mobile : set_value('contact_number');    
                        ?>
                        <input type="tel" name="contact_number" placeholder="Contact Number" value="<?php echo $phone; ?>" id="contact1">
                        <?php else :  
                        $phone = set_value('contact_number') == false ? $lastaddress->phone : set_value('contact_number');?>
                        <input type="tel" name="contact_number" placeholder="Contact Number" value="<?php echo $phone; ?>" id="contact1">    
                        <?php endif;?>

                    </div>
                    <div class="form-group">
                        <div class="checkout-action-bar btn-back-next">
                            <div class="button-prev-next">
                                <a href="<?php echo base_url()?>item/manage_cart/<?php echo $this->uri->segment(3)?>" class="button-prev btn">Back</a>
                                <button class="button-next btn btnnext" type="button">Next</button>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="form-step-second"> 
                    <div class="user-form-title"><span class="step-form">2</span>  <span class="title-text">Delivery Details</span> </div>
                    <?php
                    $css1 = '';
                    $css2 = '';
                   
                    if(in_array('both', $type)){
                        $css1 = "display:block";
                        $css2 = "display:none";
                    }
                    elseif (count($products) > 0 && in_array('delivery', $type) && in_array('offline', $type)) {
                        $css1 = "display:block";
                        $css2 = "display:none";
                    }
                    elseif (count($products) > 0 && in_array('delivery', $type)) {
                        $css1 = "display:none";
                        $css2 = "display:block";
                    }
                    elseif (count($products) > 0 && in_array('offline', $type)) {
                        $css1 = "display:none";
                        $css2 = "display:none";
                    }
                    elseif(count($products) == 1 && in_array('delivery', $type)){
                        $css1 = "display:none";
                        $css2 = "display:block";
                    }
                    elseif(count($products) == 1 && in_array('offline', $type)){
                        $css1 = "display:none";
                        $css2 = "display:none";
                    }
                    else{
                        $css1 = "display:none";
                        $css2 = "display:none";
                    }
                    ?>  
                    <h3 class="label" style="<?php echo $css1;?>"><i class="far fa-paper-plane" style="margin-right: 10px; font-size: 17px;"></i>Delivery Type</h3>   
                    <div class="form-group form-group-radio" id="delitype1" style="<?php echo $css1;?>">
                        <?php if(in_array('both', $type)):?>
                            <div class="radio">
                                <input id="radio-21" name="delivery_type" type="radio" checked value="pickup" <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype1">
                                <label for="radio-21" class="radio-label">Buy At Store / Digital Service</label>
                            </div>

                            <div class="radio">
                                <input id="radio-22" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype1">
                                <label for="radio-22" class="radio-label">Home Delivery</label>
                            </div>
                        <?php elseif(count($products) > 0 && in_array('delivery', $type) && in_array('offline', $type)): ?>
                            <div class="radio">
                                <input id="radio-23" name="delivery_type" type="radio" value="pickup" <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype1">
                                <label for="radio-23" class="radio-label">Buy At Store / Digital Service</label>
                            </div>

                            <div class="radio">
                                <input id="radio-24" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype1">
                                <label for="radio-24" class="radio-label">Home Delivery</label>
                            </div>
                        <?php elseif(count($products) > 0 && in_array('delivery', $type)): ?>
                            <div class="radio">
                                <input id="radio-24" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype1" checked>
                                <label for="radio-24" class="radio-label">Home Delivery</label>
                            </div>
                        <?php elseif(count($products) > 0 && in_array('offline', $type)): ?>
                           <div class="radio">
                                <input id="radio-23" name="delivery_type" type="radio" value="pickup" checked <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype1">
                                <label for="radio-23" class="radio-label">Buy At Store / Digital Service</label>
                            </div>
                        <?php elseif(count($products) == 1 && in_array('delivery', $type)): ?>
                            <div class="radio">
                                <input id="radio-25" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype1" checked>
                                <label for="radio-25" class="radio-label">Home Delivery</label>
                            </div> 
                        <?php elseif(count($products) == 1 && in_array('offline', $type)): ?>
                            <div class="radio">
                                <input id="radio-26" name="delivery_type" type="radio" value="pickup" checked <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype1" checked>
                                <label for="radio-26" class="radio-label">Buy At Store / Digital Service</label>
                            </div>  
                        <?php else :?>
                            <div class="radio">
                                <input id="radio-mobile-27" name="delivery_type" type="radio" value="pickup" checked <?php echo (!isset($reset)) ? "" :  set_radio('delivery_type', 'pickup'); ?> class="deliverytype1">
                                <label for="radio-mobile-27" class="radio-label">Buy At Store / Digital Service</label>
                            </div>

                            <div class="radio">
                                <input id="radio-mobile-28" name="delivery_type" type="radio" value="delivery" <?php echo (!isset($reset)) ? "" : set_radio('delivery_type', 'delivery'); ?> class="deliverytype1">
                                <label for="radio-mobile-28" class="radio-label">Home Delivery</label>
                            </div>
                        <?php endif;?>
                    </div>

                    <?php if($chatdata && !empty($chatdata->delivery_services) && $chatdata->delivery_services == 1): ?>
                    <h3 class="label"><i class="fas fa-location-arrow" style="margin-right: 10px; font-size: 17px;"></i>Delivery Option</h3>
                    <div class="form-group form-group-radio" id="options1">
                    <?php 
                    $i = 10;
                    foreach (unserialize($chatdata->service_provides) as $value):
                    ?>
                        <div class="radio">
                            <input id="radio-mobile-<?php echo $i?>" name="delivery_option" type="radio" value="<?php echo $value;?>" <?php echo (isset($reset)) ? "" : set_radio('delivery_option', $value); ?> class="deliveryoption">
                            <label for="radio-mobile-<?php echo $i?>" class="radio-label"><?php echo ucfirst($value);?></label>
                        </div>
                    <?php $i++; endforeach; ?></div><?php endif;?>

                    <?php
                    if(set_value('delivery_type') == "pickup"){
                    $css = "display:none";  
                    }
                    else if(set_value('delivery_type') == "delivery"){
                    $css = "display:block";   
                    }
                    else{
                    $css = "display:none";   
                    }
                    ?>

                    <?php if($caddress):?>
                    <div class="form-group deliverymethod" style="<?php echo $css2;?>" id="deliverymethod-2">
                        <h3 class="label">Address</h3>
                        <?php $i=3; foreach($caddress as $addres): ?>
                        <div class="radio">
                            <input id="radio-useraddress-<?php echo $i?>" name="custaddress" type="radio" value="<?php echo $addres->address_id?>" class="custaddressses1" <?php echo ($addres->make_default == '1') ? "checked" : "";?>>
                            <label for="radio-useraddress-<?php echo $i?>" class="radio-label"><?php echo $addres->address.', '.$addres->state.','.$addres->district.', '.$addres->country;?></label>
                        </div>
                        <?php $i++; endforeach;?>
                    </div>
                    <?php else:?>
        
                    <div class="form-group form-group-select deliverymethod" style="<?php echo $css2;?>">
                        <h3 class="label">Shipping State</h3>
                        <select name="choose_state" id="selectstate1" >
                            <option value="">Choose From List of States</option>
                            <?php
                            $states = "";
                            $city = "";
                            foreach ($statelists as $value) {  
                            if(isset($value->province)){
                                $city = $value->province;
                            }
                            else if(isset($value->state_name)){
                                $city = $value->state_name;
                            }  
                            $states.="<option value=".str_replace(" ", "_", $city).">".$city."</option>";
                            }
                            echo $states;
                            ?>
                        </select>
                    </div>

                    <div class="form-group form-group-select deliverymethod" style="<?php echo $css2;?>">
                        <h3 class="label">Local Government</h3>
                        <select name="local_government" id="selectarea1"><option value="">Choose From List of Local Governments</option></select>
                    </div>

                    <div class="form-group deliverymethod" style="<?php echo $css2;?>">
                        <h3 class="label">Shipping Address</h3>
                        <?php if(empty($this->session->userdata('customerId'))): ?>
                        <textarea name="shiping_address" placeholder="Enter Shipping Address" cols="30" rows="10" id="shiping_address1"><?php echo (!isset($reset)) ? "" : set_value('shiping_address1'); ?></textarea>
                        <?php else:
                        $address = set_value('shiping_address1') == false ? $lastaddress->shipping_address : set_value('shiping_address1');     
                        ?>
                        <textarea name="shiping_address" placeholder="Enter Shipping Address" cols="30" rows="10" id="shiping_address1"><?php echo $address; ?></textarea>    
                        <?php endif;?>
                    </div>
                    <?php endif;?>
                    <div class="checkout-action-bar btn-back-next">
                    <div class="button-prev-next">
                        <a class="button-prev btn btnstepprev2" href="#">Back</a>
                        <button class="button-next btn btnstep2" type="button">Next</button>
                    </div>
                    </div>
                </div>  
                <div class="form-step-third"> 
                    <div class="user-form-title"><span class="step-form">3</span> <span class="title-text">Payment Option</span> </div> 
                    <!-- <h3 class="label"><i class="far fa-credit-card" style="margin-right: 10px; font-size: 17px;"></i>Payment Option</h3> -->
                    <div class="form-group" id="paymethod1">
                        <?php if($sellerinfo->admin_id == '15'): ?>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-3" name="payment_method" type="radio" value="cash" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'cash'); ?> class="pay-method">
                            <label for="radio-mobile-3" class="radio-label">Cash</label>
                        </div>
                       
                        <div class="radio radio-btn">
                            <input id="radio-mobile-114" name="payment_method" type="radio" value="account" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'account'); ?> class="pay-method">
                            <label for="radio-mobile-114" class="radio-label"><i class="fas fa-exchange-alt"></i> Bank Transfer</label>
                        </div>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-074" name="payment_method" type="radio" value="ussd" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'ussd'); ?> class="pay-method">
                            <label for="radio-mobile-074" class="radio-label"><b>*123#</b> USSD</label>
                        </div>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-004" name="payment_method" type="radio" value="card" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'card'); ?> class="pay-method">
                            <label for="radio-mobile-004" class="radio-label"><i class="fa fa-credit-card" aria-hidden="true"></i> Bank Card</label>
                        </div>
                        <?php elseif($sellerinfo->admin_id == '229'): ?>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-3" name="payment_method" type="radio" value="cash" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'cash'); ?> class="pay-method">
                            <label for="radio-mobile-3" class="radio-label">Cash</label>
                        </div>
                        <?php elseif($sellerinfo->country == 'thailand'): ?>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-3" name="payment_method" type="radio" value="prepaid" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'prepaid'); ?> class="pay-method">
                            <label for="radio-mobile-3" class="radio-label">Card / Transfer</label>
                       
                        </div>
                        <?php elseif($sellerinfo->country == "nigeria") :?>
                      
                        <div class="radio radio-btn">
                            <input id="radio-mobile-114" name="payment_method" type="radio" value="account" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'account'); ?> class="pay-method">
                            <label for="radio-mobile-114" class="radio-label"><span class="label-title">Bank Transfer</span><span class="label-img"><img src="<?php echo base_url()?>assets/img/bank_icon.png" alt=""></span></label> <!-- <i class="fas fa-exchange-alt"></i> -->
                        </div>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-074" name="payment_method" type="radio" value="ussd" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'ussd'); ?> class="pay-method">
                            <label for="radio-mobile-074" class="radio-label"> <span class="label-title">USSD</span><span class="label-img"></span> <span class="label-img">*123#</span></label>
                        </div>
                        <div class="radio radio-btn">
                            <input id="radio-mobile-004" name="payment_method" type="radio" value="card" <?php echo (!isset($reset)) ? "" : set_radio('payment_method1', 'card'); ?> class="pay-method">
                            <label for="radio-mobile-004" class="radio-label"><span class="label-title">Bank Card</span><span class="label-img"><img src="<?php echo base_url()?>assets/img/card_icon.png" alt=""></span></label> <!-- <i class="fa fa-credit-card" aria-hidden="true"></i> -->
                        </div>
                       
                        <?php endif;?>
                    </div>
                </div>
            </div>

            <div class="form-step-third">

                <div class="coupon-apply-section">
                    <div class="container">
                            <div class="form-group">
                                <h3 class="label">Apply Voucher</h3>
                                <div class="input-wrap">
                                    <input type="text" class="form-control" id="coupon" data-user="<?php echo $user;?>" data-amount="<?php echo $sum;?>">
                                    <button type="button" id="btnapply">Apply</button>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="coupon-apply-section productsdetails">
                <div class="container">
                    <div class="form-group">
                        <h3 class="label">Order Summary</h3>
                    </div>
                    <?php foreach ($pdata as $key => $value) {
                        if($value->sale_price > 0){
                            $pricecart = $value->sale_price;
                        }
                        else{
                            $pricecart = $value->product_price;   
                        }
                    ?>
                    <div class="checkout-single">
                        <div class="checkout-image">
                            <img src="<?php echo base_url()?>uploads/products/<?php echo $value->product_image?>" alt="">
                        </div>
                        <div class="checkout-details">
                            <p><?php echo $value->product_name;?></p>
                            <div class="checkout-product-quantity">
                                <span>Qty : <b><?php echo $value->qty;?></b></span>
                            </div>
                            <div class="checkout-product-price">
                                <span>Price : <b><?php echo $currency."".$pricecart?></b></span>
                            </div>
                        </div>  
                    </div>
                    <?php }?>
                    <div style="margin: 20px 0; border-bottom:1px solid #ddd;"></div>
                </div>
            </div>

                <div class="total-section">
                    <div class="container">
                        <div class="main-total">
                            <label for="">Total: </label>
                            <p><?php echo $currency." ".$sum;?></p>
                        </div>

                        <div class="voucher-amount">
                            <label for="">Voucher: </label>
                            <p>- <?php echo $currency;?> <span class='discountamount'><?php echo ($codes && empty($codes->order_id)) ? $codes->total_discount : 0;?></span> <?php echo ($codes && empty($codes->order_id)) ? "<a id='removecode' href='#' data-id='".$codes->id."'>X</a>" : '';?></p>
                        </div>

                        <div class="grand-total">
                            <label for="">Grand Total: </label>
                            <p><?php echo $currency;?> <span class='gtotal'><?php echo ($codes && empty($codes->order_id)) ? ($sum - $codes->total_discount) : $sum;?></span></p>
                        </div>
                    </div>
                </div>
                <div class="checkout-action-bar btn-back-next">
                    <div class="button-prev-next">
                    <a class="button-prev btn btnstepprev3" href="#">Back</a>
                    <?php if($sellerinfo->country == "nigeria"):?>
                        <button class="button-next btn" id="makepayment">Pay Now</button>
                    <?php else: ?>
                        <button class="button-next btn" id="btncheckout-button-11">Pay Now</button>
                    <?php endif;?> 
                    </div>  
                </div>
            </div>
            <input type="hidden" name="omiseToken1">
            <input type="hidden" name="omiseSource">
        </form>
        </div>
    </div>
    
    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

     <script type="text/javascript">
        //delivery_type1 = $("input:radio.deliverytype1:checked").val();
        //console.log(delivery_type1);
        $(document).on('click','.btnnext',function(event){

            name = $("#name1").val();
            email =$("#email1").val();
            contact = $("#contact1").val();
            var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            flag = 0;

             if(name.length == 0){
                $('span.text-danger.err11').remove();
                $("#name1").after("<span class='text-danger err11'>* Please enter name.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err11').remove();
               }

               if(email.length == 0){
                $('span.text-danger.err22').remove();
                $("#email1").after("<span class='text-danger err22'>* Please enter email.</span>"); 
                flag = 1; 
               }
               else if(!validateEmail(email)){
                $('span.text-danger.err22').remove();
                $('#email1').after('<span class="text-danger err22">Please enter valid email.</span>');
                flag=1;
               }
               else{
                $('span.text-danger.err22').remove();
               }

               if(contact.length == 0){
                $('span.text-danger.err33').remove();
                $('#contact1').after("<span class='text-danger err33'>* Please enter contact number.</span>"); 
                flag = 1; 
               }
                /*else if(!numericReg.test(contact)) {
                $('span.text-danger.err33').remove();
                $('#contact1').after("<span class='text-danger err33'>* Numeric characters only.</span>");
                flag = 1; 
                }*/
               else{
                $('span.text-danger.err33').remove();
               }

               if(flag){
                  event.preventDefault();
               }
               else{
                  $(".form-step-first").css("display","none");    
                  $(".form-step-second").css("display","block");  
               }
        });
        $(document).on('click','.btnstep2',function(event){
           delivery_type = $("input:radio.deliverytype1:checked").val();
           delivery_opt = $("input:radio.deliveryoption:checked").val();
           options = $("#options1").length;
           optionsaddress = $("#deliverymethod-2").length;
           state = $("#selectstate1").val();
           caddresses = $("input:radio.custaddressses1:checked").val();
           local_government = $("#selectarea1").val() ;
           address = $("#shiping_address1").val()
           flag = 0;


           if(delivery_type === undefined){
                $('span.text-danger.err44').remove();
                $("#delitype1").after("<span class='text-danger err44'> * Please select delivery type.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err44').remove();
               }

               if(options == 1){ 
                   if(delivery_opt === undefined){
                    $('span.text-danger.err55').remove();
                    $("#options1").after("<span class='text-danger err55'> * Please select delivery option.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err55').remove();
                   }
               }
               if(optionsaddress == 0){
                   if(delivery_type == 'delivery' && state.length == 0){
                    $('span.text-danger.err66').remove();
                    $("#selectstate1").after("<span class='text-danger err66'> * Please select shipping state.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err66').remove();
                   }

                   if(delivery_type == 'delivery' && local_government.length == 0){
                    $('span.text-danger.err77').remove();
                    $("#selectarea1").after("<span class='text-danger err77'> * Please select local government.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err77').remove();
                   }

                   if(delivery_type == 'delivery' && address.length == 0){
                    $('span.text-danger.err88').remove();
                    $("#shiping_address1").after("<span class='text-danger err88'> * Please enter shipping address.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err88').remove();
                   }
               }
               else{
                    if(delivery_type == 'delivery' && optionsaddress == 1 && caddresses == undefined){
                        $('span.text-danger.err100').remove();
                        $("#deliverymethod-2").after("<span class='text-danger err100'> * Please choose address.</span>"); 
                        flag = 1;
                   }
                   else{
                        $('span.text-danger.err100').remove();
                   }
               }

               if(flag){
                  event.preventDefault();
               }
               else{
                  $(".form-step-second").css("display","none");    
                  $(".form-step-third").css("display","block");  
               }
        });
        $(document).on('click','.btnstepprev2',function(event){
            event.preventDefault();
            $(".form-step-first").css("display","block");    
            $(".form-step-second").css("display","none"); 
        });
         $(document).on('click','.btnstepprev3',function(event){
            event.preventDefault();
            $(".form-step-second").css("display","block");    
            $(".form-step-third").css("display","none"); 
        });
    </script>

    <script type="text/javascript">
        country = "<?php echo $sellerinfo->country?>";
        //console.log(country)
        if(country !="" && country == 'thailand'){
            // Mobile check
            $("#usercheckoutForm").on('submit', function (event) {
               event.preventDefault();
               optionsavail = "<?php echo $chatdata->delivery_services?>";
               //Omise.setPublicKey("pkey_test_5cmlmbpqt7wap70e7u7");
                
               name = $("#name1").val();
               email =$("#email1").val();
               const buttonTHB = document.querySelector('#btncheckout-button-11');
               const form = document.querySelector('#usercheckoutForm');
               contact = $("#contact1").val();
               delivery_type = $("input:radio.deliverytype1:checked").val();
               delivery_opt = $("input:radio.deliveryoption:checked").val();
               options = $("#options1").length;
               optionsaddress = $("#deliverymethod-2").length;
               state = $("#selectstate1").val();
               caddresses = $("input:radio.custaddressses1:checked").val();
               local_government = $("#selectarea1").val() ;
               address = $("#shiping_address1").val()
               method = $("input:radio.pay-method:checked").val();
               flag = 0;

               if(name.length == 0){
                $('span.text-danger.err11').remove();
                $("#name1").after("<span class='text-danger err11'>* Please enter name.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err11').remove();
               }

               if(email.length == 0){
                $('span.text-danger.err22').remove();
                $("#email1").after("<span class='text-danger err22'>* Please enter email.</span>"); 
                flag = 1; 
               }
               else if(!validateEmail(email)){
                $('span.text-danger.err22').remove();
                $('#email1').after('<span class="text-danger err22">Please enter valid email.</span>');
                flag=1;
               }
               else{
                $('span.text-danger.err22').remove();
               }

               if(contact.length == 0){
                $('span.text-danger.err33').remove();
                $('#contact1').after("<span class='text-danger err33'>* Please enter contact number.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err33').remove();
               }

               if(delivery_type === undefined){
                $('span.text-danger.err44').remove();
                $("#delitype1").after("<span class='text-danger err44'> * Please select delivery type.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err44').remove();
               }

               if(options == 1){ 
                   if(delivery_opt === undefined){
                    $('span.text-danger.err55').remove();
                    $("#options1").after("<span class='text-danger err55'> * Please select delivery option.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err55').remove();
                   }
               }
               if(optionsaddress == 0){
                   if(delivery_type == 'delivery' && state.length == 0){
                    $('span.text-danger.err66').remove();
                    $("#selectstate1").after("<span class='text-danger err66'> * Please select shipping state.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err66').remove();
                   }

                   if(delivery_type == 'delivery' && local_government.length == 0){
                    $('span.text-danger.err77').remove();
                    $("#selectarea1").after("<span class='text-danger err77'> * Please select local government.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err77').remove();
                   }

                   if(delivery_type == 'delivery' && address.length == 0){
                    $('span.text-danger.err88').remove();
                    $("#shiping_address1").after("<span class='text-danger err88'> * Please enter shipping address.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err88').remove();
                   }
               }
               else{
                    if(delivery_type == 'delivery' && optionsaddress == 1 && caddresses == undefined){
                        $('span.text-danger.err100').remove();
                        $("#deliverymethod-2").after("<span class='text-danger err100'> * Please choose address.</span>"); 
                        flag = 1;
                   }
                   else{
                        $('span.text-danger.err100').remove();
                   }
               }


               if(method === undefined){
                $('span.text-danger.err99').remove();
                $("#paymethod1").after("<span class='text-danger err99'> * Please choose payment method.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err99').remove();
               }

               if(flag){
                  event.preventDefault();
                  //return false;
               }
               else{
                const orderamount = "<?php echo ($sum - $offer);?>";
                const omiseToken = document.querySelector('input[name="omiseToken1"]');
                //alert(orderamount)
                OmiseCard.configure({
                  publicKey: 'pkey_test_5cmltmh9uqb9rr4r0u7',
                  amount: <?php echo ($sum);?>,
                  submitLabel: 'Pay',
                  submitFormTarget: '#usercheckoutForm',
                });

                const formClosedhandler = () => {
                    location.reload();
                }
                const createTokenSuccessHandler = (token) => {
                  //console.log('[Omise.js Testing page]: Payment success ! TOKEN' + token)
                  omiseToken.value = token
                  form.submit()
                }

                buttonTHB.addEventListener('click', (e) => {
                  e.preventDefault()
                  OmiseCard.open({
                    frameLabel: 'Omise',
                    currency: 'THB',
                    amount: orderamount,
                    onCreateTokenSuccess: createTokenSuccessHandler,
                    onFormClosed: formClosedhandler,
                  })
                })

                setInterval(function(){
                    $("#btncheckout-button-11").trigger('click');
                    $("#btncheckout-button-11").prop("disabled",true);
                }, 1000);
               }
            });

            $("#checkoutForm").on('submit', function (event) {
               event.preventDefault();
               optionsavail = "<?php echo $chatdata->delivery_services?>";
               //Omise.setPublicKey("pkey_test_5cmlmbpqt7wap70e7u7");
                
               name = $("#name").val();
               email =$("#email").val();
               const buttonTHB = document.querySelector('#btncheckout-button-1');
               const form = document.querySelector('#checkoutForm');
               contact = $("#contact").val();
               delivery_type = $("input:radio.deliverytype:checked").val();
               delivery_opt = $("input[name=delivery_option]:checked").val();
               optionsaddress = $("#deliverymethod-1").length;
               caddresses = $("input:radio.custaddressses:checked").val();
               //console.log(delivery_opt);
               state = $("#selectstate").val();
               local_government = $("#selectarea").val() ;
               address = $("#shiping_address").val()
               method = $("input[name=payment_method]:checked").val();
               flag = 0;

               if(name.length == 0){
                $('span.text-danger.err1').remove();
                $("#name").after("<span class='text-danger err1'>* Please enter name.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err1').remove();
               }

               if(email.length == 0){
                $('span.text-danger.err2').remove();
                $("#email").after("<span class='text-danger err2'>* Please enter email.</span>"); 
                flag = 1; 
               }
               else if(!validateEmail(email)){
                $('span.text-danger.err2').remove();
                $('#email').after('<span class="text-danger err2">Please enter valid email.</span>');
                flag=1;
               }
               else{
                $('span.text-danger.err2').remove();
               }

               if(contact.length == 0){
                $('span.text-danger.err3').remove();
                $('#contact').after("<span class='text-danger err3'>* Please enter contact number.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err3').remove();
               }

               if(delivery_type === undefined){
                $('span.text-danger.err4').remove();
                $("#delitype").after("<span class='text-danger err4'> * Please select delivery type.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err4').remove();
               }

               if(optionsavail == '1'){
                   if(delivery_opt === undefined){
                    $('span.text-danger.err5').remove();
                    $("#options").after("<span class='text-danger err5'> * Please select delivery option.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err5').remove();
                   }
               }

               /*if(delivery_opt === undefined){
                $('span.text-danger.err5').remove();
                $("#options").after("<span class='text-danger err5'> * Please select delivery option.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err5').remove();
               }*/

                if(optionsaddress == 0){

                   if(delivery_type == 'delivery' && state.length == 0){
                    $('span.text-danger.err6').remove();
                    $("#selectstate").after("<span class='text-danger err6'> * Please select shipping state.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err6').remove();
                   }

                   if(delivery_type == 'delivery' && local_government.length == 0){
                    $('span.text-danger.err7').remove();
                    $("#selectarea").after("<span class='text-danger err7'> * Please select local government.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err7').remove();
                   }

                   if(delivery_type == 'delivery' && address.length == 0){
                    $('span.text-danger.err8').remove();
                    $("#shiping_address").after("<span class='text-danger err8'> * Please enter shipping address.</span>"); 
                    flag = 1; 
                   }
                   else{
                    $('span.text-danger.err8').remove();
                   }
                }   
                else
                {
                   if(delivery_type == 'delivery' && optionsaddress == 1 && caddresses == undefined){
                        $('span.text-danger.err102').remove();
                        $("#deliverymethod-1").after("<span class='text-danger err102'> * Please choose address.</span>"); 
                        flag = 1;
                   }
                   else{
                        $('span.text-danger.err102').remove();
                   } 
                }

               if(method === undefined){
                $('span.text-danger.err9').remove();
                $("#paymethod").after("<span class='text-danger err9'> * Please choose payment method.</span>"); 
                flag = 1; 
               }
               else{
                $('span.text-danger.err9').remove();
               }

               if(flag){
                  event.preventDefault();
                  //return false;
               }
               else{
                const orderamount = "<?php echo ($sum - $offer);?>";
                const omiseToken = document.querySelector('input[name="omiseToken"]');

                OmiseCard.configure({
                  publicKey: 'pkey_test_5cmltmh9uqb9rr4r0u7',
                  amount: <?php echo ($sum);?>,
                  submitLabel: 'Pay',
                  submitFormTarget: '#checkoutForm',
                });

                const formClosedhandler = () => {
                    location.reload();
                }
                const createTokenSuccessHandler = (token) => {
                  //console.log('[Omise.js Testing page]: Payment success ! TOKEN' + token)
                  omiseToken.value = token
                  form.submit()
                }

                buttonTHB.addEventListener('click', (e) => {
                  e.preventDefault()
                  OmiseCard.open({
                    frameLabel: 'Omise',
                    currency: 'THB',
                    amount: orderamount,
                    onCreateTokenSuccess: createTokenSuccessHandler,
                    onFormClosed: formClosedhandler,
                  })
                })

                setInterval(function(){
                    $("#btncheckout-button-1").trigger('click');
                    $("#btncheckout-button-1").prop("disabled",true);
                }, 1000);
               }
            });
        }
        
        function validateEmail(Email) {
            var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            return $.trim(Email).match(pattern) ? true : false;
        }

        $(document).on("input", "#contact", function() {
            this.value = this.value.replace(/\D/g,'');
        });
    </script>

    <script>
        session = "<?php echo $this->session->userdata('customerId')?>";
        if(session == "" || session == null || session.length == 0){
            $('.details-overlay').show();
            $('#login-popup').show();
           /* $(window).on('load', function() {
            });*/

            $(document).ready(function() {
                $('#sign-in-email').on('click', function(event) {
                    event.stopPropagation();
                    $('#login-popup').fadeOut();
                    $('#email-login-popup').fadeIn();
                });

                $('#email-login-popup .close-container a').on('click', function(event) {
                    event.stopPropagation();
                    $('#email-login-popup').fadeOut();
                    $('#login-popup').fadeIn();
                });

                $('#guest_checkout_link').on('click', function(e) {
                    e.stopPropagation();
                    console.log(e);
                    $('.details-overlay').fadeOut();
                    $('#login-popup').fadeOut();
                });
            });
        }

    </script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <!-- <script src="assets/js/main.js"></script> -->
</body>

</html>
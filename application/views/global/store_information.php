<?php
$storetime = "";
if($storedata){ 
    if($storedata->store_open == 'custom'){
        $storetime = unserialize($storedata->business_hours);
    }
    else{
        $storetime = $storedata->store_open;
    }

    if($storedata->delivery_services == '1'){
        $deliveryoptions = unserialize($storedata->service_provides);
    }
    else{
        $deliveryoptions = "";
    }

    if($storedata->return_offer == '1'){ 
        $deliveryoffer = unserialize($storedata->offer_days);
    }
    else{
        $deliveryoffer = "";
    }
    $socialinfo = unserialize($storedata->social_info);
}
if(!empty($storedata->contact_number)){
    $codes = explode("-", $storedata->contact_number);
    if(count($codes) > 1){
        $code = $codes[0];
        $mobile = ltrim($codes[1], '0');
    }
    else{
        $code = '234';
        $mobile = ltrim($codes[0], '0');
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title;?></title>
</head>

<body>
     <div class="sidebar-menu">
        <div class="sidebar-menu--close--container text-right">
            <i id="menu_slide" class="fas fa-chevron-left"></i>
        </div>

        <div class="sidebar-menu--list--container">
            <ul class="sidebar-menu--list">
                <li class="sidebar-menu--list--item">
                <a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>">Shopping Cart</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>">Catalogue</a>
                </li>
                <?php if($this->session->userdata('customerId')):?>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>item/orders"><i class="fa fa-history"></i>Order History</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>customer/signOut"><i class="fas fa-sign-out-alt"></i>Logout</a>
                </li>
                <?php endif;?>
            </ul>
        </div>

    </div>
    <div id="popup-form"></div>
    <section id="page-title-section">
        <div class="container">
            <div class="mobile-menu">
                <a href="#" id="sidebar-open--anchor">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <!-- <ul>
                <li><a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>">Shopping Cart</a></li>
            </ul> -->
             <select id="main-nav--selector" class="main-nav--selector">
                    <option value="#">Go to</option>
                    <option data-fa="fas fa-th" value="<?php echo base_url()?>item/category/<?php echo $sellerID ?>">Catalogue</option>
                    <option data-fa="fas fa-shopping-cart" value="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID?>">Shopping Cart</option>
                    <?php if($this->session->userdata('customerId')):?>
                    <option data-fa="fa fa-history" value="<?php echo base_url()?>item/orders">Order History</option>
                    <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                    <?php endif;?>
                </select>
            
            <h1><?php echo $title; ?></h1>

            <a href="#">
                <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
            </a>
        </div>
    </section>
    <section id="store-information">
        <div class="container opening-closing-time">
            <h2>Opening and Closing Time</h2>
            <?php if($storedata):?>
            <div class="section-content">
                <?php if($storedata->store_open == "custom"):
                $t1 = explode("-", $storetime[0]);
                $tmon = date('h:i A', strtotime($t1[1]));
                $tmon2 = date('h:i A', strtotime($t1[2]));
                $t2 = explode("-", $storetime[1]);
                $ttue =  date('h:i A', strtotime($t2[1]));
                $ttue2 = date('h:i A', strtotime($t2[2]));
                $t3 = explode("-", $storetime[2]);
                $twed = date('h:i A', strtotime($t3[1]));
                $twed2 = date('h:i A', strtotime($t3[2]));
                $t4 = explode("-", $storetime[3]);
                $tthu = date('h:i A', strtotime($t4[1]));
                $tthu2 = date('h:i A', strtotime($t4[2]));
                $t5 = explode("-", $storetime[4]);
                $tfri = date('h:i A', strtotime($t5[1]));
                $tfri2 = date('h:i A', strtotime($t5[2]));
                $t6 = explode("-", $storetime[5]);
                $tsat = date('h:i A', strtotime($t6[1]));
                $tsat2 = date('h:i A', strtotime($t6[2]));
                $t7 = explode("-", $storetime[6]);
                $tsun = date('h:i A', strtotime($t7[2]));
                $tsun2 = date('h:i A', strtotime($t7[2]));
                ?>
                <p><?php echo ($storedata && in_array("Mon-Closed", $storetime)) ? "Monday : Closed" : "Monday : $tmon "." - ".$tmon2; ?>
                </p>
                <p><?php echo ($storedata && in_array("Tue-Closed", $storetime)) ? "Tuesday : Closed" : "Tuesday : $ttue "." - ".$ttue2; ?>
                </p>
                <p><?php echo ($storedata && in_array("Wed-Closed", $storetime)) ? "Wednesday : Closed" : "Wednesday : $twed "." - ".$twed2; ?>
                </p>
                <p><?php echo ($storedata && in_array("Thu-Closed", $storetime)) ? "Thursday : Closed" : "Thursday : $tthu "." - ".$tthu2; ?>
                </p>
                <p><?php echo ($storedata && in_array("Fri-Closed", $storetime)) ? "Friday : Closed" : "Friday : $tfri "." - ".$tfri2; ?>
                </p>
                <p><?php echo ($storedata && in_array("Sat-Closed", $storetime)) ? "Saturday : Closed" : "Saturday : $tsat "." - ".$tsat2; ?>
                </p>
                <p><?php echo ($storedata && in_array("Sun-Closed", $storetime)) ? "Sunday : Closed" : "Sunday : $tsun "." - ".$tsun2; ?>
                </p>
                <?php elseif($storedata->store_open == "alltime"):?>
                <p>Always Open</p>
                <?php endif;?>
            </div>
            <?php endif;?>
        </div>
        <div class="container contact-media">
            <h2>Contact and Media</h2>
            <?php if($storedata):?>
            <div class="section-content">
                <div class="section-content">
                    <p>
                        <a href="#">
                            <i class="fab fa-2x fa-twitter"></i>Twitter</a>
                        <?php
                        if(!is_null($socialinfo['twitter']['website'])):
                        ?>
                        <a href="<?php echo $socialinfo['twitter']['website']?>" target="_blank"><?php echo $socialinfo['twitter']['website']?></a>
                        <?php
                        endif;
                        ?>
                    </p>
                    <p>
                        <a href="#">
                            <i class="fab fa-2x fa-instagram"></i>Instagram</a>
                        <?php
                        if(!empty($socialinfo['insta']['website'])):
                        ?>
                        <a href="<?php echo (strpos($socialinfo['insta']['website'], 'instagram.com') !== false) ? $socialinfo['insta']['website']: "https://www.instagram.com/{$socialinfo['insta']['website']}"; ?>" target="_blank"><?php echo $socialinfo['insta']['website']?></a>
                        <?php
                        endif;
                        ?>
                    </p>
                    <p>
                        <a href="#">
                            <i class="fab fa-2x fa-facebook"></i>Facebook</a>
                        <?php
                        if(!empty($socialinfo['facebook']['website'])):
                        ?>
                        <a href="<?php echo $socialinfo['facebook']['website']?>" target="_blank"><?php echo $socialinfo['facebook']['website']?></a>
                        <?php
                        endif;
                        ?>
                    </p>
                    <p>
                        <a href="#">
                            <i class="fas fa-2x fa-globe"></i>Website</a>
                        <?php
                        $parsed = parse_url($storedata->website_url);
                        if(!empty($storedata->website_url)):
                        ?>
                        <a href="<?php echo (empty($parsed['scheme']) ? 'https://' . ltrim($storedata->website_url, '/') : $storedata->website_url);?>" target="_blank"><?php echo $storedata->website_url?></a>
                        <?php
                        endif;
                        ?>
                    </p>
                    <p>
                        <a href="#">
                            <i class="fas fa-2x fa-phone"></i>Contact Number</a>
                        <?php
                        if(!empty($storedata->contact_number)):
                        ?>
                        <a href="tel:+<?php echo $mobile?>"><?php echo $mobile;?></a>
                        <?php
                        endif;
                        ?>
                    </p>
                </div>
            </div>
            <?php endif;?>
        </div>
        <div class="container delivery-return">
            <h2>Delivery and Return</h2>
            <?php if($storedata):?>
            <div class="section-content">
                <div class="delivery-single">
                    <h3>Do we offer delivery?</h3>
                    <div class="delivery-answer">
                        <?php if($storedata && $storedata->delivery_services == '1'):?>
                        <p class="yes">
                            <i class="far fa-check-circle"></i>
                            Yes
                        </p>
                        <?php endif;?>
                        <?php if($storedata && $storedata->delivery_services == '0'):?>
                        <p class="no">
                            <i class="far fa-times-circle"></i>
                            No
                        </p>
                        <?php endif;?>
                    </div>
                    <?php if($storedata && $storedata->delivery_services == '1'):?>
                    <div class="delivery-content">
                        <h3>Here are our delivery options</h3>
                        <p><?php echo implode(", ",$deliveryoptions);?></p>
                    </div>
                    <?php endif;?>
                </div>


                <div class="delivery-single">
                    <h3>Do we offer Returns?</h3>
                    <div class="delivery-answer">
                        <?php if($storedata && $storedata->return_offer == '1'):?>
                        <p class="yes">
                            <i class="far fa-check-circle"></i>
                            Yes
                        </p>
                        <?php endif;?>
                        <?php if($storedata && $storedata->return_offer == '0'):?>
                        <p class="no">
                            <i class="far fa-times-circle"></i>
                            No
                        </p>
                        <?php endif;?>
                    </div>
                    <?php if($storedata && $storedata->return_offer == '1'):?>
                    <div class="delivery-content">
                        <h3>Returns available up to</h3>
                        <p><?php echo implode(" Days, ",$deliveryoffer);?> Days</p>
                    </div>
                    <?php endif;?>
                </div>
            </div>
            <?php endif;?>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
</body>

</html>
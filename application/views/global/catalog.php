<?php
if(is_digit($this->uri->segment(3))){
    $sellers = $this->uri->segment(3);
}
else{
    $sellers = decrypt($this->uri->segment(3));
}
$sellerID = $this->uri->segment(3);
//$sellers = decrypt($this->uri->segment(3));
$sellerinfo = admin_info($sellers);
//var_dump($categories);
$code = '';
$mobile ='';
if(!empty($storedata->contact_number)){
    $codes = explode("-", $storedata->contact_number);
    if(count($codes) > 1){
        $code = $codes[0];
        $mobile = ltrim($codes[1], '0');
    }
    else{
        $code = '234';
        $mobile = ltrim($codes[0], '0');
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/rater.js"></script>
    <title><?php echo $title;?></title>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121417530-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121417530-1');
    </script>


     <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2244243415809474');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2244243415809474&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
      fbq('track', 'ViewContent');
    </script>
</head>

<body id="catalog">
    <div class="desktop-view">
        <div class="sidebar-menu">
            <div class="sidebar-menu--close--container text-right">
                <i id="menu_slide" class="fas fa-chevron-left"></i>
            </div>

            <div class="sidebar-menu--list--container">
                <ul class="sidebar-menu--list">
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>"><i class="fas fa-shopping-cart"></i>Shopping Cart</a>
                    </li>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/viewstore/<?php echo $sellerID ?>"><i class="fas fa-info-circle"></i>Store Information</a>
                    </li>
                    <?php if($this->session->userdata('customerId')):?>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/orders"><i class="fa fa-history"></i>Order History</a>
                    </li>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>customer/signOut"><i class="fas fa-sign-out-alt"></i>Logout</a>
                    </li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
        <div id="popup-form"></div>
        <section id="page-title-section">
            <div class="container">
                <div class="mobile-menu">
                    <a href="#" id="sidebar-open--anchor">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <select id="main-nav--selector" class="main-nav--selector">
                        <option value="#">Go to</option>
                        <option data-fa="fas fa-shopping-cart" value="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>"><a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>">Shopping Cart</a></option>
                        <option data-fa="fas fa-info-circle" value="<?php echo base_url()?>item/viewstore/<?php echo $sellerID ?>">Store Information</option>
                        <?php if($this->session->userdata('customerId')):?>
                        <option data-fa="fa fa-history" value="<?php echo base_url()?>item/orders">Order History</option>
                        <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                        <?php endif;?>
                    </select>
              
                <h1><?php echo $title;?></h1>
                <a href="#">
                    <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
                </a>
            </div>
        </section>
        <section id="category-list-section">
            <div class="container">
                <?php if(count($categories) == 0 || !$categories):?>
                <p class="nocart">* No categories found.</p>
                <?php endif;?>
                <?php foreach($categories as $value):
                $catID = $value->category_id;
                ?>
                <div class="category-single">
                    <a href="<?php echo base_url()?>item/products/<?php echo is_digit($this->uri->segment(3)) ? $catID.'/'.$sellerID : encrypt($catID).'/'.$sellerID?>">
                        <img src="<?php echo base_url()?>uploads/categories/<?php echo $value->category_image?>" alt="">
                        <div class="category-title">
                            <p><?php echo $value->category_name?></p>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div> 
        </section>
    </div>

     <!-- Mobile View -->
    <div class="mobile-view">
        <div class="detail-overlay"></div>
        <?php
        $storetime = "";
        if($storedata){ 
            if($storedata->store_open == 'custom'){
                $storetime = unserialize($storedata->business_hours);
            }
            else{
                $storetime = $storedata->store_open;
            }

            if($storedata->delivery_services == '1'){
                $deliveryoptions = unserialize($storedata->service_provides);
            }
            else{
                $deliveryoptions = "";
            }

            if($storedata->return_offer == '1'){ 
                $deliveryoffer = unserialize($storedata->offer_days);
            }
            else{
                $deliveryoffer = "";
            }
            $socialinfo = unserialize($storedata->social_info);
        }
        else{
            $socialinfo = array();
        }

        $rating = seller_rating($sellers);
        ?>
        <div class="store-header">
            <div class="container">
                <?php
                //var_dump($sellerinfo->admin_photo);
                if($sellerinfo->login_with == 'facebook' || $sellerinfo->login_with == 'twitter' || $sellerinfo->login_with == 'instagram'){
                    $img = $sellerinfo->login_picture;
                    $newurl = $img;
                }
                else{
                    if(empty($sellerinfo->admin_photo)){
                        //$img = "http://placehold.it/70x70";
                        $newurl = base_url("assets/img/logo.png");
                    }
                    else{
                        $img = base_url("uploads/{$sellerinfo->admin_photo}");
                        $newurl = base_url()."uploads/".$sellerinfo->admin_photo;
                        //$newurl = base_url()."Image_lib.php?src=".$img_link."&w=70&h=70&q=50";
                    }
                }

                ?>

                <div class="store-image"><img src="<?php echo $newurl?>" width="70" height="70"></div>
                <div class="store-details">
                    <div class="store-name">
                        <p><strong><?php echo $sellerinfo->admin_name?></strong></p>
                    </div>
                    <?php if(!empty($rating[0]->average)):?>
                    <div class="store-ratings rate" data-rate-value="<?php echo !is_null($rating[0]->average) ? number_format($rating[0]->average,1) : "0"?>
                    "></div>
                    <?php endif;?>
                </div>
                <!-- <div class="store-share"><a href=""><i class="fa fa-share-alt"></i></a></div> -->
            </div>
        </div>

        <div class="store-section store-collections">
            <div class="container">
                <div class="store-section-title">
                    <h3>MENU</h3>
                </div>
                </div>
                <div class="store-section-content">
                <?php if(count($categories) == 0 || !$categories):?>
                <p class="nocart">* No categories found.</p>
                <?php endif;?>
                    <div id="collection-carousel" class="store-carousel flexslider">
                        <ul class="slides">
                            <?php foreach($categories as $value):
                            $catID = $value->category_id;
                            $img_link = base_url()."uploads/categories/".$value->category_image;
                           // $newurl = base_url()."Image_lib.php?src=".$img_link;
                            ?>
                            <li class="collection-item">
                                <a href="<?php echo base_url()?>item/products/<?php echo is_digit($this->uri->segment(3)) ? $catID.'/'.$sellerID : encrypt($catID).'/'.$sellerID?>">
                                    <img src="<?php echo $img_link?>" alt="">
                                    <div class="title">
                                        <p><?php echo $value->category_name?></p>
                                    </div>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
            </div>
        </div>


        <div class="store-section store-featured-products">
            <div class="container">
                <div class="store-section-title">
                    <h3>You Might Like</h3>
                </div>
                </div>
                <div class="store-section-content">
                    <?php if(count($bestproducts) == 0 || !$bestproducts):?>
                    <p class="nocart">* No products found.</p>
                    <?php endif;?>
                    <div id="featured-products" class="store-carousel flexslider">
                        <ul class="slides">
                            <?php foreach($bestproducts as $value):
                            $pID = $value->product_id;
                            ?>
                            <li class="collection-item">
                                <a href="<?php echo base_url()?>item/view/<?php echo is_digit($this->uri->segment(3)) ? $sellerID.'/'.$pID : $sellerID.'/'.encrypt($pID)?>">
                                    <img src="<?php echo base_url()?>uploads/products/<?php echo $value->product_image?>" alt="">
                                    <div class="title">
                                        <p><?php echo $value->product_name;?></p>
                                    </div>
                                    <div class="price">
                                        <?php
                                        if($sellerinfo->country == "thailand"){
                                            echo "฿".$value->product_price;
                                        }
                                        else{
                                            echo "₦".$value->product_price;
                                        }
                                        ?>
                                    </div>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
            </div>
        </div>

        <div class="store-section store-settings">
            <div class="container">
                <div class="store-section-title">
                    <h3>Store Settings</h3>
                </div>

                <div class="store-section-content">
                    <div class="details-main opening-closing-time">
                        <h4 class="details-title">Opening and Closing Time</h4>
                        <?php if($storedata):?>
                        <div class="details-content">
                            <?php if($storedata->store_open == "custom"):
                            if(isset($storetime[0])){
                                $t1 = explode("-", $storetime[0]);
                                $tmon = date('h:i A', strtotime($t1[1]));
                                if(isset($t1[2])){
                                    $tmon2 = date('h:i A', strtotime($t1[2]));
                                }
                                else{
                                    $tmon2 = '';
                                }
                            }
                            if(isset($storetime[1])){
                                $t2 = explode("-", $storetime[1]);
                                $ttue =  date('h:i A', strtotime($t2[1]));
                                if(isset($t2[2])){
                                    $ttue2 = date('h:i A', strtotime($t2[2]));
                                }
                                else{
                                    $ttue2 = '';
                                }
                            }   
                            if(isset($storetime[2])){
                                $t3 = explode("-", $storetime[2]);
                                $twed = date('h:i A', strtotime($t3[1]));
                                if(isset($t3[2])){
                                    $twed2 = date('h:i A', strtotime($t3[2]));
                                }
                                else{
                                    $twed2 = '';
                                }
                            }
                            if(isset($storetime[3])){
                                $t4 = explode("-", $storetime[3]);
                                $tthu = date('h:i A', strtotime($t4[1]));
                                if(isset($t4[2])){
                                    $tthu2 = date('h:i A', strtotime($t4[2]));
                                }
                                else{
                                    $tthu2 = '';
                                }
                            }
                            if(isset($storetime[4])){
                                $t5 = explode("-", $storetime[4]);
                                $tfri = date('h:i A', strtotime($t5[1]));
                                if(isset($t5[2])){
                                    $tfri2 = date('h:i A', strtotime($t5[2]));
                                }
                                else{
                                    $tfri2 = '';
                                }
                            }
                            if(isset($storetime[5])){
                                $t6 = explode("-", $storetime[5]);
                                $tsat = date('h:i A', strtotime($t6[1]));
                                if(isset($t6[2])){
                                    $tsat2 = date('h:i A', strtotime($t6[2]));
                                }
                                else{
                                    $tsat2 = '';
                                }
                            }
                            if(isset($storetime[6])){
                                $t7 = explode("-", $storetime[6]);
                                $tsun = date('h:i A', strtotime($t7[1]));
                                if(isset($t7[2])){
                                    $tsun2 = date('h:i A', strtotime($t7[2]));
                                }
                                else{
                                    $tsun2 = '';
                                }
                            }
                            ?>
                            <p><?php echo ($storedata && in_array("Mon-Closed", $storetime)) ? "Monday : Closed" : "Monday : $tmon "." - ".$tmon2; ?>
                            </p>
                            <p><?php echo ($storedata && in_array("Tue-Closed", $storetime)) ? "Tuesday : Closed" : "Tuesday : $ttue "." - ".$ttue2; ?>
                            </p>
                            <p><?php echo ($storedata && in_array("Wed-Closed", $storetime)) ? "Wednesday : Closed" : "Wednesday : $twed "." - ".$twed2; ?>
                            </p>
                            <p><?php echo ($storedata && in_array("Thu-Closed", $storetime)) ? "Thursday : Closed" : "Thursday : $tthu "." - ".$tthu2; ?>
                            </p>
                            <p><?php echo ($storedata && in_array("Fri-Closed", $storetime)) ? "Friday : Closed" : "Friday : $tfri "." - ".$tfri2; ?>
                            </p>
                            <p><?php echo ($storedata && in_array("Sat-Closed", $storetime)) ? "Saturday : Closed" : "Saturday : $tsat "." - ".$tsat2; ?>
                            </p>
                            <p><?php echo ($storedata && in_array("Sun-Closed", $storetime)) ? "Sunday : Closed" : "Sunday : $tsun "." - ".$tsun2; ?>
                            </p>
                            <?php elseif($storedata->store_open == "alltime"):?>
                            <p>Always Open</p>
                            <?php endif;?>
                        </div>
                        <?php endif;?>
                    </div>

                    <div class="details-main opening-closing-time">
                        <h4 class="details-title">Delivery and Return</h4>
                        <?php if($storedata):?>
                        <div class="details-content">
                            <div class="delivery-single">
                                <h3>Do we offer delivery?</h3>
                                <div class="delivery-answer">
                                    <?php if($storedata && $storedata->delivery_services == '1'):?>
                                        <p class="yes">
                                            <i class="far fa-check-circle"></i>
                                            Yes
                                        </p>
                                        <?php endif;?>
                                        <?php if($storedata && $storedata->delivery_services == '0'):?>
                                        <p class="no">
                                            <i class="far fa-times-circle"></i>
                                            No
                                        </p>
                                    <?php endif;?>
                                </div>
                                <?php if($storedata && $storedata->delivery_services == '1'):?>
                                <div class="delivery-content">
                                    <h3>Here are our delivery options</h3>
                                    <p><?php echo implode(", ",$deliveryoptions);?></p>
                                </div>
                                 <?php endif;?>
                            </div>
                            <div class="delivery-single">
                                <h3>Do we offer Returns?</h3>
                                <div class="delivery-answer">
                                    <?php if($storedata && $storedata->return_offer == '1'):?>
                                        <p class="yes">
                                            <i class="far fa-check-circle"></i>
                                            Yes
                                        </p>
                                        <?php endif;?>
                                        <?php if($storedata && $storedata->return_offer == '0'):?>
                                        <p class="no">
                                            <i class="far fa-times-circle"></i>
                                            No
                                        </p>
                                    <?php endif;?>
                                </div>
                                <?php if($storedata && $storedata->return_offer == '1'):?>
                                <div class="delivery-content">
                                    <h3>Returns available up to</h3>
                                    <p><?php echo implode(" Days, ",$deliveryoffer);?> Days</p>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                    <div class="details-main opening-closing-time">
                        <h4 class="details-title">Which states are we available in</h4>
                        <?php if($storedata):?>
                        <div class="details-content">
                            <div class="delivery-single">
                                <div class="delivery-content">
                                <?php if($storedata && !empty($storedata->offer_states)):?>
                                    <p><?php
                                     $states = unserialize($storedata->offer_states);
                                     echo implode(", ",$states);?></p>
                                <?php else :?>
                                    <p>No states availabel</p>
                                </div>
                                 <?php endif;?>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>

        <div class="store-section social-section-sticky">
            <div class="container">
                <a href="<?php echo $socialinfo['twitter']['website']?>" target="_blank"><i class="fab fa-twitter" style="color: #1da1f2;"></i></a>
                <a href="<?php echo (strpos($socialinfo['insta']['website'], 'instagram.com') !== false) ? $socialinfo['insta']['website']: "https://www.instagram.com/{$socialinfo['insta']['website']}"; ?>" target="_blank"><i class="fab fa-instagram" style="color: #c32aa3;"></i></a>
                <?php
                if(!empty($storedata->website_url)){
                    $var = $storedata->website_url;
                    if(strpos($var, 'http://') !== 0) {
                      $url = 'http://' . $var;
                    }else{
                      $url = $var;
                    }
                }
                else{
                    $url = '';
                }
                ?>
                <!-- <a href="<?php echo $socialinfo['facebook']['website']?>" target="_blank"><i class="fab fa-facebook" style="color: #3b5998;"></i></a> -->
                <a href="<?php echo (!empty($url)) ? $url : '';?>" target="_blank"><i class="fa fa-globe" style="color: #ff3300;"></i></a>
                <?php if(!empty($storedata->contact_number)):?>
                <a href="#" class="trackwp"><img src="<?php echo base_url('assets/img/whatsapp_icon.png')?>" width="18px"></a>
                <?php endif;?>
                <!-- <a href="tel:+<?php echo $storedata->contact_number?>"><i class="fa fa-phone"></i></a> -->
            </div>
        </div>
    </div>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>

    <script>
         $(document).on('click','.trackwp',function(e){
           var seller = "<?php echo $sellers ?>";

           $.ajax({                
            type : 'POST',
            url  : "<?php echo base_url('customer/trackusers') ?>",
            dataType: "json",
            data : {
                sellerID:seller
            },
            success: function(response){
                if(response.status == true){ // Nigeria country code
                    window.location.href = "https://wa.me/<?php echo $code?><?php echo $mobile?>";
                }
            }
            });
        });  

        $(".rate").rate({
            //selected_symbol_type: 'fontawesome_star',
            max_value: 5,
            step_size: 0.1,
            readonly: true
        })

        $('#collection-carousel').flexslider({
            animation: "slide",
            itemWidth: 140,
            itemMargin: 15,
            nav: false,
            touch: true,
            controlNav: false,
            directionNav: false,
            reverse: false,
            animationLoop: false,
            slideShow: false
        });

        $('#featured-products').flexslider({
            animation: "slide",
            itemWidth: 180,
            itemMargin: 15,
            nav: false,
            touch: true,
            controlNav: false,
            directionNav: false,
            reverse: false,
            animationLoop: false,
            slideShow: false
        });

    </script>
     
</body>

</html>
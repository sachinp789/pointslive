<?php
$price = 0;
$count = 0;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?php echo $title;?></title>
</head>
<body>
   <div class="desktop-view">
    <section id="page-title-section">
        <div class="container">
            <div class="mobile-menu">
                <a href="#" id="sidebar-open--anchor">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
           
            <select id="main-nav--selector" class="main-nav--selector">
                    <option value="#">Go to</option>
                    <?php if($this->session->userdata('customerId')):?>
                    <option data-fa="fas fa-user" value="<?php echo base_url()?>customer/profile">Profile</option>
                    <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">logout</option>
                    <?php endif;?>
            </select>
            <h1><?php echo $title;?></h1>

            <a href="#">
                <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
            </a>
        </div>
    </section>

    <section id="cart-section" class="order-history" style="border-top: 1px solid lightgray">
        <div class="container">
            <?php 
            if(count($results) > 0):
            foreach ($results as $data) {
                $users = admin_info($data->created_by);    
                foreach ($data->products as $key => $value) {
                    if($value->sale_price > 0){
                        $price = $value->sale_price;
                    }
                    else{
                        $price = $value->product_price;
                    }
                 $pimg = $this->db->select('product_image')->from('product_images')->where('product_id',$value->product_id)->get()->row();
            ?>
                <div class="cart-single">
                    <div class="cart-image">
                        <img src="<?php echo base_url("uploads/products/{$pimg->product_image}")?>" alt="">
                    </div>
                    <div class="cart-details">
                        <div class="product-price">
                            <h2>Order No.</h2>
                            <h3><?php echo $data->id;?></h3>
                        </div>

                        <div class="product-price">
                            <h2>Seller Name</h2>
                            <h3><?php echo $users->admin_name;?></h3>
                        </div>

                        <div class="product-price">
                            <h2>Total Price</h2>
                            <h3><?php echo $data->order_amount;?></h3>
                        </div>

                        <div class="product-price">
                            <h2>Status</h2>
                            <?php
                            if($data->status == '0'){
                                $class = 'processing';
                                $text = 'Processing';
                            }
                            else if($data->status == '1'){
                                $class = 'dispatched';
                                $text = 'Dispatched';
                            }
                            else if($data->status == '2'){
                                $class = 'completed';
                                $text = 'Completed';    
                            }
                            else if($data->status == '3'){
                                $class = 'canceled';
                                $text = 'Canceled';
                            }
                            else if($data->status == '4'){
                                $class = 'rejected';
                                $text = 'Rejected';
                            }
                            else if($data->status == '5'){
                                $class = 'failed';
                                $text = 'Failed';
                            }
                            else if($data->status == '6'){
                                $class = 'completed';
                                $text = 'Confirm Delivered';  
                            }
                            else if($data->status == '7'){
                                $class = 'failed';
                                $text = 'Confirm Failed';  
                            }
                            /*else if($orders[0]->status == '6'){
                                $class = 'processing';
                                $text = 'Processing';
                            }*/
                            ?>
                            <p class="status status-<?php echo $class?>"><?php echo $text;?></p>
                        </div>
                        <div class="single-order-action">
                            <a href="<?php echo base_url("item/order_details/{$data->transaction_id}")?>">
                                Action
                            </a>
                        </div>
                    </div>
                </div>
            <?php } $count++;
            }
            else:
            ?>
            <p>* No orders found.</p>
        <?php endif;?>
        </div>
        <div id="pagination">
            <ul class="tsc_pagination">
            <?php foreach ($links as $link) {
                    echo "<li>". $link."</li>";
            } ?>
        </ul>
        </div>
    </section>
   </div>

   <div class="mobile-view">
       <div class="details-overlay"></div>
       <div id="order-confirm-popup" class="modal-window">
            <div class="close-container">
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>

            <div class="modal-content">
                <img class="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="">
                 <h2>Do you confirm this order is Delivered/Failed ?</h2>
                <div class="action-container">
                    <a href="#" class="confirm-positive" id="confrim-order-event" data-id="<?php echo $results[0]->id?>">Confirmed</a>
                    <a href="#" class="confirm-negative" id="confrim-order-negative" data-id="<?php echo $results[0]->id?>">No</a>
                </div>
            </div>
        </div>
        <div class="navigator-head">
            <div class="container">
                <div class="left-head">
                <a href="<?php echo base_url()?>customer/profile">
                    <i class="fa fa-chevron-left"></i>
                    <span><?php echo $title?></span>
                </a> 
                </div>
            </div>
        </div> 
        <div class="order-info-container">
         <div class="order-listing">
        <?php 
        if(count($results) > 0):

        foreach ($results as $data) {
            $users = admin_info($data->created_by);
            if($data->status == '0'){
                $class = 'processing';
                $text = 'Processing';
            }
            else if($data->status == '1'){
                $class = 'dispatched';
                $text = 'Dispatched';
            }
            else if($data->status == '2'){
                $class = 'completed';
                $text = 'Delivered';    
            }
            else if($data->status == '3'){
                $class = 'canceled';
                $text = 'Canceled';
            }
            else if($data->status == '4'){
                $class = 'rejected';
                $text = 'Rejected';
            }
            else if($data->status == '5'){
                $class = 'failed';
                $text = 'Failed';
            }
            else if($data->status == '6'){
                $class = 'completed';
                $text = 'Confirm Delivered';  
            }
            else if($data->status == '7'){
                $class = 'failed';
                $text = 'Confirm Failed';  
            }
            //$users = admin_info($data->created_by);    
            foreach ($data->products as $key => $value) {
                if($value->sale_price > 0){
                    $price = $value->sale_price;
                }
                else{
                    $price = $value->product_price;
                }
                $pimg = $this->db->select('product_image')->from('product_images')->where('product_id',$value->product_id)->get()->row();
                $ratings = products_rating_by_user($value->product_id,$this->session->userdata("customerId"),$data->id);
        ?>
        <div id="order-rating-popup-<?php echo $count?>" class="modal-window">
            <div class="close-container">
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>

            <div class="modal-content">
                <img class="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="">
                
                <h2>Please rate the product/service you got from</h2>

                <div class="rate-popup" data-rate-value="<?php echo ($ratings[0]->product_id == $value->product_id) ? $ratings[0]->rating : "0";?>" data-order="<?php echo $data->id?>" data-id="<?php echo $value->product_id?>" data-user="<?php echo $users->admin_id?>"></div>
                
            </div>
        </div>
        <div class="order-single">
            <div class="order-details">
                <div class="order-image">
                    <img src="<?php echo base_url("uploads/products/{$pimg->product_image}")?>" alt="">
                </div>

                <div class="order-meta">
                    <div class="product-title">
                        <p><?php echo $value->product_name;?></p>
                    </div>
                       <div class="seller-title">
                        <p>Seller Name: <?php echo $users->admin_name;?></p>
                    </div>
                    <div class="product-amount">
                        <p>Total Amount: <?php echo $data->order_amount;?></p>
                    </div>
                    <div class="product-quantity">
                        <p>Status: <?php echo $text;?></p>
                    </div>
                </div>
            </div>
            <div class="action-bar m-t-10">
                <a href="<?php echo base_url("item/order_details/{$data->transaction_id}")?>" class="view-detail-btn">View Detail</a>
                <?php                                  
                 if($key == 0 && $data->status == '2' && $data->store_type == 'delivery'): ?>
                    <a id="order-confirm-anchor" class="order-confirm" data->Confirm Order</a>
                <?php endif; ?>
                <?php  
                if($data->store_type == 'delivery' && $data->status == '6' && count($ratings) == 0):?>
                  <a id="order-rating-anchor" data-count="<?php echo $count?>" class="btn-rating">Rating</a>  
                <?php                
                endif;
                ?>
                <?php 
                if($data->status == '2' || $data->status == '6' && $data->store_type == 'pickup' && count($ratings) == 0): ?>
                    <a id="order-rating-anchor"  data-count="<?php echo $count?>" class="btn-rating">Rating</a>
                <?php endif; ?>
                <?php if(count($ratings) > 0):?>
                    <a class="high-rating"> <i class="fa fa-star-o"></i> <?php echo $ratings[0]->rating;?></a>
                <?php else :?>
                    <a class="low-rating"> <i class="fa fa-star-o"></i> 0</a>
                <?php endif;?>
            </div>
        </div>
        <?php $count++;
            }
        } 
        else : echo "<p>* No orders found.</p>";
        endif;
        ?>
    </div>   
    </div>
   </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jqueryui.js"></script>
    <script src="<?php echo base_url()?>assets/js/rater.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <script type="application/javascript">
     $('#order-confirm-anchor').on('click', function (e) {
            e.preventDefault();
            $('#order-confirm-popup').fadeIn();
            $('.details-overlay').fadeIn();

            $('#order-confirm-popup .close-container a').on('click', function () {
                $('#order-confirm-popup').fadeOut();
                $('.details-overlay').fadeOut();
            });
        });

        $('.btn-rating').on('click', function (e) {
            e.preventDefault()
            count = $(this).attr("data-count");

            $('#order-rating-popup-'+count).fadeIn();
            $('.details-overlay').fadeIn();

            $('#order-rating-popup-'+count+' .close-container a').on('click', function () {
                $('#order-rating-popup-'+count).fadeOut();
                $('.details-overlay').fadeOut();
            });
        });

       /* $('.confirm-negative').on('click', function () {
            $('#order-confirm-popup').fadeOut();
            $('.details-overlay').fadeOut();
        });*/
           $(".rate-popup").rate({
            max_value: 5,
            step_size: 1,
        }).on('click', function () {
            rate = $(this).attr("data-rate-value");
            product = $(this).attr("data-id");
            order = $(this).attr("data-order");
            seller = $(this).attr("data-user");

            $.ajax({
              url:'<?php echo base_url()?>item/ajax_product_rating_by_order',
              type:"POST",
              data: {orderID:order,orderRate:rate,productID:product,sellerID:seller},
              dataType:"json",
              beforeSend: function(){ 
                $(".rate-popup").html('&nbsp; <span style="font-size: 18px;">Please wait...</span>');
              },
              success: function(response){
                if(response.staus == true){
                    alert(response.msg);
                }
                else{
                    alert(response.msg);
                }
                location.reload();
              }
            });

        });
    </script>
</body>

</html>
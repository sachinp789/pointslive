<?php
echo '<pre>';
print_r($categories);exit;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title;?></title>
</head>

<body>
    <section id="product-slide-section">
        <div class="container">
            <div class="slide-container">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <?php
                        $pimages = $productInfo[0]->images;
                        foreach ($pimages as $key => $value) {
                        ?>
                        <li>
                            <img src="<?php echo base_url()?>uploads/products/<?php echo $value->product_image?>" />
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <!-- <div id="carousel" class="flexslider">
                    <ul class="slides">
                       <?php
                        $pimages = $productInfo[0]->images;
                        foreach ($pimages as $key => $value) {
                        ?>
                        <li>
                            <img src="<?php echo base_url()?>uploads/products/<?php echo $value->product_image?>" />
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div> -->
            </div>
        </div>
    </section>

    <section id="product-detail-section">
        <div class="container">
            <div class="product-name">
                <h1>Product / Category Name</h1>
                <?php
                $categories = $productInfo[0]->categories;
                $catname = "";
                foreach ($categories as $key => $value) {
                   $catname = $value->category_name;
                }
                ?>
                <p><?php echo $productInfo[0]->product_name ;?> / <?php echo $catname;?></p>
            </div>

            <div class="product-description">
                <h2>Description</h2>
                <p><?php echo $productInfo[0]->product_description ;?></p>
            </div>

            <div class="product-quantity">
                <h2>Quantity</h2>
                <input type="number" name="quantity" min="1" max="<?php echo $productInfo[0]->total_qty?>" value="1">
            </div>

            <div class="product-price">
                <h2>Price</h2>
                <?php
                $price = 0;
                $percentChange = 0;
                if($productInfo[0]->sale_price > 0){
                    $price = $productInfo[0]->sale_price;
                    $percentChange = (1 - $price / $productInfo[0]->product_price) * 100;
                }
                else{
                    $price = $productInfo[0]->product_price;
                }
                ?>
                <h3><?php echo number_format($price);?>
                    <span class="discount green">(<?php echo $percentChange;?>% off!)</span>
                </h3>
            </div>

            <div class="checkout-btn-group">
               <a href="" class="checkout-btn">Add To Cart</a>
               <a href="<?php echo base_url("item/manage_cart/{$this->uri->segment(3)}");?>" class="checkout-btn">Manage Cart (<?php echo $totalcartno; ?>)</a>
               <a href="" class="checkout-btn">Checkout</a>
            </div>
        </div>
    </section>

    <!-- <script src="assets/js/jquery.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
     <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <!-- <script src="assets/js/main.js"></script> -->
    <script>
        $(window).on('load', function () {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 210,
                itemMargin: 5,
                asNavFor: '#slider'
            });
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            });
        });
    </script>
</body>

</html>
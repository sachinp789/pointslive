<?php
$total = 0;
$price = 0;
$currency = '';
$sellerInfo = admin_info($orders[0]->created_by);
if($sellerInfo->country == "thailand"){
    $currency = "฿";
}
else if($sellerInfo->country == "nigeria"){
    $currency = "₦";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title; ?></title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121417530-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121417530-1');
    </script>


     <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2244243415809474');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2244243415809474&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
      fbq('track', 'Purchase');
    </script>

</head>

<body id="order_confirmation">
    <div class="sidebar-menu">
            <div class="sidebar-menu--close--container text-right">
                <i id="menu_slide" class="fas fa-chevron-left"></i>
            </div>

            <div class="sidebar-menu--list--container">
                <ul class="sidebar-menu--list">
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/orders"><i class="fa fa-history"></i>Order History</a>
                    </li>
                </ul>
            </div>

        </div>
        <div id="popup-form"></div>
    <section id="page-title-section">
        <div class="container">
            <div class="mobile-menu">
                <a href="#" id="sidebar-open--anchor">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <select id="main-nav--selector" class="main-nav--selector">
                <option value="#">Go to</option>
                <option data-fa="fa fa-history" value="<?php echo base_url()?>item/orders">Order history</option>
            </select>
            <h1><?php echo $title; ?></h1>
             <a href="#">
                <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
            </a>
        </div>
    </section>
    <section id="order-meta">
        <div class="container">
       <!--  <?php if (!empty($this->session->flashdata('flashmsg'))): ?>
            <div id="message-box-<?php echo $this->session->userdata('msg')?>"><?php echo $this->session->flashdata('flashmsg');?></div>
        <?php endif;?> -->
         <div id="message-box-success"></div>
        </div>
        <div class="container">
            <div class="order-date">
                <h2>Order Date:</h2>
                <span><?php echo date("jS F, Y", strtotime($orders[0]->created_date));  ?></span>
            </div>
            <div class="order-id">
                <h2>Order ID:</h2>
                <span><?php echo $orders[0]->transaction_id ?></span>
            </div>
            <div class="order-id">
                <h2>Payment Type:</h2>
                <span><?php echo $orders[0]->payment_method ?></span>
            </div>
            <div class="total">
                <h2>Order Total:</h2>
                <span><?php echo $currency."".$orders[0]->order_amount ?></span>
            </div>
        </div>
    </section>
    <?php if(count($orders) > 0) : ?>
    <div class="container eticket-container">
            <a href="<?php echo base_url()?>uploads/invoices/eTicket-<?php echo $orders[0]->transaction_id?>.pdf" class="btn" download>Download eTicket</a>
    </div>
    <?php endif;?>
    <section id="cart-section">
        <div class="container">
            <?php if(count($orders) > 0): 
                foreach($orders[0]->products as $value) :
                if($value->sale_price > 0){
                    $price = $value->sale_price;
                }
                else{
                    $price = $value->product_price;
                }
                //$total  = $total + ($price * $value->ordered_qty);
            ?>
            <div class="cart-single">
                <div class="cart-image">
                    <?php
                    $pimages = $this->db->select('product_image')->from('product_images')->where('product_id',$value->product_id)->get()->row();
                    ?>
                    <img src="<?php echo base_url()?>uploads/products/<?php echo $pimages->product_image?>" alt="">
                </div>
                <div class="cart-details">
                    <div class="name-container">
                        <h2><?php echo $value->product_name; ?></h2>
                    </div>
                    <div class="product-price">
                        <h2>Quantity</h2>
                        <h3><?php echo $value->ordered_qty; ?></h3>
                    </div>
                    <div class="product-price">
                        <h2>Price</h2>
                        <h3><?php echo $currency."".number_format($price); ?></h3>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php else :?>
            <p>* No orders found.</p>
            <?php endif;?>
        </div>
        <div class="container">
            <div class="total-price-container">
                <div class="label-container">
                    <h2>Total Price</h2>
                </div>

                <div class="price-container">
                    <p><?php echo $currency."".number_format($orders[0]->order_amount);?></p>
                </div>
            </div>
        </div>
        <?php if($orders[0]->store_type == "delivery"):?>
        <div class="container order-details-container">
            <h2>Shipping Address</h2>
            <p>
                <?php echo $orders[0]->shipping_address.",\n".$orders[0]->state.",\t".$orders[0]->local_government.",\n".$orders[0]->country?>
            </p>
        </div>
        <?php endif;?>
       <!--  <div class="container eticket-container">
            <a href="<?php echo base_url()?>uploads/invoices/eTicket-<?php echo $orders[0]->transaction_id?>.pdf" class="btn" download>Download eTicket</a>
        </div> -->
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <script type="text/javascript">
    baseurl = "<?php echo base_url()?>";    
    message = "<?php echo $this->session->flashdata('flashmsg');?>";
    if(message.length > 0){
        $('#message-box-success').html('<img src='+baseurl+'assets/img/logo.png>'+message);
        $('#message-box-success').slideDown('slow').delay(2000).slideUp('slow');
    }
    </script>
</body>

</html>
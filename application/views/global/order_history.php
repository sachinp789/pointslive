<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?php echo $title;?></title>
</head>
<body>
    <div class="sidebar-menu">
        <div class="sidebar-menu--close--container text-right">
            <i id="menu_slide" class="fas fa-chevron-left"></i>
        </div>

        <div class="sidebar-menu--list--container">
            <ul class="sidebar-menu--list">
                <?php if($this->session->userdata('customerId')):?>
                <li class="sidebar-menu--list--item"><a href="<?php echo base_url()?>customer/signOut">Logout</a></li>
                 <?php endif;?>
            </ul>
        </div>
    </div>
    <div id="popup-form"></div>
    <section id="page-title-section">
        <div class="container">
            <div class="mobile-menu">
                <a href="#" id="sidebar-open--anchor">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <select id="main-nav--selector" class="main-nav--selector">
                    <option value="#">Go to</option>
                    <?php if($this->session->userdata('customerId')):?>
                    <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                    <?php endif;?>
            </select>
            <h1><?php echo $title;?></h1>

            <a href="#">
                <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
            </a>
        </div>
    </section>

     <section id="cart-section" class="order-history" style="border-top: 1px solid lightgray">
        <div class="container">
            <?php 
            $price = 0;
            if(count($results) > 0):
            foreach ($results as $data) {
                $users = admin_info($data->created_by);    
                foreach ($data->products as $key => $value) {
                    if($value->sale_price > 0){
                        $price = $value->sale_price;
                    }
                    else{
                        $price = $value->product_price;
                    }
                 $pimg = $this->db->select('product_image')->from('product_images')->where('product_id',$value->product_id)->get()->row();
                }
            ?>
                <div class="cart-single">
                    <div class="cart-image">
                        <img src="<?php echo base_url("uploads/products/{$pimg->product_image}")?>" alt="">
                    </div>
                    <div class="cart-details">
                        <div class="product-price">
                            <h2>Order No.</h2>
                            <h3><?php echo $data->id;?></h3>
                        </div>

                        <div class="product-price">
                            <h2>Seller Name</h2>
                            <h3><?php echo $users->admin_name;?></h3>
                        </div>

                        <div class="product-price">
                            <h2>Total Price</h2>
                            <h3><?php echo $price;?></h3>
                        </div>

                        <div class="product-price">
                            <h2>Status</h2>
                            <?php
                            if($data->status == '0'){
                                $class = 'processing';
                                $text = 'Processing';
                            }
                            else if($data->status == '1'){
                                $class = 'dispatched';
                                $text = 'Dispatched';
                            }
                            else if($data->status == '2'){
                                $class = 'completed';
                                $text = 'Delivered';    
                            }
                            else if($data->status == '3'){
                                $class = 'canceled';
                                $text = 'Canceled';
                            }
                            else if($data->status == '4'){
                                $class = 'rejected';
                                $text = 'Rejected';
                            }
                            else if($data->status == '5'){
                                $class = 'failed';
                                $text = 'Failed';
                            }
                            else if($data->status == '6'){
                                $class = 'completed';
                                $text = 'Completed';  
                            }
                            else if($data->status == '7'){
                                $class = 'failed';
                                $text = 'Confirm Failed';  
                            }
                            /*else if($orders[0]->status == '6'){
                                $class = 'processing';
                                $text = 'Processing';
                            }*/
                            ?>
                            <p class="status status-<?php echo $class?>"><?php echo $text;?></p>
                        </div>
                        <div class="single-order-action">
                            <a href="<?php echo base_url("item/order_details/{$data->transaction_id}")?>">
                                Action
                            </a>
                        </div>
                    </div>
                </div>
            <?php }
            else:
            ?>
            <p>* No orders found.</p>
        <?php endif;?>
        </div>
        <div id="pagination">
            <ul class="tsc_pagination">
            <?php foreach ($links as $link) {
            echo "<li>". $link."</li>";
            } ?>
        </ul>
        </div>
        <!-- <p><?php print_r($links); ?></p> -->
    </section>

    <!-- <script src="assets/js/jquery.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
</body>

</html>
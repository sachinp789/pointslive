<?php
$price = 0;
$total = 0;
$shipping_cost = 0;
$urlinfo = parse_url($_SERVER['REQUEST_URI']);

if(is_digit($this->uri->segment(3))){
    $sellerID = $this->uri->segment(3);
}
else{
    $sellerID = decrypt($this->uri->segment(3));
}

$seller = $this->uri->segment(3);
//$sellerID = decrypt($this->uri->segment(3));
$sellerinfo = admin_info($sellerID);
$currency = "";
if($sellerinfo && $sellerinfo->country == "thailand"){
    $currency = "฿";
}
else if($sellerinfo && $sellerinfo->country =="nigeria"){
    $currency = "₦";
}

$user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

$cartcount = $this->Shopping_carts_model->where("user_id",$user)->get_all();
if($cartcount){
    $totalcartno = count($cartcount);
}
else{
    $totalcartno = 0;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title;?></title>
</head>

<body id="cart">
    <section id="page-title-section" style="border-bottom: 1px solid lightgray">
        <div class="container">
            <a href="<?php echo $this->session->userdata('previous_page');?>">
                <i class="fa fa-chevron-left"></i>
                <span> BACK</span>
            </a>
            <h1><?php echo $title;?></h1>
            <a href="#">
                <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
            </a>
        </div>
    </section>
    <section id="cart-section">
        <input type="hidden" id="productscount" value="<?php echo count($products);?>">
       
        <div class="container">
            <p class="nocart"></p>
            <!-- <?php if (!empty($this->session->flashdata('flashmsg'))): ?>
            <?php endif;?>     -->
            <div id="message-box-success"><?php //echo $this->session->flashdata('flashmsg');?></div>
            <?php
            foreach ($products as $key => $value) {
            ?>
            <div class="cart-single" id="cart_single_<?php echo $value->product_id?>">
                <div class="cart-image">
                    <img src="<?php echo base_url()?>uploads/products/<?php echo $value->product_image?>" alt="">
                </div>
                <div class="cart-details">
                    <h2><?php echo $value->product_name?></h2>
                    <div class="product-quantity">
                        <h2>Quantity</h2>
                        <input type="number" name="quantity[]" min="1" max="<?php echo $value->total_qty?>" value="<?php echo $value->qty?>" id="qty_<?php echo $value->product_id?>" class="qty" data-value="<?php echo $value->product_id?>">
                        <input type="hidden" name="products[]" value="<?php echo $value->product_id?>">
                        <input type="hidden" name="masterid[]" value="<?php echo $value->id?>">
                    </div>
                    <div class="product-price">
                        <h2>Price</h2>
                        <?php
                        if($value->sale_price > 0){
                            $price = $value->sale_price;
                        }
                        else{
                            $price = $value->product_price;   
                        }
                        $total = ($total + ($price * $value->qty)); //- ($value->shipping_cost)
                        ?>
                        <h3><?php echo $currency."".number_format($price);?></h3>
                    </div>
                    <div class="cart-product-delete">
                        <a onclick="RemoveCart(<?php echo $value->product_id;?>)" id="removecart_<?php echo $value->product_id;?>" class="deleteitem">Remove</a>
                        <a onclick="UpdateCart(<?php echo $value->product_id;?>)" id="updatecart_<?php echo $value->product_id?>" data-attr="<?php echo $value->product_id;?>">Update</a>
                    </div>
                </div>
            </div>
            <?php 
            }
            ?>
        </div>
        <div class="container">
            <div class="total-price-container">
                <div class="label-container">
                    <h2>Total Price</h2>
                </div>

                <div class="price-container">
                    <p><?php echo $currency."".number_format($total);?></p>
                </div>
            </div>
        </div>
        <?php $this->session->set_userdata('previous_page_2', base_url(uri_string()));?>
        <div class="container">
            <div class="checkout-btn-group">
               <!--  <input type=submit class="checkout-btn" value="Update" id="btnupdate" style="cursor:pointer;"> -->
                <a href="<?php echo base_url()?>item/category/<?php echo $seller?>" class="checkout-btn">Continue Shopping</a>
                <a href="<?php echo base_url()?>item/checkout/<?php echo $seller; ?>" class="checkout-btn" <?php echo(count($products) > 0) ? "style=pointer-events:auto;cursor:pointer;" : "style=pointer-events:none;cursor:not-allowed;" ?> id="btncheckout">Checkout</a>
            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
     <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <script type="text/javascript">
    baseurl = "<?php echo base_url() ?>";
    countproduct = $("#productscount").val();
    if(countproduct == 0){
         $(".nocart").text("* No items in cart.");
    }
    function RemoveCart(productID){
        var r = confirm("Remove item from cart ?");
        if(r == true){
            $.ajax({
              url:"<?php echo base_url()?>item/RemoveItem",
              type:"POST",
              data: {product:productID},
              dataType:"json",
              success: function(response){
                if(response.msg == true){
                    $("#cart_single_"+productID).remove();
                    countproduct--;
                    $("#productscount").val(countproduct);
                    if(countproduct == 0){
                        $(".price-container p").text("0");
                        $(".nocart").text("* No items in cart.");
                        $("#btncheckout").css("cursor","not-allowed");
                        $("#btncheckout").css("pointer-events","none");
                    }
                    //$("#").css({"pointer-events":"none;","cursor":"not-allowed;"});
                    $(".total-price-container").load(location.href+" .total-price-container>*","");
                }
                else{
                    alert("Error ! Item not removed from cart.");
                    return false;
                }
              }
            });
        }
    }

   /* $("#qty").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#btnupdate").click();
        }
    });*/

    $(".qty").keyup(function(event) {
        pid = $(this).attr('data-value');
        //alert(pid);return false;
        if (event.keyCode === 13) {
            $("#updatecart_"+pid).click();
        }
    });

    // Update cart of user by Product ID
    function UpdateCart(productID){
        userqty = $("#qty_"+productID).val();
        $.ajax({
          url:"<?php echo base_url()?>item/update_items",
          type:"POST",
          data: {product:productID,qty:userqty},
          dataType:"json",
          success: function(response){
            if(response.msg == true){
                $('#message-box-success').html('<img src='+baseurl+'assets/img/logo.png><p>'+response.message+'</p>');
                $('#message-box-success').slideDown('slow').delay(1000).slideUp('slow');
                $(".total-price-container").load(location.href+" .total-price-container>*","");
                //setTimeout(function(){ location.reload(true); }, 1000);
            }
            else{
                $('#message-box-error').html('<p>'+response.message+'</p>');
                return false;
            }
          }
        });
    }
    </script>
</body>

</html>
<?php 
$this->load->model('Shopping_carts_model');
$sellers = decrypt($this->uri->segment(3));
$pimages = ($productInfo) ? $productInfo[0]->images : [];

if(empty($sellers) || $sellers == '1'){

    if(is_digit($this->uri->segment(3))){
        $seller =  ($productInfo) ? $productInfo[0]->created_by : ''; 
        $sellerID = ($productInfo) ? $productInfo[0]->created_by : ''; 
    }
    else{
        $seller =  ($productInfo) ? $productInfo[0]->created_by : '';
        $sellerID = encrypt($productInfo[0]->created_by); 
    }

}
else{
    if(is_digit($this->uri->segment(3))){
        $seller = $this->uri->segment(3);
        $sellerID = $this->uri->segment(3);
    }
    else{
        $seller = $sellers;
        $sellerID = $this->uri->segment(3);
    }

}

$user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

//var_dump($seller);
$sellerinfo = admin_info($seller);

if(is_digit($this->uri->segment(4))) {
    $product = $this->uri->segment(4);
    $category = ($productInfo) ? $productInfo[0]->categories : [];
}
else{
    $product = decrypt($this->uri->segment(4));
    $category = ($productInfo) ? encrypt($productInfo[0]->categories) : '';
}


$cartcount = $this->Shopping_carts_model->where("user_id",$user)->get_all();
if($cartcount){
    $totalcartno = count($cartcount);
}
else{
    $totalcartno = 0;
}

$price = 0;
$percentChange = 0;
if($productInfo){
    if($productInfo[0]->sale_price > 0){
        $price = $productInfo[0]->sale_price;
        $percentChange = (1 - $price / $productInfo[0]->product_price) * 100;
    }
    else{
        $price = $productInfo[0]->product_price;
        $percentChange = 0;
    }
}

$currency = "";

if($productInfo){
    if($sellerinfo->country == "thailand"){
        $currency = "฿";
    }
    else if($sellerinfo->country =="nigeria"){
        $currency = "₦";
    }
    foreach ($productInfo[0]->categories as $key => $value) {
        $catID = $value->category_id;
    }
    $category = is_digit($this->uri->segment(4)) ? $catID : encrypt($catID);//encrypt($catID);
}
else{
    $category = '';
}
$views = 0;
$visitors_file = "visitors.txt";
// Load up the persisted value from the file and update $views
if (file_exists($visitors_file))
{
    $views = (int)file_get_contents($visitors_file); 
}
// Increment the views counter since a new visitor has loaded the page
$views++;
// Save the contents of this variable back into the file for next time
file_put_contents($visitors_file, $views);

$code = '';
$mobile = '';

if(!empty($storedata->contact_number)){
    $codes = explode("-", $storedata->contact_number);
    if(count($codes) > 1){
        $code = $codes[0];
        $mobile = ltrim($codes[1], '0');
    }
    else{
        $code = '234';
        $mobile = ltrim($codes[0], '0');
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <title><?php echo $title;?></title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121417530-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121417530-1');
    </script>


   <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2244243415809474');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2244243415809474&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    <script>
      fbq('track', 'ViewContent');
    </script>

</head>

<body id="product_detail">
    <div class="desktop-view">
        <div class="sidebar-menu">
            <div class="sidebar-menu--close--container text-right">
                <i id="menu_slide" class="fas fa-chevron-left"></i>
            </div>

            <div class="sidebar-menu--list--container">
                <ul class="sidebar-menu--list">
                    <li class="sidebar-menu--list--item"><a href="<?php echo base_url()?>item/products/<?php echo $category?>/<?php echo $sellerID ?>"><i class="fas fa-th-large"></i>Product List</a>
                    </li>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>"><i class="fas fa-th"></i>Catalogue</a>
                    </li>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/manage_cart/<?php echo $this->uri->segment(3) ?>"><i class="fas fa-shopping-cart"></i>Shopping Cart</a>
                    </li>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/viewstore/<?php echo $sellerID ?>"><i class="fas fa-info-circle"></i>Store Information</a>
                    </li>
                    <?php if($this->session->userdata('customerId')):?>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>item/orders"><i class="fa fa-history"></i>Order History</a>
                    </li>
                    <li class="sidebar-menu--list--item">
                        <a href="<?php echo base_url()?>customer/signOut"><i class="fas fa-sign-out-alt"></i>Logout</a>
                    </li>
                    <?php endif;?>
                </ul>
            </div>
    </div>

    <div id="popup-form"></div>
        <section id="page-title-section">
            <div class="container">
                <div class="mobile-menu">
                    <a href="#" id="sidebar-open--anchor">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <select id="main-nav--selector" class="main-nav--selector">
                        <option value="#">Go to</option>
                        <option data-fa="fas fa-th-large" value="<?php echo base_url()?>item/products/<?php echo $category?>/<?php echo $sellerID ?>"><a href="<?php echo base_url()?>item/products/<?php echo $category?>/<?php echo $sellerID ?>">Product List</a></option>
                        <option data-fa="fas fa-th" value="<?php echo base_url()?>item/category/<?php echo $sellerID ?>"><a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>">Catalogue</a></option>
                        <option data-fa="fas fa-shopping-cart" value="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>"><a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>">Shopping Cart</a></option>
                        <option data-fa="fas fa-info-circle" value="<?php echo base_url()?>item/viewstore/<?php echo $sellerID ?>">Store Information</option>
                        <?php if($this->session->userdata('customerId')):?>
                        <option data-fa="fa fa-history" value="<?php echo base_url()?>item/ordes">Order History</option>
                        <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                        <?php endif;?>
                    </select>
              
                <h1><?php echo $title;?></h1>
                <a href="#">
                    <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
                </a>
            </div>
        </section>
        <section id="product-slide-section">
            <div class="container">
                <div class="slide-container">
                    <div id="slider" class="flexslider">
                        <ul class="slides">
                            <?php
                            if($productInfo && !empty($pimages)):
                            foreach ($pimages as $key => $value) {
                            ?>
                            <li>
                                <img src="<?php echo base_url()?>uploads/products/<?php echo $value->product_image?>" />
                            </li>
                            <?php
                            }
                            else :
                            ?>
                            <li><img src="<?php echo base_url('assets/img/no-p-image.png');?>"></li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="product-detail-section">
            <div class="container">
                <div class="product-name">
                    <h1><?php echo ($productInfo) ? $productInfo[0]->product_name : '' ;?></h1>
                </div>

                <div class="product-description">
                    <h2>Description</h2>
                    <p><?php echo ($productInfo) ? $productInfo[0]->product_description : '' ;?></p>
                </div>

                <div class="product-description">
                    <h2>Available Country</h2>
                    <?php echo ($productInfo) ? ucfirst($sellerinfo->country) : ''; ?>
                </div>

                <div class="product-quantity">
                    <h2>Quantity</h2>
                    <input type="number" name="quantity" min="1" max="<?php echo ($productInfo) ? $productInfo[0]->total_qty : 0 ;?>" value="1" id="qty">
                </div>

                <input type="hidden" name="seller" id="seller" value="<?php echo $seller?>">
                <input type="hidden" name="product" id="product" value="<?php echo $product?>">
                <input type="hidden" name="productsku" id="productsku" value="<?php echo ($productInfo) ? $productInfo[0]->product_sku : ''?>">

                <div class="product-price">
                    <h2>Price</h2>
                    <h3><?php echo $currency."".number_format($price);?>
                        <?php if($percentChange > 0 ):?><span class="discount green">(<?php echo number_format($percentChange);?>% off!)</span>
                        <?php endif;?>
                    </h3>
                </div>

                <div class="checkout-btn-group" id="cartdata">
                   <a href="#" class="checkout-btn" id="addtocart">Add Product To Cart</a>
                   <a href="<?php echo base_url("item/manage_cart/{$sellerID}");?>" class="checkout-btn">Manage Cart (<?php echo $totalcartno; ?>)</a>
                   <a href="<?php echo base_url()?>item/checkout/<?php echo $sellerID; ?>" class="checkout-btn <?php echo($totalcartno > 0) ? "enable": "disable"?>" <?php echo($totalcartno > 0) ? "style=pointer-events:auto;cursor:pointer;" : "style=pointer-events:none;cursor:not-allowed;" ?> id="btncheckout">Checkout</a>
                </div>
                <div id="message-box-success"></div>
                <div id="message-box-error"></div>
            </div>
        </section>
    </div>

     <!-- Mobile View --->
    <div class="mobile-view">
        <input type="hidden" id="seller-id" value="<?php echo $sellerID;?>">
        <div class="details-overlay" style="display:none"></div>

        <div id="email-login-popup" class="modal-window">
            <span class="error error-user" style="display: none;"></span>
            <div class="close-container">
                <a href="#" id="close-modal">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <div class="modal-content">
                <form method="post" id="login-form-customer">
                    <input type="email" id="emailaddress" placeholder="Enter Email" class="form-control" >
                    <input type="password" id="userpassword" placeholder="Enter Password" class="form-control" >
                    <button type="submit" name="login-submit" id="login-submit" class="btn">
                        </span> Signup / In 
                    </button>       
                </form>
            </div>
        </div>

        <div id="order-confirm-popup" class="modal-window" style="display:none">
            <div class="close-container">
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <div class="modal-content" id="confirmmessage">
                <img class="logo" src="<?php echo base_url()?>/assets/img/logo.png" alt=""> 
                <h2>Item has been successfully added to cart.</h2>
            </div>
        </div>

        <div id="login-popup" class="modal-window">
            <div class="close-container" style="text-align:right">
                <a href="#" style="color:#333"><i class="fas fa-times"></i></a>
            </div>
            <div class="modal-content" >
                <img class="logo" src="<?php echo base_url()?>assets/img/logo.png" alt="">
                <div class="social-login-container">
                    <h2>Sign in with</h2>

                    <ul class="login-list">
                        <li class="modal-list-item"><a href="<?php echo base_url('customer/signIn/facebook')?>"><i class="fab fa-facebook"></i></a></li>
                        <li class="modal-list-item"><a href="#" id="sign-in-email"><i class="fas fa-envelope"></i></a></li>
                        <li class="modal-list-item"><a href="<?php echo base_url('customer/signIn/twitter')?>"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="action-container">
                    <span id="guest_checkout_link" class="btn">Continue as Guest</span>
                </div>
            </div>
        </div>

        <div class="navigator-head">
            <div class="container">
                <div class="left-head">
                    <a href="<?php echo base_url()?>item/products/<?php echo $category?>/<?php echo $sellerID ?>"><i class="fa fa-chevron-left"></i>Back</a>
                </div>
                <div class="right-head navigator-right">
                    <a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>" id="head-cart"><i class="fa fa-shopping-cart"></i><span class='cartedge'><?php echo $totalcartno; ?></span></a>
                </div>
            </div>
        </div>

        <div class="product-detail-section">
            <div class="product-image">
                <?php if($productInfo && empty($pimages)):?>
                <img src="<?php echo base_url('assets/img/no-p-image.png');?>">
                <?php elseif($productInfo && !empty($pimages)): ?>
                <img src="<?php echo base_url()?>uploads/products/<?php echo $pimages[0]->product_image;?>"> 
                <?php endif;?>   
            </div>

            <div class="container">
                <div class="product-description">
                    <h3 class="product-name">
                       <?php echo ($productInfo) ? $productInfo[0]->product_name : '';?>
                    </h3>

                    <div class="product-desc-title">
                        <p>
                            Product Details
                        </p>

                        <p>
                        
                            <?php echo $currency."".number_format($price);?>
                        </p>
                    </div>

                    <div class="product-desc-text text-overflow"><?php echo ($productInfo) ? $productInfo[0]->product_description : '' ;?>
                    </div>
                    <?php if($productInfo && strlen($productInfo[0]->product_description) > 100):?>
                        <div class="product-desc-see-more">
                            <a href="#" class="btn-overflow">Read More</a>
                        </div>
                    <?php endif;?>
                </div>

                <div class="product-meta-container">
                    <div class="item-location meta-single">
                        <p class="title">Item Location</p>
                        <div class="meta-details">
                            <p><i class="fas fa-location-arrow"></i><?php echo ($productInfo) ? ucfirst($sellerinfo->country) : ''; ?></p>
                        </div>
                    </div>

                    <div class="item-location meta-single">
                        <p class="title">Quantity</p>
                        <div class="meta-details">
                            <input type="number" name="quantity" id="qty2" min="1" max="<?php echo ($productInfo) ? $productInfo[0]->total_qty : 0;?>" value="1">
                        </div>
                    </div>
                </div>

                <div class="order-details-action-container"> <!-- checkout-btn-group checkout-links -->
                    <div class="action-bar">
                        <!-- <a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>"><i class="fas fa-store"></i></a> -->
                        <a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>"><img src="<?php echo base_url("assets/img/akin_icon.png");?>" width="18px"></a>
                        <?php if(!empty($storedata->contact_number)):?>
                        <a href="#" class="trackwp"><img src="<?php echo base_url('assets/img/whatsapp_icon.png')?>" width="18px"></a>
                        <?php endif;?>
                        <!-- <a class="<?php echo (empty($this->session->userdata('customerId'))) ? 'my_account_link' : '' ?>" href="<?php echo base_url()?>customer/profile"><i class="fas fa-user"></i></a> -->
                        <a id="addcart" href="#">Add to Cart</a>   
                        <a <?php echo ($totalcartno > 0) ? "href='".base_url('item/checkout/'.$sellerID)."'" : "href=#";?> id="order-confirm-anchor" class="directbuy">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    $this->session->set_userdata('previous_page', base_url(uri_string()));?>
    <!-- <script src="assets/js/jquery.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    
    <script type="text/javascript">
        $('#addcart').click(function() {
            fbq('track', 'AddToCart');
        });
        $('#addtocart').click(function() {
            fbq('track', 'AddToCart');
        });
        $('#order-confirm-anchor').click(function() {
            fbq('track', 'AddToCart');
        });
    </script>
    <!-- <script src="assets/js/main.js"></script> -->
    <script>
        $(window).on('load', function () {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 210,
                itemMargin: 5,
                asNavFor: '#slider'
            });
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            });
        });

        $("#qty").keyup(function(event) {
            if (event.keyCode === 13) {
                $("#addtocart").click();
            }
        });

        $('#order-confirm-popup .close-container a').on('click', function () {
            $('#order-confirm-popup').fadeOut();
            $('.details-overlay').fadeOut();
        });

        $('.my_account_link').on('click', function(e) {
            e.preventDefault();
            $('#login-popup').fadeIn();
            $('.details-overlay').fadeIn();
        });

        $('#sign-in-email').on('click', function(e) {
            e.preventDefault();
            $('#login-popup').fadeOut();
            $('#email-login-popup').fadeIn();
        });

        $('#login-popup .close-container a, #guest_checkout_link').on('click', function(e) {
            e.preventDefault();
            $('#login-popup').fadeOut();
            $('.details-overlay').fadeOut();
        });

        $('#email-login-popup .close-container a').on('click', function(e) {
            e.preventDefault();
            $('#email-login-popup').fadeOut();
            $('#login-popup').fadeIn();
        });

        var text = $('.text-overflow'),
          btn = $('.btn-overflow'),
          h = text[0].scrollHeight; 

        if(h > 30) {
            btn.addClass('less');
            btn.css('display', 'block');
        }

        btn.click(function(e) 
        {
          e.preventDefault();

          if (btn.hasClass('less')) {
              btn.removeClass('less');
              btn.addClass('more');
              btn.text('Show less');
              text.animate({'height': h});
          } else {
              btn.addClass('less');
              btn.removeClass('more');
              btn.text('Read more');
              text.animate({'height': '50px'});
          } 
        }); 

        $(document).on('click','.trackwp',function(e){
           var seller = "<?php echo $sellers ?>";

           $.ajax({                
            type : 'POST',
            url  : "<?php echo base_url('customer/trackusers') ?>",
            dataType: "json",
            data : {
                sellerID:seller
            },
            success: function(response){
                if(response.status == true){ // Nigeria country code
                    window.location.href = "https://wa.me/<?php echo $code?><?php echo $mobile?>";
                }
            }
            });
        });
    </script>

</body>

</html>
<?php
$this->load->model('Shopping_carts_model');

if(is_digit($this->uri->segment(3))){
    $category = $this->uri->segment(3);
}
else{
    $category = decrypt($this->uri->segment(3));
}

if(is_digit($this->uri->segment(4))){
    $sellers = $this->uri->segment(4);
}
else{
    $sellers = decrypt($this->uri->segment(4));
}

$sellerID = $this->uri->segment(4);

$sellerinfo = admin_info($sellers);
$maxprice = Maxprice($sellers);

$user = !empty($this->session->userdata("customerId")) ? $this->session->userdata("customerId") : $this->session->userdata("guest");

$cartcount = $this->Shopping_carts_model->where("user_id",$user)->get_all();
if($cartcount){
    $totalcartno = count($cartcount);
}
else{
    $totalcartno = 0;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jqueryui.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/devices.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/lightbox.min.css">
    <title><?php echo $title;?></title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121417530-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-121417530-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '241094959880535'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=241094959880535&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body id="category_product">
    <!-- DESKTOP VIEW START -->
    <div class="desktop-view">
    <div class="sidebar-menu">
        <div class="sidebar-menu--close--container text-right">
            <i id="menu_slide" class="fas fa-chevron-left"></i>
        </div>

        <div class="sidebar-menu--list--container">
            <ul class="sidebar-menu--list">
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>"><i class="fas fa-th"></i>Catalogue</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>"><i class="fas fa-shopping-cart"></i>Shopping Cart</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>item/viewstore/<?php echo $sellerID ?>"><i class="fas fa-info-circle"></i>Store Information</a>
                </li>
                <?php if($this->session->userdata('customerId')):?>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>item/orders"><i class="fa fa-history"></i>Order History</a>
                </li>
                <li class="sidebar-menu--list--item">
                    <a href="<?php echo base_url()?>customer/signOut"><i class="fas fa-sign-out-alt"></i>Logout</a>
                </li>
                <?php endif;?>
            </ul>
        </div>
    </div>

    <div id="popup-form"></div>
        <section id="page-title-section">
            <div class="container">
                 <div class="mobile-menu">
                    <a href="#" id="sidebar-open--anchor">
                        <i class="fa fa-bars"></i>
                    </a>
                </div> 
                <select id="main-nav--selector" class="main-nav--selector">
                        <option value="#">Go to</option>
                        <option data-fa="fas fa-th-large" value="<?php echo base_url()?>item/category/<?php echo $sellerID ?>"><a href="<?php echo base_url()?>item/category/<?php echo $sellerID ?>">Catalogue</a></option>
                        <option data-fa="fas fa-shopping-cart" value="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>"><a href="<?php echo base_url()?>item/manage_cart/<?php echo $sellerID ?>">Shopping Cart</a></option>
                        <option data-fa="fas fa-info-circle" value="<?php echo base_url()?>item/viewstore/<?php echo $sellerID ?>">Store Information</option>
                        <?php if($this->session->userdata('customerId')):?>
                        <option data-fa="fa fa-history" value="<?php echo base_url()?>item/orders">Order History</option>
                        <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                        <?php endif;?>
                    </select>
                <h1><?php echo $title;?></h1>
                 <a href="#">
                    <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
                </a>
            </div>
        </section>
        <?php if(count($categories) > 0){?>
        <section id="product-listing-section">
            <div class="container">
                <div class="slide-container">
                    <div id="slider" class="flexslider">
                        <ul class="slides">
                                <?php
                                foreach (@$categories as $key => $value) { 
                                	$final_link = is_digit($this->uri->segment(3)) ? $value->created_by.'/'.$value->product_id : encrypt($value->created_by).'/'.encrypt($value->product_id);//encrypt($value->created_by).'/'.encrypt($value->product_id);
                                    if(empty($value->product_images[0]->product_image)){
                                       $img_url = base_url('assets/img/no-p-image.png'); 
                                    }
                                    else{
                                     $img_url = base_url().'uploads/products/'.$value->product_images[0]->product_image;
                                    } 
                                    $newurl = base_url()."Image_lib.php?src=".$img_url."&w=200&h=200&q=50&strip=all";
                                ?>
                            <li>
                                <div class="product-single" data-title="<?php echo $value->product_name; ?>" data-content="<?php echo $value->product_description; ?>" data-url="<?php echo base_url(); ?>item/view/<?php echo $final_link; ?>">
                                    <img src="<?php echo $newurl; ?>" alt="">
                                    <div class="flex-caption">
                                        <p class="product-name"><?php echo $value->product_name;?></p>
                                        <a id="popup_btn" class="btn" href="<?php echo base_url() ?>item/view/<?php echo $final_link; ?>"style="padding: 12px 20px">View Product</a>
                                    </div>  
                                </div>
                            </li>
                        <?php   }  // end foreach ?>
                        </ul>
                    </div>
                </div>

            </div>
        </section>
        <?php   } else { ?>  <div class="container-1" style="border-top: 1px solid lightgray;"><p class="nocart" style="padding: 10px 95px;">* No products found.</p></div> <?php } ?>
    </div>
    <!-- DESKTOP VIEW END -->

    <!-- MOBILE VIEW START -->
    <!-- PRODUCT LISTING PAGE -->
    <div class="mobile-view">
        <?php 
            $order = isset($_GET['order']) ? $_GET['order'] : '';
            $pricemin = isset($_GET['min-amount'])? $_GET['min-amount'] : 0;
            $pricemax = isset($_GET['max-amount']) ? $_GET['max-amount'] : $maxprice[0]->maximumprice;
            $search = isset($_GET['search']) ? $_GET['search'] : '';
            $rating = isset($_GET['rating']) ? $_GET['rating'] : 0;
        ?>
        <!-- <div class="options-overlay"></div> -->
        <div class="navigator-head">
            <div class="container">
                <div class="left-head">
                    <?php if(!empty($search)):?>
                    <a href="<?php echo base_url(uri_string())?>"><i class="fa fa-chevron-left"></i>Back</a>    
                    <?php else:?>
                    <a href="<?php echo base_url("item/category/{$sellerID}")?>"><i class="fa fa-chevron-left"></i>Back</a>
                    <?php endif;?>
                </div>
                <div class="right-head navigator-right">
                    <a href="#" id="head-search" class="listing-anchor"><i class="fa fa-search"></i></a>
                    <div id="search-form">
                        <div class="form-wrap" style="display:flex; align-items:center">
                        <form action="">
                            <input type="text" id="search" name="search" placeholder="Search">
                            <input type="hidden" name="min-amount" value="<?php echo $pricemin;?>">
                            <input type="hidden" name="max-amount" value="<?php echo $pricemax;?>">
                            <input id="order" type="hidden" name="order" value="<?php echo $order;?>">
                            <input type="submit" style="display:none;">
                        </form>

                        <div class="close-container">
                            <a href="#" id="search-close"><i class="fas fa-times"></i></a>
                        </div>
                        </div>
                        
                    </div>
                    <a href="<?php echo base_url("item/manage_cart/{$sellerID}");?>" id="head-cart"><i class="fa fa-shopping-cart"></i><span class="cartedge"><?php echo $totalcartno; ?></span></a>
                   <!--  <a href="<?php echo base_url("item/manage_cart/{$sellerID}");?>" id="head-cart"><i class="fa fa-shopping-cart"></i></a> -->
                </div>
            </div>

        </div>
        <div class="product-listing-head">
            <div class="container">
                <div class="left-head">
                    <p><?php echo count($categories);?> products</p>
                </div>

                <div class="right-head listing-right">
                    <div class="sort-div">
                        <a class="listing-anchor <?php echo(isset($_GET['order'])) ? 'active-filter' : '';?>" href="#" id="head-sort"><i class="fas fa-exchange-alt"></i></a>
                        <div id="sort-options">
                            <ul>
                                <li class="<?php echo($order == "asc") ?'active-option' : '';?>"><a href="?order=asc&search=<?php echo $search?>"><i class="fa fa-sort-alpha-up"></i>Ascending</a></li> <!-- &min-amount=<?php echo $pricemin;?>&max-amount=<?php echo $pricemax;?> -->
                                <li class="<?php echo($order == "desc") ?'active-option' : '';?>"><a href="?order=desc&search=<?php echo $search?>"><i class="fa fa-sort-alpha-down"></i>Descending</a></li> <!-- &min-amount=<?php echo $pricemin;?>&max-amount=<?php echo $pricemax;?> -->
                            </ul>
                        </div>
                    </div>
                    <div class="filter-div">
                        <a class="listing-anchor <?php echo(isset($_GET['rating']) || isset($_GET['min-amount']) || isset($_GET['max-amount'])) ? 'active-filter' : '';?>" href="#" id="head-filter"><i class="fas fa-filter"></i></a>
                        <div id="filter-options">
                            <form action="<?php echo current_full_url();?>" method="GET" id="filter-form">
                                <ul>
                                    <li>

                                        <div class="slider-input-group">
                                            <span id="min-amount"><?php echo ($pricemin && !is_null($pricemin)) ? $pricemin : 0;?></span>
                                            <input id="min-amount-ip" type="hidden" name="min-amount" value="0">
                                            <span id="max-amount"><?php echo ($pricemax && !is_null($pricemax)) ? $pricemax : $maxprice[0]->maximumprice;?></span>
                                            <input id="max-amount-ip" type="hidden" name="max-amount" value="<?php echo ($pricemax && !is_null($pricemax)) ? $pricemax : $maxprice[0]->maximumprice;?>">
                                            <input id="order" type="hidden" name="order" value="<?php echo $order;?>">
                                            <input type="hidden" name="search" value="<?php echo $search;?>">
                                        </div>

                                        <div id="slider-range"></div>

                                        <input type="submit" value="Filter" class="btn" style="padding:5px 10px;">
                                        

                                    </li>

                                    <li>
                                        <a href="?order=<?php echo $order?>&min-amount=<?php echo $pricemin;?>&max-amount=<?php echo $pricemax;?>&search=<?php echo $search?>&rating=4">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>&nbsp;& above</a>
                                    </li>

                                    <li>
                                        <a href="?order=<?php echo $order?>&min-amount=<?php echo $pricemin;?>&max-amount=<?php echo $pricemax;?>&search=<?php echo $search?>&rating=3"><i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>&nbsp; & above</a>
                                    </li>

                                    <li>
                                        <a href="?order=<?php echo $order?>&min-amount=<?php echo $pricemin;?>&max-amount=<?php echo $pricemax;?>&search=<?php echo $search?>&rating=2"><i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>&nbsp;& above</a>
                                    </li>

                                    <li>
                                        <a href="?order=<?php echo $order?>&min-amount=<?php echo $pricemin;?>&max-amount=<?php echo $pricemax;?>&search=<?php echo $search?>&rating=1">
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>&nbsp;& above</a>
                                    </li>
                                </ul>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-grid">
            <div class="container">
            <?php

             if(count($categories) > 0) { 
                foreach (@$categories as $key => $value) { 
                $final_link = is_digit($this->uri->segment(3)) ? $value->created_by.'/'.$value->product_id : encrypt($value->created_by).'/'.encrypt($value->product_id);
                if(empty($value->product_images[0]->product_image)){
                   $img_url = base_url('assets/img/no-p-image.png'); 
                }
                else{
                    $img_url = base_url().'uploads/products/'.$value->product_images[0]->product_image;
                }
                $newurl = base_url()."Image_lib.php?src=".$img_url."&w=200&h=200&q=50&strip=all";
            ?>
                <div class="product-single">
                    <a href="<?php echo base_url(); ?>item/view/<?php echo $final_link; ?>">
                    <img src="<?php echo $newurl; ?>" width="200" height="200">
                    <div class="title">
                        <?php echo $value->product_name;?>
                    </div>
                    <div class="price">
                        <?php
                        if($sellerinfo->country == "thailand"){
                            echo "฿".$value->product_price;
                        }
                        else{
                            echo "₦".$value->product_price;
                        }
                        ?>
                    </div>
                    </a>
                </div>

            <?php } } else{ ?><p class="nocart" style="margin: 0 auto;text-align:center;"><i class="fas fa-exclamation-triangle fa-2x"></i><br>* No products found.</p><?php } ?>
            </div>
        </div>
    </div>
    <!-- MOBILE VIEW END -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/lightbox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/rater.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jqueryui.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/touchpunch.js"></script>
     <script>
        $(window).on('load', function () {
                    var slider = $('#carousel').flexslider({
                        selector: '.slides > li',
                        animation: "slide",
                        controlNav: false,
                        animationLoop: false,
                        slideshow: false,
                        itemWidth: 210,
                        itemMargin: 5,
                        asNavFor: '#slider'
                    });
                    $('#slider').flexslider({
                            animation: "slide",
                            controlNav: false,
                            animationLoop: false,
                            slideshow: false,
                            sync: "#carousel",
                            after: function (slider) {
                                var title = $('.flex-active-slide .product-single').data('title');
                                $('#product-title-main').html(title);

                                var content = $('.flex-active-slide .product-single').data('content');
                                $('#product-content-main').html(content);

                                var url = $('.flex-active-slide .product-single').data('url');
                                $('#product-url-main').attr('href',url);
                            },
                            start: function (slider) {
                                var title = $('.flex-active-slide .product-single').data('title');
                                $('#product-title-main').html(title);

                                var content = $('.flex-active-slide .product-single ').data('content'); 
                                $('#product-content-main').html(content);

                                var url = $('.flex-active-slide .product-single').data('url');
                                $('#product-url-main').attr('href',url) ;
                                }
                            });
                    });
    </script>
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#head-search').on('click', function () {
                $('#search-form').fadeToggle();
            });

            $('#search-close').on('click', function() {
                $('#search-form').fadeOut();
            });

            $('#head-search').on('click', function (event) {
                event.stopPropagation();
            });

            $('.listing-right').find('a.listing-anchor').each(function () {
                $(this).on('click', function (e) {
                    $(this).blur();
                    $(this).next().fadeToggle()
                    $(this).next().focus();
                    if($(this).next().attr('id') == 'sort-options') {
                        $('#filter-options').fadeOut();
                    } else if($(this).next().attr('id') == 'filter-options') {
                        $('#sort-options').fadeOut();
                    }
                })
            })

            $(function () {

                $("#slider-range").slider({
                    range: true,
                    min: 0,
                    max: <?php echo $maxprice[0]->maximumprice; ?>,
                    values: ["<?php echo $pricemin?>", "<?php echo ($pricemax) ? $pricemax : $maxprice[0]->maximumprice?>"],
                    slide: function (event, ui) {
                        $('#min-amount').text(ui.values[0]);
                        $('#min-amount-ip').val(ui.values[0]);
                        $('#max-amount').text(ui.values[1]);
                        $('#max-amount-ip').val(ui.values[1]);
                    }
                });
            });


            $("#filter-options ul").keydown(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    $("#filter-form").submit();
                }
            });
        })
    </script>
</body>

</html>
<?php 
$states = GetStatesByCountry('nigeria');
$this->load->model('Shopping_carts_model');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/mockup.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fa.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jqueryui.css">
    <title>
        <?php echo $title;?>
    </title>
    <style type="text/css">
        .mobile-view .dropdown {
            display: none;
        }

    </style>
</head>

<body id="my_account">
    <div class="details-overlay"></div>
    <!--   rinkal code for my account desktop design start-->
    <div class="desktop-view">

        <section id="page-title-section">
            <div class="container">
                <div class="mobile-menu">
                    <a href="#" id="sidebar-open--anchor">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <select id="main-nav--selector" class="main-nav--selector">
                        <option value="#">Go to</option>
                        <option data-fa="fas fa-sign-out-alt" value="<?php echo base_url()?>customer/signOut">Logout</option>
                    </select>
                <h1>
                    <?php echo $title; ?>
                </h1>
                <a href="#">
                    <img src="<?php echo base_url()?>assets/img/logo.png" alt="">
                </a>
            </div>
        </section>
        <section id="myaccount-section">
            <div class="container">
                <div class="profile-single">
                    <div class="profile-image">
                        <?php
                            if($profiledata->is_social == '0' && !empty($profiledata->admin_photo)){
                            ?>
                            <img src="<?php echo base_url("uploads/customers/{$profiledata->admin_photo}")?>" width="100">
                            <?php
                            }
                            else if($profiledata->is_social == '0' && empty($profiledata->admin_photo)){
                            ?>    
                                <img src="https://dummyimage.com/200x200/000/fff" alt="">
                            <?php 
                            }
                            else{
                            ?>
                                <img src="<?php echo $profiledata->login_picture?>" alt="">
                            <?php
                            }
                        ?>
                    </div>
                    <div class="profile-details">
                        <div class="name-container">
                            <h3>State: <?php echo ($profiledata->admin_state) ? $profiledata->admin_state : ""; ?></h3>
                        </div>
                        <div class="name-container">
                            <h3>Name: <?php echo ($profiledata->admin_name) ? $profiledata->admin_name : ""; ?></h3>
                        </div>
                        <div class="email-container">
                            <h3>Email: <?php echo ($profiledata->admin_email) ? $profiledata->admin_email : ""; ?></h3>
                        </div>
                        <div class="contact-container">
                            <h3>Contact: <?php echo ($profiledata->admin_mobile) ? $profiledata->admin_mobile : ""; ?></h3>
                        </div>
                        <div class="referral-container">
                            <h3>Referral Code: <?php echo ($profiledata->customer_referral_code) ? $profiledata->customer_referral_code : ""; ?></h3>
                        </div>
                        <a href="#" id="order-confirm-anchor-1" class="edit-btn">
                        <div class="fa fa-edit"></div>
                    </a>
                    </div>
                </div>
            </div>
            <div id="order-confirm-popup-1" class="modal-window">
            <div class="close-container profile-modal">
                <h3 class="edit-profile-title">Edit Profile</h3>
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>

            <div class="modal-content">
                <div class="order-meta-edit-profile" id="edit-profile">
                    <form id="profileform" method="post" action="<?php echo base_url("customer/profile ")?>" enctype="multipart/form-data">
                        <?php if($profiledata->is_social == '0'):?>
                        <input type="hidden" name="profileimage" value="<?php echo (!empty($profiledata->admin_photo)) ? $profiledata->admin_photo : '';?>">
                        <div class="form-group">
                            <input type="file" name="profile_photo" id="profile_photo">
                        </div>
                        <?php endif;?>
                        <div class="product-title form-group">
                            <input type="text" name="name" placeholder="Name" class="form-control" value="<?php echo ($profiledata->admin_name) ? $profiledata->admin_name : " "; ?>">
                        </div>
                        <div class="seller-title form-group">
                            <input type="email" name="email" placeholder="Email" class="form-control" value="<?php echo ($profiledata->admin_email) ? $profiledata->admin_email : " "; ?>">
                        </div>
                        <div class="product-amount form-group">
                            <input type="number" name="contact_number" placeholder="Contact No." class="form-control" value="<?php echo ($profiledata->admin_mobile) ? $profiledata->admin_mobile : " "; ?>">
                        </div>
                        <div class="product-amount form-group">
                            <select name="local_state_user">
                            <?php 
                           // $states = GetStatesByCountry('nigeria');
                            foreach ($states as $key => $value):?>
                                <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($profiledata->admin_state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                            <?php 
                            endforeach;
                            ?>
                            </select>
                        </div> 
                        <input type="submit" name="profilesave" id="profilesave" class="btn-save" value="Save">
                    </form>
                </div>
            </div>
            </div>
            <div class="container">
                <div class="bank-info-container">
                    <div class="bank-image">
                        <img src="https://dummyimage.com/200x200/000/fff" alt="">
                    </div>
                    <div class="bank-content">
                        <form action="<?php echo base_url('customer/saveBankDetails')?>" method="post" id="bankinfo">
                            <div class="form-group bank-field">
                                <label for="bankname">Bank Name</label>
                                <select name="bankname" id="bankname" class="form-control">
                                <option value="">List of Banks</option>
                                <?php foreach($banks as $bank) :?>
                                    <option value="<?php echo $bank->bank_code?>" <?php echo ($banksinfo && $banksinfo->bank_code == $bank->bank_code) ? "selected" : "";?>><?php echo $bank->bank_name?></option>
                                <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group bank-field">
                                <label for="account_number">Account Number</label>
                                <input type="text" placeholder="Account No." class="form-control" name="account_number" value="<?php echo ($banksinfo) ? $banksinfo->account_number : "";?>" id="account_number">
                            </div>
                            <div class="form-group bank-field">
                                <label for=""></label>
                                <button class="btn btn-save" type="submit">Save/Edit</button>
                                <button class="btn btn-cancel" type="reset">Cancel</button>
                            </div>
                       </form>
                    </div>

                </div>
            </div>
            <div class="container desktop-view">
                <div class="profile-order-link">
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                    <a href="<?php echo base_url()?>item/orders">My Orders</a>
                </div>
            </div>
            <div class="container desktop-view">
                <div class="profile-order-link">
                    <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                    <a <?php echo (count($carts) > 0) ? "href='".base_url('item/manage_cart/'.encrypt($carts[0]->created_by))."'" : '';?>>My Shopping</a>
                </div>
            </div>
            <div class="container order-details-container desktop-view">
                <form action="<?php echo base_url('customer/saveaddress');?>" method="post" name="caddressForm" id="caddressForm">
                <input type="hidden" name="address_ids" value="<?php echo ($addressdata) ? $addressdata[0]->address_id : "";?>">
                <div class="address-main">
                <div class="address-content">
                    <i class="fa fa-home" aria-hidden="true"></i> My Address
                </div>
                <!-- <div class="form-group">
                    <h3 class="label">Country</h3>
                    <select name="local_country" id="choosecountry">
                        <option value="">Choose Country</option>
                        <option value="nigeria" <?php echo ($addressdata && $addressdata[0]->country == "nigeria") ? "selected" : "";?>>Nigeria</option>
                        <option value="thailand" <?php echo ($addressdata && $addressdata[0]->country == "thailand") ? "selected" : "";?>>Thailand</option>
                     </select>
                </div> -->
                <div class="form-group">
                    <h3 class="label">State</h3>
                    <?php if ($addressdata): 
                       // $states = GetStatesByCountry('nigeria'); 
                        ?>
                    <select name="local_state" id="choosestate" data-country="nigeria">
                        <option value="">Choose State</option>  
                        <?php 
                        foreach ($states as $key => $value):?>
                            <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($addressdata[0]->state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                        <?php 
                        endforeach;
                        ?>
                        </select>
                        <?php else: ?>
                        <select name="local_state" id="choosestate" data-country="nigeria">
                        <option value="">Choose State</option>  
                        <?php 
                        //$states = GetStatesByCountry('nigeria');
                        foreach ($states as $key => $value):?>
                            <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>"><?php echo $value->state_name;?></option>
                        <?php 
                        endforeach;
                        ?>
                        <?php
                        endif;?>
                        </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Local GOV</h3>
                    <?php if ($addressdata): 
                        $areas = getDistrictByState($addressdata[0]->state,'nigeria');    
                        ?>
                    <select name="local_government" id="local_government">
                        <option value="">Choose Local Government</option>  
                        <?php 
                        foreach ($areas as $key => $value):?>
                            <option value="<?php echo str_replace(" ", "_", $value->local_area) ?>" <?php echo ($addressdata[0]->district == $value->local_area) ? "selected" : "";?>><?php echo $value->local_area;?></option>
                        <?php 
                        endforeach;
                        ?>
                        </select>
                        <?php else: ?>
                        <select name="local_government" id="local_government">
                        <option value="">Choose Local Government</option>
                        <?php
                        endif;?>
                        </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Address</h3>
                    <textarea id="custaddress" name="custaddress" maxlength="150"><?php echo ($addressdata ) ? $addressdata[0]->address : "";?></textarea>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="makedefault" id="default" value="1" class="checkbox-address" <?php echo ($addressdata && $addressdata[0]->make_default == '1') ? "checked" : "";?>/> <label for="default">Make it Default Address</label>
                </div>
                <div class="form-group">
                    <h3 class="label"></h3>
                    <input class="btn-save" type="submit" value="Save">
                    <input class="btn-cancel" type="reset" id="btnreset" value="Cancel">
                </div>
            </div>
                </form>
            </div>
             <div class="container order-details-container desktop-view">
                <form action="<?php echo base_url('customer/saveaddress');?>" method="post" name="caddressForm1" id="caddressForm1">
                <input type="hidden" name="address_ids" value="<?php echo ($addressdata) ? $addressdata[1]->address_id : "";?>">
               <div class="address-main">
                <div class="address-content">
                    <i class="fa fa-building" aria-hidden="true"></i> My 2nd Address
                </div>
                <!-- <div class="form-group">
                    <h3 class="label">Country</h3>
                    <select name="local_country" id="choosecountry1">
                        <option value="">Choose Country</option>
                        <option value="nigeria" <?php echo ($addressdata && $addressdata[1]->country == "nigeria") ? "selected" : "";?>>Nigeria</option>
                        <option value="thailand" <?php echo ($addressdata && $addressdata[1]->country == "thailand") ? "selected" : "";?>>Thailand</option>
                    </select>
                </div> -->
                <div class="form-group">
                    <h3 class="label">State</h3>
                    <?php if ($addressdata): 
                    //$states = GetStatesByCountry('nigeria'); 
                    ?>
                    <select name="local_state" id="choosestate1" data-country="nigeria">
                    <option value="">Choose State</option>  
                    <?php 
                    //$states = GetStatesByCountry('nigeria');
                    foreach ($states as $key => $value):?>
                        <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($addressdata[1]->state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                    <?php 
                    endforeach;
                    ?>
                    </select>
                    <?php else: ?>
                    <select name="local_state" id="choosestate1" data-country="nigeria">
                    <option value="">Choose State</option>  
                    <?php 
                    //$states = GetStatesByCountry('nigeria');
                    foreach ($states as $key => $value):?>
                        <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>"><?php echo $value->state_name;?></option>
                    <?php 
                    endforeach;
                    ?>
                    <?php
                    endif;?>
                    </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Local GOV</h3>
                    <?php if ($addressdata): 
                    $areas = getDistrictByState($addressdata[1]->state,'nigeria');    
                    ?>
                    <select name="local_government" id="chooseaddresarea1">
                    <option value="">Choose Local Government</option>  
                    <?php 
                    foreach ($areas as $key => $value):?>
                        <option value="<?php echo str_replace(" ", "_", $value->local_area) ?>" <?php echo ($addressdata[1]->district == $value->local_area) ? "selected" : "";?>><?php echo $value->local_area;?></option>
                    <?php 
                    endforeach;
                    ?>
                    </select>
                    <?php else: ?>
                    <select name="local_government" id="chooseaddresarea1">
                    <option value="">Choose Local Government</option>
                    <?php
                    endif;?>
                    </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Address</h3>
                    <textarea id="custaddress" name="custaddress" maxlength="150"><?php echo ($addressdata ) ? $addressdata[1]->address : "";?></textarea>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="makedefault" id="default1" value="1" class="checkbox-address" <?php echo ($addressdata && $addressdata[1]->make_default == '1') ? "checked" : "";?>/> <label for="default1">Make it Default Address</label>
                </div>
                <div class="form-group">
                    <h3 class="label"></h3>
                    <input class="btn-save" type="submit" value="Save">
                    <input class="btn-cancel" type="reset" id="btnreset1" value="Cancel">
                </div>
            </div>
                 </form>
            </div>
        </section>
    </div>
    <!--   rinkal code for my account desktop design end-->
    <!--   rinkal code for my account mobile design start-->
    <div class="mobile-view">

        <div class="navigator-head">
            <div class="container">
                <div class="left-head">
                    <strong><?php echo ($profiledata->admin_name) ? $profiledata->admin_name : $title; ?></strong>
                </div>
                <div class="right-head">
                    <a href="#" id="order-confirm-anchor" class="edit-btn">
                        <div class="fa fa-edit"></div>
                    </a>
                    <a href="<?php echo base_url('customer/signOut');?>" class="logout-link">
                    <i class="fa fa-power-off"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="order-info-container">

            <div class="order-listing">

                <div class="order-single border-none">

                    <div class="order-details">
                        <div class="order-image">
                            <?php
                                if($profiledata->is_social == '0' && !empty($profiledata->admin_photo)){
                                ?>
                                <img src="<?php echo base_url("uploads/customers/{$profiledata->admin_photo}")?>" width="100">
                                <?php
                                }
                                else if($profiledata->is_social == '0' && empty($profiledata->admin_photo)){
                                ?>    
                                    <img src="https://dummyimage.com/200x200/000/fff" alt="">
                                <?php 
                                }
                                else{
                                ?>
                                    <img src="<?php echo $profiledata->login_picture?>" alt="">
                                <?php
                                }
                            ?>

                        </div>

                        <div class="order-meta" id="profile-content">
                            <div class="product-amount">
                                <p>State:
                                    <?php echo ($profiledata->admin_state) ? $profiledata->admin_state : ""; ?>
                                </p>
                            </div>
                            <!-- <div class="product-title">
                                <p>Name:
                                    <?php echo ($profiledata->admin_name) ? $profiledata->admin_name : ""; ?>
                                </p>
                            </div> -->
                            <div class="product-amount">
                                <p>Email:
                                    <?php echo ($profiledata->admin_email) ? $profiledata->admin_email : ""; ?>
                                </p>
                            </div>
                            <div class="product-amount">
                                <p>Phone:
                                    <?php echo ($profiledata->admin_mobile) ? $profiledata->admin_mobile : ""; ?>
                                </p>
                            </div>
                            <div class="product-quantity">
                                <p>Referal Code:
                                    <?php echo $profiledata->customer_referral_code;?>
                                </p>
                            </div>
                            <?php 
                                $owned_imagesurl = array('http://pbs','http://abs');
                            ?>
                            <div class="social-bar m-t-10">
                                <span class="<?php echo ($profiledata->is_social == '1' && strpos($profiledata->login_picture, 'https://graph') !== false) ? 'logged-in-social' : ''?>"><i class="fab fa-facebook"></i></span>
                                <span class="<?php echo ($profiledata->is_social == '0' && $profiledata->login_with == 'customer') ? 'logged-in-social' : ''?>"><i class="fa fa-envelope"></i></span>
                                <span class="<?php echo ($profiledata->is_social == '1' && match($owned_imagesurl, $profiledata->login_picture)) ? 'logged-in-social' : ''?>"><i class="fab fa-twitter"></i></span>
                            </div>
                        </div>
                        <div id="order-confirm-popup" class="modal-window">
                            <div class="close-container profile-modal">
                                <h3 class="edit-profile-title">Edit Profile</h3>
                                <a href="#">
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>

                            <div class="modal-content">
                                <div class="order-meta-edit-profile" id="edit-profile">
                                    <form id="profileform" method="post" action="<?php echo base_url("customer/profile ")?>" enctype="multipart/form-data">
                                        <?php if($profiledata->is_social == '0'):?>
                                        <input type="hidden" name="profileimage" value="<?php echo (!empty($profiledata->admin_photo)) ? $profiledata->admin_photo : '';?>">
                                        <div class="form-group">
                                            <input type="file" name="profile_photo" id="profile_photo">
                                        </div>
                                        <?php endif;?>
                                        <div class="product-title form-group">
                                            <input type="text" name="name" placeholder="Name" class="form-control" value="<?php echo ($profiledata->admin_name) ? $profiledata->admin_name : " "; ?>">
                                        </div>
                                        <div class="seller-title form-group">
                                            <input type="email" name="email" placeholder="Email" class="form-control" value="<?php echo ($profiledata->admin_email) ? $profiledata->admin_email : " "; ?>">
                                        </div>
                                        <div class="product-amount form-group">
                                            <input type="number" name="contact_number" placeholder="Contact No." class="form-control" value="<?php echo ($profiledata->admin_mobile) ? $profiledata->admin_mobile : " "; ?>">
                                        </div>
                                        <div class="product-amount form-group">
                                            <select name="local_state_user">
                                            <?php 
                                            //$states = GetStatesByCountry('nigeria');
                                            foreach ($states as $key => $value):?>
                                                <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($profiledata->admin_state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                                            <?php 
                                            endforeach;
                                            ?>
                                            </select>
                                        </div>
                                        <input type="submit" name="profilesave" id="profilesave" class="btn-save" value="Save">
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="profile-order-link">
            <!-- <i class="fa fa-shopping-basket" aria-hidden="true" style="color: #fbbc05;"></i> -->
            <img src="<?php echo base_url('assets/img/order_icon.png')?>">
            <a href="<?php echo base_url()?>item/orders">My Orders</a>
        </div>

        <div class="profile-order-link">
            <img src="<?php echo base_url('assets/img/cart_icon.png')?>">
            <!-- <i class="fa fa-cart-arrow-down" aria-hidden="true" style="color: #4285f4;"> --><span class="cartedge" style="font-size: 12px;margin-left: -10px;margin-top: -22px;"><?php echo count($carts); ?></span>
            <a <?php echo (count($carts) > 0) ? "href='".base_url('item/manage_cart/'.encrypt($carts[0]->created_by))."'" : '';?>>My Shopping <!-- (<?php echo count($carts);?>) --></a>
        </div>

        <form action="<?php echo base_url('customer/saveaddress');?>" method="post" name="caddressForm" id="caddressForm">
            <input type="hidden" name="address_ids" value="<?php echo ($addressdata) ? $addressdata[0]->address_id : "";?>">
            <div class="address-main">
                <div class="address-content">
                    <img src="<?php echo base_url('assets/img/home_icon.png')?>">
                    <!-- <i class="fa fa-home" aria-hidden="true" style="color: #34a853;"></i> --> My Address
                </div>
               <!--  <div class="form-group">
                    <h3 class="label">Country</h3>
                    <select name="local_country" id="choosecountry2">
                        <option value="">Choose Country</option>
                        <option value="nigeria" <?php echo ($addressdata && $addressdata[0]->country == "nigeria") ? "selected" : "";?>>Nigeria</option>
                        <option value="thailand" <?php echo ($addressdata && $addressdata[0]->country == "thailand") ? "selected" : "";?>>Thailand</option>
                     </select>
                </div> -->
                <div class="form-group">
                    <h3 class="label">State</h3>
                    <?php if ($addressdata): 
                        //$states = GetStatesByCountry('nigeria'); 
                        ?>
                    <select name="local_state" id="choosestate2" data-country="nigeria">
                        <option value="">Choose State</option>  
                        <?php 
                        foreach ($states as $key => $value):?>
                            <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($addressdata[0]->state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                        <?php 
                        endforeach;
                        ?>
                        </select>
                        <?php else: ?>
                        <select name="local_state" id="choosestate2" data-country="nigeria">
                        <option value="">Choose State</option>  
                        <?php 
                        //$states = GetStatesByCountry('nigeria');
                        foreach ($states as $key => $value):?>
                            <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>"><?php echo $value->state_name;?></option>
                        <?php 
                        endforeach;
                        ?>
                        </select>
                        <?php
                        endif;?>
                        </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Local GOV</h3>
                    <?php if ($addressdata): 
                        $areas = getDistrictByState($addressdata[0]->state,'nigeria');    
                        ?>
                    <select name="local_government" id="local_government2">
                        <option value="">Choose Local Government</option>  
                        <?php 
                        foreach ($areas as $key => $value):?>
                            <option value="<?php echo str_replace(" ", "_", $value->local_area) ?>" <?php echo ($addressdata[0]->district == $value->local_area) ? "selected" : "";?>><?php echo $value->local_area;?></option>
                        <?php 
                        endforeach;
                        ?>
                        </select>
                        <?php else: ?>
                        <select name="local_government" id="local_government2">
                        <option value="">Choose Local Government</option>
                        <?php
                        endif;?>
                        </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Address</h3>
                    <textarea id="custaddress" name="custaddress" maxlength="150"><?php echo ($addressdata ) ? $addressdata[0]->address : "";?></textarea>
                </div>
                <div class="form-group">
                     <h3 class="label"></h3>
                    <input type="checkbox" name="makedefault" id="default" value="1" class="checkbox-address" <?php echo ($addressdata && $addressdata[0]->make_default == '1') ? "checked" : "";?>/> <label for="default">Make it Default Address</label>
                </div>
                <div class="form-group">
                    <h3 class="label"></h3>
                    <input class="btn-save" type="submit" value="Save">
                    <input class="btn-cancel" type="reset" id="btnreset" value="Cancel">
                </div>
            </div>
        </form>

        <form action="<?php echo base_url('customer/saveaddress');?>" method="post" name="caddressForm1" id="caddressForm1">
            <input type="hidden" name="address_ids" value="<?php echo ($addressdata) ? $addressdata[1]->address_id : "";?>">
            <div class="address-main">
                <div class="address-content">
                    <!-- <i class="fa fa-building" aria-hidden="true" style="color: #34a853;"></i> --><img src="<?php echo base_url('assets/img/address2_icon.png')?>"> My 2nd Address
                </div>
                <!-- <div class="form-group">
                    <h3 class="label">Country</h3>
                    <select name="local_country" id="choosecountry11">
                        <option value="">Choose Country</option>
                        <option value="nigeria" <?php echo ($addressdata && $addressdata[1]->country == "nigeria") ? "selected" : "";?>>Nigeria</option>
                        <option value="thailand" <?php echo ($addressdata && $addressdata[1]->country == "thailand") ? "selected" : "";?>>Thailand</option>
                    </select>
                </div> -->
                <div class="form-group">
                    <h3 class="label">State</h3>
                    <?php if ($addressdata): 
                    //$states = GetStatesByCountry('nigeria'); 
                    ?>
                    <select name="local_state" id="choosestate11" data-country="nigeria">
                    <option value="">Choose State</option>  
                    <?php 
                    foreach ($states as $key => $value):?>
                        <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($addressdata[1]->state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                    <?php 
                    endforeach;
                    ?>
                    </select>
                    <?php else: ?>
                    <select name="local_state" id="choosestate11" data-country="nigeria">
                    <option value="">Choose State</option>  
                    <?php 
                   // $states = GetStatesByCountry('nigeria');
                    foreach ($states as $key => $value):?>
                        <option value="<?php echo str_replace(" ", "_", $value->state_name) ?>" <?php echo ($addressdata[1]->state == $value->state_name) ? "selected" : "";?>><?php echo $value->state_name;?></option>
                    <?php 
                    endforeach;
                    ?>
                    <?php
                    endif;?>
                    </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Local GOV</h3>
                    <?php if ($addressdata): 
                    $areas = getDistrictByState($addressdata[1]->state,'nigeria');    
                    ?>
                    <select name="local_government" id="chooseaddresarea11">
                    <option value="">Choose Local Government</option>  
                    <?php 
                    foreach ($areas as $key => $value):?>
                        <option value="<?php echo str_replace(" ", "_", $value->local_area) ?>" <?php echo ($addressdata[1]->district == $value->local_area) ? "selected" : "";?>><?php echo $value->local_area;?></option>
                    <?php 
                    endforeach;
                    ?>
                    </select>
                    <?php else: ?>
                    <select name="local_government" id="chooseaddresarea11">
                    <option value="">Choose Local Government</option>
                    <?php
                    endif;?>
                    </select>
                </div>
                <div class="form-group">
                    <h3 class="label">Address</h3>
                    <textarea id="custaddress" name="custaddress" maxlength="150"><?php echo ($addressdata ) ? $addressdata[1]->address : "";?></textarea>
                </div>
                <div class="form-group">
                    <h3 class="label"></h3>
                    <input type="checkbox" name="makedefault" id="default1" value="1" class="checkbox-address" <?php echo ($addressdata && $addressdata[1]->make_default == '1') ? "checked" : "";?>/> <label for="default1">Make it Default Address</label>
                </div>
                <div class="form-group">
                    <h3 class="label"></h3>
                    <input class="btn-save" type="submit" value="Save">
                    <input class="btn-cancel" type="reset" id="btnreset1" value="Cancel">
                </div>
            </div>
        </form>
    </div>
    <!--   rinkal code for my account mobile design end-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--<script src="<?php echo base_url()?>assets/js/main.js"></script>-->
    <script src="<?php echo base_url()?>assets/build/js/site.js"></script>
    <script type="application/javascript">
        $('#order-confirm-anchor').on('click', function(e) {
            e.preventDefault();
            $('#order-confirm-popup').fadeIn();
            $('.details-overlay').fadeIn();

            $('#order-confirm-popup .close-container a').on('click', function() {
                $('#order-confirm-popup').fadeOut();
                $('.details-overlay').fadeOut();
            });
        });

         $('#order-confirm-anchor-1').on('click', function(e) {
            e.preventDefault();
            $('#order-confirm-popup-1').fadeIn();
            $('.details-overlay').fadeIn();

            $('#order-confirm-popup-1 .close-container a').on('click', function() {
                $('#order-confirm-popup-1').fadeOut();
                $('.details-overlay').fadeOut();
            });
        });

        $(function() {
            var checkboxes;
            checkboxes = $('input[name^=makedefault]').change(function() {
                if (this.checked) {
                    checkboxes.not(this).prop('checked', false);
                }
            });
        });

    </script>
</body>

</html>

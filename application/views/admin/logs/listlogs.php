<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
	              <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                  </button>
	                    <?php echo $this->session->flashdata('flashmsg'); ?>
	              </div>
	              <?php endif; ?>
               
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">

                        <div class="col-sm-12">
                        <div class="pull-right">
                        <a href="<?php echo base_url(); ?>logs/export_excel" class="btn btn-success">Export Excel</a>
                        </div>
                          <table id="table-logs" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            	<th>#</th>
                            	<!-- <th>Error No</th> -->
                            	<th>Error Type</th>
                            	<th>Error</th>
                              	<th>File Path</th>
                              	<th>Line No</th>
                            	<th>User Agent</th>
                            	<th>IP Address</th>
                            	<th>Time</th>
                            </thead>
	                      	<tbody> 
                       
	                       </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>

<script type="text/javascript">

$(document).ready(function() {
    //datatables
    var table = $('#table-logs').DataTable({ 
 
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('logs/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {              
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });
 
   
 
});
 
</script>
<?php 
  $this->load->model('Chatbots_model'); 
  $chatdetails = $this->Chatbots_model->getChatData();
  if($chatdetails){
    $businesshours = unserialize($chatdetails->business_hours);
    if($businesshours){  
      $exptimes = implode("-", $businesshours);
    }
    $providers = unserialize($chatdetails->service_provides);
    $offerdays = unserialize($chatdetails->offer_days);
    $apikeys = unserialize($chatdetails->social_apikeys);
    $socials = unserialize($chatdetails->social_info);
    $banks = unserialize($chatdetails->bank_info);
    $stateschecked = unserialize($chatdetails->offer_states);
  }
  else{
    $businesshours = array();
  }
 
  $last = $this->db->order_by('id','desc')
        ->where('created_by',$this->session->userdata('adminId'))
        ->limit(1)
        ->get('subscription_orders')
        ->row();
  if($last){
    $dt = new DateTime($last->created_date);
    $registereddate = $dt->format('Y-m-d');
    $afteronemonth = get_month_diff($registereddate,false);
  } 
  else{
    $dt = new DateTime($profileinfo->created_date);
    $registereddate = $dt->format('Y-m-d');
    $afteronemonth = get_month_diff($registereddate,false);
  }   
  $info = admin_info($this->session->userdata('adminId')); 

  $rightnow = date($info->created_date);
  $add7days = date('Y-m-d', strtotime("$rightnow +7 days"));
  // States list by country of seller 
  if(!is_null($info->country)){
    $states = GetStatesByCountry($info->country);
  }

  $socialac = explode(",", $info->connected_with);

  $this->db->where_in('admin_id',$socialac);
  $accounts = $this->Admin_model->get_all();
  $conn = [];
  if($accounts){
    foreach ($accounts as $connection) {
      $conn[] = $connection->login_with;
    } 
  }
  $banklists = getbanks();
  if(!empty($this->session->userdata('isLogin'))){
    $codes = explode("-", $chatdetails->contact_number);
  }
  else{
    $codes = array("");
  }
?>
<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">

                    <h2><?php echo $title ?></h2>

                    <div class="clearfix"></div>

                  </div>

                  <?php //$msg = $this->uri->segment_array(); 

                  ?>

                  <div class="x_content">
                    <input type="hidden" id="sellercountry" value="<?php echo $info->country;?>">
                   <input type="hidden" id="sellercreateddate" value="<?php echo date('Y-m-d',strtotime($rightnow));?>">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>

                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">

                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>

                      </button>

                        <?php echo $this->session->flashdata('flashmsg'); ?>

                      </div>

                    <?php endif; ?>

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">

                        <ul id="myTab" class="nav nav-tabs bar_tabs mobile-tabs" role="tablist">

                            <!-- <?php 
                            if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isFacebook') == false):
                           
                            ?>
                             <li role="presentation" class="active"><a href="#tab_content7" role="tab" id="social-tab" data-toggle="tab" aria-expanded="false">Chatbot</a>
                            </li>
                           <?php endif;?>  -->
                            <!-- <?php echo ($chatdetails == false && $this->session->userdata('isFacebook') == true || empty($this->session->userdata('isLogin'))) ? 'active' : ''; ?> -->
                            <li role="presentation" class="active" id="steps1"><a href="#tab_content1" id="profile-tab" role="tab" data-toggle="tab" aria-expanded="false">Profile</a>
                            </li>
                           <!-- && $chatdetails == true -->
                           <?php if(!empty($this->session->userdata('isLogin')) && !empty($info->country)):
                            ?>
                             <li role="presentation" class="<?php echo($this->session->userdata('isFacebook') == true) ? 'active in' : ''?>"><a href="#tab_content6" role="tab" id="payment-tab" data-toggle="tab" aria-expanded="false">Payments & Billing</a>
                            </li>
                           <?php endif;?> 
                       
                          <?php if(is_null($page) && empty($this->session->userdata('isLogin'))):?>
                            <li role="presentation"><a href="#tab_content2" role="tab" id="smtp-tab" data-toggle="tab" aria-expanded="true">SMTP Configuration</a>
                          </li>
                          <?php endif; ?>

                          <?php if(!empty($this->session->userdata('isLogin')))://&& $chatdetails == true?>
                            <li role="presentation" class="" id="store_info"><a href="#tab_content3" role="tab" id="store-tab" data-toggle="tab" aria-expanded="false">Store Information</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="channel-tab" data-toggle="tab" aria-expanded="false">Channels</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content5" role="tab" id="delivery-tab" data-toggle="tab" aria-expanded="false">Delivery and Return</a>
                            </li>
                        <?php endif; ?>
                        </ul>

                        <div id="myTabContent" class="tab-content">
                          <!-- 
                          <?php echo($this->session->userdata('isFacebook') == false && !empty($this->session->userdata('isLogin'))) ? 'active in' : ''?>
                          -->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content7" aria-labelledby="smtp-tab">
                           <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                  <img src="<?php echo base_url()?>assets/img/fbsymbol.png" alt="" width="150">
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                  <p><?php echo strtoupper("connect out store bot to your facebook page");?></p>
                                  <p>In line with recent privacy updates on Facebook, you need to give us permission to setup this chatbot on your Facebook Page. Here is what you need to do</p>
                                  <p>(1) Connect your facebook page &nbsp;&nbsp;
                                    <?php //var_dump($conn); ?>
                                    <?php if(in_array('facebook',$conn)) : ?>
                                    <button disabled class="btn btn-xs btn-info" style="display: block;margin-top: 40px;">Connected</button>
                                      <?php else :?>
                                    <a class="btn btn-xs btn-primary" href="<?php echo base_url()?>user/ConnectProcess/facebook" <?php echo ($info->login_with == "facebook") ? "visibility:hidden;" : "";?>">Connect</a>
                                    <?php endif;?>  
                                    <br><br>
                                    See this video for step (2) to (6) &nbsp;&nbsp;<a class="btn btn-xs btn-primary" data-toggle="modal" data-target=".bs-pageadmin-modal-lg"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                                  </p>
                                  <p>(2) Go to your Web Browser anmd login to Facebook -> www.facebook.com</p>
                                  <p>(3) Go to your Facebook Page</p>
                                  <p>(4) Go to Settings in the Top right corner</p>
                                  <p>(5) Go to Page Roles</p>
                                  <p>(6) Add Akin AI as an admin to your Facebook Page</p>
                                  <p>(7) Click on Test Connection</p>
                                </div>  
                           </div>
                          <div class="ln_solid"></div>
                          </div>  
                          <?php
                          $class = "";
                          if(empty($this->session->userdata('isLogin'))){
                            $class = "active in";
                          }
                          else if(!empty($this->session->userdata('isLogin')) && !empty($this->session->userdata('isFacebook')) && $chatdetails == false){
                            $class = "active in";
                          }
                          else if(!empty($this->session->userdata('isLogin')) && empty($this->session->userdata('isFacebook'))){
                            $class = "active in"; // blank
                          }
                          ?>
                          <!--  <?php echo $class;?> -->
                          <div role="tabpanel" class="tab-pane fade <?php echo $class?>" id="tab_content1" aria-labelledby="profile-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'admin/profile/'.$profileinfo->admin_id?>"  method="post" enctype="multipart/form-data">

                              <div class="item form-group" id="pf1">


                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_name">Name <span class="required"></span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input id="admin_name" class="form-control col-md-7 col-xs-12" name="admin_name" value="<?php echo($profileinfo->admin_name ? $profileinfo->admin_name : '') ?>" type="text">

                                  <?php echo form_error('admin_name','<span class="help-block">','</span>'); ?>

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_email">Email <span class="required">*</span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="email" id="admin_email" name="admin_email" class="form-control col-md-7 col-xs-12" value="<?php echo($profileinfo->admin_email ? $profileinfo->admin_email : '') ?>" required="required">

                                  <?php echo form_error('admin_email','<span class="help-block">','</span>'); ?>

                                </div>

                              </div>

                              <div class="item form-group" id="pf2">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile_no">Mobile Number

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="number" id="mobile_no" name="mobile_no" class="form-control col-md-7 col-xs-12"  value="<?php echo($profileinfo->admin_mobile ? $profileinfo->admin_mobile : '') ?>">

                                  <?php echo form_error('admin_email','<span class="help-block">','</span>'); ?>

                                </div>

                              </div>
                               <?php if(!empty($this->session->userdata('isLogin'))):?>
                              <div class="item form-group" id="pf3">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile_no">Country
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <?php if(empty($info->country)):?>
                                  <select class="form-control" name="country" required>
                                    <option value="">-Select Country-</option>
                                    <option value="thailand" <?php echo($info->country == "thailand") ? "selected=selected" : ""?>>Thailand</option>
                                    <option value="nigeria" <?php echo($info->country == "nigeria") ? "selected=selected" : ""?>>Nigeria</option>
                                  </select>
                                  <?php else :?>
                                  <select class="form-control" name="country" required readonly>
                                    <option value="<?php echo $info->country; ?>" selected><?php echo ucfirst($info->country); ?></option>
                                  </select> 
                                <?php endif;?>
                                </div>
                              </div>
                            <?php endif;?>
                              <?php if(empty($this->session->userdata('isLogin')) || $this->session->userdata('isLogin') == 'seller'):?>
                              <div class="item form-group" id="pf4">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_photo">Profile Photo

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="file" id="profile_photo" name="profile_photo" class="form-control col-md-7 col-xs-12">

                                    <div class="profile_img">

                                        <div class="crop-avatar">

                                        <!-- Current avatar -->

                                          <img class="img-responsive avatar-view" src="<?php echo base_url().'uploads/'.$profileinfo->admin_photo ?>"  width="100" height="100">

                                        </div>

                                        <input type="hidden" name="existimage" value="<?php echo($profileinfo->admin_photo ? $profileinfo->admin_photo : '') ?>">

                                    </div>

                                </div>

                              </div>
                               <?php endif; ?>
                               <?php if(!empty($this->session->userdata('isLogin'))):?>
                                <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_photo">Referral Code
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="referral_code" class="form-control col-md-7 col-xs-12" name="referral_code" value="<?php echo($profileinfo->match_referral_code ? $profileinfo->match_referral_code : '') ?>" type="text" <?php echo($profileinfo->match_referral_code) ? "disabled=true" : ''; ?>>
                                </div>
                                <input type="hidden" id="lastdate" value="<?php echo $add7days?>">
                              </div>
                              <?php endif; ?> 
                               <?php if(!is_null($page) && $page = "652803498256943"):?>
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="page_events">Enable page events ?
                                  </label>
                                  <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="radio">
                                      <label>
                                        <div><input type="radio" class="flat" value="1" name="page_events" id="page_events" <?php echo ($profileinfo->page_events == 1) ? "checked" : "";?>></div> On
                                      </label>
                                      <label>
                                        <div><input type="radio" class="flat" value="0" name="page_events" id="page_events" <?php echo ($profileinfo->page_events == 0) ? "checked" : "";?>></div> Off
                                    </label>
                                  </div>
                                  </div>
                                </div>
                              <?php endif;?>
                              <div class="ln_solid"></div>

                              <div class="form-group">

                                <div class="col-md-6 col-md-offset-3">

                                  <input type="submit" name="profilepost" id="profilepost1" value="Save" class="btn btn-success">

                                </div>

                              </div>

                            </form>  
                          </div>
                          <?php if(is_null($page) && empty($this->session->userdata('isLogin'))):
                          ?>  
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="smtp-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'admin/mailConfiguration'?>" method="post">

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="protocol">Protocol <span class="required"></span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input id="protocol" class="form-control col-md-7 col-xs-12" name="protocol" value="<?php echo($configurations['protocol'] ? $configurations['protocol'] : '') ?>" type="text">

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_host">Host <span class="required"></span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="text" id="smtp_host" name="smtp_host" class="form-control col-md-7 col-xs-12" value="<?php echo($configurations['smtp_host'] ? $configurations['smtp_host'] : '') ?>" required="required">

                                  

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_pass">Password

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="password" id="smtp_pass" name="smtp_pass" class="form-control col-md-7 col-xs-12"  value="<?php echo($configurations['smtp_pass'] ? $configurations['smtp_pass'] : '') ?>">

                                  

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_port">Port

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="number" id="smtp_port" name="smtp_port" class="form-control col-md-7 col-xs-12"  value="<?php echo($configurations['smtp_port'] ? $configurations['smtp_port'] : '') ?>">

                                 

                                </div>

                              </div>

                               <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_user">Username

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="text" id="smtp_user" name="smtp_user" class="form-control col-md-7 col-xs-12"  value="<?php echo($configurations['smtp_user'] ? $configurations['smtp_user'] : '') ?>">

                                  

                                </div>

                              </div>

                              <div class="ln_solid"></div>

                              <div class="form-group">

                                <div class="col-md-6 col-md-offset-3">

                                  <input type="submit" name="mailpost" id="mailpost" value="Save" class="btn btn-success">

                                </div>

                              </div>
                          </div>
                          <?php endif; ?>
                          <!-- && $chatdetails == true $chatdetails->id -->
                          <?php if(!empty($this->session->userdata('isLogin'))):?>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="store-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                                <div class="item form-group" id="steps3">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Opening and Closing Time <span class="required"></span>
                                  </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="businesshours" id="businesshours">
                                        <option value="alltime" <?php echo(!empty($chatdetails->store_open) && $chatdetails->store_open == "alltime") ? "selected" : ""; ?>>24 hours</option>
                                        <option value="custom" <?php echo(!empty($chatdetails->store_open) && $chatdetails->store_open == "custom") ? "selected" : ""; ?>>Custom</option>
                                    </select>
                                  </div>
                                </div>
                              <div class="item form-group" id="times" style="<?php echo(!empty($chatdetails->store_open) && $chatdetails->store_open == "custom") ? "display:bock" : "display:none"; ?>">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                   <div class="range-day" id="range-day-1" data-day="1">
                                      <input type="checkbox" name="day-1" id="day-1" value="Mon" class="range-checkbox" <?php if($businesshours):if(in_array("Mon-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-1" class="range-label">Monday:</label>
                                      <div id="range-slider-1" class="range-slider1"></div>
                                      <span id="range-time-1" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-2" data-day="2">
                                      <input type="checkbox" name="day-2" id="day-2" value="Tue" class="range-checkbox" <?php if($businesshours):if(in_array("Tue-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-2" class="range-label">Tuesday:</label>
                                      <div id="range-slider-2" class="range-slider2"></div>
                                      <span id="range-time-2" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-3" data-day="3">
                                      <input type="checkbox" name="day-3" id="day-3" value="Wed" class="range-checkbox" <?php if($businesshours):if(in_array("Wed-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-3" class="range-label">Wednesday:</label>  
                                      <div id="range-slider-3" class="range-slider3"></div>
                                      <span id="range-time-3" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-4" data-day="4">
                                      <input type="checkbox" name="day-4" id="day-4" value="Thu" class="range-checkbox" <?php if($businesshours):if(in_array("Thu-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-4" class="range-label">Thursday‎:</label>  
                                      <div id="range-slider-4" class="range-slider4"></div>
                                      <span id="range-time-4" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-5" data-day="5">
                                      <input type="checkbox" name="day-5" id="day-5" value="Fri" class="range-checkbox" <?php if($businesshours):if(in_array("Fri-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-5" class="range-label">Friday:</label>  
                                      <div id="range-slider-5" class="range-slider5"></div>
                                      <span id="range-time-5" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-6" data-day="6">
                                      <input type="checkbox" name="day-6" id="day-6" value="Sat" class="range-checkbox" <?php if($businesshours):if(in_array("Sat-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-6" class="range-label">Saturday:</label>  
                                      <div id="range-slider-6" class="range-slider6"></div>
                                      <span id="range-time-6" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-7" data-day="7">
                                      <input type="checkbox" name="day-7" id="day-7" value="Sun" class="range-checkbox" <?php if($businesshours):if(in_array("Sun-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-7" class="range-label">Sunday:</label>  
                                      <div id="range-slider-7" class="range-slider7"></div>
                                      <span id="range-time-7" class="range-time"></span>
                                    </div>
                                    <input type="hidden" name="days[]" value="" id="range-time-val-1" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-2" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-3" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-4" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-5" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-6" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-7" class="range-time-val">
                                    <!-- <div class="servicetime" style="display: none"></div> -->
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnconnect" id="btnconnect" value="Save" class="btn btn-success">
                                </div>
                              </div>
                            </form>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="channel-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Channel<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="radio">
                                    <label class="" style="margin-bottom: 25px;">
                                      <i class="fa fa-instagram fa-6" style="font-size: 30px;margin-right: 10px;position:relative; left:15px;" aria-hidden="true"></i>
                                      <div class="iradio_flat-green" style="position: relative;top: 25px;right: 38px;bottom: 25px;left: -20px;"><input type="radio" class="flat" name="channel_connect" <?php echo ($info->login_with == "instagram") ? "checked" : "";?> style="position: absolute; opacity: 0;" value="insta"></div>

                                    </label>
                                    
                                     <label class="channelstwt" style="margin-bottom: 25px;">
                                      <i class="fa fa-twitter fa-6" style="font-size: 30px;margin-right: 10px;position:relative; left:17px;" aria-hidden="true"></i>
                                      <div class="iradio_flat-green" style="position: relative;top: 25px;right: 38px;bottom: 25px;left: -20px;"><input type="radio" class="flat" name="channel_connect" <?php echo ($info->login_with == "twitter" || $info->login_with == "seller") ? "checked" : "";?> style="position: absolute; opacity: 0;" value="twitter"></div>

                                      <?php
                                      if($info->login_with == "twitter"):
                                      ?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/twitter" style="display: inline-block;<?php echo ($info->login_with == "twitter") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php elseif(in_array('twitter',$conn)) :?>
                                      <button disabled class="btn btn-xs btn-info" style="display: inline-block;">Connected</button>
                                      <?php else :?>
                                      <a class="btn btn-xs btn-success twitterbtn" href="<?php echo base_url()?>user/ConnectProcess/twitter" style="display: none;<?php echo ($info->login_with == "twitter") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php endif;?>

                                      <!-- <a class="btn btn-xs btn-success twitterbtn" href="<?php echo base_url()?>user/ConnectProcess/twitter" style="display: none;<?php echo ($info->login_with == "twitter") ? "visibility:hidden;" : "";?>">Connect</a> -->
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="socialchannesl">
                                <div class="row" id="insta" style="<?php echo($info->login_with == 'instagram') ? "display:block":"display:none;"?>">
                            
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Instagram Name
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="website_url_insta" class="form-control" value="<?php echo isset($socials['insta']['website']) ? $socials['insta']['website'] : ""; ?>">
                                    </div>
                                  </div>
                                
                                  <input type="hidden" name="channelname1" value="insta">
                                </div>
                                <div class="row" id="facebook" style="<?php echo($info->login_with == 'facebook') ? "display:block":"display:none;"?>">
                                
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook URL
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="website_url_fb" class="form-control" value="<?php echo isset($socials['facebook']['website']) ? $socials['facebook']['website'] : "";?>">
                                    </div>
                                  </div>
                                
                                  <input type="hidden" name="channelname2" value="facebook">
                                </div>
                                <div class="row" id="twitter" style="<?php echo($info->login_with == 'twitter' || $info->login_with == "seller") ? "display:block":"display:none;"?>">
                                 
                                   <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter URL
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="website_url_tweeter" class="form-control" value="<?php echo isset($socials['twitter']['website']) ? $socials['twitter']['website'] : "";?>">
                                    </div>
                                  </div>
                                 
                                  <input type="hidden" name="channelname3" value="twitter">
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                               <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12" id="websiteurl">
                                      <input type="text" name="website_url" class="form-control" value="<?php echo $chatdetails->website_url; ?>">
                                  </div>
                              </div>
                                <div class="item form-group" id="contactnumber">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                        <select class="form-control" name="countrycode" id="countrycode">
                                    <option data-countrycode="DZ" value="213">Algeria (+213)</option>
                                    <option data-countrycode="AD" value="376">Andorra (+376)</option>
                                    <option data-countrycode="AO" value="244">Angola (+244)</option>
                                    <option data-countrycode="AI" value="1264">Anguilla (+1264)</option>
                                    <option data-countrycode="AG" value="1268">Antigua &amp; Barbuda (+1268)</option>
                                    <option data-countrycode="AR" value="54">Argentina (+54)</option>
                                    <option data-countrycode="AM" value="374">Armenia (+374)</option>
                                    <option data-countrycode="AW" value="297">Aruba (+297)</option>
                                    <option data-countrycode="AU" value="61">Australia (+61)</option>
                                    <option data-countrycode="AT" value="43">Austria (+43)</option>
                                    <option data-countrycode="AZ" value="994">Azerbaijan (+994)</option>
                                    <option data-countrycode="BS" value="1242">Bahamas (+1242)</option>
                                    <option data-countrycode="BH" value="973">Bahrain (+973)</option>
                                    <option data-countrycode="BD" value="880">Bangladesh (+880)</option>
                                    <option data-countrycode="BB" value="1246">Barbados (+1246)</option>
                                    <option data-countrycode="BY" value="375">Belarus (+375)</option>
                                    <option data-countrycode="BE" value="32">Belgium (+32)</option>
                                    <option data-countrycode="BZ" value="501">Belize (+501)</option>
                                    <option data-countrycode="BJ" value="229">Benin (+229)</option>
                                    <option data-countrycode="BM" value="1441">Bermuda (+1441)</option>
                                    <option data-countrycode="BT" value="975">Bhutan (+975)</option>
                                    <option data-countrycode="BO" value="591">Bolivia (+591)</option>
                                    <option data-countrycode="BA" value="387">Bosnia Herzegovina (+387)</option>
                                    <option data-countrycode="BW" value="267">Botswana (+267)</option>
                                    <option data-countrycode="BR" value="55">Brazil (+55)</option>
                                    <option data-countrycode="BN" value="673">Brunei (+673)</option>
                                    <option data-countrycode="BG" value="359">Bulgaria (+359)</option>
                                    <option data-countrycode="BF" value="226">Burkina Faso (+226)</option>
                                    <option data-countrycode="BI" value="257">Burundi (+257)</option>
                                    <option data-countrycode="KH" value="855">Cambodia (+855)</option>
                                    <option data-countrycode="CM" value="237">Cameroon (+237)</option>
                                    <option data-countrycode="CA" value="1">Canada (+1)</option>
                                    <option data-countrycode="CV" value="238">Cape Verde Islands (+238)</option>
                                    <option data-countrycode="KY" value="1345">Cayman Islands (+1345)</option>
                                    <option data-countrycode="CF" value="236">Central African Republic (+236)</option>
                                    <option data-countrycode="CL" value="56">Chile (+56)</option>
                                    <option data-countrycode="CN" value="86">China (+86)</option>
                                    <option data-countrycode="CO" value="57">Colombia (+57)</option>
                                    <option data-countrycode="KM" value="269">Comoros (+269)</option>
                                    <option data-countrycode="CG" value="242">Congo (+242)</option>
                                    <option data-countrycode="CK" value="682">Cook Islands (+682)</option>
                                    <option data-countrycode="CR" value="506">Costa Rica (+506)</option>
                                    <option data-countrycode="HR" value="385">Croatia (+385)</option>
                                    <option data-countrycode="CU" value="53">Cuba (+53)</option>
                                    <option data-countrycode="CY" value="90392">Cyprus North (+90392)</option>
                                    <option data-countrycode="CY" value="357">Cyprus South (+357)</option>
                                    <option data-countrycode="CZ" value="42">Czech Republic (+42)</option>
                                    <option data-countrycode="DK" value="45">Denmark (+45)</option>
                                    <option data-countrycode="DJ" value="253">Djibouti (+253)</option>
                                    <option data-countrycode="DM" value="1809">Dominica (+1809)</option>
                                    <option data-countrycode="DO" value="1809">Dominican Republic (+1809)</option>
                                    <option data-countrycode="EC" value="593">Ecuador (+593)</option>
                                    <option data-countrycode="EG" value="20">Egypt (+20)</option>
                                    <option data-countrycode="SV" value="503">El Salvador (+503)</option>
                                    <option data-countrycode="GQ" value="240">Equatorial Guinea (+240)</option>
                                    <option data-countrycode="ER" value="291">Eritrea (+291)</option>
                                    <option data-countrycode="EE" value="372">Estonia (+372)</option>
                                    <option data-countrycode="ET" value="251">Ethiopia (+251)</option>
                                    <option data-countrycode="FK" value="500">Falkland Islands (+500)</option>
                                    <option data-countrycode="FO" value="298">Faroe Islands (+298)</option>
                                    <option data-countrycode="FJ" value="679">Fiji (+679)</option>
                                    <option data-countrycode="FI" value="358">Finland (+358)</option>
                                    <option data-countrycode="FR" value="33">France (+33)</option>
                                    <option data-countrycode="GF" value="594">French Guiana (+594)</option>
                                    <option data-countrycode="PF" value="689">French Polynesia (+689)</option>
                                    <option data-countrycode="GA" value="241">Gabon (+241)</option>
                                    <option data-countrycode="GM" value="220">Gambia (+220)</option>
                                    <option data-countrycode="GE" value="7880">Georgia (+7880)</option>
                                    <option data-countrycode="DE" value="49">Germany (+49)</option>
                                    <option data-countrycode="GH" value="233">Ghana (+233)</option>
                                    <option data-countrycode="GI" value="350">Gibraltar (+350)</option>
                                    <option data-countrycode="GR" value="30">Greece (+30)</option>
                                    <option data-countrycode="GL" value="299">Greenland (+299)</option>
                                    <option data-countrycode="GD" value="1473">Grenada (+1473)</option>
                                    <option data-countrycode="GP" value="590">Guadeloupe (+590)</option>
                                    <option data-countrycode="GU" value="671">Guam (+671)</option>
                                    <option data-countrycode="GT" value="502">Guatemala (+502)</option>
                                    <option data-countrycode="GN" value="224">Guinea (+224)</option>
                                    <option data-countrycode="GW" value="245">Guinea - Bissau (+245)</option>
                                    <option data-countrycode="GY" value="592">Guyana (+592)</option>
                                    <option data-countrycode="HT" value="509">Haiti (+509)</option>
                                    <option data-countrycode="HN" value="504">Honduras (+504)</option>
                                    <option data-countrycode="HK" value="852">Hong Kong (+852)</option>
                                    <option data-countrycode="HU" value="36">Hungary (+36)</option>
                                    <option data-countrycode="IS" value="354">Iceland (+354)</option>
                                    <option data-countrycode="IN" value="91">India (+91)</option>
                                    <option data-countrycode="ID" value="62">Indonesia (+62)</option>
                                    <option data-countrycode="IR" value="98">Iran (+98)</option>
                                    <option data-countrycode="IQ" value="964">Iraq (+964)</option>
                                    <option data-countrycode="IE" value="353">Ireland (+353)</option>
                                    <option data-countrycode="IL" value="972">Israel (+972)</option>
                                    <option data-countrycode="IT" value="39">Italy (+39)</option>
                                    <option data-countrycode="JM" value="1876">Jamaica (+1876)</option>
                                    <option data-countrycode="JP" value="81">Japan (+81)</option>
                                    <option data-countrycode="JO" value="962">Jordan (+962)</option>
                                    <option data-countrycode="KZ" value="7">Kazakhstan (+7)</option>
                                    
                                    <option data-countrycode="KI" value="686">Kiribati (+686)</option>
                                    <option data-countrycode="KP" value="850">Korea North (+850)</option>
                                    <option data-countrycode="KR" value="82">Korea South (+82)</option>
                                    <option data-countrycode="KW" value="965">Kuwait (+965)</option>
                                    <option data-countrycode="KG" value="996">Kyrgyzstan (+996)</option>
                                    <option data-countrycode="KE" value="254">Kenya (+254)</option>
                                    <option data-countrycode="LA" value="856">Laos (+856)</option>
                                    <option data-countrycode="LV" value="371">Latvia (+371)</option>
                                    <option data-countrycode="LB" value="961">Lebanon (+961)</option>
                                    <option data-countrycode="LS" value="266">Lesotho (+266)</option>
                                    <option data-countrycode="LR" value="231">Liberia (+231)</option>
                                    <option data-countrycode="LY" value="218">Libya (+218)</option>
                                    <option data-countrycode="LI" value="417">Liechtenstein (+417)</option>
                                    <option data-countrycode="LT" value="370">Lithuania (+370)</option>
                                    <option data-countrycode="LU" value="352">Luxembourg (+352)</option>
                                    <option data-countrycode="MO" value="853">Macao (+853)</option>
                                    <option data-countrycode="MK" value="389">Macedonia (+389)</option>
                                    <option data-countrycode="MG" value="261">Madagascar (+261)</option>
                                    <option data-countrycode="MW" value="265">Malawi (+265)</option>
                                    <option data-countrycode="MY" value="60">Malaysia (+60)</option>
                                    <option data-countrycode="MV" value="960">Maldives (+960)</option>
                                    <option data-countrycode="ML" value="223">Mali (+223)</option>
                                    <option data-countrycode="MT" value="356">Malta (+356)</option>
                                    <option data-countrycode="MH" value="692">Marshall Islands (+692)</option>
                                    <option data-countrycode="MQ" value="596">Martinique (+596)</option>
                                    <option data-countrycode="MR" value="222">Mauritania (+222)</option>
                                    <option data-countrycode="YT" value="269">Mayotte (+269)</option>
                                    <option data-countrycode="MX" value="52">Mexico (+52)</option>
                                    <option data-countrycode="FM" value="691">Micronesia (+691)</option>
                                    <option data-countrycode="MD" value="373">Moldova (+373)</option>
                                    <option data-countrycode="MC" value="377">Monaco (+377)</option>
                                    <option data-countrycode="MN" value="976">Mongolia (+976)</option>
                                    <option data-countrycode="MS" value="1664">Montserrat (+1664)</option>
                                    <option data-countrycode="MA" value="212">Morocco (+212)</option>
                                    <option data-countrycode="MZ" value="258">Mozambique (+258)</option>
                                    <option data-countrycode="MN" value="95">Myanmar (+95)</option>
                                    <option data-countrycode="NA" value="264">Namibia (+264)</option>
                                    <option data-countrycode="NR" value="674">Nauru (+674)</option>
                                    <option data-countrycode="NP" value="977">Nepal (+977)</option>
                                    <option data-countrycode="NL" value="31">Netherlands (+31)</option>
                                    <option data-countrycode="NC" value="687">New Caledonia (+687)</option>
                                    <option data-countrycode="NZ" value="64">New Zealand (+64)</option>
                                    <option data-countrycode="NI" value="505">Nicaragua (+505)</option>
                                    <option data-countrycode="NE" value="227">Niger (+227)</option>
                                    <option data-countrycode="nigeria" value="234">Nigeria (+234)</option>
                                    <option data-countrycode="NU" value="683">Niue (+683)</option>
                                    <option data-countrycode="NF" value="672">Norfolk Islands (+672)</option>
                                    <option data-countrycode="NP" value="670">Northern Marianas (+670)</option>
                                    <option data-countrycode="NO" value="47">Norway (+47)</option>
                                    <option data-countrycode="OM" value="968">Oman (+968)</option>
                                    <option data-countrycode="PW" value="680">Palau (+680)</option>
                                    <option data-countrycode="PA" value="507">Panama (+507)</option>
                                    <option data-countrycode="PG" value="675">Papua New Guinea (+675)</option>
                                    <option data-countrycode="PY" value="595">Paraguay (+595)</option>
                                    <option data-countrycode="PE" value="51">Peru (+51)</option>
                                    <option data-countrycode="PH" value="63">Philippines (+63)</option>
                                    <option data-countrycode="PL" value="48">Poland (+48)</option>
                                    <option data-countrycode="PT" value="351">Portugal (+351)</option>
                                    <option data-countrycode="PR" value="1787">Puerto Rico (+1787)</option>
                                    <option data-countrycode="QA" value="974">Qatar (+974)</option>
                                    <option data-countrycode="RE" value="262">Reunion (+262)</option>
                                    <option data-countrycode="RO" value="40">Romania (+40)</option>
                                    <option data-countrycode="RU" value="7">Russia (+7)</option>
                                    <option data-countrycode="RW" value="250">Rwanda (+250)</option>
                                    <option data-countrycode="SM" value="378">San Marino (+378)</option>
                                    <option data-countrycode="ST" value="239">Sao Tome &amp; Principe (+239)</option>
                                    <option data-countrycode="SA" value="966">Saudi Arabia (+966)</option>
                                    <option data-countrycode="SN" value="221">Senegal (+221)</option>
                                    <option data-countrycode="CS" value="381">Serbia (+381)</option>
                                    <option data-countrycode="SC" value="248">Seychelles (+248)</option>
                                    <option data-countrycode="SL" value="232">Sierra Leone (+232)</option>
                                    <option data-countrycode="SG" value="65">Singapore (+65)</option>
                                    <option data-countrycode="SK" value="421">Slovak Republic (+421)</option>
                                    <option data-countrycode="SI" value="386">Slovenia (+386)</option>
                                    <option data-countrycode="SB" value="677">Solomon Islands (+677)</option>
                                    <option data-countrycode="SO" value="252">Somalia (+252)</option>
                                    <option data-countrycode="ZA" value="27">South Africa (+27)</option>
                                    <option data-countrycode="ES" value="34">Spain (+34)</option>
                                    <option data-countrycode="LK" value="94">Sri Lanka (+94)</option>
                                    <option data-countrycode="SH" value="290">St. Helena (+290)</option>
                                    <option data-countrycode="KN" value="1869">St. Kitts (+1869)</option>
                                    <option data-countrycode="SC" value="1758">St. Lucia (+1758)</option>
                                    <option data-countrycode="SD" value="249">Sudan (+249)</option>
                                    <option data-countrycode="SR" value="597">Suriname (+597)</option>
                                    <option data-countrycode="SZ" value="268">Swaziland (+268)</option>
                                    <option data-countrycode="SE" value="46">Sweden (+46)</option>
                                    <option data-countrycode="CH" value="41">Switzerland (+41)</option>
                                    <option data-countrycode="SI" value="963">Syria (+963)</option>
                                    <option data-countrycode="TW" value="886">Taiwan (+886)</option>
                                    <option data-countrycode="TJ" value="7">Tajikstan (+7)</option>
                                    <option data-countrycode="thailand" value="66">Thailand (+66)</option>
                                    <option data-countrycode="TG" value="228">Togo (+228)</option>
                                    <option data-countrycode="TO" value="676">Tonga (+676)</option>
                                    <option data-countrycode="TT" value="1868">Trinidad &amp; Tobago (+1868)</option>
                                    <option data-countrycode="TN" value="216">Tunisia (+216)</option>
                                    <option data-countrycode="TR" value="90">Turkey (+90)</option>
                                    <option data-countrycode="TM" value="7">Turkmenistan (+7)</option>
                                    <option data-countrycode="TM" value="993">Turkmenistan (+993)</option>
                                    <option data-countrycode="TC" value="1649">Turks &amp; Caicos Islands (+1649)</option>
                                    <option data-countrycode="TV" value="688">Tuvalu (+688)</option>
                                    <option data-countrycode="US" value="1">USA (+1)</option>
                                    <option data-countrycode="UG" value="256">Uganda (+256)</option>
                                    <option data-countrycode="UA" value="380">Ukraine (+380)</option>
                                    <option data-countrycode="AE" value="971">United Arab Emirates (+971)</option>
                                    <option data-countrycode="UY" value="598">Uruguay (+598)</option>

                                    <option data-countrycode="UZ" value="7">Uzbekistan (+7)</option>
                                    <option data-countrycode="VU" value="678">Vanuatu (+678)</option>
                                    <option data-countrycode="VA" value="379">Vatican City (+379)</option>
                                    <option data-countrycode="VE" value="58">Venezuela (+58)</option>
                                    <option data-countrycode="VN" value="84">Vietnam (+84)</option>
                                    <option data-countrycode="VG" value="84">Virgin Islands - British (+1284)</option>
                                    <option data-countrycode="VI" value="84">Virgin Islands - US (+1340)</option>
                                    <option data-countrycode="WF" value="681">Wallis &amp; Futuna (+681)</option>
                                    <option data-countrycode="YE" value="969">Yemen (North)(+969)</option>
                                    <option data-countrycode="YE" value="967">Yemen (South)(+967)</option>
                                    <option data-countrycode="ZM" value="260">Zambia (+260)</option>
                                    <option data-countrycode="ZW" value="263">Zimbabwe (+263)</option>
                          </select>
                      </div>
                  
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <input type="text" name="contact_number" class="form-control" value="<?php echo (count($codes) > 1) ? $codes[1] : $chatdetails->contact_number; ?>">
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                                <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnchannel" id="btnchannel" value="Save" class="btn btn-success">
                                </div>
                              </div>
                             </form>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="delivery-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Do You Offer Returns ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12" id="returnofferdiv">
                                  <select class="form-control" id="returnoffer" name="offer">
                                      <option value="0" <?php echo(!empty($chatdetails->return_offer) && $chatdetails->return_offer == 0) ? "selected":"";?>>No</option>
                                      <option value="1" <?php echo(!empty($chatdetails->return_offer) && $chatdetails->return_offer == 1) ? "selected":"";?>>Yes</option>
                                  </select>
                                </div>
                              </div>
                              <div class="item form-group offer-return" style=<?php echo(!empty($chatdetails->return_offer) && $chatdetails->return_offer == 1) ? "display:block":"display:none";?>>
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Returns Days ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox services-days">
                                    <label>
                                      <input type="checkbox" value="7" name="daysoffer[]" <?php echo(!empty($chatdetails->offer_days) && in_array("7", $offerdays)) ? "checked" : "";?>> 7 Days
                                    </label>
                                  </div>
                                  <div class="checkbox services-days">
                                    <label>
                                      <input type="checkbox" value="14" name="daysoffer[]" <?php echo(!empty($chatdetails->offer_days) && in_array("14", $offerdays)) ? "checked" : "";?>>14 Days
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="item form-group" id="serviceofferdiv">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Do You Offer Delivery Services ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select class="form-control" name="serviceprovide" id="serviceprovide">
                                      <option value="0" <?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 0) ? "selected" : ""?>>No</option>
                                      <option value="1" <?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 1) ? "selected" : ""?>>Yes</option>
                                  </select>
                                </div>
                              </div>
                              <div class="item form-group services-offer" style="<?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 1) ? "display:block":"display:none"?>">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Delivery Options ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12" <?php echo($chatdetails->delivery_services == 1) ? "style=display:block":"display:none"?>>
                                  <div class="checkbox">
                                    <label>
                                    <input type="checkbox" value="Postal Service" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Postal Service", $providers)) ? "checked" : "";?>>Postal Service
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Standard Delivery 5 + Days" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Standard Delivery 5 + Days", $providers)) ? "checked" : "";?>>Standard Delivery 5 + Days 
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Express Delivery 2 - 4 Days" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Express Delivery 2 - 4 Days", $providers)) ? "checked" : "";?>>Express Delivery 2 - 4 Days
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Next Day Delivery" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Next Day Delivery", $providers)) ? "checked" : "";?>>Next Day Delivery
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Same Day Delivery" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Same Day Delivery", $providers)) ? "checked" : "";?>> Same Day Delivery
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="International Standard" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("International Standard", $providers)) ? "checked" : "";?>>International Standard
                                    </label>
                                  </div>
                                  <div class="checkbox lastone">
                                    <label>
                                      <input type="checkbox" value="International Express" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("International Express", $providers)) ? "checked" : "";?>>International Express
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <?php if(!is_null($states)):?>
                              <div id="delivery_states" class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">States You Operate in<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="checkbox">
                                    <label style="font-weight: bold;">
                                     <input type="checkbox" id="checkAll">Select All States
                                    </label>
                                  </div>
                                  <?php foreach($states as $state):?>
                                  <div class="checkbox state-delivery">
                                    <label>
                                      <?php if($info->country == 'thailand'):?>
                                      <input type="checkbox" class="state-country" value="<?php echo $state->province?>" name="offerstates[]" <?php echo(!empty($chatdetails->offer_states) && in_array($state->province, $stateschecked)) ? "checked" : "";?>> <?php echo $state->province; ?>
                                      <?php elseif($info->country == 'nigeria'):?>
                                        <input type="checkbox" class="state-country" value="<?php echo $state->state_name?>" name="offerstates[]" <?php echo(!empty($chatdetails->offer_states) && in_array($state->state_name, $stateschecked)) ? "checked" : "";?>> <?php echo $state->state_name; ?>
                                      <?php endif;?> 
                                    </label>
                                  </div>
                                <?php endforeach;?>
                                </div>
                              </div>
                              <?php endif;?>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnservice" id="btnservice" value="Save" class="btn btn-success">
                                </div>
                              </div>
                            </form>
                          </div>
                          <!-- <?php echo($this->session->userdata('isFacebook') == true) ? 'active in' : ''?> -->
                          <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="payment-tab">
                            <?php 
                            $friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
                            if(date('Y-m-d') == date('Y-m-d',strtotime('friday'))){
                              $fridaydate = date('Y-m-d');
                            }else{
                              $fridaydate = $friday;
                            }

                           // echo $fridaydate;

                            $today = date('Y-m-d');
                            $nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($fridaydate))." +7 days")); 
                            ?>
                            <?php if($info->country == "nigeria"):?>
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$profileinfo->admin_id?>"  method="post" enctype="multipart/form-data">
                              
                              <div class="row" id="steps5">
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="bank_name" id="bank_name">

                                        <option value="">-Select Bank-</option>
                                        <?php foreach($banklists as $bank) {?>
                                          <option value="<?php echo $bank->bank_code?>" <?php echo($banks['bank_name'] == $bank->bank_code) ? "selected" : "";?>><?php echo $bank->bank_name;?></option>
                                        <?php } ?>
                                        
                                    </select>
                                   
                                  </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Number<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="account_number" value="<?php echo($banks) ? $banks['account_number'] : '';?>" class="form-control" id="account_number">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Name<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="account_name" value="<?php echo($banks) ? $banks['account_name'] : '';?>" class="form-control" id="account_name">
                                    </div>
                                </div>
                                
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <p style="border:1px solid #cccccc;padding: 5px;">Your next payment will be made on : <span class="label label-info"><?php echo date('d F Y',strtotime($nextbill));?></span> <br> 
                                    Amount to Be Paid : <span>₦<?php echo (empty($orders['Totalamount']) || $orders['Totalamount'] == 0) ? 0 : '<a href="<?php echo base_url("order/finances")?>" class="label label-info">'.@$orders['Totalamount'].'</a>'?></span>  
                                    </p>
                                    
                                  </div>
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                              
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnpayment" id="btnpayment2" value="Save" class="btn btn-success btnpayment-payment">
                                </div>
                              </div>
                             </form>
                             <?php else : ?>
                             <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                              <div id="steps5">
                                <div class="row">
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"><img src="<?php echo base_url() ?>assets/img/omise1.png" alt="" width="80">
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <p style="padding-top: 8%;"><strong><?php echo strtoupper("omise credentials");?></strong></p>
                                    </div>
                                  </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Public Key<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="public_key" value="<?php echo(trim($chatdetails->public_key));?>" class="form-control" required>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Secret Key<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="secret_key" value="<?php echo(trim($chatdetails->secret_key));?>" class="form-control" required>
                                    </div>
                                </div>
                                 
                              </div>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <input type="submit" name="btnpayment" id="btnpayment-3" value="Save" class="btn btn-success btnpayment-payment">
                                </div>
                              </div>
                              </div> 
                              </form>
                             <?php endif;?>
                             <form class="form-horizontal form-label-left" novalidate method="post" enctype="multipart/form-data" action="<?php echo base_url()?>admin/paywithflutterwave"><!-- https://hosted.flutterwave.com/processPayment.php -->
                              
                              <?php
                              if($afteronemonth == 1):
                              ?>
                              <div class="ln_solid"></div>
                              <input type="hidden" name="amount" value="0" id="payamount"/> <!-- Replace the value with your transaction amount -->
                              <input type="hidden" name="payment_method" value="both" />
                              <!-- Replace the value with your transaction description -->
                              <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                              <input type="hidden" name="currency" value="USD" /> <!-- Replace the value with your transaction currency -->
                              <input type="hidden" name="email" value="<?php echo $info->admin_email;?>"/> <!-- Replace the value with your customer email -->
                              <input type="hidden" name="ref" value="<?php echo uniqid();?>" />
                              <!-- <input type="hidden" name="env" value="staging"> --> 
                              <!-- <input type="hidden" name="successurl" value="<?php echo base_url()?>order/subscription/?name=success">
                              <input type="hidden" name="failureurl" value="<?php echo base_url()?>order/subscription/?name=failed"> -->

                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">When Subscription due<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="100" name="servicescharge" id="servicescharge">$100 (Ritekit Plan)
                                  </label>
                                  <br><br>
                                  <p>
                                    <b>Social Media Marketing</b> 
                                    <ul>
                                      <li>Set Hashtags and Keywords for your business</li>
                                      <li>Find Influencers for your business and mention them in your posts </li>
                                      <li>Schedule up to 5 enhanced posts at a time across <li>Facebook, Twitter and Instagram</li>
                                      <li>Promoted post with popular blogs on Facebook, Instagram and Twitter</li>
                                    </ul>
                                  </p>
                                  <div class="ln_solid"></div>
                                 </div>
                                </div>
                              </div>
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Auto renew payment ?<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="subscriptionplan" id="subscriptionplan" value="<?php echo($chatdetails->renew_subscription == "1") ? "1":"0";?>" <?php echo($chatdetails->renew_subscription == "1") ? "checked":"";?>>
                                    </label>
                                  </div>
                                  
                                </div>
                              </div>
                              
                              <?php 
                              $extenddate = date('Y-m-d', strtotime($dt. ' + 3 days')); // Add  3 days in due date
                              $date1 = date_create($extenddate);
                      
                              $date2 = date_create(date('Y-m-d'));
                              //difference between two dates
                              $diff = date_diff($date1,$date2);
                              $duedays = $diff->format("%a"); // Difference in days
                              if(date('Y-m-d') === $date || date('Y-m-d') <= $extenddate) : ?>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnpay" id="btnpay" value="Payment" class="btn btn-info">
                                  <br><br>
                                  <h4><span class="label label-primary">Note : No need to pay if you subscribed auto renew option.</span></h4>
                                </div>
                              </div>
                              <?php endif;?>
                             </form>
                            <?php
                            endif;
                            ?>
                          </div>
                        <?php endif;?>
                        </div>
                      <?php
                      if(!is_null($info->payment_link)):
                      ?>
                        <div class="text-center">
                          <h4><b>Payment Link :- </b><?php echo $info->payment_link;?></h4>
                        </div>
                        <?php
                      endif;
                      ?>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Payment Setup</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/Flutterwave_Keys.mov" >
      </video>
      </div>
      </div>
      </div>
  </div>

  <div class="modal fade bs-pageadmin-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Page Admin Role Assign</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/Fb-pageadmin.mov" >
      </video>
      </div>
      </div>
      </div>
  </div>
  <script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
      var country = "<?php echo $info->country?>";
      var code = "<?php echo $codes[0]?>";
      $('#countrycode option').each(function() {
        //console.log($(this).attr('data-countrycode'));
        if(code == $(this).val()){
          //$('#countrycode option[data-countrycode="'+country+'"]').attr("selected", "selected");
          $('#countrycode option[value="'+$(this).val()+'"]').attr("selected", "selected");
        }
       /* else if($(this).attr('data-countrycode') != "nigeria" && $(this).attr('data-countrycode') != "thailand"){
          $('#countrycode option[value="'+$(this).val()+'"]').attr("disabled", "disabled"); 
        }*/
        /*else{
          $('#countrycode option[value="'+$(this).val()+'"]').attr("disabled", "disabled");
        }*/
        //optionValues.push($(this).val());
      });
  })  
</script>
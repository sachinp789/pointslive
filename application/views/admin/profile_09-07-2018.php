<?php 
  $this->load->model('Chatbots_model'); 
  $chatdetails = $this->Chatbots_model->getChatData();
  if($chatdetails){
    $businesshours = unserialize($chatdetails->business_hours);
    if($businesshours){  
      $exptimes = implode("-", $businesshours);
    }
    $providers = unserialize($chatdetails->service_provides);
    $offerdays = unserialize($chatdetails->offer_days);
    $apikeys = unserialize($chatdetails->social_apikeys);
    $socials = unserialize($chatdetails->social_info);
  }
 
  $last = $this->db->order_by('id','desc')
        ->where('created_by',$this->session->userdata('adminId'))
        ->limit(1)
        ->get('subscription_orders')
        ->row();
  if($last){
    $dt = new DateTime($last->created_date);
    $registereddate = $dt->format('Y-m-d');
    $afteronemonth = get_month_diff($registereddate,false);
  } 
  else{
    $dt = new DateTime($profileinfo->created_date);
    $registereddate = $dt->format('Y-m-d');
    $afteronemonth = get_month_diff($registereddate,false);
  }   
  $info = admin_info($this->session->userdata('adminId')); 
  $socialac = explode(",", $info->connected_with);

  $this->db->where_in('admin_id',$socialac);
  $accounts = $this->Admin_model->get_all();
  $conn = [];
  foreach ($accounts as $connection) {
    # code...
    $conn[] = $connection->login_with;
  } 
?>
<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">

                    <h2><?php echo $title ?></h2>

                    <div class="clearfix"></div>

                  </div>

                  <?php //$msg = $this->uri->segment_array(); 

                  ?>

                  <div class="x_content">

                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>

                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">

                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>

                      </button>

                        <?php echo $this->session->flashdata('flashmsg'); ?>

                      </div>

                    <?php endif; ?>

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">

                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">

                            <?php 
                            if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isFacebook') == false):
                            //var_dump(empty($this->session->userdata('isLogin')));
                            ?>
                             <li role="presentation" class="active"><a href="#tab_content7" role="tab" id="social-tab" data-toggle="tab" aria-expanded="false">Chatbot</a>
                            </li>
                           <?php endif;?> 

                           <?php if(!empty($this->session->userdata('isLogin')) && $chatdetails == true):
                            //var_dump(empty($this->session->userdata('isLogin')));
                            ?>
                             <li role="presentation" class="<?php echo($this->session->userdata('isFacebook') == true) ? 'active in' : ''?>"><a href="#tab_content6" role="tab" id="payment-tab" data-toggle="tab" aria-expanded="false">Payments & Billing</a>
                            </li>
                           <?php endif;?> 
                          
                            <li role="presentation" class="<?php echo ($chatdetails == false && $this->session->userdata('isFacebook') == true || empty($this->session->userdata('isLogin'))) ? 'active' : ''; ?>"><a href="#tab_content1" id="profile-tab" role="tab" data-toggle="tab" aria-expanded="false">Profile</a>
                            </li>
                       
                          <?php if(is_null($page) && empty($this->session->userdata('isLogin'))):?>
                            <li role="presentation"><a href="#tab_content2" role="tab" id="smtp-tab" data-toggle="tab" aria-expanded="true">SMTP Configuration</a>
                          </li>
                          <?php endif; ?>

                          <?php if(!empty($this->session->userdata('isLogin')) && $chatdetails == true):?>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="store-tab" data-toggle="tab" aria-expanded="false">Store Information</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="channel-tab" data-toggle="tab" aria-expanded="false">Channels</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content5" role="tab" id="delivery-tab" data-toggle="tab" aria-expanded="false">Delivery and Return</a>
                            </li>
                        <?php endif; ?>
                        </ul>

                        <div id="myTabContent" class="tab-content">

                          <div role="tabpanel" class="tab-pane fade <?php echo($this->session->userdata('isFacebook') == false && !empty($this->session->userdata('isLogin'))) ? 'active in' : ''?>" id="tab_content7" aria-labelledby="smtp-tab">
                           <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                  <img src="<?php echo base_url()?>assets/img/fbsymbol.png" alt="" width="150">
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                  <p><?php echo strtoupper("connect out store bot to your facebook page");?></p>
                                  <p>In line with recent privacy updates on Facebook, you need to give us permission to setup this chatbot on your Facebook Page. Here is what you need to do</p>
                                  <p>(1) Connect your facebook page &nbsp;&nbsp;
                                    <?php //var_dump($conn); ?>
                                    <?php if(in_array('facebook',$conn)) : ?>
                                    <button disabled class="btn btn-xs btn-info" style="display: block;margin-top: 40px;">Connected</button>
                                      <?php else :?>
                                    <a class="btn btn-xs btn-primary" href="<?php echo base_url()?>user/ConnectProcess/facebook" <?php echo ($info->login_with == "facebook") ? "visibility:hidden;" : "";?>">Connect</a>
                                    <?php endif;?>  
                                    <br><br>
                                    See this video for step (2) to (6) &nbsp;&nbsp;<a class="btn btn-xs btn-primary" data-toggle="modal" data-target=".bs-pageadmin-modal-lg"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                                  </p>
                                  <p>(2) Go to your Web Browser anmd login to Facebook -> www.facebook.com</p>
                                  <p>(3) Go to your Facebook Page</p>
                                  <p>(4) Go to Settings in the Top right corner</p>
                                  <p>(5) Go to Page Roles</p>
                                  <p>(6) Add Akin AI as an admin to your Facebook Page</p>
                                  <p>(7) Click on Test Connection</p>
                                </div>  
                           </div>
                          <div class="ln_solid"></div>
                          </div>  
                          <?php
                          $class = "";
                          if(empty($this->session->userdata('isLogin'))){
                            $class = "active in";
                          }
                          else if(!empty($this->session->userdata('isLogin')) && !empty($this->session->userdata('isFacebook')) && $chatdetails == false){
                            $class = "active in";
                          }
                          else if(!empty($this->session->userdata('isLogin')) && empty($this->session->userdata('isFacebook'))){
                            $class = "";
                          }
                          ?>

                          <div role="tabpanel" class="tab-pane fade <?php echo $class;?>" id="tab_content1" aria-labelledby="profile-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'admin/profile/'.$profileinfo->admin_id?>"  method="post" enctype="multipart/form-data">

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_name">Name <span class="required"></span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input id="admin_name" class="form-control col-md-7 col-xs-12" name="admin_name" value="<?php echo($profileinfo->admin_name ? $profileinfo->admin_name : '') ?>" type="text">

                                  <?php echo form_error('admin_name','<span class="help-block">','</span>'); ?>

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin_email">Email <span class="required">*</span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="email" id="admin_email" name="admin_email" class="form-control col-md-7 col-xs-12" value="<?php echo($profileinfo->admin_email ? $profileinfo->admin_email : '') ?>" required="required">

                                  <?php echo form_error('admin_email','<span class="help-block">','</span>'); ?>

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile_no">Mobile Number

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="number" id="mobile_no" name="mobile_no" class="form-control col-md-7 col-xs-12"  value="<?php echo($profileinfo->admin_mobile ? $profileinfo->admin_mobile : '') ?>">

                                  <?php echo form_error('admin_email','<span class="help-block">','</span>'); ?>

                                </div>

                              </div>
                              <?php if(empty($this->session->userdata('isLogin'))):?>
                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_photo">Profile Photo

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="file" id="profile_photo" name="profile_photo" class="form-control col-md-7 col-xs-12">

                                    <div class="profile_img">

                                        <div class="crop-avatar">

                                        <!-- Current avatar -->

                                          <img class="img-responsive avatar-view" src="<?php echo base_url().'uploads/'.$profileinfo->admin_photo ?>"  width="100" height="100">

                                        </div>

                                        <input type="hidden" name="existimage" value="<?php echo($profileinfo->admin_photo ? $profileinfo->admin_photo : '') ?>">

                                    </div>

                                </div>

                              </div>
                               <?php endif; ?>
                               <?php if(!is_null($page) && $page = "652803498256943"):?>
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="page_events">Enable page events ?
                                  </label>
                                  <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="radio">
                                      <label>
                                        <div><input type="radio" class="flat" value="1" name="page_events" id="page_events" <?php echo ($profileinfo->page_events == 1) ? "checked" : "";?>></div> On
                                      </label>
                                      <label>
                                        <div><input type="radio" class="flat" value="0" name="page_events" id="page_events" <?php echo ($profileinfo->page_events == 0) ? "checked" : "";?>></div> Off
                                    </label>
                                  </div>
                                  </div>
                                </div>
                              <?php endif;?>
                              <div class="ln_solid"></div>

                              <div class="form-group">

                                <div class="col-md-6 col-md-offset-3">

                                  <input type="submit" name="profilepost" id="profilepost1" value="Save" class="btn btn-success">

                                </div>

                              </div>

                            </form>  
                          </div>
                          <?php if(is_null($page) && empty($this->session->userdata('isLogin'))):
                          ?>  
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="smtp-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'admin/mailConfiguration'?>" method="post">

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="protocol">Protocol <span class="required"></span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input id="protocol" class="form-control col-md-7 col-xs-12" name="protocol" value="<?php echo($configurations['protocol'] ? $configurations['protocol'] : '') ?>" type="text">

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_host">Host <span class="required"></span>

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="text" id="smtp_host" name="smtp_host" class="form-control col-md-7 col-xs-12" value="<?php echo($configurations['smtp_host'] ? $configurations['smtp_host'] : '') ?>" required="required">

                                  

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_pass">Password

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="password" id="smtp_pass" name="smtp_pass" class="form-control col-md-7 col-xs-12"  value="<?php echo($configurations['smtp_pass'] ? $configurations['smtp_pass'] : '') ?>">

                                  

                                </div>

                              </div>

                              <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_port">Port

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="number" id="smtp_port" name="smtp_port" class="form-control col-md-7 col-xs-12"  value="<?php echo($configurations['smtp_port'] ? $configurations['smtp_port'] : '') ?>">

                                 

                                </div>

                              </div>

                               <div class="item form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="smtp_user">Username

                                </label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                  <input type="text" id="smtp_user" name="smtp_user" class="form-control col-md-7 col-xs-12"  value="<?php echo($configurations['smtp_user'] ? $configurations['smtp_user'] : '') ?>">

                                  

                                </div>

                              </div>

                              <div class="ln_solid"></div>

                              <div class="form-group">

                                <div class="col-md-6 col-md-offset-3">

                                  <input type="submit" name="mailpost" id="mailpost" value="Save" class="btn btn-success">

                                </div>

                              </div>
                          </div>
                          <?php endif; ?>
                          <?php if(!empty($this->session->userdata('isLogin')) && $chatdetails == true):?>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="store-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Opening and Closing Time <span class="required"></span>
                                  </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="businesshours" id="businesshours">
                                        <option value="alltime" <?php echo($chatdetails->store_open == "alltime") ? "selected" : ""; ?>>24 hours</option>
                                        <option value="custom" <?php echo($chatdetails->store_open == "custom") ? "selected" : ""; ?>>Custom</option>
                                    </select>
                                  </div>
                                </div>
                              <div class="item form-group" id="times" style="<?php echo($chatdetails->store_open == "custom") ? "display:bock" : "display:none"; ?>">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                </label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                   <div class="range-day" id="range-day-1" data-day="1">
                                      <input type="checkbox" name="day-1" id="day-1" value="Mon" class="range-checkbox" <?php if($businesshours):if(in_array("Mon-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-1" class="range-label">Monday:</label>
                                      <div id="range-slider-1" class="range-slider1"></div>
                                      <span id="range-time-1" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-2" data-day="2">
                                      <input type="checkbox" name="day-2" id="day-2" value="Tue" class="range-checkbox" <?php if($businesshours):if(in_array("Tue-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-2" class="range-label">Tuesday:</label>
                                      <div id="range-slider-2" class="range-slider2"></div>
                                      <span id="range-time-2" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-3" data-day="3">
                                      <input type="checkbox" name="day-3" id="day-3" value="Wed" class="range-checkbox" <?php if($businesshours):if(in_array("Wed-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-3" class="range-label">Wednesday:</label>  
                                      <div id="range-slider-3" class="range-slider3"></div>
                                      <span id="range-time-3" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-4" data-day="4">
                                      <input type="checkbox" name="day-4" id="day-4" value="Thu" class="range-checkbox" <?php if($businesshours):if(in_array("Thu-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-4" class="range-label">Thursday‎:</label>  
                                      <div id="range-slider-4" class="range-slider4"></div>
                                      <span id="range-time-4" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-5" data-day="5">
                                      <input type="checkbox" name="day-5" id="day-5" value="Fri" class="range-checkbox" <?php if($businesshours):if(in_array("Fri-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-5" class="range-label">Friday:</label>  
                                      <div id="range-slider-5" class="range-slider5"></div>
                                      <span id="range-time-5" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-6" data-day="6">
                                      <input type="checkbox" name="day-6" id="day-6" value="Sat" class="range-checkbox" <?php if($businesshours):if(in_array("Sat-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-6" class="range-label">Saturday:</label>  
                                      <div id="range-slider-6" class="range-slider6"></div>
                                      <span id="range-time-6" class="range-time"></span>
                                    </div>
                                    
                                    <div class="range-day" id="range-day-7" data-day="7">
                                      <input type="checkbox" name="day-7" id="day-7" value="Sun" class="range-checkbox" <?php if($businesshours):if(in_array("Sun-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                                      <label for="day-7" class="range-label">Sunday:</label>  
                                      <div id="range-slider-7" class="range-slider7"></div>
                                      <span id="range-time-7" class="range-time"></span>
                                    </div>
                                    <input type="hidden" name="days[]" value="" id="range-time-val-1" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-2" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-3" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-4" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-5" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-6" class="range-time-val">
                                    <input type="hidden" name="days[]" value="" id="range-time-val-7" class="range-time-val">
                                    <!-- <div class="servicetime" style="display: none"></div> -->
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnconnect" id="btnconnect" value="Save" class="btn btn-success">
                                </div>
                              </div>
                            </form>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="channel-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Channel<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="radio">
                                    <label class="">
                                      <i class="fa fa-instagram fa-6" style="font-size: 30px;margin-right: 10px;position:relative; left:15px;" aria-hidden="true"></i>
                                      <div class="iradio_flat-green" style="position: relative;top: 25px;right: 38px;bottom: 25px;left: -20px;"><input type="radio" class="flat" name="channel_connect" <?php echo ($info->login_with == "instagram") ? "checked" : "";?> style="position: absolute; opacity: 0;" value="insta"></div>

                                      <?php
                                      if($info->login_with == "instagram"):
                                      ?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/insta" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "instagram") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php elseif(in_array("instagram",$conn)) :?>
                                        <button disabled class="btn btn-xs btn-info" style="display: block;margin-top: 40px;">Connected</button>
                                      <?php else :?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/insta" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "instagram") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php endif;?>

                                      <!-- <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/loginprocess/insta" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "instagram") ? "visibility:hidden" : "";?>">Connect</a> -->
                                    </label>
                                     <label class="">
                                      <i class="fa fa-facebook fa-6" style="font-size: 30px;margin-right: 10px;position:relative; left:20px;" aria-hidden="true"></i>
                                      <div class="iradio_flat-green" style="position: relative;top: 25px;right: 38px;bottom: 25px;left: -20px;"><input type="radio" class="flat" name="channel_connect" <?php echo ($info->login_with == "facebook") ? "checked" : "";?> style="position: absolute; opacity: 0;" value="facebook"></div>

                                      <?php                                 
                                      if($info->login_with == "facebook"):
                                        
                                      ?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/facebook" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "facebook") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php elseif(in_array('facebook',$conn)) : ?>
                                      <button disabled class="btn btn-xs btn-info" style="display: block;margin-top: 40px;">Connected</button>
                                      <?php else :?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/facebook" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "facebook") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php endif;?>

                                      <!-- <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/loginprocess/facebook" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "facebook") ? "visibility:hidden" : "";?>">Connect</a> -->
                                    </label>
                                     <label class="">
                                      <i class="fa fa-twitter fa-6" style="font-size: 30px;margin-right: 10px;position:relative; left:17px;" aria-hidden="true"></i>
                                      <div class="iradio_flat-green" style="position: relative;top: 25px;right: 38px;bottom: 25px;left: -20px;"><input type="radio" class="flat" name="channel_connect" <?php echo ($info->login_with == "twitter") ? "checked" : "";?> style="position: absolute; opacity: 0;" value="twitter"></div>

                                      <?php
                                      if($info->login_with == "twitter"):
                                      ?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/twitter" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "twitter") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php elseif(in_array('twitter',$conn)) :?>
                                      <button disabled class="btn btn-xs btn-info" style="display: block;margin-top: 40px;">Connected</button>
                                      <?php else :?>
                                      <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/ConnectProcess/twitter" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "twitter") ? "visibility:hidden;" : "";?>">Connect</a>
                                      <?php endif;?>

                                      <!-- <a class="btn btn-xs btn-success" href="<?php echo base_url()?>user/loginprocess/twitter" style="display: block;margin-top: 40px;<?php echo ($info->login_with == "twitter") ? "visibility:hidden;" : "";?>">Connect</a> -->
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="row" id="insta" style="<?php echo($info->login_with == 'instagram') ? "display:block":"display:none;"?>">
                                <!-- <div class="item form-group" style="padding: 20px 0 0;">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">API Key<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="apikeys" class="form-control" value="<?php echo $apikeys['insta']['apikey']?>">
                                  </div>
                                </div>
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">API Secret<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="apisecret" class="form-control" value="<?php echo $apikeys['insta']['apisecret']?>">
                                  </div>
                                </div> -->
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="website_url_insta" class="form-control" value="<?php echo isset($socials['insta']['website']) ? $socials['insta']['website'] : ""; ?>">
                                  </div>
                                </div>
                                <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" name="contact_number_insta" class="form-control" value="<?php echo isset($socials['insta']['contact']) ? $socials['insta']['contact'] : "";?>">
                                </div>
                                </div>
                                <input type="hidden" name="channelname1" value="insta">
                              </div>
                              <div class="row" id="facebook" style="<?php echo($info->login_with == 'facebook') ? "display:block":"display:none;"?>">
                               <!--  <div class="item form-group" style="padding: 30px 0 0;">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">App ID<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="appID" class="form-control" value="<?php echo $apikeys['facebook']['apikey']?>">
                                  </div>
                                </div>
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Secret Key<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="fbsecretkey" class="form-control" value="<?php echo $apikeys['facebook']['apisecret']?>">
                                  </div>
                                </div> -->
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="website_url_fb" class="form-control" value="<?php echo isset($socials['facebook']['website']) ? $socials['facebook']['website'] : "";?>">
                                  </div>
                                </div>
                                <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" name="contact_number_fb" class="form-control" value="<?php echo isset($socials['facebook']['contact']) ? $socials['facebook']['contact'] : "";?>">
                                </div>
                                </div>
                                <input type="hidden" name="channelname2" value="facebook">
                              </div>
                              <div class="row" id="twitter" style="<?php echo($info->login_with == 'twitter') ? "display:block":"display:none;"?>">
                               <!--  <div class="item form-group" style="padding: 30px 0 0;">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Consumer Key<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="consumer_keys" class="form-control" value="<?php echo $apikeys['twitter']['apikey']?>">
                                  </div>
                                </div>
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Consumer Secret<span class="required"></span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="consumer_secret" class="form-control" value="<?php echo $apikeys['twitter']['apisecret']?>">
                                  </div>
                                </div> -->
                                 <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="website_url_tweeter" class="form-control" value="<?php echo isset($socials['twitter']['website']) ? $socials['twitter']['website'] : "";?>">
                                  </div>
                                </div>
                                <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" name="contact_number_tweeter" class="form-control" value="<?php echo isset($socials['twitter']['contact']) ? $socials['twitter']['contact'] : "";?>">
                                </div>
                                </div>
                                <input type="hidden" name="channelname3" value="twitter">
                              </div>
                              <!-- <div class="ln_solid"></div>
                               <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="website_url" class="form-control" value="<?php echo $chatdetails->website_url; ?>">
                                  </div>
                              </div>
                                <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" name="contact_number" class="form-control" value="<?php echo $chatdetails->contact_number; ?>">
                                </div>
                              </div> -->
                              <div class="ln_solid"></div>
                                <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnchannel" id="btnchannel" value="Save" class="btn btn-success">
                                </div>
                              </div>
                             </form>
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="delivery-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Do You Offer Delivery Services ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select class="form-control" name="serviceprovide" id="serviceprovide">
                                      <option value="0" <?php echo($chatdetails->delivery_services == 0) ? "selected" : ""?>>No</option>
                                      <option value="1" <?php echo($chatdetails->delivery_services == 1) ? "selected" : ""?>>Yes</option>
                                  </select>
                                </div>
                              </div>
                              <div class="item form-group services-offer" style="<?php echo($chatdetails->delivery_services == 1) ? "display:block":"display:none"?>">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Delivery Options ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12" <?php echo($chatdetails->delivery_services == 1) ? "style=display:block":"display:none"?>>
                                  <div class="checkbox">
                                    <label>
                                    <input type="checkbox" value="Postal Service" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Postal Service", $providers)) ? "checked" : "";?>>Postal Service
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Standard Delivery 5 + Days" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Standard Delivery 5 + Days", $providers)) ? "checked" : "";?>>Standard Delivery 5 + Days 
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Express Delivery 2 - 4 Days" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Express Delivery 2 - 4 Days", $providers)) ? "checked" : "";?>>Express Delivery 2 - 4 Days
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Next Day Delivery" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Next Day Delivery", $providers)) ? "checked" : "";?>>Next Day Delivery
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="Same Day Delivery" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Same Day Delivery", $providers)) ? "checked" : "";?>> Same Day Delivery
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="International Standard" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("International Standard", $providers)) ? "checked" : "";?>>International Standard
                                    </label>
                                  </div>
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" value="International Express" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("International Express", $providers)) ? "checked" : "";?>>International Express
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Do You Offer Returns ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select class="form-control" id="returnoffer" name="offer">
                                      <option value="0" <?php echo($chatdetails->return_offer == 0) ? "selected":"";?>>No</option>
                                      <option value="1" <?php echo($chatdetails->return_offer == 1) ? "selected":"";?>>Yes</option>
                                  </select>
                                 <!--  <div class="radio">
                                      <label class="">
                                        <div class="iradio_flat-green"><input type="radio" class="flat" name="offer" value="1" <?php echo($chatdetails->return_offer == 1) ? "checked":"";?>>Yes</div>
                                      </label>
                                       <label class="">
                                        <div class="iradio_flat-green"><input type="radio" class="flat" name="offer" value="0" <?php echo($chatdetails->return_offer == 0) ? "checked":"";?>>No</div>
                                      </label>
                                  </div> -->
                                </div>
                              </div>
                               <div class="item form-group offer-return" style=<?php echo($chatdetails->return_offer == 1) ? "display:block":"display:none";?>>
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Returns Days ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="checkbox services-days">
                                    <label>
                                      <input type="checkbox" value="7" name="daysoffer[]" <?php echo(!empty($chatdetails->offer_days) && in_array("7", $offerdays)) ? "checked" : "";?>> 7 Days
                                    </label>
                                  </div>
                                  <div class="checkbox services-days">
                                    <label>
                                      <input type="checkbox" value="14" name="daysoffer[]" <?php echo(!empty($chatdetails->offer_days) && in_array("14", $offerdays)) ? "checked" : "";?>>14 Days
                                    </label>
                                  </div>
                                 <!--  <select class="form-control offersdays" name="daysoffer">
                                      <option value="0" <?php echo($chatdetails->offer_days == 0) ? "selected":"";?>>-Select Days-</option>
                                      <option value="7" <?php echo($chatdetails->offer_days == 7) ? "selected":"";?>>7 Days</option>
                                      <option value="14" <?php echo($chatdetails->offer_days == 14) ? "selected":"";?>>14 Days</option>
                                  </select> -->
                                </div>
                              </div>
                              <div class="ln_solid"></div>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnservice" id="btnservice" value="Save" class="btn btn-success">
                                </div>
                              </div>
                            </form>
                          </div>
                          <div role="tabpanel" class="tab-pane fade <?php echo($this->session->userdata('isFacebook') == true) ? 'active in' : ''?>" id="tab_content6" aria-labelledby="payment-tab">
                            <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                             <!-- <div class="row">
                                  <p><strong>Signup for Rave by Flutterwave http://rave.flutterwave.com</strong></p>
                                  <p><strong>Go to the Settings Tab, then API</strong><i class="fa fa-info-circle" style="font-size: 17px;margin-left: 5px;" data-toggle="tooltip" data-original-title="Payment Settings"></i></p>
                                  <p><strong>Copy the Public Key and Private Key, paste them into the fields below and Save</strong><i class="fa fa-info-circle" style="font-size: 17px;margin-left: 5px;" data-toggle="tooltip" data-original-title="public Key and Secret Key"></i></p>
                             </div>
                             <br> -->
                             <div class="row">
                               <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"><img src="<?php echo base_url()?>assets/img/flutterwave.png" alt="" width="150">
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <p><?php echo strtoupper("connect your rave account");?></p>
                                  <p>(1) Login to you Rave Account. If you don't have one, signup here <b>https://rave.flutterwave.com</b></p>
                                  <p>(2) Go to Setting, Tab then click o API &nbsp;<?php if($this->session->userdata('isLogin')=="social"){ ?>  <a class="btn btn-xs btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-video-camera" aria-hidden="true"></i></a><?php } ?></p>
                                  <p>(3) Copy the Private Key and Public Key. Paste them into the fields below.</p>
                                </div>
                              </div>
                             </div>

                             <div class="row">
                               <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Public Key<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" name="public_key" value="<?php echo(trim($chatdetails->public_key));?>" class="form-control">
                                </div>
                              </div>
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Secret Key<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" name="secret_key" value="<?php echo(trim($chatdetails->secret_key));?>" class="form-control">
                                </div>
                              </div>
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <p style="padding: 5px;">Your 14 Day trial will end on <b><?php 
                                  $createddate = strtotime(date("Y-m-d", strtotime($info->created_date)) . " +14 day"); // +1 month
                                  $nextdate = date("Y-m-d",$createddate);
                                  echo $nextdate; ?></b><br>
                                  <?php
                                  $date = strtotime(date("Y-m-d", strtotime($registereddate)) . " +14 day"); // +1 month
                                  $date = date("Y-m-d",$date);
                                  //echo $date;
                                  ?>
                                  Next Bill will be ready on : <span class="label label-info"><?php echo $date?></span> </p>
                                </div>
                              </div>
                             </div>
                             <div class="ln_solid"></div>
                              <!-- <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Auto renew payment ?<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="subscriptionplan" id="subscriptionplan" value="<?php echo($chatdetails->renew_subscription == "1") ? "1":"0";?>" <?php echo($chatdetails->renew_subscription == "1") ? "checked":"";?>>
                                      ($30 subscription package for every month)
                                    </label>
                                  </div>
                                  <br>
                                  <div class="recurringlink" style="<?php echo($chatdetails->renew_subscription == "1") ? 'display:block' : 'display:none';?>">
                                    <span><b>Note : Please click on below links for auto subscription payment</b></span>
                                    <a href="https://ravesandbox.flutterwave.com/pay/autocharge" id="paylink" target="_blank">https://ravesandbox.flutterwave.com/pay/autocharge</a>
                                  </div>
                                </div>
                              </div> -->
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnpayment" id="btnpayment" value="Save" class="btn btn-success">
                                </div>
                              </div>
                             </form>
                             <form class="form-horizontal form-label-left" novalidate method="post" enctype="multipart/form-data" action="<?php echo base_url()?>admin/paywithflutterwave"><!-- https://hosted.flutterwave.com/processPayment.php -->
                              <!-- <div class="row">
                                <h4><b>Note : $100 subscription package for every month</b>
                                </h4>
                              </div>
                              <div class="row">
                                <h4><b>Note : You are currently on the 14 Day trial version. After this ends, you will need to pay to use Akinai</b>
                                </h4>
                              </div>
                              <div class="row">
                                <h4><b>Note : Next Bill will be ready on : </b> <span class="label label-info">
                                  <?php
                                  $date = strtotime(date("Y-m-d", strtotime($registereddate)) . " +1 month");
                                  $date = date("Y-m-d",$date);
                                  echo $date;
                                  ?>
                                  </span>
                                </h4>
                              </div>
                              <div class="row">
                                <h4><b>Note : If you do not pay 3 days after your bill has been sent, your account will be deactivated.</b>
                                </h4>
                              </div> -->
                              <?php
                              if($afteronemonth == 1):
                              ?>
                              <div class="ln_solid"></div>
                              <input type="hidden" name="amount" value="0" id="payamount"/> <!-- Replace the value with your transaction amount -->
                              <input type="hidden" name="payment_method" value="both" />
                              <!-- Replace the value with your transaction description -->
                              <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                              <input type="hidden" name="currency" value="USD" /> <!-- Replace the value with your transaction currency -->
                              <input type="hidden" name="email" value="<?php echo $info->admin_email;?>"/> <!-- Replace the value with your customer email -->
                              <input type="hidden" name="ref" value="<?php echo uniqid();?>" />
                              <!-- <input type="hidden" name="env" value="staging"> --> 
                              <!-- <input type="hidden" name="successurl" value="<?php echo base_url()?>order/subscription/?name=success">
                              <input type="hidden" name="failureurl" value="<?php echo base_url()?>order/subscription/?name=failed"> -->

                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">When Subscription due<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="100" name="servicescharge" id="servicescharge">$100 (Ritekit Plan)
                                  </label>
                                  <br><br>
                                  <p>
                                    <b>Social Media Marketing</b> 
                                    <ul>
                                      <li>Set Hashtags and Keywords for your business</li>
                                      <li>Find Influencers for your business and mention them in your posts </li>
                                      <li>Schedule up to 5 enhanced posts at a time across <li>Facebook, Twitter and Instagram</li>
                                      <li>Promoted post with popular blogs on Facebook, Instagram and Twitter</li>
                                    </ul>
                                  </p>
                                  <div class="ln_solid"></div>
                                 </div>
                                </div>
                              </div>
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Auto renew payment ?<span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="subscriptionplan" id="subscriptionplan" value="<?php echo($chatdetails->renew_subscription == "1") ? "1":"0";?>" <?php echo($chatdetails->renew_subscription == "1") ? "checked":"";?>>
                                    </label>
                                  </div>
                                  <!-- <br>
                                  <div class="recurringlink" style="<?php echo($chatdetails->renew_subscription == "1") ? 'display:block' : 'display:none';?>">
                                    <span><b>Note : Please click on below links for auto subscription payment</b></span>
                                    <a href="https://ravesandbox.flutterwave.com/pay/autocharge" id="paylink" target="_blank">https://ravesandbox.flutterwave.com/pay/autocharge</a>
                                  </div> -->
                                </div>
                              </div>
                              <!-- <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                 <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="165" name="payment_plan" id="planchecked">Auto Charge Vendor for Subscription Service (Please tick) ?
                                  </label>
                                 </div>
                               </div>
                              </div> -->
                              <?php 
                              $extenddate = date('Y-m-d', strtotime($date. ' + 3 days')); // Add  3 days in due date
                              $date1 = date_create($extenddate);
                      
                              $date2 = date_create(date('Y-m-d'));
                              //difference between two dates
                              $diff = date_diff($date1,$date2);
                              $duedays = $diff->format("%a"); // Difference in days
                              if(date('Y-m-d') === $date || date('Y-m-d') <= $extenddate) : ?>
                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <input type="submit" name="btnpay" id="btnpay" value="Payment" class="btn btn-info">
                                  <br><br>
                                  <h4><span class="label label-primary">Note : No need to pay if you subscribed auto renew option.</span></h4>
                                </div>
                              </div>
                            <?php endif;?>
                             </form>
                             <?php
                              endif;
                             ?>
                          </div>
                        <?php endif;?>
                        </div>

                      <?php
                      if(!is_null($info->payment_link)):
                      ?>
                        <div class="text-center">
                          <h4><b>Payment Link :- </b><?php echo $info->payment_link;?></h4>
                        </div>
                        <?php
                      endif;
                      ?>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Payment Setup</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/Flutterwave_Keys.mov" >
      </video>
      </div>
      </div>
      </div>
  </div>

  <div class="modal fade bs-pageadmin-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Page Admin Role Assign</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/Fb-pageadmin.mov" >
      </video>
      </div>
      </div>
      </div>
  </div>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                     <a href="<?php echo base_url('customer/exportCitizens')?>" class="btn btn-md btn-success pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                    <div class="clearfix"></div>
                  </div>
                <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                </div>
                <?php endif; ?>
                <div class="panel panel-default">
                <div class="panel-body">
                <form id="form-filter" class="form-horizontal">
                    <div class="form-group">
                        <label for="timepick" class="col-sm-2 control-label">Time Period</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="timepick">
                                <option value="all">All Time</option>
                                <option value="week">This Week</option>
                                <option value="month">This Month</option>
                                <option value="last_month">Last Month</option>
                            </select>
                        </div>
                    </div>
                </form>
                </div>
                </div>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table id="table-order-21" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                              <th>Customer ID</th>
                              <th>Customer Name</th>
                              <th>Customer State</th>
                              <th>Member Since</th>
                              <th>Average Spend</th>
                              <th>Last Order Made</th>
                              <th>Last Login</th>
                              <th>Consecutive Logins</th>
                              <th>Sellers Onboarded</th>
                            </thead>
                          <tbody>
                          </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>

<script type="text/javascript">

$(document).ready(function() {
    //datatables
    var table = $('#table-order-21').DataTable({ 
 
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('customer/ajax_citizenlist')?>",
            "type": "POST",
            "data": function ( data ) {
                data.time = $('#timepick').val();
                data.filters = $('#otherfilter').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });

    $('#timepick').change(function(){
        table.ajax.reload();
    });

    $("#otherfilter").change(function(){
       table.ajax.reload();
    });
 
});
 
</script>
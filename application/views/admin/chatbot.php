<?php 
  $this->load->model('Chat_prefrences_model');
  $this->load->model('Chatbots_model'); 
  $chatdetails = $this->Chatbots_model->getChatData();  
?>



<div class="right_col" role="main" style="min-height: 949px;">



          <div class="">



            <div class="clearfix"></div>



            <div class="row">



              <div class="col-md-12 col-sm-12 col-xs-12">



                <div class="x_panel">



                  <div class="x_title">



                    <h2><?php echo $title ?></h2>



                    <div class="clearfix"></div>



                  </div>



                  <div class="x_content">



                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>



                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">



                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>



                      </button>



                        <?php echo $this->session->flashdata('flashmsg'); ?>



                      </div>


                    <?php endif; ?>
                    <input type="hidden" id="base_url" value="<?php echo base_url();?>">  

                    <?php 
                    if(empty($this->session->userdata('isLogin'))):
                    ?>

                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>admin/savePrefrences"  method="post">



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="welcome_reply">Welcome Message <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input id="welcome_reply" class="optional form-control col-md-7 col-xs-12" name="welcome_reply" value="<?php echo($this->Chat_prefrences_model->get()->welcome_msg ? $this->Chat_prefrences_model->get()->welcome_msg : '') ?>" type="text">



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="welcome_reply_thai">Welcome Message Thai <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="welcome_reply_thai" class="optional form-control col-md-7 col-xs-12" name="welcome_reply_thai" value="<?php echo($this->Chat_prefrences_model->get()->welcome_msg_thai ? $this->Chat_prefrences_model->get()->welcome_msg_thai : '') ?>" type="text">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="faq_message">FAQ Message <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="text" id="faq_message" name="faq_message" class="optional form-control col-md-7 col-xs-12" value="<?php echo($this->Chat_prefrences_model->get()->faq_msg ? $this->Chat_prefrences_model->get()->faq_msg : '') ?>">



                        </div>



                      </div>



                       <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="faq_msg_thai">FAQ Message Thai <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="text" id="faq_msg_thai" name="faq_message_thai" class="optional form-control col-md-7 col-xs-12" value="<?php echo($this->Chat_prefrences_model->get()->faq_msg_thai ? $this->Chat_prefrences_model->get()->faq_msg_thai : '') ?>">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dismiss_message">Dismiss Message



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="text" id="dismiss_message" name="dismiss_message" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->dismiss_msg ? $this->Chat_prefrences_model->get()->dismiss_msg : '') ?>">



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dismiss_msg_thai">Dismiss Message Thai

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="text" id="dismiss_msg_thai" name="dismiss_message_thai" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->dismiss_msg_thai ? $this->Chat_prefrences_model->get()->dismiss_msg_thai : '') ?>">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="feedback_message">Feedback Message



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="text" id="feedback_message" name="feedback_message" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->feedback_msg ? $this->Chat_prefrences_model->get()->feedback_msg : '') ?>">



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="feedback_msg_thai">Feedback Message Thai

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="text" id="feedback_msg_thai" name="feedback_message_thai" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->feedback_msg_thai ? $this->Chat_prefrences_model->get()->feedback_msg_thai : '') ?>">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_confirm">Product Confirm Message



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="text" id="product_confirm" name="product_confirm" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->product_confirm_msg ? $this->Chat_prefrences_model->get()->product_confirm_msg : '') ?>">



                        </div>



                      </div>



                       <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_confirm_msg_thai">Product Confirm Message Thai

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="text" id="product_confirm_msg_thai" name="product_confirm_thai" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->product_confirm_msg_thai ? $this->Chat_prefrences_model->get()->product_confirm_msg_thai : '') ?>">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="order_cost">Order Cost Message



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="text" id="order_cost" name="order_cost" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->order_cost_msg ? $this->Chat_prefrences_model->get()->order_cost_msg : '') ?>">



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="order_cost_msg_thai">Order Cost Message Thai

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="text" id="order_cost_msg_thai" name="order_cost_thai" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->order_cost_msg_thai ? $this->Chat_prefrences_model->get()->order_cost_msg_thai : '') ?>">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment_confirm">Payment Confirm Message



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input id="payment_confirm" type="text" name="payment_confirm" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->payment_confirm_msg ? $this->Chat_prefrences_model->get()->payment_confirm_msg : '') ?>">



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment_confirm_msg_thai">Payment Confirm Message Thai

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="payment_confirm_msg_thai" type="text" name="payment_confirm_thai" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrences_model->get()->payment_confirm_msg_thai ? $this->Chat_prefrences_model->get()->payment_confirm_msg_thai : '') ?>">

                        </div>

                      </div>



                      <div class="ln_solid"></div>



                      <div class="form-group">



                        <div class="col-md-6 col-md-offset-3">



                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>



                        </div>



                      </div>



                      <input type="hidden" name="last_id" value="<?php if(isset($this->Chat_prefrences_model->get()->id)){echo $this->Chat_prefrences_model->get()->id;}else{echo '';} ?>">

                    </form>
                    <?php elseif(empty($this->session->userdata('isLoginexists')) && $chatdetails == false) : ?>
                      <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>user/savechatBot"  method="post">
                          <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12"><h2><i class="fa fa-info-circle" style="font-size: 30px;margin-right: 10px;"></i> <strong style="position: relative;top: -6px;">Store Infromation</strong></h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Opening and Closing Time <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="businesshours" id="businesshours">
                                    <option value="alltime">24 hours</option>
                                    <option value="custom">Custom</option>
                                </select>
                              </div>
                            </div>
                            <div class="item form-group" id="times" style="display: none">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">
                              </label>
                              <div class="col-md-8 col-sm-8 col-xs-12">
                                 <div class="range-day" id="range-day-1" data-day="1">
                                    <input type="checkbox" name="day-1" id="day-1" value="Mon" class="range-checkbox">
                                    <label for="day-1" class="range-label">Monday:</label>
                                    <div id="range-slider-1" class="range-slider"></div>
                                    <span id="range-time-1" class="range-time"></span>
                                  </div>
                                  
                                  <div class="range-day" id="range-day-2" data-day="2">
                                    <input type="checkbox" name="day-2" id="day-2" value="Tue" class="range-checkbox">
                                    <label for="day-2" class="range-label">Tuesday:</label>
                                    <div id="range-slider-2" class="range-slider"></div>
                                    <span id="range-time-2" class="range-time"></span>
                                  </div>
                                  
                                  <div class="range-day" id="range-day-3" data-day="3">
                                    <input type="checkbox" name="day-3" id="day-3" value="Wed" class="range-checkbox">
                                    <label for="day-3" class="range-label">Wednesday:</label>  
                                    <div id="range-slider-3" class="range-slider"></div>
                                    <span id="range-time-3" class="range-time"></span>
                                  </div>
                                  
                                  <div class="range-day" id="range-day-4" data-day="4">
                                    <input type="checkbox" name="day-4" id="day-4" value="Thu" class="range-checkbox">
                                    <label for="day-4" class="range-label">Thursday‎:</label>  
                                    <div id="range-slider-4" class="range-slider"></div>
                                    <span id="range-time-4" class="range-time"></span>
                                  </div>
                                  
                                  <div class="range-day" id="range-day-5" data-day="5">
                                    <input type="checkbox" name="day-5" id="day-5" value="Fri" class="range-checkbox">
                                    <label for="day-5" class="range-label">Friday:</label>  
                                    <div id="range-slider-5" class="range-slider"></div>
                                    <span id="range-time-5" class="range-time"></span>
                                  </div>
                                  
                                  <div class="range-day" id="range-day-6" data-day="6">
                                    <input type="checkbox" name="day-6" id="day-6" value="Sat" class="range-checkbox">
                                    <label for="day-6" class="range-label">Saturday:</label>  
                                    <div id="range-slider-6" class="range-slider"></div>
                                    <span id="range-time-6" class="range-time"></span>
                                  </div>
                                  
                                  <div class="range-day" id="range-day-7" data-day="7">
                                    <input type="checkbox" name="day-7" id="day-7" value="Sun" class="range-checkbox">
                                    <label for="day-7" class="range-label">Sunday:</label>  
                                    <div id="range-slider-7" class="range-slider"></div>
                                    <span id="range-time-7" class="range-time"></span>
                                  </div>
                                  <input type="hidden" name="days[]" value="" id="range-time-val-1" class="range-time-val">
                                  <input type="hidden" name="days[]" value="" id="range-time-val-2" class="range-time-val">
                                  <input type="hidden" name="days[]" value="" id="range-time-val-3" class="range-time-val">
                                  <input type="hidden" name="days[]" value="" id="range-time-val-4" class="range-time-val">
                                  <input type="hidden" name="days[]" value="" id="range-time-val-5" class="range-time-val">
                                  <input type="hidden" name="days[]" value="" id="range-time-val-6" class="range-time-val">
                                  <input type="hidden" name="days[]" value="" id="range-time-val-7" class="range-time-val">
                                  <!-- <div class="servicetime" style="display: none"></div> -->
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12"><h2><i class="fa fa-rocket" style="font-size: 30px;margin-right: 10px;"></i> <strong style="position: relative;top: -6px;">Delivery and Returns</strong></h2>
                            </div>
                          </div>
                          <div class="row">
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Do You Offer Delivery Services ? <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="serviceprovide" id="serviceprovide">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                              </div>
                            </div>
                            <div class="item form-group services-offer" style="display: none">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Delivery Options ? <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12 service-provide">
                                <div class="checkbox">
                                  <label>
                                  <input type="checkbox" value="Postal Service" name="services[]">Postal Service
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="Standard Delivery 5 + Days" name="services[]">Standard Delivery 5 + Days 
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="Express Delivery 2 - 4 Days" name="services[]">Express Delivery 2 - 4 Days
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="Next Day Delivery" name="services[]">Next Day Delivery
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="Same Day Delivery" name="services[]"> Same Day Delivery
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="International Standard" name="services[]">International Standard
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="International Express" name="services[]">International Express
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Do You Offer Returns ? <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="returnoffer" name="offer">
                                      <option value="0">No</option>
                                      <option value="1">Yes</option>
                                  </select>
                                  <!-- <div class="radio">
                                      <label class="">
                                        <div class="iradio_flat-green"><input type="radio" class="flat" name="offer" value="1">Yes</div>
                                      </label>
                                       <label class="">
                                        <div class="iradio_flat-green"><input type="radio" class="flat" name="offer" value="0" checked>No</div>
                                      </label>
                                  </div> -->
                                </div>
                            </div>
                             <div class="item form-group offer-return" style="display:none">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Returns Days ? <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                               <!--  <select class="form-control offersdays" name="daysoffer">
                                    <option value="0">-Select Days-</option>
                                    <option value="7">7 Days</option>
                                    <option value="14">14 Days</option>
                                </select> -->
                                <div class="checkbox">
                                  <label>
                                  <input type="checkbox" value="0" name="daysoffer[]">7 Days
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" value="14" name="daysoffer[]">14 Days
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12"><h2><i class="fa fa-comments" style="font-size: 30px;margin-right: 10px;"></i> <strong style="position: relative;top: -6px;">Business Contact</strong></h2>
                            </div>
                          </div>
                          <div class="row">
                            <!-- <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Connect Your Media Channel
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="radio">
                                  <label>
                                    <div>
                                    <input type="radio" checked value="facebook" id="optionsRadios3" name="optchannel"></div><i class="fa fa-facebook fa-6" style="font-size: 30px;margin-right: 10px;" aria-hidden="true"></i>
                                  </label>
                                  <label>
                                    <div>
                                    <input type="radio" checked value="insta" id="optionsRadios1" name="optchannel"></div><i class="fa fa-instagram fa-6" style="font-size: 30px;margin-right: 10px;" aria-hidden="true"></i>
                                  </label>
                                  <label>
                                    <div>
                                    <input type="radio" value="twitter" id="optionsRadios2" name="optchannel"></div><i class="fa fa-twitter fa-6" style="font-size: 30px;" aria-hidden="true"></i>
                                  </label>
                                </div>
                              </div> -->
                            </div>
                            <!-- <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Page
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="chatbot_page" class="form-control" required="required" >
                                    <option value="">Select page</option>
                                    <?php foreach ($store_pages as $rows) { ?>
                                      <option value="<?php echo $rows->page_id ?>"><?php echo $rows->page_name; ?></option>  
                                    <?php   } ?>
                                  </select>
                              </div>
                            </div> -->  
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" name="website_url" class="form-control">
                              </div>
                            </div>
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="number" name="contact_number" class="form-control">
                              </div>
                            </div>
                          </div>
                          <!-- <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-12"><h2><i class="fa fa-calculator" style="font-size: 30px;margin-right: 10px;"></i> <strong style="position: relative;top: -6px;">Receive Payments from Customers</strong></h2>
                              <div style="position: relative;left: 6%;">
                              <p style="margin-bottom: 20px;">The Chatbot is be able to check payment confirmation for each of your Orders.</p>
                              <p style="margin: 0">Signup for a Flutterwave Account here.</p>
                              <p style="margin: 0">Go to Your Settings Tab then API <i class="fa fa-info-circle" style="font-size: 15px;margin-left: 5px;" data-toggle="tooltip" data-original-title="Payment Settings"></i></p>
                              <p style="margin-bottom: 0px;">Copy Paste your Public Key and Secret Key below <i class="fa fa-info-circle" style="font-size: 15px;margin-left: 5px;" data-toggle="tooltip" data-original-title="Public key and Secret key"></i></p>
                              <p style="margin-bottom: 35px">You can find this in the "My Account" Tab</p>
                              </div>
                            </div>
                          </div>
                          <div class="row" style="margin-bottom: 40px">
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Public Key
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" name="public_key" class="form-control">
                              </div>
                            </div>
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Secret Key
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" name="secret_key" class="form-control">
                              </div>
                            </div>
                          </div> -->
                          <div class="row"> 
                            <div class="item form-group">
                              <div class="col-md-4 col-sm-4 col-md-offset-4 col-xs-12">
                                  <input type="submit" name="btnconnect" value="Complete" class="btn btn-md btn-success" id="btnconnect">
                                  <input type="reset" name="btnreset" value="Cancel" class="btn btn-md btn-info">
                              </div>
                            </div>
                          </div>
                      </form>
                    <?php else :?>
                      <div class="row">
                        <span><b>Important Note : </b></span><br><br>
                        <?php if($chatdetails):?>
                        You are successfully setup chatbot ! <br><br>
                        Please add facebook page details for interaction with chatbot. <br><br>
                        Navigate to stores->Add store menu
                        <div class="ln_solid"></div>
                        <?php
                        $chatdata = $this->db->select('*')->from('stores')->where('created_by',$this->session->userdata('adminId'))->get()->result();
                        //var_dump($this->session->userdata('adminId'));
                        if($chatdata):
                        ?>
                        <form class="form-horizontal form-label-left" novalidate method="post">
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Page Token
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select class="form-control" id="fbpage">
                                  <option value="0">-Select Page-</option>
                                  <?php
                                  $options = "";
                                  foreach ($chatdata as $chat):
                                  ?>
                                  <option value="<?php echo $chat->access_token;?>"><?php echo $chat->page_name;?></option>
                                  <?php
                                  endforeach;
                                  ?>    
                                  </select>
                              </div>
                            </div>
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Whitelist Domain
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" id="website_url" class="form-control">
                              </div>
                            </div>
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="button" name="btnauthenticate" id="btnauthenticate" value="Authenticate" class="btn btn-sm btn-info">
                              </div>
                            </div>
                            <div class="item form-group">
                               <label class="control-label col-md-3 col-sm-3 col-xs-12">
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <h4><span class="auth"></span></h4>
                              </div>
                            </div>
                        </form>
                      <?php endif;?>
                        <?php else :?>
                        Please setup your chatbot !<br><br>
                        Click here to <a href="<?php echo base_url()?>admin/setupchatbot" class="btn btn-xs btn-info">Setup Bot</a>  
                        <?php endif;?>
                      </div>
                    <?php endif;?>
                    

                  </div>



                </div>



            </div>



        </div>



    </div>



</div>
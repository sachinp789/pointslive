<?php $this->load->model('Chat_prefrence_model'); ?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>admin/savePrefrences"  method="post">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_name">Name : <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="welcome_reply" class="optional form-control col-md-7 col-xs-12" name="category_name" value="" type="text">
                        </div>
                      </div>
              <!--         <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_">Sale Price : <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="faq_message" name="faq_message" class="optional form-control col-md-7 col-xs-12" value="<?php echo($this->Chat_prefrence_model->get()->faq_msg ? $this->Chat_prefrence_model->get()->faq_msg : '') ?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dismiss_message">Dismiss Message
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="dismiss_message" name="dismiss_message" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrence_model->get()->dismiss_msg ? $this->Chat_prefrence_model->get()->dismiss_msg : '') ?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="feedback_message">Feedback Message
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="feedback_message" name="feedback_message" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrence_model->get()->feedback_msg ? $this->Chat_prefrence_model->get()->feedback_msg : '') ?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_confirm">Product Confirm Message
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="product_confirm" name="product_confirm" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrence_model->get()->product_confirm_msg ? $this->Chat_prefrence_model->get()->product_confirm_msg : '') ?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="order_cost">Order Cost Message
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="order_cost" name="order_cost" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrence_model->get()->order_cost_msg ? $this->Chat_prefrence_model->get()->order_cost_msg : '') ?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment_confirm">Payment Confirm Message
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="payment_confirm" type="text" name="payment_confirm" class="optional form-control col-md-7 col-xs-12"  value="<?php echo($this->Chat_prefrence_model->get()->payment_confirm_msg ? $this->Chat_prefrence_model->get()->payment_confirm_msg : '') ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>
                        </div>
                      </div>
                      <input type="hidden" name="last_id" value="<?php if(isset($this->Chat_prefrence_model->get()->id)){echo $this->Chat_prefrence_model->get()->id;}else{echo '';} ?>"> -->

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button id="new_category" type="submit" class="btn btn-success" name="new_category">Save</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                <div class="x_content">
                	<section class="content invoice">
                		<div class="row">
                		<?php
                		$this->load->model('Products_model');
	                  	$this->load->model('Product_images_model');
                		$checkshared = $this->db->select('*')->from('schedulars')->where('parent_id',$schedulars[0]->id)->get()->result();
	                  	$products = $this->Products_model->where('product_id',$schedulars[0]->product_id)->get();
	                    $productsimages = $this->Product_images_model->where('product_id',$schedulars[0]->product_id)->get();
                		?>
                        <div class="col-xs-12 invoice-header">
                          <h1>
	                          <?php
	                          echo ucfirst($schedulars[0]->choose_item);
	                          ?>
	                          <small class="pull-right">Share on :- <?php
	                          echo $schedulars[0]->share_on;
	                          ?></small>
	                      </h1>
	                      <hr>
	                      <?php
	                      if($schedulars[0]->choose_item == 'events'):
	                      $pageid = is_null($page) ? '1048681488539732' : $page;	
	                      $token = getFacebookpagetoken($pageid);  
				          $url = "https://graph.facebook.com/{$schedulars[0]->product_id}?fields=name&access_token=".$token[0]->access_token; 
				          $ch = curl_init();
				          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
				          curl_setopt($ch,CURLOPT_URL,$url);
				          curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				          $content = curl_exec($ch);
				          $result = json_decode($content);
				          curl_close($ch);
	                      ?>
	                      <p><h2>Event Name : <?php echo @$result->name;?> </h2><hr></p>
	                      <p><h2>Description : <?php echo $schedulars[0]->post_description;?> </h2><hr></p>
	                      <p>
	                      	<?php if($schedulars[0]->video_link !=""):?>
	                      	<h2>Youtube URL :
	                      	<span><?php echo $schedulars[0]->video_link; ?></span></h2>
	                      	<?php elseif($schedulars[0]->post_image != ""): ?>
	                      	<h2>Posted Image</h2>
	                      	<!-- <img src="<?php echo base_url()?>/uploads/events/<?php echo $schedulars[0]->post_image?>" width="100"> -->
	                      	<img src="<?php echo $schedulars[0]->post_image?>" width="100">	
	                      	<?php endif;?>
	                      <hr>
	                      </p>
	                      <p>
	                      	<h2>Post Status : 
	                      	<?php
	                      	
	                      	$contain = array();
	                          foreach ($checkshared as $key => $value){ 
	                            $contain[] = $value->is_share;
	                          }
	                          if(in_array("0", $contain)){
	                            echo '<span class="label label-default">Pending</span>';
	                          }
	                          else{
	                            echo '<span class="label label-success">Success</span>';
	                          }
	                      	?>
	                      </h2>	
	                      <hr>
	                      </p>
	                  	  <?php else :?>
	                  	  <p>
	                  	  	<?php
	                  	  	
	                  	  	?>
	                  	  	<h2>Product Name : <?php echo $products->product_name;?> </h2><hr>
	                  	  </p>	
	                  	  <p><h2>Description : <?php echo $schedulars[0]->post_description;?> </h2><hr></p>
	                  	  <p>
	                      	<?php if($schedulars[0]->video_link !=""):?>
	                      	<h2>Youtube URL :
	                      	<span><?php echo $schedulars[0]->video_link; ?></span></h2>
	                      	<?php else :
	                      
	                      	?>
	                      	<h2>Posted Image</h2>
	                      	<img src="<?php echo base_url()?>/uploads/products/<?php echo $productsimages->product_image?>" width="100">	
	                      	<?php endif;?>
	                      <hr>
	                      </p>
	                      <p>
	                      	<h2>Post Status : 
	                      	<?php
	                      	$contain = array();
	                          foreach ($checkshared as $key => $value){ 
	                            $contain[] = $value->is_share;
	                          }
	                          if(in_array("0", $contain)){
	                            echo '<span class="label label-default">Pending</span>';
	                          }
	                          else{
	                            echo '<span class="label label-success">Success</span>';
	                          }
	                      	?>
	                      </h2>	
	                      <hr>
	                      </p>
	                  	  <?php endif;?>
                        </div>
                        <!-- /.col -->
                      </div>
                      <a href="<?php echo base_url()?>userengagement/getSchedularLists" class="btn btn-sm btn-default">Back</a>
                	</section>
                </div>
            </div>
        </div>
      </div>
  </div>
</div>
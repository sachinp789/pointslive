<?php 
  $this->load->model('Products_model');
  $productsdata = $this->Products_model->fields('product_name,product_id')->where(['facebook_page' => $page])->get_all();
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php //$msg = $this->uri->segment_array(); 
                  ?>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                            
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>userengagement/updateSchedular" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="schedular_id" value="<?php echo $schedulars->id;?>">    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="choose_item">Choose Product / Event <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control" name="choose_item" id="choose_item" required>
                            <option value="products" <?php if($schedulars->choose_item == 'products'){echo "selected=true"; "disabled";}else{echo "";} ?><?php /*if($schedulars->choose_item == 'events'){echo "disabled";} */?>>Products</option>
                            <option value="events" <?php if($schedulars->choose_item == 'events'){echo "selected=true";}else{echo "";} ?> <?php /*if($schedulars->choose_item == 'products'){echo "disabled";} */?>>Events</option>
                         </select>     
                        </div>
                        <input type="hidden" name="url" id="url" value="<?php echo base_url();?>">
                      </div>
                      <!-- <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="dvTable">
                        </div>
                      </div>
                      </div> -->
                      <?php if($schedulars->choose_item == 'products'): ?>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="dvTable">
                              <?php 
                              $table ='<table border="1" width="100%" id="productist"><tr><th style="text-align:center">#</th><th style="text-align:center">Product Name</th></tr>';
                              $checked="";
                              foreach ($productsdata as $key => $product) {
                                if($schedulars->product_id == $product->product_id){
                                  $checked="checked=true";
                                }
                                else{
                                  $checked = "";
                                }
                                $table.="<tr>";
                                $table.="<td style='text-align:center'><input type=radio value={$product->product_id} name='schedularproduct' $checked></td>";
                                $table.="<td style='text-align:center'>{$product->product_name}</td>";
                                $table.="</tr>";
                              } 
                              $table.="</table>";
                              echo $table;        
                              ?>
                        </div>
                      </div>
                      </div>
                      <?php else : ?>
                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="dvTable">
                        </div>
                      </div>
                      </div>
                      <?php endif;?>
                       <div class="item form-group" style="<?php if($schedulars->choose_item == 'events'){echo 'display:block';}else{echo 'display:none';}?>" id="eventname">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event_name">Event Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="event_name" class="form-control col-md-7 col-xs-12" name="event_name"  type="text" required value="<?php echo $schedulars->event_name?>">
                          <?php echo form_error('event_name','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                     
                      <div class="item form-group" style="<?php if($schedulars->choose_item == 'events'){echo 'display:block';}else{echo 'display:none';}?>" id="eventimgname">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_image">Event Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" id="post_image" name="post_image" class="form-control col-md-7 col-xs-12" <?php if(is_null($schedulars->post_image)){echo "required";}?>>
                          <?php
                          if(!is_null($schedulars->post_image)):?>
                          <input type="hidden" name="post_image_exists" value="<?php echo $schedulars->post_image;?>">
                          <img src="<?php echo base_url();?>/uploads/events/<?php echo $schedulars->post_image; ?>" width="100">
                          <?php endif;?>
                        </div>
                      </div>
                     
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="choose_schedular">Schedular Time Slot  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <!--  <select class="form-control" name="choose_schedular" id="choose_schedular" required>
                            <option value="">--Select Frequency--</option>
                            <option value="intraday" <?php if($schedulars->sharing_frequency == 'intraday'){echo "selected=true";}else{echo "";}?>>Intraday</option>
                            <option value="daily" <?php if($schedulars->sharing_frequency == 'daily'){echo "selected=true";}else{echo "";}?>>Daily</option>
                            <option value="weekly" <?php if($schedulars->sharing_frequency == 'weekly'){echo "selected=true";}else{echo "";}?>>Weekly</option>
                            <option value="monthly" <?php if($schedulars->sharing_frequency == 'monthly'){echo "selected=true";}else{echo "";}?>>Monthly</option>
                         </select> -->
                         <p></p>
                         <?php 
                         $i = 1;
                         foreach ($schedularstimes as $key => $schedular) {
                         ?>
                          <div class='element input-group date' id='myDatepicker4_<?php echo $i ?>'>
                          <?php if($i > 1): ?>
                          <span id="remove_<?php echo $i; ?>" class="remove">X</span>
                          <?php endif;?>
                          <input type='text' class="form-control" id="txt_<?php echo $i ?>" name="sharing_timeslot[<?php echo $i ?>]" readonly="readonly" value="<?php echo $schedular->sharing_time?>" />
                                <span class="input-group-addon">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                          </div>
                          <input type="hidden" name="existid[<?php echo $i ?>]" value="<?php echo $schedular->id ?>">
                         <?php $i++; } ?>
                          <span class='add'>Add More</span>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="share_on">Share On <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <div class="radio">
                              <label>
                                <div><input type="radio" class="flat" <?php if($schedulars->share_on == 'facebook'){echo "checked=true";}else{echo "";}?> value="facebook" name="share_on"></div> Facebook
                              </label>
                              <!-- <label>
                                <div><input type="radio" class="flat" <?php if($schedulars->share_on == 'insta'){echo "checked=true";}else{echo "";}?> value="insta" name="share_on"></div> Instagram
                              </label>
                              <label>
                                <div><input type="radio" class="flat" <?php if($schedulars->share_on == 'both'){echo "checked=true";}else{echo "";}?> value="both" name="share_on" ></div> Both
                              </label> -->
                          </div>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_description"> Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" required name="post_description"><?php echo $schedulars->post_description?></textarea>
                          <?php echo form_error('post_description','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>

                       <!-- <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="events_buylink"> Link Events <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="text" name="video_link" class="form-control">
                        </div>
                      </div> -->

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video_link"> Video URL (optional)
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="text" name="video_link" class="form-control" value="<?php echo $schedulars->video_link?>">
                        </div>
                      </div>
                     
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="submit" name="profilepost" id="profilepost" value="Save" class="btn btn-success">
                        </div>
                      </div>
                  </form>  
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
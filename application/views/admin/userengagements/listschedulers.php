<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
	              <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                  </button>
	                    <?php echo $this->session->flashdata('flashmsg'); ?>
	              </div>
	              <?php endif; ?>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table aria-describedby="datatable_info" role="grid" id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer">
                            <thead>
                            	<th>#</th>
                            	<th>Item</th>
                              <th>Description</th>
                              <!-- <th>Frequency</th> -->
                              <th>Share On</th>
                              <th>Sharing Date & Time</th>
                              <th>Posted</th>
                          		<th>Action</th>
                            </thead>
	                      	<tbody> 
                            <?php 
                            $count=1; 
                            if($schedulars):
                            foreach ($schedulars as $key => $row) { 
                             $checkshared = $this->db->select('*')->from('schedulars')->where('parent_id',$row->id)->get()->result();
                            ?>
                             <tr class="odd" role="row">
                              <td class="sorting_1"> <?php echo $count ?> </td>
                              <td><?php echo $row->choose_item ?> </td>
                              <td><?php echo $row->post_description ?> </td>
                              <!-- <td><?php echo $row->sharing_frequency ?> </td> -->
                              <td><?php echo $row->share_on ?> </td>
                              <td>
                                <?php
                                $myArray = array();
                                foreach ($checkshared as $key => $value){ 
                                    if(!is_null($value->datepicker_time)){
                                      $datepick = $value->datepicker_time;
                                    }
                                    else{
                                       $datepick = $value->sharing_time;
                                    }
                                    $myArray[$key] = '<span>'.ucfirst($datepick).'</span>';
                                }
                                echo '<strong>'.implode('<br> ', $myArray ).'</strong>';
                               ?>
                              </td>
                              <td>
                                <?php 
                                  $contain = array();
                                  foreach ($checkshared as $key => $value){ 
                                    $contain[] = $value->is_share;
                                  }
                                  if(in_array("0", $contain)){
                                    echo '<span class="label label-default">Pending</span>';
                                  }
                                  else{
                                    echo '<span class="label label-success">Success</span>';
                                  }
                                ?> 
                              </td>
                             <td> 
                              <!-- <a href="<?php echo base_url() ?>userengagement/editSchedular/<?php echo $row->id?>" class="btn btn-info btn-xs"> Edit</a>  -->
                              <?php if(in_array("0", $contain)) :?>
                              <a href="<?php echo base_url() ?>userengagement/removeProduct/<?php echo $row->id?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete schedular?');" > Delete</a> 
                              <?php else: ?>
                              <a href="<?php echo base_url() ?>userengagement/viewSchedular/<?php echo $row->id?>" class="btn btn-default btn-xs">View</a>
                              <?php endif; ?>  
                              </td>
                            </tr>
                            <?php $count++;} endif; ?>
	                      	</tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
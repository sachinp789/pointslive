<?php 
$this->load->model('Stores_model'); 
$stores = $this->Stores_model->getStores();  
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php //$msg = $this->uri->segment_array(); 
                  ?>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <input type="hidden" name="url" id="url" value="<?php echo base_url();?>">
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>userengagement/saveSchedular" method="post" enctype="multipart/form-data">
                      <!-- <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_page">Facebook Page <span class="required"></span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                           <select class="select2_single form-control" name="facebook_page" id="facebook_page">
                             <option value="">-Select Facebook Page-</option>
                             <?php
                             $options='';
                             foreach ($stores as $key => $value) {
                              $options.="<option value={$value->page_id}>";
                              $options.= $value->page_name;
                              $options.= "</option>";
                             }
                             echo $options;
                             ?>
                           </select> 
                           <?php if(!$stores):?>
                           <span>Note : Please add store information </span><a href="<?php echo base_url()?>store/addStore" class="btn btn-xs btn-info">Add Store</a>
                          <?php endif;?>
                        </div>
                      </div>
                      <?php else :?>
                      <input type="hidden" name="facebook_page" id="facebook_page" readonly class=" form-control col-md-7 col-xs-12" value="<?php echo $page; ?>">
                      <?php endif; ?> -->
                      <!-- <input type="hidden" name="page" value="<?php echo $page;?>"> -->
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="choose_item">Choose Product / Event <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control" name="choose_item" id="choose_item" required>
                            <option value="">--Select--</option>
                            <option value="products">Products</option>
                            <option value="events">Events</option>
                         </select> 
                         <div id="dvTable" style="margin-top: 2px;">
                        </div>   
                        </div>
                        <input type="hidden" name="url" id="url" value="<?php echo base_url();?>">
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="choose_media">Choose Media Type <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select class="form-control" name="choose_media" id="choose_media" required>
                            <option value="">--Select--</option>
                            <option value="text">Text</option>
                            <option value="image">Image & Text</option>
                            <option value="video">Video & Text</option>
                         </select>    
                        </div>  
                      </div>

                     <!--  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="dvTable">
                        </div>
                      </div>
                      </div> -->
                      <div class="item form-group" id="eventname" style="display: none">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event_name">Event Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="event_name" class="form-control col-md-7 col-xs-12" name="event_name"  type="text" required>
                          <?php echo form_error('event_name','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                     
                      <div class="item form-group" id="eventimgname" style="display: none">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_image">Event Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" id="post_image" name="post_image" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>
                     
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Posting Time  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <!--  <select class="form-control" name="choose_schedular" id="choose_schedular" required>
                            <option value="">--Select Frequency--</option>
                            <option value="intraday">Intraday</option>
                            <option value="daily">Daily</option>
                            <option value="weekly">Weekly</option>
                            <option value="monthly">Monthly</option>
                         </select> -->
                         <p></p>
                          <div class='element input-group date' id='myDatepicker4_1'>
                          <input type='text' class="form-control" id="txt_1" name="sharing_timeslot[1]" readonly="readonly" required/>
                                <span class="input-group-addon">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                          </div>
                          <span class='add' style="cursor:pointer">Add More</span>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook">Share On 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <div class="radio social-icon">
                               <label>
                                <div ><input type="checkbox" class="flat" value="facebook" name="share_on[]" id="facebook" style="margin-left:4px"> <i class="fa fa-facebook fa-6" aria-hidden="true"></i></div> 
                                
                              </label> 
                              <label id="insta"><div><input type="checkbox" class="flat" value="insta" name="share_on[]" id="instagram" style="margin-left:4px" ><i class="fa fa-instagram fa-6" aria-hidden="true"></i></div> 
                                </label>
                                <label><div><input type="checkbox" class="flat" value="twitter" name="share_on[]" id="twitter" style="margin-left:4px" id="twitter"><i class="fa fa-twitter fa-6" aria-hidden="true"></i></div> </label>
                             <!--  <label>
                                <div><input type="radio" class="flat" value="insta" name="share_on"></div> Instagram
                              </label>
                              <label>
                                <div><input type="radio" class="flat" value="both" name="share_on" ></div> Both
                              </label> -->
                          </div>
                        </div>
                      </div>

                      <div class="item form-group" style="display: none" id="videouploader">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Upload Video / Add Link<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="file" name="video_upload" id="video_upload" class="form-control" onChange="validate(this.value)">
                         <span>Maximum 1 MB</span>
                         <p><strong>OR</strong></p>
                         <input type="text" name="video_link" id="video_link" class="form-control" onblur="removeFile(this.value)" placeholder="Add link">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_description"> Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" required name="post_description" id="post_description"></textarea>
                          <?php echo form_error('post_description','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hashtags"> Choose Metadata <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="hashtags[]" class="form-control" id="hashtags" multiple>
                            <!-- <option value="">-Select Hashtag-</option> -->
                            <?php
                            $option="";
                            if($hashtags){
                              foreach ($hashtags as $key => $tags) :
                                $option.="<option value={$tags->tagname}>";
                                $option.=$tags->tagname;
                                $option.="</option>";     
                              endforeach;
                            }
                            else{
                              $option.="No tags";
                            }
                            echo $option;
                            ?>
                          </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trendhashtags">Hashtag Suggestion <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="trendhashtags[]" class="form-control" multiple="" id="trendhashtags">
                          </select>
                          <!-- <textarea name="trendhashtags" id="trendhashtags" class="form-control" readonly></textarea> -->
                          <input type="button" id="trendinghashtagslist" value="Generate Hashtag Suggestions" class="btn btn-xs btn-primary" onclick="createTrendingHashtag()">
                          <span id="load"></span>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="influencers"> Influencers <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="influencers[]" class="form-control" multiple id="influencers">
                          </select>
                          <!-- <textarea name="trendhashtags" id="trendhashtags" class="form-control" readonly></textarea> -->
                          <input type="button" id="influencerslist" value="Generate Influence List" class="btn btn-xs btn-primary" onclick="createInfluencers()">
                          <span id="loads"></span>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emoji"> Emojis <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="emoji[]" class="form-control" multiple id="emoji">
                          </select>
                          <!-- <textarea name="emoji" id="emoji" class="form-control" readonly></textarea> -->
                          <input type="button" id="emojilist" value="Generate Emojis List" class="btn btn-xs btn-primary" onclick="createEmojis()">
                          <span id="loadss"></span>
                        </div>
                      </div>

                      <div class="item form-group" style="display: none" id="eventslink">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="events_shortlink"> Short URL <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="text" name="events_shortlink" id="events_shortlink" class="form-control" required>
                         <p>Note : Facebook product URL must be valid</p>
                         <p><span id="shortenlink"></span></p>  
                         <input type="hidden" name="shorturl" id="shorturl">
                         <input type="button" name="generateurl" id="generateurl" value="Generate Short URL" class="btn btn-xs btn-info" onclick="createShortUrl()">
                        </div>
                      </div>

                     <!--  <div class="item form-group" style="display: none" id="eventslink">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="events_shortlink"> Short URL <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="text" name="events_shortlink" id="events_shortlink" class="form-control" required>
                        </div>
                      </div> -->

                      <div class="item form-group" style="display: none">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="events_buylink"> Link Events <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="text" name="events_buylink" id="events_buylink" class="form-control">
                        </div>
                      </div>
<!-- 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video_link"> Video URL (optional) <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <input type="text" name="video_link" id="video_link" class="form-control">
                        </div>
                      </div> -->

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="submit" name="profilepost" id="profilepost11" value="Save" class="btn btn-success">
                        </div>
                      </div>
                  </form>  
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
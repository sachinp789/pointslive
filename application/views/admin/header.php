<?php 
$this->load->model('Admin_model'); 
$userinfo = admin_info($this->session->userdata('adminId'));

if(!$this->session->userdata('isUserLoggedIn')){
   if(is_null($this->session->userdata('isLogin'))){
    redirect(base_url());
   }
}
?>
<!DOCTYPE html>



<html lang="en">



  <head>



    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



    <!-- Meta, title, CSS, favicons, etc. -->



    <meta charset="utf-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1">







    <title><?php echo $title; ?></title>



    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">



    <!-- Bootstrap -->



    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">



    <!-- Font Awesome -->



    <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">



    <!-- NProgress -->



    <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">



    <!-- iCheck -->



    <link href="<?php echo base_url(); ?>assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">



	



    <!-- bootstrap-progressbar -->



    <!-- <link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"> -->



    <!-- JQVMap -->



    <!-- <link href="<?php echo base_url(); ?>assets/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/> -->

    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->



    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">







    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">



    <!-- <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">



    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">



    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">



    <link href="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet"> -->




    <link href="<?php echo base_url(); ?>assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->



    <link href="<?php echo base_url(); ?>assets/build/css/custom.min.css" rel="stylesheet">

    <?php  
    if(!empty($this->session->userdata('isLogin'))){
      $url = $this->uri->uri_string();
      if($url == "admin/profile/".$this->session->userdata('adminId') || $url == "categories/newCategory" ||  $url == "products/newProduct" || $url == "coupon/addCoupon" || $url == "order/listOrders" || $url == "admin/dashboard"):
    ?>
      <link href="<?php echo base_url(); ?>assets/css/enjoyhint.css" rel="stylesheet">
    <?php endif; } ?>
    
    <!-- Hotjar Tracking Code for www.akinai.io -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:930145,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
  </head>

  <body class="nav-md">

    <script type="text/javascript">
      <?php if(isset($chartdata)){ ?>
          var chartdata = <?php echo $chartdata; ?>
      <?php }else{ ?>
          var chartdata = "";
      <?php } ?>
    </script>

    <!-- <script>

        (function(d, s, id){

          var js, fjs = d.getElementsByTagName(s)[0];

          if (d.getElementById(id)) {return;}

          js = d.createElement(s); js.id = id;

          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";

          fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'Messenger'));

    </script> -->



    <div class="container body">



      <div class="main_container">



        <div class="col-md-3 left_col">



          <div class="left_col scroll-view">



            <div class="navbar nav_title" style="border: 0;">



              <a href="<?php echo base_url().'admin/dashboard';?>" class="site_title"><img src="<?php echo base_url() ?>assets/img/logo.png" width="45" height="45" alt=""><!-- <i class="fa fa-paw"></i> <span><?php //echo $this->config->item('globalname'); ?></span> --></a>



            </div>







            <div class="clearfix"></div>







            <!-- menu profile quick info -->



            <div class="profile clearfix">



              <div class="profile_pic">

                <?php
                  $userdata = $this->Admin_model->get($this->session->userdata('adminId'));
                  if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "social"){
                  ?>
                  <img src="<?php echo $userdata->login_picture?>" class="img-circle profile_img">
                  <?php
                  }
                  else if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "seller") {
                  //$userdata = $this->Admin_model->get($this->session->userdata('adminId'));
                  if(is_null($userdata->admin_photo)){
                    $photo = base_url('assets/img/logo.png');
                  }
                  else{
                    $photo = base_url('uploads/').$userdata->admin_photo;  
                  }
                  ?>
                  <img src="<?php echo $photo ?>" class="img-circle profile_img">
                  <?php
                  }
                  else{
                  ?>  
                  <img src="<?php echo base_url().'uploads/'.admin_info($this->session->userdata('adminId'))->admin_photo ?>" alt="..." class="img-circle profile_img">
                  <?php
                  }
                ?>

              </div>







              <div class="profile_info">



                <span>Welcome,</span>



                <h2><?php echo ucwords($this->session->userdata('username'));?></h2>



              </div>



            </div>



            <!-- /menu profile quick info -->







            <br />







            <!-- sidebar menu -->



            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">



              <div class="menu_section">



                <h3>General</h3>



                <ul class="nav side-menu">



                  <!-- <li><a href="<?php echo base_url().'admin/dashboard';?>"><i class="fa fa-home"></i> Home</a></li> -->

                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url("admin/dashboard") ?>">Dashboard</a></li>
                      <li><a href="<?php echo base_url("admin/profile/{$this->session->userdata('adminId')}") ?>">Profile</a></li>
                      <?php if(!empty($this->session->userdata('isLogin'))):?>
                      <li><a href="<?php echo base_url("user/socialchannel") ?>">Channels</a></li>
                      <li><a href="<?php echo base_url("user/payment_billing") ?>">Payments and Billing</a></li>
                      <li><a href="<?php echo base_url("user/storeview") ?>">Store Opening Time</a></li>
                      <li><a href="<?php echo base_url("user/service_delivery") ?>">Service Area, Delivery and Returns</a></li>
                      <?php endif;?>
                    </ul>
                  </li>


                  <?php if(empty($this->session->userdata('isLogin'))):?>
                   <li><a><i class="fa fa-cog "></i> Settings <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="<?php echo base_url() ?>admin/chatoptions">Chatbot</a></li>
                      
                      <?php if(empty($this->session->userdata('isLogin'))):?>
                      <li><a href="<?php echo base_url() ?>admin/SendEmail">Email Template</a></li>
                      <?php endif;?>

                    </ul>



                  </li>

                <?php endif;?>

                 <?php if(empty($this->session->userdata('isLogin'))):?>
                  <li><a href="<?php echo base_url() ?>subscribers/listUsers"><i class="fa fa-users" aria-hidden="true"></i> Subscribers</a>
                  </li>
                  <li><a href="<?php echo base_url() ?>subscribers/users"><i class="fa fa-users" aria-hidden="true"></i>Users</a>
                  </li>
                <?php endif;?>

                <?php if(empty($this->session->userdata('isLogin'))):?>
                   <li><a href="<?php echo base_url() ?>customer/citizens"><i class="fa fa-users"></i> Citizen </a>
                  </li>
                <?php endif;?>

                  <li><a><i class="fa fa-tags "></i> Categories <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="<?php echo base_url() ?>categories/newCategory">New Category</a></li>



                      <li><a href="<?php echo base_url() ?>categories/getCatgories">Categories</a></li>







                    </ul>



                  </li>







                  <li><a><i class="fa fa-product-hunt"></i> Products <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="<?php echo base_url() ?>products/newProduct">New Product</a></li>



                      <li><a href="<?php echo base_url() ?>products/showProducts">Products</a></li>



                      <li><a href="<?php echo base_url() ?>products/bulkUpload">Bulk Create/Update Products</a></li>

                      <li><a href="<?php echo base_url() ?>products/bulkoperation">Bulk Image Upload</a></li>


                    </ul>



                  </li>


                  <?php if(empty($this->session->userdata('isLogin'))):?>
                  <li><a><i class="fa fa-university"></i> Stores <span class="fa fa-chevron-down"></span></a>

                    <ul class="nav child_menu">
                      <?php if(is_null($this->session->userdata('pagemanager')) || !empty($this->session->userdata('isLogin'))):?>
                      <li><a href="<?php echo base_url() ?>store/addStore">Add Store</a></li>
                      <?php endif;?>
                      <li><a href="<?php echo base_url() ?>store/listStores">Stores</a></li>

                    </ul>

                  </li>
                <?php endif;?>
                  <li><a><i class="fa fa-gift"></i> Coupon List <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>coupon/addCoupon">Add Coupon</a></li>
                      <li><a href="<?php echo base_url() ?>coupon/listCoupons">Coupons</a></li>
                      <li><a href="<?php echo base_url() ?>coupon/redeemCoupons">Redeem Coupons</a></li>
                    </ul>
                  </li>
                  <?php if($userinfo->admin_id == '19' || empty($this->session->userdata('isLogin'))):?>
                  <li><a><i class="fa fa-hashtag"></i> Metadata <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>coupon/addHashtag">Add Metadata</a></li>
                      <li><a href="<?php echo base_url() ?>coupon/listTags">Manage Metadata</a></li>
                    </ul>
                  </li>
                  <?php endif;?>
                  <!-- Added By mayur panchal 14-02-2018  -->
                  <?php if(is_null($this->session->userdata('pagemanager')) && empty($this->session->userdata('isLogin'))):?>
                  <li><a><i class="fa fa-money"></i> Packages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>package/addPackage">Add Package</a></li>
                      <li><a href="<?php echo base_url() ?>package/listPackages">Packages</a></li>
                    </ul>
                  </li>
                <?php elseif(!is_null($this->session->userdata('pagemanager')) && $this->session->userdata('pagemanager') == '652803498256943'):?>
                  <li><a><i class="fa fa-money"></i> Packages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>package/addPackage">Add Package</a></li>
                      <li><a href="<?php echo base_url() ?>package/listPackages">Packages</a></li>
                    </ul>
                  </li>
                  
                <?php endif;?>
                  <!-- end mayur panchal 14-02-2018  -->
                  <?php if($userinfo->admin_id == '19' || empty($this->session->userdata('isLogin'))):?>
                  <li><a><i class="fa fa-calendar" aria-hidden="true"></i> Social Media Post <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>userengagement/newSchedular">Create Social Media Post</a></li>
                      <li><a href="<?php echo base_url() ?>userengagement/getSchedularLists">Recent Social Media Post</a></li>
                    </ul>
                  </li>
                  <?php endif;?>

                  <li><a><i class="fa fa-shopping-cart"></i> Orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>order/listOrders"> Orders</a></li>
                      <li><a href="<?php echo base_url() ?>order/listAutoCancelledOrders">Auto Cancelled Orders</a></li>
                      <?php if(empty($this->session->userdata('isLogin'))):?>
                      <li><a href="<?php echo base_url() ?>order/charges">Commission & Fees</a></li>
                      <?php endif;?>
                    </ul>
                  </li>
                  <?php if(!empty($this->session->userdata('isLogin'))):?>
                  <li><a><i class="fa fa-money" aria-hidden="true"></i> Finance <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>order/finances"> Account Fees</a></li>
                    </ul>
                  </li>
                  <?php endif;?>
                  
                  <?php if(empty($this->session->userdata('isLogin'))):?>
                  <li><a><i class="fa fa-money" aria-hidden="true"></i> Billing <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url() ?>order/sellerbilling"> Seller Payments</a></li>
                    </ul>
                  </li>
                  <?php endif; ?>

                  <?php if(empty($this->session->userdata('isLogin'))):?>
                  <li><a href="<?php echo base_url() ?>logs/list"> <i class="fa fa-warning"></i> Error Logs</a></li>
                  <?php endif; ?>

                 <!--  <li><a href="<?php echo base_url() ?>order/listOrders"><i class="fa fa-shopping-cart"></i> Orders</a>

                  </li> -->

                  <!-- <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="form.html">General Form</a></li>



                      <li><a href="form_advanced.html">Advanced Components</a></li>



                      <li><a href="form_validation.html">Form Validation</a></li>



                      <li><a href="form_wizards.html">Form Wizard</a></li>



                      <li><a href="form_upload.html">Form Upload</a></li>



                      <li><a href="form_buttons.html">Form Buttons</a></li>



                    </ul>



                  </li> -->



                  <!-- <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="general_elements.html">General Elements</a></li>



                      <li><a href="media_gallery.html">Media Gallery</a></li>



                      <li><a href="typography.html">Typography</a></li>



                      <li><a href="icons.html">Icons</a></li>



                      <li><a href="glyphicons.html">Glyphicons</a></li>



                      <li><a href="widgets.html">Widgets</a></li>



                      <li><a href="invoice.html">Invoice</a></li>



                      <li><a href="inbox.html">Inbox</a></li>



                      <li><a href="calendar.html">Calendar</a></li>



                    </ul>



                  </li>



                  <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="tables.html">Tables</a></li>



                      <li><a href="tables_dynamic.html">Table Dynamic</a></li>



                    </ul>



                  </li>



                  <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="chartjs.html">Chart JS</a></li>



                      <li><a href="chartjs2.html">Chart JS2</a></li>



                      <li><a href="morisjs.html">Moris JS</a></li>



                      <li><a href="echarts.html">ECharts</a></li>



                      <li><a href="other_charts.html">Other Charts</a></li>



                    </ul>



                  </li>



                  <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>



                      <li><a href="fixed_footer.html">Fixed Footer</a></li>



                    </ul>



                  </li> -->



                </ul>



              </div>



              <!-- <div class="menu_section">



                <h3>Live On</h3>



                <ul class="nav side-menu">



                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="e_commerce.html">E-commerce</a></li>



                      <li><a href="projects.html">Projects</a></li>



                      <li><a href="project_detail.html">Project Detail</a></li>



                      <li><a href="contacts.html">Contacts</a></li>



                      <li><a href="profile.html">Profile</a></li>



                    </ul>



                  </li>



                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                      <li><a href="page_403.html">403 Error</a></li>



                      <li><a href="page_404.html">404 Error</a></li>



                      <li><a href="page_500.html">500 Error</a></li>



                      <li><a href="plain_page.html">Plain Page</a></li>



                      <li><a href="login.html">Login Page</a></li>



                      <li><a href="pricing_tables.html">Pricing Tables</a></li>



                    </ul>



                  </li>



                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>



                    <ul class="nav child_menu">



                        <li><a href="#level1_1">Level One</a>



                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>



                          <ul class="nav child_menu">



                            <li class="sub_menu"><a href="level2.html">Level Two</a>



                            </li>



                            <li><a href="#level2_1">Level Two</a>



                            </li>



                            <li><a href="#level2_2">Level Two</a>



                            </li>



                          </ul>



                        </li>



                        <li><a href="#level1_2">Level One</a>



                        </li>



                    </ul>



                  </li>                  



                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>



                </ul>



              </div> -->







            </div>



            <!-- /sidebar menu -->


          </div>



        </div>
        <!-- top navigation -->



        <div class="top_nav">



          <div class="nav_menu">



            <nav>



              <div class="nav toggle">



                <a id="menu_toggle"><i class="fa fa-bars"></i></a>



              </div>







              <ul class="nav navbar-nav navbar-right">



                <li class="profile-nav">



                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                    <?php
                      if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "social"){
                      ?>
                      <img src="<?php echo $userdata->login_picture?>"><?php echo ucwords($this->session->userdata('username'));?>
                      <?php
                      }
                      else if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "seller") {
                      if(is_null($userdata->admin_photo)){
                        $photo = base_url('assets/img/logo.png');
                      }
                      else{
                        $photo = base_url('uploads/').$userdata->admin_photo;  
                      }
                      ?>
                      <img src="<?php echo $photo ?>">
                      <?php
                      }
                      else{
                      ?>  
                     <img src="<?php echo base_url().'uploads/'.admin_info($this->session->userdata('adminId'))->admin_photo ?>" alt=""><?php echo ucwords($this->session->userdata('username'));?>
                      <?php
                      }
                    ?>
                    <span class=" fa fa-angle-down"></span>



                  </a>



                  <ul class="dropdown-menu dropdown-usermenu pull-right">



                    <li><a href="<?php echo base_url().'admin/profile/'.$this->session->userdata('adminId')?>"> Profile</a></li>


                     <?php if(empty($this->session->userdata('isLogin')) || $this->session->userdata('isLogin') == 'seller'):?>
                      <li>
                        <a href="<?php echo base_url().'admin/changePassword'?>">
                          <span>Change Password</span>
                        </a>
                      </li>
                    <?php endif;?>


                    <li><a href="<?php echo base_url().'admin/signout'?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>



                  </ul>



                </li>







                <!-- <li role="presentation" class="dropdown">



                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">



                    <i class="fa fa-envelope-o"></i>



                    <span class="badge bg-green">6</span>



                  </a>



                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">



                    <li>



                      <a>



                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>



                        <span>



                          <span>John Smith</span>



                          <span class="time">3 mins ago</span>



                        </span>



                        <span class="message">



                          Film festivals used to be do-or-die moments for movie makers. They were where...



                        </span>



                      </a>



                    </li>



                    <li>



                      <a>



                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>



                        <span>



                          <span>John Smith</span>



                          <span class="time">3 mins ago</span>



                        </span>



                        <span class="message">



                          Film festivals used to be do-or-die moments for movie makers. They were where...



                        </span>



                      </a>



                    </li>



                    <li>



                      <a>



                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>



                        <span>



                          <span>John Smith</span>



                          <span class="time">3 mins ago</span>



                        </span>



                        <span class="message">



                          Film festivals used to be do-or-die moments for movie makers. They were where...



                        </span>



                      </a>



                    </li>



                    <li>



                      <a>



                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>



                        <span>



                          <span>John Smith</span>



                          <span class="time">3 mins ago</span>



                        </span>



                        <span class="message">



                          Film festivals used to be do-or-die moments for movie makers. They were where...



                        </span>



                      </a>



                    </li>



                    <li>



                      <div class="text-center">



                        <a>



                          <strong>See All Alerts</strong>



                          <i class="fa fa-angle-right"></i>



                        </a>



                      </div>



                    </li>



                  </ul>



                </li> -->



              </ul>



            </nav>



          </div>



        </div>



        <!-- /top navigation -->




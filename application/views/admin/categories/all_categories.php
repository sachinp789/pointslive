<?php $info = admin_info($this->session->userdata('adminId'));?>
<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">

                    <h2><?php echo $title ?></h2>
                    <?php echo (!empty($info->catalog_shortlink)) ? "<p style=text-align:right>Seller Catalog Shortlink :- </p><p style=color:#B86447;font-weight:600;text-align:right><a href=https://".$info->catalog_shortlink." target='_blank'>".$info->catalog_shortlink."</a></p><p style=text-align:right><a href=".base_url()."uploads/qr_image/".$info->catalog_qrcode." class='btn btn-xs btn-primary' download>Download QR code</a></p>" : "";?></p>

                    <div class="clearfix"></div>

                  </div>

                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>

                    <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>

                    </button>

                      <?php echo $this->session->flashdata('flashmsg'); ?>

                    </div>

                  <?php endif; ?>

                  <div class="x_content">

                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">

                      <div class="row">

                        <div class="col-sm-12">

                          <table aria-describedby="datatable_info" role="grid" id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer categories">

                            <thead>

                              <tr role="row">

                                <th>ID</th>

                                <th>Image</th>
                                <th>Name</th>
                                <th>Seller Category Product Sortlink</th>
                                <th>Action</th>

                              </tr>

                            </thead>

                      <tbody>

                      <?php 

                      $count=1; 

                      if($categories):

                      foreach ($categories as $category) { ?>

                        <tr class="odd" role="row">

                          <td class="sorting_1"> <?php echo $count; ?> </td>

                          <td><?php
                              $img_link = base_url()."uploads/categories/".$category->category_image;
                              $newurl = base_url()."Image_lib.php?src=".$img_link."&w=60&h=60&q=50";
                              echo (!empty($category->category_image)) ? "<img src=".$newurl.">" : 'No Image'; ?></td>

                          <td><?php echo $category->category_name; ?></td>

                          <td style="color:#B86447;font-weight:600;"><?php echo !empty($category->global_link) ? "<a href=https://".$category->global_link." target='_blank'>".$category->global_link."</a>" : ""; ?></td>

                          <td> 

                          <a href="<?php echo base_url() ?>categories/editCategory/<?php echo $category->category_id;?>" class="btn btn-info btn-xs"> Edit</a> 

                          <a href="<?php echo base_url() ?>categories/removeCategory/<?php echo $category->category_id;?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this category as it will also delete all the related products  ?');" > Delete</a> 

                          </td>

                        </tr>

                        <?php $count++; } endif; ?>

                      </tbody>

                    </table>

                  </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>
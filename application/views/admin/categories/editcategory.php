<?php 
$this->load->model('Stores_model'); 
$stores = $this->Stores_model->getStores();  
//print_r($category_details);
?>
<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">

                    <h2><?php echo $title ?></h2>

                    <!-- <?php echo (!empty($category_details->global_link)) ? "<p style=text-align:right>Seller Category Product Shortlink :- </p><p style=color:#B86447;font-weight:600;text-align:right>".$category_details->global_link."</p>" : "";?></p>   -->

                    <div class="clearfix"></div>

                  </div>

                  <div class="x_content">

                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>

                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">

                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>

                      </button>

                        <?php echo $this->session->flashdata('flashmsg'); ?>

                      </div>

                    <?php endif; ?>

                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>categories/updateCategory/<?php echo $category_details->category_id; ?>"  method="post" enctype="multipart/form-data">

                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_name">Name : <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="category_name" class="form-control col-md-7 col-xs-12" name="category_name" type="text" value="<?php echo $category_details->category_name; ?>" required="">

                        </div>

                      </div>

                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_name_thai">Name Local :

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="category_name_thai" class="form-control col-md-7 col-xs-12" name="category_name_thai" type="text" value="<?php echo $category_details->category_name_thai; ?>">

                        </div>

                      </div>
                       <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                           <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>
                             <option value="">-Select Facebook Page-</option>
                             <?php
                             $selected = '';
                             $options='';
                             foreach ($stores as $key => $value) {
                              if($category_details->page_id == $value->page_id){
                                $selected="selected=selected";
                              }else{
                                $selected="";
                              }
                              $options.="<option value={$value->page_id}  $selected>";
                              $options.= $value->page_name;
                              $options.= "</option>";
                             }
                             echo $options;
                             ?>
                           </select>     
                        </div>
                      </div>
                      <?php else: ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo $page; ?>" type="hidden">
                      <?php endif;?>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_image">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="category_image" class="form-control col-md-7 col-xs-12" name="category_image"  type="file" <?php echo (!empty($category_details->category_image)) ? '' : 'required';?>>
                          <?php if(!empty($category_details->category_image)):?>
                          <img draggable="false" width="100" src="<?php echo base_url();?>uploads/categories/<?php echo $category_details->category_image; ?>">
                          <?php endif;?>
                          <input type="hidden" name="cat_image" value="<?php echo $category_details->category_image ; ?>">
                          <?php if(!empty($category_details->qrcode)):?>
                          <a href="<?php echo base_url()?>uploads/qr_image/<?php echo $category_details->qrcode?>" class="btn btn-xs btn-primary" download>Download QR code</a>
                          <?php endif;?>
                        </div>
                      </div>

                    <div class="ln_solid"></div>

                      <div class="form-group">

                        <div class="col-md-6 col-md-offset-3">

                          <button id="update_category" type="submit" class="btn btn-success" name="update_category">Update</button>

                          <a class="btn btn-default" href="<?php echo base_url().'categories/getCatgories'?>">Cancel</a>

                        </div>

                      </div>

                    </form>

                  </div>

                </div>

            </div>

        </div>

    </div>

</div>
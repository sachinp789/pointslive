<?php 
$this->load->model('Stores_model'); 
$stores = $this->Stores_model->getStores();  
?>
<div class="right_col" role="main" style="min-height: 949px;">



          <div class="">



            <div class="clearfix"></div>



            <div class="row">



              <div class="col-md-12 col-sm-12 col-xs-12">



                <div class="x_panel">



                  <div class="x_title">



                    <h2><?php echo $title ?></h2>


                    <a class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                    <div class="clearfix"></div>



                  </div>



                  <div class="x_content">



                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>



                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">



                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>



                      </button>



                        <?php echo $this->session->flashdata('flashmsg'); ?>



                      </div>



                    <?php endif; ?>



                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>categories/saveCategory"  method="post" enctype="multipart/form-data">



                      <div class="item form-group" id="cat_step1">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_name">Name : <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12" >



                          <input id="category_name" class="form-control col-md-7 col-xs-12" name="category_name" value="" type="text" required="">



                          <?php echo form_error('category_name','<span class="help-block">','</span>'); ?>



                        </div>



                      </div>



                      <div class="item form-group" id="cat_step2">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_name_thai">Name Local:

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="category_name_thai" class="form-control col-md-7 col-xs-12" name="category_name_thai" value="" type="text">

                          <?php echo form_error('category_name_thai','<span class="help-block">','</span>'); ?>

                        </div>

                      </div>
                      <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                           <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>
                             <option value="">-Select Facebook Page-</option>
                             <?php
                             $options='';
                             foreach ($stores as $key => $value) {
                              $options.="<option value={$value->page_id}>";
                              $options.= $value->page_name;
                              $options.= "</option>";
                             }
                             echo $options;
                             ?>
                           </select> 
                           <?php if(!$stores):?>
                           <span>Note : Please add store information </span><a href="<?php echo base_url()?>store/addStore" class="btn btn-xs btn-info">Add Store</a>
                          <?php endif;?>    
                        </div>
                      </div>
                      <?php else: ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo $page; ?>" type="hidden">
                      <?php endif;?> 

                      <div class="item form-group" id="cat_step3">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_image">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="category_image" class="form-control col-md-7 col-xs-12" name="category_image"  type="file" required>
                        </div>
                      </div>

                    <div class="ln_solid"></div>



                      <div class="form-group">



                        <div class="col-md-6 col-md-offset-3">



                          <button id="new_category" type="submit" class="btn btn-success" name="new_category">Save</button>



                          <a class="btn btn-default" href="<?php echo base_url().'admin/dashboard'?>">Cancel</a>



                        </div>



                      </div>



                    </form>



                  </div>



                </div>



            </div>



        </div>



    </div>



</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Create Category</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;width: 100%;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/CreateACategory.mov" >
      </video>
      </div>
      </div>
      </div>
</div>
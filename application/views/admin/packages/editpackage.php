<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>package/updatePackage" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="package_id" value="<?php echo $packagesdata->id; ?>">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="package_country">Package Country : <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="package_country" class="form-control col-md-7 col-xs-12" name="package_country" type="text" required value="<?php echo $packagesdata->package_country; ?>">
                          <?php echo form_error('package_country','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="package_country_thai">Package Country Thai : <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="package_country_thai" class="form-control col-md-7 col-xs-12" name="package_country_thai" type="text" required value="<?php echo $packagesdata->package_country_thai; ?>">
                          <?php echo form_error('package_country_thai','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                       <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Package Rates : <span class="required">*</span>
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="control-label">Small <span class="required">*</span></label>
                           <input id="package_small_weight" class="form-control col-md-2 col-xs-12" name="package_small_weight" type="number" required data-validate-minmax="1,9999" value="<?php echo $packagesdata->package_small_weight; ?>">
                          <?php echo form_error('package_small_weight','<span class="help-block">','</span>'); ?>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="control-label">Medium <span class="required">*</span></label>
                           <input id="package_medium_weight" class="form-control col-md-2 col-xs-12" name="package_medium_weight" vtype="number" required data-validate-minmax="1,9999" value="<?php echo $packagesdata->package_medium_weight; ?>">
                          <?php echo form_error('package_medium_weight','<span class="help-block">','</span>'); ?>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12">
                          <label class="control-label">Large <span class="required">*</span></label>
                          <input id="package_large_weight" class="form-control col-md-2 col-xs-12" name="package_large_weight" type="number" required data-validate-minmax="1,9999" value="<?php echo $packagesdata->package_large_weight; ?>">
                          <?php echo form_error('package_large_weight','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="package_image">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="package_image" class="form-control col-md-7 col-xs-12" name="package_image"  type="file" <?php echo (!empty($packagesdata->package_image)) ? '' : 'required';?>>
                          <?php if(!empty($packagesdata->package_image)):?>
                          <img draggable="false" width="100" src="<?php echo base_url();?>uploads/packages/<?php echo $packagesdata->package_image; ?>">
                          <?php endif;?>
                          <input type="hidden" name="pack_img" value="<?php echo $packagesdata->package_image ; ?>">
                        </div>
                      </div>
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>
                          <a class="btn btn-default" href="<?php echo base_url().'package/listPackages'?>">Cancel</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
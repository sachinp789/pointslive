<?php 
$this->load->model('Stores_model'); 
$stores = $this->Stores_model->getStores(); 
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>coupon/updateTag" method="post">
                      <input type="hidden" name="tag_id" value="<?php echo $tags_details->id ?>">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Meta Type  <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="radio">
                            <label>
                                  <div><input type="radio" class="flat" value="hashtag" name="metatype" <?php echo($tags_details->type == "hashtag") ? "checked=checked" : "" ?>>Hashtag</div>
                            </label>
                            <label>
                                  <div><input type="radio" class="flat" value="keyword" name="metatype" <?php echo($tags_details->type == "keyword") ? "checked=checked" : "" ?>>Keyword</div> 
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hashtag">MetaName  <span class="required">*</span>
                        </label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <input id="hashtag" class="form-control col-md-7 col-xs-12" name="hashtag"  value="<?php echo $tags_details->tagname ?>" type="text" required>
                          <?php echo form_error('hashtag','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>
                          </label>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                           <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>
                             <option value="">-Select Facebook Page-</option>
                             <?php
                             $selected = '';
                             $options='';
                             foreach ($stores as $key => $value) {
                              if($tags_details->page_id == $value->page_id){
                                $selected="selected=selected";
                              }else{
                                $selected="";
                              }
                              $options.="<option value={$value->page_id} $selected>";
                              $options.= $value->page_name;
                              $options.= "</option>";
                             }
                             echo $options;
                             ?>
                           </select> 
                        </div>
                      </div>
                      <?php else :?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo $page; ?>" type="hidden">
                      <?php endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>
                          <a class="btn btn-default" href="<?php echo base_url().'coupon/listTags'?>">Cancel</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">

                    <h2><?php echo $title ?></h2>

                    <div class="clearfix"></div>

                  </div>

                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>

                <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">

                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>

                    </button>

                      <?php echo $this->session->flashdata('flashmsg'); ?>

                </div>

                <?php endif; ?>

                  <div class="x_content">

                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">

                      <div class="row">

                        <div class="col-sm-12">

                          <table aria-describedby="datatable_info" role="grid" id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer stores">

                            <thead>

                              <th>S. No.</th>

                              <th>Page Name</th>

                              <th>Token</th>

                              <th>Action</th>

                            </thead>

                          <tbody> 

                            <?php 

                            $count=1; 

                            if($stores):

                            foreach ($stores as $key => $row) { ?>

                             <tr class="odd" role="row">

                              <td class="sorting_1"> <?php echo $count ?> </td>

                              <td><?php echo $row->page_name ?></td>

                              <td><?php echo substr($row->access_token, 0,70).'.....';  ?> </td>

                              <td> 

                              <a href="<?php echo base_url() ?>store/editStore/<?php echo $row->id?>" class="btn btn-info btn-xs"> Edit</a> 

                              <a href="<?php echo base_url() ?>store/removeStore/<?php echo $row->id?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this store?');" > Delete</a> 

                              </td>

                            </tr>

                            <?php $count++;} endif; ?>

                          </tbody>

                    </table>

                  </div>

                </div>

            </div>

        </div>

    </div>

</div>

</div>

</div>

</div>
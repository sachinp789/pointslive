<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>store/saveStore" method="post" enctype="multipart/form-data">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="page_name">Page Name  <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="page_name" class="form-control col-md-7 col-xs-12" name="page_name"  value="<?php echo set_value('page_name'); ?>" type="text" required>
                          <?php echo form_error('page_name','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="page_id">Page ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="page_id" class="form-control col-md-7 col-xs-12" name="page_id"  value="<?php echo set_value('page_id'); ?>" type="number" required>
                          <?php echo form_error('page_id','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="access_token">Access Token <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="access_token" name="access_token" class=" form-control col-md-7 col-xs-12" value="<?php echo set_value('access_token'); ?>" required>
                          <?php echo form_error('access_token','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>
                          <a class="btn btn-default" href="<?php echo base_url().'admin/dashboard'?>">Cancel</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
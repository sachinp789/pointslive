<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php //$msg = $this->uri->segment_array(); 
                  ?>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'admin/SendEmail'?>" method="post">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="users">Select Users<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="vendoruser[]" id="users" class="form-control" multiple required>
                            <option value="">-Select Users-</option>
                              <?php
                              $vendorlist = "";
                              foreach ($vendors as $value) {
                                  $vendorlist.="<option value=$value->admin_id>";
                                  $vendorlist.= $value->admin_name;
                                  $vendorlist.="</option>";
                              }
                              echo $vendorlist;
                              ?>
                          </select>
                          <?php echo form_error('vendoruser','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment_link">Payment Link
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="payment_link" id="payment_link" class="form-control" required placeholder="Payment Link">
                          <?php echo form_error('payment_link','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="submit" name="passwordpost" id="passwordpost" value="Save" class="btn btn-success">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
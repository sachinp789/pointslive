<?php 
$this->load->model('Stores_model'); 
$stores = $this->Stores_model->getStores();  
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left col-lg-offset-2" novalidate action="<?php echo base_url() ?>coupon/updateCoupon" method="post">
                      <input type="hidden" name="coupon_id" value="<?php echo $coupons_details->id; ?>">
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Code  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12" name="title"  value="<?php echo $coupons_details->code; ?>" type="text" required>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Description  <span class="required"></span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <textarea id="description" name="description" class="form-control col-md-7 col-xs-12"><?php echo $coupons_details->description; ?></textarea>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="status">Status  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="status" id="status" required>
                            <option value="0" <?php echo ($coupons_details->status == 0) ? "selected=selected" : ''?> >Inactive</option>
                            <option value="1" <?php echo ($coupons_details->status == 1) ? "selected=selected" : ''?>>Active</option>
                         </select>
                        </div>
                      </div>

                      <div class="item form-group">
                         <label class="control-label col-md-2 col-sm-3 col-xs-12" for="fromdate">From date  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class='input-group date' id='datetimepicker6'>
                            <input type='text' class="form-control col-md-7 col-xs-12" name="fromdate" id="fromdate" value="<?php echo $coupons_details->from_date; ?>" required/>
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div>
                      </div>

                      <div class="item form-group">
                         <label class="control-label col-md-2 col-sm-3 col-xs-12" for="todate">To date  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class='input-group date' id='datetimepicker7'>
                            <input type='text' class="form-control col-md-7 col-xs-12" name="todate" id="todate" value="<?php echo $coupons_details->to_date; ?>" required/>
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="apply_on">Apply by  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="apply_on" id="apply_on" required>
                            <option value="fixed" <?php echo ($coupons_details->by_apply == 'fixed') ? "selected=selected" : ''?>>Fixed</option>
                            <option value="%" <?php echo ($coupons_details->by_apply == '%') ? "selected=selected" : ''?>>%</option>
                         </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="amount">Discount value  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="amount" class="form-control col-md-7 col-xs-12" name="amount"  value="<?php echo $coupons_details->discount_value; ?>" type="number" required onkeypress="return isNumberKey(event)">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="uses_per_customer">Uses per customer  <span class="required"></span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="uses_per_customer" class="form-control col-md-7 col-xs-12" name="uses_per_customer" value="<?php echo $coupons_details->per_customer_uses; ?>" type="text" onkeypress="return isNumberKey(event)">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="minimum_amount">Minimum order amount   <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="minimum_amount" class="form-control col-md-7 col-xs-12" name="minimum_amount" value="<?php echo $coupons_details->minimum_amount; ?>" type="text" onkeypress="return isNumberKey(event)" required>
                        </div>
                      </div>
                      <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                        <div class="item form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>
                          </label>
                          <div class="col-md-5 col-sm-6 col-xs-12">
                           <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>
                             <option value="">-Select Facebook Page-</option>
                             <?php
                             $selected = '';
                             $options='';
                             foreach ($stores as $key => $value) {
                              if($coupons_details->page_id == $value->page_id){
                                $selected="selected=selected";
                              }else{
                                $selected="";
                              }
                              $options.="<option value={$value->page_id} $selected>";
                              $options.= $value->page_name;
                              $options.= "</option>";
                             }
                             echo $options;
                             ?>
                           </select> 
                        </div>
                      </div>
                      <?php else: ?>
                        <input type="hidden" name="facebook_page" id="facebook_page" readonly class=" form-control col-md-7 col-xs-12" value="<?php echo $page; ?>"> 
                      <?php endif; ?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>
                          <a class="btn btn-default" href="<?php echo base_url().'coupon/listCoupons'?>">Cancel</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
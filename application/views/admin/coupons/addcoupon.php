<?php 
$this->load->model('Stores_model'); 
$stores = $this->Stores_model->getStores();  
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <a class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left col-lg-offset-2" novalidate action="<?php echo base_url() ?>coupon/saveCoupon" method="post">
                      <div class="item form-group" id="csteps1">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Code  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="title" class="form-control col-md-7 col-xs-12" name="title"  value="<?php echo set_value('title'); ?>" type="text" required>
                          <?php echo form_error('title','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <div class="item form-group" id="csteps2">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="title">Description  <span class="required"></span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <textarea id="description" name="description" class="form-control col-md-7 col-xs-12"><?php echo set_value('description'); ?></textarea>
                          <?php echo form_error('description','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>

                      <div class="item form-group" id="csteps3">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="status">Status  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="status" id="status" required>
                            <option value="0">Inactive</option>
                            <option value="1">Active</option>
                         </select>
                        </div>
                      </div>

                      <div class="item form-group" id="csteps4">
                         <label class="control-label col-md-2 col-sm-3 col-xs-12" for="fromdate">From date  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class='input-group date' id='datetimepicker6'>
                            <input type='text' class="form-control col-md-7 col-xs-12" name="fromdate" id="fromdate" required/>
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div>
                      </div>

                      <div class="item form-group" id="csteps5">
                         <label class="control-label col-md-2 col-sm-3 col-xs-12" for="todate">To date  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                        <div class='input-group date' id='datetimepicker7'>
                            <input type='text' class="form-control col-md-7 col-xs-12" name="todate" id="todate" required/>
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div>
                      </div>

                      <div class="item form-group" id="csteps6">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="apply_on">Apply by  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="apply_on" id="apply_on" required>
                            <option value="fixed">Fixed</option>
                            <option value="%">%</option>
                         </select>
                        </div>
                      </div>

                      
                      <div class="item form-group" id="csteps7">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="amount">Discount value  <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="value" class="form-control col-md-7 col-xs-12" name="amount"  value="<?php echo set_value('amount'); ?>" type="number" onkeypress="return isNumberKey(event)" required > <!-- data-validate-minmax="1,100" -->
                          <?php echo form_error('amount','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>


                      <div class="item form-group" id="csteps8">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="uses_per_customer">Uses per customer  <span class="required"></span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="uses_per_customer" class="form-control col-md-7 col-xs-12" name="uses_per_customer" value="<?php echo set_value('uses_per_customer'); ?>" type="text" onkeypress="return isNumberKey(event)">
                          <?php echo form_error('uses_per_customer','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>

                      <div class="item form-group" id="csteps9">
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="minimum_amount">Minimum order amount   <span class="required">*</span>
                        </label>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <input id="minimum_amount" class="form-control col-md-7 col-xs-12" name="minimum_amount" value="<?php echo set_value('minimum_amount'); ?>" type="text" onkeypress="return isNumberKey(event)" required>
                          <?php echo form_error('minimum_amount','<span class="help-block">','</span>'); ?>
                        </div>
                      </div>
                      <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                        <div class="item form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>
                          </label>
                          <div class="col-md-5 col-sm-6 col-xs-12">
                           <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>
                             <option value="">-Select Facebook Page-</option>
                             <?php
                             $options='';
                             foreach ($stores as $key => $value) {
                              $options.="<option value={$value->page_id}>";
                              $options.= $value->page_name;
                              $options.= "</option>";
                             }
                             echo $options;
                             ?>
                           </select> 
                           <?php if(!$stores):?>
                           <span>Note : Please add store information </span><a href="<?php echo base_url()?>store/addStore" class="btn btn-xs btn-info">Add Store</a>
                          <?php endif;?>
                        </div>
                      </div>
                      <?php else :?>
                        <input type="hidden" name="facebook_page" id="facebook_page" readonly class=" form-control col-md-7 col-xs-12" value="<?php echo $page; ?>">
                      <?php endif; ?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-2" id="csteps10">
                          <button id="sendcoupon" type="submit" class="btn btn-success" name="send">Save</button>
                          <a class="btn btn-default" href="<?php echo base_url().'admin/dashboard'?>">Cancel</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Create Coupon</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/CreateaCoupon.mov" >
      </video>
      </div>
      </div>
      </div>
</div>
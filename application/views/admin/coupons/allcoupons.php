<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                    <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                    </div>
                  <?php endif; ?>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table aria-describedby="datatable_info" role="grid" id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer coupon">
                            <thead>
                              <tr role="row">
                                <th>ID</th>
                                <th>Coupon Code</th>
                                <th>Discount Amount</th>
                                <th>By Apply</th>
                                <th>Status</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                      <tbody>
                      <?php 
                      $count=1; 
                      if($coupons):
                      foreach ($coupons as $coupon) { ?>
                        <tr class="odd" role="row">
                          <td class="sorting_1"> <?php echo $count; ?> </td>
                          <td><?php echo $coupon->code; ?></td>
                          <td><?php echo $coupon->discount_value; ?></td>
                          <td><?php echo $coupon->by_apply; ?></td>
                          <td>
                          <?php
                          if($coupon->status == 1){
                            echo '<span class="label label-success">Active</span>';
                          }
                          else{
                            echo '<span class="label label-danger">Inactive</span>';
                          }
                          ?>
                          </td>
                          <td> 
                          <a href="<?php echo base_url() ?>coupon/editCoupon/<?php echo $coupon->id;?>" class="btn btn-info btn-xs"> Edit</a> 
                          <a href="<?php echo base_url() ?>coupon/removeCoupon/<?php echo $coupon->id;?>" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this coupon ?');" > Delete</a> 
                          </td>
                        </tr>
                        <?php $count++; } endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                     <a href="<?php echo base_url('subscribers/exportUsers')?>" class="btn btn-md btn-success pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                    <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                    </div>
                  <?php endif; ?>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table aria-describedby="datatable_info" role="grid" id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer">
                            <thead>
                              <tr role="row">
                                <th>ID</th>
                                <th>Image</th>
                                <th>User Name</th>
                                <th>Login By</th>
                                <th>Status</th>
                                <th>#</th>
                              </tr>
                            </thead>
                      <tbody>
                      <?php 
                      $count=1; 
                      if($users):
                      foreach ($users as $subuser) { ?>
                        <tr class="odd" role="row">
                          <td class="sorting_1"><?php echo $count; ?> </td>                          
                          <td><img src="<?php echo $subuser->login_picture; ?>" width="60"> </td>
                          <td><?php echo $subuser->admin_name; ?></td>
                          <td><?php echo ($subuser->login_with == 'seller') ? "Email" : ucfirst($subuser->login_with); ?></td>
                          <td class="status_<?php echo $subuser->admin_id; ?>">

                            <?php
                          if($subuser->is_active == 1){
                            echo '<span class="label label-success">Active</span>';
                          }else {
                           echo '<span class="label label-danger">Blocked</span>';
                          }
                          ?></td>
                          <td>
                            <div class="">
                            <label>
                              <input type="checkbox" class="js-switch check_status" data-switchery="false" style="display: none;" value="<?php echo $subuser->admin_id; ?>" <?php echo ($subuser->is_active == '1') ? "checked" : "";?>>
                              <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
                            </label>
                          </div>
                          </td>
                        </tr>
                        <?php $count++; } endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

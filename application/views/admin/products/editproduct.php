<?php 

$this->load->model('Stores_model'); 
$this->load->model('Categories_model'); 
$stores = $this->Stores_model->getStores(); 
$userinfo = admin_info($this->session->userdata('adminId')); 

?>

<div class="right_col" role="main" style="min-height: 949px;">



          <div class="">



            <div class="clearfix"></div>



            <div class="row">



              <div class="col-md-12 col-sm-12 col-xs-12">



                <div class="x_panel">



                  <a href="<?php echo base_url()?>products/showProducts" class="btn btn-sm btn-default">Back</a>
                  <div class="x_title">


                    <h2><?php echo $title ?></h2>


                    <!-- <?php echo (!empty($product_details->global_link)) ? "<p style=text-align:right>Seller Product Shortlink :- </p><p style=color:#B86447;font-weight:600;text-align:right>".$product_details->global_link."</p>" : "";?></p> -->
                    



                    <div class="clearfix"></div>



                  </div>



                  <div class="x_content">







                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>



                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">



                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>



                      </button>



                        <?php echo $this->session->flashdata('flashmsg'); ?>



                      </div>



                    <?php endif; ?>



                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>products/updateProduct/<?php echo $product_details->product_id; ?>" method="post" enctype="multipart/form-data">

                      <input type="hidden" name="url" id="url" value="<?php echo base_url();?>">
                      <input type="hidden" name="pid" id="product_id" value="<?php echo $product_details->product_id; ?>">

                      <!-- <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_sku">Product_ID (SKU)  <span class="required">*</span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="product_sku" class="form-control col-md-7 col-xs-12" name="product_sku"  value="<?php echo $product_details->product_sku;?>" type="text" required>
                          <span class="skucheck"></span>
                        </div>

                      </div> -->



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Name  <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input id="product_name" class="form-control col-md-7 col-xs-12" name="product_name"  value="<?php echo $product_details->product_name;?>" type="text" required>



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name_thai">Name Local <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="product_name_thai" class="form-control col-md-7 col-xs-12" name="product_name_thai"  value="<?php echo $product_details->product_name_thai;?>" type="text">

                        </div>

                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_image">Image <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="hidden" name="count_img" value="<?php echo $product_details->count_img; ?>" class="form-control col-md-7 col-xs-12">

                          <?php if($product_details->count_img<3){ ?>

                          <input id="product_image" class="form-control col-md-7 col-xs-12" name="product_image[]" type="file"  multiple="true">You can upload maximum 3 images & File size :- 5MB</label>

                          <?php } ?>

                          <div class="product_gallery editproduct">



                          <?php if($product_details->images) :



                          foreach ($product_details->images as $img_value) { 
                            if(!empty($img_value->product_image)):
                            ?>


                            <a>



                            <span id="<?php echo $img_value->image_id ; ?>">



                                <button type="button" class="btn btn-danger btn-xs" 



                            onclick="removeImages('<?php echo base_url('products/removeImage');?>','<?php echo $img_value->image_id ; ?>')"  >x</button>



                              <img width="100" src="<?php echo base_url();?>/uploads/products/<?php echo $img_value->product_image; ?>">



                              <input type="hidden" name="image_id" value="<?php echo $img_value->image_id ; ?>">



                            </span>



                            </a>



                          <?php endif; } endif; ?>



                          </div>
                          <?php
                          if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "social" && $userinfo->login_with == "instagram"):
                          ?>
                          <div class="images-insta">
                            <span>OR</span><br>
                            <input type="button" name="btninstagram" id="btninstagram" value="Upload By Instagram" class="bt btn-xs btn-info" data-user ="<?php echo $userinfo->login_uid;?>" onclick="getInstagramImages()">
                            <input type="hidden" id="usertoken" value="<?php echo $userinfo->login_access_token?>">
                            <div class="ln_solid"></div>
                            <div class="instaimg"></div>
                          </div>
                        <?php endif;?>

                        </div>



                      </div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shippingtype">Shipping Type <span class="required">*</span>
                          </label>
                          <div class="col-md-5 col-sm-3 col-xs-3">
                            <div class="radio">
                              <label>
                                <div><input type="radio" class="generatelink" value="delivery" name="shippingtype" id="shippingtype" <?php echo ($product_details->shipping_type == "delivery") ? "checked" : "" ?>></div> Delivery
                              </label>
                              <label>
                                <div><input type="radio" class="generatelink" value="offline" name="shippingtype" id="shippingtype" <?php echo ($product_details->shipping_type == "offline") ? "checked" : "" ?>></div> Offline
                              </label>
                              <label>
                                <div><input type="radio" class="generatelink" value="both" name="shippingtype" id="shippingtype" <?php echo ($product_details->shipping_type == "both") ? "checked" : "" ?>></div> Offine & Delivery
                              </label>
                          </div>
                          </div>
                      </div> 
                      
                      <div class="item form-group"  id="offlinetype">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="offlinelink">Offline Short Link <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="offlinelink" name="offlinelink" class=" form-control col-md-7 col-xs-12" value="<?php echo ($product_details->shortlink != "") ? $product_details->shortlink : '' ?>" <?php echo ($product_details->shortlink != "") ? "readonly": '' ?>>
                          <input type="hidden" name="finallink" id="finallink" value="">
                          <div class="generate"></div>
                          <?php if ($product_details->qrcode != ""):?>
                          <img width="100" src="<?php echo base_url();?>uploads/qr_image/<?php echo $product_details->qrcode; ?>">
                          <a href="<?php echo base_url();?>uploads/qr_image/<?php echo $product_details->qrcode; ?>" download="<?php echo $product_details->qrcode; ?>" class="bt btn-info btn-xs">Download</a>
                          <?php endif;?>
                          <!-- <?php if (!empty($product_details->global_qrcode)):?>
                          <a href="<?php echo base_url();?>uploads/qr_image/<?php echo $product_details->global_qrcode; ?>" download="<?php echo $product_details->global_qrcode; ?>" class="bt btn-info btn-xs">Download QR code</a>
                          <?php endif;?> -->
                        </div>
                        <?php if ($product_details->shortlink == ""):?>
                        <input type="button" name="generatelink" id="generatelink" class="btn btn-sm btn-primary" value="Generate" onclick="createshortlink()">
                        <?php endif;?>
                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_price">Price <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="number" id="product_price" name="product_price" class=" form-control col-md-7 col-xs-12" value="<?php echo $product_details->product_price;?>" required>



                        </div>



                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sale_price">Sale Price <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="number" id="sale_price" name="sale_price" value="<?php echo $product_details->sale_price;?>" class="form-control col-md-7 col-xs-12">



                        </div>



                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                            <textarea id="description" required="required" name="description"  class="form-control col-md-7 col-xs-12"><?php echo $product_details->product_description;?></textarea>



                        </div>



                      </div>



                       <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_description_thai">Description Local <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <textarea id="product_description_thai" name="product_description_thai"  class="form-control col-md-7 col-xs-12"><?php echo $product_details->product_description_thai;?></textarea>

                        </div>

                      </div> 



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_qty">Total Qty <span class="required">*</span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="number" id="total_qty" name="total_qty" class=" form-control col-md-7 col-xs-12" value="<?php echo $product_details->total_qty;?>" required>

                        </div>

                      </div> 



                      <!-- <div class="item form-group">



                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inventory">Inventory <span class="required">*</span>



                          </label>



                          <div class="col-md-3 col-sm-3 col-xs-3">



                            <div class="radio">



                              <label>



                                <div><input type="radio" class="flat"  value="yes" name="inventory" id="inventory" <?php echo $product_details->inventory == 'yes' ? "checked":"" ?> ></div> In Stock



                              </label>



                              <label>



                                <div><input type="radio"  class="flat" value="no" name="inventory" id="inventory" <?php echo $product_details->inventory == 'no' ? "checked":"" ?>></div> Out of Stock



                            </label>



                          </div>



                          </div>



                      </div> --> 



                      <div class="item form-group">



                      <?php 



                            $array = [];



                            foreach ($product_details->categories as $rows) {



                                $array[] = $rows->category_id;



                            }



                      ?>



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_id">Category <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                         <?php 
                          $categories = $this->Categories_model->getCategoryByUser(); 
                          ?>



                         <select class="select2_multiple form-control" name="category_id" id="category_id"  required >

                          <option value="">-Select Category-</option>

                           <?php



                           $list = '';



                           foreach ($categories as $key => $value) {



                            $category_id  = $value->category_id;



                              if(in_array($category_id, $array)){



                                $selected = "selected='selected'";



                              }



                              else{



                                $selected = "";



                              }



                              $list.="<option value=$value->category_id  $selected>";



                              $list.= $value->category_name;



                              $list.="</option>";



                           }



                           echo $list;



                           ?>



                         </select>     



                        </div>



                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shipping_cost">Shipping Cost <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                            <input type="number" name="shipping_cost" id="shipping_cost"  value="<?php echo $product_details->shipping_cost;?>" class=" form-control col-md-7 col-xs-12">



                        </div>



                      </div>

                      <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">
                      <?php elseif(is_null($page)):?>
                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                         <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>



                          <option value="">-Select Facebook Page-</option>

                           <?php

                           $option='';

                           foreach ($stores as $key => $value) {

                             if($product_details->facebook_page == $value->page_id){

                              $selected = "selected=selected";

                             }else{

                              $selected="";

                             }

                             $option.="<option value={$value->page_id} {$selected}>";

                             $option.=$value->page_name;

                             $option.="</option>";

                           }

                           echo $option;

                           ?>



                           <!-- <option value="">-Select Facebook Page-</option>



                           <option value="1048681488539732" <?php echo $product_details->facebook_page == '1048681488539732' ? "selected":"" ?> >Points Live</option> -->



                         </select>     



                        </div>



                      </div>

                      <?php else: ?>
                        <input type="hidden" name="facebook_page" id="facebook_page" readonly class=" form-control col-md-7 col-xs-12" value="<?php echo $page; ?>"> 
                      <?php endif; ?>

                      <div class="ln_solid"></div>



                      <div class="form-group">



                        <div class="col-md-6 col-md-offset-3">



                          <input type="submit" id="send" name="update" class="btn btn-success" value="Update">



                          <a class="btn btn-default" href="<?php echo base_url().'products/showProducts'?>">Cancel</a>



                        </div>



                      </div>



                    </form>



                  </div>



                </div>



            </div>



        </div>



    </div>



</div>
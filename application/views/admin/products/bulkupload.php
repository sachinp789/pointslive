<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">

                    <h2><?php echo $title;?></h2>

                    <div class="clearfix"></div>

                  </div>

                  <div class="x_content">

                    <?php if (isset($error)): ?>
                        <div class="alert alert-error">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                          <?php foreach(array_unique($error) as $err){
                              echo "<p>".($err)."</p>";
                          }
                          ?>
                      </div>
                    <?php endif; ?>

                    <?php if(!empty($this->session->flashdata('flashmsg'))): ?>

                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">

                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>

                      </button>

                        <?php echo $this->session->flashdata('flashmsg'); ?>

                      </div>

                    <?php endif; ?>

                      <a download class="btn btn-success" style="float: right" href="<?php echo base_url().'uploads/bulkupload.csv'?>">Upload Template</a>

                      </div>

                   

                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>products/import" method="post" enctype="multipart/form-data">

                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="importData">Upload Csv  <span class="required">*</span>

                        </label>

                       <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="importData" class="form-control col-md-7 col-xs-12" name="importData"  value="" type="file" required>

                        </div>

                      </div>

                      <!--  <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="uploadZip">Upload Image Zip <span class="required">*</span>

                        </label>

                       <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="uploadZip" class="form-control col-md-7 col-xs-12" name="uploadZip"  value="" type="file"  required>

                        </div>

                      </div> -->

                       <div class="ln_solid"></div>

                      <div class="form-group">

                        <div class="col-md-6 col-md-offset-3">

                          <button id="send" type="submit" class="btn btn-success" name="send">Submit</button>

                          <a class="btn btn-default" href="<?php echo base_url().'admin/dashboard'?>">Cancel</a>

                        </div>

                      </div>

                    </form>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>


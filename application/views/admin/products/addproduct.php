<?php 

$this->load->model('Stores_model'); 
$this->load->model('Categories_model'); 

$stores = $this->Stores_model->getStores(); 
$userinfo = admin_info($this->session->userdata('adminId')); 

?>

<div class="right_col" role="main" style="min-height: 949px;">



          <div class="">



            <div class="clearfix"></div>



            <div class="row">



              <div class="col-md-12 col-sm-12 col-xs-12">



                <div class="x_panel">



                  <div class="x_title">



                    <h2><?php echo $title ?></h2>

                    <a class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-video-camera" aria-hidden="true"></i></a>

                    <div class="clearfix"></div>



                  </div>



                  <div class="x_content">



                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>



                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">



                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>



                      </button>



                        <?php echo $this->session->flashdata('flashmsg'); ?>



                      </div>



                    <?php endif; ?>



                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url() ?>products/saveProduct" method="post" enctype="multipart/form-data">

                      <input type="hidden" name="url" id="url" value="<?php echo base_url();?>">
                      <input type="hidden" name="pid" id="product_id" value="">

                     <!--  <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_sku">Product_ID (SKU)  <span class="required">*</span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="product_sku" class="form-control col-md-7 col-xs-12" name="product_sku"  value="<?php echo set_value('product_sku'); ?>" type="text" required>
                          <span class="skucheck"></span>
                        </div>

                      </div> -->



                      <div class="item form-group" id="pstep-1">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Name  <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input id="product_name" class="form-control col-md-7 col-xs-12" name="product_name"  type="text" required>



                        </div>



                      </div>



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name_thai">Name Local  <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input id="product_name_thai" class="form-control col-md-7 col-xs-12" name="product_name_thai"  value="<?php echo set_value('product_name_thai'); ?>" type="text">

                        </div>

                      </div>

                      <div class="item form-group" id="pstep-3">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                            <textarea id="description" required="required" name="description" class="form-control col-md-7 col-xs-12"></textarea>



                        </div>



                      </div> 



                      <div class="item form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_description_thai">Description Local <span class="required"></span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <textarea id="product_description_thai" name="product_description_thai" class="form-control col-md-7 col-xs-12"></textarea>

                        </div>

                      </div>



                      <div class="item form-group" id="pstep-5">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category_id">Category <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                        <?php 
                          $categories = $this->Categories_model->getCategoryByUser(); 
                        ?>



                         <select class="select2_multiple form-control" name="category_id" id="category_id" required>

                          <option value="">-Select Category-</option> 

                           <?php



                           $list = '';



                           foreach ($categories as $key => $value) {



                              $list.="<option value=$value->category_id>";



                              $list.= $value->category_name;



                              $list.="</option>";



                           }



                           echo $list;



                           ?>



                         </select>     



                        </div>



                      </div>

                      <div class="item form-group" id="pstep-7">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shippingtype">Shipping Type <span class="required">*</span>
                          </label>
                          <div class="col-md-5 col-sm-3 col-xs-3">
                            <div class="radio">
                              <label>
                                <div><input type="radio" class="generatelink" checked value="delivery" name="shippingtype" id="shippingtype"></div> Delivery
                              </label>
                              <label>
                                <div><input type="radio" class="generatelink" value="offline" name="shippingtype" id="shippingtype"></div> Offline
                              </label>
                              <label>
                                <div><input type="radio" class="generatelink" value="both" name="shippingtype" id="shippingtype"></div> Offine & Delivery
                              </label>
                          </div>
                          </div>
                      </div>

                       <div class="item form-group"  id="offlinetype">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="offlinelink">Offline Short Link <span class="required"></span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="offlinelink" name="offlinelink" class=" form-control col-md-7 col-xs-12">
                            <input type="hidden" name="finallink" id="finallink" value="">
                            <div class="generate"></div>
                          </div>
                          <input type="button" name="generatelink" id="generatelink" class="btn btn-sm btn-primary" value="Generate" onclick="createshortlink()">
                      </div>

                      <div class="pstep-8">

                      <div class="item form-group" id="pstep-2">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_price">Price <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="number" id="product_price" name="product_price" class=" form-control col-md-7 col-xs-12" required>



                        </div>



                      </div>



                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sale_price">Sale Price <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input type="number" id="sale_price" name="sale_price" class="form-control col-md-7 col-xs-12">



                        </div>



                      </div>
                      </div>

                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shipping_cost">Shipping Cost <span class="required"></span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                            <input type="number" name="shipping_cost" id="shipping_cost" class=" form-control col-md-7 col-xs-12">



                        </div>



                      </div>


                      <div class="item form-group" id="pstep-4">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_qty">Total Qty <span class="required">*</span>

                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                          <input type="number" id="total_qty" name="total_qty" class=" form-control col-md-7 col-xs-12"  value="<?php echo set_value('total_qty'); ?>" required>

                        </div>

                      </div>

                      <div class="item form-group" id="pstep-6">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_image">Image <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                          <input id="product_image" class="form-control col-md-7 col-xs-12" name="product_image[]" type="file" multiple>
                          <label id="img_warning" class="" style="display: block;">You can upload maximum 3 images & File size :- 5MB</label>

                          <?php
                          if(!empty($this->session->userdata('isLogin')) && $this->session->userdata('isLogin') == "social" && $userinfo->login_with == "instagram"):
                          ?>

                          <div class="ln_solid"></div>
                          <span>OR</span><br>
                          <input type="button" name="btninstagram" id="btninstagram" value="Upload By Instagram" class="bt btn-xs btn-info" data-user ="<?php echo $userinfo->login_uid;?>" onclick="getInstagramImages()">
                          <input type="hidden" id="usertoken" value="<?php echo $userinfo->login_access_token?>">
                          <div class="ln_solid"></div>
                          <div class="instaimg"></div>
                          <?php endif;?>
                        </div>



                      </div>


                      <?php if(!empty($this->session->userdata('isLogin'))) :
                      $pages = $this->Stores_model->where("created_by",$this->session->userdata('adminId'))->get();
                      ?>
                      <input id="facebook_page" class="form-control col-md-7 col-xs-12" name="facebook_page" value="<?php echo ($pages) ? $pages->page_id : ''; ?>" type="hidden">

                      <?php elseif(is_null($page)):?>
                      <div class="item form-group">



                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_page">Facebook Page <span class="required">*</span>



                        </label>



                        <div class="col-md-6 col-sm-6 col-xs-12">



                         <select class="select2_single form-control" name="facebook_page" id="facebook_page" required>



                           <option value="">-Select Facebook Page-</option>

                           <?php

                           $options='';

                           foreach ($stores as $key => $value) {

                            $options.="<option value={$value->page_id}>";

                            $options.= $value->page_name;

                            $options.= "</option>";

                           }

                           echo $options;

                           ?>



                          <!--  <option value="1048681488539732">Points Live</option> -->



                         </select>     



                        </div>



                      </div>

                      <?php else :?>
                        <input type="hidden" name="facebook_page" id="facebook_page" readonly class=" form-control col-md-7 col-xs-12" value="<?php echo $page; ?>">
                      <?php endif; ?>

                      <div class="ln_solid"></div>



                      <div class="form-group">



                        <div class="col-md-6 col-md-offset-3">



                          <button id="send" type="submit" class="btn btn-success" name="send">Save</button>



                          <a class="btn btn-default" href="<?php echo base_url().'admin/dashboard'?>">Cancel</a>



                        </div>



                      </div>



                    </form>



                  </div>



                </div>



            </div>



        </div>



    </div>



</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="myModalLabel">Create Product</h4>
      </div>
      <div class="modal-body">
      <h4></h4>
      <video controls loop style="max-width: 100%; height: 500px;width: 100%;" >         
        <source type="video/mp4" src="<?php echo base_url(); ?>uploads/videos/CreateaProduct.mov" >
      </video>
      </div>
      </div>
      </div>
</div>
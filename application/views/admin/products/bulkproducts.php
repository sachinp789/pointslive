<?php 
$this->load->model('Products_model');
$users = $this->db->select('admin_id,admin_name,admin_email')->from('admin')->where_in('login_with',array('facebook','instagram','twitter','seller'))->get()->result();
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                     <!-- <a href="<?php echo base_url('products/exportProducts')?>" class="btn btn-md btn-success pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a> -->
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                </div>
                <?php endif; ?>
                <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <h3 class="panel-title" >Filter</h3>
                </div> -->
                <div class="panel-body">
                <form id="form-filter" class="form-horizontal mobile-lists">
                    <div class="form-group">
                        <label for="category" class="col-sm-2 col-xs-6 control-label">Category</label>
                        <div class="col-sm-2 col-xs-6">
                            <?php
                            $catlists = $this->Products_model->GetCategories();
                            ?>
                            <select class="form-control" id="category">
                                <option value="">All Category</option>
                                <?php
                                $list = "";
                                foreach ($catlists as $cat) {
                                  $list.= "<option value=$cat->category_id>".$cat->category_name."</option>";
                                }
                                echo $list;
                                ?>
                            </select>
                        </div>
                        <label for="stock" class="col-sm-2 col-xs-6 control-label">Stock</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control" id="stock">
                                <option value="">All</option>
                                <option value="1">Availabel</option>
                                <option value="0">Out of stock</option>
                            </select>
                        </div>
                        <label for="stock" class="col-sm-2 col-xs-6 control-label">Has Image</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control" id="hasimage">
                                <option value="">All</option>
                                <option value="1">Yes</option>
                                <option value="2">No</option>
                            </select>
                        </div>
                    </div>
                </form>
                </div>
                </div>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                        
                          <table id="table-order-4" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                              <th>S. No.</th>
                              <th>Name</th>
                              <th>Seller Product Shortlink</th>
                              <th>Image 1</th>
                              <th>Image 2</th>
                              <th>Image 3</th>
                            </thead>
                            <tbody> 
                            
                            </tbody>
                          </table>
               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>

<script type="text/javascript">

$(document).ready(function() {
    //datatables
    var table = $('#table-order-4').DataTable({ 
 
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('products/bulk_ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.category = $('#category').val();
                data.stock = $('#stock').val();
                data.hasimage = $('#hasimage').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });

    $('#category').change(function(){
        table.ajax.reload();
    });

    $("#stock").change(function(){
       table.ajax.reload();
    });

    $("#hasimage").change(function(){
       table.ajax.reload();
    });

    $("#sellerproduct").change(function(){
       table.ajax.reload();
    });

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });

     $("body").on("change",".imgupload",function(e){
        var imgid = $(this).attr("data-id");
        $(".btnupload1, .btnupload2, .btnupload3").trigger('click');
        //abc(imgid);
    });
 
    $("body").on("click",".btnupload1, .btnupload2, .btnupload3",function(e){

      var producttime = $(this).attr("data-name");
      var product = $(this).attr("data-id");
      var fileimg    = document.querySelector('#img-'+producttime).files[0];

      var image_id = $("#imgid-"+producttime).val();

      var reader  = new FileReader();
      if (typeof fileimg  !== "undefined") {
        if(['image/jpeg', 'image/jpg', 'image/png', 'image/gif'].indexOf($('#img-'+producttime).get(0).files[0].type) == -1) {
              alert('Error : Only JPEG, PNG & GIF allowed');
              return false;;
        }
        reader.onload = function(){

            var FileSize = fileimg.size / 1024 / 1024; // in MB

            if (FileSize > 5) {
                alert('File size exceeds 5 MB');
                return false;
            }
            fileimage = fileimg.name;

            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('products/Saveimage');?>",
                dataType: 'json',
                cache: false,
                data: {
                  productid: product,
                  file: reader.result,
                  filename: fileimage,
                  imageID: image_id
                },
                success: function(response) {
                    if(response.msg == true){
                        alert("Success: Upload successfully !");
                        table.ajax.reload();
                    }
                },
                error: function(response) {
                    alert(response.data);
                    table.ajax.reload();
                },
            });
        };
        reader.readAsDataURL($('#img-'+producttime).get(0).files[0]);  
      }
    })

    $("body").on("click","#removeimg, #removeimg1, #removeimg2",function(e){
        var imageID = $(this).attr("data-id");
        if (confirm('Are you sure want to delete image ?')) {
          $.ajax({
              type: 'POST',
              url: "<?php echo base_url('products/DeleteImage');?>",
              dataType: 'json',
              cache: false,
              data: {
                image: imageID
              },
              success: function(response) {
                  if(response.msg == true){
                      alert("Success: Image Removed !");
                      table.ajax.reload();
                  }
              },
              error: function(response) {
                  table.ajax.reload();
              },
          });
        }
    });
});
</script>
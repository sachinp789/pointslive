<?php
$this->load->model('Chatbots_model');
$chatdetails = $this->Chatbots_model->getChatData();

if($chatdetails){
  $providers = unserialize($chatdetails->service_provides);
  $offerdays = @unserialize($chatdetails->offer_days);
  $stateschecked = unserialize($chatdetails->offer_states);
}
$info = admin_info($this->session->userdata('adminId'));
// States list by country of seller 
if(!empty($info->country)){
  $states = GetStatesByCountry($info->country);
}
else{
  $states = [];
}
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Do You Offer Returns ? <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12" id="returnofferdiv">
                          <select class="form-control" id="returnoffer" name="offer">
                              <option value="0" <?php echo(!empty($chatdetails->return_offer) && $chatdetails->return_offer == 0) ? "selected":"";?>>No</option>
                              <option value="1" <?php echo(!empty($chatdetails->return_offer) && $chatdetails->return_offer == 1) ? "selected":"";?>>Yes</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group offer-return" style=<?php echo(!empty($chatdetails->return_offer) && $chatdetails->return_offer == 1) ? "display:block":"display:none";?>>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Returns Days ? <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="checkbox services-days">
                            <label>
                              <input type="checkbox" value="7" name="daysoffer[]" <?php echo(!empty($chatdetails->offer_days) && in_array("7", $offerdays)) ? "checked" : "";?>> 7 Days
                            </label>
                          </div>
                          <div class="checkbox services-days">
                            <label>
                              <input type="checkbox" value="14" name="daysoffer[]" <?php echo(!empty($chatdetails->offer_days) && in_array("14", $offerdays)) ? "checked" : "";?>>14 Days
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="item form-group" id="serviceofferdiv">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Do You Offer Delivery Services ? <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="serviceprovide" id="serviceprovide">
                              <option value="0" <?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 0) ? "selected" : ""?>>No</option>
                              <option value="1" <?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 1) ? "selected" : ""?>>Yes</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group services-offer" style="<?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 1) ? "display:block":"display:none"?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">What Your Delivery Options ? <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12" <?php echo(!empty($chatdetails->delivery_services) && $chatdetails->delivery_services == 1) ? "style=display:block":"display:none"?>>
                          <div class="checkbox">
                            <label>
                            <input type="checkbox" value="Postal Service" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Postal Service", $providers)) ? "checked" : "";?>>Postal Service
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="Standard Delivery 5 + Days" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Standard Delivery 5 + Days", $providers)) ? "checked" : "";?>>Standard Delivery 5 + Days 
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="Express Delivery 2 - 4 Days" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Express Delivery 2 - 4 Days", $providers)) ? "checked" : "";?>>Express Delivery 2 - 4 Days
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="Next Day Delivery" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Next Day Delivery", $providers)) ? "checked" : "";?>>Next Day Delivery
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="Same Day Delivery" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("Same Day Delivery", $providers)) ? "checked" : "";?>> Same Day Delivery
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" value="International Standard" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("International Standard", $providers)) ? "checked" : "";?>>International Standard
                            </label>
                          </div>
                          <div class="checkbox lastone">
                            <label>
                              <input type="checkbox" value="International Express" name="services[]" <?php echo(!empty($chatdetails->service_provides) && in_array("International Express", $providers)) ? "checked" : "";?>>International Express
                            </label>
                          </div>
                        </div>
                      </div>
                      <?php if(!is_null($states)):?>
                      <div id="delivery_states" class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">States You Operate in<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="checkbox">
                            <label style="font-weight: bold;">
                             <input type="checkbox" id="checkAll">Select All States
                            </label>
                          </div>
                          <?php foreach($states as $state): //var_dump($state);?>
                          <div class="checkbox state-delivery">
                            <label>
                              <?php if($info->country == 'thailand'):?>
                              <input type="checkbox" class="state-country" value="<?php echo $state->province?>" name="offerstates[]" <?php echo(!empty($chatdetails->offer_states) && in_array($state->province, $stateschecked)) ? "checked" : "";?>> <?php echo $state->province; ?>
                              <?php elseif($info->country == 'nigeria'):?>
                                <input type="checkbox" class="state-country" value="<?php echo $state->state_name?>" name="offerstates[]" <?php echo(!empty($chatdetails->offer_states) && in_array($state->state_name, $stateschecked)) ? "checked" : "";?>> <?php echo $state->state_name; ?>
                              <?php endif;?> 
                            </label>
                          </div>
                        <?php endforeach;?>
                        </div>
                      </div>
                      <?php endif;?>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="submit" name="btnservice" id="btnservice" value="Save" class="btn btn-success">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
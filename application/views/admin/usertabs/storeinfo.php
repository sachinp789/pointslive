<?php
$this->load->model('Chatbots_model');
$chatdetails = $this->Chatbots_model->getChatData();
if($chatdetails){
  $businesshours = unserialize($chatdetails->business_hours);
  if($businesshours){  
    $exptimes = implode("-", $businesshours);
  }
}
else{
  $businesshours = [];
}
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                    <?php endif; ?>
                    <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                      <div class="item form-group" id="steps3">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Opening and Closing Time <span class="required"></span>
                        </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="businesshours" id="businesshours">
                              <option value="alltime" <?php echo(!empty($chatdetails->store_open) && $chatdetails->store_open == "alltime") ? "selected" : ""; ?>>24 hours</option>
                              <option value="custom" <?php echo(!empty($chatdetails->store_open) && $chatdetails->store_open == "custom") ? "selected" : ""; ?>>Custom</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group" id="times" style="<?php echo(!empty($chatdetails->store_open) && $chatdetails->store_open == "custom") ? "display:bock" : "display:none"; ?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                           <div class="range-day" id="range-day-1" data-day="1">
                              <input type="checkbox" name="day-1" id="day-1" value="Mon" class="range-checkbox" <?php if($businesshours):if(in_array("Mon-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-1" class="range-label">Monday:</label>
                              <div id="range-slider-1" class="range-slider1"></div>
                              <span id="range-time-1" class="range-time"></span>
                            </div>
                            
                            <div class="range-day" id="range-day-2" data-day="2">
                              <input type="checkbox" name="day-2" id="day-2" value="Tue" class="range-checkbox" <?php if($businesshours):if(in_array("Tue-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-2" class="range-label">Tuesday:</label>
                              <div id="range-slider-2" class="range-slider2"></div>
                              <span id="range-time-2" class="range-time"></span>
                            </div>
                            
                            <div class="range-day" id="range-day-3" data-day="3">
                              <input type="checkbox" name="day-3" id="day-3" value="Wed" class="range-checkbox" <?php if($businesshours):if(in_array("Wed-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-3" class="range-label">Wednesday:</label>  
                              <div id="range-slider-3" class="range-slider3"></div>
                              <span id="range-time-3" class="range-time"></span>
                            </div>
                            
                            <div class="range-day" id="range-day-4" data-day="4">
                              <input type="checkbox" name="day-4" id="day-4" value="Thu" class="range-checkbox" <?php if($businesshours):if(in_array("Thu-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-4" class="range-label">Thursday‎:</label>  
                              <div id="range-slider-4" class="range-slider4"></div>
                              <span id="range-time-4" class="range-time"></span>
                            </div>
                            
                            <div class="range-day" id="range-day-5" data-day="5">
                              <input type="checkbox" name="day-5" id="day-5" value="Fri" class="range-checkbox" <?php if($businesshours):if(in_array("Fri-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-5" class="range-label">Friday:</label>  
                              <div id="range-slider-5" class="range-slider5"></div>
                              <span id="range-time-5" class="range-time"></span>
                            </div>
                            
                            <div class="range-day" id="range-day-6" data-day="6">
                              <input type="checkbox" name="day-6" id="day-6" value="Sat" class="range-checkbox" <?php if($businesshours):if(in_array("Sat-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-6" class="range-label">Saturday:</label>  
                              <div id="range-slider-6" class="range-slider6"></div>
                              <span id="range-time-6" class="range-time"></span>
                            </div>
                            
                            <div class="range-day" id="range-day-7" data-day="7">
                              <input type="checkbox" name="day-7" id="day-7" value="Sun" class="range-checkbox" <?php if($businesshours):if(in_array("Sun-Closed", $businesshours)){echo "";}else{echo "checked";}else: echo "";endif;?>>
                              <label for="day-7" class="range-label">Sunday:</label>  
                              <div id="range-slider-7" class="range-slider7"></div>
                              <span id="range-time-7" class="range-time"></span>
                            </div>
                            <input type="hidden" name="days[]" value="" id="range-time-val-1" class="range-time-val">
                            <input type="hidden" name="days[]" value="" id="range-time-val-2" class="range-time-val">
                            <input type="hidden" name="days[]" value="" id="range-time-val-3" class="range-time-val">
                            <input type="hidden" name="days[]" value="" id="range-time-val-4" class="range-time-val">
                            <input type="hidden" name="days[]" value="" id="range-time-val-5" class="range-time-val">
                            <input type="hidden" name="days[]" value="" id="range-time-val-6" class="range-time-val">
                            <input type="hidden" name="days[]" value="" id="range-time-val-7" class="range-time-val">
                            <!-- <div class="servicetime" style="display: none"></div> -->
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="submit" name="btnconnect" id="btnconnect" value="Save" class="btn btn-success">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
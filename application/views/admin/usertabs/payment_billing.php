<?php
$this->load->model('Chatbots_model');
$chatdetails = $this->Chatbots_model->getChatData();
if($chatdetails){
  $banks = unserialize($chatdetails->bank_info);
}
$banklists = getbanks();

$last = $this->db->order_by('id','desc')
      ->where('created_by',$this->session->userdata('adminId'))
      ->limit(1)
      ->get('subscription_orders')
      ->row();
if($last){
  $dt = new DateTime($last->created_date);
  $registereddate = $dt->format('Y-m-d');
  $afteronemonth = get_month_diff($registereddate,false);
} 
else{
  $dt = new DateTime($profileinfo->created_date);
  $registereddate = $dt->format('Y-m-d');
  $afteronemonth = get_month_diff($registereddate,false);
} 
$info = admin_info($this->session->userdata('adminId'));

$rightnow = date($info->created_date);
$add7days = date('Y-m-d', strtotime("$rightnow +7 days"));  

if(!empty($info->country)): 
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                   <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                        <?php echo $this->session->flashdata('flashmsg'); ?>
                      </div>
                      <?php endif; ?>
                      <?php 
                      $friday = date( 'Y-m-d', strtotime( 'last friday' ) ); // friday this week
                      if(date('Y-m-d') == date('Y-m-d',strtotime('friday'))){
                        $fridaydate = date('Y-m-d');
                      }else{
                        $fridaydate = $friday;
                      }

                      $today = date('Y-m-d');
                      $nextbill = date('Y-m-d',strtotime(date("Y-m-d", strtotime($fridaydate))." +7 days")); 
                      ?>
                      <?php if($info->country == "nigeria"):?>
                      <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$profileinfo->admin_id?>"  method="post" enctype="multipart/form-data">
                        <div class="row" id="steps5">
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name<span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="form-control" name="bank_name" id="bank_name">
                              <option value="">-Select Bank-</option>
                              <?php foreach($banklists as $bank) {?>
                                <option value="<?php echo $bank->bank_code?>" <?php echo($banks['bank_name'] == $bank->bank_code) ? "selected" : "";?>><?php echo $bank->bank_name;?></option>
                              <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Number<span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="account_number" value="<?php echo($banks) ? $banks['account_number'] : '';?>" class="form-control" id="account_number">
                              </div>
                          </div>
                          <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Name<span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="account_name" value="<?php echo($banks) ? $banks['account_name'] : '';?>" class="form-control" id="account_name">
                              </div>
                          </div>
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <p style="border:1px solid #cccccc;padding: 5px;">Your next payment will be made on : <span class="label label-info"><?php echo date('d F Y',strtotime($nextbill));?></span> <br> 
                              Amount to Be Paid : <span>₦<?php echo empty($orders['Totalamount']) ? 0 : '<a href="<?php echo base_url("order/finances")?>" class="label label-info">'.@$orders['Totalamount'].'</a>'?></span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                            <input type="submit" name="btnpayment" id="btnpayment2" value="Save" class="btn btn-success btnpayment-payment">
                          </div>
                        </div>
                      </form>
                      <?php else : ?>
                      <form class="form-horizontal form-label-left" novalidate action="<?php echo base_url().'user/saveChatbotDetails/'.$chatdetails->id?>"  method="post" enctype="multipart/form-data">
                        <div id="steps5">
                          <div class="row">
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><img src="<?php echo base_url() ?>assets/img/omise1.png" alt="" width="80">
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <p style="padding-top: 8%;"><strong><?php echo strtoupper("omise credentials");?></strong></p>
                              </div>
                            </div>
                          </div>

                          <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Public Key<span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="public_key" value="<?php echo(trim($chatdetails->public_key));?>" class="form-control" required>
                              </div>
                          </div>
                          <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Secret Key<span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="secret_key" value="<?php echo(trim($chatdetails->secret_key));?>" class="form-control" required>
                              </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                              <input type="submit" name="btnpayment" id="btnpayment-3" value="Save" class="btn btn-success btnpayment-payment">
                          </div>
                        </div>
                      </form>
                      <?php endif;?>
                      <form class="form-horizontal form-label-left" novalidate method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/paywithflutterwave')?>">
                        <?php if($afteronemonth == 1):?>
                          <div class="ln_solid"></div>
                          <input type="hidden" name="amount" value="0" id="payamount"/> <!-- Replace the value with your transaction amount -->
                          <input type="hidden" name="payment_method" value="both" />
                          <!-- Replace the value with your transaction description -->
                          <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                          <input type="hidden" name="currency" value="USD" /> <!-- Replace the value with your transaction currency -->
                          <input type="hidden" name="email" value="<?php echo $info->admin_email;?>"/> <!-- Replace the value with your customer email -->
                          <input type="hidden" name="ref" value="<?php echo uniqid();?>" />
                          
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">When Subscription due<span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                             <div class="checkbox">
                              <label>
                                <input type="checkbox" value="100" name="servicescharge" id="servicescharge">$100 (Ritekit Plan)
                              </label>
                              <br><br>
                              <p>
                                <b>Social Media Marketing</b> 
                                <ul>
                                  <li>Set Hashtags and Keywords for your business</li>
                                  <li>Find Influencers for your business and mention them in your posts </li>
                                  <li>Schedule up to 5 enhanced posts at a time across <li>Facebook, Twitter and Instagram</li>
                                  <li>Promoted post with popular blogs on Facebook, Instagram and Twitter</li>
                                </ul>
                              </p>
                              <div class="ln_solid"></div>
                             </div>
                            </div>
                          </div>
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Auto renew payment ?<span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox" name="subscriptionplan" id="subscriptionplan" value="<?php echo($chatdetails->renew_subscription == "1") ? "1":"0";?>" <?php echo($chatdetails->renew_subscription == "1") ? "checked":"";?>>
                                </label>
                              </div>
                            </div>
                          </div>
                        <?php 
                        $extenddate = date('Y-m-d', strtotime($dt. ' + 3 days')); // Add  3 days in due date
                        $date1 = date_create($extenddate);
                
                        $date2 = date_create(date('Y-m-d'));
                        //difference between two dates
                        $diff = date_diff($date1,$date2);
                        $duedays = $diff->format("%a"); // Difference in days
                        if(date('Y-m-d') === $date || date('Y-m-d') <= $extenddate) : ?>
                        <div class="form-group">
                          <div class="col-md-6 col-md-offset-3">
                            <input type="submit" name="btnpay" id="btnpay" value="Payment" class="btn btn-info">
                            <br><br>
                            <h4><span class="label label-primary">Note : No need to pay if you subscribed auto renew option.</span></h4>
                          </div>
                        </div>
                        <?php endif;?>
                       </form>
                      <?php
                      endif;
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</div>
<?php else:?>
<script type="text/javascript">
  alert("WARNING: Please select country from profile section !");
  window.location.href="<?php echo base_url("admin/profile/{$info->admin_id}")?>";
</script>
<?php endif;?>
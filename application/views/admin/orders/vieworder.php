<?php 
$this->load->model('Orders_model');
$havedisocunt = $this->db->select('coupon_id,total_discount')->from('coupon_redemptions')->where('order_id',$orders[0]->transaction_id)->get()->row(); 
if(!is_null($havedisocunt)):
$coupons = $this->db->select('code,by_apply,discount_value')->from('coupons')->where('id',$havedisocunt->coupon_id)->get()->row();     
endif;
if(is_null($orders[0]->receiver_name)){ // facebook_username
  $name = facebook_username($orders[0]->user_id,$orders[0]->page_id);
  $this->Orders_model->where('transaction_id',$orders[0]->transaction_id)->update(array("facebook_username" => $name));
  $orderupdate = $this->Orders_model->get($orders[0]->id);
  $username = $orderupdate->facebook_username;
}
else{
  $username = $orders[0]->facebook_username;
}
?>
<div class="right_col" role="main" style="min-height: 949px;">

          <div class="">

            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                  <div class="x_title">
                    <a href="<?php echo base_url("order/listOrders");?>" class="btn btn-sm btn-default">Back</a>
                    <h2><?php //echo $title ?></h2>
                      <a href="<?php echo base_url() ?>order/printInvoice/<?php echo $orders[0]->transaction_id?>" class="btn btn-md btn-default pull-right"><i class="fa fa-print"></i> Print Invoice</a>
                    <div class="clearfix"></div>

                  </div>

                <div class="x_content">

                  <section class="content invoice" id="invoiceorder">

                      <!-- title row -->

                     

                      <div class="row">

                        <div class="col-xs-12 invoice-header">

                          <h2 style="margin-left:0px;">

                              <i class="fa fa-list"></i> Order Summury

                          </h2>

                        </div>

                        <!-- /.col -->

                      </div>

                      <!-- info row -->

                      <div class="row invoice-info">

                        <div class="col-sm-4 invoice-col">
                          <div class="order-summary-column">
                           <p><b>CUSTOMER NAME : </b><span><?php echo ($orders[0]->receiver_name !="") ? $orders[0]->receiver_name : $username;?></span></p>
                           <p> <b>ADDRESS : </b><?php echo $orders[0]->shipping_address;?><span></span></p>
                           <p> <?php echo $orders[0]->district;?>, <?php echo $orders[0]->country;?><span></span></p>
                           <p><?php echo $orders[0]->postcode;?><span></span></p>
                           <p><b>EMAIL : </b><span><?php echo $orders[0]->email;?></span></p>
                           <p><b>PHONE : </b><span><?php echo $orders[0]->phone;?></span></p>
                          </div>
                        </div>
                        <!-- /.col -->

                        <div class="col-sm-4 invoice-col">
                          <div class="order-summary-column">
                          <p><b>PAYMENT TYPE : </b><span><?php echo ucfirst($orders[0]->payment_method) ?></span></p>
                          <p><b>SHIPPING TYPE : </b><span>
                            <?php
                              $shippingtypes = array();
                                    $shiptypes = $this->Orders_model->get_shippingtypes($orders[0]->transaction_id);

                              for ($k = 0; $k < count($shiptypes); $k++) {
                                if ($shiptypes[$k]->order_id == $orders[0]->transaction_id) {
                                  $shippingtypes[] = $shiptypes[$k]->shipping_type;
                                }
                              }
                            ?>
                          <?php echo ucfirst(implode(",", str_replace("both", "Delivery & Offilne", $shippingtypes)));?>
                          </span></p>
                          <p> <b>COUPON : </b><span><?php
                             if(!is_null($havedisocunt)): 
                                $coupon = $coupons->code;
                              else :
                                $coupon = "-";
                              endif;
                           ?><?php echo $coupon;?></span></p>
                        </div></div>

                        <!-- /.col -->

                        <div class="col-sm-4 invoice-col">
                          <div class="order-summary-column">
                           <p> <b>ORDER ID :</b> <span><?php echo $orders[0]->transaction_id; ?></span></p>
                           <p> <b>ORDER DATE : </b><span><?php echo $orders[0]->created_date;?></span></p>
                           <?php
                           if($orders[0]->status == 0){
                            $class = "status-process";
                            $status = "Processing";
                           }
                           elseif($orders[0]->status == 1){
                            $class = "status-dispatch";
                            $status = "Dispatched";
                           }
                           elseif($orders[0]->status == 2){
                            $class = "status-success";
                            $status = "Delivered";
                           }
                           elseif($orders[0]->status == 3){
                            $class = "status-cancel";
                            $status = "Canceled";
                           }
                           elseif($orders[0]->status == 5){
                            $class = "status-fail";
                            $status = "Failed";
                           }
                           elseif($orders[0]->status == 6){
                            $class = "status-success";
                            $status = "Completed";
                           }
                           elseif($orders[0]->status == 7){
                            $class = "status-fail";
                            $status = "Confirmed Failed";
                           }
                           ?>
                           <p class="order-status"> <b>ORDER STATUS : </b><span class="order-<?php echo $class ?>"><?php echo $status;?></span></p>
                          </div>
                        </div>

                        <!-- /.col -->

                      </div>

                      <!-- /.row -->
                      <div class="row"></div>
                      <!-- Table row -->

                      <div class="row">

                        <div class="col-xs-12 table">

                          <table class="table order-summery-table">

                            <thead>

                              <tr>

                                <th>Product</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Price</th>

                              </tr>

                            </thead>

                            <tbody>

                            <?php

                              $shipcost = 0;

                              $subtotal = 0;

                              $grandtotal = 0;

                              $price = 0;

                              if(empty($orders[0]->products)){



                              $packgaes = $this->db->select('*')

                                          ->from('shipping_packages')

                                          ->where('transaction_id',$orders[0]->transaction_id)

                                          ->get()->row();
                                          
                              $orderqty = $this->db->select('*')
                                          ->from('orders_products')
                                          ->where('order_id',$orders[0]->transaction_id)
                                          ->get()->row();

                              ?>

                              <tr>  

                                    <td><?php echo $packgaes->package_type ?></td>
                                    <td class="text-center" style="width: 100px;"><?php echo $orderqty->ordered_qty; ?></td>
                                    <td class="text-center" style="width: 100px;"><?php echo $this->config->item('currency'); ?><?php echo $orders[0]->order_amount ?></td>

                                    <?php $subtotal = $subtotal + $orders[0]->order_amount;?>

                                </tr> 

                              <?php

                              }

                              else{

                              foreach ($orders as $order) {

                                foreach ($order->products as $key => $product) {

                                  if($product->sale_price > 0){

                                    $price = $product->sale_price;

                                  }else{

                                    $price = $product->product_price;

                                  }

                              ?>

                              <tr>  

                                  <?php $shipcost = $shipcost + $product->shipping_cost;?>

                                  <td><?php echo $product->product_name ?></td>
                                  <td class="text-center" style="width: 100px;"><?php echo $product->ordered_qty ?></td>
                                  <td class="text-center" style="width: 100px;"><?php echo $this->config->item('currency'); ?><?php echo $price ?></td>

                                  <?php $subtotal = $subtotal + ($product->ordered_qty * $price);?>

                              </tr>           

                              <?php  

                                } 

                              }    

                            }

                            ?>

                            </tbody>

                          </table>

                        </div>

                        <!-- /.col -->

                      </div>

                      <!-- /.row -->
                     

                      <div class="row">

                        <!-- accepted payments column -->

                        <div class="col-xs-7"><!-- <a href="<?php echo base_url("order/listOrders");?>" class="btn btn-sm btn-default">Back</a> --></div>
                        <!-- /.col -->

                        <div class="col-xs-5">

                          <div class="table-responsive">

                            <table class="table order-summery-table">

                              <tbody>

                                <tr>

                                  <th style="width:50%" class="text-right">Subtotal</th>

                                  <td class="text-right"><?php echo $this->config->item('currency'); ?><?php echo $subtotal;?></td>

                                </tr>

                                <tr>

                                  <th style="width:50%" class="text-right">Shipping Cost</th>

                                  <td class="text-right"><?php echo $this->config->item('currency'); ?><?php echo $shipcost;?></td>

                                </tr>
                                <tr>
                                <?php
                                $discount = 0;
                                if(!is_null($havedisocunt)): 
                                $discount = $havedisocunt->total_discount;
                                else :
                                $discount = 0;
                                endif;  
                                ?>

                                  <th class="text-right">Coupon</th>

                                  <td class="text-right"><?php echo $this->config->item('currency'); ?><?php echo $discount;?></td>

                                </tr>
                                <tr>

                                  <th class="text-right" class="text-right">Total</th>

                                  <td class="text-right"><?php echo $this->config->item('currency'); ?><?php echo $grandtotal = ($subtotal + $shipcost) - ($discount); ?></td>

                                </tr>

                              </tbody>

                            </table>

                          </div>

                        </div>

                        <!-- /.col -->

                      </div>
    
                     </div>
                      

                    </section>

                </div>

            </div>

        </div>

      </div>

  </div>

</div>
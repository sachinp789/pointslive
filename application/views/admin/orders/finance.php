<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <a href="<?php echo base_url('order/exportFinanceCharges')?>" class="btn btn-md btn-success pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
	              <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                  </button>
	                    <?php echo $this->session->flashdata('flashmsg'); ?>
	              </div>
	              <?php endif; ?>
                <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <h3 class="panel-title" >Filter</h3>
                </div> -->
                <div class="panel-body">
                <form id="form-filter" class="form-horizontal mobile-lists">
                    <div class="form-group">
                        <label for="country" class="col-sm-2 col-xs-6 control-label">Transaction Type</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control" id="transaction_type">
                                <option value="">All</option>
                                <option value="commission">Commission</option>
                                <option value="auto cancel fee">Auto Cancel Fee</option>
                                <!-- <option value="delivery">Delivery</option>
                                <option value="offline">Offline</option>
                                <option value="both">Delivery & Offline</option> -->
                            </select>
                        </div>
                       <!--  <label for="country" class="col-sm-2 control-label">Payment Type</label> -->
                       <!--  <div class="col-sm-2">
                            <select class="form-control" id="payment_type">
                                <option value="">All Payment</option>
                                <option value="cash">Cash</option>
                                <option value="prepaid">Prepaid</option>
                            </select>
                        </div> -->
                        <div class="col-sm-2 col-xs-6">
                            <label for="country" class="control-label">Payment Cycle From</label><br>
                            <label for="country" class="control-label">Payment Cycle To</label>
                        </div>
                         <div class="col-sm-2 col-xs-6">
                            <input class="form-control mobile-mob" id="payment_from" type="text" name="payment_from" style="margin-bottom: 5px;"/>
                            <input class="form-control" id="payment_to" type="text" name="payment_to"/>
                        </div>
                        <label for="country" class="col-sm-1 col-xs-6 control-label">Payment Status</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control" id="payment_status">
                                <option value="">All Status</option>
                                <option value="0">Not Paid</option>
                                <option value="1">Paid</option>
                                <!-- <option value="2">Partial</option> -->
                            </select>
                        </div>
                        <button type="button" id="btn-reset" class="btn btn-default mobile-btn">Reset</button>
                    </div>
                </form>
                </div>
              </div>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table id="table-orders" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            	<th>Order No</th>
                            	<th>Order Date</th>
                                <th>Order Value</th>
                                <th>Transaction Type</th>
                                <th>Transaction Amount</th>
                                <th>Transaction Date</th>
                                <th>Billing Amount</th>
                                <th>Payment Cycle</th>
                                <th>Payment Status</th>
                            </thead>
	                      	<tbody> 
	                       </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
<!-- <script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script> -->

<script type="text/javascript">

$(document).ready(function() {
    //datatables
    var table = $('#table-orders').DataTable({ 
 
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('order/finance_ajax_list')?>",
            "type": "POST",
            "data": function ( data ) {
                data.transaction_type = $('#transaction_type').val();
                data.payment_status = $('#payment_status').val();
                data.payment_from = $('#payment_from').val();
                data.payment_to = $('#payment_to').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });

    $('#transaction_type').change(function(){
        table.ajax.reload();
    });

    $("#payment_status").change(function(){
       table.ajax.reload();
    });

    $("#payment_from").blur(function(){
       table.ajax.reload();
    });

    $("#payment_to").blur(function(){
       table.ajax.reload();
    });
 
    /*$('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });*/

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });
 
});

$(function() {
    //$("#start_date").datepicker();
  $('input[name="payment_from"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: false,
    locale: {
      format: 'YYYY-MM-DD'
    },
    autoUpdateInput: false

  }, function(start, end, label) {
    //$('#payment_from').val(start.format('YYYY-MM-DD'));

  });

  $("#payment_from").on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD'));
  });

    $('input[name="payment_to"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: false,
    locale: {
      format: 'YYYY-MM-DD'
    },
    autoUpdateInput: false

  }, function(start, end, label) {
    //$('#payment_to').val(start.format('YYYY-MM-DD'));

  });

  $("#payment_to").on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD'));
  });

});
 
</script>
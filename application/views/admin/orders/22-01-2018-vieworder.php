<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                <div class="x_content">
                  <section class="content invoice" id="invoiceorder">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                              <i class="fa fa-globe"></i> Invoice.
                              <small class="pull-right">Order Date: <?php echo $orders[0]->created_date; ?></small>
                          </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          From
                          <address>
                              <strong><?php echo $this->config->item('site_name'); ?></strong>
                          </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          To
                          <address>
                            <?php 
                            echo $orders[0]->shipping_address;
                            ?>
                           <!--  <strong>John Doe</strong>
                            <br>795 Freedom Ave, Suite 600
                            <br>New York, CA 94107
                            <br>Phone: 1 (804) 123-9876
                            <br>Email: jon@ironadmin.com -->
                        </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <b>Order ID:</b> <?php echo $orders[0]->transaction_id; ?>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Price</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                              $shipcost = 0;
                              $subtotal = 0;
                              $grandtotal = 0;
                              $price = 0;
                              foreach ($orders as $order) {
                                foreach ($order->products as $key => $product) {
                                  if($product->sale_price > 0){
                                    $price = $product->sale_price;
                                  }else{
                                    $price = $product->product_price;
                                  }
                              ?>
                              <tr>  
                                  <?php $shipcost = $shipcost + $product->shipping_cost;?>
                                  <td><?php echo $product->ordered_qty ?></td>
                                  <td><?php echo $product->product_name ?></td>
                                  <td>$<?php echo $price ?></td>
                                  <?php $subtotal = $subtotal + ($product->ordered_qty * $price);?>
                              </tr>           
                              <?php  
                                }     
                              }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          <p class="lead">Payment Methods:</p>
                          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                           <?php echo $orders[0]->payment_method;?>
                          </p>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-5">
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Subtotal:</th>
                                  <td>$<?php echo $subtotal; ?></td>
                                </tr>
                                <tr>
                                  <th>Shipping:</th>
                                  <td>$<?php echo $shipcost;?></td>
                                </tr>
                                <tr>
                                  <th>Total:</th>
                                  <td>$<?php echo $grandtotal = $subtotal + $shipcost; ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <?php if(is_null($orders[0]->shipment_date)): ?>
                          <div class="col-xs-4">
                            <p>Status : <span class="label label-info">Processing</span></p>
                          </div>
                          <form class="form-horizontal form-label-left" novalidate action='<?php echo base_url()."order/shipOrder/{$orders[0]->id}"?>' method="post">
                          <button class="btn btn-sm btn-info pull-left">Dispatch Order</button>
                          </form>
                          <?php endif;?>
                          <?php if(!is_null($orders[0]->shipment_date) && is_null($orders[0]->complete_date)):?>
                            <div class="col-xs-4">
                              <p>Status : <span class="label label-primary">Dispatched</span></p>
                            </div>
                            <form class="form-horizontal form-label-left" novalidate action='<?php echo base_url()."order/orderComplete/{$orders[0]->id}"?>' method="post">
                            <button class="btn btn-sm btn-success pull-left">Complete Order</button>
                            </form>
                          <?php endif;?>
                          <?php if(!is_null($orders[0]->complete_date)):?>
                          <div class="col-xs-4">
                            <p>Status : <span class="label label-success">Completed</span></p>
                          </div>
                          <a href="<?php echo base_url()."order/listOrders"?>" class="btn btn-sm btn-primary pull-left">Close</a>
                          <?php endif;?>
                        </div>
                      </div>
                    </section>
                </div>
            </div>
        </div>
      </div>
  </div>
</div>
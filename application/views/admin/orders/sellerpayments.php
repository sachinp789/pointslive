<?php
//facebook','instagram
$users = $this->db->select('admin_id,admin_name,admin_email')->from('admin')->where_in('login_with',array('twitter','seller'))->get()->result();
?>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
	              <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                  </button>
	                    <?php echo $this->session->flashdata('flashmsg'); ?>
	              </div>
	              <?php endif; ?>
                <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <h3 class="panel-title" >Filter</h3>
                </div> -->
                <div class="panel-body">
                <form id="form-filter" class="form-horizontal">
                    <div class="form-group">
                        <?php
                        if(empty($this->session->userdata('isLogin'))):
                        ?>
                        <label for="stock" class="col-sm-2 control-label">Seller</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="seller">
                                <option value="">All</option>
                                <?php
                                $seller = "";
                                foreach ($users as $user) {
                                  if(is_null($user->admin_name)){
                                    $nameofseller = $user->admin_email;
                                  }
                                  else{
                                    $nameofseller = $user->admin_name;
                                  }
                                  $seller.= "<option value=$user->admin_id>".$nameofseller."</option>";
                                }
                                echo $seller;
                                ?>
                            </select>
                        </div>
                        <?php endif;?>
                       <!--  <label for="country" class="col-sm-2 control-label">Payment Type</label> -->
                       <!--  <div class="col-sm-2">
                            <select class="form-control" id="payment_type">
                                <option value="">All Payment</option>
                                <option value="cash">Cash</option>
                                <option value="prepaid">Prepaid</option>
                            </select>
                        </div> -->
                        <label for="country" class="col-sm-1 control-label">Payment Status</label>
                        <div class="col-sm-2">
                            <select class="form-control" id="payment_status">
                                <option value="">All Status</option>
                                <option value="0">Unpaid</option>
                                <option value="1">Paid</option>
                                <option value="2">Partial</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label for="country" class="control-label">Payment Cycle From</label><br>
                            <label for="country" class="control-label">Payment Cycle To</label>
                        </div>
                         <div class="col-sm-2">
                            <input class="form-control" id="payment_from" type="text" name="payment_from" style="margin-bottom: 5px;"/>
                            <input class="form-control" id="payment_to" type="text" name="payment_to"/>
                        </div>
                        <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                    </div>
                </form>
                </div>
              </div>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table id="table-orders" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            	<th>Seller Name</th>
                            	<th>Payment Cycle</th>
                                <th>Total Amount</th>
                                <th>Auto Payment</th>
                                <th>Payment Status</th>
                                <th>Paid Amount</th>
                                <th>Unpaid Amount</th>
                                <th>Action</th>
                            </thead>
	                      	<tbody> 
	                       </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="requestform" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Pay Now</h3>
            </div>
            <div class="modal-body">
              
            <div class="requestwizard">
                <div class="requestwizard-row setup-panel">
                    <div class="requestwizard-step">
                        <a href="#step-1" type="button" id="step1" class="btn btn-primary btn-circle">1</a>
              
                    </div>
                    <div class="requestwizard-step">
                        <a href="#step-2" type="button" id="step2" class="btn btn-default btn-circle" disabled="disabled" style="pointer-events:none;">2</a>
                       
                    </div>
                </div>
            </div>
    <form role="form">
        <input type="hidden" value="" class="paymentid" name="payment_id">
        <input type="hidden" value="" class="sellername" name="sellername">
        <div class="row setup-content" id="step-1">
            <div class="col-xs-12">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Unpaid Amount</label>
                        <span style="padding-left: 15px;" class="amount">21545</span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount to Pay</label>
                        <input type="number" required="required" class="form-control textamount" placeholder="Amount" min="1"/>
                    </div>

                    <button class="btn btn-primary nextBtn btn-md pull-left" type="button" >Next</button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-2">
            <div class="col-xs-12">
                <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label">You will pay <span class='username'></span>, <span class='inputamt'></span></label>
                    </div>
                    <button type="button" class="btn prevbtn btn-primary">Cancel</button>
                    <button class="btn btn-primary nextBtn btn-md pull-right" type="button" id="btnpayment">Submit</button>
                </div>
            </div>
        </div>
    </form>
        </div>
    </div>
</div>
</div>

<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>

<script type="text/javascript">

$(document).ready(function () {

    $(document).on('click', '.center-block', function (event) {
        $("#step-1").css("display","block");
        $("#step-2").css("display","none");
        $("#step2").removeClass('btn-primary');
        $("#step2").addClass('btn-default');
        $("#step1").addClass('btn-primary');
        $(".textamount").val("");
        $(".textamount").focus();
        $(".has-err").remove();
        amt = $(this).attr("data-amount");
        seller = $(this).attr("data-seller");
        payment = $(this).attr("data-id");
        $(".paymentid").val(payment);
        $(".amount").text(amt);
        $(".sellername").val(seller);
    });

    $(".textamount").on('keyup mouseup',function(){
        unpaidamount = parseFloat($(".amount").text());
        seller = $(".sellername").val();
        inputval = parseFloat($(this).val());
       
        if(inputval > unpaidamount){
            $(".has-err").remove();
            $(".textamount").after("<span style='color:red' class='has-err'>* Please input valid amount</span>");
            $(".nextBtn").prop('disabled', true);
        }
        else{
            $(".has-err").remove();
            $(".nextBtn").prop("disabled",false);
            $(".inputamt").text(inputval);
            $(".username").text(seller);
        }
    })

   /* $('#requestform').on('show.bs.modal', function (e) {
       alert($(this).attr('seller'));
    });*/

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevbtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function(){
        $("#step-1").css('display','block');
        $("#step-2").css('display','none');
        $("#step1").addClass('btn-primary');
        $("#step2").removeClass('btn-primary');
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

$(document).ready(function() {
    //datatables
    var table = $('#table-orders').DataTable({ 
 
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('order/SellerPaymentLists')?>",
            "type": "POST",
            "data": function ( data ) {
                data.seller = $('#seller').val();
                data.payment_status = $('#payment_status').val();
                data.payment_from = $('#payment_from').val();
                data.payment_to = $('#payment_to').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });

    $('#seller').change(function(){
        table.ajax.reload();
    });

    $("#payment_status").change(function(){
       table.ajax.reload();
    });

    $("#payment_from").blur(function(){
       table.ajax.reload();
    });

    $("#payment_to").blur(function(){
       table.ajax.reload();
    });
 
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });
 
});

$(function() {
    //$("#start_date").datepicker();
  $('input[name="payment_from"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: false,
    locale: {
      format: 'YYYY-MM-DD'
    },
    autoUpdateInput: false

  }, function(start, end, label) {
    //$('#payment_from').val(start.format('YYYY-MM-DD'));

  });

  $("#payment_from").on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD'));
  });

    $('input[name="payment_to"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: false,
    locale: {
      format: 'YYYY-MM-DD'
    },
    autoUpdateInput: false

  }, function(start, end, label) {
    //$('#payment_to').val(start.format('YYYY-MM-DD'));

  });

  $("#payment_to").on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD'));
  });

});
 
</script>
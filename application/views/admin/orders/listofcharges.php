<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
	              <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                  </button>
	                    <?php echo $this->session->flashdata('flashmsg'); ?>
	              </div>
	              <?php endif; ?>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table aria-describedby="datatable_info" role="grid" id="datatable-responsive" class="table table-striped table-bordered dataTable no-footer orders">
                            <thead>
                            	<th>Order No</th>
                            	<th>Seller Name</th>
                            	<th>Order Amount</th>
                              <th>Akin Commission</th>
                            	<th>Auto Cancelled Charges</th>
                              <th>Payment Type</th>
                              <th>Order Date</th>
                            </thead>
	                      	<tbody> 
                          <?php
                          if($orders):
                            foreach ($orders as $key => $row) { ?>
                             <tr class="odd" role="row">
                              <td><?php echo $row->transaction_id ?> </td>
                              <td><?php echo $row->admin_name ?></td>
                              <td><?php echo $row->order_amount ?> </td>
                              <td><?php echo $row->akin_commission ?> </td>
                              <td><?php echo $row->auto_cancelled_fees ?></td>
                              <td><?php echo ucfirst($row->payment_method)  ?></td>
                              <td><?php echo $row->created_date ?></td>
                            </tr>
                            <?php } endif; 
                          ?>
	                       </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <a href="<?php echo base_url('order/exportOrders')?>" class="btn btn-md btn-success pull-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                    <label class="qrcode-text-btn scanner"><input onchange="openQRCamera(this);" tabindex="-1" type="file"></label>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                </div>
                <?php endif; ?>
                <div class="panel panel-default" style="border: none;-webkit-box-shadow: none !important;">
                <!-- <div class="panel-heading">
                    <h3 class="panel-title" >Filter</h3>
                </div> -->
                <div class="panel-body">
                <form id="form-filter" class="form-horizontal mobile-lists">
                    <div class="form-group">
                        <label for="country" class="col-sm-2 col-xs-6 control-label">Order Type</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control lists" id="shipping_type">
                                <option value="">All</option>
                                 <option value="delivery">Delivery</option>
                                <option value="pickup">Offline</option>
                               <!--  <option value="delivery">Delivery</option>
                                <option value="offline">Offline</option>
                                <option value="both">Delivery & Offline</option> -->
                            </select>
                        </div>
                        <label for="country" class="col-sm-2 col-xs-6 control-label">Payment Type</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control" id="payment_type">
                                <option value="">All Payment</option>
                                <option value="cash">Cash</option>
                                <option value="prepaid">Prepaid</option>
                            </select>
                        </div>
                        <label for="country" class="col-sm-2 col-xs-6 control-label">Status</label>
                        <div class="col-sm-2 col-xs-6">
                            <select class="form-control" id="payment_status">
                                <option value="">All Status</option>
                                <option value="0">Processing</option>
                                <option value="1">Dispatched</option>
                                <option value="2">Delivered</option>
                                <option value="3">Canceled</option>
                                <option value="5">Failed</option>
                                <option value="6">Completed</option>
                                <option value="7">Confirmed Failed</option>
                            </select>
                        </div>
                    </div>
                </form>
                </div>
              </div>
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table id="table-order" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                              <th>Order No</th>
                              <th>Username</th>
                              <th>Order Type</th>
                              <th>Payment Type</th>
                              <th>Purchased Date</th>
                              <th>Grand Total</th>
                              <th>Status</th>
                              <th>Action</th>
                            </thead>
                          <tbody> 
                         </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>

<script type="text/javascript">

$(document).ready(function() {
    //datatables
    var table = $('#table-order').DataTable({ 
 
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('order/ajax_list')?>",
            "type": "POST",
            "data": function ( data ) { //console.log(data);
                data.shipping_type = $('#shipping_type').val();
                data.payment_type = $('#payment_type').val();
                data.payment_status = $('#payment_status').val();
            }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": true, //set not orderable
        },
        ],
 
    });
 
    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });

    $('#shipping_type').change(function(){
        table.ajax.reload();
    });

    $("#payment_type").change(function(){
       table.ajax.reload();
    });

    $("#payment_status").change(function(){
        table.ajax.reload();
    });

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload();  //just reload table
    });
 
});
 
</script>
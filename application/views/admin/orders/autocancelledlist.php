<div class="right_col" role="main" style="min-height: 949px;">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
	              <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                  </button>
	                    <?php echo $this->session->flashdata('flashmsg'); ?>
	              </div>
	              <?php endif; ?>
                
                  <div class="x_content">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table id="table-order" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            	<th>Order No</th>
                            	<th>Username</th>
                              <th>Payment Type</th>
                            	<th>Purchased Date</th>
                            	<th>Grand Total</th>        	
                              <th>Action</th>          
                            </thead>
	                      	<tbody> 
                           <?php
                          if($orders):
                            foreach ($orders as $key => $row) {
                              ?>
                              <tr class="odd" role="row">
                              <td><?php echo $row->transaction_id; ?> </td>
                              <td><?php echo ($row->receiver_name !="") ? $row->receiver_name : $row->facebook_username; ?></td>
                              
                              <td><?php echo ucfirst($row->payment_method); ?> </td>
                              <td><?php echo $row->created_date ?> </td>
                              <td><?php echo $row->order_amount ?> </td>
                              <td>
                             <a href="<?php echo base_url() ?>order/viewCancelledOrder/<?php echo $row->transaction_id?>" class="btn btn-info btn-xs"> View</a>
                              </td>
                              
                            </tr>
                            <?php } endif; 
                          ?> 
	                       </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
<!-- <script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script> -->

<script type="text/javascript">

$(document).ready(function() {
    //datatables
    var table = $('#table-order').DataTable();
 
});
 
</script>
<?php 

  /*header("X-Frame-Options: ALLOW-FROM https://www.messenger.com/");

  header("X-Frame-Options: ALLOW-FROM https://www.facebook.com/");*/

  $currency = '฿';

  $this->load->model('Address_model'); 

  $check = $this->Address_model->where('user_id',$userID)->get_all();
  $discountamount = $this->cache->get("discountamount_{$userID}");
  //var_dump($this->session->userdata('deliverymethod'));
  //var_dump($this->session->userdata('redeemID'));
?>

<!DOCTYPE html>

<html lang="en">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title><?php echo $title ?></title>



    <!-- Bootstrap -->

    <link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->

    <link href="<?php echo base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- NProgress -->

    <link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->

    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">

  </head>



  <body class="login">

  <!--  <script>

        (function(d, s, id){

          var js, fjs = d.getElementsByTagName(s)[0];

          if (d.getElementById(id)) {return;}

          js = d.createElement(s); js.id = id;

          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";

          fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'Messenger'));

    </script> -->

    <div>

      <a class="hiddenanchor" id="signup"></a>

      <a class="hiddenanchor" id="signin"></a>



      <div class="sas login_wrapper">

          <div class="x_content">

           <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable-responsive_wrapper">

            <div class="row">

              <h2><?php echo $title ?></h2>

              <div class="separator">

                <div class="clearfix"></div>

              </div>

              <div class="col-sm-12">

              <?php

              //die("Dff");

              $type="sales";

              $payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";

              //echo $language;

              lang_switcher($language);

              ?>

              <form class="form-horizontal" novalidate action="<?php echo $payment_url ?>" method="post">

                <?php if(count($users) > 0): ?>

                <table role="grid" id="datatable-responsive1" class="table table-striped table-bordered dataTable no-footer">

                  <tr>

                        <th>#</th>

                        <th><?php echo $this->lang->line('pname');?></th>

                        <th><?php echo $this->lang->line('pqty');?></th>

                        <th><?php echo $this->lang->line('shipamt');?> (<?php echo $this->lang->line('pprice');?> + <?php echo $this->lang->line('shipcharge');?>)</th>

                  </tr>

                  <?php

                  $i=1;

                  $subtotal = 0;

                  $cost = 0;

                  $grandTotal = 0;

                  foreach($users as $user){

                    if($user->sale_price > 0){

                      $price = $user->sale_price;

                    }else{

                      $price = $user->product_price;

                    }

                  ?>

                    <tr>

                    <td><?php echo $i; ?></td>

                    <td><?php if($language == 'thai'){echo $user->product_name_thai;}else{ echo $user->product_name;} ?></td>

                    <td><?php echo $user->qty; ?></td>

                    <td><?php echo $currency.$price ." + {$this->lang->line('shipcharge')} : ".$currency.$user->shipping_cost; ?></td>

                    </tr>

                  <?php  

                  $i++;

                  $subtotal = $subtotal + $price * $user->qty;

                  $cost = $cost + $user->shipping_cost;

                  $grandTotal = ($subtotal + $cost) - ($discountamount);

                  }

                  ?>

                  <tr>

                    <td colspan="3"><?php echo $this->lang->line('subtotal') ?></td>

                    <td colspan="1"><?php echo $currency.$subtotal; ?></td>

                  </tr>

                  <tr>

                    <td colspan="3"><?php echo $this->lang->line('shipcharge') ?></td>

                    <td colspan="1"><?php echo $currency.$cost; ?></td>

                  </tr>

                  <?php
                  if(!empty($discountamount) && $discountamount > 0):
                  ?>
                  <tr>
                    <td colspan="3"><?php echo "Discount Amount (-)" ?></td>
                    <td colspan="1"><?php echo $currency.$discountamount; ?></td>
                  </tr>  
                  <?php
                  endif;
                  ?>
                  <tr>

                    <th colspan="3"><?php echo $this->lang->line('gt') ?></th>

                    <td colspan="1"><?php echo $currency.$grandTotal; ?></td>

                  </tr>

                </table>

                <?php 

                   $gtotalvalue = strchr($grandTotal,".");

                   $point = "00";

                   $total = $grandTotal;

                   if($gtotalvalue){

                    $point = explode(".", $gtotalvalue);

                    $beforepoint = explode(".", $grandTotal);

                    $point = $point[1];

                    $total = $beforepoint[0].$point;

                   }else{

                    $total = $total.$point;

                   }

                  //Merchant's account information

                  //$merchant_id = "JT01";    //Get MerchantID when opening account with 2C2P

                 // $secret_key = "7jYcp4FxFdf0"; //Get SecretKey from 2C2P PGW Dashboard



                  //Transaction information

                  $payment_description  = 'Points Live 2c2p Payment';

                  $order_id  = time();

                

                    $merchant_id = "JT04";    //Get MerchantID when opening account with 2C2P

                    $secret_key = "QnmrnH6QE23N"; //Get SecretKey from 2C2P PGW Dashboard



                    $currency = "764"; // USD-840 

                    

                 

                 

                   // $merchant_id = "JT01";    //Get MerchantID when opening account with 2C2P

                   // $secret_key = "7jYcp4FxFdf0"; //Get SecretKey from 2C2P PGW Dashboard

                   // $currency = "702"; // USD-840 

                 /* }*/

                  $amount  = str_pad($total, 12, "000000000000", STR_PAD_LEFT );



                  //Request information

                  $version = "7.2"; 

                  $payment_url = "https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment";

                  $result_url_1 = base_url()."admin/paymentResponse/$userID/$pageID/$type/{$language}";



                  //Construct signature string

                  $params = $version.$merchant_id.$payment_description.$order_id.$currency.$amount.$result_url_1;

                  $hash_value = hash_hmac('sha1',$params, $secret_key,false); //Compute hash value

              ?>

              <input type="hidden" name="version" value="<?php echo $version ?>"/>

              <input type="hidden" name="merchant_id" value="<?php echo $merchant_id ?>"/>

              <input type="hidden" name="currency" value="<?php echo $currency ?>"/>

              <input type="hidden" name="result_url_1" value="<?php echo $result_url_1 ?>"/>

              <input type="hidden" name="hash_value" value="<?php echo $hash_value ?>"/>

              <input type="hidden" name="payment_description" value="<?php echo $payment_description ?>"  readonly/>

              <input type="hidden" name="order_id" value="<?php echo $order_id ?>"  readonly/>

              <input type="hidden" name="amount" value="<?php echo $amount ?>" readonly/>

              <input type="submit" name="submit" value="<?php echo $this->lang->line('paybtn') ?>" class="btn btn-primary btn-md" id="payment" <?php if(!$check){} ?>/> 

              <div class="separator">

                <div class="clearfix"></div>

              </div>

              <!-- <div class="clearfix"></div> -->

              </form>
              <?php if($pageID != "1048681488539732"): ?>              
              <form class="form-horizontal" novalidate action="<?php echo base_url() ?>order/addShippingAddress/<?php echo $userID ?>" method="post" id="address">

              <h2><?php echo $this->lang->line('shipaddress'); ?></h2>

              <?php if($check): ?>

              <select id="address_select" class="form-control" required="">

                <option value=""><?php echo $this->lang->line('choose'); ?></option>

                <?php 

                  $text = "";
                  $selected = "";

                  foreach($check as $address){
                  if($address->id == $this->session->userdata('shipping_address')){
                    $selected='selected=selected';
                  }
                  else{
                    $selected='';
                  }  
                  $text.="<option value={$address->id} $selected>";

                  $text.= $address->shipping_address;

                  $text.="</option>";

                  }

                  echo $text;

                ?>

              </select>

              <?php endif;?>

              <div class="separator">

                <div class="clearfix"></div>

              </div>

              <input type="hidden" name="userid" id="userid" value="<?php echo $userID ?>">

              <?php endif; ?>
              <?php else: ?>

                <p class="text-muted well well-sm no-shadow">

                  * <?php echo $this->lang->line('noitems'); ?>

                </p>

              <?php endif; ?>

              <div class="clearfix"></div>

              </form>

              </div>

             </div>

            </div>

          </div>

      </div>

    </div>

    <!-- <script type="text/javascript">

      window.extAsyncInit = function() {

      // the Messenger Extensions JS SDK is done loading 

    };

    </script> -->

     <!-- jQuery -->

    <script src="<?php echo base_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->

    <script src="<?php echo base_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url() ?>assets/vendors/validator/validator.js"></script>

    <!-- Custom Theme Scripts -->
    <?php if($pageID != "1048681488539732"): ?>  
    <script type="text/javascript">

      $("#payment").click(function(){

          var address = $("#address_select").val();

          if(address == '' || address == null){

            alert('Please select shipping_address.')

            return false;

          }

          return true;

      });

      $("#btnadd").click(function(){



          var address1 = $("#address_line1").val();

          var city = $("#city").val();

          var state = $("#state").val();

          var country = $("#country").val();

          var zipcode = $("#zipcode").val();



          if(address1 == ''){

            alert("Please add shipping address_line 1 !");

            $("#address_line1").focus();

            return false;

          }

          if(city == ''){

            alert("Please enter city !");

            $("#city").focus();

            return false;

          }

          if(state == ''){

            alert("Please enter state !");

            $("#state").focus();

            return false;

          }

          if(country == ''){

            alert("Please enter zipcode !");

            $("#country").focus();

            return false;

          }

          if(zipcode == ''){

            alert("Please enter zipcode !");

            $("#zipcode").focus();

            return false;

          }

          return true;

      });



      $("#address_select").on('change',function(){

          var selected = $(this).val();

          if(selected != ''){

          $.ajax({

             url:'<?php echo base_url() ?>order/saveAddress',

             type: 'POST',

             dataType: 'json', //Added this

             data: "id=" + selected,

             success: function(response) {

                 alert(response);

             }

         });

        }

      });



    </script>
  <?php endif; ?>
   <!-- <script src="<?php echo base_url() ?>pointslive/assets/build/js/custom.min.js"></script> -->

    <!-- <script type="text/javascript">

      $("#btnadd").click(function(e){alert(1)  // passing down the event 

          $.ajax({

             url:'<?php echo base_url() ?>pointslive/order/addShippingAddress',

             type: 'POST',

             data: $("#address").serialize(),

             success: function(data){

                 console.log(data);return false;

                 $('#email').val('');

                 $('#qText').val('');

             },

             error: function(){

                 alert("Fail")

             }

         });

         e.preventDefault(); // could also use: return false;

      });

    </script> -->

  </body>

</html>


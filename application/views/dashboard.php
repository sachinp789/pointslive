<?php
$date = date('Y-m-d H:i:s', strtotime('yesterday'));

$page_id = getFacebookPageID();

$sellerinfo = admin_info($this->session->userdata('adminId'));

//count total user of chatbot
if($this->session->userdata('isLogin')){
     /* if(!empty($page_id)){
        $this->db->where(['page_id'=>$page_id]);
        $totalusers = $this->db->select('COUNT(*) as Total_users')->from('chat_languages')->get()->result();
      }else{
        $totalusers[0]->Total_users= "0";
      }*/

       $this->db->where(['admin_id'=>$this->session->userdata('adminId')]);
       $totalusers = $this->db->select('COUNT(*) as Total_users')->from('admin')->get()->result();

      }else{
      /*$totalusers = $this->db->select('COUNT(*) as Total_users')->from('chat_languages')->get()->result();*/
      $totalusers = $this->db->select('COUNT(*) as Total_users')->from('admin')->where('login_with IS NOT NULL')->get()->result();
}


// count new user since last week
if($this->session->userdata('isLogin')){
           /* if(!empty($page_id)){
              $this->db->where(['page_id'=>$page_id]);
              $new_users = $this->db->select('COUNT(*) as new_users')->from('chat_languages')->where("created_date BETWEEN CURRENT_TIMESTAMP - INTERVAL '7' DAY AND CURRENT_TIMESTAMP")->get()->result();
            }else{
              $new_users[0]->new_users= "0";
            }*/
             $this->db->where(['admin_id'=>$this->session->userdata('adminId')]); 	
            $new_users = $this->db->select('COUNT(*) as new_users')->from('admin')->where("created_date BETWEEN CURRENT_TIMESTAMP - INTERVAL '7' DAY AND CURRENT_TIMESTAMP")->get()->result();


          }else{
			/*$new_users = $this->db->select('COUNT(*) as new_users')->from('chat_languages')->where("created_date BETWEEN CURRENT_TIMESTAMP - INTERVAL '7' DAY AND CURRENT_TIMESTAMP")->get()->result();*/
			$new_users = $this->db->select('COUNT(*) as new_users')->from('admin')->where("created_date BETWEEN CURRENT_TIMESTAMP - INTERVAL '7' DAY AND CURRENT_TIMESTAMP")->get()->result();
}
// count user who make chat in last week
if($this->session->userdata('isLogin')){
            if(!empty($page_id)){
              $this->db->where(['page_id'=>$page_id]);
             $activeusers = $this->db->select('COUNT(*) as New_users')->from('chat_languages')->where("updated_date BETWEEN CURRENT_TIMESTAMP - INTERVAL '7' DAY AND CURRENT_TIMESTAMP")->get()->result();
            }else{
              $activeusers[0]->New_users= "0";
            }

          }else{
$activeusers = $this->db->select('COUNT(*) as New_users')->from('chat_languages')->where("updated_date BETWEEN CURRENT_TIMESTAMP - INTERVAL '7' DAY AND CURRENT_TIMESTAMP")->get()->result();
}
// count user who was not chat from last three weeks
if($this->session->userdata('isLogin')){
            if(!empty($page_id)){
              $this->db->where(['page_id'=>$page_id]);
            $inactive_users = $this->db->select("count(*) as inactive_user")->from('chat_languages')->having("MAX(chat_languages.updated_date) < CURDATE() - INTERVAL 21 DAY OR MAX(chat_languages.updated_date) IS NULL")->get()->result();
            }else{
              $inactive_users[0]->inactive_user= "0";
            }

          }else{
$inactive_users = $this->db->select("count(*) as inactive_user")->from('chat_languages')->having("MAX(chat_languages.updated_date) < CURDATE() - INTERVAL 21 DAY OR MAX(chat_languages.updated_date) IS NULL")->get()->result();
}

// confirmed order total amount is revenue
if($this->session->userdata('isLogin')){
            $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
              $revenue = $this->db->select("IFNULL(SUM(order_amount),0) as amount")->from('orders')->where(['status'=>'2'])->get()->result();
            /*if(!empty($page_id)){
              $this->db->where(['page_id'=>$page_id]);
              $revenue = $this->db->select("IFNULL(SUM(order_amount),0) as amount")->from('orders')->where(['status'=>'2'])->get()->result();
            }else{
              $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
              $revenue = $this->db->select("IFNULL(SUM(order_amount),0) as amount")->from('orders')->where(['status'=>'2'])->get()->result();
              //$revenue[0]->amount = "";
            }*/

          }else{            
$revenue = $this->db->select("IFNULL(SUM(order_amount),0) as amount")->from('orders')->where(['status'=>'2'])->get()->result();
}
// count total confirmed order of delivered
if($this->session->userdata('isLogin')){
              $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
              $confirmed = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
             // echo $this->db->last_query();
            /*if(!empty($page_id)){
              $this->db->where(['page_id'=>$page_id]);
              $confirmed = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
            }else{
              $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
              $confirmed = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
              //$confirmed[0]->total = "0";
            }*/

          }else{          
$confirmed = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
}

// count total confirmed order placed
if($this->session->userdata('isLogin')){
              $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
              $confirmedplacedorders = $this->db->select("count(*) as total")->from('orders')->get()->result();
            /*if(!empty($page_id)){
              $this->db->where(['page_id'=>$page_id]);
              $confirmed = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
            }else{
              $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
              $confirmed = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
              //$confirmed[0]->total = "0";
            }*/

          }else{          
$confirmedplacedorders = $this->db->select("count(*) as total")->from('orders')->get()->result();
}

// conversational rate, initiated chat / number of orders
if($this->session->userdata('isLogin')){

          // Rate calculation on 14-08-2018
          $this->db->where(['created_by'=>$this->session->userdata('adminId')]);
          $userorder = $this->db->select("count(*) as total")->from('orders')->where(['status'=>'2'])->get()->result();
          // ENd;

          // Comment :- 14-08-2018     
         /* $where_order = " AND created_by='".$this->session->userdata('adminId')."'";
              $this->db->where("orders.created_by",$this->session->userdata('adminId'));
              $userorder = $this->db->select("(SELECT count(*) as initiated FROM chat_languages)/(SELECT count(*) as total FROM orders where orders.status='0') as conversation_rate")->join('orders','orders.user_id=chat_languages.user_id','left')->where(['status'=>'2'])->get('chat_languages')->result();* END
/ 
         /*if(!empty($page_id)){
              $where = " WHERE page_id='$page_id'";
              $where_order = " AND page_id='$page_id'";
              $this->db->where("orders.page_id",$page_id);
              $userorder = $this->db->select("(SELECT count(*) as initiated FROM chat_languages $where)/(SELECT count(*) as total FROM orders where orders.status='0' $where_order) as conversation_rate")->join('orders','orders.user_id=chat_languages.user_id','left')->where(['status'=>'2'])->get('chat_languages')->result();
            }else{
              //$where = " WHERE created_by='".$this->session->userdata('adminId')."'";
              $where_order = " AND created_by='".$this->session->userdata('adminId')."'";
              $this->db->where("orders.created_by",$this->session->userdata('adminId'));
              $userorder = $this->db->select("(SELECT count(*) as initiated FROM chat_languages)/(SELECT count(*) as total FROM orders where orders.status='0') as conversation_rate")->join('orders','orders.user_id=chat_languages.user_id','left')->where(['status'=>'2'])->get('chat_languages')->result();
              //$userorder[0]->conversation_rate = "0";
              
            }*/

}else{
$userorder = $this->db->select("count(*) as total")->from('orders')->get()->result();
//var_dump($userorder);
/*$userorder = $this->db->select("(SELECT count(*) as initiated FROM chat_languages )/(SELECT count(*) as total FROM orders where orders.status='0' ) as conversation_rate")->join('orders','orders.user_id=chat_languages.user_id','left')->where(['status'=>'2'])->get('chat_languages')->result();
*/}


// total number of cancelled order / total confirmed order is cancellation ratio
/*if(!empty($page_id)){
  
  $where_order = " AND page_id='$page_id'";
  $this->db->where("orders.page_id",$page_id);
}else{
  $where = "";
  $where_order = " ";
}*/

if($this->session->userdata('isLogin')){
              $this->db->where("orders.created_by",$this->session->userdata('adminId'));
              $this->db->group_by('created_by');
              $order_cancel = $this->db->select("(SELECT count(*) from orders where status='3')/(SELECT count(*) FROM orders where status='2') as cancellations_rate")->from('orders')->get()->result();
              //echo $this->db->last_query();
             // var_dump($order_cancel);
  /*if(!empty($page_id)){
              $where = " WHERE page_id='$page_id'";
              $where_order = " AND page_id='$page_id'";
              $this->db->where("orders.page_id",$page_id);
              $order_cancel = $this->db->select("(SELECT count(*) from orders where status='3' $where_order)/(SELECT count(*) FROM orders where status='2' $where_order) as cancellations_rate")->from('orders')->get()->result();
            }else{
              //$order_cancel[0]->cancellations_rate = "0";
              $this->db->where("orders.created_by",$this->session->userdata('adminId'));
              $order_cancel = $this->db->select("(SELECT count(*) from orders where status='3')/(SELECT count(*) FROM orders where status='2') as cancellations_rate")->from('orders')->get()->result();
              
            }*/

}else{
  //  $where_order
  //  $where_order
$order_cancel = $this->db->select("(SELECT count(*) from orders where status='3')/(SELECT count(*) FROM orders where status='2') as cancellations_rate")->from('orders')->get()->result();
//var_dump($order_cancel);
}
// basket size
if(!empty($revenue) && !empty($confirmed)){
  if($revenue[0]->amount==""){
    $revenue[0]->amount = 0;
    $basket_size = "0";
  }else{ 
    $revenue_amount = $revenue[0]->amount;
    $basket_size = ($revenue_amount / $confirmed[0]->total); 
    //var_dump($basket_size);       
  } 
  
}else{
    $basket_size = "0";
}

//$views = 0;
$visitors_file = "visitors.txt";
if (file_exists($visitors_file))
{ 
    $views = (int)file_get_contents($visitors_file); 
}
else{
  $views = 0;
}

// Total Commission of vendors
if($this->session->userdata('isLogin')){
  $commission = $this->db->select("SUM(akin_commission) as Total_commission")->from('commission_fees')->where('transaction_type','Commission')->where('seller_id',$this->session->userdata('adminId'))->get()->result();
  //var_dump($commission);
}
else{
  $commission = $this->db->select("SUM(akin_commission) as Total_commission")->from('commission_fees')->where('transaction_type','Commission')->get()->result();
}

// Total payment cycle summation ofr vendor
if($this->session->userdata('isLogin')){
  $paymentscycles = $this->db->select("SUM(payment_bill_amount) as Total_payment")->from('payment_cycles')->where('payment_by',$this->session->userdata('adminId'))->get()->result();
}else{
   $paymentscycles = $this->db->select("SUM(payment_bill_amount) as Total_payment")->from('payment_cycles')->get()->result();
}

$topproducts = topfiveproductsbyrevenue();
$topfivestate = topfivestate();
//var_dump($topfivestate);
$topcategories = topcategories();


//$activeusers = $this->db->select('COUNT(*) as Active_users')->from('chat_languages')->get()->result();  
//$inactiveusers = $this->db->select('COUNT(*) as Inactive_users')->from('chat_languages')->get()->result();  
?>
<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <!-- 30-08-2018 <?php if(!empty($this->session->userdata('isLogin'))):?>
  <div class="row" style="margin: 8px;">
      <span class="text-uppercase">Welcome To Akinai Seller Center <br> To Get Started, Click Here For Our User Guide</span>
      <a href="https://docs.google.com/presentation/d/1ELTUZsgDo-MJEiMphpFa9NX14Kr9gPfg9M910K4GjOM" target="_blank" class="bt btn-sm btn-primary">User Guide</a>
  </div>
  <hr> 
<?php endif;?> -->
  <div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
      <div class="count"><?php  echo $totalusers[0]->Total_users;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> New Users</span>
      <div class="count"><?php echo $new_users[0]->new_users; ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Commission</span>
      <div class="count"><?php if(empty($commission[0]->Total_commission)){ echo "0";}else{
        echo ($commission[0]->Total_commission); }  ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Payment Cycle Total</span>
      <div class="count"><?php if(empty($paymentscycles[0]->Total_payment) || is_null($paymentscycles[0]->Total_payment)){echo "0";}else{
        echo ($paymentscycles[0]->Total_payment); }  ?></div>
    </div>
   <!--  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Orders Placed</span>
      <div class="count"><?php if(empty($confirmedplacedorders)){ echo "0"; }else{echo number_format($confirmedplacedorders[0]->total); } ?></div>
    </div> -->
    <!-- <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Active Users</span>
      <div class="count"><?php echo $activeusers[0]->New_users;?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Inactive Users</span>
      <div class="count green"><?php  if(empty($inactive_users[0]->inactive_user)){ echo "0";}else{ echo $inactive_users[0]->inactive_user; } ?></div>
    </div> -->
  </div>
  <!-- /top tiles -->

  <div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Revenue</span>
      <div class="count"><?php 
      if ($sellerinfo->country == "thailand") {
        echo "฿";
      }
      elseif ($sellerinfo->country == "nigeria") {
        echo "₦";
      }
      echo number_format($revenue[0]->amount); ?></div>
      <!-- <span class="count_bottom"><i class="green">4% </i>  From last Week </span> -->
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Orders Placed</span>
      <div class="count"><?php if(empty($confirmedplacedorders)){ echo "0"; }else{echo number_format($confirmedplacedorders[0]->total); } ?></div>
      <!-- <span class="count_bottom"> <i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week </span> -->
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Orders Delivered</span>
      <div class="count"><?php if(empty($confirmed)){ echo "0"; }else{echo number_format($confirmed[0]->total); } ?></div>
      <!-- <span class="count_bottom"> <i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week </span> -->
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Coversation Rate</span>
      <div class="count"><?php if(empty($userorder)){ echo "0";}else{
        echo (number_format($userorder[0]->total / $views,2)); }  ?></div>
      <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span> -->
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Avg Cart Size</span>
      <div class="count green"><?php echo (is_nan($basket_size)) ? "0" : number_format($basket_size,1); ?></div>
      <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Cancellations</span>
      <div class="count green"><?php if(empty($order_cancel)){ echo "0";}else{ echo number_format($order_cancel[0]->cancellations_rate,2); } ?>%</div>
      <!-- <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span> -->
    </div>
  </div>


  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Sales Revenue <!-- <small>Graph title sub-title</small> --></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div id="graph_line" class="demo-placeholder"></div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">  
    <div class="col-md-4 col-sm-4 col-xs-12 bg-white">
    <div class="x_title">
      <h2>Top 5 Products by Revenue</h2>
      <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-6">
    <?php 
    $percent = 100;
    if(!empty($topproducts)){
    foreach ($topproducts as $rows) {
     ?>
        <div>
          <p><?php echo $rows->product_name; ?><strong style="float: right;
    margin-right: 20px;">
    <?php if ($sellerinfo->country == "thailand") {
        echo "฿";
      }
      elseif ($sellerinfo->country == "nigeria") {
        echo "₦";
      }
      ?><?php echo number_format($rows->total_revenue);?></strong></p>
      <hr>
          
      </div>
      <?php } }else{ ?>
        <p>Product Not Found</p>
        <?php } ?>
     
    </div>
    
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 bg-white">
    <div class="x_title">
      <h2>Top 5 Shipping States by Revenue</h2>
      <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-6">
    <?php 

    if(!empty($topfivestate)){ 
      foreach ($topfivestate as $rows) {      
        if(!empty($rows->state)){
      ?>

      <div>
        <p><?php echo $rows->state; ?> <strong style="float: right;
    margin-right: 20px;">
    <?php if ($sellerinfo->country == "thailand") {
        echo "฿";
      }
      elseif ($sellerinfo->country == "nigeria") {
        echo "₦";
      }
      ?><?php echo number_format($rows->revenue);?></strong></p>
        <hr>
        
      </div>
      <?php } } }else{ ?>
      <p>State Not Found</p>
      <?php } ?>      
    </div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-12 bg-white">
    <div class="x_title">
      <h2>Revenue Split per Category</h2>
      <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-6">
    <?php
    /*if(!empty($this->session->userdata('isLogin'))){ //die("s");
      $totalcatrevenue = $this->db->select('SUM(order_amount) as totalorders')->from('orders')->where('created_by',$this->session->userdata('adminId'))->get()->row();
    }
    else{
      $totalcatrevenue = $this->db->select('SUM(order_amount) as totalorders')->from('orders')->get()->row();
    }
    echo '<pre>';
    var_dump($totalcatrevenue);*/
    //echo $totalorders
    if(!empty($topcategories)){ 
            foreach ($topcategories as $rows) {
              //var_dump($rows);
             //$per = ($rows->revenue / $totalcatrevenue->totalorders) * 100;
            ?>
           
            <div>
              <p><?php echo $rows->category_name; ?> <strong style="float: right;
    margin-right: 20px;">
      <?php if ($sellerinfo->country == "thailand") {
        echo "฿";
      }
      elseif ($sellerinfo->country == "nigeria") {
        echo "₦";
      }
      ?><?php echo number_format($rows->revenue);?></strong></p>
              <hr>
            </div>
            <?php } }else{ ?>
            <p>Category Not Found</p>
            <?php } ?>     
    </div>
  </div>
  </div>
</div>
<!-- /page content -->
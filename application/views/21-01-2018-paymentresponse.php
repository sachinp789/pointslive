<?php
$this->load->model('Address_model'); 
$this->load->model('Orders_model'); 
lang_switcher($language); 
$chatoptionsdata = chatoptions(); 
if($language == 'english'){
  $welcome = $chatoptionsdata[0]->welcome_msg;
}
else{
  $welcome = $chatoptionsdata[0]->welcome_msg_thai;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      p.responsebot{
        display: none;
      }
    </style>
  </head>

  <body class="login">
  <script>
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'Messenger'));
    </script>
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <div class="sas login_wrapper">
          <div class="x_content">
            <div class="row">
              <h2><?php echo $title ?></h2>
              <div class="separator">
                <div class="clearfix"></div>
              </div>
              <div class="col-sm-12">
                <?php if(!empty($this->session->flashdata('flashmsg1'))): ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                   <!--  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button> -->
                    <?php echo $this->session->flashdata('flashmsg1');
                     //echo $this->lang->line('paysuccess');  
                    ?>
                  </div>
                <?php endif; ?> 
                <div class="clearfix"></div>
                <?php if($status == 000) : ?>
                <p class="text-muted well well-sm no-shadow">
                  <?php echo $this->lang->line('orderrec') ?><br><?php echo $this->lang->line('orderis') ?>: <?php echo $orders;?>
                </p>
                <p class="responsebot">
                <?php 
                // get message on chatbot messenger
                $text = $this->lang->line('paymentdone');
                $response = [
                        'recipient' => [ 'id' => $userid ],
                        'message' => [ 'text' => $text]
                ];

                $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                $content = curl_exec($ch);
                curl_close($ch);

                $findstring = "";
                $findstring = str_replace("<pagename>","Points live",$this->lang->line('cashthanks'));
                $findstring = str_replace("<ordernumber>",$orders,$findstring);

                $message_to_reply = $findstring;

                $response1 = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => [ 'text' => $message_to_reply]
                ];

                $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response1));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                $content = curl_exec($ch);
                curl_close($ch);

                //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                $message_to_reply = $this->lang->line('keepstatus');

                $response2 = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => [ 'text' => $message_to_reply]
                ];

                $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response2));
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                $content = curl_exec($ch);
                curl_close($ch);

                //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                // Create sales orders
                $shipaddress = $this->Address_model->get($addressId);
                $shiptype = "";
                    if($shipaddress->delivery_method == 'next_day'){
                      $shiptype = "NEXT_DAY";
                    }
                    if($shipaddress->delivery_method == 'express' || $shipaddress->delivery_method == 'ด่วน'){
                      $shiptype = "EXPRESS_1_2_DAYS";
                    }
                    if($shipaddress->delivery_method == 'standard' || $shipaddress->delivery_method == 'มาตรฐาน'){
                      $shiptype = "STANDARD_2_4_DAYS";
                    }

                    $provinces = getStateByDistrict($shipaddress->district); // States shipping address

                    if($language == 'english'){
                      $state = $provinces[0]->province;
                    }else{
                      $state = $provinces[0]->province_thai;
                    }
                
                    $userinfo = $this->CI->getUserInfo("{$pageID}","{$userid}");

                    $fullname = $userinfo->first_name.' '.$userinfo->last_name;
                    
                    $businessaddress = $this->CI->getPageLocationHours("{$pageID}");
                 
                    $validatetoken = $this->CI->validateAPI(); // Get Access Token of Logistic API

                    if(!is_null($validatetoken)){
                      $pagetoken = $validatetoken['token']['token_id'];
                    }
       
                    $addressee = "";
                    $address1 = "";
                    $province = "";
                    $postalCode = "";
                    $country = "";
                    $phone = "";
                    $email = "";

                    if(isset($businessaddress->name)){
                      $addressee = $businessaddress->name;
                    }
                    if(isset($businessaddress->single_line_address)){
                      $address1 = $businessaddress->single_line_address;
                      $explodeaddr = explode(",", $businessaddress->single_line_address);
                      $provinces = explode(" ", $explodeaddr[2]);
                      $provincename = $provinces[1];
                      $postalCode = $provinces[2];
                    }
                    
                    if(isset($businessaddress->phone)){
                      $phone = $businessaddress->phone;
                    }
                    if(isset($businessaddress->emails)){
                      $email = $businessaddress->emails[0];
                    }

                    $salesorders = orderlists($orders);
                    
                    $orderItems = array();

                    foreach ($salesorders[0]->products as $key => $value) {
                        if($value->sale_price > 0){
                          $price = $value->sale_price;
                        }else{
                          $price = $value->product_price;
                        }
                        if($language == 'english'){
                            $pname = $value->product_name;
                        }else{
                            $pname = $value->product_name_thai;
                        }
                       $orderItems[] = array(
                        "partnerId"=> "1163",
                        "itemId"=> "{$value->product_sku}",
                        "qty"=> (int)$value->ordered_qty,
                        "subTotal"=> (int)$price 
                        );

                        $shiporderItems[] = array(
                          "itemDescription" => "{$pname}",
                          "itemQuantity"    => (int)$value->ordered_qty,
                          );
                    }
      
                    $ordersDetails = [
                      "customerInfo" => [
                          "addressee"=>"{$addressee}", // {$addressee}
                          "address1"=>"{$address1}",
                          "province"=>"{$provincename}",
                          "postalCode"=>"{$postalCode}",
                          "country"=>"{$provincename}",
                          "phone"=> "{$phone}",
                          "email"=>"{$email}"
                      ],
                      "orderShipmentInfo" =>[
                          "addressee"=>"{$fullname}",
                          "address1"=>"{$shipaddress->shipping_address}",
                          "address2"=>"",
                          "subDistrict"=>"",
                          "district"=>"{$shipaddress->district}",
                          "city"=>"",
                          "province"=>"{$state}",
                          "postalCode"=>"{$shipaddress->postcode}",
                          "country"=>"{$shipaddress->country}",
                          "phone"=>"{$shipaddress->phone}",
                          "email"=>"{$shipaddress->email}"
                      ],
                      "paymentType"=>"CCOD",
                      "shippingType"=>"{$shiptype}",
                      "grossTotal"=>(int)$salesorders[0]->order_amount,
                      "currUnit"=>"THB",
                      "orderItems"=>$orderItems
                    ]; 
                    //echo '<pre>';
                   // print_r($ordersDetails);
                    $salesorder = createSalesOrder("https://fulfillment.api.acommercedev.com/channel/demoth1/order/{$orders}",$pagetoken,$ordersDetails); 
                    //echo '<br>';
                   // print_r($salesorder);
                     if(isset($salesorder['http_code']) && $salesorder['http_code'] == 201){
                      $findorder = str_replace("<orderno>", "{$orders}", $this->lang->line('is_process'));
                      $message_to_reply = $findorder;

                      $response = [
                          'recipient' => [ 'id' => $userid ],
                          'message' => [ 'text' => $message_to_reply]
                      ];

                      $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                      $content = curl_exec($ch);
                      curl_close($ch);

                      // shipping order generation
                      $shipordersDetails = [
                        "shipServiceType" => "DELIVERY",
                        "shipSender" => [
                            "addressee"=>"{$addressee}",
                            "address1"=>"{$address1}",
                            "city"=>"",
                            "province"=>"{$provincename}",
                            "postalCode"=>"{$postalCode}",
                            "country"=> "{$provincename}",
                            "phone"=> "{$phone}",
                            "email"=>"{$email}"
                        ],
                        "shipShipment" =>[
                            "addressee"=>"{$fullname}",
                            "address1"=>"{$shipaddress->shipping_address}",
                            "address2"=>"",
                            "subDistrict"=>"",
                            "district"=>"{$shipaddress->district}",
                            "city"=>"",
                            "province"=>"{$state}",
                            "postalCode"=>"{$shipaddress->postcode}",
                            "country"=>"{$shipaddress->country}",
                            "phone"=>"{$shipaddress->phone}",
                            "email"=>"{$shipaddress->email}"
                        ],
                        "shipPickup" =>[
                          "addressee"=>"{$addressee}",
                          "address1"=>"{$address1}",
                          "city"=>"",
                          "province"=>"{$provincename}",
                          "postalCode"=>"{$postalCode}",
                          "country"=> "{$provincename}",
                          "phone"=> "{$phone}",
                          "email"=>"{$email}"
                        ],
                        "shipShippingType"=>"{$shiptype}",
                        "shipPaymentType"=>"COD",
                        "shipCurrency"=>"THB",
                        "shipGrossTotal"=>(int)$salesorders[0]->order_amount,
                        "shipInsurance"=>false,
                        "shipPickingList"=>$shiporderItems,
                        "shipPackages"  => []
                      ];

                      $shippingorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$orders}",$pagetoken,$shipordersDetails);
                      
                      if(isset($shippingorder['http_code']) && $shippingorder['http_code'] == 201){
                         $this->Orders_model->where('transaction_id',$orders)->update(array('status' => '1','shipment_date' => date('Y-m-d h:i:s')));
                      }
                      //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');
                    }else{
                      $message_to_reply = "422 code. Validation failed.";
                      $response = [
                          'recipient' => [ 'id' => $userid ],
                          'message' => [ 'text' => $message_to_reply]
                      ];

                      $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                      $content = curl_exec($ch);
                      curl_close($ch);

                      //$this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');
                    }

                // get message on chatbot messenger for start loop
                $answer = ["attachment"=>[
                      "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('mostpopular'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('discover')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                  $responses = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => $answer 
                  ];  

                  $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($responses));
                  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                  $content = curl_exec($ch);
                  curl_close($ch);
                  endif; 
                ?>  
                </p>
             </div>
            </div>
          </div>
      </div>
    </div>
    <script>
       window.extAsyncInit = function() {
         setTimeout(function(){
          MessengerExtensions.requestCloseBrowser(function success() { // close dialogbox
              //alert("ok");
          }, function error(err) {
             // alert(err);
          });
      
        }, 1000);
      }
      </script>
     <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/build/js/custom.min.js"></script>
  </body>
</html>

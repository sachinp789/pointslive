<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $page_title ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>assets/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="sas login_wrapper">
          <div class="x_content">
          <section class="login_content">
            <form class="form-horizontal" method="post" action="<?php echo base_url().'login/forgotPassword'?>" novalidate>
              <h1><?php echo $title; ?></h1>
              <?php if(!empty($this->session->flashdata('flashmsg'))): ?>
                 <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                      <?php echo $this->session->flashdata('flashmsg'); ?>
                    </div>
              <?php endif; ?>
              <div class="item form-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required" />
                <?php echo form_error('email','<span class="help-block">','</span>'); ?>
              </div>
              <div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" class="btn btn-success" name="forgotpost" id="forgotpost" value="Send">
                    <a class="btn btn-xs" href="<?php echo base_url().'login'?>">Back</a>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div class="clearfix"></div>
              </div>
            </form>
          </section>
          </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="<?php echo base_url(); ?>assets/vendors/validator/validator.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>

  </body>
</html>

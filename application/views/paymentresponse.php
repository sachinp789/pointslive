<?php
$this->load->model('Address_model'); 
$this->load->model('Orders_model'); 
lang_switcher($language); 
$chatoptionsdata = chatoptions(); 
if($language == 'english'){
  $welcome = $chatoptionsdata[0]->welcome_msg;
  $productname = $product[0]->product_name;
}
else{
  $welcome = $chatoptionsdata[0]->welcome_msg_thai;
  $productname = $product[0]->product_name_thai;
}
//$products = array();
$productss = array();
$subtotal = 0;
$shiptotal = 0;
$price = 0;

foreach ($products as $key => $value) {

  if($this->language == 'english'){
    $productss[]=$value->product_name;
  }
  else{
    $productss[]=$value->product_name_thai;
  }

  if($value->sale_price > 0){
    $price = $value->sale_price;
  }else{
    $price = $value->product_price;
  }

  $subtotal = $subtotal + ($value->qty * $price);
  $shiptotal = $shiptotal + $value->shipping_cost;
}

/*foreach ($product as $key => $value) {
  if($this->language == 'english'){
    $products[]=$value->product_name;
  }
  else{
    $products[]=$value->product_name_thai;
  }
}*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      p.responsebot{
        display: none;
      }
    </style>
  </head>

  <body class="login">
  <script>
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'Messenger'));
    </script>
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <div class="sas login_wrapper">
          <div class="x_content">
            <div class="row">
              <h2><?php echo $title ?></h2>
              <div class="separator">
                <div class="clearfix"></div>
              </div>
              <div class="col-sm-12">
                <?php if(!empty($this->session->flashdata('flashmsg1'))): ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('msg')?> alert-dismissible fade in" role="alert">
                   <!--  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button> -->
                    <?php echo $this->session->flashdata('flashmsg1');
                     //echo $this->lang->line('paysuccess');  
                    ?>
                  </div>
                <?php endif; ?> 
                <div class="clearfix"></div>
                <?php if($status == 000) : ?>
                <p class="text-muted well well-sm no-shadow">
                  <?php echo $this->lang->line('orderrec') ?><br><?php echo $this->lang->line('orderis') ?>: <?php echo $orders;?>
                </p>
                <p class="responsebot">
                <?php 
                  $discounts = $this->db->select('total_discount')->from('coupon_redemptions')->where('order_id',$orders)->where('user_id',$userid)->where('page_id',$pageID)->get()->row();
                  // get message on chatbot messenger
                  $text = $this->lang->line('paymentdone');
                  $response = [
                          'recipient' => [ 'id' => $userid ],
                          'message' => [ 'text' => $text]
                  ];

                  $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                  $content = curl_exec($ch);
                  curl_close($ch);

                  $findstring = "";
                  $findstring = str_replace("<pagename>","Points live",$this->lang->line('cashthanks'));
                  $findstring = str_replace("<ordernumber>",$orders,$findstring);

                  $message_to_reply = $findstring;

                  $response1 = [
                        'recipient' => [ 'id' => $userid ],
                        'message' => [ 'text' => $message_to_reply]
                  ];

                  $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response1));
                  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                  $content = curl_exec($ch);
                  curl_close($ch);

                  $message_to_reply = str_replace("<orderno>",$orders,$this->lang->line('is_process'));

                  $response = [
                        'recipient' => [ 'id' => $userid ],
                        'message' => [ 'text' => $message_to_reply]
                  ];
                  
                  $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                  /*// Etickets Send

                  $QRcode = $this->CI->generateQrcodeofOrder($orders);

                  $pdf = $this->CI->generateeTicket($orders,$QRcode['qrcode'],$products); 

                  if($pdf){

                    $response = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => [ 'text' => $this->lang->line('digital')]
                    ];

                    $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                    $path = base_url()."uploads/invoices/".$pdf;//base_url()."uploads/invoices".$pdf;
                    $sendFIle = $this->CI->sendEticketToUser($pageID,$userid,$path,$token); // Send PDF
                  }*/

                  // Sales order flow remove from points live page :- 27-02-2018
                  if($pageID != "1048681488539732"){
                    $message_to_reply = $this->lang->line('keepstatus');
                    $response2 = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => [ 'text' => $message_to_reply]
                    ];

                    $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response2));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                    $content = curl_exec($ch);
                    var_dump($content);
                    curl_close($ch);
                    // Create sales orders
                    $shipaddress = $this->Address_model->get($addressId);
                    $shiptype = "";

                    if($this->session->userdata('deliverymethod') == 'next_day')
                    {
                      $shiptype = "NEXT_DAY";
                    }
                    else if($this->session->userdata('deliverymethod') == 'express' || $this->session->userdata('deliverymethod') == 'ด่วน')
                    {
                      $shiptype = "EXPRESS_1_2_DAYS";
                    }
                    else if($this->session->userdata('deliverymethod') == 'standard' || $this->session->userdata('deliverymethod') == 'มาตรฐาน')
                    {
                      $shiptype = "STANDARD_2_4_DAYS";
                    }
                     
                    $provinces = getStateByDistrict($shipaddress->district); // States shipping address

                    if($language == 'english'){
                      $state = $provinces[0]->province;
                    }else{
                      $state = $provinces[0]->province_thai;
                    }
              
                    $businessaddress = $this->CI->getPageLocationHours("{$pageID}",$token);

                    $validatetoken = $this->CI->validateAPI(); // Get Access Token of Logistic API

                    if(!is_null($validatetoken)){
                      $pagetoken = $validatetoken['token']['token_id'];
                    }
       
                    $addressee = "";
                    $address1 = "";
                    $province = "";
                    $postalCode = "";
                    $country = "";
                    $phone = "";
                    $email = "";

                    if(isset($businessaddress->name)){
                      $addressee = $businessaddress->name;
                    }
                    if(isset($businessaddress->single_line_address)){
                      $address1 = $businessaddress->single_line_address;
                      $explodeaddr = explode(",", $businessaddress->single_line_address);
                      $provinces = explode(" ", $explodeaddr[2]);
                      $provincename = $provinces[1];
                      $postalCode = $provinces[2];
                    }
                    else{
                      $address1 = "303, 3rd Floor, Sigma 2, Ahmedabad";
                      $provincename = "Gujarat";
                      $postalCode = "38005";
                    }
                      
                    if(isset($businessaddress->phone)){
                      $phone = $businessaddress->phone;
                    }
                    else{
                      $phone = "0123456789";
                    }
                    if(isset($businessaddress->emails)){
                      $email = $businessaddress->emails[0];
                    }
                    else{
                      $email = "navil.shah@searchnative.in";
                    }

                    $salesorders = orderlists($orders);
                    
                    $orderItems = array();

                    foreach ($salesorders[0]->products as $key => $value) {
                      if($value->sale_price > 0){
                        $price = $value->sale_price;
                      }else{
                        $price = $value->product_price;
                      }
                      if($language == 'english'){
                          $pname = $value->product_name;
                      }else{
                          $pname = $value->product_name_thai;
                      }
                     $orderItems[] = array(
                      "partnerId"=> "1163",
                      "itemId"=> "{$value->product_sku}",
                      "qty"=> (int)$value->ordered_qty,
                      "subTotal"=> (int)$price 
                      );

                      $shiporderItems[] = array(
                        "itemDescription" => "{$pname}",
                        "itemQuantity"    => (int)$value->ordered_qty,
                        );
                    }
        
                    $ordersDetails = [
                      "customerInfo" => [
                          "addressee"=>"{$addressee}", // {$addressee}
                          "address1"=>"{$address1}",
                          "province"=>"{$provincename}",
                          "postalCode"=>"{$postalCode}",
                          "country"=>"{$provincename}",
                          "phone"=> "{$phone}",
                          "email"=>"{$email}"
                      ],
                      "orderShipmentInfo" =>[
                          "addressee"=>"{$shipaddress->receiver_name}",
                          "address1"=>"{$shipaddress->shipping_address}",
                          "address2"=>"",
                          "subDistrict"=>"",
                          "district"=>"{$shipaddress->district}",
                          "city"=>"",
                          "province"=>"{$state}",
                          "postalCode"=>"{$shipaddress->postcode}",
                          "country"=>"{$shipaddress->country}",
                          "phone"=>"{$shipaddress->phone}",
                          "email"=>"{$shipaddress->email}"
                      ],
                      "paymentType"=>"CCOD",
                      "shippingType"=>"{$shiptype}",
                      "grossTotal"=>(int)$salesorders[0]->order_amount,
                      "currUnit"=>"THB",
                      "orderItems"=>$orderItems
                    ]; 
                    
                    $salesorder = createSalesOrder("https://fulfillment.api.acommercedev.com/channel/demoth1/order/{$orders}",$pagetoken,$ordersDetails); 

                    if(isset($salesorder['http_code']) && $salesorder['http_code'] == 201){
                        $findorder = str_replace("<orderno>", "{$orders}", $this->lang->line('is_process'));
                        $message_to_reply = $findorder;

                        $response = [
                            'recipient' => [ 'id' => $userid ],
                            'message' => [ 'text' => $message_to_reply]
                        ];

                        $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                        $content = curl_exec($ch);
                        curl_close($ch);

                        // shipping order generation
                        $shipordersDetails = [
                          "shipServiceType" => "SMARTSHIP_DROPOFF",
                          "shipSender" => [
                              "addressee"=>"{$addressee}",
                              "address1"=>"{$address1}",
                              "city"=>"",
                              "province"=>"{$provincename}",
                              "postalCode"=>"{$postalCode}",
                              "country"=> "{$provincename}",
                              "phone"=> "{$phone}",
                              "email"=>"{$email}"
                          ],
                          "shipShipment" =>[
                              "addressee"=>"{$shipaddress->receiver_name}",
                              "address1"=>"{$shipaddress->shipping_address}",
                              "address2"=>"",
                              "subDistrict"=>"",
                              "district"=>"{$shipaddress->district}",
                              "city"=>"",
                              "province"=>"{$state}",
                              "postalCode"=>"{$shipaddress->postcode}",
                              "country"=>"{$shipaddress->country}",
                              "phone"=>"{$shipaddress->phone}",
                              "email"=>"{$shipaddress->email}"
                          ],
                          "shipShippingType"=>"{$shiptype}",
                          "shipPaymentType"=>"NON_COD",
                          "shipCurrency"=>"THB",
                          "shipGrossTotal"=>(int)$salesorders[0]->order_amount,
                          "shipInsurance"=>false,
                          "shipPickingList"=>$shiporderItems,
                          "shipPackages"  => []
                        ];

                        $shippingorder = createSalesOrder("https://shipping.api.acommercedev.com/partner/1163/order/{$orders}",$pagetoken,$shipordersDetails);
                        
                        if(isset($shippingorder['http_code']) && $shippingorder['http_code'] == 201){
                           $this->Orders_model->where('transaction_id',$orders)->update(array('status' => '0','shipment_date' => ''));
                        } 
                    }
                    else{
                      $message_to_reply = $this->lang->line('code');
                      $response = [
                          'recipient' => [ 'id' => $userid ],
                          'message' => [ 'text' => $message_to_reply]
                      ];

                      $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                      $content = curl_exec($ch);
                      curl_close($ch);
                    }
                  }
                  else{

                    // 20-06-2018

                    // Receipt Templates for Orders
                      $answer = ["attachment"=>[
                          "type"=>"template",
                          "payload"=>[
                          "template_type"=>"receipt",
                          "recipient_name"=>$address->receiver_name,
                          "order_number"=>$orders,
                          "currency"=>"NGN",
                          "payment_method"=>"Prepaid",
                          "timestamp"=>strtotime($dateoforder),        
                          "address" => [
                            "street_1"=>$address->shipping_address,
                            "street_2"=>"",
                            "city"=>$address->local_government,//$getAddress->state
                            "postal_code"=>$address->postcode,
                            "state"=>$address->state,//Thailand
                            "country"=>$address->country
                          ],
                          "summary"=>[
                            "subtotal"=>$subtotal,
                            "shipping_cost"=>$shiptotal,
                            "total_cost"=>$total
                          ],
                          "adjustments" => [
                            [
                              "name" => "Discount",
                              "amount"  => ($discounts) ? $discounts->total_discount : "0" 
                            ]
                          ],
                          "elements"=>$productdata
                        ]
                      ]
                    ];

                    $response = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => $answer
                    ];

                    $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                    // 03-05-2018
                    //if($product[0]->shipping_type != "delivery"){
                         // QR code generate of order number
                        $QRcode = $this->CI->generateQrcodeofOrder($orders);

                        $pdf = $this->CI->generateeTicket($orders,$QRcode['qrcode'],$product); 
                        
                        if($pdf){

                          $response = [
                            'recipient' => [ 'id' => $userid ],
                            'message' => [ 'text' => $this->lang->line('digital')]
                          ];

                          $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                          $path = base_url()."uploads/invoices/".$pdf;//base_url()."uploads/invoices".$pdf;
                          $sendFIle = $this->CI->sendEticketToUser($pageID,$userid,$path,$token); // Send PDF
                        }
                    //}
                  }   
                  $this->cache->delete("delivery_postcode_{$userid}");
                  $this->cache->delete("validdistrict_{$userid}");
                  $this->cache->delete("validpostcode_{$userid}");
                  $this->cache->delete("valid_getaddress_{$userid}");
                  $this->cache->delete("delivery_method_{$userid}"); 
                  //$this->cache->delete("delivery_getcountry_{$userid}"); 
                  $this->cache->delete("delivery_getphone_{$userid}"); 
                  $this->cache->delete("delivery_getemail_{$userid}");
                  $this->cache->delete("deliver_address_{$userid}");
                  $this->cache->delete("discountamount_{$userid}");   
                  $this->cache->delete("lastcouponID_{$userid}");
                  $this->cache->delete("pay_method_{$userid}");
                  $this->cache->delete("pay_method_{$userid}");
                  $this->cache->delete("shipername_{$userid}");
                  $this->cache->delete("delivery_pickup_{$userid}");
                  $this->cache->delete("valid_nigeria_{$userid}");
                  $this->cache->delete("valid_nigeriaarea_{$userid}");
                  
                  // Session destroy of address and coupon ID
                  $this->session->unset_userdata("shipping_address_{$userid}");
                  $this->session->unset_userdata("redeemID_{$userid}");
                  $this->session->unset_userdata('deliverymethod');
                  // get message on chatbot messenger for start loop
                  $answer = ["attachment"=>[
                        "type"=>"template",
                      "payload"=>[
                        "template_type"=>"button",
                        "text"=>$welcome ? $welcome : 'Hey, How can i help you ?',
                        "buttons"=>[
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('mostpopular'), //What's Hot
                          ],
                          [
                            "type"=>"postback",
                            "payload"=>"DEVELOPER_DEFINED_PAYLOAD",
                            "title" => $this->lang->line('discover')
                          ],
                          [
                            "type"=>"postback",
                            "title"=>$this->lang->line('viewmore'),
                            "payload"=>"PAY_LOAD_1"
                          ]
                        ]
                      ]
                    ]];

                  $responses = [
                      'recipient' => [ 'id' => $userid ],
                      'message' => $answer 
                  ];  

                  $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($responses));
                  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                  $content = curl_exec($ch);
                  curl_close($ch);
                  endif; 
                ?>  
              </p>
             </div>
            </div>
          </div>
      </div>
    </div>
    <script>
       window.extAsyncInit = function() {
         setTimeout(function(){
          MessengerExtensions.requestCloseBrowser(function success() { // close dialogbox
              //alert("ok");
          }, function error(err) {
             // alert(err);
          });
      
        }, 1000);
      }
      </script>
     <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/build/js/custom.min.js"></script>
  </body>
</html>

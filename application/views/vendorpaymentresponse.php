<?php
//$this->load->model('Address_model'); 
lang_switcher($language); 
$chatoptionsdata = chatoptions(); 
$products = array();
$subtotal = 0;
$shiptotal = 0;
$price = 0;
foreach ($product as $key => $value) {
  if($this->language == 'english'){
    $products[]=$value->product_name;
  }
  else{
    $products[]=$value->product_name_thai;
  }
  if($value->sale_price > 0){
    $price = $value->sale_price;
  }else{
    $price = $value->product_price;
  }

  $subtotal = $subtotal + ($value->qty * $price);
  $shiptotal = $shiptotal + $value->shipping_cost;
}

if($language == 'english'){
  $welcome = $chatoptionsdata[0]->welcome_msg;
}
else{
  $welcome = $chatoptionsdata[0]->welcome_msg_thai;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <script>
        (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'Messenger'));
    </script>
    <div class="sas login_wrapper">
          <div class="x_content">
            <div class="row">
              <h2><?php echo $title ?></h2>
              <div class="separator">
                <div class="clearfix"></div>
              </div>
              <div class="col-sm-12">
                <div class="clearfix"></div>
                <?php if($status == "successful"):?>
                    <p class="text-muted well well-sm no-shadow">
                     <?php echo $this->lang->line('orderrec') ?><br><?php echo $this->lang->line('orderis') ?>: <?php echo $orders;?>
                    </p>
                    <p class="responsebot">
                      <?php 
                       $discounts = $this->db->select('total_discount')->from('coupon_redemptions')->where('order_id',$orders)->get()->row();
                      //get message on chatbot messenger
                      $text = $this->lang->line('paymentdone');
                      $response = [
                              'recipient' => [ 'id' => $userID ],
                              'message' => [ 'text' => $text]
                      ];

                      $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                      $content = curl_exec($ch);
                      curl_close($ch);

                      $findstring = "";
                      $findstring = str_replace("<pagename>",$pagename,$this->lang->line('cashthanks'));
                      $findstring = str_replace("<ordernumber>",$orders,$findstring);

                      $message_to_reply = $findstring;

                      $response1 = [
                            'recipient' => [ 'id' => $userID ],
                            'message' => [ 'text' => $message_to_reply]
                      ];

                      $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response1));
                      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                      $content = curl_exec($ch);
                      curl_close($ch);

                      $message_to_reply = str_replace("<orderno>",$orders,$this->lang->line('is_process'));

                      $response = [
                            'recipient' => [ 'id' => $userID ],
                            'message' => [ 'text' => $message_to_reply]
                      ];
                      
                      $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                        // Receipt Templates for Orders
                        $answer = ["attachment"=>[
                            "type"=>"template",
                            "payload"=>[
                            "template_type"=>"receipt",
                            "recipient_name"=>$address->receiver_name,
                            "order_number"=>$orders,
                            "currency"=>"NGN",
                            "payment_method"=>"Prepaid",
                            "timestamp"=>strtotime($dateoforder),        
                            "address" => [
                              "street_1"=>$address->shipping_address,
                              "street_2"=>"",
                              "city"=>$address->district,
                              "postal_code"=>$address->postcode,
                              "state"=>$address->state,
                              "country"=>$address->country
                            ],
                            "summary"=>[
                              "subtotal"=>$subtotal,
                              "shipping_cost"=>$shiptotal,
                              "total_cost"=>$total
                            ],
                            "adjustments" => [
                              [
                                "name" => "Discount",
                                "amount"  => ($discounts) ? $discounts->total_discount : "0" 
                              ]
                            ],
                            "elements"=>$productdata
                          ]
                        ]
                      ];

                      $response = [
                        'recipient' => [ 'id' => $userID ],
                        'message' => $answer
                      ];

                      $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                      sleep(2);
                      // Etickets Send

                      $QRcode = $this->CI->generateQrcodeofOrder($orders);

                      $pdf = $this->CI->generateeTicket($orders,$QRcode['qrcode'],$products); 

                      if($pdf){

                        $response = [
                          'recipient' => [ 'id' => $userID ],
                          'message' => [ 'text' => $this->lang->line('digital')]
                        ];

                        $this->CI->curl("https://graph.facebook.com/v2.6/me/messages?access_token=".$token,$response,'POST');

                        $path = base_url()."uploads/invoices/".$pdf;//base_url()."uploads/invoices".$pdf;
                        $sendFIle = $this->CI->sendEticketToUser($pageID,$userID,$path,$token); // Send PDF
                      }

                      ?>
                    </p>
                    <p class="responsebot">
                    <?php elseif($status == "failed"):?>
                       <p class="text-muted well well-sm no-shadow">
                        <?php echo $this->lang->line('payfailed') ?><br><?php echo $this->lang->line('orderis') ?>: <?php echo $orders;?>
                       </p>
                       <?php
                          $discounts = $this->db->select('total_discount')->from('coupon_redemptions')->where('order_id',$orders)->where('user_id',$userID)->where('page_id',$pageID)->get()->row();

                          if($discounts){
                            // Remove discount from database 
                            $this->db->where('order_id', $orders);
                            $this->db->where('user_id', $userID);
                            $this->db->where('page_id', $pageID);
                            $this->db->delete('coupon_redemptions');
                          }

                          $text = $this->lang->line('payfailed');
                          $response = [
                                  'recipient' => [ 'id' => $userID ],
                                  'message' => [ 'text' => $text]
                          ];

                          $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                          curl_setopt($ch, CURLOPT_POST, 1);
                          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
                          curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                          $content = curl_exec($ch);
                          curl_close($ch);

                          $findstring = "";
                          $findstring = str_replace("<pagename>",$pagename,$this->lang->line('cashthanks'));
                          $findstring = str_replace("<ordernumber>",$orders,$findstring);

                          $message_to_reply = $findstring;

                          $response1 = [
                                'recipient' => [ 'id' => $userID ],
                                'message' => [ 'text' => $message_to_reply]
                          ];

                          $ch = curl_init("https://graph.facebook.com/v2.6/me/messages?access_token=".$token);
                          curl_setopt($ch, CURLOPT_POST, 1);
                          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response1));
                          curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                          $content = curl_exec($ch);
                          curl_close($ch);
                      ?>
                    </p>
                    <p class="responsebot">
                <?php else:?>
                        <p class="text-muted well well-sm no-shadow">
                         <?php echo $this->lang->line('paycancel') ?>
                        </p>
                    </p>
                <?php endif;?>  
             </div>
            </div>
          </div>
      </div>
    <script>
       window.extAsyncInit = function() {
         setTimeout(function(){
          MessengerExtensions.requestCloseBrowser(function success() { // close dialogbox
          }, function error(err) {
          });
      
        }, 1000);
      }
      </script>
     <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/build/js/custom.min.js"></script>
  </body>
</html>

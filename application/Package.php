<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends MY_Controller {
	public $pageID="";
	public function __construct(){
		parent::__construct();
	    $this->load->library('form_validation');
	    $this->load->model('Packages_model');
	    $this->pageID = $this->session->userdata('pagemanager');
	}

	// show store page
	public function addPackage(){
	    $data['content'] = 'admin/packages/addpackage';
	    $data['title'] = 'Add Package';
	    $this->load->view('admin/template', $data);
	}

	// show store page
	public function savePackage(){
	    $data = array();
        if($this->input->post()){

        	$this->form_validation->set_rules('package_country', 'Package Country', 'required');
        	$this->form_validation->set_rules('package_country_thai', 'Package Country Thai', 'required');
        	$this->form_validation->set_rules('package_small_weight', 'Small Package', 'required');
        	$this->form_validation->set_rules('package_medium_weight', 'Medium Package', 'required');
        	$this->form_validation->set_rules('package_large_weight', 'Large Package', 'required');
          
            if ($this->form_validation->run() == true) {

            	$packagedata = array(
            			'package_country' 	   => $this->input->post('package_country'),
            			'package_country_thai' => $this->input->post('package_country_thai'),
            			'package_small_weight' => $this->input->post('package_small_weight'),
            			'package_medium_weight'=> $this->input->post('package_medium_weight'),
            			'package_large_weight' => $this->input->post('package_large_weight'),
            			'created_date' 		   => date('Y-m-d')
            		);
     				
                $success = $this->Packages_model->insert($packagedata);

                if($success){
                    $this->session->set_flashdata('flashmsg', 'Shipping Package has been added.');
                    $this->session->set_flashdata('msg', 'success');
                    redirect(base_url().'package/listPackages');
                }else{
                	$data['msg'] = 'danger';
                    $data['error_msg'] = 'Please try again...';
                }
            }
        }
        
		$data['content'] = 'admin/packages/addPackage';
	    $data['title'] = 'Add Package';
        //load the view
        $this->load->view('admin/template', $data);
	} 

	// Get all stores
	public function listPackages(){
	  $packagelists = $this->Packages_model->get_all();
	  $data['packages'] = $packagelists;
      $data['content'] = 'admin/packages/all_packages';
      $data['title'] = 'All Packages';
      $this->load->view('admin/template', $data);
	}

	// Edit store
	public function editPackage($packageID=""){
		if($packageID){
			$packagedetails = $this->Packages_model->get(array('id' => $packageID));
			if($packagedetails){
			    $data['content'] = 'admin/packages/editpackage';
			    $data['title'] = 'Edit Package';
			    $data['packagesdata'] = $packagedetails;
			    $this->load->view('admin/template', $data);
			}else{
				redirect(base_url('admin/dashboard'));
			}
		}else{	
		redirect(base_url('admin/dashboard'));
		}
	}

	// update store by id
	public function updatePackage(){
		$packagedetails = $this->Packages_model->get(array('id' => $this->input->post('package_id')));
		if($packagedetails){
			$packagedata = array(
    			'package_country' 	   => $this->input->post('package_country'),
    			'package_country_thai' => $this->input->post('package_country_thai'),
    			'package_small_weight' => $this->input->post('package_small_weight'),
    			'package_medium_weight'=> $this->input->post('package_medium_weight'),
    			'package_large_weight' => $this->input->post('package_large_weight'),
    		);

			$updated = $this->Packages_model->update($packagedata, $this->input->post('package_id'));
			if($updated){
				$this->session->set_flashdata('flashmsg', 'Shipping Package has been updated.');
	            $this->session->set_flashdata('msg', 'success');
			}else{
				$this->session->set_flashdata('flashmsg', 'No package details has been updated.');
	            $this->session->set_flashdata('msg', 'info');
			}
		}else{
			$this->session->set_flashdata('flashmsg', 'Shipping Package not found.');
            $this->session->set_flashdata('msg', 'danger');
		}
	  	redirect(base_url() . 'package/listPackages'); 
	 }
	// Store has been removed
	public function removePackage($packageID=""){
		$this->Packages_model->delete(array('id' => $packageID));
        $this->session->set_flashdata('flashmsg', 'Package has been removed.');
        $this->session->set_flashdata('msg', 'success');
        redirect(base_url() . 'package/listPackages');
	}

}
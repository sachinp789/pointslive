
var enjoyhint_instance = new EnjoyHint({});
var enjoyhint_script_steps = [
  {
  	//'next #pstep-1' : "Add name of product.",
  	'next #csteps1' : "Enter coupon code",
  	'showSkip' : false
  },
  {
  	'next #csteps2' : "Enter coupon description",
  	'showSkip' : false
  },
  {
  	'next #csteps3' : "Select coupon status from list",
  	'showSkip' : false
  },
  {
  	'next #csteps4' : "Select coupon from date",
  	'showSkip' : false
  },  
  {
	'next #csteps5' : 'Select coupon to date',
	'showSkip' : false
  },
  {
	'next #csteps6' : 'Select coupon apply by',
	'showSkip' : false
  },
  {
	'next #csteps7' : 'Enter discount value',
	'showSkip' : false
  },
  {
	'next #csteps8' : 'Enter uses per customer',
	'showSkip' : false
  },
  {
	'next #csteps9' : 'Enter order minimum amount',
	'showSkip' : false
  },
  {
	'change #sendcoupon' : 'Click here to create coupon',
	'showSkip' : true
  }

];

enjoyhint_instance.set(enjoyhint_script_steps);
enjoyhint_instance.run();
var enjoyhint_instance = new EnjoyHint({});
var enjoyhint_script_steps = [
  {
  	'next #cat_step1' : "Enter the name on the product category",  	
	 event_type: "next",	 
	'showSkip' : false

  },
 /* {
  	'next #cat_step2' : "Enter name of local category.",  	
	 event_type: "next",	 
	'showSkip' : false

  },*/
	{
  	'change #cat_step3' : "Upload an Image. The file should not be bigger than 5MB",  		 
	'showSkip' : false

  },  
  {
	 'click #new_category' : "Save Category and Now let's create a Product",
	 'showSkip' : false  
  }

];

enjoyhint_instance.set(enjoyhint_script_steps);
enjoyhint_instance.run();
var enjoyhint_instance = new EnjoyHint({});
var enjoyhint_script_steps = [
  {
  	//'next #pstep-1' : "Add name of product.",
  	'change #shipping_type' : "Select order type from list",
     onBeforeStart:function(){
      $('#shipping_type').change(function(e){
        type = $(this).val();
        if(type == 'pickup'){
          $(".scanner" ).addClass( "scan" );
          enjoyhint_instance.trigger('next');
        }
        else if(type == 'delivery'){
         $('table:first tr').find(':last-child').addClass("lasttd")
         enjoyhint_instance.trigger('next');
        }
      })
    },
    'showSkip' : true
  },
  {
	'next .lasttd' : 'Clik on below button to process delivery order',
	'showSkip' : true,
   showNext : true
  },
  {
  'next .scan' : 'Click on scanner and scan QR code then confirm order',
   showNext:false,
  'showSkip' : false,
  'shape' : 'circle',
  'nextButton' : {className: "myNext", text: "Got it!"}
  }
];

enjoyhint_instance.set(enjoyhint_script_steps);
enjoyhint_instance.run();
var enjoyhint_instance = new EnjoyHint({});
var enjoyhint_script_steps = [
  {
  'next .profile-nav' : 'Welcome to the Akinai Seller Center. Let us help you setup your shop',
   showNext:false,
  'showSkip' : false,
  'shape' : 'circle',
  'nextButton' : {className: "myNext", text: "Got it!"}
  }

];

enjoyhint_instance.set(enjoyhint_script_steps);
enjoyhint_instance.run();
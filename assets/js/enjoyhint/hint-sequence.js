var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth() + 1; //Months are zero based
if (curr_month < 10){
    curr_month = "0"+curr_month;
}
var curr_year = d.getFullYear();
var finaldate = curr_year + "-" + curr_month + "-" + curr_date;
var sellerdate = $("#sellercreateddate").val();
var country = $("#sellercountry").val();

//if(sellerdate == finaldate){
  var enjoyhint_instance = new EnjoyHint({});

  var enjoyhint_script_steps = [
    {
      'next #profile-tab' : '',
      'showSkip' : true,
       showNext : true
    },
    {
    	'next #pf1' : 'Enter your shop name',
    	'showSkip' : false,
      showNext : true
    },
    {
      'next #pf2' : 'Enter your mobile number',
      'showSkip' : false,
      showNext : true
    },
    {
      'next #pf3' : 'Choose your country',
      'showSkip' : false,
       showNext : true
    },
   /* {
      'change #pf4' : 'Upload profile photo',
      'showSkip' : false,
      showNext : true
    },*/
    {
      'click #profilepost1' : 'Save profile data',
      showNext : true ,
      'showSkip' : false,
    },
    {
    	'click #store_info' : "Now choose your Opening and Closing Time",
  	  'showSkip' : false,
       showNext : false
    },
    {
    	'click #steps3' : "Seller choose opening and closing time",
    	showNext:true,
    	onBeforeStart:function() {
        box = $("#businesshours").val()
        if(box == "alltime"){
          $("#times" ).removeClass("customtime1");
          //$('#payment-tab').addClass("payments");
          //enjoyhint_instance.trigger('next');
        }
        else if(box == 'custom'){
          $("#times" ).addClass("customtime1");
          //enjoyhint_instance.trigger('next');
        }

        $('#businesshours').change(function(e){
          type = $(this).val();
          if(type == 'alltime'){
            //$("#times" ).removeClass("customtime");
            $("#times" ).removeClass("customtime1");
            //$('#payment-tab').addClass("payments");
            //enjoyhint_instance.trigger('next');
          }
          else if(type == 'custom'){
           $("#times" ).addClass( "customtime1" );
           //enjoyhint_instance.trigger('next');
          }
        })
    	 },
    	 event_type: "next",
       showNext : true,
    	'showSkip' : false	
    },
   /* {
      'next .customtime' : 'Select Information from Checkbox',
      'showSkip' : false,
       showNext : true
    },*/
    {
      'next .customtime1' : 'Check atleast one opening and closing day',
      'showSkip' : false,
       showNext : true
    },
    {
       'click #btnconnect' : 'Save store information data',
       'showSkip' : false,
       showNext : true 
    },
    {
       'click #channel-tab' : "Now let's add your online channels",
       'showSkip' : false
    },
    {
       'click .channelstwt' : "Enter your twitter handle. If you don't have one, click next",
       'showSkip' : false,
        showNext : true
    },
    {
       'next .socialchannesl' : "Enter details or click on next",
       'showSkip' : false,
        showNext : true
    },
    {
       'next #websiteurl' : "Enter your website. If you don't have one, click next",
       'showSkip' : false,
        showNext : true
    },
    {
       'click #btnchannel' : "Save channel information",
       'showSkip' : false,
        showNext : true
    },
    {
       'click #delivery-tab' : "Now let's setup your return and delivery options",
       'showSkip' : false
    },
    {
       'next #returnofferdiv' : "Do you offer Returns ?",
        onBeforeStart:function(){
        option = $('#returnoffer').val();
        if(option == '0'){
          $("#serviceofferdiv").addClass("nextdivservice");
        }
        else{
          $("#serviceofferdiv").addClass("nextdivservice");
        }
        $('#returnoffer').change(function(e){
            option = $(this).val();
            if(option == '1'){
              $("#serviceofferdiv").removeClass("nextdivservice");
              $(".offer-return").addClass("offerreturns");
              enjoyhint_instance.trigger('next');
            }
            else if(option == '0'){
              $("#serviceofferdiv").addClass("servicesoffers");
              enjoyhint_instance.trigger('next');
            }
        })
        },
       'showSkip' : false,
        showNext : true
    },
    {
    'next .offerreturns' : 'Choose return option',
      onBeforeStart:function(){
        $("input[name='daysoffer[]']").click(function(){
           $("#serviceofferdiv").addClass("nextdivservice");
           //enjoyhint_instance.trigger('next');
        })
      },
      'showSkip' : false,
       showNext : true
    },
    {
      'next .nextdivservice' : 'Do you offer deliveries ?',
       onBeforeStart:function(){ //alert(1)
          option = $('#serviceprovide').val();
          //alert(option)
          if(option == '0'){
            $("#delivery_states").addClass("nextdivstate");
          }
          else{
            $(".services-offer").addClass("offerservices");
          }
          $('#serviceprovide').change(function(e){
              option = $(this).val();
              //alert(option)
              if(option == '1'){
                $("#serviceofferdiv").removeClass("nextdivservice");
                $(".services-offer").addClass("offerservices");
                //enjoyhint_instance.trigger('next');
              }
              else if(option == '0'){
                $(".services-offer").removeClass("offerservices");
                $("#serviceofferdiv").addClass("servicesoffers");
                enjoyhint_instance.trigger('next');
              }
          })
        },
      'showSkip' : false,
       showNext : true
    },
    {
    'next .offerservices' : 'Choose delivery option',
      onBeforeStart:function(){
        $("input[name='services[]']").click(function(){
           $("#serviceofferdiv").addClass("nextdivservice");
           //enjoyhint_instance.trigger('next');
        })
      },
      'showSkip' : false,
       showNext : true
    },
    {
      'next #delivery_states' : "Which states do you operate in ?",  
       showNext:false,  
       'showSkip' : false 
    },
    {
      'click #btnservice' : "Save delivery and return data", 
       showNext:true,  
       'showSkip' : false 
    },
    {
      'click #payment-tab' : "Click Here for Payment &billing Tab.",	
    	 showNext:false,	
    	 'showSkip' : false	
    },
    {
      'change #steps5' : "Please fill payment and billing details",
       onBeforeStart:function() {
        if(country == 'nigeria'){
          $("#payment-tab").trigger("click");
        }
        else if(country == "thailand"){
          $( "#payment-tab" ).trigger( "click" );
        }
       },
       showNext:true,
       'keyCode' : 13,
       'showSkip' : false 
    },
    {
       'click .btnpayment-payment' : "Save payment and billing details. Well done you finished Store Setup ! &#128077; Now let's create a Product Category",
       'showSkip' : false 
    }
  ];

  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
//}

var enjoyhint_instance = new EnjoyHint({});
var enjoyhint_script_steps = [
  {
  	//'next #pstep-1' : "Add name of product.",
  	'change #pstep-1' : "Enter the name of product or service",
    showNext : true,
  	'showSkip' : false
  },
  {
  	'next #pstep-3' : "Tell people what this product or service is about",
  	'showSkip' : false
  },
  {
  	'change #pstep-5' : "Now pick from a list Product Category that you created",
  	'showSkip' : false
  },
  {
    'next #pstep-7' : "How can customer get this product / service? <br> (1) Delivery - you can send it to them <br> (2) Offline - they are buying in your shop <br> (3) Delivery & Offline - they buy in the shop and you can also send it to them",
    showNext : true,
    'showSkip' : false
  },
  {
    'next .pstep-8' : "Enter the price <br> If you are doing sales, enter the sales price. If not, leave it empty",
    //showNext : true,
    'showSkip' : false
  },
  /*{
  	'change #pstep-2' : "Enter product price",
  	'showSkip' : false
  }, */ 
  {
	'next #pstep-4' : 'Enter the stock that you have for this product',
	'showSkip' : false
  },
  {
    'change #pstep-6' : "Upload an image. The file should not be more than 5MB. <br> You can skip this and do later",
    showNext : true,
    'showSkip' : false
  },
  {
	'change #send' : 'Save product',
	'showSkip' : false
  }

];

enjoyhint_instance.set(enjoyhint_script_steps);
enjoyhint_instance.run();

/*  //initialize instance
var enjoyhint_instance = new EnjoyHint({});

//simple config. 
//Only one step - highlighting(with description) "New" button 
//hide EnjoyHint after a click on the button.
var enjoyhint_script_steps = [
  {
    'click .step-12' : 'Search for a drug you\'re familiar with here'
  },
  { 
    'click .ratings-axis:first-child .rating' : 'Enter a rating by clicking the stars or by selecting "Don\'t know"'
  },
  { 
    'click .ratings-axis:nth-child(2) .rating' : 'Enter a rating by clicking the stars or by selecting "Don\'t know"'
  },
  { 
    'click .ratings-axis:nth-child(3) .rating' : 'Enter a rating by clicking the stars or by selecting "Don\'t know"'
  },
  { 
    'click .ratings-axis:nth-child(4) .rating' : 'Enter a rating by clicking the stars or by selecting "Don\'t know"'
  },
  { 
    'click .ratings-axis:last-child .rating' : 'Enter a rating by clicking the stars or by selecting "Don\'t know"'
  }
];

//set script config
enjoyhint_instance.set(enjoyhint_script_steps);

//run Enjoyhint script
enjoyhint_instance.run();*/
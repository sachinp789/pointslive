baseurl = window.location.origin + '/';
$(document).ready(function () {
    $('.mobile-nav--section').hide();

    var scroll = $(window).scrollTop();
    if (scroll > 40) {
        $('header').addClass('fixed');
    } if (scroll > 400) {
        $('#go-top').css('opacity', '1');
    }

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();

        if (scroll > 100) {
            $('header').addClass('fixed');
            $('a.nav-section--list--anchor').addClass('fixed-link');
            $('.mobile-nav--section a').addClass('fixed-link');
        } else {
            $('header').removeClass('fixed');
            $('a.nav-section--list--anchor').removeClass('fixed-link');
            $('.mobile-nav--section a').removeClass('fixed-link');
        }
    });

    $(window).scroll(function () {
        var scrolltop = $(window).scrollTop();

        if (scrolltop > 400) {
            $('#go-top').css('opacity', '1');
        } else {
            $('#go-top').css('opacity', '0');
        }
    });

    $('.nav-section--list .nav-section--list--item').each(function () {
        $('a', this).on('click', function (e) {
            e.preventDefault();
            var headerHeight = $('header').innerHeight();
            var hash = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 70}, 800, function () {});
            $(this).focus();
        });
    });

    $('#go-top').on('click', function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, 400, function () {});
    });

    $('.nav-section--user--action').on('click', function (e) {
        e.preventDefault();
        $('#popup-form, .form--main').fadeIn();

        $('#popup-form, .form-main--close--container i').not('.from--main').on('click', function () {
            $('#popup-form, .form--main').fadeOut();
        });

        if ($(this).attr('id') == 'sellers-login') {
            $('.form--main').find('.sellers-twitter').show();
            $('.form--main').find('.customers-twitter').hide();
            $('.form--main').find('.sellers-email').show();
            $('.form--main').find('.customers-email').hide();
        } else if ($(this).attr('id') == 'customers-login') {
            $('.form--main').find('.sellers-twitter').hide();
            $('.form--main').find('.customers-twitter').show();
            $('.form--main').find('.sellers-email').hide();
            $('.form--main').find('.customers-email').show();
        }
    });

    $('.sellers-email').on('click', function () {
        $('#sellers-form, #popup-form').fadeIn();
        $('.form--main').fadeOut();
    });
    
    $('.customers-email').on('click', function () {
        $('#customers-form, #popup-form').fadeIn();
        $('.form--main').fadeOut();
    });

    $('#menu_slide_form_sec_sellers').on('click', function () {
        $('#sellers-form').fadeOut();
        $('.form--main').fadeIn();
        $('.form--main').find('.sellers-twitter').show();
        $('.form--main').find('.customers-twitter').hide();
        $('.form--main').find('.sellers-email').show();
        $('.form--main').find('.customers-email').hide();
    });

    $('#menu_slide_form_sec_customers').on('click', function () {
        $('#customers-form').fadeOut();
        $('.form--main').fadeIn();
        $('.form--main').find('.sellers-twitter').hide();
        $('.form--main').find('.customers-twitter').show();
        $('.form--main').find('.sellers-email').hide();
        $('.form--main').find('.customers-email').show();
    });


    /*$('.nav-section--user--action').on('click', function(e) {
        e.preventDefault();
        $('#popup-form, .form--main').fadeIn();

        $('#popup-form, .form-main--close--container i').not('.from--main').on('click', function() {
            $('#popup-form, .form--main').fadeOut();
        });
    });*/

    $('#sidebar-open--anchor').on('click', function() {
        $('.sidebar-menu').addClass('move--left');
        $('#popup-form').fadeIn().on('click', function() {
            $('.sidebar-menu').removeClass('move--left');
            $(this).fadeOut();
        });
        $('#menu_slide').on('click', function () {
            $('.sidebar-menu').removeClass('move--left');
            $('#popup-form').fadeOut();
        });
    });

    $('.sidebar-menu--user--action').on('click', function(e) {
        e.preventDefault();
        
        $('#popup-form, .form--main').fadeIn();
        $('#popup-form, .form-main--close--container i').not('.from--main').on('click', function () {
            $('#popup-form, .form--main').fadeOut();
        });
        $('.sidebar-menu').removeClass('move--left');
    });

     $('#popup-form').on('click', function() {
        $('.form--main, .form-sec').fadeOut();
        $('.sidebar-menu').removeClass('move--left');
    });


    $('.sidebar-menu--list .sidebar-menu--list--item').each(function () {
        $('a', this).on('click', function (e) {
            e.preventDefault();
            var headerHeight = $('header').innerHeight();
            var hash = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(hash).offset().top - headerHeight
            }, 800, function () { });
            $('.sidebar-menu').removeClass('move--left');
            $('#popup-form').fadeOut();
        });
    });


});
//alert(1)
function create_custom_dropdowns() {
    $('select').each(function (i, select) {
        if (!$(this).next().hasClass('dropdown')) {
            $(this).after('<div class="dropdown ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
            var dropdown = $(this).next();
            var options = $(select).find('option');
            var selected = $(this).find('option:selected');

            var img = "";
            //dropdown.find('.current').html(selected.text());

            $('.dropdown .current').html($('.list ul').children().first().html());
            options.each(function (j, o) {
                var display = $(o).data('display-text') || '';
                if ($(o).val() === 'English') {
                    img = 'https://www.akinai.io/uploads/nigeria.png';
                } else if ($(o).val() === 'Thai') {
                    img = 'https://www.akinai.io/uploads/thailand.png';
                } else {
                    img = 'https://www.akinai.io/uploads/nigeria.png';
                }
                dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '"><img src=' + img + '>' + $(o).text() + '</li>');
            });

            /*$('.dropdown .current').html($('.list ul').children().first().html());
            options.each(function (j, o) {
                //alert($(o).text())
                var display = $(o).data('display-text') || '';
				if($(o).val() === 'English'){
					news = 'Nigeria';
                    img = 'https://pr01002.searchnative.com/uploads/nigeria.png';
				}else if($(o).val() === 'Thai'){
					news = 'Thailand';
                    img = 'https://pr01002.searchnative.com/uploads/thailand.png';
				} else {
					news = $(o).text();
                    img = 'https://pr01002.searchnative.com/uploads/nigeria.png';
				}
                dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '"><img src='+img+'>' + news + '</li>');
            });*/
        }
    });
}

// Event listeners

// Open/close
$(document).on('click', '.dropdown', function (event) {
    $('.dropdown').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).find('.option').attr('tabindex', 0);
        $(this).find('.selected').focus();
    } else {
        $(this).find('.option').removeAttr('tabindex');
        $(this).focus();
    }
});
// Close when clicking outside
$(document).on('click', function (event) {
    if ($(event.target).closest('.dropdown').length === 0) {
        $('.dropdown').removeClass('open');
        $('.dropdown .option').removeAttr('tabindex');
    }
    event.stopPropagation();
});
// Option click
$(document).on('click', '.dropdown .option', function (event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).html();
    $(this).closest('.dropdown').find('.current').html(text);
    $(this).closest('.dropdown').prev('select').val($(this).data('value')).trigger('change');
    translateLanguage($(this).data('value'));

});

// Keyboard events
$(document).on('keydown', '.dropdown', function (event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    if (event.keyCode == 32 || event.keyCode == 13) {
        if ($(this).hasClass('open')) {
            focused_option.trigger('click');
        } else {
            $(this).trigger('click');
        }
        return false;
        // Down
    } else if (event.keyCode == 40) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            focused_option.next().focus();
        }
        return false;
        // Up
    } else if (event.keyCode == 38) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
            focused_option.prev().focus();
        }
        return false;
        // Esc
    } else if (event.keyCode == 27) {
        if ($(this).hasClass('open')) {
            $(this).trigger('click');
        }
        return false;
    }
});

$(document).ready(function () {
    create_custom_dropdowns();
});
function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
}
var firstScroll = false;

$(window).on('scroll', function () {
    if (isScrolledIntoView('#pricing') && !firstScroll) {
         firstScroll = true;
         $('.free--icon').addClass('animated--icon');
    }
});

 function googleTranslateElementInit() {         
            new google.translate.TranslateElement({ pageLanguage: 'en,th', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false }, 'google_translate_element');
        }

function translateLanguage(lang) {
    var $frame = $('.goog-te-menu-frame:first');
    if (!$frame.size()) {
        alert("Error: Could not find Google translate frame.");
        return false;
    }
    $frame.contents().find('.goog-te-menu2-item span.text:contains(' + lang + ')').get(0).click();
    return false;
}

// Video next by next play home dashboard

//var video_count =1;
var videoPlayer = document.getElementById("episodeVideo");
var videoPlayer1 = document.getElementById("episodeVideo1");

/*herovide = document.getElementById('episodeVideo');
alert(herovide);
herovide.autoplay=true;
herovide.load();*/

//$(window).load(function(){
var bool = $("#episodeVideo").prop("muted");
//alert(bool);
function run(){

    attrvalue = $("#episodeVideo").attr("data-info");
       
    if(attrvalue.length == 0){
        $("#episodeVideo").attr('data-info', '4');
        $("#episodeVideo1").attr('data-info', '');
        $("#episodeVideo").removeAttr('muted');
        $("#episodeVideo").removeAttr('autoplay');
        $("#episodeVideo").removeAttr('preload');
        var nextVideo = "assets/img/Video2.mov"; 
        videoPlayer1.src = nextVideo;
        videoPlayer1.play(); 
    }
    else{
        var nextVideo = "assets/img/Video"+attrvalue+".mov";
        videoPlayer1.src = nextVideo;
        videoPlayer1.play();
    }
};
//});

document.getElementById('episodeVideo1').addEventListener('ended',function(){

    attrvalue = $("#episodeVideo1").attr("data-info");
    if(attrvalue.length == 0){
        $("#episodeVideo1").attr('data-info', '1');
        $("#episodeVideo").attr("preload","");
        $("#episodeVideo").attr("muted","");
        setTimeout(function(){
            videoPlayer.src = "assets/img/Video3.mov";
            videoPlayer.play();
        }, 10000);
    }
    else{
        //var oldone = $('#episodeVideo1').data('data-info');
        $("#episodeVideo").attr('data-info', '');
        videoPlayer.src = "assets/img/Video1.mov";
        videoPlayer.play();
    }

}, false);

// Seller signup / In
$("#login-form1").on('submit',function(event){ 

    var email = $('#selleremail').val();
    var password = $('#sellerpassword').val();
    var error = 0;
    //var emailReg = "/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/";
    
    if(email.length == 0|| email.length == null){
        $('span.error-keyup-words3').remove();
        $('#selleremail').after('<span class="sellererror error-keyup-words3">Email address field is required.</span>');
        error=1;
    }
    else if(!validateEmail(email)){
        $('span.error-keyup-words3').remove();
        $('#selleremail').after('<span class="sellererror error-keyup-words3">Please enter valid email.</span>');
        error=1;
    }
    else{
        $('span.error-keyup-words3').remove();
    }

    if(password.length == 0|| password.length == null){
        $('span.error-keyup-words4').remove();
        $('#sellerpassword').after('<span class="sellererror error-keyup-words4">Password field is required.</span>');
        error=1;
    }
    else{
        $('span.error-keyup-words4').remove();
    }

    if(error){
        event.preventDefault();
    }
    else{
        $.ajax({                
            type : 'POST',
            url  : baseurl+'user/signUpwithLogin',
            dataType: "json",
            data : {
                useremail:email,
                userpwd:password 
            },
            beforeSend: function(){ 
                //$(".error").fadeOut();
                $("#sellerlogin-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Signing In ...');
            },
            success : function(response){
                //console.log(response);   
                if($.trim(response) === "1"){                                    
                    $("#sellerlogin-submit").html('Signing In ...');
                    setTimeout(' window.location.href = "'+baseurl+'admin/dashboard"; ',2000);
                } else {                                    
                    $(".sellererror").html(response).show();
                }
            },
            error: function(response){//console.log(response);
                $(".sellererror").html(response.responseText).show();
            }
        }); 
        event.preventDefault();
    }
});

// Customer signup / In
$("#login-form").on('submit',function(event){ 

    var email = $('#email').val();
    var password = $('#password').val();
    var error = 0;
    //var emailReg = "/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/";
    
    if(email.length == 0|| email.length == null){
        $('span.error-keyup-words5').remove();
        $('#email').after('<span class="error error-keyup-words5">Email address field is required.</span>');
        error=1;
    }
    else if(!validateEmail(email)){
        $('span.error-keyup-words5').remove();
        $('#email').after('<span class="error error-keyup-words5">Please enter valid email.</span>');
        error=1;
    }
    else{
        $('span.error-keyup-words5').remove();
    }

    if(password.length == 0|| password.length == null){
        $('span.error-keyup-words6').remove();
        $('#password').after('<span class="error error-keyup-words6">Password field is required.</span>');
        error=1;
    }
    else{
        $('span.error-keyup-words6').remove();
    }

    if(error){
        event.preventDefault();
    }
    else{
        $.ajax({                
            type : 'POST',
            url  : baseurl+'customer/signUpwithLogin',
            dataType: "json",
            data : {
                useremail:email,
                userpwd:password 
            },
            beforeSend: function(){ 
                //$(".error").fadeOut();
                $("#customer-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Signing In ...');
            },
            success : function(response){
                //console.log(response);   
                if ($.trim(response) === "1") {
                    $("#customer-submit").html('Signing In ...');
                    setTimeout(' window.location.href = "' + baseurl + 'item/order_history"; ', 2000);
                } else {
                    $(".error").html(response).show();
                }
            },
            error: function(response){//console.log(response);
                $(".error").html(response.responseText).show();
            }
        }); 
        event.preventDefault();
    }
});

function validateEmail(Email) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    return $.trim(Email).match(pattern) ? true : false;
}

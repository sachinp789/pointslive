function create_custom_dropdowns() {
    $("select").each(function(e, o) {
        if (!$(this).next().hasClass("dropdown")) {
            $(this).after('<div class="dropdown ' + ($(this).attr("class") || "") + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
            var t = $(this).next(),
                i = $(o).find("option"),
                r = ($(this).find("option:selected"), "");
            $(".dropdown .current").html($(".list ul").children().first().html()), i.each(function(e, o) {
                var i = $(o).data("display-text") || "";
                r = "English" === $(o).val() ? "https://www.akinai.io/uploads/nigeria.png" : "Thai" === $(o).val() ? "https://www.akinai.io/uploads/thailand.png" : "https://www.akinai.io/uploads/nigeria.png", t.find("ul").append('<li class="option ' + ($(o).is(":selected") ? "selected" : "") + '" data-value="' + $(o).val() + '" data-display-text="' + i + '"><img src=' + r + ">" + $(o).text() + "</li>")
            })
        }
    })
}

function isScrolledIntoView(e) {
    var o = $(window).scrollTop(),
        i = o + $(window).height(),
        t = $(e).offset().top;
    $(e).height();
    return t <= i && o <= t
}
baseurl = window.location.origin + "/", $(document).ready(function() {
    $('#banner-cta').on('click', function (e) {
        e.preventDefault();
        var headerHeight = $('header').innerHeight();
        var hash = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(hash).offset().top - 70
        }, 800, function () {});
        $(this).focus();
    });
    $('#features .service-wrap').each(function () {
        $('a', this).on('click', function (e) {
            e.preventDefault();
            var headerHeight = $('header').innerHeight();
            var hash = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 150
            }, 800, function () {});
            $(this).focus();
        });
    });
    $(".mobile-nav--section").hide();
    var e = $(window).scrollTop();
    40 < e && $("header").addClass("fixed"), 400 < e && $("#go-top").css("opacity", "1"), $(window).scroll(function(e) {
        100 < $(window).scrollTop() ? ($("header").addClass("fixed"), $("a.nav-section--list--anchor").addClass("fixed-link"), $(".mobile-nav--section a").addClass("fixed-link")) : ($("header").removeClass("fixed"), $("a.nav-section--list--anchor").removeClass("fixed-link"), $(".mobile-nav--section a").removeClass("fixed-link"))
    }), $(window).scroll(function() {
        400 < $(window).scrollTop() ? $("#go-top").css("opacity", "1") : $("#go-top").css("opacity", "0")
    }), $(".nav-section--list .nav-section--list--item").each(function() {
        $("a", this).on("click", function(e) {
            e.preventDefault();
            $("header").innerHeight();
            var o = $(this).attr("href");
            $("html, body").animate({
                scrollTop: $(o).offset().top - 70
            }, 800, function() {}), $(this).focus()
        })
    }), $("#go-top").on("click", function() {
        $("html, body").animate({
            scrollTop: $("body").offset().top
        }, 400, function() {})
    }), $(".nav-section--user--action").on("click", function(e) {
        e.preventDefault(), $("#popup-form, .form--main").fadeIn(), $("#popup-form, .form-main--close--container i").not(".from--main").on("click", function() {
            $("#popup-form, .form--main").fadeOut()
        }), "sellers-login" == $(this).attr("id") ? ($(".form--main").find(".sellers-twitter").show(), $(".form--main").find(".customers-twitter").hide(), $(".form--main").find(".sellers-email").show(), $(".form--main").find(".customers-email").hide()) : "customers-login" == $(this).attr("id") && ($(".form--main").find(".sellers-twitter").hide(), $(".form--main").find(".customers-twitter").show(), $(".form--main").find(".sellers-email").hide(), $(".form--main").find(".customers-email").show())
    }), $(".sellers-email").on("click", function() {
        $("#sellers-form, #popup-form").fadeIn(), $(".form--main").fadeOut()
    }), $(".customers-email").on("click", function() {
        $("#customers-form, #popup-form").fadeIn(), $(".form--main").fadeOut()
    }), $("#menu_slide_form_sec_sellers").on("click", function() {
        $("#sellers-form").fadeOut(), $(".form--main").fadeIn(), $(".form--main").find(".sellers-twitter").show(), $(".form--main").find(".customers-twitter").hide(), $(".form--main").find(".sellers-email").show(), $(".form--main").find(".customers-email").hide()
    }), $("#menu_slide_form_sec_customers").on("click", function() {
        $("#customers-form").fadeOut(), $(".form--main").fadeIn(), $(".form--main").find(".sellers-twitter").hide(), $(".form--main").find(".customers-twitter").show(), $(".form--main").find(".sellers-email").hide(), $(".form--main").find(".customers-email").show()
    }), $("#sidebar-open--anchor").on("click", function() {
        $(".sidebar-menu").addClass("move--left"), $("#popup-form").fadeIn().on("click", function() {
            $(".sidebar-menu").removeClass("move--left"), $(this).fadeOut()
        }), $("#menu_slide").on("click", function() {
            $(".sidebar-menu").removeClass("move--left"), $("#popup-form").fadeOut()
        })
    }), $(".sidebar-menu--user--action").on("click", function(e) {
        e.preventDefault(), $("#popup-form, .form--main").fadeIn(), $("#popup-form, .form-main--close--container i").not(".from--main").on("click", function() {
            $("#popup-form, .form--main").fadeOut()
        }), $(".sidebar-menu").removeClass("move--left")
    }), $("#popup-form").on("click", function() {
        $(".form--main, .form-sec").fadeOut(), $(".sidebar-menu").removeClass("move--left")
    }), $(".sidebar-menu--list .sidebar-menu--list--item").each(function() {
        $("a", this).on("click", function(e) {
            e.preventDefault();
            var o = $("header").innerHeight(),
                i = $(this).attr("href");
            $("html, body").animate({
                scrollTop: $(i).offset().top - o
            }, 800, function() {}), $(".sidebar-menu").removeClass("move--left"), $("#popup-form").fadeOut()
        })
    })
}), $(document).on("click", ".dropdown", function(e) {
    $(".dropdown").not($(this)).removeClass("open"), $(this).toggleClass("open"), $(this).hasClass("open") ? ($(this).find(".option").attr("tabindex", 0), $(this).find(".selected").focus()) : ($(this).find(".option").removeAttr("tabindex"), $(this).focus())
}), $(document).on("click", function(e) {
    0 === $(e.target).closest(".dropdown").length && ($(".dropdown").removeClass("open"), $(".dropdown .option").removeAttr("tabindex")), e.stopPropagation()
}), $(document).on("click", ".dropdown .option", function(e) {
    $(this).closest(".list").find(".selected").removeClass("selected"), $(this).addClass("selected");
    var o = $(this).html();
    $(this).closest(".dropdown").find(".current").html(o), $(this).closest(".dropdown").prev("select").val($(this).data("value")).trigger("change"), translateLanguage($(this).data("value"))
}), $(document).on("keydown", ".dropdown", function(e) {
    var o = $($(this).find(".list .option:focus")[0] || $(this).find(".list .option.selected")[0]);
    if (32 == e.keyCode || 13 == e.keyCode) return $(this).hasClass("open") ? o.trigger("click") : $(this).trigger("click"), !1;
    if (40 == e.keyCode) return $(this).hasClass("open") ? o.next().focus() : $(this).trigger("click"), !1;
    if (38 == e.keyCode) {
        if ($(this).hasClass("open"))(o = $($(this).find(".list .option:focus")[0] || $(this).find(".list .option.selected")[0])).prev().focus();
        else $(this).trigger("click");
        return !1
    }
    return 27 == e.keyCode ? ($(this).hasClass("open") && $(this).trigger("click"), !1) : void 0
}), $(document).ready(function() {
    create_custom_dropdowns()
});
var firstScroll = !1;

function googleTranslateElementInit() {
    new google.translate.TranslateElement({
        pageLanguage: "en,th",
        layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
        autoDisplay: !1
    }, "google_translate_element")
}

function translateLanguage(e) {
    var o = $(".goog-te-menu-frame:first");
    return o.size() ? o.contents().find(".goog-te-menu2-item span.text:contains(" + e + ")").get(0).click() : alert("Error: Could not find Google translate frame."), !1
}
$(window).on("scroll", function() {
    isScrolledIntoView("#pricing") && !firstScroll && (firstScroll = !0, $(".free--icon").addClass("animated--icon"))
});

/*var videoPlayer = document.getElementById("episodeVideo"),
    videoPlayer1 = document.getElementById("episodeVideo1"),
    bool = $("#episodeVideo").prop("muted");

function run() {
    if (attrvalue = $("#episodeVideo").attr("data-info"), 0 == attrvalue.length) {
        $("#episodeVideo").attr("data-info", "4"), $("#episodeVideo1").attr("data-info", ""), $("#episodeVideo").removeAttr("muted"), $("#episodeVideo").removeAttr("autoplay"), $("#episodeVideo").removeAttr("preload");
        var e = "assets/img/Video2.mov";
        videoPlayer1.src = e, videoPlayer1.play()
    } else {
        e = "assets/img/Video" + attrvalue + ".mov";
        videoPlayer1.src = e, videoPlayer1.play()
    }
}*/

function validateEmail(e) {
    return !!$.trim(e).match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)
}
/*document.getElementById("episodeVideo1").addEventListener("ended", function() {
    attrvalue = $("#episodeVideo1").attr("data-info"), 0 == attrvalue.length ? ($("#episodeVideo1").attr("data-info", "1"), $("#episodeVideo").attr("preload", ""), $("#episodeVideo").attr("muted", ""), setTimeout(function() {
        videoPlayer.src = "assets/img/Video3.mov", videoPlayer.play()
    }, 1e4)) : ($("#episodeVideo").attr("data-info", ""), videoPlayer.src = "assets/img/Video1.mov", videoPlayer.play())
}, !1), */
$("#login-form1").on("submit", function(e) {
    var o = $("#selleremail").val(),
        i = $("#sellerpassword").val(),
        t = 0;
    0 == o.length || null == o.length ? ($("span.error-keyup-words3").remove(), $("#selleremail").after('<span class="sellererror error-keyup-words3">Email address field is required.</span>'), t = 1) : validateEmail(o) ? $("span.error-keyup-words3").remove() : ($("span.error-keyup-words3").remove(), $("#selleremail").after('<span class="sellererror error-keyup-words3">Please enter valid email.</span>'), t = 1), 0 == i.length || null == i.length ? ($("span.error-keyup-words4").remove(), $("#sellerpassword").after('<span class="sellererror error-keyup-words4">Password field is required.</span>'), t = 1) : $("span.error-keyup-words4").remove(), t || $.ajax({
        type: "POST",
        url: baseurl + "user/signUpwithLogin",
        dataType: "json",
        data: {
            useremail: o,
            userpwd: i
        },
        beforeSend: function() {
            $("#sellerlogin-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Signing In ...')
        },
        success: function(e) {
            "1" === $.trim(e) ? ($("#sellerlogin-submit").html("Signing In ..."), setTimeout(' window.location.href = "' + baseurl + 'admin/dashboard"; ', 2e3)) : $(".sellererror").html(e).show()
        },
        error: function(e) {
            $(".sellererror").html(e.responseText).show()
        }
    }), e.preventDefault()
}), 

// Customer signup / In
$("#login-form").on('submit', function (event) {
	//event.preventDefault();    
	var email = $('#email').val();
	var password = $('#password').val();
	var error = 0;
	//var emailReg = "/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/";

	if (email.length == 0 || email.length == null) {
		$('span.error-keyup-words31').remove();
		$('#email').after('<span class="error error-keyup-words31">Email address field is required.</span>');
		error = 1;
	} else if (!validateEmail(email)) {
		$('span.error-keyup-words31').remove();
		$('#email').after('<span class="error error-keyup-words31">Please enter valid email.</span>');
		error = 1;
	} else {
		$('span.error-keyup-words31').remove();
	}

	if (password.length == 0 || password.length == null) {
		$('span.error-keyup-words42').remove();
		$('#password').after('<span class="error error-keyup-words42">Password field is required.</span>');
		error = 1;
	} else {
		$('span.error-keyup-words42').remove();
	}

	if (error) {
		event.preventDefault();
	} else {
		$.ajax({
			type: 'POST',
			url: baseurl + 'customer/signUpwithLogin',
			dataType: "json",
			data: {
				useremail: email,
				userpwd: password
			},
			beforeSend: function () {
				//$(".error").fadeOut();
				$("#customer-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Please Wait..');
			},
			success: function (response) {
				if (response.status == true) {
					$("#customer-submit").html('Signing In ...');
					setTimeout(' window.location.href = "' +  response.msg + '"; ', 2000);
				} else {
					$(".error").html(response.msg).show();
				}
			},
			error: function (response) {
				//console.log(response.responseText);
				$(".error").html(response.responseText).show();
			}
		});
		event.preventDefault();
	}
});

(function () {
    let maxSlides = $('.mockup-image img').length
    let counter = 2
    let autoPlay = function () {
        $('.mockup-image img:nth-child(' + counter + ')').addClass('active').siblings().removeClass(
            'active')
        $('.navigator .step-single:nth-child(' + counter + ')').addClass('active').siblings().removeClass(
            'active')
        $('.mobile-navigator .digits li:nth-child(' + counter + ')').addClass('active').siblings().removeClass(
            'active')

        $('.mobile-navigator-text .step-single:nth-child(' + counter + ')').addClass('active').siblings()
            .removeClass(
                'active')
        counter++
        if (counter > maxSlides) {
            counter = 1
        }
    }

    // SET INTERVAL
    let loopThrough = setInterval(autoPlay, 3000)

    // CHECK FOR CLICK 
    $('.navigator .step-single button').on('click', function () {
        clearInterval(loopThrough)
        counter = $(this).parent().data('index')
        $('.mockup-image img:nth-child(' + counter + ')').addClass('active').siblings().removeClass(
            'active')
        $('.navigator .step-single:nth-child(' + counter + ')').addClass('active').siblings().removeClass(
            'active')
        loopThrough = setInterval(autoPlay, 3000)
        intervalStatus = true
    });


    $('.mobile-navigator .digits li a').on('click', function (e) {
        e.preventDefault();
        clearInterval(loopThrough)
        counter = $(this).data('index')
        $('.mockup-image img:nth-child(' + counter + ')').addClass('active').siblings().removeClass(
            'active')

        $(this).parent().addClass('active').siblings().removeClass('active')

        $('.mobile-navigator .step-single:nth-child(' + counter + ')').addClass('active').siblings()
            .removeClass('active')
        loopThrough = setInterval(autoPlay, 3000)
        intervalStatus = true
    });
})();

/*$("#login-form").on("submit", function(e) {
    var o = $("#email").val(),
        i = $("#password").val(),
        t = 0;
    0 == o.length || null == o.length ? ($("span.error-keyup-words5").remove(), $("#email").after('<span class="error error-keyup-words5">Email address field is required.</span>'), t = 1) : validateEmail(o) ? $("span.error-keyup-words5").remove() : ($("span.error-keyup-words5").remove(), $("#email").after('<span class="error error-keyup-words5">Please enter valid email.</span>'), t = 1), 0 == i.length || null == i.length ? ($("span.error-keyup-words6").remove(), $("#password").after('<span class="error error-keyup-words6">Password field is required.</span>'), t = 1) : $("span.error-keyup-words6").remove(), t || $.ajax({
        type: "POST",
        url: baseurl + "customer/signUpwithLogin",
        dataType: "json",
        data: {
            useremail: o,
            userpwd: i
        },
        beforeSend: function() {
            $("#customer-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Signing In ...')
        },
        success: function(e) {
            "1" === $.trim(e) ? ($("#customer-submit").html("Signing In ..."), setTimeout(' window.location.href = "' + baseurl + 'item/order_history"; ', 2e3)) : $(".error").html(e).show()
        },
        error: function(e) {
            $(".error").html(e.responseText).show()
        }
    }), e.preventDefault()
});*/
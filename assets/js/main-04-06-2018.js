$(document).ready(function () {
    $('.mobile-nav--section').hide();

    var scroll = $(window).scrollTop();
    if (scroll > 40) {
        $('header').addClass('fixed');
    } if (scroll > 400) {
        $('#go-top').css('opacity', '1');
    }

    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();

        if (scroll > 50) {
            $('header').addClass('fixed');
            $('a.nav-section--list--anchor').addClass('fixed-link');
            $('.mobile-nav--section a').addClass('fixed-link');
        } else {
            $('header').removeClass('fixed');
            $('a.nav-section--list--anchor').removeClass('fixed-link');
            $('.mobile-nav--section a').removeClass('fixed-link');
        }
    });

    $(window).scroll(function () {
        var scrolltop = $(window).scrollTop();

        if (scrolltop > 400) {
            $('#go-top').css('opacity', '1');
        } else {
            $('#go-top').css('opacity', '0');
        }
    });

    $('.nav-section--list .nav-section--list--item').each(function () {
        $('a', this).on('click', function (e) {
            e.preventDefault();
            var headerHeight = $('header').innerHeight();
            var hash = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(hash).offset().top}, 800, function () {});
            $(this).focus();
        });
    });

    $('#go-top').on('click', function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        }, 400, function () {});
    });

    $('.nav-section--user--action').on('click', function(e) {
        e.preventDefault();
        $('#popup-form, .form--main').fadeIn();

        $('#popup-form, .form-main--close--container i').not('.from--main').on('click', function() {
            $('#popup-form, .form--main').fadeOut();
        });
    });

    $('#sidebar-open--anchor').on('click', function() {
        $('.sidebar-menu').addClass('move--left');
        $('#popup-form').fadeIn().on('click', function() {
            $('.sidebar-menu').removeClass('move--left');
            $(this).fadeOut();
        });
        $('#menu_slide').on('click', function () {
            $('.sidebar-menu').removeClass('move--left');
            $('#popup-form').fadeOut();
        });
    });

    $('.sidebar-menu--user--action').on('click', function(e) {
        e.preventDefault();
        
        $('#popup-form, .form--main').fadeIn();
        $('#popup-form, .form-main--close--container i').not('.from--main').on('click', function () {
            $('#popup-form, .form--main').fadeOut();
        });
        $('.sidebar-menu').removeClass('move--left');
    });


    $('.sidebar-menu--list .sidebar-menu--list--item').each(function () {
        $('a', this).on('click', function (e) {
            e.preventDefault();
            var headerHeight = $('header').innerHeight();
            var hash = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(hash).offset().top - headerHeight
            }, 800, function () { });
            $('.sidebar-menu').removeClass('move--left');
            $('#popup-form').fadeOut();
        });
    });


});

function create_custom_dropdowns() {
    $('select').each(function (i, select) {
        if (!$(this).next().hasClass('dropdown')) {
            $(this).after('<div class="dropdown ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
            var dropdown = $(this).next();
            var options = $(select).find('option');
            var selected = $(this).find('option:selected');
            dropdown.find('.current').html(selected.data('display-text') || selected.text());
            options.each(function (j, o) {
                var display = $(o).data('display-text') || '';
				if($(o).val() === 'English'){
					news = 'Nigeria';
				}else if($(o).val() === 'Thai'){
					news = 'Thailand';
				} else {
					news = $(o).text();
				}
                dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + news + '</li>');
            });
        }
    });
}

// Event listeners

// Open/close
$(document).on('click', '.dropdown', function (event) {
    $('.dropdown').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).find('.option').attr('tabindex', 0);
        $(this).find('.selected').focus();
    } else {
        $(this).find('.option').removeAttr('tabindex');
        $(this).focus();
    }
});
// Close when clicking outside
$(document).on('click', function (event) {
    if ($(event.target).closest('.dropdown').length === 0) {
        $('.dropdown').removeClass('open');
        $('.dropdown .option').removeAttr('tabindex');
    }
    event.stopPropagation();
});
// Option click
$(document).on('click', '.dropdown .option', function (event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown').find('.current').text(text);
    $(this).closest('.dropdown').prev('select').val($(this).data('value')).trigger('change');
    translateLanguage($(this).data('value'));

});

// Keyboard events
$(document).on('keydown', '.dropdown', function (event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    if (event.keyCode == 32 || event.keyCode == 13) {
        if ($(this).hasClass('open')) {
            focused_option.trigger('click');
        } else {
            $(this).trigger('click');
        }
        return false;
        // Down
    } else if (event.keyCode == 40) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            focused_option.next().focus();
        }
        return false;
        // Up
    } else if (event.keyCode == 38) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
            focused_option.prev().focus();
        }
        return false;
        // Esc
    } else if (event.keyCode == 27) {
        if ($(this).hasClass('open')) {
            $(this).trigger('click');
        }
        return false;
    }
});

$(document).ready(function () {
    create_custom_dropdowns();
});
function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
}
var firstScroll = false;

$(window).on('scroll', function () {
    if (isScrolledIntoView('#pricing') && !firstScroll) {
         firstScroll = true;
         $('.free--icon').addClass('animated--icon');
    }
});
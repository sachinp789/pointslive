baseurl = window.location.origin+'/';
$(document).ready(function(){
	// Ajax product add
	$("#addtocart").on('click',function(event){
		event.preventDefault();

		sellerID = $("#seller").val();
		productID = $("#product").val();
		userqty = $("#qty").val();
		psku = $("#productsku").val();

		if(userqty <= 0 || userqty == "" || userqty == null){
			alert("Warning ! Quantity must be equal or greater than 1");
			return false;
		}
			
		$.ajax({
	      url:baseurl +'item/AddItems',
	      type:"POST",
	      data: {seller:sellerID,product:productID,qty:userqty,sku:psku},
	      dataType:"json",
	      /*beforeSend: function(){
	         $("#addtocart").text("Please wait....");
	      },*/
	      success: function(response){
	      	if(response.msg == true){
	      		$('#message-box-success').html('<img src='+baseurl+'assets/img/logo.png><p>'+response.message+'</p>');
	      		$('#message-box-success').slideDown('slow').delay(1000).slideUp('slow');
                //$(".mobile-view").load(location.href+" .mobile-view>*","");
                $("#cartdata").load(location.href+" #cartdata>*","");
	      		$("#btncheckout").removeClass('disable');
	      		//setTimeout(function(){ location.reload(true); }, 1000);
	      	}
	      	else{
	      		$('#message-box-error').html('<p>'+response.message+'</p>');
	      		$('#message-box-error').slideDown('slow').delay(1000).slideUp('slow');
	      	}
	      }
	    });
	});

	$("#qty").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
     }
   });

	$("#quantity").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
     }
   });

	
 	// Get local areas 
 	$("#selectstate, #selectstate1").on('change',function(){
 		var dest = $(this).val();
        finalword = dest.split("_").join(" ");
        countryname = $("#countryname").val();
        selectarea = "";
        city = "";
       
        formid = $(this).closest("form").attr('id');

        $.ajax({
	      url:baseurl +'item/GetLocalAreas',
	      type:"POST",
	      data: {state:finalword,country:countryname},
	      dataType:"json",
	      success: function(response){
	      	if(response.msg == true){
                if(formid == 'checkoutForm'){
    	      		$("#selectarea").find('option').get(0).remove();
    	      		selectarea = "<option value=''>Choose From List of Local Governments</option>";
    	      		$.each(response.listcities, function( index, value ){
    	      			if(value.district){
    	      				city = value.district;
    	      			}
    	      			else if(value.local_area){
    	      				city = value.local_area;
    	      			}
    				    selectarea+="<option value="+city+">"+city+"</option>";
    				});
                    //console.log(selectarea);
    				$("#selectarea").html(selectarea);
                }
                else if(formid == 'usercheckoutForm'){
                    $("#selectarea1").find('option').get(0).remove();
                    selectarea = "<option value=''>Choose From List of Local Governments</option>";
                    $.each(response.listcities, function( index, value ){
                        if(value.district){
                            city = value.district;
                        }
                        else if(value.local_area){
                            city = value.local_area;
                        }
                        selectarea+="<option value="+city+">"+city+"</option>";
                    });
                    $("#selectarea1").html(selectarea);
                }
	      	}
	      }
	    });

 	});	

 	// Hide address of pickup in store

 	$(".deliverytype").on('click',function(){
 		type = $(this).val();

 		if(type == "pickup"){
 			$(".deliverymethod").hide();
 		}
 		else if(type == "delivery"){
 			$(".deliverymethod").show();
 		}
 	})	
	

});

// Load quick menu js

$('#sidebar-open--anchor').on('click', function() {
        $('.sidebar-menu').addClass('move--left');
        $('#popup-form').fadeIn().on('click', function() {
            $('.sidebar-menu').removeClass('move--left');
            $(this).fadeOut();
        });
        $('#menu_slide').on('click', function () {
            $('.sidebar-menu').removeClass('move--left');
            $('#popup-form').fadeOut();
        });
    });

function create_custom_dropdowns() {
    $('#main-nav--selector').each(function (i, select) {
        if (!$(this).next().hasClass('dropdown')) {
            $(this).after('<div class="dropdown ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
            var dropdown = $(this).next();
            var options = $(select).find('option');
            var selected = $(this).find('option:selected');
            dropdown.find('.current').html(selected.data('display-text') || selected.text());
            options.each(function (j, o) {
                var display = $(o).data('display-text') || '';
                dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '"><i class="'+ $(o).data('fa')+'"></i><a href="'+ $(o).val() +'">' + $(o).text() + '</a></li>');
            });
        }
    });
 }
// Event listeners
// Open/close
$(document).on('click', '.dropdown', function (event) {
    $('.dropdown').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).find('.option').attr('tabindex', 0);
        $(this).find('.selected').focus();
    } else {
        $(this).find('.option').removeAttr('tabindex');
        $(this).focus();
    }
});
// Close when clicking outside
$(document).on('click', function (event) {
    if ($(event.target).closest('.dropdown').length === 0) {
        $('.dropdown').removeClass('open');
        $('.dropdown .option').removeAttr('tabindex');
    }
    event.stopPropagation();
});
// Option click
$(document).on('click', '.dropdown .option', function (event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown').find('.current').text(text);
    $(this).closest('.dropdown').prev('select').val($(this).data('value')).trigger('change');
});

// Keyboard events
$(document).on('keydown', '.dropdown', function (event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    if (event.keyCode == 32 || event.keyCode == 13) {
        if ($(this).hasClass('open')) {
            focused_option.trigger('click');
        } else {
            $(this).trigger('click');
        }
        return false;
        // Down
    } else if (event.keyCode == 40) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            focused_option.next().focus();
        }
        return false;
        // Up
    } else if (event.keyCode == 38) {
        if (!$(this).hasClass('open')) {
            $(this).trigger('click');
        } else {
            var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
            focused_option.prev().focus();
        }
        return false;
        // Esc
    } else if (event.keyCode == 27) {
        if ($(this).hasClass('open')) {
            $(this).trigger('click');
        }
        return false;
    }
});

$(document).ready(function () {
    create_custom_dropdowns();
});

/*function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
}
var firstScroll = false;

$(window).on('scroll', function () {
    if (isScrolledIntoView('#pricing') && !firstScroll) {
        firstScroll = true;
        $('.free--icon').addClass('animated--icon');
    }
});*/

//16-08-2018
$('.btn.confirm-failed').on('click', function(e) {
    e.preventDefault();
    //console.log('Clicked !');
    $('.dropdown-confirm-failed').toggleClass('show');
});

$("#btnaction").on('click',function(){
    radio = $('input[name=failed_reason]:checked').val();
    orderID = $("#orderid").val();
    if(typeof(radio)  === "undefined"){
        $('span.error.err').remove(); 
        $("#btnaction").after("<span class='error err'>* Reason required.</span>");
    }
    else{
        $("#loader").fadeIn();
        $('span.error.err').remove();
        $.ajax({                
            type : 'POST',
            dataType:'json',
            url  : baseurl+'item/submitReason',
            data : {
                reason:radio,
                order:orderID
            },
            beforeSend: function(){
                $("#loader").text('Processing..');
            },
            success: function(response){ 
              if(response.msg){
                $("#loader").text("Success!");
                $(".status-container").load(location.href + " .status-container");
                $(".dropdown-confirm-failed").removeClass('show');
                $(".dropdown-confirm-failed").addClass('hide');
                $(".confirm-failed").toggle()
              }
              else{
                $("#loader").text("Errr!");
              }
              $("#loader").fadeOut("slow");
            }
        });
    }
});

// 19-09-2018 Ajax add to cart popup

$(document).on('click','#addcart',function(event){
    event.preventDefault();
    sellerID = $("#seller").val();
    productID = $("#product").val();
    userqty = $("#qty2").val();
    psku = $("#productsku").val();

    if(userqty <= 0 || userqty == "" || userqty == null){
        alert("Warning ! Quantity must be equal or greater than 1");
        return false;
    }
        
    $.ajax({
      url:baseurl +'item/AddItems',
      type:"POST",
      data: {seller:sellerID,product:productID,qty:userqty,sku:psku},
      dataType:"json",
      success: function(response){
        if(response.msg == true){
            $(".details-overlay, #order-confirm-popup").fadeIn();
            $("#confirmmessage > h2").text(response.message);
            //$(".order-details-action-container a:nth-child(2)").load(location.href+" .order-details-action-container a:nth-child(2)>*","");
            $(".order-details-action-container").load(location.href+" .order-details-action-container>*","");
            $(".right-head.navigator-right").load(location.href+" .right-head.navigator-right>*","");
            setTimeout(
              function() 
              {
                $(".details-overlay, #order-confirm-popup").fadeOut();
              }, 3000);
        }
        else{
            alert(response.message);
        }
      }
    });
});

$(document).on('click','.directbuy',function(event){
    event.preventDefault();
    sellerID = $("#seller").val();
    productID = $("#product").val();
    userqty = $("#qty2").val();
    psku = $("#productsku").val();
    seller_info = $("#seller-id").val();

    if(userqty <= 0 || userqty == "" || userqty == null){
        alert("Warning ! Quantity must be equal or greater than 1");
        return false;
    }
        
    $.ajax({
      url:baseurl +'item/AddItems',
      type:"POST",
      data: {seller:sellerID,product:productID,qty:userqty,sku:psku},
      dataType:"json",
      success: function(response){
        if(response.msg == true){
            window.location.href = baseurl+"item/checkout/"+seller_info;
            /*$(".details-overlay, #order-confirm-popup").fadeIn();
            $("#confirmmessage > h2").text(response.message);
            $(".order-details-action-container").load(location.href+" .order-details-action-container>*","");
            $(".right-head.navigator-right").load(location.href+" .right-head.navigator-right>*","");
            setTimeout(
              function() 
              {
                $(".details-overlay, #order-confirm-popup").fadeOut();
              }, 3000);*/
        }
        else{
            alert(response.message);
        }
      }
    });
});

//11-09-2018

// Address validation
$("#caddressForm").on("submit",function(e){

    //var country = $("#choosecountry").val();
    var state = $("#choosestate").val();
    var area = $("#local_government").val();
    var address = $("#custaddress").val();
    var action = $("#caddressForm").attr("action");
    
    var error = 0;

    /*if(country == null || country == ""){
        $('span.error-keyup-ct1').remove();
        $('#choosecountry').after('<span class="error error-keyup-ct1"> *</span>');
        error = 1;
    }else {
        $('span.error-keyup-ct1').remove();
    }*/

    if(state == null || state == ""){
        $('span.error-keyup-ct2').remove();
        $('#choosestate').after('<span class="error error-keyup-ct2"> *</span>');
        error = 1;
    }else {
        $('span.error-keyup-ct2').remove();
    }

    if(area == null || area == ""){
        $('span.error-keyup-ct3').remove();
        $('#local_government').after('<span class="error error-keyup-ct3"> *</span>');
        error = 1;
    }else {
        $('span.error-keyup-ct3').remove();
    }
    //alert(error)
    if (error) {
       e.preventDefault();
    } else {
       window.location.href = action;
    }

})

$("#caddressForm1").on("submit",function(e){
    
    //var country = $("#choosecountry1").val();
    var state = $("#choosestate1").val();
    var area = $("#chooseaddresarea1").val();
    var address = $("#custaddress1").val();
    var action = $("#caddressForm1").attr("action");
    
    var error = 0;

    /*if(country == null || country == ""){
        $('span.error-keyup-ct11').remove();
        $('#choosecountry1').after('<span class="error error-keyup-ct11"> *</span>');
        error = 1;
    }else {
        $('span.error-keyup-ct11').remove();
    }*/

    if(state == null || state == ""){
        $('span.error-keyup-ct21').remove();
        $('#choosestate1').after('<span class="error error-keyup-ct21"> *</span>');
        error = 1;
    }else {
        $('span.error-keyup-ct21').remove();
    }

    if(area == null || area == ""){
        $('span.error-keyup-ct31').remove();
        $('#chooseaddresarea1').after('<span class="error error-keyup-ct31"> *</span>');
        error = 1;
    }else {
        $('span.error-keyup-ct31').remove();
    }

    if (error) {
       e.preventDefault();
    } else {
       window.location.href = action;
    }

})

// Reset Form
$("#btnreset, #btnreset1").click(function(){
    formid = $(this).closest("form").attr('id');
    if(formid == "caddressForm"){
        $("#caddressForm")[0].reset();
    }
    else{
        $("#caddressForm1")[0].reset();
    }
});

// Get local areas 
$("#choosestate, #choosestate1, #choosestate2, #choosestate11").on('change',function(){
    var dest = $(this).val();
    var countryname = $(this).attr("data-country");
    finalword = dest.split("_").join(" ");
    formid = $(this).closest("form").attr('id');
   /* if(formid == "caddressForm"){
        countryname = $("#choosecountry").val();
    }
    else{
        countryname = $("#choosecountry1").val();
    }*/
    selectarea = "";
    city = "";

    $.ajax({
      url:baseurl +'item/GetLocalAreas',
      type:"POST",
      data: {state:finalword,country:countryname},
      dataType:"json",
      success: function(response){
        if(response.msg == true){
            if(formid == "caddressForm"){
                $("#local_government").find('option').get(0).remove();
                $("#local_government2").find('option').get(0).remove();
            }
            else{
                $("#chooseaddresarea1").find('option').get(0).remove();
                $("#chooseaddresarea11").find('option').get(0).remove();
            }
            selectarea = "<option value=''>Choose Local Government</option>";
            $.each(response.listcities, function( index, value ){
                if(value.district){
                    city = value.district;
                }
                else if(value.local_area){
                    city = value.local_area;
                }
                selectarea+='<option value="'+city+'">'+city+'</option>';
            });
            if(formid == "caddressForm"){
                 $("#local_government").html(selectarea);
                $("#local_government2").html(selectarea);
            }
            else{
               $("#chooseaddresarea1").html(selectarea); 
               $("#chooseaddresarea11").html(selectarea);
            }
        }
      }
    });
});

// Get local areas 
$("#choosecountry, #choosecountry1, #choosecountry2, #choosecountry11").on('change',function(){
    var country = $(this).val();
    selectarea = "";
    city = "";
    formid = $(this).closest("form").attr('id');

    if(formid == "caddressForm"){
        $("#choosestate").attr("data-country",country); 
        $("#choosestate2").attr("data-country",country); 
    }
    else{
        $("#choosestate1").attr("data-country",country);
        $("#choosestate11").attr("data-country",country);
    }

    if(country.length > 0){ 
        $.ajax({
          url:baseurl +'item/GetstateBycountry',
          type:"POST",
          data: {countryname:country},
          dataType:"json",
          success: function(response){
            if(response.msg == true){
                if(formid == "caddressForm"){
                    $("#choosestate").find('option').get(0).remove();
                    $("#choosestate2").find('option').get(0).remove();
                }
                else{
                   $("#choosestate1").find('option').get(0).remove();
                    $("#choosestate11").find('option').get(0).remove();
                }
                selectarea = "<option value=''>Choose State</option>";
                $.each(response.liststates, function( index, value ){
                    if(value.province){
                        state = value.province;
                    }
                    else if(value.state_name){
                        state = value.state_name;
                    }
                    selectarea+='<option value="'+state.replace(/ /g, '_')+'">'+state+'</option>';
                });
                if(formid == "caddressForm"){
                    $("#local_government2").html("<option value=''>Choose Local Government</option>")
                    $("#local_government").html("<option value=''>Choose Local Government</option>")
                    $("#choosestate").html(selectarea);
                    $("#choosestate2").html(selectarea);
                }
                else{
                     $("#chooseaddresarea1").html("<option value=''>Choose Local Government</option>")
                    $("#chooseaddresarea11").html("<option value=''>Choose Local Government</option>")
                    $("#choosestate1").html(selectarea);
                    $("#choosestate11").html(selectarea);
                }
            }
          }
        });
    }
    else{
        if(formid == "caddressForm"){
             $("#local_government").html("<option value=''>Choose Local Government</option>")
            $("#local_government2").html("<option value=''>Choose Local Government</option>")
            $("#choosestate").html("<option value=''>Choose State</option>");
            $("#choosestate2").html("<option value=''>Choose State</option>");
        }
        else{
            $("#chooseaddresarea1").html("<option value=''>Choose Local Government</option>")
            $("#chooseaddresarea11").html("<option value=''>Choose Local Government</option>")
            $("#choosestate1").html("<option value=''>Choose State</option>");
            $("#choosestate11").html("<option value=''>Choose State</option>");
        }
    }
}); 

// 24-09-2018 Order confirm by ajax
$("#confrim-order-event").on('click',function(e){
    e.preventDefault();
    orderid = $(this).attr("data-id");
    $.ajax({
      url:baseurl +'item/ajax_order_confirm_by_id',
      type:"POST",
      data: {orderID:orderid},
      dataType:"json",
      beforeSend: function(){
        $(".action-container").html('<span class="process">Processing...</span>');
      },
      success: function(response){
        if(response.staus == true){
            alert(response.msg);
        }
        else{
            alert(response.msg);
        }
        location.reload();
      }
    });
});

// 25-09-2018 Order track if not confirm by ajax
$("#confrim-order-negative").on('click',function(e){
    e.preventDefault();
    orderid = $(this).attr("data-id");

    $.ajax({
      url:baseurl +'item/ajax_order_notconfirm_trackby_id',
      type:"POST",
      data: {orderID:orderid},
      dataType:"json",
      beforeSend: function(){
        $(".action-container").html('<span class="process">Processing...</span>');
      },
      success: function(response){
        if(response.staus == true){
            alert(response.msg);
        }
        else{
            alert(response.msg);
        }
        location.reload();
      }
    });
});

// Customer signup / In - 27-09-2018
$("#login-form-customer").on('submit', function (event) {
    //event.preventDefault();    
    var email = $('#emailaddress').val();
    var password = $('#userpassword').val();
    var error = 0;
    //var emailReg = "/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/";

    if (email.length == 0 || email.length == null ) {
        $('span.error-keyup-words55').remove();
        $('#emailaddress').after('<span class="error error-keyup-words55">* Email is required</span>');
        error = 1;
    } else if (!validateEmail(email)) {
        $('span.error-keyup-words55').remove();
        $('#emailaddress').after('<span class="error error-keyup-words55">* Please enter valid email.</span>');
        error = 1;
    } else {
        $('span.error-keyup-words55').remove();
    }

    if (password.length == 0 || password.length == null) {
        $('span.error-keyup-words4').remove();
        $('#userpassword').after('<span class="error error-keyup-words4">* Password is required.</span>');
        error = 1;
    } else {
        $('span.error-keyup-words4').remove();
    }

    if (error) {
        event.preventDefault();
    } else {
        $.ajax({
            type: 'POST',
            url: baseurl + 'customer/signUpwithLogin',
            dataType: "json",
            data: {
                useremail: email,
                userpwd: password
            },
            beforeSend: function () {
                $("#login-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Please Wait..');
            },
            success: function (response) {
                if (response.status == true) {
                    $("#login-submit").html('Signing In ...');
                    setTimeout('window.location.href = "' + response.msg + '";',2000);
                } else {
                    $(".error-user").html(response.msg).show();
                }
            },
            error: function (response) {
                $(".error-user").html(response.responseText).show();
            }
        });
        event.preventDefault();
    }
});

function validateEmail(Email) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    return $.trim(Email).match(pattern) ? true : false;
}

$("#makepayment").on('click',function(e){
       name = $("#name1").val();
       email =$("#email1").val();
       contact = $("#contact1").val();
       delivery_opt = $("input:radio.deliveryoption:checked").val();
       delivery_type = $("input:radio.deliverytype1:checked").val();
       options = $("#options1").length;
       //caddress = $(".deliverymethod-1").length;
       state = $("#selectstate1").val();
       optionsaddress = $("#deliverymethod-2").length;
       caddresses = $("input:radio.custaddressses1:checked").val();
       local_government = $("#selectarea1").val() ;
       address = $("#shiping_address1").val()
       method = $("input:radio.pay-method:checked").val()
       flag = 0;

       if(name.length == 0){
        $('span.text-danger.err11').remove();
        $("#name1").after("<span class='text-danger err11'>* Please enter name.</span>"); 
        flag = 1; 
       }
       else{
        $('span.text-danger.err11').remove();
       }

       if(email.length == 0){
        $('span.text-danger.err22').remove();
        $("#email1").after("<span class='text-danger err22'>* Please enter email.</span>"); 
        flag = 1; 
       }
       else if(!validateEmail(email)){
        $('span.text-danger.err22').remove();
        $('#email1').after('<span class="text-danger err22">Please enter valid email.</span>');
        flag=1;
       }
       else{
        $('span.text-danger.err22').remove();
       }

       if(contact.length == 0){
        $('span.text-danger.err33').remove();
        $('#contact1').after("<span class='text-danger err33'>* Please enter contact number.</span>"); 
        flag = 1; 
       }
       else{
        $('span.text-danger.err33').remove();
       }

       if(delivery_type === undefined){
        $('span.text-danger.err44').remove();
        $("#delitype1").after("<span class='text-danger err44'> * Please select delivery type.</span>"); 
        flag = 1; 
       }
       else{
        $('span.text-danger.err44').remove();
       }
       
       if(options == 1){
        if(delivery_opt === undefined){
            $('span.text-danger.err55').remove();
            $("#options1").after("<span class='text-danger err55'> * Please select delivery option.</span>"); 
            flag = 1; 
           }
           else{
            $('span.text-danger.err55').remove();
        }
       } 

       if(optionsaddress == 0){
           if(delivery_type == 'delivery' && state.length == 0){
            $('span.text-danger.err66').remove();
            $("#selectstate1").after("<span class='text-danger err66'> * Please select shipping state.</span>"); 
            flag = 1; 
           }
           else{
            $('span.text-danger.err66').remove();
           }

           if(delivery_type == 'delivery' && local_government.length == 0){
            $('span.text-danger.err77').remove();
            $("#selectarea1").after("<span class='text-danger err77'> * Please select local government.</span>"); 
            flag = 1; 
           }
           else{
            $('span.text-danger.err77').remove();
           }

           if(delivery_type == 'delivery' && address.length == 0){
            $('span.text-danger.err88').remove();
            $("#shiping_address1").after("<span class='text-danger err88'> * Please enter shipping address.</span>"); 
            flag = 1; 
           }
           else{
            $('span.text-danger.err88').remove();
           }        
       }
       else{
            if(delivery_type == 'delivery' && optionsaddress == 1 && caddresses == undefined){
                $('span.text-danger.err104').remove();
                $("#deliverymethod-2").after("<span class='text-danger err104'> * Please choose address.</span>"); 
                flag = 1;
           }
           else{
                $('span.text-danger.err104').remove();
           }
       }


       if(method === undefined){
        $('span.text-danger.err99').remove();
        $("#paymethod1").after("<span class='text-danger err99'> * Please choose payment method.</span>"); 
        flag = 1; 
       }
       else{
        $('span.text-danger.err99').remove();
       }

       if(flag){
           e.preventDefault();
       }
})

// Hide address of pickup in store
$(".deliverytype1").on('click',function(){
    type = $(this).val();

    if(type == "pickup"){
        $(".deliverymethod").hide();
    }
    else if(type == "delivery"){
        $(".deliverymethod").show();
    }
})

// Voucher apply ajax checkout
$(document).on('click','#btnapply',function(event){
    //event.preventDefault();    
    var couponcode = $('#coupon').val();
    var user = $('#coupon').attr('data-user');
    var total = $('#coupon').attr('data-amount');
   
    var error = 0;
    if (couponcode.length == 0) {
        $('span.error-keyup-wordsc').remove();
        $('.input-wrap').after('<span class="error error-keyup-wordsc">* Please enter voucher code.</span>');
        error = 1;
    } else {
        $('span.error-keyup-wordsc').remove();
    }
    //alert(1)
    if (error) {
        event.preventDefault();
    } else {
        $.ajax({
            type: 'POST',
            url: baseurl + 'customer/checkCoupon',
            dataType: "json",
            data: {
                voucher: couponcode,
                userID: user,
                amount: total,
            },
            beforeSend: function () {
                $("#btnapply").html('<span class="glyphicon glyphicon-transfer"></span>Wait..');
            },
            success: function (response) {
                if(response.status == true){
                    $("#coupon").val('');
                    $(".total-section").load(location.href + " .total-section");
                    //$(".coupon-apply-section").load(location.href + " .coupon-apply-section");
                    $('span.error-keyup-wordsc').remove();
                    $(".discountamount").text(response.discount);
                    $(".gtotal").text(response.finaltotal);
                    $("#btnapply").attr('disabled','disabled');
                }
                else{
                    //$("#btnapply").html('<span class="glyphicon glyphicon-transfer"></span>Apply');
                    $('.input-wrap').after('<span class="error error-keyup-wordsc">* Invalid coupon code.</span>');
                }
                $("#btnapply").html('<span class="glyphicon glyphicon-transfer"></span>Apply');
            }
        });
    }
});

// Remove voucher code
$(document).on( "click", "#removecode", function(e) {
    e.preventDefault();
    redeemid = $(this).attr("data-id");
    if (confirm('Remove coupon code ?')) {
        $.ajax({
            type: 'POST',
            url: baseurl + 'customer/removeCoupon',
            dataType: "json",
            data: {
                coupon: redeemid
            },
            success: function (response) {
                if(response.status == true){
                    $("#btnapply").removeAttr("disabled");
                    //$(".coupon-apply-section").load(location.href + " .coupon-apply-section");
                    $(".total-section").load(location.href + " .total-section");
                }
            }
        });  
    }
});